package com.cordialsr.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cordialsr.domain.deserializer.ContractStornoRequestDeserializer;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@JsonDeserialize(using = ContractStornoRequestDeserializer.class)
@Table(name = "contract_storno_request")
public class ContractStornoRequest implements java.io.Serializable, Cloneable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final int STATUS_NEW = 1;
	public static final int STATUS_APPROVED = 2;
	public static final int STATUS_CANCEL_NEW = -1;
	public static final int STATUS_CANCEL_APPROVED = -2;
	
	private Long id;
	private Company company;
	private Branch branch;
	private String reqNumber;
	private Contract contract;
	private User reqAuthor;
	private Calendar reqDate;
	private Integer status;
	private String info;
	private BigDecimal returnAmountDue;
	private Currency currency;
	private BigDecimal returnAmount;
	private User grantAuthor;
	private Calendar grantDate;
	private Date closeDate;
	
	public ContractStornoRequest() {
		
	}
	
	public ContractStornoRequest(Long id) {
		this.id = id;
	}
	
	public ContractStornoRequest(Long id, Company company, Branch branch, String reqNumber,
					Contract contract, User reqAuthor, Calendar reqDate, Integer status, 
					String info, BigDecimal returnAmountDue, Currency currency,
					BigDecimal returnAmount, User grantAuthor, Calendar grantDate, Date closeDate) {
		this.id = id;
		this.company = company;
		this.branch = branch;
		this.reqNumber = reqNumber;
		this.contract = contract;
		this.reqAuthor = reqAuthor;
		this.reqDate = reqDate;
		this.status = status;
		this.info = info;
		this.returnAmountDue = returnAmountDue;
		this.currency = currency;
		this.returnAmount = returnAmount;
		this.grantAuthor = grantAuthor;
		this.grantDate = grantDate;
		this.closeDate = closeDate;
	}

	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company")
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch")
	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	@Column(name = "req_number", nullable = false)
	public String getReqNumber() {
		return reqNumber;
	}

	public void setReqNumber(String reqNumber) {
		this.reqNumber = reqNumber;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contract_id")
	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "req_author")
	public User getReqAuthor() {
		return reqAuthor;
	}

	public void setReqAuthor(User reqAuthor) {
		this.reqAuthor = reqAuthor;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "req_date", nullable = false)
	public Calendar getReqDate() {
		return reqDate;
	}

	public void setReqDate(Calendar reqDate) {
		this.reqDate = reqDate;
	}

	@Column(name = "status", nullable = false)
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Column(name = "info", nullable = false)
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@Column(name = "return_amount_due", nullable = false)
	public BigDecimal getReturnAmountDue() {
		return returnAmountDue;
	}

	public void setReturnAmountDue(BigDecimal returnAmountDue) {
		this.returnAmountDue = returnAmountDue;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currency")
	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	@Column(name = "return_amount")
	public BigDecimal getReturnAmount() {
		return returnAmount;
	}

	public void setReturnAmount(BigDecimal returnAmount) {
		this.returnAmount = returnAmount;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "grant_author")
	public User getGrantAuthor() {
		return grantAuthor;
	}

	public void setGrantAuthor(User grantAuthor) {
		this.grantAuthor = grantAuthor;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "grant_date")
	public Calendar getGrantDate() {
		return grantDate;
	}

	public void setGrantDate(Calendar grantDate) {
		this.grantDate = grantDate;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "close_date")
	public Date getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}

	@Override
	public ContractStornoRequest clone() throws CloneNotSupportedException {
		return (ContractStornoRequest) super.clone();
	}
}
