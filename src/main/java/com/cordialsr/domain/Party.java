package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.cordialsr.domain.deserializer.PartyDeserializer;
import com.cordialsr.domain.security.User;
import com.cordialsr.general.GeneralUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "party")
@JsonDeserialize(using = PartyDeserializer.class)
public class Party implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Company company;
	private PartyType partyType;
	private String iinBin;
	private String firstname;
	private String lastname;
	private String middlename;
	private String email;
	private Date birthday;
	private Boolean isYur;
	private String passportNumber;
	private Date dateIssue;
	private Date dateExpire;
	private String issuedInstance;
	private String iik;
	private String bik;
	private String companyName;
	private Date cpuDate;
	private String info;
	private String target;
	private String oldId;
//	private Boolean gender;
	private String familyStatus;
	private String birthPlace;
	private Country residency;
//	private Integer children;
	private Party original;
	
	private PartyInfo partyInfo;
	
	@Transient
	private String positions;
	
	@Transient
	private Boolean actual;
	
	private Photo photo;	
	
	private Set<Address> addresses = new HashSet<>(0);
	private Set<PhoneNumber> phoneNumbers = new HashSet<>(0);
	
//	private Set<KeySkill> keySkill = new HashSet<>(0);
	private Set<PartyExperience> partyExperience = new HashSet<>(0);
	private Set<PartyEducation> partyEducation = new HashSet<>(0);
	private Set<PartyRelatives> partyRelatives = new HashSet<>(0);
//	private Set<PartyInfo> partyInfo = new HashSet<>(0);
	
	private User userCreated; 
	
	public Party() {
	}

	public Party(Long id) {
		this.id = id;
	}

	public Party(Company company, PartyType partyType, String iinBin, String firstname, String lastname, String middlename, String email,
			Date birthday, Boolean isYur, String passportNumber, Date dateIssue, Date dateExpire, String issuedInstance,
			String iik, String bik, Photo photo,Set<Address> addresses, Set<PhoneNumber> phoneNumbers, Date cpuDate,
			String info, String target, Set<PartyExperience> partyExperience, Set<PartyEducation> partyEducation,
			String familyStatus, String birthPlace, Country residency, Party original, PartyInfo partyInfo, Set<PartyRelatives> partyRelatives,
			User userCreated) {
		this.company = company;
		this.partyType = partyType;
		this.iinBin = iinBin;
		this.firstname = firstname;
		this.lastname = lastname;
		this.middlename = middlename;
		this.email = email;
		this.birthday = birthday;
		this.isYur = isYur;
		this.passportNumber = passportNumber;
		this.dateIssue = dateIssue;
		this.dateExpire = dateExpire;
		this.issuedInstance = issuedInstance;
		this.iik = iik;
		this.bik = bik;
		this.photo = photo;
		this.addresses = addresses;
		this.phoneNumbers = phoneNumbers;
		this.cpuDate = cpuDate;
		this.info = info;
		this.target = target;
		this.partyExperience = partyExperience;
		this.partyEducation = partyEducation;
		this.familyStatus = familyStatus;
		this.birthPlace = birthPlace;
		this.original = original;
//		this.keySkill = keySkill;
		this.partyInfo = partyInfo;
		this.partyRelatives = partyRelatives;
		this.userCreated = userCreated;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "party_type", nullable = false)
	public PartyType getPartyType() {
		return this.partyType;
	}

	public void setPartyType(PartyType partyType) {
		this.partyType = partyType;
	}

	@Column(name = "iin_bin", length = 20)
	public String getIinBin() {
		return this.iinBin;
	}

	public void setIinBin(String iinBin) {
		this.iinBin = iinBin;
	}

	@Column(name = "firstname", length = 45)
	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	@Column(name = "lastname", length = 45)
	public String getLastname() {
		return this.lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@Column(name = "middlename", length = 45)
	public String getMiddlename() {
		return this.middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	@Column(name = "email", length = 45)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "birthday", length = 10)
	public Date getBirthday() {
		return this.birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	@Column(name = "is_yur")
	public Boolean getIsYur() {
		return this.isYur;
	}

	public void setIsYur(Boolean isYur) {
		this.isYur = isYur;
	}

	@Column(name = "passport_number", length = 45)
	public String getPassportNumber() {
		return this.passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "date_issue", length = 10)
	public Date getDateIssue() {
		return this.dateIssue;
	}

	public void setDateIssue(Date dateIssue) {
		this.dateIssue = dateIssue;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "date_expire", length = 10)
	public Date getDateExpire() {
		return this.dateExpire;
	}

	public void setDateExpire(Date dateExpire) {
		this.dateExpire = dateExpire;
	}

	@Column(name = "issued_instance", length = 45)
	public String getIssuedInstance() {
		return this.issuedInstance;
	}

	public void setIssuedInstance(String issuedInstance) {
		this.issuedInstance = issuedInstance;
	}

	@Column(name = "iik", length = 45)
	public String getIik() {
		return this.iik;
	}

	public void setIik(String iik) {
		this.iik = iik;
	}

	@Column(name = "bik", length = 45)
	public String getBik() {
		return this.bik;
	}

	public void setBik(String bik) {
		this.bik = bik;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "party", cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<Address> getAddresses() {
		return this.addresses;
	}

	public void setAddresses(Set<Address> addresses) {
		this.addresses = addresses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "party", cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<PhoneNumber> getPhoneNumbers() {
		return this.phoneNumbers;
	}

	public void setPhoneNumbers(Set<PhoneNumber> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}
	
//	@JsonIgnore
//	@ManyToMany(fetch = FetchType.LAZY)
//	@JoinTable(name = "party_has_skill", joinColumns = {
//			@JoinColumn(name = "party_id", nullable = false, updatable = false) }, inverseJoinColumns = {
//					@JoinColumn(name = "key_skill_id", nullable = false, updatable = false) })
//	public Set<KeySkill> getKeySkill() {
//		return this.keySkill;
//	}
//
//	public void setKeySkill(Set<KeySkill> keySkill) {
//		this.keySkill = keySkill;
//	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "party", cascade = CascadeType.ALL)
	public Set<PartyRelatives> getPartyRelatives() {
		return this.partyRelatives;
	}

	public void setPartyRelatives(Set<PartyRelatives> partyRelatives) {
		this.partyRelatives = partyRelatives;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "party", cascade = CascadeType.ALL)
	public Set<PartyExperience> getPartyExperience() {
		return this.partyExperience;
	}
	
	public void setPartyExperience(Set<PartyExperience> partyExperience) {
		this.partyExperience = partyExperience;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "party", cascade = CascadeType.ALL)
	public Set<PartyEducation> getPartyEducation() {
		return this.partyEducation;
	}
	
	public void setPartyEducation(Set<PartyEducation> partyEducation) {
		this.partyEducation = partyEducation;
	}
	
//	@OneToMany(fetch = FetchType.LAZY, mappedBy = "party", cascade = CascadeType.ALL)
//	public Set<PartyInfo> getPartyInfo() {
//		return this.partyInfo;
//	}
//	
//	public void setPartyInfo(Set<PartyInfo> partyInfo) {
//		this.partyInfo = partyInfo;
//	}
	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY,
			  cascade = CascadeType.ALL,
			  mappedBy = "party")
	public PartyInfo getPartyInfo( ) {
		return this.partyInfo;
	}
	
	public void setPartyInfo(PartyInfo partyInfo) {
		this.partyInfo = partyInfo;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "cpu_date", length = 10)
	public Date getCpuDate() {
		return cpuDate;
	}

	public void setCpuDate(Date cpuDate) {
		this.cpuDate = cpuDate;
	}
	
	@Column(name = "info", length = 225)
	public String getInfo() {
		return this.info;
	}
	
	public void setInfo(String info) {
		this.info = info;
	}
	
	@Column(name = "target", length = 45)
	public String getTarget() {
		return this.target;
	}
	
	public void setTarget(String target) {
		this.target = target;
	}

	@Column(name = "old_id", length = 15)
	public String getOldId() {
		return oldId;
	}

	public void setOldId(String oldId) {
		this.oldId = oldId;
	}
	
	@Column(name = "family_status")
	public String getFamilyStatus() {
		return this.familyStatus;
	}

	public void setFamilyStatus(String familyStatus) {
		this.familyStatus = familyStatus;
	}
	
	
	@Column(name = "birth_place")
	public String getBirthPlace() {
		return this.birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}	

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "residency")
	public Country getResidency() {
		return this.residency;
	}

	public void setResidency(Country residency) {
		this.residency = residency;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "original")
	public Party getOriginal() {
		return this.original;
	}

	public void setOriginal(Party original) {
		this.original = original;
	}
	
	// ***********************************************************
	
	@Transient
	public String getFullFIO() {
		String fio = "";
		if (this.lastname != null) fio = this.lastname;
		fio += (fio.length() > 0) ? " " : "";
		if (this.firstname != null) fio += this.firstname;
		fio += (fio.length() > 0) ? " " : "";
		if (this.middlename != null) fio += this.middlename;
		if (GeneralUtil.isEmptyString(fio) || fio.equals("null null null")) fio = this.companyName;
		return fio;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company", nullable = false)
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
//		result = prime * result + ((addresses == null) ? 0 : addresses.hashCode());
		result = prime * result + ((bik == null) ? 0 : bik.hashCode());
		result = prime * result + ((birthday == null) ? 0 : birthday.hashCode());
		result = prime * result + ((company == null || company.getId() == null) ? 0 : company.getId().hashCode());
		result = prime * result + ((companyName == null) ? 0 : companyName.hashCode());
		result = prime * result + ((dateExpire == null) ? 0 : dateExpire.hashCode());
		result = prime * result + ((dateIssue == null) ? 0 : dateIssue.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((iik == null) ? 0 : iik.hashCode());
		result = prime * result + ((iinBin == null) ? 0 : iinBin.hashCode());
		result = prime * result + ((isYur == null) ? 0 : isYur.hashCode());
		result = prime * result + ((issuedInstance == null) ? 0 : issuedInstance.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((middlename == null) ? 0 : middlename.hashCode());
		result = prime * result + ((partyType == null || partyType.getName() == null) ? 0 : partyType.getName().hashCode());
		result = prime * result + ((passportNumber == null) ? 0 : passportNumber.hashCode());
//		result = prime * result + ((phoneNumbers == null) ? 0 : phoneNumbers.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Party other = (Party) obj;
//		if (addresses == null) {
//			if (other.addresses != null)
//				return false;
//		} else if (!addresses.equals(other.addresses))
//			return false;
		if (bik == null) {
			if (other.bik != null)
				return false;
		} else if (!bik.equals(other.bik))
			return false;
		if (birthday == null) {
			if (other.birthday != null)
				return false;
		} else if (!birthday.equals(other.birthday))
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.getId().equals(other.company.getId()))
			return false;
		if (companyName == null) {
			if (other.companyName != null)
				return false;
		} else if (!companyName.equals(other.companyName))
			return false;
		if (dateExpire == null) {
			if (other.dateExpire != null)
				return false;
		} else if (!dateExpire.equals(other.dateExpire))
			return false;
		if (dateIssue == null) {
			if (other.dateIssue != null)
				return false;
		} else if (!dateIssue.equals(other.dateIssue))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (iik == null) {
			if (other.iik != null)
				return false;
		} else if (!iik.equals(other.iik))
			return false;
		if (iinBin == null) {
			if (other.iinBin != null)
				return false;
		} else if (!iinBin.equals(other.iinBin))
			return false;
		if (isYur == null) {
			if (other.isYur != null)
				return false;
		} else if (!isYur.equals(other.isYur))
			return false;
		if (issuedInstance == null) {
			if (other.issuedInstance != null)
				return false;
		} else if (!issuedInstance.equals(other.issuedInstance))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (middlename == null) {
			if (other.middlename != null)
				return false;
		} else if (!middlename.equals(other.middlename))
			return false;
		if (partyType == null) {
			if (other.partyType != null)
				return false;
		} else if (!partyType.getName().equals(other.partyType.getName()))
			return false;
		if (passportNumber == null) {
			if (other.passportNumber != null)
				return false;
		} else if (!passportNumber.equals(other.passportNumber))
			return false;
//		if (phoneNumbers == null) {
//			if (other.phoneNumbers != null)
//				return false;
//		} else if (!phoneNumbers.equals(other.phoneNumbers))
//			return false;
		return true;
	}

	@Column(name = "company_name", length = 45)
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@JsonIgnore
	@Transient
	public String getPhotoString() {
		String str = "";
		if (this.photo != null) {
			try {
				str = photo.getPhotoString();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "photo_id")
	public Photo getPhoto() {
		return photo;
	}
	public void setPhoto(Photo photo) {
		this.photo = photo;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_created")
	public User getUserCreated() {
		return userCreated;
	}

	public void setUserCreated(User userCreated) {
		this.userCreated = userCreated;
	}
	
	@Transient
	public String getPositions() {
		return positions;
	}

	@Transient
	public void setPositions(String positions) {
		this.positions = positions;
	}

	@Transient
	public Boolean getActual() {
		return actual;
	}

	@Transient
	public void setActual(Boolean actual) {
		this.actual = actual;
	}

	
	@Override
	public Party clone() throws CloneNotSupportedException {
		return (Party) super.clone();
	}
}
