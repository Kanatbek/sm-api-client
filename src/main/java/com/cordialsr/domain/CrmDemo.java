package com.cordialsr.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Time;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
//@JsonDeserialize(using = DemoDeserializer.class)
@Table(name = "crm_demo")
public class CrmDemo implements java.io.Serializable, Cloneable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Employee dealer;
	private CrmLead lead;
	private Date demoDate;
	private Date demoTime;
	private Date cpuDate;
	private Date cpuTime;
	private User user;
	private String status;
	private String info;
	private Long transport;
	private Currency currency;
	private CrmReason reason;
	private Party party;
	private Company company;
	private Branch branch;
	
	public CrmDemo() {
		
	}

	public CrmDemo(Integer id, Employee dealer, CrmLead lead, Date demoDate, Date demoTime, Date cpuDate, Date cpuTime,
			User user, String status, String info, Long transport, Currency currency, CrmReason reason) {
		super();
		this.id = id;
		this.dealer = dealer;
		this.lead = lead;
		this.demoDate = demoDate;
		this.demoTime = demoTime;
		this.cpuDate = cpuDate;
		this.cpuTime = cpuTime;
		this.user = user;
		this.status = status;
		this.info = info;
		this.transport = transport;
		this.currency = currency;
		this.reason = reason;
	}
	
	public CrmDemo(Integer id, Employee dealer, CrmLead lead, Date demoDate, Date demoTime, Date cpuDate, Date cpuTime,
			User user, String status, String info, Long transport, Currency currency, CrmReason reason, Company company,
			Branch branch, Party party) {
		this.id = id;
		this.dealer = dealer;
		this.lead = lead;
		this.demoDate = demoDate;
		this.demoTime = demoTime;
		this.cpuDate = cpuDate;
		this.cpuTime = cpuTime;
		this.user = user;
		this.status = status;
		this.info = info;
		this.transport = transport;
		this.currency = currency;
		this.reason = reason;
		this.company = company;
		this.branch = branch;
		this.party = party;
	}
	
	
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "dealer")
	public Employee getDealer() {
		return dealer;
	}

	public void setDealer(Employee dealer) {
		this.dealer = dealer;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "lead")
	public CrmLead getLead() {
		return lead;
	}

	public void setLead(CrmLead lead) {
		this.lead = lead;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "demo_date", length = 10)
	public Date getDemoDate() {
		return demoDate;
	}

	public void setDemoDate(Date demoDate) {
		this.demoDate = demoDate;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "demo_time", length = 10)
	public Date getDemoTime() {
		return demoTime;
	}

	public void setDemoTime(Date demoTime) {
		this.demoTime = demoTime;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "cpu_date", length = 10)
	public Date getCpuDate() {
		return cpuDate;
	}

	public void setCpuDate(Date cpuDate) {
		this.cpuDate = cpuDate;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "cpu_time", length = 10)
	public Date getCpuTime() {
		return cpuTime;
	}

	public void setCpuTime(Date cpuTime) {
		this.cpuTime = cpuTime;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "info")
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@Column(name = "transport")
	public Long getTransport() {
		return transport;
	}

	public void setTransport(Long transport) {
		this.transport = transport;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currency")
	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	@ManyToOne(fetch = FetchType.LAZY)	
	@JoinColumn(name = "reason")
	public CrmReason getReason() {
		return reason;
	}

	public void setReason(CrmReason reason) {
		this.reason = reason;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party")
	public Party getParty() {
		return this.party;
	}

	public void setParty(Party party) {
		this.party = party;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company")
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch")
	public Branch getBranch() {
		return this.branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	@Override
	public CrmDemo clone() throws CloneNotSupportedException {
		return (CrmDemo) super.clone();
	}
	
}
