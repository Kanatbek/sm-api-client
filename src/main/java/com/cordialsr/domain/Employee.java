package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.cordialsr.domain.deserializer.EmployeeDeserializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "employee")
@JsonDeserialize(using = EmployeeDeserializer.class)
public class Employee implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Party party;
	private Company company;
	private Region region;
	private Branch branch;
	private Department department;
	private Position position;
	private BigDecimal salary;
	private Currency currency;
	private Date dateHired;
	private Date dateFired;
	private Employee accountableTo;
	private BigDecimal officialSalary;
	private Calendar cpuDate;
	private String info;
	
	@Transient
	private Integer contractNumber;
	
	@Transient
	private Integer branchContractNumber;
	
	@Transient
	private Integer companyContractNumber;
	
	
	public Employee() {
	}

	public Employee(Long id) {
		this.id = id;
	}

	public Employee(Party party, Company company, Region region, Branch branch, Department department, Position position,
			BigDecimal salary, Currency currency, Date dateHired, Date dateFired, Employee accountableTo, BigDecimal officialSalary, String info) {
		this.party = party;
		this.company = company;
		this.region = region;
		this.branch = branch;
		this.department = department;
		this.position = position;
		this.salary = salary;
		this.currency = currency;
		this.dateHired = dateHired;
		this.dateFired = dateFired;
		this.accountableTo = accountableTo;
		this.officialSalary = officialSalary;
		this.info = info;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party_id", nullable = false)
	public Party getParty() {
		return this.party;
	}

	public void setParty(Party party) {
		this.party = party;
	}
	

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch_id", nullable = false)	
	public Branch getBranch() {
		return this.branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	// ************************************************************************************************
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currency")
	public Currency getCurrency() {
		return this.currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "position_id", nullable = false)
	public Position getPosition() {
		return this.position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	@Column(name = "salary", precision = 21, scale = 12)
	public BigDecimal getSalary() {
		return this.salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}
	
	@Column(name = "official_salary", precision = 21, scale = 12)
	public BigDecimal getOfficialSalary() {
		return this.officialSalary;
	}

	public void setOfficialSalary(BigDecimal officialSalary) {
		this.officialSalary = officialSalary;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "date_hired", length = 10)
	public Date getDateHired() {
		return this.dateHired;
	}

	public void setDateHired(Date dateHired) {
		this.dateHired = dateHired;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "date_fired", length = 10)
	public Date getDateFired() {
		return this.dateFired;
	}

	public void setDateFired(Date dateFired) {
		this.dateFired = dateFired;
	}

	// *****************************************************************
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company_id")
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "department")
	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "region")
	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "accountable_to")
	public Employee getAccountableTo() {
		return accountableTo;
	}

	public void setAccountableTo(Employee accountableTo) {
		this.accountableTo = accountableTo;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cpu_date")
	public Calendar getCpuDate() {
		return cpuDate;
	}

	public void setCpuDate(Calendar cpuDate) {
		this.cpuDate = cpuDate;
	}
	
	@Column(name = "info")
	public String getInfo() {
		return info;
	}
	
	public void setInfo(String info) {
		this.info = info;
	}
	
	@Override
	public Employee clone() throws CloneNotSupportedException {
		return (Employee) super.clone();
	}

	
	
	// ****************************************************************************************************
	
	@Override
	public String toString() {
		return "Employee [id=" + id + ", party=" + ((party != null) ? party.getFullFIO() : "null")
				+ ", company=" + ((company != null) ? company.getName() : "null" ) 
				+ ", branch=" + ((branch != null) ? branch.getBranchName() : "null") 
				+ ", department=" + ((department != null) ? department.getName() : "null")
				+ ", region= " + ((region != null) ? region.getName() : "null") 
				+ ", position=" + ((position != null) ? position.getName() : "null")
				+ ", salary=" + salary + ", currency=" + ((currency != null) ? currency.getCurrency() : "null")
				+ ", dateHired=" + dateHired + ", dateFired=" + dateFired 
				+ ", accountableTo=" + ((accountableTo != null && accountableTo.party != null) 
						? accountableTo.party.getFullFIO() : "null") + "]";
	}
	
	
	
	@Transient
	public Integer getContractNumber() {
		return contractNumber;
	}
	
	
	@Transient
	public void setContractNumber(Integer contractNumber) {
		this.contractNumber = contractNumber;
	}
	
	@Transient
	public Integer getBranchContractNumber() {
		return branchContractNumber;
	}
	
	
	@Transient
	public void setBranchContractNumber(Integer branchContractNumber) {
		this.branchContractNumber = branchContractNumber;
	}
	
	@Transient
	public Integer getCompanyContractNumber() {
		return companyContractNumber;
	}
	
	
	@Transient
	public void setCompanyContractNumber(Integer companyContractNumber) {
		this.companyContractNumber = companyContractNumber;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountableTo == null || accountableTo.getId() == null) ? 0 : accountableTo.getId().hashCode());
		result = prime * result + ((branch == null || branch.getId() == null) ? 0 : branch.getId().hashCode());
		result = prime * result + ((company == null || company.getId() == null) ? 0 : company.getId().hashCode());
		result = prime * result + ((currency == null || currency.getCurrency() == null) ? 0 : currency.getCurrency().hashCode());
		result = prime * result + ((dateFired == null) ? 0 : dateFired.hashCode());
		result = prime * result + ((dateHired == null) ? 0 : dateHired.hashCode());
		result = prime * result + ((department == null || department.getId() == null) ? 0 : department.getId().hashCode());
		result = prime * result + ((region == null || region.getId() == null) ? 0 : region.getId().hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((party == null || party.getId() == null) ? 0 : party.getId().hashCode());
		result = prime * result + ((position == null || position.getId() == null) ? 0 : position.getId().hashCode());
		result = prime * result + ((salary == null) ? 0 : salary.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		
		
		if (accountableTo == null) {
			if (other.accountableTo != null)
				return false;
		} else if (!accountableTo.getId().equals(other.accountableTo.getId()))
			return false;
		if (branch == null) {
			if (other.branch != null)
				return false;
		} else if (!branch.getId().equals(other.branch.getId()))
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.getId().equals(other.company.getId()))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.getCurrency().equals(other.currency.getCurrency()))
			return false;
		if (dateFired == null) {
			if (other.dateFired != null)
				return false;
		} else if (!dateFired.equals(other.dateFired))
			return false;
		if (dateHired == null) {
			if (other.dateHired != null)
				return false;
		} else if (!dateHired.equals(other.dateHired))
			return false;
		if (department == null) {
			if (other.department != null)
				return false;
		} else if (!department.getId().equals(other.department.getId()))
			return false;
		if (region == null) {
			if (other.region != null) 
				return false;
		} else if (!region.getId().equals(other.region.getId()))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (party == null) {
			if (other.party != null)
				return false;
		} else if (!party.getId().equals(other.party.getId()))
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.getId().equals(other.position.getId()))
			return false;
		if (salary == null) {
			if (other.salary != null)
				return false;
		} else if (!salary.equals(other.salary))
			return false;
		return true;
	}
	
}
