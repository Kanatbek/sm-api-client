package com.cordialsr.domain.module;

import com.cordialsr.domain.SmService;
import com.cordialsr.domain.deserializer.SmServiceDeserializer;
import com.fasterxml.jackson.databind.Module.SetupContext;
import com.fasterxml.jackson.databind.module.SimpleDeserializers;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.module.SimpleSerializers;

public class CustomModule extends SimpleModule {

	  @Override
	  public void setupModule(SetupContext context) {

//	    SimpleSerializers serializers = new SimpleSerializers();
	    SimpleDeserializers deserializers = new SimpleDeserializers();

//	    serializers.addSerializer(SmService.class, new MyEntitySerializer());
	    deserializers.addDeserializer(SmService.class, new SmServiceDeserializer());

//	    context.addSerializers(serializers);
	    context.addDeserializers(deserializers);
	  }
}