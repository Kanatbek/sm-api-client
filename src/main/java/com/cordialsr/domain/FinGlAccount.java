package com.cordialsr.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "fin_gl_account")
public class FinGlAccount implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String A_ST_CASH_ON_HAND_1010 = "1010";
	public static final String A_ST_CASH_ON_BANK_1030 = "1030"; // cash on current bank accounts
	public static final String A_ST_RECEIVABLES_FROM_CUSTOMER_1210 = "1210";
	public static final String A_ST_ACCOUNTABLE_EMPLOYEE_1250 = "1250"; // receivables of employees
	public static final String A_ST_LEASE_RECEIVABLES_1260 = "1260"; // lease receivables
	public static final String A_ST_OTHER_RECEIVABLES_1280 = "1280";
	public static final String A_ST_RAW_MATERIALS_1310 = "1310";
	public static final String A_ST_READY_PRODUCT_1320 = "1320";
	public static final String A_ST_GOODS_1330 = "1330";
	public static final String A_ST_OTHER_STOCKS_1350 = "1350";
	public static final String A_ST_VALUE_ADDED_TAX_1420 = "1420";
	public static final String A_ST_ADVANCE_1610 = "1610";
	
	public static final String A_LT_RECEIVABLES_FROM_CUSTOMER_2110 = "2110"; // Долгосрочные дебиторские задолженности покупателей и заказчиков
	public static final String A_LT_FIXED_ASSETS_2410 = "2410"; // Основные средства
	public static final String P_LT_DEPRECIATION_OF_FIXED_ASSETS_2420 = "2420"; // Амортизация основных средств
	public static final String A_LT_GOODWILL_2710 = "2710"; // Нематериальные активы
	public static final String P_LT_DEPRECIATION_OF_OTHER_ASSETS_2740 = "2740";
	
	public static final String P_ST_BANK_LOANS_3010 = "3010";
	public static final String P_ST_CORPORATE_INCOME_TAX_3110 = "3110";
	public static final String P_ST_INDIVIDUAL_INCOME_TAX_3120 = "3120";
	public static final String P_ST_VALUE_ADDED_TAX_3130 = "3130";
	public static final String P_ST_EXCISES_3140 = "3140";
	public static final String P_ST_SOCIAL_TAX_3150 = "3150";
	public static final String P_ST_LAND_TAX_3160 = "3160";
	public static final String P_ST_VEHICLE_TAX_3170 = "3170";
	public static final String P_ST_PROPERTY_TAX_3180 = "3180";
	public static final String P_ST_OTHER_LIABILITIES_3190 = "3190";
	public static final String P_ST_LIABILITIES_FOR_SOCIAL_INSURANCE_3210 = "3210"; // social security obligations // liabilities for social insurance
	public static final String P_ST_LIABILITIES_ON_PENSION_CONTRIBUTIONS_3220 = "3220"; // liabilities on pension contributions
	
	public static final String P_ST_PROVIDER_3310 = "3310";
	
	public static final String P_ST_PAYROLL_DEBT_3350 = "3350"; // short-term payroll debt
	public static final String P_ST_PAYROLL_DEBT_TRANSIT_3351 = "3351"; // Short-term payroll debt TRANSIT
	
	public static final String P_ST_INTEREST_PAYABLE_3380 = "3380"; // short-term interest payable
	public static final String P_ST_ADVANCES_RECEIVED_3510 = "3510"; // short-term advances received
	public static final String P_ST_OTHER_PAYABLES_3390 = "3390"; // other short-term debts payables
	public static final String P_ST_PENALTY_FOR_DELAY_3397 = "3397"; // Пеня за просрочку кредита
	
	public static final String P_LT_BANK_LOANS_4010 = "4010";
	public static final String P_LT_REMUNERATION_TO_PAYMENT_4160 = "4160"; // long-term remuneration to payment
	
	public static final String P_ST_DEPOSITS_AND_SHARES_5030 = "5030"; // deposits and shares
	public static final String P_ST_UNPAID_CAPITAL_5110 = "5110"; // Unpaid capital
	public static final String P_ST_RETAINED_EARNINGS_5510 = "5510"; // retained earnings
	public static final String P_ST_TOTAL_PROFIT_5610 = "5610"; // Total profit
	
	public static final String P_IN_REVENUE_6010 = "6010"; // Revenues from sales and services provided
	public static final String P_IN_RETURN_OF_SOLD_PRODUCTS_6020 = "6020";
	public static final String P_IN_DISCOUNT_FROM_SALES_PRICE_6030 = "6030"; // discount from sales price
	public static final String P_IN_OTHER_INCOME_FROM_FINANCING_6160 = "6160"; // other income from financing
	public static final String P_IN_GAINS_ON_DISPOSAL_OF_ASSETS_6210 = "6210"; // gains on disposal of assets
	public static final String P_IN_INCOME_FROM_EX_RATE_DIFF_6250 = "6250"; // income from exchange rate differences
	
	public static final String A_EX_COST_OF_SALES_7010 = "7010"; // cost of sales and services provided
	public static final String A_EX_EXPENSES_FOR_SALES_7110 = "7110"; // expenses for sales of products and services
	public static final String A_EX_ADMIN_EXPENSES_7210 = "7210";
	public static final String A_EX_EXPENSES_REMUNERATION_7310 = "7310"; // expenses on remuneration
	public static final String A_EX_OTHER_FINANCING_COSTS_7340 = "7340"; // other financing costs
	public static final String A_EX_ASSETS_RETIREMENT_COSTS_7410 = "7410";
	public static final String A_EX_ASSETS_DEVALUATION_7420 = "7420"; // offset of depreciation of fixed assets
	public static final String A_EX_EXCHANGE_EXPENSE_7430 = "7430"; // exchange expense
	public static final String A_EX_OTHER_EXPENSES_7470 = "7470";	
	
	public static final Integer AKT_ACTIVE = 0;
	public static final Integer AKT_PASSIVE = 1;

	private String code;
	private String name;
	private String info;
	private FinGlAccount parentAccount;
	private String taxCode;
	private FinGlSection finGlSection;
	
	private Boolean zab;
	private Integer akt;
	private Boolean kol;
	private Boolean val;
	
	private Set<FinGlAccount> childAccounts = new HashSet<>();
	
	public FinGlAccount() {
	}
	
	public FinGlAccount(String code) {
		this.code = code;
	}

	public FinGlAccount(FinGlSection finGlSection) {
		this.finGlSection = finGlSection;
	}

	public FinGlAccount(FinGlSection finGlSection, String code, String name, String info, FinGlAccount parent,
			String taxCode) {
		this.finGlSection = finGlSection;
		this.code = code;
		this.name = name;
		this.info = info;
		this.parentAccount = parent;
		this.taxCode = taxCode;
	}

	@Id
	@Column(name = "code", unique = true, nullable = false, length = 4)
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "section_code", nullable = false)
	public FinGlSection getFinGlSection() {
		return this.finGlSection;
	}

	public void setFinGlSection(FinGlSection finGlSection) {
		this.finGlSection = finGlSection;
	}

	

	@Column(name = "name", length = 100)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "info")
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@Column(name = "tax_code", length = 5)
	public String getTaxCode() {
		return this.taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}


	// ********************************************************
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_account")
	public FinGlAccount getParentAccount() {
		return parentAccount;
	}

	public void setParentAccount(FinGlAccount parentAccount) {
		this.parentAccount = parentAccount;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "parentAccount")
	public Set<FinGlAccount> getChildAccounts() {
		return childAccounts;
	}

	public void setChildAccounts(Set<FinGlAccount> childAccounts) {
		this.childAccounts = childAccounts;
	}

	@Column(name = "ZAB")
	public Boolean getZab() {
		return zab;
	}

	public void setZab(Boolean zab) {
		this.zab = zab;
	}

	@Column(name = "AKT")
	public Integer getAkt() {
		return akt;
	}

	public void setAkt(Integer akt) {
		this.akt = akt;
	}

	@Column(name = "KOL")
	public Boolean getKol() {
		return kol;
	}

	public void setKol(Boolean kol) {
		this.kol = kol;
	}

	@Column(name = "VAL")
	public Boolean getVal() {
		return val;
	}

	public void setVal(Boolean val) {
		this.val = val;
	}

	@Override
	public String toString() {
		return "FinGlAccount [code=" + code + ", name=" + name + ", info=" + info + ", parentAccount=" + parentAccount
				+ ", taxCode=" + taxCode + ", finGlSection=" + finGlSection + ", zab=" + zab + ", akt=" + akt + ", kol="
				+ kol + ", val=" + val + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((akt == null) ? 0 : akt.hashCode());
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((finGlSection == null || finGlSection.getCode() == null) ? 0 : finGlSection.getCode().hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((kol == null) ? 0 : kol.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((parentAccount == null || parentAccount.getCode() == null) ? 0 : parentAccount.getCode().hashCode());
		result = prime * result + ((taxCode == null) ? 0 : taxCode.hashCode());
		result = prime * result + ((val == null) ? 0 : val.hashCode());
		result = prime * result + ((zab == null) ? 0 : zab.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinGlAccount other = (FinGlAccount) obj;
		if (akt == null) {
			if (other.akt != null)
				return false;
		} else if (!akt.equals(other.akt))
			return false;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (finGlSection == null) {
			if (other.finGlSection != null)
				return false;
		} else if (!finGlSection.equals(other.finGlSection))
			return false;
		if (info == null) {
			if (other.info != null)
				return false;
		} else if (!info.equals(other.info))
			return false;
		if (kol == null) {
			if (other.kol != null)
				return false;
		} else if (!kol.equals(other.kol))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (parentAccount == null) {
			if (other.parentAccount != null)
				return false;
		} else if (!parentAccount.equals(other.parentAccount))
			return false;
		if (taxCode == null) {
			if (other.taxCode != null)
				return false;
		} else if (!taxCode.equals(other.taxCode))
			return false;
		if (val == null) {
			if (other.val != null)
				return false;
		} else if (!val.equals(other.val))
			return false;
		if (zab == null) {
			if (other.zab != null)
				return false;
		} else if (!zab.equals(other.zab))
			return false;
		return true;
	}

	@Override
	public FinGlAccount clone() throws CloneNotSupportedException {
		return (FinGlAccount) super.clone();
	}
	
}
