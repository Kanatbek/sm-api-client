package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "contract_item")
public class ContractItem implements java.io.Serializable, Cloneable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private ConItemStatus conItemStatus;
	private Contract contract;
	private Inventory inventory;
	private PriceList priceList;
	private Unit unit;
	private String serialNumber;
	private BigDecimal price;
	private BigDecimal quantity;
	private BigDecimal cost;
	private BigDecimal discount;
	private BigDecimal summ;
	private Date writeoffDate;
	private String glCode;

	
	public ContractItem() {
	}

	public ContractItem(Long cid) {
		this.id = cid;
	}

	public ContractItem(ConItemStatus conItemStatus, Contract contract) {
		this.conItemStatus = conItemStatus;
		this.contract = contract;
	}

	public ContractItem(ConItemStatus conItemStatus, Contract contract, Inventory inventory, PriceList priceList,
			Unit unit, String serialNumber, BigDecimal price, BigDecimal quantity, BigDecimal cost, BigDecimal discount,
			BigDecimal summ, Date writeoffDate, String glCode) {
		this.conItemStatus = conItemStatus;
		this.contract = contract;
		this.inventory = inventory;
		this.priceList = priceList;
		this.unit = unit;
		this.serialNumber = serialNumber;
		this.price = price;
		this.quantity = quantity;
		this.cost = cost;
		this.discount = discount;
		this.summ = summ;
		this.writeoffDate = writeoffDate;
		this.glCode = glCode;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "item_status_id")
	public ConItemStatus getConItemStatus() {
		return this.conItemStatus;
	}

	public void setConItemStatus(ConItemStatus conItemStatus) {
		this.conItemStatus = conItemStatus;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CONTRACT_id", nullable = false)
	public Contract getContract() {
		return this.contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventory_id")
	public Inventory getInventory() {
		return this.inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "price_list_id")
	public PriceList getPriceList() {
		return this.priceList;
	}

	public void setPriceList(PriceList priceList) {
		this.priceList = priceList;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "unit")
	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Column(name = "serial_number", length = 45)
	public String getSerialNumber() {
		return this.serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	@Column(name = "price", precision = 21)
	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Column(name = "quantity")
	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	@Column(name = "cost", precision = 21)
	public BigDecimal getCost() {
		return this.cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	@Column(name = "discount", precision = 21)
	public BigDecimal getDiscount() {
		return this.discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	@Column(name = "summ", precision = 21)
	public BigDecimal getSumm() {
		return this.summ;
	}

	public void setSumm(BigDecimal summ) {
		this.summ = summ;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "writeoff_date", length = 10)
	public Date getWriteoffDate() {
		return this.writeoffDate;
	}

	public void setWriteoffDate(Date writeoffDate) {
		this.writeoffDate = writeoffDate;
	}

	@Column(name = "gl_code", length = 4)
	public String getGlCode() {
		return glCode;
	}

	public void setGlCode(String glCode) {
		this.glCode = glCode;
	}

	@Override
	public ContractItem clone() throws CloneNotSupportedException {
		return (ContractItem) super.clone();
	}
		
}
