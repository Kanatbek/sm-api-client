package com.cordialsr.domain.reducer;

import com.cordialsr.domain.SmService;

public class SmServiceReducer {
	
	public static SmService reduceMax(SmService service) {
		if (service != null) {
			service.setCareman(null);
			service.setCompany(null);
			service.setContract(null);
			service.setCustomer(null);
			service.setInventory(null);
			service.setPremiCurrecny(null);
			service.setUserAdded(null);
			service.setSmServiceItems(null);
			service.setSmEnquiry(null);
			service.setSmCalls(null);
		}
		return service;
	}
	

}
