package com.cordialsr.domain.reducer;

import com.cordialsr.domain.Payroll;

public class PayrollReducer {
	
	public static Payroll reducePayroll(Payroll p ) {
		p.setBranch(BranchReducer.reduceMax(p.getBranch()));
		if (p.getEmployee() != null) {
			p.setEmployee(EmployeeReducer.reduceEmployee(p.getEmployee(), 1));
			p.getEmployee().setParty(null);
		}
		if (p.getContract() != null) {
			p.setContract(ContractReducer.reduceMax(p.getContract()));
		}
		if (p.getConAward() != null) {
			p.setConAward(ConAwardReducer.reduceMax(p.getConAward()));
		}
		p.setParty(PartyReducer.reduceMax(p.getParty()));
		p.getParty().setCompany(null);
		return p;
	}
}
