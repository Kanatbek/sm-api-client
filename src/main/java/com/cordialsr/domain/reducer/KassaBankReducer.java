package com.cordialsr.domain.reducer;

import com.cordialsr.domain.KassaBank;

public class KassaBankReducer {

	public static KassaBank reduceMax(KassaBank kassaBank) {
		KassaBank kb = new KassaBank();
		if (kassaBank != null) {
			kb.setId(kassaBank.getId());
			kb.setName(kassaBank.getName());
		}
		return kb;
	}
	
}
