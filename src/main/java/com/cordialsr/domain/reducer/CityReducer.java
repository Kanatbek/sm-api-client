package com.cordialsr.domain.reducer;

import com.cordialsr.domain.City;

public class CityReducer {
	
	public static City reduceMax(City city) {
		City c = new City();
		if (city != null) {
			c.setId(city.getId());
			c.setName(city.getName());
		}
		return c;
	}
	
	public static City reduceCityForView(City city) {
		City c = new City();
		if (city != null) {
			try {
				c = city.clone();
				
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
		}
		return c;
	}
	
}
