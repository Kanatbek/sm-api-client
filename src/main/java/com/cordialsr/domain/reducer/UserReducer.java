package com.cordialsr.domain.reducer;

import com.cordialsr.domain.security.User;

public class UserReducer {

	public static User reduceMax(User user) {
		User u = null;
		if (user != null) {
			u = new User();
			u.setUserid(user.getUserid());
			u.setFirstName(user.getFirstName());
			u.setLastName(user.getLastName());
			u.setUsername(user.getUsername());
		}
		return u;
	}
	
}
