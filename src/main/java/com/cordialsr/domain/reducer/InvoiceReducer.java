package com.cordialsr.domain.reducer;

import java.util.ArrayList;

import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.InvoiceItem;
import com.cordialsr.domain.InvoiceStatus;
public class InvoiceReducer {
	
	public static Invoice reduceMax(Invoice inInvoice ) {
		Invoice invoice = new Invoice();
		if (inInvoice != null) {
			invoice.setId(inInvoice.getId());
			invoice.setInvoiceNumber(inInvoice.getInvoiceNumber());
		}
		return invoice;
	}
	
	public static Invoice reduceInvoiceForPosting(Invoice inInvoice ) {
		Invoice invoice = new Invoice();
		if (inInvoice != null) {
			try {
				invoice = inInvoice.clone();
				invoice.setUserAdded(null);
				invoice.setUserUpdated(null);
				invoice.setBankPartner(null);
				invoice.setBranch(null);
				invoice.setBranchImporter(null);
				invoice.setCompany(null);
				invoice.setIncoterm(null);
				invoice.setInvoiceStatus(null);
				invoice.setInvoiceTraces(null);
				invoice.setParty(null);
				for (InvoiceItem ii: invoice.getInvoiceItems()) {
					ii.setInvoice(null);
					ii.getInventory().setCompany(null);
					ii.getInventory().setInvSubCategory(null);
//					ii.getInventory().setParentInventory(null);
				}
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
		}
		return invoice;
	}

	public static ArrayList<Invoice> reduceInvoiceForReport(ArrayList<Invoice> invoices) {
		
		ArrayList<Invoice> newInvs = new ArrayList<Invoice>();
		
			for (Invoice inv: invoices) {
				if ((inv.getInvoiceType().equals(Invoice.INVOICE_TYPE_IMPORT) || inv.getInvoiceType().equals(Invoice.INVOICE_TYPE_TRANSFER)) && 
						inv.getInvoiceStatus().getName().equals(InvoiceStatus.STATUS_NEW)) {
					continue;
				}
				Invoice newInv = new Invoice();
				newInv.setCompany(inv.getCompany());
				newInv.setBranch(inv.getBranch());
				newInv.setBranchImporter(inv.getBranchImporter());
				newInv.setData(inv.getData());
				newInv.setInvoiceItems(new ArrayList<InvoiceItem>());
				for (InvoiceItem ii: inv.getInvoiceItems()) {
					InvoiceItem newII = new InvoiceItem();
					newII.setQuantity(ii.getQuantity());
					newII.setInventory(ii.getInventory());
					newInv.getInvoiceItems().add(newII);
				}
				newInvs.add(newInv);
			}
			
		return newInvs;
	}

	public static Invoice reduceForSerNumReport(Invoice inv) {		
		inv.setBankPartner(null);
		inv.setCompany(null);
		inv.setCost(null);
		inv.setCurrency(null);
//		inv.setDocId(null);
//	    inv.setDocno(null);
		inv.setIncoterm(null);
		inv.setInvoiceItems(null);
		inv.setInvoiceTraces(null);
		inv.setInvoiceStatus(null);
//		inv.setParty(null);
		inv.setUserUpdated(null);
		inv.setUserAdded(null);
		inv.setScopeType(null);
		return inv;
	}
	
}
