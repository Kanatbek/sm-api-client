package com.cordialsr.domain.reducer;

import com.cordialsr.domain.Branch;

public class BranchReducer {
	
	public static Branch reduceMax(Branch branch) {
		Branch br = null;
		if (branch != null) {
			br = new Branch();
			br.setId(branch.getId());
			br.setBranchName(branch.getBranchName());
		}
		return br;
	}
	
	public static Branch reduceBranchForView(Branch branch) {
		Branch br = new Branch();
		if (branch != null) {
			try {
				br = branch.clone();
				br.setCompany(null);
				br.setCity(CityReducer.reduceMax(br.getCity()));
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
		}
		return br;
	}
}
