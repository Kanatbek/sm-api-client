package com.cordialsr.domain.reducer;

import com.cordialsr.domain.SmCall;

public class SmCallReducer {
	
	public static SmCall reduceMax(SmCall call) {
		if (call != null) {
			call.setBranch(null);
			call.setCompany(null);
			call.setContract(null);
			call.setParty(null);
			call.setPhoneNumber(null);
			call.setService(null);
		}
		return call;
	}
	

}
