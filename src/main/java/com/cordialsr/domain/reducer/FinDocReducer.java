package com.cordialsr.domain.reducer;

import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.FinEntry;

public class FinDocReducer {
	
	public static FinDoc reduceMax(FinDoc findoc) {
		FinDoc fd = new FinDoc();
		if (findoc != null) {
			fd.setId(findoc.getId());
			fd.setDocno(findoc.getDocno());
			fd.setTitle(findoc.getTitle());
			fd.setUser(UserReducer.reduceMax(findoc.getUser()));
		}
		return fd;
	}
	
	public static FinDoc reduceFinDocForView(FinDoc fd) {
		if (fd != null) {
			fd.setParty(PartyReducer.reduceMax(fd.getParty()));
			fd.setChildFinDocs(null);
			fd.setUser(UserReducer.reduceMax(fd.getUser()));
			fd.setBranch(BranchReducer.reduceMax(fd.getBranch()));
			fd.setInvoice(InvoiceReducer.reduceMax(fd.getInvoice()));
			fd.setContract(ContractReducer.reduceMax(fd.getContract()));
			fd.setParentFindoc(reduceMax(fd.getParentFindoc()));
			for (FinEntry fe: fd.getFinEntries()) {
				fe.setCompany(null);
				fe.setBranch(BranchReducer.reduceMax(fe.getBranch()));
				fe.setDepartment(null);
				fe.setFinDoc(null);
				fe.setInvoiceItem(null);
				fe.setKassaBank(KassaBankReducer.reduceMax(fe.getKassaBank()));
			}
		}
		return fd;
	}
}
