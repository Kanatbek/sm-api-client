package com.cordialsr.domain.reducer;

import com.cordialsr.domain.FinEntry;

public class FinEntryReducer {
	
	public static FinEntry reduceMax(FinEntry finEntry) {
		FinEntry fe = new FinEntry();
		if (finEntry != null) {
			fe.setId(finEntry.getId());
			fe.setGlAccount(finEntry.getGlAccount());
			fe.setDc(finEntry.getDc());
			fe.setWsumm(finEntry.getWsumm());
			fe.setDsumm(finEntry.getDsumm());
			fe.setWcurrency(finEntry.getWcurrency());
			fe.setDcurrency(finEntry.getDcurrency());
		}
		return fe;
	}
	
	public static FinEntry reduceFeForView(FinEntry fe) {
		if (fe != null) {
			fe.setBranch(BranchReducer.reduceMax(fe.getBranch()));
			fe.setFinDoc(FinDocReducer.reduceMax(fe.getFinDoc()));
			fe.setInvoiceItem(null);
			fe.setKassaBank(KassaBankReducer.reduceMax(fe.getKassaBank()));
			fe.setParty(PartyReducer.reduceMax(fe.getParty()));
		}
		
		return fe;
	}
}
