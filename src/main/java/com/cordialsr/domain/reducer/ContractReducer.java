package com.cordialsr.domain.reducer;

import com.cordialsr.domain.Contract;
import com.cordialsr.general.GeneralUtil;

public class ContractReducer {
	
	public static Contract reduceMax(Contract con) {
		Contract c = null;
		if (con != null) {
			c = new Contract();
			c.setId(con.getId());
			c.setContractNumber(con.getContractNumber());
			c.setDateSigned(con.getDateSigned());
			c.setIsRent(con.getIsRent());
			c.setRefkey(con.getRefkey());
			c.setRegisteredUser(UserReducer.reduceMax(con.getRegisteredUser()));
		}
		return c;
	}
	
	public static Contract reduceMaxIfNotNull(Contract con) {
		Contract c = null;
		if (con != null && GeneralUtil.isEmptyLong(con.getId())) {
			c = new Contract();
			c.setId(con.getId());
			c.setContractNumber(con.getContractNumber());
			c.setRegisteredUser(UserReducer.reduceMax(con.getRegisteredUser()));
		}
		return c;
	}
	
	public static Contract reduceMin(Contract con) {
		Contract c = null;
		if (con != null) {
			c = new Contract();
			c.setId(con.getId());
			c.setContractNumber(con.getContractNumber());
			c.setCustomer(PartyReducer.reduceMax(con.getCustomer()));
			c.setRegisteredUser(UserReducer.reduceMax(con.getRegisteredUser()));
			c.setCost(con.getCost());
			c.setDiscount(con.getDiscount());
			c.setFromDealerSumm(con.getFromDealerSumm());
			c.setSumm(con.getSumm());
			c.setPaid(con.getPaid());
		}
		return c;
	}
	
	public static Contract reduceMed(Contract con) {
		Contract c = null;
		if (con != null) {
			c = new Contract();
			c.setId(con.getId());
			c.setCompany(con.getCompany());
			c.setContractNumber(con.getContractNumber());
			c.setDateSigned(con.getDateSigned());
			c.setCustomer(PartyReducer.reduceMax(con.getCustomer()));
			c.setBranch(BranchReducer.reduceMax(con.getBranch()));
			c.setCurrency(con.getCurrency());
			c.setRegisteredUser(UserReducer.reduceMax(con.getRegisteredUser()));
			c.setServiceBranch(BranchReducer.reduceMax(con.getServiceBranch()));
			c.setDealer(EmployeeReducer.reduceMin(con.getDealer()));
			c.setCollector(EmployeeReducer.reduceMin(con.getCollector()));
			c.setCareman(EmployeeReducer.reduceMin(con.getCareman()));
			c.setManager(EmployeeReducer.reduceMin(con.getManager()));
			c.setInventorySn(con.getInventorySn());
			c.setInventory(con.getInventory());
			c.setStorno(con.getStorno());
			c.setIsRent(con.getIsRent());
			c.setCost(con.getCost());
			c.setDiscount(con.getDiscount());
			c.setFromDealerSumm(con.getFromDealerSumm());
			c.setSumm(con.getSumm());
			c.setPaid(con.getPaid());
		}
		return c;
	}
	
}
