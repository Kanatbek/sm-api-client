package com.cordialsr.domain.reducer;

import com.cordialsr.domain.StockIn;

public class StockInReducer {
	
	public static StockIn reduceStockIn(StockIn sti ) {
		sti.setCompany(null);
		sti.setBranch(null);
		// sti.setFinEntry(FinEntryReducer.reduceMax(sti.getFinEntry()));
		sti.setInvoiceItem(null);
		sti.getInventory().setManufacturer(null);
		sti.getInventory().setInvMainCategory(null);
		sti.getInventory().setInvSubCategory(null);
		return sti;
	}
	
}
