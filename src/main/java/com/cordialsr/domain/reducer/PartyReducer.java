package com.cordialsr.domain.reducer;

import com.cordialsr.domain.Party;

public class PartyReducer {

	public static Party reduceMax(Party party) {
		Party p = null;
		if (party != null) {
			p = new Party();
			p.setId(party.getId());
			if (party.getFirstname() != null) {
				p.setFirstname(party.getFirstname());	
			}
			p.setLastname(party.getLastname());
			p.setMiddlename(party.getMiddlename());
			p.setCompanyName(party.getCompanyName());
			p.setIinBin(party.getIinBin());
			p.setBirthday(party.getBirthday());
			p.setPositions(party.getPositions());
			p.setActual(party.getActual());
		}
		return p;
	}
}
