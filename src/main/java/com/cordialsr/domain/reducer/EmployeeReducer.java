package com.cordialsr.domain.reducer;

import com.cordialsr.domain.Employee;

public class EmployeeReducer {

	public static Employee reduceMax(Employee e ) {
		Employee newEmpl = new Employee();
		if (e != null) {
			newEmpl.setId(e.getId());
		}
		return newEmpl;
	}
	
	public static Employee reduceEmployee(Employee e, int step ) {
		e.setBranch(BranchReducer.reduceMax(e.getBranch()));
		e.setParty(PartyReducer.reduceMax(e.getParty()));
		if (e.getAccountableTo() != null && step < 3) {
			e.setAccountableTo(reduceMax(e.getAccountableTo()));
		}
		e.setRegion(null);
		return e;
	}
	
	public static Employee reduceMin(Employee e ) {
		Employee empl = null;
		if (e != null) {
			empl = new Employee();
			empl.setBranch(BranchReducer.reduceMax(e.getBranch()));
			empl.setParty(PartyReducer.reduceMax(e.getParty()));
			empl.setContractNumber(e.getContractNumber());
			// empl.setPosition(e.getPosition());
		}
		
		return empl;
	}
	

	public static Employee reduceMed(Employee e ) {
		Employee empl = null;
		if (e != null) {
			empl = new Employee();
			empl.setId(e.getId());
			empl.setBranch(BranchReducer.reduceMax(e.getBranch()));
			empl.setParty(PartyReducer.reduceMax(e.getParty()));
			empl.setDepartment(e.getDepartment());
			empl.setDateHired(e.getDateHired());
			empl.setDateFired(e.getDateFired());
			empl.setPosition(e.getPosition());
			empl.setContractNumber(e.getContractNumber());
			
			// empl.setPosition(e.getPosition());
		}
		
		return empl;
	}
	
}
