package com.cordialsr.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "branch_type")
public class BranchType implements Serializable, Cloneable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String TYPE_DEPO = "DEPO"; 
	public static final String TYPE_HEAD = "HEAD";
	public static final String TYPE_OFFICE = "OFFICE";
	public static final String TYPE_SERVICE = "SERVICE";
	public static final String TYPE_STORE = "STORE";
	
//	DEPO	Склад
//	HEAD	Головной (центральный офис)
//	OFFICE	Филиал Офис продаж
//	SERVICE	Сервис центр
//	STORE	Магазин
	
	private String name;
	private String info;	
	
	public BranchType() {}

	public BranchType(String name, String info) {
		this.name = name;
		this.info = info;
	}
	
	@Id
	@Column(name = "name", unique = true, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	@Column(name = "info", length = 100)
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	// **************************************************************
	
	@Override
	public String toString() {
		return "BranchType [name=" + name + ", info=" + info + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BranchType other = (BranchType) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public BranchType clone() throws CloneNotSupportedException {
		return (BranchType) super.clone();
	}
	
}
