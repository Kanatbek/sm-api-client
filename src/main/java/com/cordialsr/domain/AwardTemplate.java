package com.cordialsr.domain;
// 03.08.2017 11:54:04 by stalbekr@gmail.com

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "award_template")
public class AwardTemplate implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Award award;
	private Integer payrollMonth;
	private Integer month;
	private BigDecimal summ;
	private Boolean revertOnCancel;
	private BigDecimal revertSumm;
	
	public AwardTemplate() {
	}

	public AwardTemplate(Award award) {
		this.award = award;
	}

	public AwardTemplate(Award award, Integer payrollMonth, Integer month,
						BigDecimal summ, Boolean revertOnCancel, BigDecimal revertSumm) {
		this.award = award;
		this.payrollMonth = payrollMonth;
		this.month = month;
		this.summ = summ;
		this.revertOnCancel = revertOnCancel;
		this.revertSumm = revertSumm;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "award_id", nullable = false)
	public Award getAward() {
		return this.award;
	}

	public void setAward(Award award) {
		this.award = award;
	}

	@Column(name = "month")
	public Integer getMonth() {
		return this.month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	@Column(name = "summ", precision = 16, scale = 4)
	public BigDecimal getSumm() {
		return this.summ;
	}

	public void setSumm(BigDecimal summ) {
		this.summ = summ;
	}

	@Column(name = "revert_on_cancel")
	public Boolean getRevertOnCancel() {
		return revertOnCancel;
	}

	public void setRevertOnCancel(Boolean revertOnCancel) {
		this.revertOnCancel = revertOnCancel;
	}

	@Column(name = "payroll_month")
	public Integer getPayrollMonth() {
		return payrollMonth;
	}

	public void setPayrollMonth(Integer payrollMonth) {
		this.payrollMonth = payrollMonth;
	}

	@Column(name = "revert_summ", precision = 16, scale = 4)
	public BigDecimal getRevertSumm() {
		return revertSumm;
	}

	public void setRevertSumm(BigDecimal revertSumm) {
		this.revertSumm = revertSumm;
	}	
	
	
	
}
