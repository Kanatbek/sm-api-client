package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cordialsr.domain.deserializer.PriceListDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@JsonDeserialize(using = PriceListDeserializer.class)
@Table(name = "price_list")
public class PriceList implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Branch branch;
	private City city;
	private Company company;
	private ContractType contractType;
	private Country country;
	private Inventory inventory;
	private Region region;
	private Scope scope;
	private Date data;
	private BigDecimal price;
	private Currency currency;
	private BigDecimal firstPayment;
	private Integer month;
	private BigDecimal dealerDeposit;
	private Boolean active;
	private Date dateClosed;
	private Boolean isRent;
	private Boolean isSell;
	private Boolean forYur;
	private Boolean forFiz;
	
	private List<PaymentTemplate> paymentTemplates = new ArrayList<>();

	public PriceList() {
	}
	public PriceList(Integer id) {
		this.id = id;
	}
	public PriceList(ContractType contractType, Inventory inventory) {
		this.contractType = contractType;
		this.inventory = inventory;
	}

	public PriceList(Integer id, Branch branch, City city, Company company, ContractType contractType, Country country,
			Inventory inventory, Region region, Scope scope, Date data, BigDecimal price, Currency currency,
			Integer month, BigDecimal dealerDeposit, BigDecimal firstPayment,  Boolean active,
			 Date dateClosed, List<PaymentTemplate> paymentTemplates, Boolean isRent, Boolean isSell, Boolean forYur, Boolean forFiz) {
		this.id = id;
		this.branch = branch;
		this.city = city;
		this.company = company;
		this.contractType = contractType;
		this.country = country;
		this.inventory = inventory;
		this.region = region;
		this.scope = scope;
		this.data = data;
		this.price = price;
		this.currency = currency;
		this.month = month;
		this.dealerDeposit = dealerDeposit;
		this.firstPayment = firstPayment;
		this.active = active;
		this.dateClosed = dateClosed;
		this.paymentTemplates = paymentTemplates;
		this.isRent = isRent;
		this.isSell = isSell;
		this.forFiz = forFiz;
		this.forYur = forYur;
	}
	
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch")
	public Branch getBranch() {
		return this.branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "city")
	public City getCity() {
		return this.city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company")
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contract_type_id")
	public ContractType getContractType() {
		return this.contractType;
	}

	public void setContractType(ContractType contractType) {
		this.contractType = contractType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "country")
	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventory_id", nullable = false)
	public Inventory getInventory() {
		return this.inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "region")
	public Region getRegion() {
		return this.region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "scope")
	public Scope getScope() {
		return this.scope;
	}

	public void setScope(Scope scope) {
		this.scope = scope;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data", length = 10)
	public Date getData() {
		return this.data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	@Column(name = "price", precision = 21, scale = 2)
	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currency")
	public Currency getCurrency() {
		return this.currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	@Column(name = "month")
	public Integer getMonth() {
		return this.month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	@Column(name = "dealer_deposit", precision = 21, scale = 2)
	public BigDecimal getDealerDeposit() {
		return this.dealerDeposit;
	}

	public void setDealerDeposit(BigDecimal dealerDeposit) {
		this.dealerDeposit = dealerDeposit;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "priceList", cascade = CascadeType.ALL)
	public List<PaymentTemplate> getPaymentTemplates() {
		return this.paymentTemplates;
	}

	public void setPaymentTemplates(List<PaymentTemplate> paymentTemplates) {
		this.paymentTemplates = paymentTemplates;
	}

	@Column(name = "first_payment", precision = 21, scale = 2)
	public BigDecimal getFirstPayment() {
		return firstPayment;
	}

	public void setFirstPayment(BigDecimal firstPayment) {
		this.firstPayment = firstPayment;
	}

	@Column(name = "active")
	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
	
	@Column(name = "is_rent")
	public Boolean getIsRent() {
		return isRent;
	}
	public void setIsRent(Boolean isRent) {
		this.isRent = isRent;
	}
	
	@Column(name = "is_sell")
	public Boolean getIsSell() {
		return isSell;
	}
	public void setIsSell(Boolean isSell) {
		this.isSell = isSell;
	}
	
	@Column(name = "for_yur")
	public Boolean getForYur() {
		return forYur;
	}
	public void setForYur(Boolean forYur) {
		this.forYur = forYur;
	}
	
	@Column(name = "for_fiz")
	public Boolean getForFiz() {
		return forFiz;
	}
	public void setForFiz(Boolean forFiz) {
		this.forFiz = forFiz;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "date_closed", length = 10)
	public Date getDateClosed() {
		return dateClosed;
	}

	public void setDateClosed(Date dateClosed) {
		this.dateClosed = dateClosed;
	}

	//===================================================================
	
	@Override
	public String toString() {
		return "PriceList [id=" + id + ", branch=" + branch + ", city=" + city + ", company=" + company
				+ ", contractType = " + contractType + " , country=" + country + ",  inventory = " + inventory 
						+ ",region=" + region + ", scope=" + scope + ", date = " + data + ", price = " + 
						price + " , currency = " + currency + ", month = " + month + ", dealerDeposit = " + dealerDeposit
						+ ", active = " + active + ", dateClosed = " + dateClosed + ", isRent=" + isRent + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((branch == null || branch.getId() == null) ? 0 : branch.getId().hashCode());
		result = prime * result + ((city == null || city.getId() == null) ? 0 : city.getId().hashCode());
		result = prime * result + ((company == null || company.getId() == null) ? 0 : company.getId().hashCode());
		result = prime * result + ((contractType == null || contractType.getId() == null) ? 0 : contractType.getId().hashCode());
		result = prime * result + ((country == null || country.getId() == null) ? 0 : country.getId().hashCode());
		result = prime * result + ((inventory == null || inventory.getId() == null) ? 0 : inventory.getId().hashCode());
		result = prime * result + ((region == null || region.getId() == null) ? 0 : region.getId().hashCode());
		result = prime * result + ((scope == null || scope.getName() == null) ? 0 : scope.getName().hashCode());
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((currency == null || currency.getCurrency() == null) ? 0 : currency.getCurrency().hashCode());
		result = prime * result + ((month == null) ? 0 : month.hashCode());
		result = prime * result + ((dealerDeposit == null) ? 0 : dealerDeposit.hashCode());
		result = prime * result + ((active == null) ? 0 : active.hashCode());
		result = prime * result + ((dateClosed == null) ? 0 : dateClosed.hashCode());
		return result;
	}
	
	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		PriceList other = (PriceList) obj;
		
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (branch == null) {
			if (other.branch != null)
				return false;
		} else if (!branch.equals(other.branch))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		if (contractType == null) {
			if (other.contractType != null)
				return false;
		} else if (!contractType.equals(other.contractType))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (inventory == null) {
			if (other.inventory != null)
				return false;
		} else if (!inventory.equals(other.inventory))
			return false;
		if (region == null) {
			if (other.region != null)
				return false;
		} else if (!region.equals(other.region))
			return false;
		if (scope == null) {
			if (other.scope != null)
				return false;
		} else if (!scope.equals(other.scope))
			return false;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (firstPayment == null) {
			if (other.firstPayment != null)
				return false;
		} else if (!firstPayment.equals(other.firstPayment))
			return false;
		if (month == null) {
			if (other.month != null)
				return false;
		} else if (!month.equals(other.month))
			return false;
		if (dealerDeposit == null) {
			if (other.dealerDeposit != null)
				return false;
		} else if (!dealerDeposit.equals(other.dealerDeposit))
			return false;
		if (active == null) {
			if (other.active != null)
				return false;
		} else if (!active.equals(other.active))
			return false;
		if (isRent == null) {
			if (other.isRent != null)
				return false;
		} else if (!isRent.equals(other.isRent))
			return false;
		if (dateClosed == null) {
			if (other.dateClosed != null)
				return false;
		} else if (!dateClosed.equals(other.dateClosed))
			return false;
		return true;
		
	}
	@Override
	public PriceList clone() throws CloneNotSupportedException {
		return (PriceList) super.clone();
	}

		
}
