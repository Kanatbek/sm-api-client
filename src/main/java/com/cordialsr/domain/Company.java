package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "company")
public class Company implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final int COMPANY_CORDIAL_SERVICE = 1;
	public static final int COMPANY_AUTOGAS = 2;
	public static final int COMPANY_SLEEP_SYSTEM = 3;
	public static final int COMPANY_CONSTRUCTION = 4;
	
//	1	Cordial Service	USD	1
//	2	AutogasWorld	USD	2
//	3	Cordial Sleep Systems	USD	3
//	4	Cordial Construction 	USD	4
	
	private Integer id;
	private Currency currency;
	private String name;
	private Party party;
	
	public Company() {
	}
	
	public Company(int id) {
		this.id = id;
	}
	
	public Company(Currency currency, String name) {
		this.currency = currency;
		this.name = name;
	}
	
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "main_currency", nullable = false)
	public Currency getCurrency() {
		return this.currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party", nullable = false)
	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}

	@Column(name = "name", nullable = false, length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
 
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currency == null || currency.getCurrency() == null) ? 0 : currency.getCurrency().hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Company other = (Company) obj;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.getCurrency().equals(other.currency.getCurrency()))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public Company clone() throws CloneNotSupportedException {
		return (Company) super.clone();
	}

	
}
