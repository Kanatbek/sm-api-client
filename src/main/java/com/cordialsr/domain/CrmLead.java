package com.cordialsr.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cordialsr.domain.deserializer.CrmLeadDeserializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
 @JsonDeserialize(using = CrmLeadDeserializer.class)
@Table(name = "crm_lead")
public class CrmLead implements Serializable, Cloneable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Branch branch;
	private Company company;
	private Party party;
	private Employee dealer;
	private CrmLead adviser;
	private String firstname;
	private String lastname;
	private String middlename;
	private String email;
	private String info;
	private String source;
	private Date cpuDate;
	private PhoneNumber phone;

	private Address address;
	private List<PhoneNumber> phoneNumbers = new ArrayList<>();
	private List<CrmLead> childLeads = new ArrayList<>();
	
	public CrmLead() {
	}
	
	public CrmLead(Long id) {
		this.id = id;
	}
	
	public CrmLead(Long id, Employee dealer, CrmLead adviser, String firstname, String lastname, String middlename,
			String email, String info, String source, Date cpuDate, Address address, List<PhoneNumber> phoneNumbers, List<CrmLead> childLeads, 
			Party party, Company company, Branch branch, PhoneNumber phone) {
		this.id = id;
		this.dealer = dealer;
		this.adviser = adviser;
		this.firstname = firstname;
		this.lastname = lastname;
		this.middlename = middlename;
		this.email = email;
		this.info = info;
		this.source = source;
		this.cpuDate = cpuDate;
		this.address = address;
		this.phoneNumbers = phoneNumbers;
		this.childLeads = childLeads;
		this.party = party;
		this.branch = branch;
		this.company = company;
		this.phone = phone;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "dealer")
	public Employee getDealer() {
		return this.dealer;
	}

	public void setDealer(Employee dealer) {
		this.dealer = dealer;
	}
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "adviser")
	public CrmLead getAdviser() {
		return this.adviser;
	}

	public void setAdviser(CrmLead adviser) {
		this.adviser = adviser;
	}

	@Column(name = "firstname")
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	@Column(name = "lastname")
	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@Column(name = "middlename")
	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "info")
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@Column(name = "source")
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "cpu_date", length = 10)
	public Date getCpuDate() {
		return cpuDate;
	}

	public void setCpuDate(Date cpuDate) {
		this.cpuDate = cpuDate;
	}
	
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "lead", cascade = CascadeType.ALL, orphanRemoval = true)
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "lead", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<PhoneNumber> getPhoneNumbers() {
		return phoneNumbers;
	}

	public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
		this.phoneNumbers = phoneNumbers;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "adviser", cascade = CascadeType.ALL)
	public List<CrmLead> getChildLeads() {
		return this.childLeads;
	}

	public void setChildLeads(List<CrmLead> childLeads) {
		this.childLeads = childLeads;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party")
	public Party getParty() {
		return this.party;
	}

	public void setParty(Party party) {
		this.party = party;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company")
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch")
	public Branch getBranch() {
		return this.branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "phone")
	public PhoneNumber getPhone() {
		return phone;
	}

	public void setPhone(PhoneNumber phone) {
		this.phone = phone;
	}
	

	@Override
	public CrmLead clone() throws CloneNotSupportedException {
		return (CrmLead) super.clone();
	}
	
}
