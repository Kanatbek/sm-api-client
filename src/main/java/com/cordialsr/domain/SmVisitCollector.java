package com.cordialsr.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "smVisitCollector")
public class SmVisitCollector {
	private Long id;
	private Date inTime;
	private Date outTime;
	private Date inDate;
	private BigDecimal longitude;
	private BigDecimal latitude;
	private Employee employee;
	private Party party;
	private String info;
	private Contract contract;
	private BigDecimal summ;
	private BigDecimal toCashBox;
	private Currency currency;
	
	public SmVisitCollector() {
		
	}

	public SmVisitCollector(Long id) {
		this.id = id;
	}
	
	public SmVisitCollector(Long id, Date inTime, Date outTime, Date inDate, BigDecimal longitude, BigDecimal latitude,
			BigDecimal summ, BigDecimal toCashBox, Employee employee, Party party, Contract contract, Currency currency, String info) {
		this.id = id;
		this.inTime = inTime;
		this.outTime = outTime;
		this.inDate = inDate;
		this.longitude = longitude;
		this.latitude = latitude;
		this.summ = summ;
		this.toCashBox = toCashBox;
		this.employee = employee;
		this.party = party;
		this.contract = contract;
		this.currency = currency;
		this.info = info;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "in_time", length = 10)
	public Date getInTime() {
		return inTime;
	}

	public void setInTime(Date inTime) {
		this.inTime = inTime;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "out_time", length = 10)
	public Date getOutTime() {
		return outTime;
	}

	public void setOutTime(Date outTime) {
		this.outTime = outTime;
	}
	
	@Column(name = "info")
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "in_date", length = 10)
	public Date getInDate() {
		return inDate;
	}

	public void setInDate(Date inDate) {
		this.inDate = inDate;
	}

	@Column(name = "longitude")
	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currency", nullable = false)
	public Currency getCurrency() {
		return currency;
	}
	
	public void setCurrency(Currency currency) {
		this.currency  = currency;
	}
	
	@Column(name = "summ")
	public BigDecimal getSumm() {
		return summ;
	}
	
	public void setSumm(BigDecimal summ) {
		this.summ = summ;
	}
	
	@Column(name = "to_cashbox")
	public BigDecimal getToCashBox() {
		return toCashBox;
	}
	
	public void setToCashBox(BigDecimal toCashBox) {
		this.toCashBox = toCashBox;
	}
	
	@Column(name = "latitude")
	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empl_id")
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party_id")
	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contract_id")
	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}
	
	
	@Override
	public SmVisitCollector clone() throws CloneNotSupportedException {
		return (SmVisitCollector) super.clone();
	}
}
