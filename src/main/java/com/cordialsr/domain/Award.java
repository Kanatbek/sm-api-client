package com.cordialsr.domain;
// 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.cordialsr.domain.deserializer.AwardDeserializer;
import com.cordialsr.domain.reducer.UserReducer;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@JsonDeserialize(using = AwardDeserializer.class)
@Table(name = "award")
public class Award implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private AwardCase awardCase;
	private AwardType awardType;
	private Branch branch;
	private City city;
	private Company company;
	private Country country;
	private Currency currency;
	private InvMainCategory invMainCategory;
	private InvSubCategory invSubCategory;
	private Inventory inventory;
	private Position position;
	private Region region;
	private Scope scope;
	private Date dateStart;
	private Date dateEnd;
	private BigDecimal min;
	private BigDecimal max;
	private BigDecimal coefficient;
	private BigDecimal summ;
	private Integer premiDiv;
	private Date dateCreated;
	private User userCreated;
	private Boolean forRent;
	private Boolean forSale;
	private Department department;
	private Boolean enabled;

	private List<AwardTemplate> awardTemplates = new ArrayList<>();

	public Award() {
	}

	public Award(AwardCase awardCase, AwardType awardType, Currency currency, Position position, Scope scope, Date dateStart, Date dateEnd,
			BigDecimal summ) {
		this.awardCase = awardCase;
		this.awardType = awardType;
		this.currency = currency;
		this.position = position;
		this.scope = scope;
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
		this.summ = summ;
	}

	public Award(Integer id, AwardCase awardCase, AwardType awardType, Branch branch, City city, Company company, Country country,
			Currency currency, InvMainCategory invMainCategory, InvSubCategory invSubCategory, Inventory inventory,
			Position position, Region region, Scope scope, Date dateStart, Date dateEnd, BigDecimal min, BigDecimal max, BigDecimal coefficient,
			BigDecimal summ, Integer premiDiv, List<AwardTemplate> awardTemplates, User userCreated, Boolean forRent,
			Boolean forSale, Department department, Boolean enabled) {
		this.id = id;
		this.awardCase = awardCase;
		this.awardType = awardType;
		this.branch = branch;
		this.city = city;
		this.company = company;
		this.country = country;
		this.currency = currency;
		this.invMainCategory = invMainCategory;
		this.invSubCategory = invSubCategory;
		this.inventory = inventory;
		this.position = position;
		this.region = region;
		this.scope = scope;
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
		this.min = min;
		this.max = max;
		this.coefficient = coefficient;
		this.summ = summ;
		this.premiDiv = premiDiv;
		this.awardTemplates = awardTemplates;
		this.userCreated = userCreated;
		this.forRent = forRent;
		this.forSale = forSale;
		this.department = department;
		this.enabled = enabled;
	}

	public Award(Integer id) {
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "award_case", nullable = false)
	public AwardCase getAwardCase() {
		return this.awardCase;
	}

	public void setAwardCase(AwardCase awardCase) {
		this.awardCase = awardCase;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "award_type", nullable = false)
	public AwardType getAwardType() {
		return this.awardType;
	}

	public void setAwardType(AwardType awardType) {
		this.awardType = awardType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch")
	public Branch getBranch() {
		return this.branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "city")
	public City getCity() {
		return this.city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company")
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "country")
	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currency", nullable = false)
	public Currency getCurrency() {
		return this.currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inv_main_category")
	public InvMainCategory getInvMainCategory() {
		return this.invMainCategory;
	}

	public void setInvMainCategory(InvMainCategory invMainCategory) {
		this.invMainCategory = invMainCategory;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inv_sub_category")
	public InvSubCategory getInvSubCategory() {
		return this.invSubCategory;
	}

	public void setInvSubCategory(InvSubCategory invSubCategory) {
		this.invSubCategory = invSubCategory;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventory_id")
	public Inventory getInventory() {
		return this.inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "position", nullable = false)
	public Position getPosition() {
		return this.position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "region")
	public Region getRegion() {
		return this.region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "scope", nullable = false)
	public Scope getScope() {
		return this.scope;
	}

	public void setScope(Scope scope) {
		this.scope = scope;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "date_start", nullable = false, length = 10)
	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "date_end", length = 10)
	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}
	
	@Column(name = "min")
	public BigDecimal getMin() {
		return this.min;
	}

	public void setMin(BigDecimal min) {
		this.min = min;
	}

	@Column(name = "max")
	public BigDecimal getMax() {
		return this.max;
	}

	public void setMax(BigDecimal max) {
		this.max = max;
	}

	@Column(name = "coefficient", precision = 21, scale = 12)
	public BigDecimal getCoefficient() {
		return this.coefficient;
	}

	public void setCoefficient(BigDecimal coefficient) {
		this.coefficient = coefficient;
	}

	@Column(name = "summ", precision = 21, scale = 2)
	public BigDecimal getSumm() {
		return this.summ;
	}

	public void setSumm(BigDecimal summ) {
		this.summ = summ;
	}

	@Column(name = "premi_div")
	public Integer getPremiDiv() {
		return this.premiDiv;
	}

	public void setPremiDiv(Integer premiDiv) {
		this.premiDiv = premiDiv;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "award", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<AwardTemplate> getAwardTemplates() {
		return this.awardTemplates;
	}

	public void setAwardTemplates(List<AwardTemplate> awardTemplates) {
		this.awardTemplates = awardTemplates;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "date_created", length = 10)
	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_created")
	public User getUserCreated() {
		return UserReducer.reduceMax(userCreated);
	}

	public void setUserCreated(User userCreated) {
		this.userCreated = userCreated;
	}

	@Column(name = "for_rent")
	public Boolean getForRent() {
		return forRent;
	}

	public void setForRent(Boolean forRent) {
		this.forRent = forRent;
	}
	
	@Column(name = "for_sale")
	public Boolean getForSale() {
		return forSale;
	}

	public void setForSale(Boolean forSale) {
		this.forSale = forSale;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "department")
	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	@Column(name = "enabled")
	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	@Transient
	public String getUserName() {
		String userName = "";
		if (this.userCreated != null) {
			userName = (this.userCreated != null) ? this.userCreated.getFirstName() + " " + this.userCreated.getLastName() : "";
			if (userName.length() <= 1) {
				userName = (this.userCreated != null) ? this.userCreated.getUsername() : ""; 
			}
		}
		return userName;
	}
	
	@Override
	public Award clone() throws CloneNotSupportedException {
		return (Award) super.clone();
	}
}
