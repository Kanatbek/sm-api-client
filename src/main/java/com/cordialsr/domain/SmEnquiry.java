package com.cordialsr.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cordialsr.domain.deserializer.SmEnquiryDeserializer;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "sm_enquiry")
@JsonDeserialize(using = SmEnquiryDeserializer.class)
public class SmEnquiry implements java.io.Serializable, Cloneable {
	
	public static final int STATUS_NEW = 1;;
	public static final int STATUS_CANCEL = 2;
	public static final int STATUS_DONE = 3;

	public static final int TYPE_DEMONTAZH= 1;;
	public static final int TYPE_SERVICE = 2;
	public static final int TYPE_SALE = 3;
	
	private Long id;
	private Company company;
	private Branch branch;
	private Contract contract;
	private Party party;
	private Date edate;
	private Date etime;
	private User operator;
	private String info;
	private String fullFio;
	private Integer status;
	private Integer etype;
	private String phone;
	private String enquiryNumber;
	private SmService smService;
	
	public SmEnquiry(Long id, Company company, Branch branch, Contract contract, Party party, 
			Date edate, Date etime, User operator, String info, String fullFio, Integer status,
			Integer etype, String phone, String enquiryNumber, SmService smService) {
		super();
		this.id = id;
		this.company = company;
		this.branch = branch;
		this.contract = contract;
		this.party = party;
		this.edate = edate;
		this.etime = etime;
		this.operator = operator;
		this.info = info;
		this.fullFio = fullFio;
		this.status = status;
		this.etype = etype;
		this.phone = phone;
		this.enquiryNumber = enquiryNumber;
		this.smService = smService;
	}
	
	public SmEnquiry(Long id) {
		this.id = id;
	}
	
	public SmEnquiry() {}

	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company", nullable = false)
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch", nullable = false)
	public Branch getBranch() {
		return branch;
	}
	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contract", nullable = false)
	public Contract getContract() {
		return contract;
	}
	public void setContract(Contract contract) {
		this.contract = contract;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party", nullable = false)
	public Party getParty() {
		return party;
	}
	public void setParty(Party party) {
		this.party = party;
	}
	
	
	@Temporal(TemporalType.DATE)
	@Column(name = "edate", nullable = false, length = 10)
	public Date getEdate() {
		return edate;
	}
	public void setEdate(Date edate) {
		this.edate = edate;
	}
	
	@Temporal(TemporalType.TIME)
	@Column(name = "etime", nullable = false, length = 10)
	public Date getEtime() {
		return etime;
	}
	public void setEtime(Date etime) {
		this.etime = etime;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
	public User getOperator() {
		return operator;
	}
	public void setOperator(User operator) {
		this.operator = operator;
	}
	
	@Column(name = "info")
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	
	@Column(name = "full_fio")
	public String getFullFio() {
		return fullFio;
	}

	public void setFullFio(String fullFio) {
		this.fullFio = fullFio;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	@Column(name = "etype")
	public Integer getEtype() {
		return etype;
	}
	public void setEtype(Integer etype) {
		this.etype = etype;
	}
	
	@Column(name = "phone")
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "enquiryNumber")
	public String getEnquiryNumber() {
		return enquiryNumber;
	}

	public void setEnquiryNumber(String enquiryNumber) {
		this.enquiryNumber = enquiryNumber;
	}
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "service_id", nullable = false)
	public SmService getSmService() {
		return smService;
	}

	public void setSmService(SmService smService) {
		this.smService = smService;
	}

	@Override
	public String toString() {
		return "SmEnquiry [id=" + id + ", company=" + company + ", branch=" + branch + ", contract=" + contract
				+ ", party=" + party + ", edate=" + edate + ", etime=" + etime + ", operator=" + operator 
				+ ", info=" + info + ", fullFio=" + fullFio
				+ ", status=" + status + ", etype=" + etype + ", phone=" + phone
				+ ", enquiryNumber=" + enquiryNumber + ", smService=" + smService + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((branch == null || branch.getId() == null) ? 0 : branch.getId().hashCode());
		result = prime * result + ((company == null || company.getId() == null) ? 0 : company.getId().hashCode());
		result = prime * result + ((contract == null || contract.getId() == null) ? 0 : contract.getId().hashCode());
		result = prime * result + ((edate == null) ? 0 : edate.hashCode());
		result = prime * result + ((enquiryNumber == null) ? 0 : enquiryNumber.hashCode());
		result = prime * result + ((etime == null) ? 0 : etime.hashCode());
		result = prime * result + ((etype == null) ? 0 : etype.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((operator == null || operator.getUserid() == null) ? 0 : operator.getUserid().hashCode());
		result = prime * result + ((party == null || party.getId() == null) ? 0 : party.getId().hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((fullFio == null) ? 0 : fullFio.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((smService == null || smService.getId() == null) ? 0 : smService.getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SmEnquiry other = (SmEnquiry) obj;
		if (branch == null) {
			if (other.branch != null)
				return false;
		} else if (!branch.equals(other.branch))
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		if (contract == null) {
			if (other.contract != null)
				return false;
		} else if (!contract.equals(other.contract))
			return false;
		if (edate == null) {
			if (other.edate != null)
				return false;
		} else if (!edate.equals(other.edate))
			return false;
		if (enquiryNumber == null) {
			if (other.enquiryNumber != null)
				return false;
		} else if (!enquiryNumber.equals(other.enquiryNumber))
			return false;
		if (etime == null) {
			if (other.etime != null)
				return false;
		} else if (!etime.equals(other.etime))
			return false;
		if (etype == null) {
			if (other.etype != null)
				return false;
		} else if (!etype.equals(other.etype))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (info == null) {
			if (other.info != null)
				return false;
		} else if (!info.equals(other.info))
			return false;
		if (operator == null) {
			if (other.operator != null)
				return false;
		} else if (!operator.equals(other.operator))
			return false;
		if (party == null) {
			if (other.party != null)
				return false;
		} else if (!party.equals(other.party))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (fullFio == null) {
			if (other.fullFio != null)
				return false;
		} else if (!fullFio.equals(other.fullFio))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (smService == null) {
			if (other.smService != null)
				return false;
		} else if (!smService.equals(other.smService))
			return false;
		return true;
	}
	
	@Override
	public SmEnquiry clone() throws CloneNotSupportedException {
		return (SmEnquiry) super.clone();
	}
	
}
