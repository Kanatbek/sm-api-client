package com.cordialsr.domain.projection;
import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractStatus;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "micro", types = { Contract.class })
public interface ContractForCountProjection {
	Integer getId();
	String getRefkey();
	BranchMiniProjection getBranch();
	ContractStatusProjection getContractStatus();
	Date getDateSigned();

}
