package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.KassaBank;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "list", types = { KassaBank.class })
public interface KassaBankProjection {
	Integer getId();
	Company getCompany();
	Branch getBranch();
	String getName();
	Boolean getIsBank();
	PartyMiniProjection getBank();
	String getAccountNumber();
	Currency getCurrency();
}
