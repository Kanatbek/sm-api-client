package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Currency;
import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.FinDocType;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "list", types = { FinDoc.class })
public interface FinDocProjectionList {
	Integer getId();
	String getTitle();
	String getDocno();
	String getYear();
	String getRefkey();
	String getReftyp();
	Integer getMonth();
	FinDocType getFinDocType();
	Date getCpuDate();
	Date getDocDate();
	Boolean getStorno();
	String getStDocno();
	String getStYear();
	UserMiniProjection getUser();
	Integer getPaymentOrder();
	BigDecimal getDsumm();
	Currency getDcurrency();
	BigDecimal getRate();
	BigDecimal getWsumm();
	Currency getWcurrency();
	Boolean getForbuh();
	String getTrcode();
	Date getUpdatedDate();
	String getInfo();
	Date getCpuTime();
	BranchMiniProjection getBranch();
	// Company getCompany();
	// Department getDepartment();
	PartyMiniProjection getParty();
	// Invoice getInvoice();
	// String getRefkeyMain();
	// Invoice getInvoice();
	// String getRefkeyMain();
	 BigDecimal getDsummPaid();
	 BigDecimal getWsummPaid();
	// Boolean getClosed();		
}
