package com.cordialsr.domain.projection;

import java.util.Date;
import java.util.Set;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Country;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.PhoneNumber;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "list", types = { Party.class })
public interface PartyProjectionList {
	PartyTypeInlineProjection getPartyType();
	CompanyMiniProjection getCompany();
	Integer getId();
	String getIinBin();
	String getFirstname();
	String getLastname();
	String getMiddlename();
	String getEmail();
	Boolean getIsYur();
	String getPassportNumber();
	CountryProjection getResidency();
	Date getDateIssue();
	Date getDateExpire();
	String getIssuedInstance();
	String getCompanyName();
	String getIik();
	String getBik();
	String getFullFIO();
	Date getBirthday();
	
	String getPositions();
	Boolean getActual();

	Set<PhoneNumber> getPhoneNumbers();
	Set<AddressInlineProjection> getAddresses();
}	