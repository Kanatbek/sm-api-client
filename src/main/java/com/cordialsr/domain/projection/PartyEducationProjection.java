package com.cordialsr.domain.projection;

import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.EducationLevel;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.PartyEducation;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "party_education", types = { PartyEducation.class })
public interface PartyEducationProjection {
	Long getId();
	PartyMiniProjection getParty();
	EducationLevel getLevel();
	String getAcademy();
	String getFaculty();
	String getSpecialization();
	Date getDateStart();
	Date getDateEnd();
	Boolean getIsAcademic();
}
