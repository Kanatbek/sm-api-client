package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.ServiceCategory;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "serviceCategory", types = { ServiceCategory.class })
public interface ServiceCategoryProjection {
	Integer getId();
	String getName();
	String getInfo();

}
