package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Employee;
import com.cordialsr.domain.Position;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "cut", types = { Employee.class })
public interface EmployeeCutProjection {
	Integer getId();
	PartyMiniProjection getParty();
//	Company getCompany();
	BranchProjection getBranch();
//	Department getDepartment();
	Position getPosition();
	// BigDecimal getSalary();
	// Currency getCurrency();
//	Date getDateHired();
//	Date getDateFired();
}
