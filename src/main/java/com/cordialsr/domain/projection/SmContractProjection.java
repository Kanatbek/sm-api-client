package com.cordialsr.domain.projection;

import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.SmBranch;
import com.cordialsr.domain.SmContract;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "full", types = { SmContract.class })
public interface SmContractProjection {
	Integer getId();
	SmBranch getSmBranch();
	Date getCdate();
	String getContractNumber();
	String getCustomerFio();
	String getIinbin();
	String getDealer();
	String getPhone();
	String getCareman();
	String getModel();
	String getSn();
	Boolean getStorno();
	String getAddrReg();
	String getAddrFact();
	Boolean getResigned();	
	Boolean getType();
	String getInfo();
	User getUpdatedUser();
	Date getUpdatedDate();
	InventoryInlineProjection getInventory();
}
