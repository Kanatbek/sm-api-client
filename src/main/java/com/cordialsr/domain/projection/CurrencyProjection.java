package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;
import com.cordialsr.domain.Currency;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "currency", types = { Currency.class })
public interface CurrencyProjection {
	String getCurrency();
	String getName();
}
