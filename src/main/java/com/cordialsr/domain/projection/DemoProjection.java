package com.cordialsr.domain.projection;

import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.CrmDemo;
import com.cordialsr.domain.CrmLead;
import com.cordialsr.domain.CrmReason;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "demo", types = { CrmDemo.class })
public interface DemoProjection {
	Integer getId();
	EmployeeMiniProjection getDealer();
	LeadProjection getLead();
	Date getDemoDate();
	Date getDemoTime();
	Date getCpuDate();
	Date getCpuTime();
	User getUser();
	String getStatus();
	String getInfo();
	Long getTransport();
	Currency getCurrency();
	CrmReason getReason();
	PartyProjection getParty();
	CompanyInlineProjection getCompany();
	BranchProjection getBranch();
	
}
