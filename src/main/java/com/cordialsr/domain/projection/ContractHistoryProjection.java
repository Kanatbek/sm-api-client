package com.cordialsr.domain.projection;

import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.ContractHistory;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "contractHistory", types = { ContractHistory.class })
public interface ContractHistoryProjection {
	
	 Integer getId();
	 ContractMiniProjection getContract();
	 String getField();
	 String getTitle();
	 Date getUpdatedDate();
	 UserMiniProjection getUpdatedUser();
	 String getOldValue();
	 String getNewValue();
	 Long getOldId();
	 Long getNewId();
	 
}
