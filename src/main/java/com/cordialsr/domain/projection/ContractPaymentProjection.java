package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Contract;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "contractPayment", types = { Contract.class })
public interface ContractPaymentProjection {
	Integer getId();
	BranchProjection getBranch();
	CompanyMiniProjection getCompany();
	PartyProjectionList getCustomer();
	
	
	Date getDateSigned();
	String getContractNumber();
	BigDecimal getSumm();
	CurrencyProjection getCurrency();
	BigDecimal getPaid();
	
	List<ContractPaymentScheduleProjection> getPaymentSchedules();
	
}
