package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.ConItemStatus;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "conItemStatus", types = { ConItemStatus.class })
public interface ConItemStatusProjection {
	 Integer getId();
	 String getName();
	 String getInfo();
}
