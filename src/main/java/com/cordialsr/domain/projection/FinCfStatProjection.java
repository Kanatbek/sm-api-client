package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.FinActivityType;
import com.cordialsr.domain.FinCashflowStatement;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "cfstat", types = { FinCashflowStatement.class})
public interface FinCfStatProjection {
	Integer getId();
	FinActivityType getFinActType();
	String getName();
	String getInfo();
}
