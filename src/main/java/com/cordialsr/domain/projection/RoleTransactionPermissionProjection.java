package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.security.RoleTransactionPermission;
import com.cordialsr.domain.security.Transaction;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "rtPermission", types = { RoleTransactionPermission.class })
public interface RoleTransactionPermissionProjection {
	 Integer getId();
	 Transaction getTransaction();
	 Integer getPermission();
}
