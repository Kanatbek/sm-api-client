package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Company;
import com.cordialsr.domain.Currency;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "mini", types = { Company.class })
public interface CompanyMiniProjection {
	Integer getId();
	Currency getCurrency();
	String getName();
}
