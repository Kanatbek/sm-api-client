package com.cordialsr.domain.projection;

import java.util.Set;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Department;
import com.cordialsr.domain.Region;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "region", types = { Region.class })
public interface RegionProjection {
	Integer getId();
	Company getCompany();
	Department getDepartment();
	String getName();
	
	Set<Branch> getBranches();
}
