package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Department;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.Position;
import com.cordialsr.domain.Region;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "Employee", types = { Employee.class })
public interface EmployeeProjection {

	Integer getId();
	PartyProjection getParty();
	Company getCompany();
	Region getRegion();
	Branch getBranch();
	Department getDepartment();
	PositionProjection getPosition();
	BigDecimal getSalary();
	BigDecimal getOfficialSalary();
	Currency getCurrency();
	Date getDateHired();
	Date getDateFired();
	EmployeeCutProjection getAccountableTo();
	String getInfo();
}
