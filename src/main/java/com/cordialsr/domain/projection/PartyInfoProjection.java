package com.cordialsr.domain.projection;

import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Party;
import com.cordialsr.domain.PartyInfo;
import com.cordialsr.domain.PcLevel;
import com.cordialsr.domain.PhoneNumber;
import com.fasterxml.jackson.annotation.JsonInclude;


@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "partyInfo", types = { PartyInfo.class })
public interface PartyInfoProjection {
	Long getId();
	Party getParty();
	String getAddressResidency();
	
	Boolean getConviction();
	
	Boolean getGender();
	Integer getChildren();

	String getAdvisorFirstname();
	String getAdvisorLastname();
	
	PhoneNumber getAdvisorPhone();
	
	Date getDateSign();
	PcLevel getPcLevel();
	String getDriverLicense();
}
