package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.SmVisitCollector;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "smVisitCollector", types = { SmVisitCollector.class })
public interface SmVisitCollectorMiniProjection {
	Long getId();
	Date igetInTime();
	Date getOutTime();
	Date getInDate();
	EmployeeMiniProjection getEmployee();
	PartyMiniProjection getParty();
	ContractMobileProjection getContract();
	BigDecimal getSumm();
	BigDecimal getToCashBox();
	CurrencyProjection getCurrency();
	
}
