package com.cordialsr.domain.projection;

import java.math.BigDecimal;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.SmServiceItem;
import com.cordialsr.domain.Unit;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "detail", types = { SmServiceItem.class })
public interface SmServiceItemProjection {
	
	Long getId();
	String getItemName();
	Integer getFno();
	BigDecimal getQuantity();
	Inventory getInventory();
	BigDecimal getPrice();
	Unit getUnit();
	BigDecimal getCost();
	Boolean getIsTovar();
}
