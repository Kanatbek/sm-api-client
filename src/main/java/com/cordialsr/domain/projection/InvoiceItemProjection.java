package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Currency;
import com.cordialsr.domain.InvItemSn;
import com.cordialsr.domain.InvoiceItem;
import com.cordialsr.domain.StockIn;
import com.cordialsr.domain.StockOut;
import com.cordialsr.domain.Unit;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "list", types = { InvoiceItem.class })
public interface InvoiceItemProjection {
	  Integer getId();
	  InventoryInlineProjection getInventory();
	  Unit getUnit();
	  BigDecimal getQuantity();
	  BigDecimal getPrice();
	  Currency getCurrency();
	  BigDecimal getCost();
	  BigDecimal getUnitCbm();
	  BigDecimal getUnitKg();
	  
	  List<InvItemSn> getChildInvSns();
	  List<StockIn> getChildStockIns();	  
	  List<StockOutProjection> getChildStockOuts();
}
