package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Department;
import com.cordialsr.domain.PlanFact;
import com.cordialsr.domain.Position;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "planfact", types = { PlanFact.class })
public interface PlanFactProjection {
	Integer getId();
	PartyMiniProjection getParty();
	Integer getYear();
	Integer getMonth();
	CompanyMiniProjection getCompany();
	Department getDepartment();
	RegionMiniProjection getRegion();
	BranchMiniProjection getBranch();
	Position getPosition();
	BigDecimal getPlan();
	BigDecimal getFact();
	BigDecimal getPaidAhead();
	BigDecimal getPaidCurrent();
	BigDecimal getPaidPast();
	BigDecimal getRemain();
	Boolean getIsOverdue();
	Currency getCurrency();
	Date getCpuDate();
	UserMiniProjection getUserCreated();
	Date getUpdatedDate();
	UserMiniProjection getUpdatedUser();
}
