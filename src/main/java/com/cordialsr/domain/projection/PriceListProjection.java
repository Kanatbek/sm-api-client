package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.City;
import com.cordialsr.domain.PriceList;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "priceList", types = { PriceList.class })
public interface PriceListProjection {

	Integer getId();
	BranchMiniProjection getBranch();
	City getCity();
	CompanyMiniProjection getCompany();
	ContractTypeProjection getContractType();
	CountryProjection getCountry();
	InventoryInlineProjection getInventory();
	RegionMiniProjection getRegion();
	ScopeProjection getScope();
	Date getData();
	BigDecimal getPrice();
	CurrencyProjection getCurrency();
	BigDecimal getFirstPayment();
	Integer getMonth();
	BigDecimal getDealerDeposit();
	Boolean getActive();
	Boolean getIsSell();
	Boolean getIsRent();
	Boolean getForYur();
	Boolean getForFiz();
	Date getDateClosed();
	Set<PaymentTemplateProjection> getPaymentTemplates();

}
