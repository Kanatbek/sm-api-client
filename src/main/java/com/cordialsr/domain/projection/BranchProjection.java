package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.BranchType;
import com.cordialsr.domain.City;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Country;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "branchProjection", types = { Branch.class })
public interface BranchProjection {
	Integer getId();
	City getCity();
	Company getCompany();
	Country getCountry();
	String getBranchName();
	BranchType getBranchType();	
}
