package com.cordialsr.domain.projection;

import java.util.Date;
import java.util.Set;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.security.Role;
import com.cordialsr.domain.security.RoleDomainPermission;
import com.cordialsr.domain.security.RoleTransactionPermission;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "role", types = { Role.class })
public interface RoleProjection {
	
	 Integer getRoleId();
	 String getName();
	 Boolean getEnabled();
	 Date getCreatedDate();
	 Date getUpdatedDate();
	 Set<RoleDomainPermission> getRoleDomainPermissions();
	 Set<RoleTransactionPermission> getRoleTransactionPermissions();
	 String getLabel();
	 // Set<User> getUsers();
}
