package com.cordialsr.domain.projection;

import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Party;
import com.cordialsr.domain.PartyRelatives;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "partyRelative", types = { PartyRelatives.class })

public interface PartyRelativeProjection {
	 Long getId();
	 Party getParty();
	 String getLevel();
	 String getFirstname();
	 String getLastname();
	 String getPhone();
	 String getJobPlace();
	 String getPosition();
	 Date getBirthDate();
}
