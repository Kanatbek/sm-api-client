package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.ContractStatus;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "contractStatusProjection", types = { ContractStatus.class })
public interface ContractStatusProjection {
	Integer getId();
	String getName();
	String getInfo();

}
