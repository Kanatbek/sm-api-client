package com.cordialsr.domain.projection;

import java.util.List;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.FinCashflowStatement;
import com.cordialsr.domain.FinOperation;
import com.cordialsr.domain.security.Transaction;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "finoper", types = { FinOperation.class })
public interface FinOperProjection {
	Integer getId();
	String getName();
	String getGlDebet();
	String getGlCredit();
	Transaction getTransaction();
	String getInfo();
	Boolean getEnabled();
	Boolean getParentMandatory();
	List<FinCashflowStatement> getCfs();
}
