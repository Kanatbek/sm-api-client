package com.cordialsr.domain.projection;

import java.math.BigDecimal;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.PaymentTemplate;
import com.cordialsr.domain.PriceList;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "paymentTemplate", types = { PaymentTemplate.class })
public interface PaymentTemplateProjection {

	Integer getId();

	@JsonIgnore
	PriceList getPriceList();
	
	Integer getPaymentOrder();
	BigDecimal getSumm();
	Integer getMonth();
}
