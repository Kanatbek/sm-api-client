package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.InvoiceStatus;
import com.cordialsr.domain.OrderHeader;
import com.cordialsr.domain.ScopeType;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "list", types = { OrderHeader.class })
public interface OrderProjection {
	
	Integer getId();
	Branch getBranchSupplier();
    Branch getBranch();
	InvoiceStatus getOrderStatus();
	CompanyMiniProjection getCompany();
	PartyMiniProjection getParty();
	ScopeType getScopeType();
	UserMiniProjection getUserUpdated();
	UserMiniProjection getUserAdded();
	Date getData();
	BigDecimal getNetWeight();
	BigDecimal getVolumeCbm();
	String getOrderNumber();
	Date getDateUpdated();
	Date getDateCreated();
	
	List<OrderItemProjection> getOrderItems();
}
