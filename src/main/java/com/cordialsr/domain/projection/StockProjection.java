package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.StockIn;
import com.cordialsr.domain.StockStatus;
import com.cordialsr.domain.Unit;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "list", types = { StockIn.class })
public interface StockProjection {
	Integer getId();
	Company getCompany();
	Branch getBranch();
	InventoryInlineProjection getInventory();
	Integer getQuantity();
	Unit getUnit();
	String getSerialNumber();
	StockStatus getGenStatus();
	StockStatus getIntStatus();
	String getRefkey();
	BigDecimal getWsumm();
	Currency getWcurrency();
	BigDecimal getDsumm();
	Currency getDcurrency();
    BigDecimal getRate();	
    Date getDateIn();
    String getGlCode();
    Boolean getBroken();
}
