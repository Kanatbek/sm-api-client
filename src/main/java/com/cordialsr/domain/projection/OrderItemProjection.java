package com.cordialsr.domain.projection;

import java.math.BigDecimal;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.OrderItem;
import com.cordialsr.domain.Unit;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "list", types = { OrderItem.class })
public interface OrderItemProjection {
	
	Integer getId();
	InventoryInlineProjection getInventory();
	BigDecimal getQuantity();
	Unit getUnit();
	BigDecimal getUnitCbm();
	BigDecimal getUnitKg();
	
}
