package com.cordialsr.domain.projection;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Company;
import com.cordialsr.domain.Country;
import com.cordialsr.domain.DriverLicense;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.PartyEducation;
import com.cordialsr.domain.PartyInfo;
import com.cordialsr.domain.PartyRelatives;
import com.cordialsr.domain.PartyType;
import com.cordialsr.domain.PhoneNumber;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "party", types = { Party.class })
public interface PartyProjection {
	Integer getId();
	String getIik();
	String getBik();	
	String getEmail();
	String getIinBin();
	Date getBirthday();
	Boolean getIsYur();
	String getFullFIO();
	Date getDateIssue();
//	Boolean getGender();
	Company getCompany();
	String getLastname();
	Date getDateExpire();
	String getFirstname();
//	Integer getChildren();
	String getMiddlename();
	String getFamilyStatus();
	String getBirthPlace();
	CountryProjection getResidency();
	String getCompanyName();
	String getPhotoString();
	PartyType getPartyType();
	
	String getIssuedInstance();
	String getPassportNumber();
	PartyInfoProjection getPartyInfo();
	PartyMiniProjection getOriginal();
	Set<PhoneNumber> getPhoneNumbers();
	Set<PartyRelativeProjection> getPartyRelatives();
	Set<AddressInlineProjection> getAddresses();
	Set<PartyEducationProjection> getPartyEducation();
	Set<PartyExperienceProjection> getPartyExperience();
	
	
//	Set<KeySkillProjection> getKeySkill();
}	