package com.cordialsr.domain.projection;

import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Company;
import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.SmFcTerm;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "mini", types = { SmFcTerm.class })
public interface SmFcTermProjection {

	Integer getId();
	Company getCompany();
	Integer getProf();
	Date getSdate();
	Integer getF1();
	Integer getF2();
	Integer getF3();
	Integer getF4();
	Integer getF5();
	Inventory getInventory();
	
}
