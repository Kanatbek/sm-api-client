package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Currency;
import com.cordialsr.domain.SmService;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "smServiceDetail", types = { SmService.class })
public interface SmServiceInlineBranchProjection {
	Long getId();
	ContractProjection getContract();
	String getType();
	Date getSdate();
	Date getStime();
	Date getItime();
	String getCaremanFio();
	Boolean getStorno();
	String getInfo();
	CompanyMiniProjection getCompany();
	BranchMiniProjection getBranch();
	EmployeeProjection getCareman();
	PartyProjection getCustomer();
	BigDecimal getCost();
	BigDecimal getDiscount();
	BigDecimal getSumm();
	BigDecimal getPaid();
	Currency getCurrency();
	BigDecimal getPremi();
	Currency getPremiCurrecny();
	InventoryInlineProjection getInventory();
	String getRefkey();
	User getUserAdded();
	Boolean getVerified();
	String getServiceNumber();
	Integer getMark();
	Boolean getOverdue();
	Boolean getIsEarly();
	
	Set<SmServiceItemProjection> getSmServiceItems();
	List<SmCallProjection> getSmCalls();
	
}
