package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.PaymentStatus;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "micro", types = { Contract.class })
public interface ContractPaySchMini {
	Integer getId();
	PaymentStatusProjection getPaymentStatus();
	EmployeeMiniProjection getCareman();
}
