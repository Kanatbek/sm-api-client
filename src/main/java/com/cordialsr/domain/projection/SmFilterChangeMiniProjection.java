package com.cordialsr.domain.projection;

import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.SmFilterChange;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "mini", types = { SmFilterChange.class })
public interface SmFilterChangeMiniProjection {
	Integer getId();
	//Contract getContract();
	//SmContract getSmContract();
	Integer getF1Mt();
	Date getF1Last();
	Date getF1Next();
	Integer getF2Mt();
	Date getF2Last();
	Date getF2Next();
	Integer getF3Mt();
	Date getF3Last();
	Date getF3Next();
	Integer getF4Mt();
	Date getF4Last();
	Date getF4Next();
	Integer getF5Mt();
	Date getF5Last();
	Date getF5Next();
	Integer getPrMt();
	Date getPrLast();
	Date getPrNext();
	Boolean getEnabled();
}