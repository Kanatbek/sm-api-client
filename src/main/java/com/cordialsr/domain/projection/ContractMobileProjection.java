package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Address;
import com.cordialsr.domain.Contract;

import com.cordialsr.domain.PhoneNumber;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "contractForMobile", types = { Contract.class })
public interface ContractMobileProjection {
	Integer getId();
	CompanyMiniProjection getCompany();
	BranchProjection getServiceBranch();
	EmployeeMiniProjection getCareman();
	EmployeeMiniProjection getCollector();
	
	PartyProjectionList getCustomer();
	Address getAddrPay();
	PhoneNumber getPhonePay();
	PhoneNumber getMobilePay();
	
	CurrencyProjection getCurrency();
	String getInventorySn();
	
//	PartyProjectionList getExploiter();
//	Address getAddrFact();
//	PhoneNumber getPhoneFact();
//	PhoneNumber getMobileFact();
	
	BigDecimal getPaid();
	BigDecimal getCost();
	BigDecimal getSumm();
	Integer getMonth();

	
	Date getDateSigned();
	String getContractNumber();
	String getRefkey();
	Boolean getIsRent();
	List<ContractPaymentScheduleProjection> getPaymentSchedules();
//	List<FinDoc> getFinDocs();
}
