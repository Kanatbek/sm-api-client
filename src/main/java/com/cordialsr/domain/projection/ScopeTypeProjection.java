package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.ScopeType;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "ScopeType", types = { ScopeType.class })
public interface ScopeTypeProjection {
	
	String getTypeCode();
	String getName();
	String getInfo();
	
}
