package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.ContractStornoRequest;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "mini", types = { ContractStornoRequest.class })
public interface ContractStornoRequestMiniProjection {
	Long getId();
	CompanyMiniProjection getCompany();
	BranchMiniProjection getBranch();
	String getReqNumber();
	ContractNanoProjection getContract();
	UserMiniProjection getReqAuthor();
	Calendar getReqDate();
	Integer getStatus();
	String getInfo();
	BigDecimal getReturnAmountDue();
	CurrencyProjection getCurrency();
	BigDecimal getReturnAmount();
	UserMiniProjection getGrantAuthor();
	Calendar getGrantDate();
	Date getCloseDate();
}
