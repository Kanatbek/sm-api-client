package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Incoterm;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.InvoiceStatus;
import com.cordialsr.domain.InvoiceTrace;
import com.cordialsr.domain.ScopeType;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "full", types = { Invoice.class })
public interface InvoiceProjection {
	 Integer getId();
	  Branch getBranchImporter();
	  Branch getBranch();
	  Company getCompany();
	  Currency getCurrency();
	  Incoterm getIncoterm();
	  InvoiceStatus getInvoiceStatus();
	  PartyMiniProjection getParty();
	  PartyMiniProjection getBankPartner();
	  ScopeType getScopeType();
	  UserMiniProjection getUserAdded();
	  UserMiniProjection getUserUpdated();
	  Date getData();
	  Calendar getDateCreated();
	  Calendar getDateUpdated();
	  String getInvoiceNumber();
	  BigDecimal getCost();
	  BigDecimal getTaxSum();
	  BigDecimal getTaxRate();
	  BigDecimal getTotalCost();
	  BigDecimal getPaid();
	  BigDecimal getNetWeight();
	  BigDecimal getGrossWeight();
	  BigDecimal getVolumeCbm();
	  Integer getInvoiceType();
	  Long getDocId();
	  String getDocno();
	  Boolean getStorno();
	
	  Set<InvoiceTrace> getInvoiceTraces();
	  List<InvoiceItemProjection> getInvoiceItems();	  
}
