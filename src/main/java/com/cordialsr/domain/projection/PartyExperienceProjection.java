package com.cordialsr.domain.projection;

import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.PartyExperience;
import com.cordialsr.domain.Position;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "party_experience", types = { PartyExperience.class })
public interface PartyExperienceProjection {
	 Long getId();
	 PartyMiniProjection getParty();
	 String getCompany();
	 String getPosition();
	 Date getDateHired();
	 Date getDateFired();
	 String getDuties();
	 String getReason();
}
