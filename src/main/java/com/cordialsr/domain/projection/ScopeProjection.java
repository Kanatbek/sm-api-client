package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Scope;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "Scope", types = { Scope.class })
public interface ScopeProjection {
	
	 String getName();
	 String getInfo();
}
