package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Department;
import com.cordialsr.domain.FinActivityType;
import com.cordialsr.domain.FinCashflowStatement;
import com.cordialsr.domain.FinEntry;
import com.cordialsr.domain.FinGlAccount;
import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.KassaBank;
import com.cordialsr.domain.Unit;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "finentry", types = { FinEntry.class })
public interface FinEntryProjection {
	 Integer getId();
	 // Company getCompany();
	 // Branch getBranch();
	 // FinDoc getFinDoc();
	 // Party getParty();
	 FinGlAccount getGlAccount();
	 String getDc();
	 BigDecimal getDsumm();
	 Currency getDcurrency();
	 BigDecimal getRate();
	 BigDecimal getWsumm();
	 Currency getWcurrency();
	 String getInfo();
	 String getDocno();
	 Integer getMonth();
	 String getYear();
	 Date getDdate();
	 String getRefkey();
	 FinCashflowStatement getCashflowStatement();
	 FinActivityType getFinActType();
	 Boolean getIsTovar();
	 String getItemName();
	 Inventory getInventory();
	 BigDecimal getQuantity();
	 Unit getUnit();
	 BigDecimal getPrice();
	 Integer getEntrycount(); 
	 Department getDepartment();
	 // Invoice getInvoice();
	 String getPostkey();
	 KassaBank getKassaBank(); 
	 Date getCpuDate();
	 Date getUpdatedDate();
}
