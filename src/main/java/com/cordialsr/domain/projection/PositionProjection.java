package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Department;
import com.cordialsr.domain.Position;
import com.fasterxml.jackson.annotation.JsonInclude;
@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "Position", types = { Position.class })
public interface PositionProjection {

	Integer getId();
	String getName();
	String getDescription();
	Department getDepartment();
}
