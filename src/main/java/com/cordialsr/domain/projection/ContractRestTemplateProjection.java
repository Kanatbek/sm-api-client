package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractPaymentSchedule;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Party;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "contractRestTemplate", types = { Contract.class })
public interface ContractRestTemplateProjection {

	Long getId();
	
	Party getCustomer();
	Party getExploiter();

	CompanyMiniProjection getCompany();
	BranchMiniProjection getBranch();
	
	Date getDateSigned();
	String getContractNumber();
	BigDecimal getCost();
	Integer getMonth();
	BigDecimal getDiscount();
	BigDecimal getSumm();
	CurrencyProjection getCurrency();
	BigDecimal getPaid();
	BigDecimal getRate();
	BigDecimal getFromDealerSumm();
	Boolean getIsRent();
	String getInfo();
	Boolean getStorno();
	Date getStornoDate();
	List<ContractPaymentScheduleFullProjection> getPaymentSchedules();
	
}
