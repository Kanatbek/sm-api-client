package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Country;
import com.cordialsr.domain.Currency;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "country", types = { Country.class })
public interface CountryProjection {
	Integer getId();
	Currency getSecondaryCurrency();
	Currency getLocalCurrency();
	String getName();
	String getPhoneCode();
	String getCountryCode();
}
