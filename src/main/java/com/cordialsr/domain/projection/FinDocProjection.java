package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Department;
import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.FinDocType;
import com.cordialsr.domain.FinEntry;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.Party;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "findoc", types = { FinDoc.class })
public interface FinDocProjection {
	Integer getId();
	  Company getCompany();
	  Branch getBranch();
	  String getTitle();
	  String getInfo();
	  String getDocno();
	  String getYear();
	  String getRefkey();
	  String getReftyp();
	  Integer getMonth();
	  FinDocType getFinDocType();
	  Date getCpuDate();
	  Date getDocDate();
	  Department getDepartment();
	  Boolean getStorno();
	  FinDoc getParentFindoc();
	  String getStDocno();
	  String getStYear();
	  UserMiniProjection getUser();
	  Party getParty();
	  Contract getContract();
	  Integer getPaymentOrder();
	  Boolean getIsMain();
	  BigDecimal getDsumm();
	  Currency getDcurrency();
	  BigDecimal getRate();
	  BigDecimal getWsumm();
	  Currency getWcurrency();
	  BigDecimal getDsummPaid();
	  BigDecimal getWsummPaid();
	  Boolean getClosed();
	  Boolean getForbuh();
	  String getTrcode();
	  Invoice getInvoice();
	  String getRefkeyMain();
	  Date getUpdatedDate();
	  Date getCpuTime();
	
	  Set<FinEntry> getFinEntries();
	  Set<FinDoc> getChildFinDocs();
}
