package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.AwardType;
import com.cordialsr.domain.ContractAwardSchedule;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.Position;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "mini", types = { ContractAwardSchedule.class })
public interface ContractAwardScheduleProjection {
	Long getId();
	AwardType getAwardType();
	Position getPosition();
	Party getParty();
	Date getDateSchedule();
	Date getDateAccrued();
	Currency getCurrency();
	BigDecimal getSumm();
	BigDecimal getAccrued();
	Boolean getRevertOnCancel();
	Integer getPorder();
	BigDecimal getRevertSumm();
	BigDecimal getDeduction();
}
