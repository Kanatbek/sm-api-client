package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.EducationLevel;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "levelProjection", types = { EducationLevel.class })
public interface levelProjection {
	Integer getId();
	String getLevel();
	String getDescription();
}
