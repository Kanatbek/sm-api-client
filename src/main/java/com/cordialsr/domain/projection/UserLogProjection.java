package com.cordialsr.domain.projection;

import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.security.Transaction;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "userlog", types = { User.class })
public interface UserLogProjection {
	Date getDate();
	Date getTime();
	User getUser();
	Transaction getTransaction();
	String getLogType();
	String getInfo();
}
