package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Address;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractPromos;
import com.cordialsr.domain.PhoneNumber;
import com.cordialsr.domain.SmCall;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "contractEdit", types = { Contract.class })
public interface ContractEditProjection {

	Integer getId();
	BranchProjection getBranch();
	CompanyMiniProjection getCompany();
	BranchProjection getServiceBranch();
	ContractTypeProjection getContractType();
	ContractStatusProjection getContractStatus();
	EmployeeMiniProjection getCoordinator();
	EmployeeMiniProjection getDemosec();
	EmployeeMiniProjection getDirector();
	EmployeeMiniProjection getManager();
	EmployeeMiniProjection getCareman();
	EmployeeMiniProjection getDealer();
	EmployeeMiniProjection getFitter();
	EmployeeMiniProjection getCollector();
	
	PartyProjectionList getCustomer();
	Address getAddrPay();
	PhoneNumber getPhonePay();
	PhoneNumber getMobilePay();
	
	PartyProjectionList getExploiter();
	Address getAddrFact();
	PhoneNumber getPhoneFact();
	PhoneNumber getMobileFact();
	
	PaymentStatusProjection getPaymentStatus();
	ServiceCategoryProjection getServiceCategory();
	Date getDateSigned();
	String getContractNumber();
	String getRefkey();
	Calendar getRegisteredDate();
	UserMiniProjection getRegisteredUser();
	Calendar getUpdatedDate();
	UserMiniProjection getUpdatedUser();
	BigDecimal getCost();
    Integer getMonth();
	BigDecimal getDiscount();
	BigDecimal getSumm();
	CurrencyProjection getCurrency();
	BigDecimal getPaid();
	BigDecimal getRate();
	BigDecimal getFromDealerSumm();
	Boolean getForbuh();
	Boolean getIsRent();
	String getInfo();
	Boolean getStorno();
	Date getStornoDate();
	Boolean getResigned();
	Date getResignedDate();
	Integer getOldId();
	InventoryInlineProjection getInventory();
	String getInventorySn();
	Boolean getCaremanSales();
	
	List<SmCall> getSmCalls();	
	List<ContractHistoryProjection> getContractHistory();
	List<ContractPaymentScheduleProjection> getPaymentSchedules();

	List<ConItemProjection> getContractItems();
	List<ContractPromos> getContractPromos();
	List<ContractAwardScheduleProjection> getCawSchedules();
	List<FinDocProjectionList> getFinDocs();

}
