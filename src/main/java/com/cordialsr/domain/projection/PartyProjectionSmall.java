package com.cordialsr.domain.projection;

import java.util.Date;
import java.util.Set;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Company;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.PartyInfo;
import com.cordialsr.domain.PartyType;
import com.cordialsr.domain.PhoneNumber;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "partysmall", types = { Party.class })
public interface PartyProjectionSmall {
	PartyType getPartyType();
	Set<AddressInlineProjection> getAddresses();
	Company getCompany();
	Integer getId();
	String getIinBin();
	String getFirstname();
	String getLastname();
	String getMiddlename();
	String getEmail();
	Date getBirthday();
	Boolean getIsYur();
	String getPassportNumber();
	Date getDateIssue();
	Date getDateExpire();
	String getIssuedInstance();
	String getIik();
	String getBik();	
	PartyMiniProjection getOriginal();
	
	Set<PartyInfo> getPartyInfo();
	Set<PhoneNumber> getPhoneNumbers();
	String getCompanyName();
}	