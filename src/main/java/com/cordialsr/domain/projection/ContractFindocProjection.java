package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.springframework.data.rest.core.config.Projection;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.FinDoc;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "contractFindoc", types = { Contract.class })
public interface ContractFindocProjection {
	Integer getId();
	BranchProjection getBranch();
	CompanyMiniProjection getCompany();
	BranchProjection getServiceBranch();
	String getRefkey();
	Calendar getUpdatedDate();
	BigDecimal getCost();
    Integer getMonth();
	BigDecimal getDiscount();
	BigDecimal getSumm();
	CurrencyProjection getCurrency();
	BigDecimal getPaid();
	BigDecimal getRate();
	BigDecimal getFromDealerSumm();
	String getInfo();
	Boolean getStorno();
	Date getStornoDate();
	Boolean getResigned();
	Date getResignedDate();
	Integer getOldId();
	String getInventorySn();
	Boolean getCaremanSales();
	List<FinDoc> getFinDocs();	
}
