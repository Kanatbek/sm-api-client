package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Award;
import com.cordialsr.domain.AwardCase;
import com.cordialsr.domain.AwardType;
import com.cordialsr.domain.City;
import com.cordialsr.domain.Department;
import com.cordialsr.domain.InvMainCategory;
import com.cordialsr.domain.InvSubCategory;
import com.cordialsr.domain.Position;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "award", types = { Award.class })
public interface AwardProjection {
	 Integer getId();
	 AwardCase getAwardCase();
	 AwardType getAwardType();
	 BranchProjection getBranch();
	 City getCity();
	 CompanyMiniProjection getCompany();
	 Department getDepartment(); 
	 CountryProjection getCountry();
	 CurrencyProjection getCurrency();
	 InvMainCategory getInvMainCategory();
	 InvSubCategory getInvSubCategory();
	 InventoryInlineProjection getInventory();
	 Position getPosition();
	 RegionProjection getRegion();
	 ScopeProjection getScope();
	 Date getDateStart();
	 Date getDateEnd();
	 BigDecimal getMin();
	 BigDecimal getMax();
	 BigDecimal getCoefficient();
	 BigDecimal getSumm();
	 Integer getPremiDiv();
	 Date getDateCreated();
	 User getUserCreated();
	 Boolean getForRent();
	 Boolean getForSale();
	 Boolean getEnabled();
	 
	 List<AwardTemplateProjection> getAwardTemplates();
}
