package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.City;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Country;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.FinCurrate;
import com.cordialsr.domain.Region;
import com.cordialsr.domain.Scope;
import com.cordialsr.domain.ScopeType;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "currate", types = { FinCurrate.class, Currency.class})
public interface FinCurrateProjection {
	Integer getId();
	Currency getMainCurrency();
	Currency getSecCurrency();
	Branch getBranch();
	City getCity();
	Company getCompany();
	Country getCountry();
	Region getRegion();
	Scope getScope();
	ScopeType getScopeType();
	BigDecimal getRate();
	Date getRdate();
	Date getCpuDate();
	UserMiniProjection getUserAuthor();
}
