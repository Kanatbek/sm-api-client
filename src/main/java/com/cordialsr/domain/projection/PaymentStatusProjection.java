package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.PaymentStatus;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "paymentStatus", types = { PaymentStatus.class })
public interface PaymentStatusProjection {
	Integer getId();
	String getName();
	String getInfo();

}
