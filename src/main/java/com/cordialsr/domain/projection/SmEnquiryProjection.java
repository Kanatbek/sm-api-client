package com.cordialsr.domain.projection;

import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.SmEnquiry;
import com.cordialsr.domain.SmService;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "full", types = { SmEnquiry.class })
public interface SmEnquiryProjection {

	Long getId();
	Company getCompany();
	Branch getBranch();
	Contract getContract();
	Party getParty();
	Date getEdate();
	Date getEtime();
	User getOperator();
	String getInfo();
	String getFullFio();
	Integer getStatus();
	Integer getEtype();
	String getPhone();
	String getEnquiryNumber();
	SmService getSmService();
	
}
