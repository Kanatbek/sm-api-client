package com.cordialsr.domain.projection;

import java.util.Date;

public interface PartyPojoProjection {
   
   Long getId();
   String getIinBin();
   String getFirstname();
   String getLastname();
   String getMiddlename();
   String getEmail();
   Date getBirthday();
   Boolean getIsYur();
   String getPassportNumber();
   Date getDateIssue();
   Date getDateExpire();
   String getIssuedInstance();
   String getIik();
   String getBik();
   String getCompanyName();
   Date getCpuDate();

}
