package com.cordialsr.domain.projection;

import java.util.Date;
import java.util.Set;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Address;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.PartyType;
import com.cordialsr.domain.PhoneNumber;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "mini", types = { Party.class })
public interface PartyMiniProjection {
	// PartyType getPartyType();
	// Set<Address> getAddresses();
	// Company getCompany();
	Long getId();
	String getIinBin();
	String getFirstname();
	String getLastname();
	String getMiddlename();
	String getEmail();
	Date getBirthday();
	Boolean getIsYur();
//	String getPhotoString();
	String getPassportNumber();
//	Date getDateIssue();
//	Date getDateExpire();
//	String getIssuedInstance();
	String getIik();
	String getBik();	
//	Set<PhoneNumber> getPhoneNumbers();
	String getFullFIO();
	String getCompanyName();	
}	