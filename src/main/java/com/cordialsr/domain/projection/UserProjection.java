package com.cordialsr.domain.projection;

import java.util.Date;
import java.util.Set;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.Region;
import com.cordialsr.domain.security.Role;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "userdetail", types = { User.class })
public interface UserProjection {
	Long getUserid();
	PartyMiniProjection getParty();
	String getUsername();
	Boolean getEnabled();
	Date getDateCreated();
	Date getDateUpdated();
	Set<Role> getUserRoles();
	String getFirstName();
	String getLastName();
	String getPhone();
	String getEmail();
	Company getCompany();	
	Region getRegion();
	Branch getBranch();
}
