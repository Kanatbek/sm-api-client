package com.cordialsr.domain.projection;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Address;
import com.cordialsr.domain.CrmLead;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.PhoneNumber;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "lead", types = { CrmLead.class })
public interface LeadProjection {
	Long getId();
	EmployeeMiniProjection getDealer();
	CrmLead getAdviser();
	String getFirstname();
	String getLastname();
	String getMiddlename();
	String getEmail();
	String getInfo();
	String getSource();
	Date getCpuDate();
	PartyProjection getParty();
	CompanyInlineProjection getCompany();
	BranchProjection getBranch();
	PhoneNumber getPhone();
	
	AddressInlineProjection getAddress();
	
	List<PhoneNumber> getPhoneNumbers();
	List<CrmLead> getChildLeads();

}
