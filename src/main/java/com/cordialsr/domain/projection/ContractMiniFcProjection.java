package com.cordialsr.domain.projection;

import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ServiceCategory;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "minifc", types = { Contract.class })
public interface ContractMiniFcProjection {
	Integer getId();
//	Branch getBranch();
//	Company getCompany();
	BranchMiniProjection getServiceBranch();
//	ContractTypeProjection getContractType();
//	ContractStatus getContractStatus();
//	EmployeeMiniProjection getCoordinator();
//	EmployeeMiniProjection getDemosec();
//	EmployeeMiniProjection getDirector();
//	EmployeeMiniProjection getManager();
	EmployeeMiniProjection getCareman();
	EmployeeMiniProjection getDealer();
	EmployeeMiniProjection getFitter();
	EmployeeMiniProjection getCollector();
//	PartyMiniProjection getCustomer();
	PartyMiniProjection getExploiter();
//	PaymentStatus getPaymentStatus();
	ServiceCategoryProjection getServiceCategory();
	Date getDateSigned();
	String getContractNumber();
	String getRefkey();
//	Date getRegisteredDate();
//	UserMiniProjection getRegisteredUser();
//	Date getUpdatedDate();
//	UserMiniProjection getUpdatedUser();
//	BigDecimal getCost();
//  Integer getMonth();
//	BigDecimal getDiscount();
//	BigDecimal getSumm();
//	Currency getCurrency();
//	BigDecimal getPaid();
//	BigDecimal getRate();
//	BigDecimal getFromDealerSumm();
//	Boolean getForbuh();
	Boolean getIsRent();
	Boolean getStorno();
//	Boolean getResigned();
//	Date getResignedDate();
	Integer getOldId();
	InventoryInlineProjection getInventory();
	String getInventorySn();
}
