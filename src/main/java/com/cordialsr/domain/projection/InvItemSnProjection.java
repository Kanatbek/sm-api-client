package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.InvItemSn;
import com.cordialsr.domain.InvoiceItem;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "invItemSn", types = { InvItemSn.class })
public interface InvItemSnProjection {
	
	Long getId();
	InvoiceItem getInvoiceItem();
	String getSerialNumber();
}
