package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.CrmDemo;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "mini", types = { CrmDemo.class })
public interface DemoMiniProjection {
	Integer getId();
	EmployeeMiniProjection getDealer();
	
}
