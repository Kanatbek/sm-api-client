package com.cordialsr.domain.projection;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.SmService;
import com.cordialsr.domain.SmVisit;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "mini", types = { SmService.class })
public interface SmServiceMini {

	Long getId();
	String getType();
	Date getSdate();
	Date getStime();
	Date getItime();
	String getCaremanFio();
	String getInfo();
	SmVisit getSmVisit();
	EmployeeMiniProjection getCareman();
	PartyMiniProjection getCustomer();
	InventoryInlineProjection getInventory();
	String getServiceNumber();
	List<SmServiceItemMiniProjection> getSmServiceItems();	
}
