package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Company;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Party;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "companyProjection", types = { Company.class })
public interface CompanyInlineProjection {

	Integer getId();
	Currency getCurrency();
	String getName();
	Party getParty();
	
}
