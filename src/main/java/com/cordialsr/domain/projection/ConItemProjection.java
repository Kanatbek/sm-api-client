package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.ContractItem;
import com.cordialsr.domain.Unit;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "mini", types = { ContractItem.class })
public interface ConItemProjection {
	Long getId();
	ConItemStatusProjection getConItemStatus();
	ContractMiniProjection getContract();
	InventoryInlineProjection getInventory();
	PriceListProjection getPriceList();
	Unit getUnit();
	String getSerialNumber();
	BigDecimal getPrice();
	BigDecimal getQuantity();
	BigDecimal getCost();
	BigDecimal getDiscount();
	BigDecimal getSumm();
	Date getWriteoffDate();
	String getGlCode();
}
