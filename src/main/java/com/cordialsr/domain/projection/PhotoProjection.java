package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Photo;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "photo", types = { Photo.class })
public interface PhotoProjection {
	Integer getId();
	String getPhotoString();
}
