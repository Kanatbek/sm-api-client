package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Inventory;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "mini", types = { Inventory.class })
public interface InventoryMiniProjection {

	Integer getId();
	Company getCompany();
//	InvMainCategory getInvMainCategory();
//	InvSubCategory getInvSubCategory();
//	Manufacturer getManufacturer();
//	Unit getUnit();
	String getName();
	String getModel();
	String getCode();
//	BigDecimal getVolumeCbm();
//	BigDecimal getWeightKg();
	
	// List<Inventory> getSpares();
//	List<Inventory> getParents();
//	List<InvFno> getInvFno();
}
