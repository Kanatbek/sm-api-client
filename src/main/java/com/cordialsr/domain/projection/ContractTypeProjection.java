package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.City;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.ContractType;
import com.cordialsr.domain.Country;
import com.cordialsr.domain.Region;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "contractType", types = { ContractType.class })
public interface ContractTypeProjection {
	Integer getId();
	Branch getBranch();
	City getCity();
	Company getCompany();
	Country getCountry();
	Region getRegion();
	String getName();
	String getInfo();
	String getScope();
	Integer getMaxItems();
	InventoryInlineProjection getInventory();
	Boolean getOs();
}
