package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.SmEnquiry;
import com.cordialsr.domain.SmService;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "list", types = { SmService.class })
public interface SmServiceList {
	Long getId();
	ContractMiniProjection getContract();
	String getType();
	Date getSdate();
	Date getStime();
	Date getItime();
	String getCaremanFio();
	Boolean getStorno();
	String getInfo();
	Branch getBranch();
	CompanyMiniProjection getCompany();
	EmployeeMiniProjection getCareman();
	PartyMiniProjection getCustomer();
	BigDecimal getCost();
	BigDecimal getDiscount();
	BigDecimal getSumm();
	BigDecimal getPaid();
	Currency getCurrency();
	BigDecimal getPremi();
	Currency getPremiCurrecny();
	InventoryInlineProjection getInventory();
	String getRefkey();
	User getUserAdded();
	Boolean getVerified();
	String getServiceNumber();
	Integer getMark();
	SmEnquiry getSmEnquiry();
	Boolean getOverdue();
	Boolean getIsEarly();
}
