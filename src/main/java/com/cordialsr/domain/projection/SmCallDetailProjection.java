package com.cordialsr.domain.projection;

import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Contract;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.PhoneNumber;
import com.cordialsr.domain.SmCall;
import com.cordialsr.domain.SmService;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "detail", types = { SmCall.class })
public interface SmCallDetailProjection {

	Integer getId();
	CompanyMiniProjection getCompany();
	BranchMiniProjection getBranch();
	Contract getContract();
	SmService getService();
	Party getParty();
	PhoneNumber getPhoneNumber();
	Date getCallDate();
	Date getCallTime();
	User getUser();
	String getInfo();
	String getResponder();
	Integer getStatus();
	Boolean getIsOutgoing();
	String getCallNumber();
	
}
