package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.PartyType;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "partyTypeProjection", types = { PartyType.class })
public interface PartyTypeInlineProjection {
	String getName();
	String getDescription();
	Boolean getYur();
	Boolean getFiz();
}
