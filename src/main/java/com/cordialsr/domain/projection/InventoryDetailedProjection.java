package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Company;
import com.cordialsr.domain.InvFno;
import com.cordialsr.domain.InvMainCategory;
import com.cordialsr.domain.InvSubCategory;
import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.Manufacturer;
import com.cordialsr.domain.Unit;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "detailed", types = { Inventory.class })
public interface InventoryDetailedProjection {
	Integer getId();
	Company getCompany();
	InvMainCategory getInvMainCategory();
	InvSubCategory getInvSubCategory();
	Manufacturer getManufacturer();
	Unit getUnit();
	String getName();
	String getModel();
	String getCode();
	BigDecimal getVolumeCbm();
	BigDecimal getWeightKg();
	
	// List<Inventory> getSpares();
	List<Inventory> getParents();
	List<InvFno> getInvFno();
}
