package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Address;
import com.cordialsr.domain.AddressType;
import com.cordialsr.domain.City;
import com.cordialsr.domain.CityArea;
import com.cordialsr.domain.Country;
import com.cordialsr.domain.Oblast;
import com.cordialsr.domain.Party;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "addressProjection", types = { Address.class })
public interface AddressInlineProjection {

	Integer getId();
	AddressType getAddressType();
	City getCity();
	CityArea getCityArea();
	Country getCountry();
	Oblast getOblast();
	Party getParty();
	String getDistrict();
	String getStreet();
	String getHouseNumber();
	String getFlatNumber();
	String getZipCode();
	String getInfo();
	
}
