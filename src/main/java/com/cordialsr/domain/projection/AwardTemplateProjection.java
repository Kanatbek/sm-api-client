package com.cordialsr.domain.projection;

import java.math.BigDecimal;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Award;
import com.cordialsr.domain.AwardTemplate;
import com.fasterxml.jackson.annotation.JsonInclude;
@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "awardTemplate", types = { AwardTemplate.class })
public interface AwardTemplateProjection {
	Integer getId();
	Award getAward();
	Integer getPayrollMonth();
	Integer getMonth();
	BigDecimal getSumm();
	Boolean getRevertOnCancel();
	BigDecimal getRevertSumm();
}
