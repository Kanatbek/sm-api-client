package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Contract;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "mini", types = { Contract.class })
public interface ContractMiniProjection {
	Integer getId();
	Branch getBranch();
	Company getCompany();
	Branch getServiceBranch();
	ContractTypeProjection getContractType();
	ContractStatusProjection getContractStatus();
//	EmployeeMiniProjection getCoordinator();
//	EmployeeMiniProjection getDemosec();
//	EmployeeMiniProjection getDirector();
//	EmployeeMiniProjection getManager();
	EmployeeMiniProjection getCareman();
	EmployeeMiniProjection getDealer();
	EmployeeMiniProjection getFitter();
	EmployeeMiniProjection getCollector();
	PartyMiniProjection getCustomer();
	PartyMiniProjection getExploiter();
	PaymentStatusProjection getPaymentStatus();
	ServiceCategoryProjection getServiceCategory();
	Date getDateSigned();
	String getContractNumber();
	String getRefkey();
	Calendar getRegisteredDate();
	UserMiniProjection getRegisteredUser();
//	Date getUpdatedDate();
//	UserMiniProjection getUpdatedUser();
	BigDecimal getCost();
    Integer getMonth();
	BigDecimal getDiscount();
	BigDecimal getSumm();
	CurrencyProjection getCurrency();
	BigDecimal getPaid();
	BigDecimal getRate();
	BigDecimal getFromDealerSumm();
	Boolean getForbuh();
	Boolean getIsRent();
	Boolean getStorno();
	Date getStornoDate();
	Boolean getResigned();
	Date getResignedDate();
	Integer getOldId();
	InventoryInlineProjection getInventory();
	String getInventorySn();
	Boolean getCaremanSales();
}
