package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Contract;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "nano", types = { Contract.class })
public interface ContractNanoProjection {
	Integer getId();
	CompanyMiniProjection getCompany();
	BranchMiniProjection getBranch();
	PartyMiniProjection getCustomer();
	Date getDateSigned();
	String getContractNumber();
	String getRefkey();
	BigDecimal getCost();
    Integer getMonth();
	BigDecimal getDiscount();
	BigDecimal getSumm();
	CurrencyProjection getCurrency();
	BigDecimal getPaid();
	BigDecimal getRate();
	BigDecimal getFromDealerSumm();
	Boolean getForbuh();
	Boolean getIsRent();
	Boolean getStorno();
	Boolean getResigned();
	Date getResignedDate();
	String getInventorySn();
}
