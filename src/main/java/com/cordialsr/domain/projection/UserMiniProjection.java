package com.cordialsr.domain.projection;

import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "list", types = { User.class })
public interface UserMiniProjection {
	Long getUserid();
	String getUsername();
	Boolean getEnabled();
	Date getDateCreated();
	String getFirstName();
	String getLastName();
	String getPhone();
	String getEmail();
	
}
