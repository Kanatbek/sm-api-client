package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Date;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.InvoiceItem;
import com.cordialsr.domain.StockStatus;
import com.cordialsr.domain.Unit;

public interface StockOutProjection {
	Long getId();
	Inventory getInventory();
	BigDecimal getQuantity();
	Unit getUnit();
	String getSerialNumber();
	StockStatus getGenStatus();
	String getRefkey();
	BigDecimal getWsumm();
	Currency getWcurrency();
	BigDecimal getDsumm();
	Currency getDcurrency();
	BigDecimal getRate();
	Date getDateOut();
	String getGlCode();
	Boolean getBroken();
}
