package com.cordialsr.domain.projection;

import java.util.Set;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.security.Menu;
import com.cordialsr.domain.security.Transaction;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "menu", types = { Menu.class })
public interface MenuProjection {

	 Integer getId();
	 Integer getLevel();
	 String getName();
	 Transaction getTransaction();
	 String getRoute();
	 String getRouterLink();
	 String getIcon();
	
	 Set<Menu> getChildMenus();
	
}
