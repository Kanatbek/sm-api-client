package com.cordialsr.domain.projection;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.ContractPaymentSchedule;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "conPaymentSchedule", types = { ContractPaymentSchedule.class })
public interface ContractPaymentScheduleFullProjection {
	Long getId();
	ContractMiniProjection getContract();
	String getRefkey();
	Integer getPaymentOrder();
	Boolean getIsFirstpayment();
	BigDecimal getSumm();
	BigDecimal getPaid();
	Date getPaymentDate();
}
