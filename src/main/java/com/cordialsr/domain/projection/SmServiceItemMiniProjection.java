package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.SmServiceItem;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "detail", types = { SmServiceItem.class })
public interface SmServiceItemMiniProjection {
	
	Long getId();
	String getItemName();
	Integer getFno();
	InventoryMiniProjection getInventory();
}