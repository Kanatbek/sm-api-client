package com.cordialsr.domain.projection;

import org.springframework.data.rest.core.config.Projection;

import com.cordialsr.domain.Region;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Projection(name = "mini", types = { Region.class })
public interface RegionMiniProjection {
	Integer getId();
	String getName();	
}
