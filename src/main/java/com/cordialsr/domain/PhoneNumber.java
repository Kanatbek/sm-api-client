package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;

import java.util.Calendar;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "phone_number")
public class PhoneNumber implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Party party;
	private CrmLead lead;
	private String phNumber;
	private Boolean ismobile;
	private Boolean ismain;
	private String info;
	private Calendar cpuDate;

	public PhoneNumber() {
	}
	
	public PhoneNumber(Long id) {
		this.id = id;
	}

	public PhoneNumber(String phNumber) {
		this.phNumber = phNumber;
	}

	public PhoneNumber(Long id, Party party, String phNumber, Boolean ismobile, Boolean ismain, CrmLead lead, String info) {
		this.id = id;	
		this.party = party;
		this.phNumber = phNumber;
		this.ismobile = ismobile;
		this.ismain = ismain;
		this.lead = lead;
		this.info = info;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party_id")
	public Party getParty() {
		return this.party;
	}

	public void setParty(Party party) {
		this.party = party;
	}

	@Column(name = "number", nullable = false, length = 45)
	public String getPhNumber() {
		return this.phNumber;
	}

	public void setPhNumber(String number) {
		this.phNumber = number;
	}

	@Column(name = "ismobile")
	public Boolean getIsmobile() {
		return this.ismobile;
	}

	public void setIsmobile(Boolean ismobile) {
		this.ismobile = ismobile;
	}

	@Column(name = "ismain")
	public Boolean getIsmain() {
		return this.ismain;
	}

	public void setIsmain(Boolean ismain) {
		this.ismain = ismain;
	}
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "lead")
	public CrmLead getLead() {
		return lead;
	}

	public void setLead(CrmLead lead) {
		this.lead = lead;
	}
	
	@Column(name = "info", length = 150)
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cpu_date")
	public Calendar getCpuDate() {
		return cpuDate;
	}

	public void setCpuDate(Calendar cpuDate) {
		this.cpuDate = cpuDate;
	}

	// *******************************************************************************************************
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ismain == null) ? 0 : ismain.hashCode());
		result = prime * result + ((ismobile == null) ? 0 : ismobile.hashCode());
		result = prime * result + ((phNumber == null) ? 0 : phNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PhoneNumber other = (PhoneNumber) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ismain == null) {
			if (other.ismain != null)
				return false;
		} else if (!ismain.equals(other.ismain))
			return false;
		if (ismobile == null) {
			if (other.ismobile != null)
				return false;
		} else if (!ismobile.equals(other.ismobile))
			return false;
		if (party == null) {
			if (other.party != null)
				return false;
		} else if (party.getId() != other.party.getId())
			return false;
		if (phNumber == null) {
			if (other.phNumber != null)
				return false;
		} else if (!phNumber.equals(other.phNumber))
			return false;
		return true;
	}

	@Override
	public PhoneNumber clone() throws CloneNotSupportedException {
		return (PhoneNumber) super.clone();
	}
	
}
