package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stock_status")
public class StockStatus implements java.io.Serializable, Cloneable {

	public static final Integer STATUS_GEN_NEW = 1;	
	public static final Integer STATUS_GEN_RENT = 2;
	public static final Integer STATUS_GEN_SOLD = 3;
	public static final Integer STATUS_GEN_RETURNED = 4;
	public static final Integer STATUS_GEN_WASTED = 5;
	
	public static final Integer STATUS_INT_IN = 101;
	public static final Integer STATUS_INT_RESERVED = 102;
	public static final Integer STATUS_INT_TRANSFER = 103;
	public static final Integer STATUS_INT_ACCOUNTABLE = 105;
	
	/*
	1	NEW			Новый товар на складе (IN)	GEN
	2	RENT		В аренде (OUT)				GEN
	3	SOLD		Продан (OUT)				GEN
	4	RETURNED	Возврат (IN)				GEN
	5	WASTED		Списан (OUT)				GEN
	
	101	IN			На складе					INT
	102	RESERVED	Зарезервирован (IN)			INT
	103	TRANSFER	Перемещение (OUT)			INT
	105	ACCOUNTABLE	Подотчет					INT
	*/
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	private String info;
	private String type;
	
	public StockStatus() {
	}

	public StockStatus(Integer id) {
		this.id = id;
	}

	public StockStatus(String name, String info) {
		this.name = name;
		this.info = info;		
	}
	public StockStatus(Integer id, String name, String info) {
		this.id = id;
		this.name = name;
		this.info = info;		
	}
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "info", length = 255)
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
	@Column(name = "type", length = 3)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	// *****************************************************************************************
	
	@Override
	public String toString() {
		return "StockStatus [id=" + id + ", name=" + name + ", info=" + info + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StockStatus other = (StockStatus) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (info == null) {
			if (other.info != null)
				return false;
		} else if (!info.equals(other.info))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public StockStatus clone() throws CloneNotSupportedException {
		return (StockStatus) super.clone();
	}

}
