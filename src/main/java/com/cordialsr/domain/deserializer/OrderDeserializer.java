package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.InvoiceStatus;
import com.cordialsr.domain.OrderHeader;
import com.cordialsr.domain.OrderItem;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.ScopeType;
import com.cordialsr.domain.Unit;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class OrderDeserializer extends JsonDeserializer<OrderHeader>{

	@Override
	public OrderHeader deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		ObjectCodec oc = p.getCodec();
		JsonNode node = oc.readTree(p), ptNode;
		ObjectMapper jsonObjectMapper = new ObjectMapper();
		
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
		
		Integer orderId = null;
		if (node.get("id") != null) {
			orderId = node.get("id").asInt();			
		}

		Branch branch = null;
		if(node.get("branch") != null) {
			ptNode = node.get("branch");
			if (ptNode.get("id") != null) {
				Integer id = ptNode.get("id").asInt();
				branch = new Branch(id);
			}
		} 
		
		Branch branchSupplier = null;
		if(node.get("branchSupplier") != null) {
			ptNode = node.get("branchSupplier");
			if (ptNode.get("id") != null) {
				Integer id = ptNode.get("id").asInt();
				branchSupplier = new Branch(id);
			}
		}
		
		
		Company company = null;
		if(node.get("company") != null) {
			ptNode = node.get("company");
			if (ptNode.get("id") != null) {
				Integer id = ptNode.get("id").asInt();
				company = new Company(id);
			}
		} 
	
		
		
		
		Date date = null;		
	    if (node.get("data") != null) {
	    	String ddStr = node.get("data").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		date = df1.parse(ddStr);
				else
					date = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    } 
	    
	    
	    InvoiceStatus orderStatus = null;
		if(node.get("orderStatus") != null) {
			orderStatus = new InvoiceStatus();
			ptNode = node.get("orderStatus");
			if(ptNode.get("name") != null) 
			{
				String name = ptNode.get("name").asText();
				orderStatus.setName(name);
			}
		}		
		
		
		Party party = null;
		if(node.get("party") != null) {
			ptNode = node.get("party");
			if (ptNode.get("id") != null) {
				party = new Party();
				Long id = ptNode.get("id").asLong();
				party.setId(id);
			}
		} 
		
		
		ScopeType scopeType = null;
		if(node.get("scopeType") != null) {
			ptNode = node.get("scopeType");
			if (ptNode.get("typeCode") != null) {
				scopeType = new ScopeType();
				String typeCode = ptNode.get("typeCode").asText();
				scopeType.setTypeCode(typeCode);
			}
		}
		
		
		User userAdded = null;
		if(node.get("userAdded") != null) {
			ptNode = node.get("userAdded");
			if (ptNode.get("userid") != null) {
				userAdded = new User();
				int id = ptNode.get("userid").asInt();
				userAdded.setUserid(new Long(id));
			}
		} 
		BigDecimal netWeight = null;
		if (node.get("netWeight") != null) {
			netWeight = node.get("netWeight").decimalValue();
		}
		
		BigDecimal volumeCbm = null;
		if (node.get("volumeCbm") != null) {
			volumeCbm = node.get("volumeCbm").decimalValue();
		}
		
		String orderNumber = null;
		if (node.get("orderNumber") != null) {
			orderNumber = node.get("orderNumber").textValue();
		}
		
		
		Date dateUpdated = null;		
	    if (node.get("dateUpdated") != null) {
	    	String ddStr = node.get("dateUpdated").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		dateUpdated = df1.parse(ddStr);
				else
					dateUpdated = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
	    
	    Date dateCreated = null;		
	    if (node.get("dateCreated") != null) {
	    	String ddStr = node.get("dateCreated").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		dateCreated = df1.parse(ddStr);
				else
					dateCreated = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    } 
		
		OrderHeader order = new OrderHeader(orderId, branchSupplier, branch, orderStatus, company, party, scopeType, 
									null, userAdded, date, netWeight, volumeCbm, orderNumber, dateUpdated,dateCreated);
		
		List<OrderItem> orderItems = new ArrayList<>();
		if (node.get("orderItems") != null) {
			JsonNode itemsNode = node.get("orderItems"), tempNode;
			
			for (Iterator<JsonNode> i = itemsNode.iterator(); i.hasNext();) {
				OrderItem si = new OrderItem();
				
				JsonNode itemNode = i.next();

				Integer iiId = null;
				if (itemNode.get("id") != null) {
					iiId = itemNode.get("id").asInt();
					si.setId(iiId);
				}
				
				int id;
				if (itemNode.get("inventory") != null) {		
					tempNode = itemNode.get("inventory");
					id = tempNode.get("id").asInt();
					si.setInventory(new Inventory(id));
				}
				
				
				if (itemNode.get("unit") != null) {
					tempNode = itemNode.get("unit");
					String name = tempNode.get("name").asText();
					si.setUnit(new Unit(name));
				}
				
				BigDecimal quantity = null;
				if (itemNode.get("quantity") != null) {
					quantity = new BigDecimal(itemNode.get("quantity").asDouble());
					si.setQuantity(quantity);
				}
				
				
				BigDecimal unitCbm = null;
				if (itemNode.get("unitCbm") != null) {
					unitCbm = new BigDecimal(itemNode.get("unitCbm").asDouble());
					si.setUnitCbm(unitCbm);
				}
				
				BigDecimal unitKg = null;
				if (itemNode.get("unitKg") != null) {
					unitKg = new BigDecimal(itemNode.get("unitKg").asDouble());
					si.setUnitKg(unitKg);
				}
				
				
				
				si.setOrderInv(order);
				orderItems.add(si);
			}
			order.setOrderItems(orderItems);
		}
		return order;
	}


}
