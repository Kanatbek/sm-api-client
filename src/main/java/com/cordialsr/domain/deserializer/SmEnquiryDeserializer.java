package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.SmEnquiry;
import com.cordialsr.domain.SmService;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class SmEnquiryDeserializer extends JsonDeserializer<SmEnquiry> {

	@Override
	public SmEnquiry deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp), seNode;
	

	    DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
		
		Long id = null;
		if (node.get("id") != null) {
		    id = node.get("id").asLong();
		}
		    
		Company company = null;
		if (node.get("company") != null) {
         	seNode = node.get("company");
        	if (seNode.get("id") != null) {
		    	Integer cid = seNode.get("id").asInt();
		    	company = new Company(cid);
		    }
	    }
		    
		Branch branch = null;
		if (node.get("branch") != null) {
		  seNode = node.get("branch");
		  if (seNode.get("id") != null) {
		    Integer cid = seNode.get("id").asInt();
		    branch = new Branch(cid);
		  }
		}
		
		  Contract contract = null;
		  if (node.get("contract") != null) {
		   	seNode = node.get("contract");
		   	if (seNode.get("id") != null) {
	    		Long cid = seNode.get("id").asLong();
	    		contract = new Contract(cid);
	    	}
		  }
		  
		  
		  	Party party = null;
		    if (node.get("party") != null) {
		    	seNode = node.get("party");
		    	if (seNode.get("id") != null) {
		    		Long cid = seNode.get("id").asLong();
		    		party = new Party(cid);
		    	}
		    }
		    
		    Date edate = null;		
		    if (node.get("edate") != null) {
		    	String ddStr = node.get("edate").asText();
			    try {
			    	if (ddStr.length() > 10)
			    		edate = df1.parse(ddStr);
					else
						edate = df2.parse(ddStr);
				} catch (ParseException e) {
					e.printStackTrace();
				}
		    }
		    
		    Date etime = null;		
		    if (node.get("etime") != null) {
		    	String ddStr = node.get("etime").asText();
			    try {
			    	if (ddStr.length() > 10)
			    		etime = df1.parse(ddStr);
					else
						etime = df2.parse(ddStr);
				} catch (ParseException e) {
					e.printStackTrace();
				}
		    }
		    
		    
		    User user = null;
		    if (node.get("operator") != null) {
		    	seNode = node.get("operator");
		    	if (seNode.get("userid") != null) {
		    		Long cid = seNode.get("userid").asLong();
		    		user = new User(cid);
		    	}
		    }
		    
		    String fullFio = null;
		    if (node.get("fullFio") != null) {
		    	fullFio = node.get("fullFio").asText();
		    }
		    
		    String info = null;
		    if (node.get("info") != null) {
		    	info = node.get("info").asText();
		    }
		    
		    String phone = null;
		    if (node.get("phone") != null) {
		    	phone = node.get("phone").asText();
		    }
		
		    Integer etype = null;
		    if (node.get("etype") != null) {
		    	etype = node.get("etype").asInt();
		    }
		    
		    Integer status = null;
		    if (node.get("status") != null) {
		    	status = node.get("status").asInt();
		    }
		    
		    String enquiryNumber = null;
		    if (node.get("enquiryNumber") != null) {
		    	enquiryNumber = node.get("enquiryNumber").asText();
		    }
			
		    SmService smService = null;
		    if (node.get("smService") != null) {
		    	seNode = node.get("smService");
		    	if (seNode.get("id") != null) {
		    		Long cid = seNode.get("id").asLong();
		    		smService = new SmService(cid);
		    	}
		    }
		    SmEnquiry smEnquiry = new SmEnquiry(id, company, branch, contract, party,  
		    									edate, etime, user, info, fullFio, status, etype, phone, enquiryNumber, smService);
		    
		return smEnquiry;
	}

}
