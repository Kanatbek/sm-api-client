package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.cordialsr.domain.ContractType;
import com.cordialsr.domain.City;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Country;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.AwardCase;
import com.cordialsr.domain.AwardTemplate;
import com.cordialsr.domain.AwardType;
import com.cordialsr.domain.Branch;
import com.cordialsr.domain.InvMainCategory;
import com.cordialsr.domain.InvSubCategory;
import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.PaymentTemplate;
import com.cordialsr.domain.Position;
import com.cordialsr.domain.PriceList;
import com.cordialsr.domain.Region;
import com.cordialsr.domain.Scope;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PriceListDeserializer extends JsonDeserializer<PriceList> {

	@Override
	public PriceList deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);
		
		Integer id = null;
		if (node.get("id") != null) {
			id = node.get("id").asInt();
		}
		
		Company company = null;
		if (node.get("company") != null) {
			JsonNode prNode = node.get("company");
			if (prNode.get("id") != null) {
				Integer prId = prNode.get("id").asInt();
				company = new Company(prId);
			}
		}
		
		Branch branch = null;
		if (node.get("branch") != null) {
			JsonNode prNode = node.get("branch");
			if (prNode.get("id") != null) {
				Integer prId = prNode.get("id").asInt();
				branch = new Branch(prId);
			}
		}
		
		Country country = null;
		if (node.get("country") != null) {
			JsonNode prNode = node.get("country");
			if (prNode.get("id") != null) 
			{
				Integer prId = prNode.get("id").asInt();
				country = new Country(prId);
			}
		}
		
		City city = null;
		if (node.get("city") != null) {
			JsonNode prNode = node.get("city");
			if (prNode.get("id") != null) 
			{
				Integer prId = prNode.get("id").asInt();
				city = new City(prId);
			}
		}
		
		Region region = null;
		if (node.get("region") != null) {
			JsonNode prNode = node.get("region");
			if (prNode.get("id") != null) 
			{
				Integer prId = prNode.get("id").asInt();
				region = new Region(prId);
			}
		}

		ContractType contractType = null;
		if (node.get("contractType") != null) {
			JsonNode prNode = node.get("contractType");
			if (prNode.get("id") != null) 
				{
					Integer prId = prNode.get("id").asInt();
					contractType = new ContractType(prId);
				}
		}

		
		Inventory inventory = null;
		if (node.get("inventory") != null) {
			JsonNode prNode = node.get("inventory");
			if (prNode.get("id") != null) 	{
				Integer prId = prNode.get("id").asInt();
				inventory = new Inventory(prId);
			}
		}

		
		Scope scope = null;
		if (node.get("scope") != null) {
			JsonNode prNode = node.get("scope");
			if (prNode.get("name") != null)	{
				String prId = prNode.get("name").asText();
				scope = new Scope(prId);
			}
		}
		
		Currency currency = null;
		if (node.get("currency") != null) {
			JsonNode curNode = node.get("currency");
			if (curNode.get("currency") != null) {
				String cur = curNode.get("currency").asText();
				currency = new Currency(cur);
			}
		}

		
		
		
		//DATA
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");

		Date data = null;
		if (node.get("data") != null) {
			String ddStr = node.get("data").asText();
			try {
				if (ddStr.length() > 10)
					data = df1.parse(ddStr);
				else
					data = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		
		//price
		BigDecimal price = null;
		if (node.get("price") != null) {
			price = new BigDecimal(node.get("price").asDouble());
		}
		
		//month
		Integer month = null;
		if (node.get("month") != null) {
			month = new Integer(node.get("month").asInt());
		}
		
		//firstPayment
		BigDecimal firstPayment = null;
		if (node.get("firstPayment") != null) {
			firstPayment = new BigDecimal(node.get("firstPayment").asDouble());
		}
		//dealerDeposit
		BigDecimal dealerDeposit = null;
		if (node.get("dealerDeposit") != null) {
			dealerDeposit = new BigDecimal(node.get("dealerDeposit").asDouble());
		}
		
		//active
		Boolean active = null;
		if (node.get("active") != null) {
			active = new Boolean(node.get("active").asBoolean());
		}
		
		//isRent
		Boolean isRent = null;
		if (node.get("isRent") != null) {
			isRent = new Boolean(node.get("isRent").asBoolean());
		}
		
		//isSell
		Boolean isSell = null;
		if (node.get("isSell") != null) {
			isSell = new Boolean(node.get("isSell").asBoolean());
		}
				
		//forYur
		Boolean forYur = null;
		if (node.get("forYur") != null) {
			forYur = new Boolean(node.get("forYur").asBoolean());
		}
		
		//forFiz
		Boolean forFiz = null;
		if (node.get("forFiz") != null) {
			forFiz = new Boolean(node.get("forFiz").asBoolean());
		}
		
		

		PriceList prlist = new PriceList(id, branch,  city,  company,  contractType,  country,
				 inventory,  region,  scope,  data,  price, currency,
				 month,  dealerDeposit, firstPayment, active,
				  null,  null, isRent, isSell, forYur, forFiz);
		
	
		// Deserializing PriceListTemplate
		List<PaymentTemplate> paymentTemplate = new ArrayList<>();
		if (node.get("paymentTemplates") != null) {
			JsonNode itemsNode = node.get("paymentTemplates");

			for (Iterator<JsonNode> i = itemsNode.iterator(); i.hasNext();) {
				JsonNode itemNode = i.next();
				
				PriceList priceList = null;
				if (itemNode.get("priceList") != null) {
					JsonNode node3 = itemNode.get("priceList");
					if (node3.get("id") != null) {
						Integer tId = node3.get("id").asInt();
						priceList = new PriceList(tId);
					}
				}
				
				Integer paymentOrder = null;
				if (itemNode.get("paymentOrder") != null) {
					paymentOrder = new Integer(itemNode.get("paymentOrder").asInt());
				}
				
				Integer Tmonth = null;
				if (itemNode.get("month") != null) {
					Tmonth = new Integer(itemNode.get("month").asInt());
				}
				
				BigDecimal Tsumm = null;
				if (itemNode.get("summ") != null) {
					Tsumm = new BigDecimal(itemNode.get("summ").asDouble());
				}
				
				PaymentTemplate paymentT = new PaymentTemplate( priceList,  paymentOrder,  Tsumm,  Tmonth);
				paymentTemplate.add(paymentT);
			}
		}
			prlist.setPaymentTemplates(paymentTemplate);
		return prlist;
	}

}
