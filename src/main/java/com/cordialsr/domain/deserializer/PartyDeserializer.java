package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.sql.rowset.serial.SerialException;

import org.apache.tomcat.util.codec.binary.Base64;

import com.cordialsr.domain.Address;
import com.cordialsr.domain.AddressType;
import com.cordialsr.domain.City;
import com.cordialsr.domain.CityArea;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Country;
import com.cordialsr.domain.EducationLevel;
import com.cordialsr.domain.Oblast;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.PartyEducation;
import com.cordialsr.domain.PartyExperience;
import com.cordialsr.domain.PartyInfo;
import com.cordialsr.domain.PartyRelatives;
import com.cordialsr.domain.PartyType;
import com.cordialsr.domain.PcLevel;
import com.cordialsr.domain.PhoneNumber;
import com.cordialsr.domain.Photo;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class PartyDeserializer extends JsonDeserializer<Party> {

	@Override
	public Party deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp), ptNode;
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");

		Date birthday = null;
		if (node.get("birthday") != null) {
			String birthdayStr = node.get("birthday").asText();
			try {
				if (birthdayStr.length() > 10)
					birthday = df1.parse(birthdayStr);
				else if (birthdayStr.length() > 7)
					birthday = df2.parse(birthdayStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		Date dateIssue = null;
		if (node.get("dateIssue") != null) {
			String dateIssueStr = node.get("dateIssue").asText();
			try {
				if (dateIssueStr.length() > 10)
					dateIssue = df1.parse(dateIssueStr);
				else if (dateIssueStr.length() > 7)
					dateIssue = df2.parse(dateIssueStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		Date dateExpire = null;
		if (node.get("dateExpire") != null) {
			String dateExpireStr = node.get("dateExpire").asText();
			try {
				if (dateExpireStr.length() > 10)
					dateExpire = df1.parse(dateExpireStr);
				else if (dateExpireStr.length() > 7)
					dateExpire = df2.parse(dateExpireStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		Long partyid = null;
		if (node.get("id") != null)
			partyid = node.get("id").asLong();

		String firstName = null;
		if (node.get("firstname") != null)
			firstName = node.get("firstname").asText();

		String lastName = null;
		if (node.get("lastname") != null)
			lastName = node.get("lastname").asText();

		String middleName = null;
		if (node.get("middlename") != null)
			middleName = node.get("middlename").asText();

		String companyName = null;
		if (node.get("companyName") != null)
			companyName = node.get("companyName").asText();

		String iinBin = null;
		if (node.get("iinBin") != null)
			iinBin = node.get("iinBin").asText();

		String email = null;
		if (node.get("email") != null)
			email = node.get("email").asText();

		String passportNumber = null;
		if (node.get("passportNumber") != null)
			passportNumber = node.get("passportNumber").asText();

		boolean isYur = false;
		if (node.get("isYur") != null)
			isYur = (node.get("isYur").asBoolean());

		String issuedInstance = null;
		if (node.get("issuedInstance") != null)
			issuedInstance = node.get("issuedInstance").asText();

		String iik = null;
		if (node.get("iik") != null)
			iik = node.get("iik").asText();

		String bik = null;
		if (node.get("bik") != null)
			bik = node.get("bik").asText();

		Photo photoObject = null;
		if (node.get("photoString") != null && node.get("photoString").asText().length() > 0) {
			String photoString = node.get("photoString").asText();
			byte[] decodedByte = Base64.decodeBase64(photoString);
			photoObject = new Photo();
			try {
				Blob photoBlob = new javax.sql.rowset.serial.SerialBlob(decodedByte);
				photoObject.setPhoto(photoBlob);
			} catch (SerialException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		String info = null;
		if (node.get("info") != null)
			info = node.get("info").asText();

		String target = null;
		if (node.get("target") != null)
			target = node.get("target").asText();

		String familyStatus = null;
		if (node.get("familyStatus") != null)
			familyStatus = (node.get("familyStatus").asText());

		String birthPlace = null;
		if (node.get("birthPlace") != null)
			birthPlace = node.get("birthPlace").asText();

		

		Party original = null;
		if (node.get("original") != null) {
			ptNode = node.get("original");
			if (ptNode.get("id") != null) {
				long id = ptNode.get("id").asLong();
				original = new Party(id);
			}
		}
		
		User userCreated = null;
		if (node.get("userCreated") != null) {
			JsonNode usNode = node.get("userCreated");
			if (usNode.get("userid") != null) {
				Long getId = usNode.get("userid").asLong();
				userCreated = new User(getId);
			}

		}

		Party party = new Party(null, null, iinBin, firstName, lastName, middleName, email, birthday, isYur,
				passportNumber, dateIssue, dateExpire, issuedInstance, iik, bik, photoObject, null, null, null,
				info, target, null, null, familyStatus, birthPlace, null, original, null, null, userCreated);

		if (partyid != null && partyid > 0)
			party.setId(partyid);
		// if (version != null) party.setVersion(version);
		// if (version2 != null) party.setVersion2(version2);
		party.setCompanyName(companyName);

		PartyType partyType = new PartyType();
		System.out.println("Before: " + partyType.hashCode());
		if (node.get("partyType") != null) {
			ptNode = node.get("partyType");
			String description = ptNode.get("description").asText();
			String name = ptNode.get("name").asText();
			partyType.setName(name);
			partyType.setDescription(description);
			System.out.println("After: " + partyType.hashCode());
			party.setPartyType(partyType);
		}
		
		
//		PartyInfo partyInfo = new PartyInfo();
//		if (node.get("partyInfo") != null) {
//			ptNode = node.get("partyInfo");	
//			long id = ptNode.get("id").asLong();
//			partyInfo = new PartyInfo(id);
//			party.setPartyInfo(partyInfo);
//		}
		
		Country residency = null;
		if (node.get("residency") != null) {
			ptNode = node.get("residency");
			if (ptNode.get("id") != null) {
				int id = ptNode.get("id").asInt();
				residency = new Country(id);
				party.setResidency(residency);
			}
		}
		
		Company company = null;
		if (node.get("company") != null) {
			ptNode = node.get("company");
			if (ptNode.get("id") != null) {
				int id = ptNode.get("id").asInt();
				company = new Company(id);
				party.setCompany(company);
			}
		}

		// Set<KeySkill> keySkill = new HashSet<>();
		// if (node.get("keySkill") != null) {
		// JsonNode itemsNode = node.get("keySkill");
		//
		// for (Iterator<JsonNode> i = itemsNode.iterator(); i.hasNext();) {
		// KeySkill skill = new KeySkill();
		//
		// JsonNode itemNode = i.next();
		//
		// if (itemNode.get("id") != null) {
		// skill.setId(itemNode.get("id").asLong());
		// }
		//
		// if (itemNode.get("description") != null) {
		// skill.setDescription(itemNode.get("description").asText());
		// }
		//
		// keySkill.add(skill);
		// }
		// party.setKeySkill(keySkill);
		// }

		Set<PartyExperience> partyExperience = new HashSet<>();
		if (node.get("partyExperience") != null) {
			JsonNode itemsNode = node.get("partyExperience");

			for (Iterator<JsonNode> i = itemsNode.iterator(); i.hasNext();) {
				PartyExperience experience = new PartyExperience();

				JsonNode itemNode = i.next();

				if (itemNode.get("id") != null) {
					experience.setId(itemNode.get("id").asLong());
				}

				Party experienceParty = null;
				if (itemNode.get("party") != null) {
					ptNode = itemNode.get("party");
					if (ptNode.get("id") != null) {
						Long id = ptNode.get("id").asLong();
						experienceParty = new Party(id);
						experience.setParty(experienceParty);
					}
				}

				if (itemNode.get("company") != null)
					experience.setCompany(itemNode.get("company").asText());

				if (itemNode.get("position") != null)
					experience.setPosition(itemNode.get("position").asText());

				if (itemNode.get("duties") != null) {
					experience.setDuties(itemNode.get("duties").asText());
				}

				if (itemNode.get("reason") != null) {
					experience.setReason(itemNode.get("reason").asText());
				}

				Date dateHired = null;
				if (itemNode.get("dateHired") != null) {
					String date = itemNode.get("dateHired").asText();
					try {
						if (date.length() > 10) {
							dateHired = df1.parse(date);
							experience.setDateHired(dateHired);
						} else if (date.length() > 7) {
							dateHired = df2.parse(date);
							experience.setDateHired(dateHired);
						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}

				Date dateFired = null;
				if (itemNode.get("dateFired") != null) {
					String date = itemNode.get("dateFired").asText();
					try {
						if (date.length() > 10) {
							dateFired = df1.parse(date);
							experience.setDateFired(dateFired);
						} else if (date.length() > 7) {
							dateFired = df2.parse(date);
							experience.setDateFired(dateFired);
						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}

				partyExperience.add(experience);
			}
			party.setPartyExperience(partyExperience);
		}
		
		Set<PartyRelatives> partyRelatives = new HashSet<>();
		if (node.get("partyRelatives") != null) {
			JsonNode itemsNode = node.get("partyRelatives");

			for (Iterator<JsonNode> i = itemsNode.iterator(); i.hasNext();) {
				PartyRelatives relatives = new PartyRelatives();

				JsonNode itemNode = i.next();

				if (itemNode.get("id") != null) {
					relatives.setId(itemNode.get("id").asLong());
				}

				Party relativeParty = null;
				if (itemNode.get("party") != null) {
					ptNode = itemNode.get("party");
					if (ptNode.get("id") != null) {
						Long id = ptNode.get("id").asLong();
						relativeParty = new Party(id);
						relatives.setParty(relativeParty);
					}
				}

				if (itemNode.get("level") != null) {
					relatives.setLevel(itemNode.get("level").asText());
				}

				if (itemNode.get("firstname") != null) {
					relatives.setFirstname(itemNode.get("firstname").asText());
				}
				
				if (itemNode.get("lastname") != null) {
					relatives.setLastname(itemNode.get("lastname").asText());
				}
				
				
				
				String phone = null;
				if (node.get("phone") != null) {
					relatives.setPhone(phone);
				}
				
				if (itemNode.get("jobPlace") != null) {
					relatives.setJobPlace(itemNode.get("jobPlace").asText());
				}
				
				if (itemNode.get("position") != null) {
					relatives.setPosition(itemNode.get("position").asText());
				}
				
				Date birthDate = null;
				if (itemNode.get("birthDate") != null) {
					String date = itemNode.get("birthDate").asText();
					try {
						if (date.length() > 10) {
							birthDate = df1.parse(date);
							relatives.setBirthDate(birthDate);
						} else if (date.length() > 7) {
							birthDate = df2.parse(date);
							relatives.setBirthDate(birthDate);
						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				
				

				partyRelatives.add(relatives);
			}
			party.setPartyRelatives(partyRelatives);
		}
		
		
		PartyInfo partyInfo = new PartyInfo();
		if (node.get("partyInfo") != null) {
			JsonNode itemsNode = node.get("partyInfo");


				if (itemsNode.get("id") != null) {
					partyInfo.setId(itemsNode.get("id").asLong());
				}

				Party infoParty = null;
				if (itemsNode.get("party") != null) {
					ptNode = itemsNode.get("party");
					if (ptNode.get("id") != null) {
						Long id = ptNode.get("id").asLong();
						infoParty = new Party(id);
						partyInfo.setParty(infoParty);
					}
				}

				if (itemsNode.get("addressResidency") != null) {
					partyInfo.setAddressResidency(itemsNode.get("addressResidency").asText());
				}

				if (itemsNode.get("conviction") != null) {
					partyInfo.setConviction(itemsNode.get("conviction").asBoolean());
				}
				
				if (itemsNode.get("advisorFirstname") != null) {
					partyInfo.setAdvisorFirstname(itemsNode.get("advisorFirstname").asText());
				}
				
				if (itemsNode.get("advisorLastname") != null) {
					partyInfo.setAdvisorLastname(itemsNode.get("advisorLastname").asText());
				}
				
				
				
				PhoneNumber infoAdvisorPhone = null;
				if (itemsNode.get("advisorPhone") != null) {
					ptNode = itemsNode.get("advisorPhone");
					if (ptNode.get("phNumber") != null) {
						String phNumber = ptNode.get("phNumber").asText();
						infoAdvisorPhone = new PhoneNumber(phNumber);
						infoAdvisorPhone.setIsmain(ptNode.get("ismain").asBoolean());
						infoAdvisorPhone.setIsmobile(ptNode.get("ismobile").asBoolean());
						partyInfo.setAdvisorPhone(infoAdvisorPhone);
					}
				}
				
				if (itemsNode.get("driverLicense") != null) {
					partyInfo.setDriverLicense(itemsNode.get("driverLicense").asText());
				}
				
				
				Date infoDateSign = null;
				if (itemsNode.get("dateSign") != null) {
					String date = itemsNode.get("DateSign").asText();
					try {
						if (date.length() > 10) {
							infoDateSign = df1.parse(date);
							partyInfo.setDateSign(infoDateSign);
						} else if (date.length() > 7) {
							infoDateSign = df2.parse(date);
							partyInfo.setDateSign(infoDateSign);
						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				
				PcLevel infoPcLevel = null;
				if (itemsNode.get("pcLevel") != null) {
					ptNode = itemsNode.get("pcLevel");
					if (ptNode.get("id") != null) {
						Integer id = ptNode.get("id").asInt();
						infoPcLevel = new PcLevel(id);
						partyInfo.setPcLevel(infoPcLevel);
					}
				}
				

				Boolean gender = null;
				if (itemsNode.get("gender") != null)
				{
					gender = (itemsNode.get("gender").asBoolean());
					partyInfo.setGender(gender);
				}
				
				Integer children = null;
				if (itemsNode.get("children") != null)
				{
					children = (itemsNode.get("children").asInt());
					partyInfo.setChildren(children);
				}
				
			party.setPartyInfo(partyInfo);
		}
		
		
		Set<PartyEducation> partyEducation = new HashSet<>();
		if (node.get("partyEducation") != null) {
			JsonNode itemsNode = node.get("partyEducation");

			for (Iterator<JsonNode> i = itemsNode.iterator(); i.hasNext();) {
				PartyEducation education = new PartyEducation();

				JsonNode itemNode = i.next();

				if (itemNode.get("id") != null) {
					education.setId(itemNode.get("id").asLong());
				}

				EducationLevel educationLevel = null;
				if (node.get("level") != null) {
					ptNode = node.get("level");
					if (ptNode.get("id") != null) {
						Integer id = ptNode.get("id").asInt();
						educationLevel = new EducationLevel(id);
						education.setLevel(educationLevel);
					}
				}

				Party educationParty = null;
				if (node.get("party") != null) {
					ptNode = node.get("party");
					if (ptNode.get("id") != null) {
						Long id = ptNode.get("id").asLong();
						educationParty = new Party(id);
						education.setParty(educationParty);
					}
				}

				if (node.get("isAcademic") != null) {
					education.setIsAcademic(itemNode.get("isAcademic").asBoolean());
				}

				if (node.get("academy") != null)
					education.setAcademy(itemNode.get("academy").asText());

				if (node.get("faculty") != null)
					education.setFaculty(itemNode.get("faculty").asText());

				if (node.get("specialization") != null) {
					education.setSpecialization(node.get("specialization").asText());
				}

				Date dateStart = null;
				if (node.get("dateStart") != null) {
					String date = node.get("dateStart").asText();
					try {
						if (date.length() > 10) {
							dateStart = df1.parse(date);
							education.setDateStart(dateStart);
						} else if (date.length() > 7) {
							dateStart = df2.parse(date);
							education.setDateStart(dateStart);
						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}

				Date dateEnd = null;
				if (node.get("dateEnd") != null) {
					String date = node.get("dateEnd").asText();
					try {
						if (date.length() > 10) {
							dateEnd = df1.parse(date);
							education.setDateEnd(dateEnd);
						} else if (date.length() > 7) {
							dateEnd = df2.parse(date);
							education.setDateEnd(dateEnd);
						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}

				partyEducation.add(education);
			}
			party.setPartyEducation(partyEducation);
		}

		Set<Address> addresses = new HashSet<>();
		if (node.get("addresses") != null) {
			JsonNode itemsNode = node.get("addresses"), tempNode;

			for (Iterator<JsonNode> i = itemsNode.iterator(); i.hasNext();) {
				Address si = new Address();

				JsonNode itemNode = i.next();

				if (itemNode.get("addressType") != null) {
					tempNode = itemNode.get("addressType");
					if (tempNode.get("id") != null) {
						int id = tempNode.get("id").asInt();
						si.setAddressType(new AddressType(id));
					}
				}

				if (itemNode.get("country") != null) {
					tempNode = itemNode.get("country");
					if (tempNode.get("id") != null) {
						int id = tempNode.get("id").asInt();
						si.setCountry(new Country(id));
					}
				}

				if (itemNode.get("oblast") != null) {
					tempNode = itemNode.get("oblast");
					if (tempNode.get("id") != null) {
						int id = tempNode.get("id").asInt();
						String name = tempNode.get("name").asText();
						si.setOblast(new Oblast(id));
					}
				}

				if (itemNode.get("city") != null) {
					tempNode = itemNode.get("city");
					if (tempNode.get("id") != null) {
						int cityId = tempNode.get("id").asInt();
						si.setCity(new City(cityId));
					}
				}

				if (itemNode.get("cityArea") != null) {
					tempNode = itemNode.get("cityArea");
					if (tempNode.get("id") != null) {
						int id = tempNode.get("id").asInt();
						si.setCityArea(new CityArea(id));
					}
				}

				if (itemNode.get("district") != null) {
					si.setDistrict(itemNode.get("district").asText());
				}

				if (itemNode.get("street") != null) {
					si.setStreet(itemNode.get("street").asText());
				}

				if (itemNode.get("houseNumber") != null) {
					si.setHouseNumber(itemNode.get("houseNumber").asText());
				}

				if (itemNode.get("flatNumber") != null) {
					si.setFlatNumber(itemNode.get("flatNumber").asText());
				}

				if (itemNode.get("zipCode") != null) {
					si.setZipCode(itemNode.get("zipCode").asText());
				}

				if (itemNode.get("info") != null) {
					si.setInfo(itemNode.get("info").asText());
				}

				if (itemNode.get("id") != null) {
					si.setId(itemNode.get("id").asLong());
				}

				si.setParty(party);
				addresses.add(si);
			}
			party.setAddresses(addresses);
		}

		Set<PhoneNumber> phoneNumbers = new HashSet<>();
		if (node.get("phoneNumbers") != null) {
			JsonNode itemsNode = node.get("phoneNumbers");

			for (Iterator<JsonNode> i = itemsNode.iterator(); i.hasNext();) {
				PhoneNumber pn = new PhoneNumber();

				JsonNode itemNode = i.next();
				if (itemNode.get("phNumber") != null) {
					pn.setPhNumber(itemNode.get("phNumber").asText());
				}

				if (itemNode.get("ismobile") != null) {
					pn.setIsmobile(itemNode.get("ismobile").asBoolean());
				}

				if (itemNode.get("ismain") != null) {
					pn.setIsmain(itemNode.get("ismain").asBoolean());
				}

				if (itemNode.get("id") != null) {
					pn.setId(itemNode.get("id").asLong());
				}

				pn.setParty(party);
				phoneNumbers.add(pn);
			}

			party.setPhoneNumbers(phoneNumbers);
		}

		return party;
	}

}
