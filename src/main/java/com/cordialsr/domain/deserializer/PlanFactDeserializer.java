package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;

import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.ParseException;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Department;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.PlanFact;
import com.cordialsr.domain.Position;
import com.cordialsr.domain.Region;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class PlanFactDeserializer extends JsonDeserializer<PlanFact> {

	@Override
	public PlanFact deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		ObjectCodec oc = jp.getCodec();
	    JsonNode node = oc.readTree(jp), ptNode;
	    DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
	    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
		
		Integer conId = null;
		if (node.get("id") != null) {
			conId = node.get("id").asInt();
		}
		
		int year = 0;
		if (node.get("year") != null)
			year = node.get("year").asInt();
		
		int month = 0;
		if (node.get("month") != null) {
			month = node.get("month").asInt();
		}
		Company company = null;
		if(node.get("company") != null) {
			ptNode = node.get("company");
			if (ptNode.get("id") != null) 
			{
				int id = ptNode.get("id").asInt();
				company = new Company(id);
			}
		}
		
		Department department = null;
		if(node.get("department") != null) {
			ptNode = node.get("department");
			if (ptNode.get("id") != null) 
			{
				int id = ptNode.get("id").asInt();
				department = new Department(id);
			}
		}

		Region region = null;
		if(node.get("region") != null) {
			ptNode = node.get("region");
			if (ptNode.get("id") != null) 
			{
				int id = ptNode.get("id").asInt();
				region = new Region(id);
			}
		}
		
		Branch branch = null;
		if(node.get("branch") != null) {
			ptNode = node.get("branch");
			if (ptNode.get("id") != null) 
			{
				int id = ptNode.get("id").asInt();
				branch = new Branch(id);
			}
		}
		

		Position position = null;
		if(node.get("position") != null) {
			ptNode = node.get("position");
			if (ptNode.get("id") != null) 
			{
				int id = ptNode.get("id").asInt();
				position = new Position(id);
			}
		}
		
		
		Party party = null;
		if(node.get("party") != null) {
			ptNode = node.get("party");
			if (ptNode.get("id") != null) 
			{
				Long id = ptNode.get("id").asLong();
				party = new Party(id);
			}
		}
		
		BigDecimal plan = null;
		if (node.get("plan") != null) {
			plan = new BigDecimal(node.get("plan").asDouble());
		}
		
		BigDecimal fact = null;
		if (node.get("fact") != null) {
			fact = new BigDecimal(node.get("fact").asDouble());
		}
		
		Boolean isOverdue = null;
		if (node.get("isOverdue") != null) {
			isOverdue = new Boolean(node.get("isOverdue").asBoolean());
		}
		
		Date cpuDate = null;		
	    if (node.get("cpuDate") != null) {
	    	String dateIssueStr = node.get("cpuDate").asText();
		    try {
		    	if (dateIssueStr.length() > 10)
		    		cpuDate = df1.parse(dateIssueStr);
		    	else if (dateIssueStr.length() > 7)
		    		cpuDate = df2.parse(dateIssueStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
	    
	    Date updatedDate = null;		
	    if (node.get("updatedDate") != null) {
	    	String dateIssueStr = node.get("updatedDate").asText();
		    try {
		    	if (dateIssueStr.length() > 10)
		    		updatedDate = df1.parse(dateIssueStr);
		    	else if (dateIssueStr.length() > 7)
		    		updatedDate = df2.parse(dateIssueStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
		
	    Currency currency = null;
		if(node.get("currency") != null) {
			JsonNode ctNode = node.get("currency");
			if (ctNode.get("currency") != null) {
				String currencyStr = ctNode.get("currency").asText();
				currency = new Currency(currencyStr);
			}
		} 
		
		User userCreated = null;
		if (node.get("userCreated") != null) {
			JsonNode usNode = node.get("userCreated");
			if (usNode.get("userid") != null) {
				Long getId = usNode.get("userid").asLong();
				userCreated = new User(getId);
			}

		}
		
		User updatedUser = null;
		if (node.get("updatedUser") != null) {
			JsonNode usNode = node.get("updatedUser");
			if (usNode.get("userid") != null) {
				Long getId = usNode.get("userid").asLong();
				updatedUser = new User(getId);
			}

		}
		
		PlanFact pf = new PlanFact(conId,  year,  month,  company,  department,  region,
				 branch,  position,  party,  plan,  fact,  cpuDate,
				 currency, userCreated, updatedDate, updatedUser, isOverdue);
		
		return pf;
	}
	

}
