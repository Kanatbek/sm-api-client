package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.cordialsr.domain.Party;
import com.cordialsr.domain.PartyExperience;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class PartyExperienceDeserializer extends JsonDeserializer<PartyExperience>{

	@Override
	public PartyExperience deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		
		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp), ptNode;
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
		
		Long exId = null;
		if (node.get("id") != null) {
			exId = (node.get("id").asLong());
		}

		Party experienceParty = null;
		if (node.get("party") != null) {
			ptNode = node.get("party");
			if (ptNode.get("id") != null) {
				Long id = ptNode.get("id").asLong();
				experienceParty = new Party(id);
			}
		}

		String exCompany = "";
		if (node.get("company") != null)
			exCompany = (node.get("company").asText());

		String exPosition = "";
		if (node.get("position") != null)
			exPosition = (node.get("position").asText());

		String exDuties = "";
		if (node.get("duties") != null) {
			exDuties = (node.get("duties").asText());
		}

		String exReason = "";
		if (node.get("reason") != null) {
			exReason = (node.get("reason").asText());
		}

		Date dateHired = null;
		if (node.get("dateHired") != null) {
			String date = node.get("dateHired").asText();
			try {
				if (date.length() > 10) {
					dateHired = df1.parse(date);
				} else if (date.length() > 7) {
					dateHired = df2.parse(date);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		Date dateFired = null;
		if (node.get("dateFired") != null) {
			String date = node.get("dateFired").asText();
			try {
				if (date.length() > 10) {
					dateFired = df1.parse(date);
				} else if (date.length() > 7) {
					dateFired = df2.parse(date);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		PartyExperience pExperience = new PartyExperience(exId,experienceParty, exCompany, exPosition, dateHired, dateFired, exDuties, exReason);
		
		return pExperience;
	}

}
