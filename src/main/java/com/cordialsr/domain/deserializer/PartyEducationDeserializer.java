package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.cordialsr.domain.CrmLead;
import com.cordialsr.domain.EducationLevel;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.PartyEducation;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class PartyEducationDeserializer extends JsonDeserializer<PartyEducation>{
	@Override
	public PartyEducation deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");

		
		Long id = null;
		if (node.get("id") != null) {
			id = (node.get("id").asLong());
		}

		EducationLevel educationLevel = null;
		if (node.get("level") != null) {
			JsonNode ptNode = node.get("level");
			if (ptNode.get("id") != null) {
				Integer iid = ptNode.get("id").asInt();
				educationLevel = new EducationLevel(iid);
			}
		}

		Party educationParty = null;
		if (node.get("party") != null) {
			JsonNode ptNode = node.get("party");
			if (ptNode.get("id") != null) {
				Long iid = ptNode.get("id").asLong();
				educationParty = new Party(iid);
			}
		}

		Boolean isAcademic = null;
		if (node.get("isAcademic") != null) {
			isAcademic = (node.get("isAcademic").asBoolean());
		}

		String academy = null;
		if (node.get("academy") != null)
			academy = (node.get("academy").asText());

		String faculty = null;
		if (node.get("faculty") != null)
			faculty = (node.get("faculty").asText());

		String specialization = null;
		if (node.get("specialization") != null) {
			specialization = (node.get("specialization").asText());
		}

		Date dateStart = null;
		if (node.get("dateStart") != null) {
			String date = node.get("dateStart").asText();
			try {
				if (date.length() > 10) {
					dateStart = df1.parse(date);
					
				} else if (date.length() > 7) {
					dateStart = df2.parse(date);
					
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		Date dateEnd = null;
		if (node.get("dateEnd") != null) {
			String date = node.get("dateEnd").asText();
			try {
				if (date.length() > 10) {
					dateEnd = df1.parse(date);
				} else if (date.length() > 7) {
					dateEnd = df2.parse(date);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		PartyEducation education = new PartyEducation(id, educationParty, educationLevel, academy, 
				  faculty, specialization, dateStart, dateEnd, isAcademic);
		
		return education;
		
	}
}
