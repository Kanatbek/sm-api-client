package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.cordialsr.domain.Contract;
import com.cordialsr.domain.SmContract;
import com.cordialsr.domain.SmFilterChange;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(using = SmFilterChangeDeserializer.class)
public class SmFilterChangeDeserializer extends JsonDeserializer<SmFilterChange> { 
 
	 @Override
	 public SmFilterChange deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
	    ObjectCodec oc = jp.getCodec();
	    JsonNode node = oc.readTree(jp), node2;
	    
	    DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
	    
	    Integer id = null;
	    if (node.get("id") != null) {
	    	id = node.get("id").asInt();
	    }
		Contract contract = null;
		if (node.get("contract") != null) {
			node2 = node.get("contract");
			if (node2.get("id") != null) {
				contract = new Contract(node2.get("id").asLong());
			}
		}
		String contractNumber = null;
		if (node.get("contractNumber") != null) {
			contractNumber = node.get("contractNumber").asText();
	    }
		
		Integer f1Mt = null;
		if (node.get("f1Mt") != null) {
			f1Mt = node.get("f1Mt").asInt();
	    }
		
		Date f1Prev = null;
		if (node.get("f1Prev") != null) {
	    	String ddStr = node.get("f1Prev").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		f1Prev = df1.parse(ddStr);
				else
					f1Prev = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
	    
		Date f1Last = null;
		if (node.get("f1Last") != null) {
	    	String ddStr = node.get("f1Last").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		f1Last = df1.parse(ddStr);
				else
					f1Last = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
		Date f1Next = null;
		if (node.get("f1Next") != null) {
	    	String ddStr = node.get("f1Next").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		f1Next = df1.parse(ddStr);
				else
					f1Next = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
		
		Integer f2Mt = null;
		if (node.get("f2Mt") != null) {
			f2Mt = node.get("f2Mt").asInt();
	    }
		
		
		Date f2Prev = null;
		if (node.get("f2Prev") != null) {
	    	String ddStr = node.get("f2Prev").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		f2Prev = df1.parse(ddStr);
				else
					f2Prev = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
		Date f2Last = null;
		if (node.get("f2Last") != null) {
	    	String ddStr = node.get("f2Last").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		f2Last = df1.parse(ddStr);
				else
					f2Last = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
		Date f2Next = null;
		if (node.get("f2Next") != null) {
	    	String ddStr = node.get("f2Next").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		f2Next = df1.parse(ddStr);
				else
					f2Next = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
		
		Integer f3Mt = null;
		if (node.get("f3Mt") != null) {
			f3Mt = node.get("f3Mt").asInt();
	    }
		Date f3Prev = null;
		if (node.get("f3Prev") != null) {
	    	String ddStr = node.get("f3Prev").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		f3Prev = df1.parse(ddStr);
				else
					f3Prev = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
		Date f3Last = null;
		if (node.get("f3Last") != null) {
	    	String ddStr = node.get("f3Last").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		f3Last = df1.parse(ddStr);
				else
					f3Last = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
		Date f3Next = null;
		if (node.get("f3Next") != null) {
	    	String ddStr = node.get("f3Next").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		f3Next = df1.parse(ddStr);
				else
					f3Next = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
		
		Integer f4Mt = null;
		if (node.get("f4Mt") != null) {
			f4Mt = node.get("f4Mt").asInt();
	    }
		Date f4Prev = null;
		if (node.get("f4Prev") != null) {
	    	String ddStr = node.get("f4Prev").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		f4Prev = df1.parse(ddStr);
				else
					f4Prev = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
		Date f4Last = null;
		if (node.get("f4Last") != null) {
	    	String ddStr = node.get("f4Last").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		f4Last = df1.parse(ddStr);
				else
					f4Last = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
		Date f4Next = null;
		if (node.get("f4Next") != null) {
	    	String ddStr = node.get("f4Next").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		f4Next = df1.parse(ddStr);
				else
					f4Next = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
		
		Integer f5Mt = null;
		if (node.get("f5Mt") != null) {
			f5Mt = node.get("f5Mt").asInt();
	    }
		Date f5Prev = null;
		if (node.get("f5Prev") != null) {
	    	String ddStr = node.get("f5Prev").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		f5Prev = df1.parse(ddStr);
				else
					f5Prev = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
		Date f5Last = null;
		if (node.get("f5Last") != null) {
	    	String ddStr = node.get("f5Last").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		f5Last = df1.parse(ddStr);
				else
					f5Last = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
		Date f5Next = null;
		if (node.get("f5Next") != null) {
	    	String ddStr = node.get("f5Next").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		f5Next = df1.parse(ddStr);
				else
					f5Next = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
		
		Integer prMt = null;
		if (node.get("prMt") != null) {
			prMt = node.get("prMt").asInt();
	    }
		Date prPrev = null;
		if (node.get("prPrev") != null) {
	    	String ddStr = node.get("prPrev").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		prPrev = df1.parse(ddStr);
				else
					prPrev = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
		Date prLast = null;
		if (node.get("prLast") != null) {
	    	String ddStr = node.get("prLast").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		prLast = df1.parse(ddStr);
				else
					prLast = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
		Date prNext = null;
		if (node.get("prNext") != null) {
	    	String ddStr = node.get("prNext").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		prNext = df1.parse(ddStr);
				else
					prNext = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
		
		Boolean enabled = null;
		if (node.get("enabled") != null) {
			enabled = node.get("enabled").asBoolean();
	    }

	  
		SmFilterChange filter = new SmFilterChange(id, contract, contractNumber, f1Mt, f1Prev, f1Last, 
				f1Next, f2Mt, f2Prev, f2Last, f2Next, f3Mt, f3Prev, f3Last, f3Next, f4Mt, f4Prev, f4Last, 
				f4Next, f5Mt, f5Prev, f5Last, f5Next, prMt, prPrev, prLast, prNext, enabled); 
		
	    return filter;
	 }
	
}
