package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.SmVisit;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class SmVisitDeserializer extends JsonDeserializer<SmVisit> {

	@Override
	public SmVisit deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp), seNode;
	

	    DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
		
		Long id = null;
		if (node.get("id") != null) {
		    id = node.get("id").asLong();
		}
		    
		Employee employee = null;
		if (node.get("employee") != null) {
         	seNode = node.get("employee");
        	if (seNode.get("id") != null) {
		    	Long emplId = seNode.get("id").asLong();
		    	employee = new Employee(emplId);
		    }
	    }
		    
		Party party = null;
		if (node.get("party") != null) {
		  seNode = node.get("party");
		  if (seNode.get("id") != null) {
		    Long partyId = seNode.get("id").asLong();
		    party = new Party(partyId);
		  }
		}
		
		Date inDate = null;		
	    if (node.get("inDate") != null) {
	    	String sDateStr = node.get("inDate").asText();
		    try {
		    	if (sDateStr.length() > 10)
		    		inDate = df1.parse(sDateStr);
				else if (sDateStr.length() > 7)
					inDate = df2.parse(sDateStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
	    
	    
	    Date inTime = null;		
	    if (node.get("inTime") != null) {
	    	String sDateStr = node.get("inTime").asText();
		    try {
		    	if (sDateStr.length() > 10)
		    		inTime = df1.parse(sDateStr);
				else if (sDateStr.length() > 7)
					inTime = df2.parse(sDateStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
	    
	    
	    Date outTime = null;		
	    if (node.get("outTime") != null) {
	    	String sDateStr = node.get("outTime").asText();
		    try {
		    	if (sDateStr.length() > 10)
		    		outTime = df1.parse(sDateStr);
				else if (sDateStr.length() > 7)
					outTime = df2.parse(sDateStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
		
	    BigDecimal longitude = null;
	    if (node.get("longitude") != null) {
	    	longitude = new BigDecimal(node.get("longitude").asDouble());
	    }
	    
	    BigDecimal latitude = null;
	    if (node.get("latitude") != null) {
	    	latitude = new BigDecimal(node.get("latitude").asDouble());
	    }
		    
		    SmVisit smVisit = new SmVisit(id, employee, party, inTime, outTime, longitude, latitude, inDate);
		    
		return smVisit;
	}

}