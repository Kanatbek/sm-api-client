package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;

import javax.sql.rowset.serial.SerialException;

import org.apache.tomcat.util.codec.binary.Base64;

import com.cordialsr.domain.Photo;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PhotoDeserializer extends JsonDeserializer<Photo> {

	@Override
	 public Photo deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
	    ObjectCodec oc = jp.getCodec();
	    JsonNode node = oc.readTree(jp), ptNode;
	    ObjectMapper jsonObjectMapper = new ObjectMapper();
	    
	    Photo ph = null;
		
	    Long id = new Long(0);
		if (node.get("id") != null) { 
			id = node.get("id").asLong();
			ph = new Photo();
		}
	    
		Blob photo = null;
		if (node.get("photo") != null) {
			JsonNode pNode = node.get("photo");
			if (pNode.get("id") != null) {
				photo = ((Blob) pNode.get("photo"));
			}
			ph = new Photo();
		}
		
		ph.setId(id);
	    ph.setPhoto(photo);
	    return ph;
	}
}
