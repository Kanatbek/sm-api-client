package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Incoterm;
import com.cordialsr.domain.InvItemSn;
import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.InvoiceItem;
import com.cordialsr.domain.InvoiceStatus;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.ScopeType;
import com.cordialsr.domain.StockIn;
import com.cordialsr.domain.StockOut;
import com.cordialsr.domain.StockStatus;
import com.cordialsr.domain.Unit;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class InvoiceDeserializer extends JsonDeserializer<Invoice> {

	@Override
	public Invoice deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp), ptNode;
		
		Long invId = null;
		if (node.get("id") != null) {
			invId = node.get("id").asLong();			
		}
		
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");

		String invoiceNumber = "";
		if (node.get("invoiceNumber") != null) 
			invoiceNumber = node.get("invoiceNumber").asText();
		
		Date date = null;		
	    if (node.get("data") != null) {
	    	String ddStr = node.get("data").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		date = df1.parse(ddStr);
				else
					date = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
	    
	    Calendar dateUpdated = null;		
	    if (node.get("dateUpdated") != null) {
	    	String ddStr = node.get("dateUpdated").asText();
		    try {
		    	if (ddStr.length() > 10) {
		    		dateUpdated = Calendar.getInstance();
		    		dateUpdated.setTime(df1.parse(ddStr));
		    	} else if (ddStr.length() > 7) {
					dateUpdated = Calendar.getInstance();
		    		dateUpdated.setTime(df2.parse(ddStr));
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
	    
	    Calendar dateCreated = null;		
	    if (node.get("dateCreated") != null) {
	    	String ddStr = node.get("dateCreated").asText();
		    try {
		    	if (ddStr.length() > 10) {
		    		dateCreated = Calendar.getInstance();
		    		dateCreated.setTime(df1.parse(ddStr));
		    	} else if (ddStr.length() > 7) {
		    		dateCreated = Calendar.getInstance();
		    		dateCreated.setTime(df2.parse(ddStr));
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    } 
		
		BigDecimal cost = null;
		if (node.get("cost") != null) {
			cost = new BigDecimal(node.get("cost").asDouble());
		}
		
		BigDecimal taxSum = null;
		if (node.get("taxSum") != null) {
			taxSum = new BigDecimal(node.get("taxSum").asDouble());
		}
		
		BigDecimal taxRate = null;
		if (node.get("taxRate") != null) {
			taxRate = new BigDecimal(node.get("taxRate").asDouble());
		}
		
		BigDecimal totalCost = null;
		if (node.get("totalCost") != null) {
			totalCost = new BigDecimal(node.get("totalCost").asDouble());
		}
		
		Company company = null;
		if(node.get("company") != null) {
			ptNode = node.get("company");
			if (ptNode.get("id") != null) {
				Integer id = ptNode.get("id").asInt();
				company = new Company(id);
			}
		} 
		
		Branch branch = null;
		if(node.get("branch") != null) {
			ptNode = node.get("branch");
			if (ptNode.get("id") != null) {
				Integer id = ptNode.get("id").asInt();
				branch = new Branch(id);
			}
		} 
		
		Branch branchImporter = null;
		if(node.get("branchImporter") != null) {
			ptNode = node.get("branchImporter");
			if (ptNode.get("id") != null) {
				int id = ptNode.get("id").asInt();
				branchImporter = new Branch(id);
			}
		} 
	
		Currency currency = null;
		if(node.get("currency") != null) {
			ptNode = node.get("currency");
			if (ptNode.get("currency") != null) {
				currency = new Currency();
				String currencyStr = ptNode.get("currency").asText();
				currency.setCurrency(currencyStr);
			}	
		} 
		
		Party bankPartner = null;
		if(node.get("bankPartner") != null) {
			ptNode = node.get("bankPartner");
			if (ptNode.get("id") != null) {
				bankPartner = new Party();
				Long id = ptNode.get("id").asLong();
				bankPartner.setId(id);
			}
		} 
		
		Incoterm incoterm = null;
		if((node.get("incoterm") != null)) {
			ptNode = node.get("incoterm");
			if(ptNode.get("code") != null) 
			{
				incoterm = new Incoterm();
				String code = ptNode.get("code").asText();
				incoterm.setCode(code);
			}
		} 
		
		InvoiceStatus invoiceStatus = null;
		if(node.get("invoiceStatus") != null) {
			invoiceStatus = new InvoiceStatus();
			ptNode = node.get("invoiceStatus");
			if(ptNode.get("name") != null) 
			{
				String name = ptNode.get("name").asText();
				invoiceStatus.setName(name);
			}
		}			
		
		Party party = null;
		if(node.get("party") != null) {
			ptNode = node.get("party");
			if (ptNode.get("id") != null) {
				party = new Party();
				Long id = ptNode.get("id").asLong();
				party.setId(id);
			}
		} 
			
		ScopeType scopeType = null;
		if(node.get("scopeType") != null) {
			ptNode = node.get("scopeType");
			if (ptNode.get("typeCode") != null) {
				scopeType = new ScopeType();
				String typeCode = ptNode.get("typeCode").asText();
				scopeType.setTypeCode(typeCode);
			}
		}
		
		Integer invoiceType = null;
		if(node.get("invoiceType") != null) {
			invoiceType = node.get("invoiceType").asInt();
		} 
		
		User userAdded = null;
		if(node.get("userAdded") != null) {
			ptNode = node.get("userAdded");
			if (ptNode.get("userid") != null) {
				Long id = ptNode.get("userid").asLong();
				userAdded = new User(id);
			}
		}
		
		User userUpdated = null;
		if(node.get("userUpdated") != null) {
			ptNode = node.get("userUpdated");
			if (ptNode.get("userid") != null) {
				Long id = ptNode.get("userid").asLong();
				userUpdated = new User(id);
			}
		} 
			
		Long docId = null;
		if(node.get("docId") != null) {
			docId = node.get("docId").asLong();
		} 
		
		String docno = null;
		if(node.get("docno") != null) {
			docno = node.get("docno").asText();
		}
		
		Boolean storno = null;
		if (node.get("storno") != null) {
			storno = node.get("storno").asBoolean();
		}
		
		Invoice invoice = new Invoice(branchImporter, branch,
				company, currency, incoterm, invoiceStatus,
				bankPartner, party, scopeType,
				userAdded, userUpdated, date, invoiceNumber, cost,
				taxSum, taxRate, totalCost,
				null, null, null,
				null, null, null, invoiceType, docId, docno, storno);
		invoice.setId(invId);
		invoice.setDateUpdated(dateUpdated);
		invoice.setDateCreated(dateCreated);
		
		List<InvoiceItem> invoiceItems = new ArrayList<>();
		if (node.get("invoiceItems") != null) {
			JsonNode itemsNode = node.get("invoiceItems"), tempNode;
			
			for (Iterator<JsonNode> i = itemsNode.iterator(); i.hasNext();) {
				InvoiceItem si = new InvoiceItem();
				
				JsonNode itemNode = i.next();

				Long iiId = null;
				if (itemNode.get("id") != null) {
					iiId = itemNode.get("id").asLong();			
				}
				
				int id;
				if (itemNode.get("inventory") != null) {		
					tempNode = itemNode.get("inventory");
					id = tempNode.get("id").asInt();
					si.setInventory(new Inventory(id));
				}
				
				String currencyItem = null;
				if (itemNode.get("currency") != null) {		
					tempNode = itemNode.get("currency");
					if (tempNode.get("currency") != null) {
						currencyItem = tempNode.get("currency").asText();
						si.setCurrency(new Currency(currencyItem));
					}
				}
				
				if (itemNode.get("unit") != null) {
					tempNode = itemNode.get("unit");
					String name = tempNode.get("name").asText();
					si.setUnit(new Unit(name));
				}
				
				BigDecimal costI = null;
				if (itemNode.get("cost") != null) {
					costI = new BigDecimal(itemNode.get("cost").asDouble());
					si.setCost(costI);
				}
				
				BigDecimal price = null;
				if (itemNode.get("price") != null) {
					price = new BigDecimal(itemNode.get("price").asDouble());
					si.setPrice(price);
				}
				
				BigDecimal quantity = null;
				if (itemNode.get("quantity") != null) {
					quantity = new BigDecimal(itemNode.get("quantity").asDouble());
					si.setQuantity(quantity);
				}
				
				
				// ********************************************************************************
				
				List<InvItemSn> childInvSns = new ArrayList<>();
				if (itemNode.get("childInvSns") != null) {
					JsonNode childNodes = itemNode.get("childInvSns");
					
					for (Iterator<JsonNode> j = childNodes.iterator(); j.hasNext();) {
						JsonNode cn2 = j.next();

						Long iiSnId = null;
						if (cn2.get("id") != null) {
							iiSnId = cn2.get("id").asLong();
						}
						
						String serialNumber = "";
						if (cn2.get("serialNumber") != null) {
							serialNumber = cn2.get("serialNumber").asText();
						}
					
						
						InvItemSn iiSn = new InvItemSn(iiSnId, si, serialNumber);
						childInvSns.add(iiSn);
					}
				}
				si.setChildInvSns(childInvSns);
				
				// ********************************************************************************
				
				List<StockIn> childStockIns = new ArrayList<>();				
				if (itemNode.get("childStockIns") != null) {
					JsonNode childNodes = itemNode.get("childStockIns");
					
					for (Iterator<JsonNode> j = childNodes.iterator(); j.hasNext();) {
						JsonNode cn2 = j.next();
						
						Long siId = null;
						if (cn2.get("id") != null) {
							siId = cn2.get("id").asLong();
						}
						
						BigDecimal squantity = null;
						if (cn2.get("quantity") != null) {
							squantity = new BigDecimal(cn2.get("quantity").asDouble());
						}
						
						BigDecimal rate = null;
						if (cn2.get("rate") != null) {
							rate = new BigDecimal(cn2.get("rate").asDouble());
						}
						
						String serialNumber = "";
						if (cn2.get("serialNumber") != null) {
							serialNumber = cn2.get("serialNumber").asText();
						}
						
						StockStatus genStatus = null;
						if (cn2.get("genStatus") != null) {
							JsonNode iNode = cn2.get("genStatus");
							if (iNode.get("id") != null) {
								Integer stockId = iNode.get("id").asInt();
								genStatus = new StockStatus(stockId);
							}
						}
						
						StockStatus intStatus = null;
						if (cn2.get("intStatus") != null) {
							JsonNode iNode = cn2.get("intStatus");
							if (iNode.get("id") != null) {
								Integer stockId = iNode.get("id").asInt();
								intStatus = new StockStatus(stockId);
							}
						}
						
						String refkey = null;
						if (cn2.get("refkey") != null) {
							refkey = cn2.get("refkey").asText();
						}
						
						Currency wcurrency = null;
						if(cn2.get("wcurrency") != null) {
							JsonNode cn3 = cn2.get("wcurrency");
							if (cn3.get("currency") != null) {
								wcurrency = new Currency();
								String currencyStr = cn3.get("currency").asText();
								wcurrency.setCurrency(currencyStr);
							}	
						}
						
						
						BigDecimal wsumm = null;
						if (cn2.get("wsumm") != null) {
							wsumm = new BigDecimal(cn2.get("wsumm").asDouble());
						}
						
						BigDecimal dsumm = null;
						if (cn2.get("dsumm") != null) {
							dsumm = new BigDecimal(cn2.get("dsumm").asDouble());
						}
						
						Currency dcurrency = null;
						if(cn2.get("dcurrency") != null) {
							JsonNode cn3 = cn2.get("dcurrency");
							if (cn3.get("currency") != null) {
								dcurrency = new Currency();
								String currencyStr = cn3.get("currency").asText();
								dcurrency.setCurrency(currencyStr);
							}	
						}
						
						String glCode = null;
						if (cn2.get("glCode") != null) {
							glCode = cn2.get("glCode").asText();
						}
						
						Date dateIn = null;
						if (cn2.get("dateIn") != null) {
							String ddStr = cn2.get("dateIn").asText();
							try {
								if (ddStr.length() > 10)
									dateIn = df1.parse(ddStr);
								else
									dateIn = df2.parse(ddStr);
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}
						
						Company siCompany = null;
						if(cn2.get("company") != null) {
							JsonNode cn3 = cn2.get("company");
							if (cn3.get("id") != null) {
								Integer cid = cn3.get("id").asInt();
								siCompany = new Company(cid);
							}
						} 
						
						Branch siBranch = null;
						if(cn2.get("branch") != null) {
							JsonNode cn3 = cn2.get("branch");
							if (cn3.get("id") != null) {
								Integer bid = cn3.get("id").asInt();
								siBranch = new Branch(bid);
							}
						} 
						
						Boolean broken = null;
						if (cn2.get("broken") != null) {
							broken = cn2.get("broken").asBoolean();
						}
						StockIn stock = new StockIn(siId, siCompany, siBranch, si.getInventory(), squantity, si.getUnit(), si,  serialNumber,
								genStatus, intStatus, broken, refkey, wsumm, wcurrency, dsumm, dcurrency, rate, dateIn, glCode);
						
						childStockIns.add(stock);
					}
				}
				
				List<StockOut> childStockOuts = new ArrayList<>();				
				if (itemNode.get("childStockOuts") != null) {
					JsonNode childNodes = itemNode.get("childStockOuts");
					
					for (Iterator<JsonNode> j = childNodes.iterator(); j.hasNext();) {
						JsonNode cn2 = j.next();
							
							
						Long siId = null;
						if (cn2.get("id") != null) {
							siId = cn2.get("id").asLong();
						}
							
						BigDecimal squantity = null;
						if (cn2.get("quantity") != null) {
							squantity = new BigDecimal(cn2.get("quantity").asDouble());
						}
							
						BigDecimal rate = null;
						if (cn2.get("rate") != null) {
							rate = new BigDecimal(cn2.get("rate").asDouble());
						}
							
						String serialNumber = "";
						if (cn2.get("serialNumber") != null) {
						 serialNumber = cn2.get("serialNumber").asText();
						}
						
						StockStatus genStatus = null;
						if (cn2.get("genStatus") != null) {
							JsonNode iNode = cn2.get("genStatus");
							if (iNode.get("id") != null) {
								Integer stockId = iNode.get("id").asInt();
								genStatus = new StockStatus(stockId);
							}
						}
							
							
						String refkey = null;
						if (cn2.get("refkey") != null) {
							refkey = cn2.get("refkey").asText();
						}
							
						Currency wcurrency = null;
						if(cn2.get("wcurrency") != null) {
							JsonNode cn3 = cn2.get("wcurrency");
							if (cn3.get("currency") != null) {
								wcurrency = new Currency();
								String currencyStr = cn3.get("currency").asText();
								wcurrency.setCurrency(currencyStr);
							}	
						}
							
							
						BigDecimal wsumm = null;
						if (cn2.get("wsumm") != null) {
							wsumm = new BigDecimal(cn2.get("wsumm").asDouble());
						}
							
						BigDecimal dsumm = null;
						if (cn2.get("dsumm") != null) {
							dsumm = new BigDecimal(cn2.get("dsumm").asDouble());
						}
							
						Currency dcurrency = null;
						if(cn2.get("dcurrency") != null) {
							JsonNode cn3 = cn2.get("dcurrency");
							if (cn3.get("currency") != null) {
								dcurrency = new Currency();
								String currencyStr = cn3.get("currency").asText();
								dcurrency.setCurrency(currencyStr);
							}	
						}
							
						String glCode = null;
						if (cn2.get("glCode") != null) {
							glCode = cn2.get("glCode").asText();
						}
							
						Date dateOut = null;
						if (cn2.get("dateOut") != null) {
							String ddStr = cn2.get("dateOut").asText();
							try {
								if (ddStr.length() > 10)
									dateOut = df1.parse(ddStr);
								else
									dateOut = df2.parse(ddStr);
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}
							
						Company siCompany = null;
						if(cn2.get("company") != null) {
							JsonNode cn3 = cn2.get("company");
							if (cn3.get("id") != null) {
								Integer cid = cn3.get("id").asInt();
								siCompany = new Company(cid);
							}
						} 
							
						Branch siBranch = null;
						if(cn2.get("branch") != null) {
							JsonNode cn3 = cn2.get("branch");
							if (cn3.get("id") != null) {
								Integer bid = cn3.get("id").asInt();
								siBranch = new Branch(bid);
							}
						} 
						
						Boolean broken = null;
						if (cn2.get("broken") != null) {
							broken = cn2.get("broken").asBoolean();
						}
						
						StockOut stock = new StockOut(siId, siCompany, siBranch, si.getInventory(), squantity, si.getUnit(), si,  serialNumber,
								genStatus, refkey, wsumm, wcurrency, dsumm, dcurrency, rate, dateOut, glCode, broken);
						
						childStockOuts.add(stock);
					}
				
				}
				si.setChildStockOuts(childStockOuts);
				si.setChildStockIns(childStockIns);
			
				si.setInvoice(invoice);
				si.setId(iiId);
				invoiceItems.add(si);
			}
			invoice.setInvoiceItems(invoiceItems);
		}
		return invoice;
	}

}
