package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Department;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.Position;
import com.cordialsr.domain.Region;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class EmployeeDeserializer extends JsonDeserializer<Employee>{

	@Override
	public Employee deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		
		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp), ptNode;
		ObjectMapper jsonObjectMapper = new ObjectMapper();
		//DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		Date dateHired = null;		
		    if (node.get("dateHired") != null) {
		    	String dateHiredStr = node.get("dateHired").asText();
			    try {
			    	dateHired = dateFormat.parse(dateHiredStr);
				} catch (ParseException e) {
					e.printStackTrace();
				}
		    }
		    
		    Date dateFired = null;		
		    if (node.get("dateFired") != null) {
		    	String dateFiredStr = node.get("dateFired").asText();
			    try {
			    	dateFired = dateFormat.parse(dateFiredStr);
				} catch (ParseException e) {
					e.printStackTrace();
				}
		    } 
		    
		    BigDecimal salary = null;
			if (node.get("salary") != null) {
				Double temp = node.get("salary").asDouble();
				salary = new BigDecimal(temp);
			}
			
			BigDecimal officialSalary = null;
			if (node.get("officialSalary") != null) {
				Double temp = node.get("officialSalary").asDouble();
				officialSalary = new BigDecimal(temp);
			}
			
			Long emplId = null;
			if (node.get("id") != null) {
				emplId = node.get("id").asLong();				
			}
			
				
			Branch branch = null;
			if(node.get("branch") != null) {
				branch = new Branch();
				ptNode = node.get("branch");
				if (ptNode.get("id") != null) 
				{
						int id = ptNode.get("id").asInt();
						branch.setId(id);
				}
			} 
			
			Company company = null;
			if(node.get("company") != null) {
				company = new Company();
				ptNode = node.get("company");
				if (ptNode.get("id") != null) 
				{
						int id = ptNode.get("id").asInt();
						company.setId(id);
				}
			} 
			
			Department department = null;
			if(node.get("department") != null) {
				department = new Department();
				ptNode = node.get("department");
				if (ptNode.get("id") != null) 
				{
				int id = ptNode.get("id").asInt();
				department.setId(id);
				}
			} 
			
			Party party = null;
			if(node.get("party") != null) {
				party = new Party();
				ptNode = node.get("party");
				if (ptNode.get("id") != null) 
				{
					Long id = ptNode.get("id").asLong();
					party.setId(id);
				}
			}
			
			Region region = null;
			if(node.get("region") != null) {
				ptNode = node.get("region");
				if (ptNode.get("id") != null) 
				{
					Integer id = ptNode.get("id").asInt();
					region = new Region(id);
				}
			} 
			
			Currency currency = null;
			if(node.get("currency") != null) {
				currency = new Currency();
				ptNode = node.get("currency");
				if (ptNode.get("currency") != null) 
				{
				String currencyStr = ptNode.get("currency").asText();
				currency.setCurrency(currencyStr);
				}
			} 
			
			Position position = null;
			if(node.get("position") != null) {
				position = new Position();
				ptNode = node.get("position");
				if (ptNode.get("id") != null) 
				{
				int id = ptNode.get("id").asInt();
				position.setId(id);
				}
			} 
			
			Employee accountableTo = null;
			if(node.get("accountableTo") != null) {
				accountableTo = new Employee();
				ptNode = node.get("accountableTo");
				if (ptNode.get("id") != null) 
				{
					Long id = ptNode.get("id").asLong();
					accountableTo.setId(id);
				}
			}
			
			String info = "";
			if (node.get("info") != null) {
				info = node.get("info").asText();				
			}
			
		Employee employee = new Employee(party, company, region, branch, department,
				position, salary, currency, dateHired, dateFired, accountableTo, officialSalary, info);
		
		employee.setId(emplId); 
		
		return employee;
	}
	
}
