package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.cordialsr.domain.security.Transaction;
import com.cordialsr.domain.security.User;
import com.cordialsr.domain.security.UserLog;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UserLogDeserializer  extends JsonDeserializer<UserLog>{
	
	@Override
	public UserLog deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp), ptNode;
		ObjectMapper jsonObjectMapper = new ObjectMapper();

		User user = null;
		if (node.get("user") != null) {
			JsonNode node2 = node.get("user");
			if (node2.get("userid") != null) {
				Integer id = node2.get("userid").asInt();
				user = new User(id);
			}
		}

		Transaction transaction = null;
		if (node.get("transaction") != null) {
			JsonNode brNode = node.get("transaction");
			if (brNode.get("id") != null) {
				Integer trId = brNode.get("id").asInt();
				transaction = new Transaction(trId);
			}
		}

		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		// DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");

		Date date = null;
		if (node.get("date") != null) {
			String ddStr = node.get("date").asText();
			try {
				date = df1.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		Date time = null;
		if (node.get("time") != null) {
			String ddStr = node.get("time").asText();
			try {
				time = df1.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}


		String info = "";
		if (node.get("info") != null) {
			info = node.get("info").asText();
		}
		
		String logType = "";
		if (node.get("logType") != null) {
			logType = node.get("logType").asText();
		}

		UserLog userLog = new UserLog(null, date, time, user, transaction, logType, info);
		return userLog;
	}

}
