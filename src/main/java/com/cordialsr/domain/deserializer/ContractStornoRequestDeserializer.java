package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractStornoRequest;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class ContractStornoRequestDeserializer extends JsonDeserializer<ContractStornoRequest>{

	@Override
	public ContractStornoRequest deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		ObjectCodec oc = p.getCodec();
		JsonNode node = oc.readTree(p);
		
		// DATA 2018-07-02T08:46:24.532+0000
		// DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		
		Long id = null;
		if (node.get("id") != null) {
			id = node.get("id").asLong();
		}
		
		Company company = null;
		if (node.get("company") != null) {
			JsonNode node2 = node.get("company");
			if (node2.get("id") != null) {
				Integer comId = node2.get("id").asInt();
				company = new Company(comId);
			}
		}
		
		Branch branch = null;
		if (node.get("branch") != null) {
			JsonNode node2 = node.get("branch");
			if (node2.get("id") != null) {
				Integer brId = node2.get("id").asInt();
				branch = new Branch(brId);
			}
		}
		
		String reqNumber = "";
		if (node.get("reqNumber") != null) {
			reqNumber = node.get("reqNumber").asText();
		}
		
		Contract contract = null;
		if (node.get("contract") != null) {
			JsonNode node2 = node.get("contract");
			if (node2.get("id") != null) {
				Long conId = node2.get("id").asLong();
				contract = new Contract(conId);
			}
		}
		
		User reqAuthor = null;
		if (node.get("reqAuthor") != null) {
			JsonNode node2 = node.get("reqAuthor");
			if (node2.get("userid") != null) {
				Integer reqAuth = node2.get("userid").asInt();
				reqAuthor = new User(reqAuth);
			}
		}
		
		Calendar reqDate = null;
		if (node.get("reqDate") != null) {
			String ddStr = node.get("reqDate").asText();
			try {
				if (ddStr.length() > 10) {
					reqDate = Calendar.getInstance();
					reqDate.setTime(df1.parse(ddStr));
				} else {
					reqDate = Calendar.getInstance();
					reqDate.setTime(df2.parse(ddStr));
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		Integer status = null;
		if (node.get("status") != null) {
			status = node.get("status").asInt();
		}
		
		String info = "";
		if (node.get("info") != null) {
			info = node.get("info").asText();
		}
		
		BigDecimal returnAmountDue = null;
		if (node.get("returnAmountDue") != null) {
			returnAmountDue = new BigDecimal(node.get("returnAmountDue").asDouble());
		}
		
		Currency currency = null;
		if (node.get("currency") != null) {
			JsonNode node2 = node.get("currency");
			if (node2.get("currency") != null) {
				String curr = node2.get("currency").asText();
				currency = new Currency(curr);
			}
		}
		
		BigDecimal returnAmount = null;
		if (node.get("returnAmount") != null) {
			returnAmount = new BigDecimal(node.get("returnAmount").asDouble());
		}
		
		User grantAuthor = null;
		if (node.get("grantAuthor") != null) {
			JsonNode node2 = node.get("grantAuthor");
			if (node2.get("userid") != null) {
				Integer reqAuth = node2.get("userid").asInt();
				grantAuthor = new User(reqAuth);
			}
		}
		
		Calendar grantDate = null;
		if (node.get("grantDate") != null) {
			String ddStr = node.get("grantDate").asText();
			try {
				if (ddStr.length() > 10) {
					grantDate = Calendar.getInstance();
					grantDate.setTime(df1.parse(ddStr));
				} else {
					grantDate = Calendar.getInstance();
					grantDate.setTime(df2.parse(ddStr));
				}	
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		Date closeDate = null;
		if (node.get("closeDate") != null) {
			String ddStr = node.get("closeDate").asText();
			try {
				if (ddStr.length() > 10) {
					closeDate = df1.parse(ddStr);
				} else {
					closeDate = df2.parse(ddStr);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		ContractStornoRequest request = new ContractStornoRequest(id, company, branch, reqNumber, contract, reqAuthor, reqDate, 
							status, info, returnAmountDue, currency, returnAmount, grantAuthor, grantDate, closeDate);
		
		
		return request;
	}

}
