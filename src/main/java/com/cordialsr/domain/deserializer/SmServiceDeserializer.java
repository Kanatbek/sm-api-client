package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.SmEnquiry;
import com.cordialsr.domain.SmService;
import com.cordialsr.domain.SmServiceItem;
import com.cordialsr.domain.SmVisit;
import com.cordialsr.domain.Unit;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
@JsonDeserialize(using = SmServiceDeserializer.class)
public class SmServiceDeserializer extends JsonDeserializer<SmService> { 
 
	 @Override
	 public SmService deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
	    ObjectCodec oc = jp.getCodec();
	    JsonNode node = oc.readTree(jp);
	    
	    DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat tf1 = new SimpleDateFormat("HH:mm:ss");
	    
	    Date sdate = null;		
	    if (node.get("sdate") != null) {
	    	String sDateStr = node.get("sdate").asText();
		    try {
		    	if (sDateStr.length() > 10)
		    		sdate = df1.parse(sDateStr);
				else if (sDateStr.length() > 7)
					sdate = df2.parse(sDateStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
	    
	    Date stime = null;		
	    if (node.get("stime") != null) {
	    	String sDateStr = node.get("stime").asText();
		    try {
		    	if (sDateStr.length() > 10)
		    		stime = df1.parse(sDateStr);
				else if (sDateStr.length() > 7)
					stime = tf1.parse(sDateStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
	    
	    
	    Date itime = null;		
	    if (node.get("itime") != null) {
	    	String sDateStr = node.get("itime").asText();
		    try {
		    	if (sDateStr.length() > 10)
		    		itime = df1.parse(sDateStr);
				else if (sDateStr.length() > 7)
					itime = df2.parse(sDateStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
	    
	    Long id = null;
		if (node.get("id") != null)
			id = node.get("id").asLong();
	    
	    String caremanFio = "";
		if (node.get("caremanFio") != null) 
			caremanFio = node.get("caremanFio").asText();
		
		String info = "";
		if (node.get("info") != null)
			info = node.get("info").asText();
		
		String refkey = null;
		if (node.get("refkey") != null)
			refkey = node.get("refkey").asText();
		
		String type = "";
		if (node.get("type") != null)
			type = node.get("type").asText();
		
		boolean storno = false;
		if (node.get("storno") != null)
			storno = (node.get("storno").asText().equalsIgnoreCase("true"));
		
		Boolean overdue = null;
		if (node.get("overdue") != null)
			overdue = (node.get("overdue").asText().equalsIgnoreCase("true"));		
	    
		Boolean isEarly = null;
		if (node.get("isEarly") != null)
			overdue = (node.get("isEarly").asText().equalsIgnoreCase("true"));		
	    
		
		Contract contract = null;
		if (node.get("contract") != null) {
			JsonNode brNode = node.get("contract");
			if (brNode.get("id") != null) {
				Long conId = brNode.get("id").asLong();
				contract = new Contract(conId);				
			}
			if (brNode.get("company") != null) {
				JsonNode comNode = brNode.get("company");
				if (comNode.get("id") != null) {
					Integer comId = comNode.get("id").asInt();
					contract.setCompany(new Company(comId));
				}
			}
		}
		
		Party customer = null;
		if (node.get("customer") != null) {
			JsonNode brNode = node.get("customer");
			if (brNode.get("id") != null) {
				Long inId = brNode.get("id").asLong();
				customer = new Party(inId);				
			}
		}
		
		Employee careman = null;
		if (node.get("careman") != null) {
			JsonNode brNode = node.get("careman");
			if (brNode.get("id") != null) {
				Long inId = brNode.get("id").asLong();
				careman = new Employee(inId);				
			}
		}
		
		Currency currency = null;
		if (node.get("currency") != null) {
			JsonNode brNode = node.get("currency");
			if (brNode.get("currency") != null) {
				String inId = brNode.get("currency").asText();
				currency = new Currency(inId);				
			}
		}
		
		Company company = null;
		if (node.get("company") != null) {
			JsonNode brNode = node.get("company");
			if (brNode.get("id") != null) {
				Integer inId = brNode.get("id").asInt();
				company = new Company(inId);				
			}
		}
		
		Branch branch = null;
		if (node.get("branch") != null) {
			JsonNode brNode = node.get("branch");
			if (brNode.get("id") != null) {
				Integer inId = brNode.get("id").asInt();
				branch = new Branch(inId);				
			}
		}
		
		Currency premiCurrency = null;
		if (node.get("premiCurrency") != null) {
			JsonNode brNode = node.get("premiCurrency");
			if (brNode.get("currency") != null) {
				String inId = brNode.get("currency").asText();
				premiCurrency = new Currency(inId);				
			}
		}
		
		BigDecimal cost = null;
		if (node.get("cost") != null) {
			cost = new BigDecimal(node.get("cost").asDouble());
		}
		
		BigDecimal discount = null;
		if (node.get("discount") != null) {
			discount = new BigDecimal(node.get("discount").asDouble());
		}
		
		BigDecimal summ = null;
		if (node.get("summ") != null) {
			summ = new BigDecimal(node.get("summ").asDouble());
		}
		
		BigDecimal paid = null;
		if (node.get("paid") != null) {
			paid = new BigDecimal(node.get("paid").asDouble());
		}
		
		BigDecimal premi = null;
		if (node.get("premi") != null) {
			premi = new BigDecimal(node.get("premi").asDouble());
		}
		
		Inventory inventory = null;
		if (node.get("inventory") != null) {
			JsonNode brNode = node.get("inventory");
			if (brNode.get("id") != null) {
				Integer inId = brNode.get("id").asInt();
				inventory = new Inventory(inId);			
			}
		}
		
		User userAdded = null;
		if(node.get("userAdded") != null) {
			JsonNode ptNode = node.get("userAdded");
			if (ptNode.get("userid") != null) {
				Long uid = ptNode.get("userid").asLong();
				userAdded = new User(uid);
			}
		}
		
		Boolean verified = false;
		if (node.get("verified") != null) {
			verified = node.get("verified").asBoolean();
		}
		
		String serviceNumber = null;
		if (node.get("serviceNumber") != null) {
			serviceNumber = node.get("serviceNumber").asText();
		}
		
		Integer mark = null;
		if (node.get("mark") != null) {
			mark = node.get("mark").asInt();
		}
		
		SmEnquiry smEnquiry = null;
		if(node.get("smEnquiry") != null) {
			JsonNode ptNode = node.get("smEnquiry");
			if (ptNode.get("id") != null) {
				Long uid = ptNode.get("id").asLong();
				smEnquiry = new SmEnquiry(uid);
			}
		}
		
		SmVisit smVisit = null;
		if(node.get("smVisit") != null) {
			JsonNode ptNode = node.get("smVisit");
			if (ptNode.get("id") != null) {
				Long visitId = ptNode.get("id").asLong();
				smVisit = new SmVisit(visitId);
			}
		}
		
		SmService smService = new SmService(id, company, branch, type, sdate, stime, itime, caremanFio, storno, null, 
				info, contract, careman, customer, cost, discount, summ, paid, currency, premi, premiCurrency, 
				inventory, refkey, userAdded, verified, serviceNumber, mark, smEnquiry, overdue, isEarly, smVisit); 
		
		Set<SmServiceItem> siList = new HashSet<>();
		if (node.get("smServiceItems") != null) {
			JsonNode itemsNode = node.get("smServiceItems");
			
			for (Iterator<JsonNode> i = itemsNode.iterator(); i.hasNext();) {
				JsonNode itemNode = i.next();
				
				BigDecimal quantity = null;
				if (itemNode.get("quantity") != null) {
					quantity = new BigDecimal(itemNode.get("quantity").asDouble());
				}
				
				Integer fno = null;
				if (itemNode.get("fno") != null) {
					fno = itemNode.get("fno").asInt();					
				}
				
				String itemName = null;
				if (itemNode.get("itemName") != null) {
					itemName= itemNode.get("itemName").asText();					
				}
				
				BigDecimal price = null;
				if (itemNode.get("price") != null) {
					price = new BigDecimal(itemNode.get("price").asDouble());
				}
				
				BigDecimal itemCost = null;
				if (itemNode.get("cost") != null) {
					itemCost = new BigDecimal(itemNode.get("cost").asDouble());
				}
				
				Unit unit = null;
				if (itemNode.get("unit") != null) {
					JsonNode iNode = itemNode.get("unit");
					if (iNode.get("name") != null) {
						String name = iNode.get("name").asText();
						unit = new Unit(name);			
					}
				}
				
				Inventory sInventory = null;
				if (itemNode.get("inventory") != null) {
					JsonNode brNode = itemNode.get("inventory");
					if (brNode.get("id") != null) {
						Integer inId = brNode.get("id").asInt();
						sInventory = new Inventory(inId);			
					}
				}
				
				Boolean isTovar = null;
				if (itemNode.get("isTovar") != null) {
					isTovar = itemNode.get("isTovar").asBoolean();
				}
				
				SmServiceItem si = new SmServiceItem(id, smService, itemName, fno, quantity, sInventory, price, unit, itemCost, isTovar);
				siList.add(si);
			}
		}
		
		smService.setSmServiceItems(siList);
	    return smService;
	 }
	
}
