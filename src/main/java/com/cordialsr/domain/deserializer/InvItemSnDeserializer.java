package com.cordialsr.domain.deserializer;

import java.io.IOException;

import com.cordialsr.domain.InvItemSn;
import com.cordialsr.domain.InvoiceItem;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class InvItemSnDeserializer extends JsonDeserializer<InvItemSn> {

	@Override
	public InvItemSn deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp), ptNode;
	
		Long invId = null;
		if (node.get("id") != null) {
			invId = node.get("id").asLong();			
		}
		
		String serialNumber = "";
		if (node.get("serialNumber") != null) {
			serialNumber = node.get("serialNumber").asText();
		}
		
		InvoiceItem invoiceItem = null;
		if(node.get("invoiceItem") != null) {
			ptNode = node.get("invoiceItem");
			if (ptNode.get("id") != null) {
				Long id = ptNode.get("id").asLong();
				invoiceItem = new InvoiceItem(id);
			}
		}
		
		InvItemSn invItemSn = new InvItemSn(invId, invoiceItem, serialNumber);
		return invItemSn;
	}
}
