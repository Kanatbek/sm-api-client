package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.PhoneNumber;
import com.cordialsr.domain.SmCall;
import com.cordialsr.domain.SmService;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class SmCallDeserializer extends JsonDeserializer<SmCall> {

	@Override
	 public SmCall deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
	    ObjectCodec oc = jp.getCodec();
	    JsonNode node = oc.readTree(jp), scNode;
	    
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
	    
	    Integer id = null;
	    if (node.get("id") != null) {
	    	id = node.get("id").asInt();
	    }
	    
	    Company company = null;
	    if (node.get("company") != null) {
	    	scNode = node.get("company");
	    	if (scNode.get("id") != null) {
	    		Integer cid = scNode.get("id").asInt();
	    		company = new Company(cid);
	    	}
	    }
	    
	    Branch branch = null;
	    if (node.get("branch") != null) {
	    	scNode = node.get("branch");
	    	if (scNode.get("id") != null) {
	    		Integer cid = scNode.get("id").asInt();
	    		branch = new Branch(cid);
	    	}
	    }
	    
	    Contract contract = null;
	    if (node.get("contract") != null) {
	    	scNode = node.get("contract");
	    	if (scNode.get("id") != null) {
	    		Long cid = scNode.get("id").asLong();
	    		contract = new Contract(cid);
	    	}
	    }
	    
	    SmService service = null;
	    if (node.get("service") != null) {
	    	scNode = node.get("service");
	    	if (scNode.get("id") != null) {
	    		Long cid = scNode.get("id").asLong();
	    		service = new SmService(cid);
	    	}
	    }
	    
	    Party party = null;
	    if (node.get("party") != null) {
	    	scNode = node.get("party");
	    	if (scNode.get("id") != null) {
	    		Long cid = scNode.get("id").asLong();
	    		party = new Party(cid);
	    	}
	    }
	    
	    PhoneNumber phoneNumber = null;
	    if (node.get("phoneNumber") != null) {
	    	scNode = node.get("phoneNumber");
	    	if (scNode.get("id") != null) {
	    		Long cid = scNode.get("id").asLong();
	    		phoneNumber = new PhoneNumber(cid);
	    	}
	    }
	    
	    Date callDate = null;		
	    if (node.get("callDate") != null) {
	    	String ddStr = node.get("callDate").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		callDate = df1.parse(ddStr);
				else
					callDate = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
	    
	    Date callTime = null;		
	    if (node.get("callTime") != null) {
	    	String ddStr = node.get("callTime").asText();
		    try {
		    	if (ddStr.length() > 10)
		    		callTime = df1.parse(ddStr);
				else
					callTime = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
	    
	    User user = null;
	    if (node.get("user") != null) {
	    	scNode = node.get("user");
	    	if (scNode.get("userid") != null) {
	    		Long cid = scNode.get("userid").asLong();
	    		user = new User(cid);
	    	}
	    }
	    
	    String info = null;
	    if (node.get("info") != null) {
	    	info = node.get("info").asText();
	    }
	    
	    String responder = null;
	    if (node.get("responder") != null) {
	    	responder = node.get("responder").asText();
	    }
	    
	    Integer status = null;
	    if (node.get("status") != null) {
	    	status = node.get("status").asInt();
	    }
	    
	    Boolean outgoing = null;
	    if (node.get("isOutgoing") != null) {
	    	outgoing = node.get("isOutgoing").asBoolean();
	    }
		
	    String callNumber = null;
	    if (node.get("callNumber") != null) {
	    	callNumber = node.get("callNumber").asText();
	    }
	    
		SmCall smCall = new SmCall(id, company, branch, contract, service, party, phoneNumber, callDate, callTime, user, info, responder, status, outgoing, callNumber);
	    
	    return smCall;
	}
}
