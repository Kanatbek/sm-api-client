package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.SmBranch;
import com.cordialsr.domain.SmContract;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class SmContractDeserializer extends JsonDeserializer<SmContract> {

	@Override
	 public SmContract deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
	    ObjectCodec oc = jp.getCodec();
	    JsonNode node = oc.readTree(jp);
	    
	    DateFormat dfLong = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
	    DateFormat dfShort = new SimpleDateFormat("yyyy-MM-dd");
	    
	    Integer id = null;
		if (node.get("id") != null)
			id = node.get("id").asInt();
		
	    String addrFact = "";
		if (node.get("addrFact") != null) 
			addrFact = node.get("addrFact").asText();
		
		String addrReg = "";
		if (node.get("addrReg") != null)
			addrReg = node.get("addrReg").asText();
		
		Date cdate = null;		
	    if (node.get("cdate") != null) {
	    	String cDateStr = node.get("cdate").asText();
		    try {
		    	if (cDateStr.length() > 10) cdate = dfLong.parse(cDateStr);
		    	else cdate = dfShort.parse(cDateStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
		
		String careman = "";
		if (node.get("careman") != null)
			careman = node.get("careman").asText();
		
		String contractNumber = "";
		if (node.get("contractNumber") != null)
			contractNumber = node.get("contractNumber").asText();
		
		String customer = "";
		if (node.get("customer") != null)
			customer = node.get("customer").asText();
		
		String dealer = "";
		if (node.get("dealer") != null)
			dealer = node.get("dealer").asText();
		
		String iinbin = "";
		if (node.get("iinbin") != null)
			iinbin = node.get("iinbin").asText();
		
		String model = "";
		if (node.get("model") != null)
			model = node.get("model").asText();
		
		String phone = "";
		if (node.get("phone") != null)
			phone = node.get("phone").asText();
		
		Boolean resigned = null;
		if (node.get("resigned") != null)
			resigned = (node.get("resigned").asText().equalsIgnoreCase("true"));
		
		SmBranch smBranch = new SmBranch();
		if (node.get("smBranch") != null) {
			JsonNode brNode = node.get("smBranch");
			String brCode = brNode.get("code").asText();
			smBranch.setCode(brCode);
		}
		
		String sn = "";
		if (node.get("sn") != null)
			sn = node.get("sn").asText();
	    
		Boolean storno = null;
		if (node.get("storno") != null)
			storno = (node.get("storno").asText().equalsIgnoreCase("true"));
		
		Boolean type = null;
		if (node.get("type") != null)
			type = (node.get("type").asText().equalsIgnoreCase("true"));
		
		String info = "";
		if (node.get("info") != null)
			info = node.get("info").asText();
		
		User updatedUser = null;
		if (node.get("updatedUser") != null) {
			JsonNode iNode = node.get("updatedUser");
			if (iNode.get("userid") != null) {
				Long userid = iNode.get("userid").asLong();
				updatedUser = new User(userid);
			}
		}
		
		Inventory inventory = null;
		if (node.get("inventory") != null) {
			JsonNode iNode = node.get("inventory");
			if (iNode.get("id") != null) {
				Integer invId = iNode.get("id").asInt();
				inventory = new Inventory(invId);
			}
		}	
		
		Date updatedDate = null;		
	    if (node.get("updatedDate") != null) {
	    	String cDateStr = node.get("updatedDate").asText();
		    try {
		    	if (cDateStr.length() > 10) {
		    		updatedDate = dfLong.parse(cDateStr);
		    	} else if (cDateStr.length() > 7) {
		    		updatedDate = dfShort.parse(cDateStr);
		    	}
			} catch (ParseException e) {
				e.printStackTrace();
			}
	    }
		
		SmContract smc = new SmContract(id, smBranch, cdate, contractNumber, customer, iinbin, dealer, phone, careman, 
				model, sn, storno, addrReg, addrFact, resigned, type, info, updatedUser, updatedDate, inventory);
		
		return smc;
	}

}
