package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Department;
import com.cordialsr.domain.FinActivityType;
import com.cordialsr.domain.FinCashflowStatement;
import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.FinDocType;
import com.cordialsr.domain.FinEntry;
import com.cordialsr.domain.FinGlAccount;
import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.InvoiceItem;
import com.cordialsr.domain.KassaBank;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.StockIn;
import com.cordialsr.domain.StockStatus;
import com.cordialsr.domain.Unit;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FinDocDeserializer extends JsonDeserializer<FinDoc> {

	@Override
	public FinDoc deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp), ptNode;
		ObjectMapper jsonObjectMapper = new ObjectMapper();

		Company company = null;
		if (node.get("company") != null) {
			JsonNode node2 = node.get("company");
			if (node2.get("id") != null) {
				Integer id = node2.get("id").asInt();
				company = new Company(id);
			}
		}

		Branch branch = null;
		if (node.get("branch") != null) {
			JsonNode brNode = node.get("branch");
			if (brNode.get("id") != null) {
				Integer brId = brNode.get("id").asInt();
				branch = new Branch(brId);
			}
		}

		Currency dcurrency = null;
		if (node.get("dcurrency") != null) {
			JsonNode brNode = node.get("dcurrency");
			if (brNode.get("currency") != null) {
				String cur = brNode.get("currency").asText();
				dcurrency = new Currency(cur);
			}
		}

		Currency wcurrency = null;
		if (node.get("wcurrency") != null) {
			JsonNode brNode = node.get("wcurrency");
			if (brNode.get("currency") != null) {
				String cur = brNode.get("currency").asText();
				wcurrency = new Currency(cur);
			}
		}

		Department department = null;
		if (node.get("department") != null) {
			JsonNode brNode = node.get("department");
			if (brNode.get("id") != null) {
				Integer id = brNode.get("id").asInt();
				department = new Department(id);
			}
		}

		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");

		Date docDate = null;
		if (node.get("docDate") != null) {
			String ddStr = node.get("docDate").asText();
			try {
				if (ddStr.length() > 10)
					docDate = df1.parse(ddStr);
				else if (ddStr.length() > 7)
					docDate = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		Party party = null;
		if (node.get("party") != null) {
			JsonNode brNode = node.get("party");
			if (brNode.get("id") != null) {
				Long id = brNode.get("id").asLong();
				party = new Party(id);
			}
		}

		BigDecimal dsumm = null;
		if (node.get("dsumm") != null) {
			dsumm = new BigDecimal(node.get("dsumm").asDouble());
		}

		BigDecimal wsumm = null;
		if (node.get("wsumm") != null) {
			wsumm = new BigDecimal(node.get("wsumm").asDouble());
		}

		BigDecimal rate = null;
		if (node.get("rate") != null) {
			rate = new BigDecimal(node.get("rate").asDouble());
		}

		Boolean isMain = null;
		if (node.get("isMain") != null) {
			isMain = node.get("isMain").asBoolean();
		}

		String info = "";
		if (node.get("info") != null) {
			info = node.get("info").asText();
		}

		User user = null;
		if (node.get("user") != null) {
			JsonNode brNode = node.get("user");
			if (brNode.get("userid") != null) {
				Integer id = brNode.get("userid").asInt();
				user = new User(new Long(id));
			}
		}
		
		Integer month = null;
		if (node.get("month") != null) {
			month = node.get("month").asInt();
		}
		
		String year = "";
		if (node.get("year") != null) {
			year = node.get("year").asText();
		}

		String title = "";
		if (node.get("title") != null) {
			title = node.get("title").asText();
		}
		
		String refkey = "";
		if (node.get("refkey") != null) {
			refkey = node.get("refkey").asText();
		}
		
		String reftyp = "";
		if (node.get("reftyp") != null) {
			reftyp = node.get("reftyp").asText();
		}
		
		String docno = "";
		if (node.get("docno") != null) {
			docno = node.get("docno").asText();
		}
		
		FinDocType finDocType = null;
		if (node.get("finDocType") != null) {
			JsonNode iNode = node.get("finDocType");
			if (iNode.get("code") != null) {
				String code = iNode.get("code").asText();
				finDocType = new FinDocType(code);
			}
		}

		FinDoc parentFindDoc = null;
		if (node.get("parentFindoc") != null) {
			JsonNode iNode = node.get("parentFindoc");
			if (iNode.get("id") != null) {
				Long id = iNode.get("id").asLong();
				parentFindDoc = new FinDoc(id);
			}
		}
		
		String stDocno = "";
		if (node.get("stDocno") != null) {
			stDocno = node.get("stDocno").asText();
		}
		
		String stYear = "";
		if (node.get("stYear") != null) {
			stYear = node.get("stYear").asText();
		}
		
		Contract contract = null;
		if (node.get("contract") != null) {
			JsonNode iNode = node.get("contract");
			if (iNode.get("id") != null) {
				Long id = iNode.get("id").asLong();
				contract = new Contract(id);
			}
		}
		
		Invoice invoice = null;
		if (node.get("invoice") != null) {
			JsonNode iNode = node.get("invoice");
			if (iNode.get("id") != null) {
				Long id = iNode.get("id").asLong();
				invoice = new Invoice(id);
				invoice.setInvoiceItems(new ArrayList<>());
			}
		}
		
		BigDecimal dsummPaid = null;
		if (node.get("dsummPaid") != null) {
			dsummPaid = new BigDecimal(node.get("dsummPaid").asDouble());
		}
		
		BigDecimal wsummPaid = null;
		if (node.get("wsummPaid") != null) {
			wsummPaid = new BigDecimal(node.get("wsummPaid").asDouble());
		}
		
		Boolean closed = null;
		if (node.get("closed") != null) {
			closed = node.get("closed").asBoolean();
		}
		
		Boolean forbuh = null;
		if (node.get("forbuh") != null) {
			forbuh = node.get("forbuh").asBoolean();
		}
		
		String trcode = "";
		if (node.get("trcode") != null) {
			trcode = node.get("trcode").asText();
		}
		
		Integer order = null;
		if (node.get("order") != null) {
			order = node.get("order").asInt();
		}
		
		String refkeyMain = "";
		if (node.get("refkeyMain") != null) {
			refkeyMain = node.get("refkeyMain").asText();
		}
		
		Boolean storno = null;
		if (node.get("storno") != null) {
			storno = node.get("storno").asBoolean();
		}
		
		FinDoc findoc = new FinDoc(company, branch, title, info, docno, year, refkey, reftyp, month, finDocType, null,
				docDate, department, storno, parentFindDoc, stDocno, stYear, user, party, contract, order, isMain, dsumm,
				dcurrency, rate, wsumm, wcurrency, dsummPaid, wsummPaid, closed, forbuh, trcode, invoice, refkeyMain, null);
		
		// Deserializing finEntries
		List<FinEntry> finEntries = new ArrayList<>();
		if (node.get("finEntries") != null) {
			JsonNode itemsNode = node.get("finEntries");

			for (Iterator<JsonNode> i = itemsNode.iterator(); i.hasNext();) {
				JsonNode itemNode = i.next();

				Branch feBranch = null;
				if (itemNode.get("branch") != null) {
					JsonNode iNode = itemNode.get("branch");
					if (iNode.get("id") != null) {
						Integer brId = iNode.get("id").asInt();
						feBranch = new Branch(brId);
					}
				}

				FinCashflowStatement cfStt = null;
				if (itemNode.get("cashflowStatement") != null) {
					JsonNode iNode = itemNode.get("cashflowStatement");
					if (iNode.get("id") != null) {
						Integer id = iNode.get("id").asInt();
						cfStt = new FinCashflowStatement(id);
					}
				}

				Integer entrycount = null;
				if (itemNode.get("entrycount") != null) {
					entrycount = itemNode.get("entrycount").asInt();
				}

				FinActivityType finActType = null;
				if (itemNode.get("finActType") != null) {
					JsonNode iNode = itemNode.get("finActType");
					if (iNode.get("code") != null) {
						String code = iNode.get("code").asText();
						finActType = new FinActivityType(code);
					}
				}

				FinGlAccount glAccount = null;
				if (itemNode.get("glAccount") != null) {
					JsonNode iNode = itemNode.get("glAccount");
					if (iNode.get("code") != null) {
						String code = iNode.get("code").asText();
						glAccount = new FinGlAccount(code);
					}
				}
				
				BigDecimal feDsumm = null;
				if (itemNode.get("dsumm") != null) {
					feDsumm = new BigDecimal(itemNode.get("dsumm").asDouble());
				}

				BigDecimal feWsumm = null;
				if (itemNode.get("wsumm") != null) {
					feWsumm = new BigDecimal(itemNode.get("wsumm").asDouble());
				}
				
				BigDecimal feRate = null;
				if (itemNode.get("rate") != null) {
					feRate = new BigDecimal(itemNode.get("rate").asDouble());
				}
				
				Currency feDCurrency = null;
				if (itemNode.get("dcurrency") != null) {
					JsonNode brNode = itemNode.get("dcurrency");
					if (brNode.get("currency") != null) {
						String cur = brNode.get("currency").asText();
						feDCurrency = new Currency(cur);
					}
				}

				Currency feWCurrency = null;
				if (itemNode.get("wcurrency") != null) {
					JsonNode brNode = itemNode.get("wcurrency");
					if (brNode.get("currency") != null) {
						String cur = brNode.get("currency").asText();
						feWCurrency = new Currency(cur);
					}
				}
				
				Boolean isTovar = null;
				if (itemNode.get("isTovar") != null) {
					isTovar = itemNode.get("isTovar").asBoolean();
				}

				KassaBank kassaBank = null;
				if (itemNode.get("kassaBank") != null) {
					JsonNode iNode = itemNode.get("kassaBank");
					if (iNode.get("id") != null) {
						Integer id = iNode.get("id").asInt();
						kassaBank = new KassaBank(id);
						if (iNode.get("currency") != null) {
							JsonNode jNode = iNode.get("currency");
							if (jNode.get("currency") != null) {
								String cur = jNode.get("currency").asText();
								kassaBank.setCurrency(new Currency(cur));
							}
						}
					}
				}

				String postkey = "";
				if (itemNode.get("postkey") != null) {
					postkey = itemNode.get("postkey").asText();
				}

				String dc = "";
				if (itemNode.get("dc") != null) {
					dc = itemNode.get("dc").asText();
				}
				
				FinDoc finDoc = null;
				if (itemNode.get("finDoc") != null) {
					JsonNode iNode = itemNode.get("finDoc");
					if (iNode.get("id") != null) {
						Long id = iNode.get("id").asLong();
						finDoc = new FinDoc(id);
					}
				}
				
				Inventory inventory = null;
				if (itemNode.get("inventory") != null) {
					JsonNode iNode = itemNode.get("inventory");
					if (iNode.get("id") != null) {
						Integer id = iNode.get("id").asInt();
						inventory = new Inventory(id);
						String invName = iNode.get("name").asText();
						inventory.setName(invName);
					}
				}
				
				BigDecimal quantity = null;
				if (itemNode.get("quantity") != null) {
					quantity = new BigDecimal(itemNode.get("quantity").asDouble());
				}

				Unit unit = null;
				if (itemNode.get("unit") != null) {
					JsonNode iNode = itemNode.get("unit");
					if (iNode.get("name") != null) {
						String name = iNode.get("name").asText();
						unit = new Unit(name);
					}
				}
				
				InvoiceItem invoiceItem = null;
				if (itemNode.get("invoiceItem") != null) {
					JsonNode iNode = itemNode.get("invoiceItem");
					if (iNode.get("id") != null) {
						Long id = iNode.get("id").asLong();
						invoiceItem = new InvoiceItem(id);
						
						List<StockIn> stocks = new ArrayList<>();
						
						if (iNode.get("childStockIns") != null) {
							JsonNode stockNode = iNode.get("childStockIns");

							for (Iterator<JsonNode> j = stockNode.iterator(); j.hasNext();) {
								JsonNode skNode = j.next();
								
								Long stockId = null;
								if (skNode.get("id") != null) {
									stockId = skNode.get("id").asLong();
								}
								BigDecimal stockQuantity = null;
								if (skNode.get("quantity") != null) {
									stockQuantity = new BigDecimal(skNode.get("quantity").asDouble());
								}
								String stockGlCode = null;
								if (skNode.get("glCode") != null) {
									stockGlCode = skNode.get("glCode").asText();
								}
								String serNumber = null;
								if (skNode.get("serialNumber") != null) {
									serNumber = skNode.get("serialNumber").asText();
								}
								BigDecimal stockRate = null;
								if (skNode.get("rate") != null) {
									stockRate = new BigDecimal(skNode.get("rate").asDouble());
								}
								StockStatus genStatus = null;
								if (skNode.get("genStatus") != null) {
									JsonNode stockInNode = skNode.get("genStatus");
									if (iNode.get("id") != null) {
										Integer stockInId = stockInNode.get("id").asInt();
										genStatus = new StockStatus(stockInId);
									}
								}
								StockStatus intStatus = null;
								if (skNode.get("intStatus") != null) {
									JsonNode stockInNode = skNode.get("intStatus");
									if (stockInNode.get("id") != null) {
										Integer stockInId = stockInNode.get("id").asInt();
										intStatus = new StockStatus(stockInId);
									}
								}
								
								String stockRefkey = null;
								if (skNode.get("refkey") != null) {
									stockRefkey = skNode.get("refkey").asText();
								}
								
								Currency stockWcurrency = null;
								if(skNode.get("wcurrency") != null) {
									JsonNode cn3 = skNode.get("wcurrency");
									if (cn3.get("currency") != null) {
										stockWcurrency = new Currency();
										String currencyStr = cn3.get("currency").asText();
										stockWcurrency.setCurrency(currencyStr);
									}	
								}
								
								
								BigDecimal stockWsumm = null;
								if (skNode.get("wsumm") != null) {
									stockWsumm = new BigDecimal(skNode.get("wsumm").asDouble());
								}
								
								BigDecimal stockDsumm = null;
								if (skNode.get("dsumm") != null) {
									stockDsumm = new BigDecimal(skNode.get("dsumm").asDouble());
								}
								
								Currency stockDcurrency = null;
								if(skNode.get("dcurrency") != null) {
									JsonNode cn3 = skNode.get("dcurrency");
									if (cn3.get("currency") != null) {
										stockDcurrency = new Currency();
										String currencyStr = cn3.get("currency").asText();
										stockDcurrency.setCurrency(currencyStr);
									}	
								}
								
								Date stockDateIn = null;
								if (skNode.get("dateIn") != null) {
									String ddStr = skNode.get("dateIn").asText();
									try {
										if (ddStr.length() > 10)
											stockDateIn = df1.parse(ddStr);
										else
											stockDateIn = df2.parse(ddStr);
									} catch (ParseException e) {
										e.printStackTrace();
									}
								}
								
								Company siCompany = null;
								if(skNode.get("company") != null) {
									JsonNode cn3 = skNode.get("company");
									if (cn3.get("id") != null) {
										Integer cid = cn3.get("id").asInt();
										siCompany = new Company(cid);
									}
								} 
								
								Branch siBranch = null;
								if(skNode.get("branch") != null) {
									JsonNode cn3 = skNode.get("branch");
									if (cn3.get("id") != null) {
										Integer bid = cn3.get("id").asInt();
										siBranch = new Branch(bid);
									}
								}
								
								Inventory siInventory = null;
								if(skNode.get("inventory") != null) {
									JsonNode cn3 = skNode.get("inventory");
									if (cn3.get("id") != null) {
										Integer bid = cn3.get("id").asInt();
										siInventory = new Inventory(bid);
									}
								}
								
								Unit siUnit = null;
								if (skNode.get("unit") !=null) {
									JsonNode cn3 = skNode.get("unit");
									if (cn3.get("name") != null) {
										String skUnit = cn3.get("name").asText();
										siUnit = new Unit(skUnit);
									}
								}
								
								Boolean broken = null;
								if (skNode.get("broken") != null) {
									broken = skNode.get("broken").asBoolean();
								}
								
								StockIn stock = new StockIn(stockId, siCompany, siBranch, siInventory, stockQuantity, siUnit, invoiceItem,  serNumber,
										genStatus, intStatus, broken, stockRefkey, stockWsumm, stockWcurrency, stockDsumm, stockDcurrency, stockRate, stockDateIn, stockGlCode);
								stocks.add(stock);
							}
							
							invoiceItem.setChildStockIns(stocks);
							
						}
					}
				}
				if (invoiceItem != null) {
					invoice.getInvoiceItems().add(invoiceItem);					
				}
				
				String itemName = "";
				if (itemNode.get("itemName") != null) {
					itemName = itemNode.get("itemName").asText();
				}
				
				BigDecimal price = null;
				if (itemNode.get("price") != null) {
					price = new BigDecimal(itemNode.get("price").asDouble());
				}
				
				String feInfo = "";
				if (itemNode.get("info") != null) {
					feInfo = itemNode.get("info").asText();
				}
				
				FinEntry fe = new FinEntry(company, feBranch, finDoc, postkey, party, dc, glAccount, feDsumm, feDCurrency, feRate, feWsumm,
						feWCurrency, feInfo, docno, month, year, docDate, refkey, cfStt, finActType, isTovar, itemName,
						inventory, quantity, unit, price, entrycount, department, invoiceItem, kassaBank);
				
				if (finDoc == null || finDoc.getId() == null) {
					fe.setFinDoc(findoc);					
				}
				
				finEntries.add(fe);
			}
		}
		
		findoc.setFinEntries(finEntries);
		findoc.setInvoice(invoice);
		
		return findoc;
	}

}
