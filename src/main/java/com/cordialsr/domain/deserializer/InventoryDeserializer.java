package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.cordialsr.domain.Company;
import com.cordialsr.domain.InvFno;
import com.cordialsr.domain.InvMainCategory;
import com.cordialsr.domain.InvSubCategory;
import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.InvoiceItem;
import com.cordialsr.domain.Manufacturer;
import com.cordialsr.domain.Unit;
import com.cordialsr.general.GeneralUtil;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class InventoryDeserializer extends JsonDeserializer<Inventory>{
	
	private GeneralUtil gu = new GeneralUtil();
	
	@Override
	public Inventory deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp), ptNode;
		ObjectMapper jsonObjectMapper = new ObjectMapper();
		
		Integer invId = null;
		if (node.get("id") != null) {
			invId = node.get("id").asInt();			
		}
		
		String name= "";
		if (node.get("name") != null) 
			name = node.get("name").asText();
		
		String model= "";
		if (node.get("model") != null) 
			model = node.get("model").asText();
		
		String code= "";
		if (node.get("code") != null) 
			code = node.get("code").asText();
		System.out.println(code);
		
		
		BigDecimal volumeCbm = null;
		if (node.get("volumeCbm") != null) {
			Double temp = node.get("volumeCbm").asDouble();
			volumeCbm = new BigDecimal(node.get("volumeCbm").asDouble());
		}
		
		BigDecimal weightKg = null;
		if (node.get("weightKg") != null) {
			Double temp = node.get("weightKg").asDouble();
			weightKg = new BigDecimal(node.get("weightKg").asDouble());
		}
					
		Company company = new Company();
		if(node.get("company") != null) {
			ptNode = node.get("company");
			int id = ptNode.get("id").asInt();
			company.setId(id);
		} 
		
		InvMainCategory invMainCategory = new InvMainCategory();
		if(node.get("invMainCategory") != null) {
			ptNode = node.get("invMainCategory");
			int id = ptNode.get("id").asInt();
			invMainCategory.setId(id);
		} 
		
		InvSubCategory invSubCategory = new InvSubCategory();
		if(node.get("invSubCategory") != null) {
			ptNode = node.get("invSubCategory");
			int id = ptNode.get("id").asInt();
			invSubCategory.setId(id);
		}
		
		
		Manufacturer manufacturer =  new Manufacturer();
		if(node.get("manufacturer") != null) {
			ptNode = node.get("manufacturer");
			String nameMn = ptNode.get("name").asText();
			manufacturer.setName(nameMn);
		}
		
		Unit unit = new Unit();
		if(node.get("unit") != null) {
			ptNode = node.get("unit");
			String nameUn = ptNode.get("name").asText();
			unit.setName(nameUn);
		}
		
		
			
	Inventory inventory = new Inventory(company, invMainCategory, invSubCategory, null, null, name, model, code, volumeCbm, weightKg);
	inventory.setId(invId);
	
	List<Inventory> parents = new ArrayList<>();
	if (node.get("parents") != null) {
		JsonNode itemsNode = node.get("parents"), tempNode;
		
		for (Iterator<JsonNode> i = itemsNode.iterator(); i.hasNext();) {
			Inventory si = new Inventory();
			
			JsonNode itemNode = i.next();
			
			Integer iiId = null;
			if (itemNode.get("id") != null) {
				iiId = itemNode.get("id").asInt();			
			}
			
			si.setId(iiId);
			parents.add(si);
		}
		inventory.setParents(parents);
	}
	
	
	List<InvFno> invFno = new ArrayList<>();
	if (node.get("invFno") != null) {
		JsonNode itemsNode = node.get("invFno"), tempNode;
		
		for (Iterator<JsonNode> i = itemsNode.iterator(); i.hasNext();) {
			
			JsonNode itemNode = i.next();
			
			Integer fno = null;
			if (itemNode.get("fno") != null) {
				fno = itemNode.get("fno").asInt();
			}
			
			InvFno si = new InvFno(inventory, fno);
			
			Long iiId = null;
			if (itemNode.get("id") != null) {
				iiId = itemNode.get("id").asLong();
				si.setId(iiId);
			}
			
			invFno.add(si);
		}
		inventory.setInvFno(invFno);
	}
		
//   if(!gu.isEmptyInteger(parentInventory.getId())) {
//			inventory.setParentInventory(parentInventory);
//		}
	if(!gu.isEmptyString(manufacturer.getName())) {
		inventory.setManufacturer(manufacturer);
	}
	if(!gu.isEmptyString(unit.getName())) {
		inventory.setUnit(unit);
	}
	return inventory;
	}
	
}
