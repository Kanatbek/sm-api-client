package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractAwardSchedule;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.Payroll;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PayrollDeserializer extends JsonDeserializer<Payroll> {

	@Override
	public Payroll deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp), ptNode;
		ObjectMapper jsonObjectMapper = new ObjectMapper();

		Company company = null;
		if (node.get("company") != null) {
			JsonNode node2 = node.get("company");
			if (node2.get("id") != null) {
				Integer id = node2.get("id").asInt();
				company = new Company(id);
			}
		}

		Branch branch = null;
		if (node.get("branch") != null) {
			JsonNode brNode = node.get("branch");
			if (brNode.get("id") != null) {
				Integer brId = brNode.get("id").asInt();
				branch = new Branch(brId);
			}
		}

		Party party = null;
		if (node.get("party") != null) {
			JsonNode iNode = node.get("party");
			if (iNode.get("id") != null) {
				Long id = iNode.get("id").asLong();
				party = new Party(id);
			}
		}
		
		Employee employee = null;
		if (node.get("employee") != null) {
			JsonNode iNode = node.get("employee");
			if (iNode.get("id") != null) {
				Long id = iNode.get("id").asLong();
				employee = new Employee(id);
			}
		}
		
		Contract contract = null;
		if (node.get("contract") != null) {
			JsonNode iNode = node.get("contract");
			
			String contractNumber = null;
			if (iNode.get("contractNumber") != null) {
				contractNumber = iNode.get("contractNumber").asText();
			}
			
			String refkey = null;
			if (iNode.get("refkey") != null) {
				refkey = iNode.get("refkey").asText();
			}
			
			Long conId = null; 
			if (iNode.get("id") != null) {
				conId = iNode.get("id").asLong();
				contract = new Contract(conId);
				contract.setContractNumber(contractNumber);
				contract.setRefkey(refkey);
			}
		}
		
		ContractAwardSchedule conAward = null;
		if (node.get("conAward") != null) {
			JsonNode iNode = node.get("conAward");
			if (iNode.get("id") != null) {
				Long id = iNode.get("id").asLong();
				conAward = new ContractAwardSchedule(id);
			}
		}
		
		String kind = null;
		if (node.get("kind") != null) {
			kind = node.get("kind").asText();
		}
		
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
//		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");

		Date payrollDate = null;
		if (node.get("payrollDate") != null) {
			String ddStr = node.get("payrollDate").asText();
			try {
				if (ddStr.length() > 10)
					payrollDate = df1.parse(ddStr);
				else if (ddStr.length() > 7)
					payrollDate = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		Date postedDate = null;
		if (node.get("postedDate") != null) {
			String ddStr = node.get("postedDate").asText();
			try {
				if (ddStr.length() > 10)
					postedDate = df1.parse(ddStr);
				else if (ddStr.length() > 7)
					postedDate = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		BigDecimal sum = null;
		if (node.get("sum") != null) {
			sum = new BigDecimal(node.get("sum").asDouble());
		}
		
		Currency currency = null;
		if (node.get("currency") != null) {
			JsonNode brNode = node.get("currency");
			if (brNode.get("currency") != null) {
				String cur = brNode.get("currency").asText();
				currency = new Currency(cur);
			}
		}
		
		Boolean approved = null;
		if (node.get("approved") != null) {
			approved = node.get("approved").asBoolean();
		}
		
		Boolean storno = null;
		if (node.get("storno") != null) {
			storno = node.get("storno").asBoolean();
		}
		

		Boolean restricted = null;
		if (node.get("restricted") != null) {
			restricted = node.get("restricted").asBoolean();
		}
		
		
		Boolean posted = null;
		if (node.get("posted") != null) {
			posted = node.get("posted").asBoolean();
		}
		
		String refkey = null;
		if (node.get("refkey") != null) {
			refkey = node.get("refkey").asText();
		}
		
		String info = null;
		if (node.get("info") != null) {
			info = node.get("info").asText();
		}
		
		Integer month = null;
		if (node.get("month") != null) {
			month = node.get("month").asInt();
		}
		
		Integer year = null;
		if (node.get("year") != null) {
			year = node.get("year").asInt();
		}
		
		String trCode = null;
		if (node.get("trCode") != null) {
			trCode = node.get("trCode").asText();
		}
		
		User user = null;
		if (node.get("user") != null) {
			JsonNode iNode = node.get("user");
			if (iNode.get("userid") != null) {
				Long userid = iNode.get("userid").asLong();
				user = new User(userid);
			}
		}
		
		BigDecimal rate = null;
		if (node.get("rate") != null) {
			rate = new BigDecimal(node.get("rate").asDouble());
		}
		
		BigDecimal accrued = null;
		if (node.get("accrued") != null) {
			accrued = new BigDecimal(node.get("accrued").asDouble());
		}
		
		Long id = null;
		if (node.get("id") != null) {
			id = node.get("id").asLong();
		}
		
		Payroll payroll = new Payroll(id, company, branch, party, employee, kind, contract, conAward, payrollDate, postedDate, sum, currency, 
										approved, restricted, posted, refkey, info, month, year, trCode, user, rate, accrued);
		
		payroll.setStorno(storno);
		return payroll;
	}
}
