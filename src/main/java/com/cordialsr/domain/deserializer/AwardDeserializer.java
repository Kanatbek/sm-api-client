package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.City;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Country;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Department;
import com.cordialsr.domain.Award;
import com.cordialsr.domain.AwardCase;
import com.cordialsr.domain.AwardTemplate;
import com.cordialsr.domain.AwardType;
import com.cordialsr.domain.InvMainCategory;
import com.cordialsr.domain.InvSubCategory;
import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.Position;
import com.cordialsr.domain.Region;
import com.cordialsr.domain.Scope;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AwardDeserializer extends JsonDeserializer<Award> {

	@Override
	public Award deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);

		Integer awardId = null;
		if (node.get("id") != null) {
			awardId = node.get("id").asInt();
		}
		
		AwardCase awardCase = null;
		if (node.get("awardCase") != null) {
			JsonNode node2 = node.get("awardCase");
			if (node2.get("code") != null) {
				String awCode = node2.get("code").asText();
				awardCase = new AwardCase(awCode);
			}
		}

		AwardType awardType = null;
		if (node.get("awardType") != null) {
			JsonNode node2 = node.get("awardType");
			if (node2.get("name") != null) {
				String strname = node2.get("name").asText();
				awardType = new AwardType(strname);
			}
		}

		Branch branch = null;
		if (node.get("branch") != null) {
			JsonNode brNode = node.get("branch");
			if (brNode.get("id") != null) {
				Integer brId = brNode.get("id").asInt();
				branch = new Branch(brId);
			}
		}

		City city = null;
		if (node.get("city") != null) {
			JsonNode brNode = node.get("city");
			if (brNode.get("id") != null) {
				Integer brId = brNode.get("id").asInt();
				city = new City(brId);
			}
		}

		Company company = null;
		if (node.get("company") != null) {
			JsonNode brNode = node.get("company");
			if (brNode.get("id") != null) {
				Integer brId = brNode.get("id").asInt();
				company = new Company(brId);
			}
		}

		Country country = null;
		if (node.get("country") != null) {
			JsonNode brNode = node.get("country");
			if (brNode.get("id") != null) {
				Integer brId = brNode.get("id").asInt();
				country = new Country(brId);
			}
		}

		Currency currency = null;
		if (node.get("currency") != null) {
			JsonNode curNode = node.get("currency");
			if (curNode.get("currency") != null) {
				String cur = curNode.get("currency").asText();
				currency = new Currency(cur);
			}
		}

		InvMainCategory invMainCategory = null;
		if (node.get("invMainCategory") != null) {
			JsonNode brNode = node.get("invMainCategory");
			if (brNode.get("id") != null) {
				Integer brId = brNode.get("id").asInt();
				invMainCategory = new InvMainCategory(brId);
			}
		}

		InvSubCategory invSubCategory = null;
		if (node.get("invSubCategory") != null) {
			JsonNode brNode = node.get("invSubCategory");
			if (brNode.get("id") != null) {
				Integer brId = brNode.get("id").asInt();
				invSubCategory = new InvSubCategory(brId);
			}
		}

		Inventory inventory = null;
		if (node.get("inventory") != null) {
			JsonNode brNode = node.get("inventory");
			if (brNode.get("id") != null) {
				Integer invId = brNode.get("id").asInt();
				inventory = new Inventory(invId);
			}
		}

		Position position = null;
		if (node.get("position") != null) {
			JsonNode brNode = node.get("position");
			if (brNode.get("id") != null) {
				Integer brId = brNode.get("id").asInt();
				position = new Position(brId);
			}
		}

		Region region = null;
		if (node.get("region") != null) {
			JsonNode brNode = node.get("region");
			if (brNode.get("id") != null) {
				Integer brId = brNode.get("id").asInt();
				region = new Region(brId);
			}
		}

		Scope scope = null;
		if (node.get("scope") != null) {
			JsonNode brNode = node.get("scope");
			if (brNode.get("name") != null) {
				String brId = brNode.get("name").asText();
				scope = new Scope(brId);
			}
		}

		// DATA
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");

		Date dateStart = null;
		if (node.get("dateStart") != null) {
			String ddStr = node.get("dateStart").asText();
			try {
				if (ddStr.length() > 10)
					dateStart = df1.parse(ddStr);
				else
					dateStart = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		Date dateEnd = null;
		if (node.get("dateEnd") != null) {
			String ddStr = node.get("dateEnd").asText();
			try {
				if (ddStr.length() > 10)
					dateEnd = df1.parse(ddStr);
				else
					dateEnd = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		// min
		BigDecimal min = null;
		if (node.get("min") != null) {
			min = new BigDecimal(node.get("min").asDouble());
		}

		// max
		BigDecimal max = null;
		if (node.get("max") != null) {
			max = new BigDecimal(node.get("max").asDouble());
		}

		// Coefficient
		BigDecimal coefficient = null;
		if (node.get("coefficient") != null) {
			coefficient = new BigDecimal(node.get("coefficient").asDouble());
		}

		// summ
		BigDecimal summ = null;
		if (node.get("summ") != null) {
			summ = new BigDecimal(node.get("summ").asDouble());
		}
		
		// premiDev
		Integer premiDiv = null;
		if (node.get("premiDiv") != null) {
			premiDiv = new Integer(node.get("premiDiv").asInt());
		}

		// userCreated
		User userCreated = null;
		if (node.get("userCreated") != null) {
			JsonNode usNode = node.get("userCreated");
			if (usNode.get("userid") != null) {
				Long getId = usNode.get("userid").asLong();
				userCreated = new User(getId);
			}

		}
		
		Department department = null;
		if (node.get("department") != null) {
			JsonNode usNode = node.get("department");
			if (usNode.get("id") != null) {
				Integer getId = usNode.get("id").asInt();
				department = new Department(getId);
			}

		}

		Boolean forRent = null;
		if (node.get("forRent") != null) {
			forRent = node.get("forRent").asBoolean();
		}

		Boolean forSale = null;
		if (node.get("forSale") != null) {
			forSale = node.get("forSale").asBoolean();
		}

		Boolean enabled = null;
		if (node.get("enabled") != null) {
			enabled = node.get("enabled").asBoolean();
		}
		
		Award awardD = new Award(awardId, awardCase, awardType, branch, city, company, country, currency, invMainCategory,
				invSubCategory, inventory, position, region, scope, dateStart, dateEnd, min, max, coefficient, summ, premiDiv, null,
				userCreated, forRent, forSale, department, enabled);

		// Deserializing AwardTemplate
		List<AwardTemplate> awardTemplate = new ArrayList<>();
		if (node.get("awardTemplates") != null) {
			JsonNode itemsNode = node.get("awardTemplates");

			for (Iterator<JsonNode> i = itemsNode.iterator(); i.hasNext();) {
				JsonNode itemNode = i.next();

				Award Taward = null;
				if (itemNode.get("award") != null) {
					JsonNode node3 = itemNode.get("award");
					if (node3.get("id") != null) {
						Integer tId = node3.get("id").asInt();
						Taward = new Award(tId);
					}
				}

				Integer payrollMonth= null;
				if (itemNode.get("payrollMonth") != null) {
					payrollMonth = new Integer(itemNode.get("payrollMonth").asInt());
				}

				Integer month = null;
				if (itemNode.get("month") != null) {
					month = new Integer(itemNode.get("month").asInt());
				}

				BigDecimal Tsumm = null;
				if (itemNode.get("summ") != null) {
					Tsumm = new BigDecimal(itemNode.get("summ").asInt());
				}
				
				BigDecimal revertSumm = null;
				if (itemNode.get("revertSumm") != null) {
					revertSumm = new BigDecimal(itemNode.get("revertSumm").asDouble());
				}
				
				Boolean revertOnCancel = null;
				if (itemNode.get("revertOnCancel") != null) {
					revertOnCancel = itemNode.get("revertOnCancel").asBoolean();
				}
				
				AwardTemplate awardT = new AwardTemplate(Taward, payrollMonth, month, Tsumm, revertOnCancel, revertSumm);
				awardTemplate.add(awardT);
			}
		}

		awardD.setAwardTemplates(awardTemplate);
		return awardD;
	}

}
