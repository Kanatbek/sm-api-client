package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.cordialsr.domain.Address;
import com.cordialsr.domain.AwardType;
import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.ConItemStatus;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractAwardSchedule;
import com.cordialsr.domain.ContractHistory;
import com.cordialsr.domain.ContractItem;
import com.cordialsr.domain.ContractPaymentSchedule;
import com.cordialsr.domain.ContractPromos;
import com.cordialsr.domain.ContractStatus;
import com.cordialsr.domain.ContractType;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.InvMainCategory;
import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.PaymentStatus;
import com.cordialsr.domain.PhoneNumber;
import com.cordialsr.domain.Position;
import com.cordialsr.domain.PriceList;
import com.cordialsr.domain.Promotion;
import com.cordialsr.domain.ServiceCategory;
import com.cordialsr.domain.Unit;
import com.cordialsr.domain.security.User;
import com.cordialsr.general.GeneralUtil;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ContractDeserializer extends JsonDeserializer<Contract> {

	@Override
	public Contract deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp), ptNode;
		ObjectMapper jsonObjectMapper = new ObjectMapper();

		Long conId = null;
		if (node.get("id") != null) {
			conId = node.get("id").asLong();
		}
		
		Company company = null;
		if (node.get("company") != null) {
			JsonNode node2 = node.get("company");
			if (node2.get("id") != null) {
				Integer id = node2.get("id").asInt();
				company = new Company(id);	
			}
		}

		Branch branch = null;
		if (node.get("branch") != null) {
			JsonNode brNode = node.get("branch");
			if (brNode.get("id") != null) {
				Integer brId = brNode.get("id").asInt();
				branch = new Branch(brId);
			}
		}
		
		Branch serviceBranch = null;
		if (node.get("serviceBranch") != null) {
			JsonNode brNode = node.get("serviceBranch");
			if (brNode.get("id") != null) {
				Integer brId = brNode.get("id").asInt();
				serviceBranch = new Branch(brId);
			}
		}

		ContractType contractType = null;
		if (node.get("contractType") != null) {
			JsonNode Node = node.get("contractType");
			if (Node.get("id") != null) {
				Integer Id = Node.get("id").asInt();
				contractType = new ContractType(Id);
			}
		}

		ContractStatus contractStatus = null;
		if (node.get("contractStatus") != null) {
			JsonNode brNode = node.get("contractStatus");
			if (brNode.get("id") != null) {
				Integer brId = brNode.get("id").asInt();
				contractStatus = new ContractStatus(brId);
			}
		}

		
		Employee employeeByCoordinator = null;
		if(node.get("coordinator") != null) {
			JsonNode EmNode = node.get("coordinator");
			if (EmNode.get("id") != null) {
				Long id = EmNode.get("id").asLong();
				employeeByCoordinator = new Employee(id);
			}
		}
		
		Employee demosec = null;
		if (node.get("demosec") != null) {
			JsonNode brNode = node.get("demosec");
			if (brNode.get("id") != null) {
				Long emplId = brNode.get("id").asLong();
				demosec = new Employee(emplId);
			}
		}

		Employee director = null;
		if (node.get("director") != null) {
			JsonNode brNode = node.get("director");
			if (brNode.get("id") != null) {
				Long brId = brNode.get("id").asLong();
				director = new Employee(brId);
			}
		}
		
		Employee manager = null;
		if (node.get("manager") != null) {
			JsonNode brNode = node.get("manager");
			if (brNode.get("id") != null) {
				Long brId = brNode.get("id").asLong();
				manager = new Employee(brId);
			}
		}
		
		Employee careman = null;
		if (node.get("careman") != null) {
			JsonNode brNode = node.get("careman");
			if (brNode.get("id") != null) {
				Long brId = brNode.get("id").asLong();
				careman = new Employee(brId);
			}
		}
		
		Employee dealer = null;
		if (node.get("dealer") != null) {
			JsonNode brNode = node.get("dealer");			
			if (brNode.get("id") != null) {
				Long brId = brNode.get("id").asLong();
				dealer = new Employee(brId);
			}
		}
		
		Employee fitter = null;
		if (node.get("fitter") != null) {
			JsonNode brNode = node.get("fitter");
			if (brNode.get("id") != null) {
				Long brId = brNode.get("id").asLong();
				fitter = new Employee(brId);
			}
		}
		
		Employee collector = null;
		if (node.get("collector") != null) {
			JsonNode brNode = node.get("collector");
			if (brNode.get("id") != null) {
				Long brId = brNode.get("id").asLong();
				collector = new Employee(brId);
			}
		}
		
		Party customer = null;
		if (node.get("customer") != null) {
			JsonNode brNode = node.get("customer");
			if (brNode.get("id") != null) {
				Long brId = brNode.get("id").asLong();
				customer = new Party(brId);	
			}			
		}
		
		Address addrPay = null;
		if (node.get("addrPay") != null) {
			JsonNode iNode = node.get("addrPay");
			if (iNode.get("id") != null) {
				Long iid = iNode.get("id").asLong();
				addrPay = new Address(iid);	
			}			
		}
		
		PhoneNumber phonePay = null;
		if (node.get("phonePay") != null) {
			JsonNode iNode = node.get("phonePay");
			if (iNode.get("id") != null) {
				Long iid = iNode.get("id").asLong();
				phonePay = new PhoneNumber(iid);	
			}			
		}
		
		PhoneNumber mobilePay = null;
		if (node.get("mobilePay") != null) {
			JsonNode iNode = node.get("mobilePay");
			if (iNode.get("id") != null) {
				Long iid = iNode.get("id").asLong();
				mobilePay = new PhoneNumber(iid);	
			}			
		}

		Party exploiter = null;
		if (node.get("exploiter") != null) {
			JsonNode iNode = node.get("exploiter");
			if (iNode.get("id") != null) {
				Long iid = iNode.get("id").asLong();
				exploiter = new Party(iid);	
			}			
		}
		
		Address addrFact = null;
		if (node.get("addrFact") != null) {
			JsonNode iNode = node.get("addrFact");
			if (iNode.get("id") != null) {
				Long iid = iNode.get("id").asLong();
				addrFact = new Address(iid);
			}			
		}
		
		PhoneNumber phoneFact = null;
		if (node.get("phoneFact") != null) {
			JsonNode iNode = node.get("phoneFact");
			if (iNode.get("id") != null) {
				Long iid = iNode.get("id").asLong();
				phoneFact = new PhoneNumber(iid);	
			}			
		}
		
		PhoneNumber mobileFact = null;
		if (node.get("mobileFact") != null) {
			JsonNode iNode = node.get("mobileFact");
			if (iNode.get("id") != null) {
				Long iid = iNode.get("id").asLong();
				mobileFact = new PhoneNumber(iid);	
			}
		}
		
		PaymentStatus paymentStatus = null;
		if (node.get("paymentStatus") != null) {
			JsonNode brNode = node.get("paymentStatus");
			if (brNode.get("id") != null) {
				Integer brId = brNode.get("id").asInt();
				paymentStatus = new PaymentStatus(brId);
			}		
		}
		
		Inventory inventory = null;
		if (node.get("inventory") != null) {
			JsonNode brNode = node.get("inventory");
			if (brNode.get("id") != null) {
				Integer iId = brNode.get("id").asInt();
				inventory = new Inventory(iId);
			}		
		}
		
		String inventorySn = null;
		if (node.get("inventorySn") != null) {
			inventorySn = node.get("inventorySn").asText();
		}
		
		
		ServiceCategory serviceCategory = null;
		if (node.get("serviceCategory") != null) {
			JsonNode brNode = node.get("serviceCategory");
			if (brNode.get("id") != null) {
				Integer brId = brNode.get("id").asInt();
				serviceCategory = new ServiceCategory(brId);		
			}
		}
		
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");

		Date dateSigned = null;
		if (node.get("dateSigned") != null) {
			String ddStr = node.get("dateSigned").asText();
			try {
				if (ddStr.length() > 10)
					dateSigned = df1.parse(ddStr);
				else if (ddStr.length() >= 6) {
					dateSigned = df2.parse(ddStr);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		String contractNumber = "";
		if (node.get("contractNumber") != null) {
			contractNumber = node.get("contractNumber").asText();
		}
		
		String refkey = "";
		if (node.get("refkey") != null) {
			refkey = node.get("refkey").asText();
		}
		
		Calendar registeredDate = null;
		if (node.get("registeredDate") != null) {
			String ddStr = node.get("registeredDate").asText();
			try {
				if (ddStr.length() > 10) {
					registeredDate = Calendar.getInstance();
					registeredDate.setTime(df1.parse(ddStr));
					
				}
				else if (ddStr.length() >= 6) {
					registeredDate = Calendar.getInstance();
					registeredDate.setTime(df2.parse(ddStr));
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
	
		
		User registeredUser = null;
		if (node.get("registeredUser") != null) {
			JsonNode usNode = node.get("registeredUser");
			if (usNode.get("userid") != null) {
				Long getId = usNode.get("userid").asLong();
				registeredUser = new User(getId);
			}
			
		}
		
		Calendar updatedDate = null;
		if (node.get("updatedDate") != null) {
			String ddStr = node.get("updatedDate").asText();
			try {
				if (ddStr.length() > 10) {
					updatedDate = Calendar.getInstance();
					updatedDate.setTime(df1.parse(ddStr));
				} else if (ddStr.length() >= 6) {
					updatedDate = Calendar.getInstance();
					updatedDate.setTime(df2.parse(ddStr));
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		User updatedUser = null;
		if (node.get("updatedUser") != null) {
			JsonNode usNode = node.get("updatedUser");
			if (usNode.get("userid") != null) {
				Long getId = usNode.get("userid").asLong();
				updatedUser = new User(getId);
			}
			
		}
		
		BigDecimal cost = null;
		if (node.get("cost") != null) {
			cost = new BigDecimal(node.get("cost").asDouble());
		}
		
		
		Integer month = null;
		if (node.get("month") != null) {
			month = new Integer(node.get("month").asInt());
		}
		
		BigDecimal discount = null;
		if (node.get("discount") != null) {
			discount = new BigDecimal(node.get("discount").asDouble());
		}
		
		
		BigDecimal summ = null;
		if (node.get("summ") != null) {
			summ = new BigDecimal(node.get("summ").asDouble());
		}
		
		Currency currency = null;
		if(node.get("currency") != null) {
			JsonNode ctNode = node.get("currency");
			if (ctNode.get("currency") != null) {
				String currencyStr = ctNode.get("currency").asText();
				currency = new Currency(currencyStr);
			}
		} 
		
		
		BigDecimal paid = null;
		if(node.get("paid") != null) {
			paid = new BigDecimal(node.get("paid").asDouble());
		}
		
		BigDecimal rate = null;
		if(node.get("rate") != null) {
			rate = new BigDecimal(node.get("rate").asDouble());
		}
		
		BigDecimal fromDealerSumm = null;
		if(node.get("fromDealerSumm") != null) {
			fromDealerSumm = new BigDecimal(node.get("fromDealerSumm").asDouble());
		}

		Boolean forbuh = null;
		if (node.get("forbuh") != null) {
			forbuh = node.get("forbuh").asBoolean();
		}
		
		Boolean isRent = null;
		if (node.get("isRent") != null) {
			isRent = node.get("isRent").asBoolean();
		}
		
		Boolean storno = null;
		if (node.get("storno") != null) {
			storno = node.get("storno").asBoolean();
		}
		
		Date stornoDate = null;
		if (node.get("stornoDate") != null) {
			String ddStr = node.get("stornoDate").asText();
			try {
				if (ddStr.length() > 10) {
					stornoDate = df1.parse(ddStr);
				} else if (ddStr.length() >= 6) {
					stornoDate = df2.parse(ddStr);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		Boolean resigned = null;
		if (node.get("resigned") != null) {
			resigned = node.get("resigned").asBoolean();
		}
		
		Date resignedDate = null;
		if (node.get("resignedDate") != null) {
			String ddStr = node.get("resignedDate").asText();
			try {
				if (ddStr.length() > 10)
					resignedDate = df1.parse(ddStr);
				else if (ddStr.length() >= 6) {
					resignedDate = df2.parse(ddStr);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		String info = null;
		if (node.get("info") != null) {
			info = node.get("info").asText();
		}
		
		Integer oldId = null;
		if (node.get("oldId") != null) {
			oldId = node.get("oldId").asInt();
		}
		
		Boolean verified = null;
		if (node.get("verified") != null) {
			verified = node.get("verified").asBoolean();
		}
		
		Boolean caremanSales = null;
		if (node.get("caremanSales") != null) {
			caremanSales = node.get("caremanSales").asBoolean();
		}
		
		Contract contract = new Contract(conId, serviceBranch, branch, company, contractStatus, contractType, 
				currency, employeeByCoordinator, demosec, director, 
				manager, careman, dealer, fitter, collector, customer, paymentStatus, 
				serviceCategory, dateSigned, contractNumber, refkey, registeredDate, registeredUser, updatedDate, cost, 
				month, discount, summ, paid, rate, fromDealerSumm, forbuh, isRent,
				addrPay, phonePay, mobilePay, exploiter, addrFact, phoneFact, mobileFact, inventory, inventorySn,
				null, null, null, null, null, info, storno, stornoDate, resigned, resignedDate, updatedUser, oldId, null, verified, caremanSales);
		  
		List<ContractHistory> contractHistory = new ArrayList<>();
		
		if (node.get("contractHistory") != null) {
			JsonNode itemsNode = node.get("contractHistory"), tempNode;
			
			for (Iterator<JsonNode> i = itemsNode.iterator(); i.hasNext();) {
				JsonNode itemNode = i.next();
				
				Long ids = null;
				if (itemNode.get("id") != null 
						&& !itemNode.get("id").asText().equals("null")) {
					ids = itemNode.get("id").asLong();			
				}
				
				Date updDate = null;
				if (itemNode.get("updatedDate") != null) {
					String ddStr = itemNode.get("updatedDate").asText();
					try {
						if (ddStr.length() > 10)
							updDate = df1.parse(ddStr);
						else
							updDate = df2.parse(ddStr);
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				
				User updUser = null;
				if (itemNode.get("updatedUser") != null) {
					JsonNode usNode = itemNode.get("updatedUser");
					if (usNode.get("userid") != null) {
						Long getId = usNode.get("userid").asLong();
						updUser = new User(getId);
					}
				}
				
				String oldValue = null;
				if (itemNode.get("oldValue") != null) {
					oldValue = itemNode.get("oldValue").asText();			
				}
				
				String newValue = null;
				if (itemNode.get("newValue") != null) {
					newValue = itemNode.get("newValue").asText();			
				}
				
				String field = null;
				if (itemNode.get("field") != null) {
					field = itemNode.get("field").asText();			
				}
				
				String title = null;
				if (itemNode.get("title") != null) {
					title = itemNode.get("title").asText();			
				}
				
				Long holdId = null;
				if (itemNode.get("oldId") != null 
						&& !itemNode.get("oldId").asText().equals("null")) {
					holdId = itemNode.get("oldId").asLong();			
				}
				
				Long newId = null;
				if (itemNode.get("newId") != null 
						&& !itemNode.get("newId").asText().equals("null")) {
					newId = itemNode.get("newId").asLong();			
				}
				
				ContractHistory conHis = new ContractHistory(ids, contract, field, title, updDate, updUser, oldValue, newValue, holdId, newId);
				contractHistory.add(conHis);
			}
			contract.setContractHistory(contractHistory);
		}
		
		//Payment Schedule
		List<ContractPaymentSchedule> paymentSchedule = new ArrayList<>();

		if (node.get("paymentSchedules") != null) {
			JsonNode itemsNode = node.get("paymentSchedules"), tempNode;
			
			for (Iterator<JsonNode> i = itemsNode.iterator(); i.hasNext();) {
				JsonNode itemNode = i.next();
				
				Long ids = null;
				if (itemNode.get("id") != null 
						&& !itemNode.get("id").asText().equals("null")) {
					ids = itemNode.get("id").asLong();			
				}
				
				String refkeys = null;
				if (itemNode.get("refkey") != null) {
					refkeys = itemNode.get("refkey").asText();			
				}
				
				Integer paymentOrder = null;
				if (itemNode.get("paymentOrder") != null) {
					paymentOrder = itemNode.get("paymentOrder").asInt();			
				}

				Boolean isFirstpayment = null;
				if (itemNode.get("isFirstpayment") != null) {
					isFirstpayment = itemNode.get("isFirstpayment").asBoolean();			
				}

				BigDecimal summ2 = null;
				if (itemNode.get("summ") != null) {
				summ2 = new BigDecimal(itemNode.get("summ").asDouble());
				}

				BigDecimal paid2 = null;
				if (itemNode.get("paid") != null) {
				paid2 = new BigDecimal(itemNode.get("paid").asDouble());
				}
				
				Date paymentDate = null;
				if (itemNode.get("paymentDate") != null) {
					String ddStr = itemNode.get("paymentDate").asText();
					try {
						if (ddStr.length() > 10)
							paymentDate = df1.parse(ddStr);
						else
							paymentDate = df2.parse(ddStr);
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				ContractPaymentSchedule paymentt = new ContractPaymentSchedule(
						ids, null, refkeys, paymentOrder, isFirstpayment, summ2, paid2, paymentDate);
				
				paymentSchedule.add(paymentt);
			}
			contract.setPaymentSchedules(paymentSchedule);
		}
		
		// Staff ContractAwardSchedules
		List<ContractAwardSchedule> cawSchedules = new ArrayList<>();
		if (node.get("cawSchedules") != null) {
			JsonNode itemsNode = node.get("cawSchedules"), tempNode;
			
			for (Iterator<JsonNode> i = itemsNode.iterator(); i.hasNext();) {
				JsonNode itemNode = i.next();
				
				Long ids = null;
				if (itemNode.get("id") != null 
						&& !itemNode.get("id").asText().equals("null")) {
					ids = itemNode.get("id").asLong();			
				}
				
				String refkeys = null;
				if (itemNode.get("refkey") != null) {
					refkeys = itemNode.get("refkey").asText();			
				}
				
				Boolean closed = null;
				if (itemNode.get("closed") != null) {
					closed = itemNode.get("closed").asBoolean();			
				}

				BigDecimal summ2 = null;
				if (itemNode.get("summ") != null) {
				summ2 = new BigDecimal(itemNode.get("summ").asDouble());
				}

				BigDecimal accrued = null;
				if (itemNode.get("accrued") != null) {
					accrued = new BigDecimal(itemNode.get("accrued").asDouble());
				}
				
				Date dateSchedule = null;
				if (itemNode.get("dateSchedule") != null) {
					String ddStr = itemNode.get("dateSchedule").asText();
					try {
						if (ddStr.length() > 10)
							dateSchedule = df1.parse(ddStr);
						else
							dateSchedule = df2.parse(ddStr);
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				
				Date dateAccrued = null;
				if (itemNode.get("dateAccrued") != null) {
					String ddStr = itemNode.get("dateAccrued").asText();
					try {
						if (ddStr.length() > 10)
							dateAccrued = df1.parse(ddStr);
						else if (ddStr.length() >= 6) {
							dateAccrued = df2.parse(ddStr);
						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				
				Currency currency2 = null;
				if(itemNode.get("currency") != null) {
					JsonNode ctNode = itemNode.get("currency");
					if (ctNode.get("currency") != null) {
						String currencyStr = ctNode.get("currency").asText();
						currency2 = new Currency(currencyStr);
					}
				}
				
				Party party = null;
				if (itemNode.get("party") != null) {
					JsonNode ctNode = itemNode.get("party");
					if (ctNode.get("id") != null) {
						Long id = ctNode.get("id").asLong();
						party = new Party(id);
					}
				}
				
				Position position = null;
				if (itemNode.get("position") != null) {
					JsonNode ctNode = itemNode.get("position");
					if (ctNode.get("id") != null) {
						Integer id = ctNode.get("id").asInt();
						position = new Position(id);
					}
				}
				
				AwardType awardType = null;
				if (itemNode.get("awardType") != null) {
					JsonNode ctNode = itemNode.get("awardType");
					if (ctNode.get("name") != null) {
						String name = ctNode.get("name").asText();
						awardType = new AwardType(name);
					}
				}
				
				Boolean revertOnCancel = null;
				if (itemNode.get("revertOnCancel") != null) {
					revertOnCancel = itemNode.get("revertOnCancel").asBoolean();
				}
				
				Integer porder = null;
				if (itemNode.get("porder") != null 
						&& !itemNode.get("porder").asText().equals("null")) {
					porder = itemNode.get("porder").asInt();			
				}
				
				BigDecimal revertSumm = null;
				if (itemNode.get("revertSumm") != null) {
					revertSumm = new BigDecimal(itemNode.get("revertSumm").asDouble());
				}
				
				BigDecimal deduction = null;
				if (itemNode.get("deduction") != null) {
					deduction = new BigDecimal(itemNode.get("deduction").asDouble());
				}
				
				ContractAwardSchedule cawSch = new ContractAwardSchedule(ids, contract, awardType, position, party, dateSchedule, dateAccrued, 
						summ2, currency2, accrued, refkey, closed, revertOnCancel, porder, revertSumm, deduction);
				
				cawSchedules.add(cawSch);
			}
			contract.setCawSchedules(cawSchedules);
		}

		// FinDocs
		List<FinDoc> finDocs = new ArrayList<>();
		if (node.get("finDocs") != null) {
			JsonNode itemsNode = node.get("finDocs"), tempNode;
			for (Iterator<JsonNode> i = itemsNode.iterator(); i.hasNext();) {
				JsonNode itemNode = i.next();
				Integer iiId = null;
				if (itemNode.get("id") != null) {
					iiId = itemNode.get("id").asInt();
					if (!GeneralUtil.isEmptyInteger(iiId)) {
						FinDoc si = new FinDoc();
						finDocs.add(si);
					}
				}
			}
			contract.setFinDocs(finDocs);
		}
		
		// Contract Items
		List<ContractItem> contractItems = new ArrayList<>();
		if (node.get("contractItems") != null) {
			JsonNode itemsNode = node.get("contractItems");
			for (Iterator<JsonNode> i = itemsNode.iterator(); i.hasNext();) {
				JsonNode itemNode = i.next();
				
				
				ConItemStatus conItemStatus = null;
				if (itemNode.get("conItemStatus") != null) {
					JsonNode iNode = itemNode.get("conItemStatus");
					if (iNode.get("id") != null)
					{
						Integer id = iNode.get("id").asInt();
						conItemStatus = new ConItemStatus(id);
					}
				}
				
//				Contract conItemcontract = null;
//				if (node.get("contract") != null) {
//					JsonNode iNode = node.get("contract");
//					Integer id = iNode.get("id").asInt();
//					contract = new Contract(id);
//				}
				
				Inventory conIteminventory = null;
				if (itemNode.get("inventory") != null) {
					JsonNode iNode = itemNode.get("inventory");
					if (iNode.get("id") != null)
					{
						Integer cId = null;
						if (iNode.get("invMainCategory") != null) {
							JsonNode invMain = iNode.get("invMainCategory");
							cId = invMain.get("id").asInt();
						}
						Integer id = iNode.get("id").asInt();
						conIteminventory = new Inventory(id, new InvMainCategory(cId));
					}
				}
				
				PriceList conItempriceList = null;
				if (itemNode.get("priceList") != null) {
					JsonNode iNode = itemNode.get("priceList");
					if (iNode.get("id") != null)
					{
						Integer id = iNode.get("id").asInt();
						conItempriceList = new PriceList(id);
					}
				}
				
				
				Unit conItemunit = null;
				if (itemNode.get("unit") != null) {
					JsonNode iNode = itemNode.get("unit");
					if (iNode.get("name") != null)
					{
						String name = iNode.get("name").asText();
						conItemunit = new Unit(name);
					}
				}
				
				String conItemserialNumber = "";
				if (itemNode.get("serialNumber") != null) {
					conItemserialNumber = itemNode.get("serialNumber").asText();
				}

				BigDecimal conItemprice = null;
				if (itemNode.get("price") != null) {
					conItemprice = new BigDecimal(itemNode.get("price").asDouble());
				}
				
				BigDecimal conItemquantity = null;
				if (itemNode.get("quantity") != null) {
					conItemquantity = new BigDecimal(itemNode.get("quantity").asDouble());
				}
				
				BigDecimal conItemcost = null;
				if (itemNode.get("cost") != null) {
					conItemcost = new BigDecimal(itemNode.get("cost").asDouble());
				}
				
				BigDecimal conItemdiscount = null;
				if (itemNode.get("discount") != null) {
					conItemdiscount = new BigDecimal(itemNode.get("discount").asDouble());
				}
				
				
				BigDecimal conItemsumm = null;
				if (itemNode.get("summ") != null) {
					conItemsumm = new BigDecimal(itemNode.get("summ").asDouble());
				}
				
				String glCode = null;
				if (itemNode.get("glCode") != null) {
					glCode = itemNode.get("glCode").asText();
				}
				
				
				Date writeoffDate = null;
				
				if (itemNode.get("writeoffDate") != null) {
					String ddStr = itemNode.get("writeoffDate").asText();
					try {
						if (ddStr.length() > 10) 
							writeoffDate = df1.parse(ddStr);
						else if (ddStr.length() >= 6) {
							writeoffDate = df2.parse(ddStr);
						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				
				ContractItem item = new ContractItem(conItemStatus, contract, conIteminventory, 
						conItempriceList, conItemunit, conItemserialNumber, conItemprice, conItemquantity,
						conItemcost, conItemdiscount, conItemsumm, writeoffDate, glCode);
						
				contractItems.add(item);
			}
			contract.setContractItems(contractItems);
		}
		
		// Contract Promos
		List<ContractPromos> contractPromos = new ArrayList<>();
		if (node.get("contractPromos") != null) {
			JsonNode itemsNode = node.get("contractPromos"), tempNode;
			for (Iterator<JsonNode> i = itemsNode.iterator(); i.hasNext();) {
				FinDoc si = new FinDoc();
				JsonNode itemNode = i.next();
				
				
//				Contract promosContract = null;			
//				if (node.get("contract") != null) {
//					JsonNode iNode = node.get("contract");
//					Integer id = iNode.get("id").asInt();
//					promosContract = new Contract(id);
//				}
				
				Promotion promosPromotion = null;
				if (node.get("promotion") != null) {
					JsonNode iNode = node.get("promotion");
					if (iNode.get("id") != null)
					{
						Integer id = iNode.get("id").asInt();
						promosPromotion = new Promotion(id);
					}
				}
				
				ContractPromos promos = new ContractPromos(contract, promosPromotion);
				contractPromos.add(promos);
			}
			contract.setContractPromos(contractPromos);
		}
		
		return contract;
	}

}
