package com.cordialsr.domain.deserializer;

import java.io.IOException;

import com.cordialsr.domain.Company;
import com.cordialsr.domain.StockIn;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class StockDeserializer extends JsonDeserializer<StockIn> {

	@Override
	public StockIn deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp), ptNode;
		ObjectMapper jsonObjectMapper = new ObjectMapper();

		Company company = null;
		if (node.get("company") != null) {
			JsonNode node2 = node.get("company");
			Integer id = node2.get("id").asInt();
			company = new Company(id);
		}
		
		StockIn invIn = new StockIn();
		
		return invIn;
	}

}
