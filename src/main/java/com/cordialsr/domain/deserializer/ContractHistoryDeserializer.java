package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractHistory;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ContractHistoryDeserializer  extends JsonDeserializer<ContractHistory> {

	@Override
	public ContractHistory deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp), ptNode;
		ObjectMapper jsonObjectMapper = new ObjectMapper();

		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
		
		Long ids = null;
		if (node.get("id") != null 
				&& !node.get("id").asText().equals("null")) {
			ids = node.get("id").asLong();			
		}
		
		Date updDate = null;
		if (node.get("updatedDate") != null) {
			String ddStr = node.get("updatedDate").asText();
			try {
				if (ddStr.length() > 10)
					updDate = df1.parse(ddStr);
				else
					updDate = df2.parse(ddStr);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		User updUser = null;
		if (node.get("updatedUser") != null) {
			JsonNode usNode = node.get("updatedUser");
			if (usNode.get("userid") != null) {
				Long getId = usNode.get("userid").asLong();
				updUser = new User(getId);
			}
		}
		
		String oldValue = null;
		if (node.get("oldValue") != null) {
			oldValue = node.get("oldValue").asText();			
		}
		
		String newValue = null;
		if (node.get("newValue") != null) {
			newValue = node.get("newValue").asText();			
		}
		
		String field = null;
		if (node.get("field") != null) {
			field = node.get("field").asText();			
		}
		
		String title = null;
		if (node.get("title") != null) {
			title = node.get("title").asText();			
		}
		
		Long holdId = null;
		if (node.get("oldId") != null 
				&& !node.get("oldId").asText().equals("null")) {
			holdId = node.get("oldId").asLong();			
		}
		
		Long newId = null;
		if (node.get("newId") != null 
				&& !node.get("newId").asText().equals("null")) {
			newId = node.get("newId").asLong();			
		}
		
		ContractHistory conHis = new ContractHistory(ids, null, field, title, updDate, updUser, oldValue, newValue, holdId, newId);
		
		if (node.get("contract") != null) {
			JsonNode itemsNode = node.get("contract");
			
			Long conId = null;
			if (itemsNode.get("id") != null) {
				conId = itemsNode.get("id").asLong();
			}
			
			Contract contract = new Contract(conId);
			conHis.setContract(contract);
		}
		
		return conHis;
	}
	
}
