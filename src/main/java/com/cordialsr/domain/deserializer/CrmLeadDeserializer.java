package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.cordialsr.domain.Address;
import com.cordialsr.domain.AddressType;
import com.cordialsr.domain.Branch;
import com.cordialsr.domain.City;
import com.cordialsr.domain.CityArea;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Country;
import com.cordialsr.domain.CrmLead;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.Oblast;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.PhoneNumber;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class CrmLeadDeserializer extends JsonDeserializer<CrmLead> {

	@Override
	public CrmLead deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp);
		
		
		Long id = null;
		if (node.get("id") != null) {
			id = node.get("id").asLong();
		}
		
		Employee dealer = null;
		if (node.get("dealer") != null) {
			JsonNode node2 = node.get("dealer");
			if (node2.get("id") != null) {
				Long parId = (long) node2.get("id").asInt();
				dealer = new Employee(parId);
			}
		}
		
		
		String firstname = null;
		if (node.get("firstname") != null) {
			JsonNode node2 = node.get("firstname");
			if (node2 != null) {
				String str = node2.asText();
				firstname = new String(str);
			}
		}
		
		String lastname = null;
		if (node.get("lastname") != null) {
			JsonNode node2 = node.get("lastname");
			if (node2 != null) {
				String str = node2.asText();
				lastname = new String(str);
			}
		}
		
		String middlename = null;
		if (node.get("middlename") != null) {
			JsonNode node2 = node.get("middlename");
			if (node2 != null) {
				String str = node2.asText();
				middlename = new String(str);
			}
		}
		
		String email = null;
		if (node.get("email") != null) {
			JsonNode node2 = node.get("email");
			if (node2 != null) {
				String str = node2.asText();
				email = new String(str);
			}
		}
		
		String info = null;
		if (node.get("info") != null) {
			JsonNode node2 = node.get("info");
			if (node2 != null) {
				String str = node2.asText();
				info = new String(str);
			}
		}

		String source = null;
		if (node.get("source") != null) {
			JsonNode node2 = node.get("source");
			if (node2 != null) {
				String str = node2.asText();
				source = new String(str);
			}
		}	
		
		Party party = null;
		if (node.get("party") != null) {
			JsonNode usNode = node.get("party");
			if (usNode.get("id") != null) {
				Long getId = usNode.get("id").asLong();
				party = new Party(getId);
			}
		}
		
		Company company = null;
		if (node.get("company") != null) {
			JsonNode usNode = node.get("company");
			if (usNode.get("id") != null) {
				Integer getId = usNode.get("id").asInt();
				company = new Company(getId);
			}
		}
		
		Branch branch = null;
		if (node.get("branch") != null) {
			JsonNode usNode = node.get("branch");
			if (usNode.get("id") != null) {
				Integer getId = usNode.get("id").asInt();
				branch = new Branch(getId);
			}
		}
		
		PhoneNumber ph2 = null;
		if (node.get("phone") != null) {
			JsonNode usNode = node.get("phone");
			if (usNode.get("id") != null) {
				Long getId = usNode.get("id").asLong();
				ph2.setId(getId);
			}
			System.out.println(usNode);
			if (usNode.get("ismain") != null) {
				Boolean getIsmain = usNode.get("ismain").asBoolean();
				System.out.println(getIsmain);
				ph2.setIsmain(getIsmain);
			}

			if (usNode.get("ismobile") != null) {
				Boolean getIsMobile = usNode.get("ismobile").asBoolean();
				ph2.setIsmobile(getIsMobile);
			}
			if (usNode.get("info") != null) {
				String getInfo = usNode.get("info").asText();
				ph2.setInfo(getInfo);
			}
			if (usNode.get("phNumber") != null) {
				String getPhNumber = usNode.get("phNumber").asText();
				ph2.setPhNumber(getPhNumber);
			}
			
		}
		CrmLead lead = new CrmLead(id, dealer, null, firstname, lastname, middlename,
				email, info, source, null, null, null, null, party, company, branch, ph2);
		
		
		CrmLead adviser = null;
		if (node.get("adviser") != null) {
			JsonNode InnerNode = node.get("adviser");
			
			Long adviserId = null;
			if (InnerNode.get("id") != null) {
				adviserId = InnerNode.get("id").asLong();
			}
			
			Employee adviserDealer = null;
			if (InnerNode.get("dealer") != null) {
				JsonNode node2 = InnerNode.get("dealer");
				if (node2.get("id") != null) {
					Long parId = (long) node2.get("id").asInt();
					adviserDealer = new Employee(parId);
				}
			}
			
			CrmLead adviserAdviser = null;
			if (InnerNode.get("adviser") != null) {
				JsonNode node2 = InnerNode.get("adviser");
				if (node2.get("id") != null) {
					Long leadId = (long) node2.get("id").asInt();
					adviserAdviser = new CrmLead(leadId);
				}
			}
			
			
			String adviserFirstname = null;
			if (InnerNode.get("firstname") != null) {
				JsonNode node2 = InnerNode.get("firstname");
				if (node2 != null) {
					String str = node2.asText();
					adviserFirstname = new String(str);
				}
			}
			
			String adviserLastname = null;
			if (InnerNode.get("lastname") != null) {
				JsonNode node2 = InnerNode.get("lastname");
				if (node2 != null) {
					String str = node2.asText();
					adviserLastname = new String(str);
				}
			}
			
			String adviserMiddlename = null;
			if (InnerNode.get("middlename") != null) {
				JsonNode node2 = InnerNode.get("middlename");
				if (node2 != null) {
					String str = node2.asText();
					adviserMiddlename = new String(str);
				}
			}
			
			String adviserEmail = null;
			if (InnerNode.get("email") != null) {
				JsonNode node2 = InnerNode.get("email");
				if (node2 != null) {
					String str = node2.asText();
					adviserEmail = new String(str);
				}
			}
			
			String adviserInfo = null;
			if (InnerNode.get("info") != null) {
				JsonNode node2 = InnerNode.get("info");
				if (node2 != null) {
					String str = node2.asText();
					adviserInfo = new String(str);
				}
			}

			String adviserSource = null;
			if (InnerNode.get("source") != null) {
				JsonNode node2 = InnerNode.get("source");
				if (node2 != null) {
					String str = node2.asText();
					adviserSource = new String(str);
				}
			}	
			
			Party adviserParty = null;
			if (InnerNode.get("party") != null) {
				JsonNode usNode = InnerNode.get("party");
				if (usNode.get("id") != null) {
					Long getId = usNode.get("id").asLong();
					adviserParty = new Party(getId);
				}
			}
			
			Company adviserCompany = null;
			if (InnerNode.get("company") != null) {
				JsonNode usNode = InnerNode.get("company");
				if (usNode.get("id") != null) {
					Integer getId = usNode.get("id").asInt();
					adviserCompany = new Company(getId);
				}
			}
			
			Branch adviserBranch = null;
			if (InnerNode.get("branch") != null) {
				JsonNode usNode = InnerNode.get("branch");
				if (usNode.get("id") != null) {
					Integer getId = usNode.get("id").asInt();
					adviserBranch = new Branch(getId);
				}
			}
			
			PhoneNumber ph = null;
			if (InnerNode.get("phone") != null) {
				JsonNode usNode = InnerNode.get("phone");
				if (usNode.get("id") != null) {
					Long getId = usNode.get("id").asLong();
					ph = new PhoneNumber(getId);
				}
			}
			
			 adviser = new CrmLead(adviserId, adviserDealer, adviserAdviser, adviserFirstname, 
					adviserLastname, adviserMiddlename, adviserEmail, adviserInfo, adviserSource, 
					null, null, null, null, adviserParty, adviserCompany, adviserBranch, ph);
		}
		lead.setAdviser(adviser);
		
		Address addr = new Address();
		
		if (node.get("address") != null) {
			JsonNode itemsNode = node.get("address");
			
			Long addrId = null;
			if (itemsNode.get("id") != null) {
				addrId = itemsNode.get("id").asLong();			
			}
			
			AddressType addressType = null;
			if (itemsNode.get("addressType") != null) {
				JsonNode usNode = itemsNode.get("addressType");
				if (usNode.get("id") != null) {
					Integer getId = usNode.get("id").asInt();
					addressType = new AddressType(getId);
				}
			}
			
			City city = null;
			if (itemsNode.get("city") != null) {
				JsonNode usNode = itemsNode.get("city");
				if (usNode.get("id") != null) {
					Integer getId = usNode.get("id").asInt();
					city = new City(getId);
				}
			}
			
			CityArea cityArea = null;
			if (itemsNode.get("cityArea") != null) {
				JsonNode usNode = itemsNode.get("cityArea");
				if (usNode.get("id") != null) {
					Integer getId = usNode.get("id").asInt();
					cityArea = new CityArea(getId);
				}
			}
			
			Country country = null;
			if (itemsNode.get("country") != null) {
				JsonNode usNode = itemsNode.get("country");
				if (usNode.get("id") != null) {
					Integer getId = usNode.get("id").asInt();
					country = new Country(getId);
				}
			}
			
			Oblast oblast = null;
			if (itemsNode.get("oblast") != null) {
				JsonNode usNode = itemsNode.get("oblast");
				if (usNode.get("id") != null) {
					Integer getId = usNode.get("id").asInt();
					oblast = new Oblast(getId);
				}
			}
			
			Party addrParty = null;
			if (itemsNode.get("party") != null) {
				JsonNode usNode = itemsNode.get("party");
				if (usNode.get("id") != null) {
					Long getId = usNode.get("id").asLong();
					addrParty = new Party(getId);
				}
			}
			
			CrmLead addrLead = null;
			if (itemsNode.get("lead") != null) {
				JsonNode usNode = itemsNode.get("lead");
				if (usNode.get("id") != null) {
					Long getId = usNode.get("id").asLong();
					addrLead = new CrmLead(getId);
				}
			}
			
			String district = null;
			if (itemsNode.get("district") != null) {
				JsonNode node2 = itemsNode.get("district");
				if (node2 != null) {
					String str = node2.asText();
					district = new String(str);
				}
			}
			
			String street = null;
			if (itemsNode.get("street") != null) {
				JsonNode node2 = itemsNode.get("street");
				if (node2 != null) {
					String str = node2.asText();
					street = new String(str);
				}
			}

			String houseNumber = null;
			if (itemsNode.get("houseNumber") != null) {
				JsonNode node2 = itemsNode.get("houseNumber");
				if (node2 != null) {
					String str = node2.asText();
					houseNumber = new String(str);
				}
			}
			
			String flatNumber = null;
			if (itemsNode.get("flatNumber") != null) {
				JsonNode node2 = itemsNode.get("flatNumber");
				if (node2 != null) {
					String str = node2.asText();
					flatNumber = new String(str);
				}
			}
			
			String zipCode = null;
			if (itemsNode.get("zipCode") != null) {
				JsonNode node2 = itemsNode.get("zipCode");
				if (node2 != null) {
					String str = node2.asText();
					zipCode = new String(str);
				}
			}
			
			String addrInfo = null;
			if (itemsNode.get("info") != null) {
				JsonNode node2 = itemsNode.get("info");
				if (node2 != null) {
					String str = node2.asText();
					info = new String(str);
				}
			}
			
			addr = new Address(addrId, addressType, city, cityArea, country, oblast, addrParty,
					 district, street, houseNumber, flatNumber, zipCode, 
					 addrInfo , addrLead);
		}
		lead.setAddress(addr);
	
			List<PhoneNumber> phoneNumbers = new ArrayList<>();

			if (node.get("phoneNumbers") != null) {
				JsonNode itemsNode = node.get("phoneNumbers");
				
				for (Iterator<JsonNode> i = itemsNode.iterator(); i.hasNext();) {
					JsonNode itemNode = i.next();
					
					Long ids = null;
					if (itemNode.get("id") != null 
							&& !itemNode.get("id").asText().equals("null")) {
						ids = (long)itemNode.get("id").asInt();			
					}
					
					
					Party partys = null;
					if (itemNode.get("party") != null) {
						JsonNode usNode = itemNode.get("party");
						if (usNode.get("id") != null) {
							Long getId = usNode.get("id").asLong();
							partys = new Party(getId);
						}
					}
					
					CrmLead leads = null;
					if (itemNode.get("lead") != null) {
						JsonNode usNode = itemNode.get("lead");
						if (usNode.get("id") != null) {
							Long getId = (long)usNode.get("id").asInt();
							leads = new CrmLead(getId);
						}
					}
					
					String phNumbers = null;
					if (itemNode.get("phNumber") != null) {
						JsonNode node2 = itemNode.get("phNumber");
						if (node2 != null) {
							String str = node2.asText();
							phNumbers = new String(str);
						}
					}
					
					Boolean ismobiles = null;
					if (itemNode.get("ismobile") != null) {
						JsonNode node2 = itemNode.get("ismobile");
						if (node2 != null) {
							Boolean str = node2.asBoolean();
							ismobiles = new Boolean(str);
						}
					}
					
					Boolean ismains = null;
					if (itemNode.get("ismain") != null) {
						JsonNode node2 = itemNode.get("ismain");
						if (node2 != null) {
							Boolean str = node2.asBoolean();
							ismains = new Boolean(str);
						}
					}
					
					String phoneInfo = null;
					if (itemNode.get("info") != null) {
						JsonNode node2 = itemNode.get("info");
						if (node2 != null) {
							String str = node2.asText();
							phoneInfo = new String(str);
						}
					}
					
					PhoneNumber ph = new PhoneNumber(ids, partys, phNumbers, ismobiles, ismains, leads, phoneInfo);
					phoneNumbers.add(ph);
				}
				lead.setPhoneNumbers(phoneNumbers);
			}
					
			
		return lead;	
	}
}
