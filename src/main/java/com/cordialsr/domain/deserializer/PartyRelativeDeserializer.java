package com.cordialsr.domain.deserializer;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.cordialsr.domain.Party;
import com.cordialsr.domain.PartyRelatives;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

public class PartyRelativeDeserializer extends JsonDeserializer<PartyRelatives>{

	@Override
	public PartyRelatives deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {

		ObjectCodec oc = jp.getCodec();
		JsonNode node = oc.readTree(jp), ptNode;
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
				PartyRelatives relatives = null;
		
				Long relativeId = null;
				if (node.get("id") != null) {
					relativeId = node.get("id").asLong();
				}

				Party relativeParty = null;
				if (node.get("party") != null) {
					ptNode = node.get("party");
					if (ptNode.get("id") != null) {
						Long id = ptNode.get("id").asLong();
						relativeParty = new Party(id);
					}
				}

				String relativeLevel = "";
				if (node.get("level") != null) {
					relativeLevel = (node.get("level").asText());
				}

				String relativeFirstname = "";
				if (node.get("firstname") != null) {
				relativeFirstname = (node.get("firstname").asText());
				}
				
				String relativeLastname = "";
				if (node.get("lastname") != null) {
					relativeLastname = (node.get("lastname").asText());
				}
				
				
				
				String phone = "";
				if (node.get("phone") != null) {
					phone = node.get("phone").asText();
				}
				
				String jobPlace = "";
				if (node.get("jobPlace") != null) {
					jobPlace = (node.get("jobPlace").asText());
				}
				
				String position = "";
				if (node.get("position") != null) {
					position = (node.get("position").asText());
				}
				
				Date birthDate = null;
				if (node.get("birthDate") != null) {
					String date = node.get("birthDate").asText();
					try {
						if (date.length() > 10) {
							birthDate = df1.parse(date);
						} else if (date.length() > 7) {
							birthDate = df2.parse(date);
						}
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				
				PartyRelatives s = new PartyRelatives(relativeId, relativeParty, relativeLevel, relativeFirstname, relativeLastname, phone, jobPlace, position, birthDate);
			
		
		return s;
	}

}
