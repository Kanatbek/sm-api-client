package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "contract_payment_schedule")
public class ContractPaymentSchedule implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Contract contract;
	private String refkey;
	private Integer paymentOrder;
	private Boolean isFirstpayment;
	private BigDecimal summ;
	private BigDecimal paid;
	private Date paymentDate;

	public ContractPaymentSchedule() {
	}

	public ContractPaymentSchedule(Long id, Contract contract, String refkey, Integer order, Boolean isFirstpayment, BigDecimal summ,
			BigDecimal paid, Date paymentDate) {
		this.id = id;
		this.contract = contract;
		this.refkey = refkey;
		this.paymentOrder = order;
		this.isFirstpayment = isFirstpayment;
		this.summ = summ;
		this.paid = paid;
		this.paymentDate = paymentDate;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contract_id")
	public Contract getContract() {
		return this.contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	@Column(name = "refkey", length = 20)
	public String getRefkey() {
		return this.refkey;
	}

	public void setRefkey(String refkey) {
		this.refkey = refkey;
	}

	@Column(name = "payment_order")
	public Integer getPaymentOrder() {
		return this.paymentOrder;
	}

	public void setPaymentOrder(Integer order) {
		this.paymentOrder = order;
	}

	@Column(name = "is_firstpayment")
	public Boolean getIsFirstpayment() {
		return this.isFirstpayment;
	}

	public void setIsFirstpayment(Boolean isFirstpayment) {
		this.isFirstpayment = isFirstpayment;
	}

	@Column(name = "summ", precision = 21)
	public BigDecimal getSumm() {
		return this.summ;
	}

	public void setSumm(BigDecimal summ) {
		this.summ = summ;
	}

	@Column(name = "paid", precision = 21)
	public BigDecimal getPaid() {
		return this.paid;
	}

	public void setPaid(BigDecimal paid) {
		this.paid = paid;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "payment_date", nullable = false, length = 10)
	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contract == null || contract.getId() == null) ? 0 : contract.getId().hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((isFirstpayment == null) ? 0 : isFirstpayment.hashCode());
		result = prime * result + ((paid == null) ? 0 : paid.hashCode());
		result = prime * result + ((paymentDate == null) ? 0 : paymentDate.hashCode());
		result = prime * result + ((paymentOrder == null) ? 0 : paymentOrder.hashCode());
		result = prime * result + ((refkey == null) ? 0 : refkey.hashCode());
		result = prime * result + ((summ == null) ? 0 : summ.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContractPaymentSchedule other = (ContractPaymentSchedule) obj;
		if (contract == null) {
			if (other.contract != null)
				return false;
		} else if (!contract.getId().equals(other.contract.getId()))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isFirstpayment == null) {
			if (other.isFirstpayment != null)
				return false;
		} else if (!isFirstpayment.equals(other.isFirstpayment))
			return false;
		if (paid == null) {
			if (other.paid != null)
				return false;
		} else if (!paid.equals(other.paid))
			return false;
		if (paymentDate == null) {
			if (other.paymentDate != null)
				return false;
		} else if (!paymentDate.equals(other.paymentDate))
			return false;
		if (paymentOrder == null) {
			if (other.paymentOrder != null)
				return false;
		} else if (!paymentOrder.equals(other.paymentOrder))
			return false;
		if (refkey == null) {
			if (other.refkey != null)
				return false;
		} else if (!refkey.equals(other.refkey))
			return false;
		if (summ == null) {
			if (other.summ != null)
				return false;
		} else if (!summ.equals(other.summ))
			return false;
		return true;
	}

	@Override
	public ContractPaymentSchedule clone() throws CloneNotSupportedException {
		return (ContractPaymentSchedule) super.clone();
	}
	
}
