package com.cordialsr.domain;
// Generated 23.05.2017 12:18:03 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.cordialsr.domain.deserializer.SmFilterChangeDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "sm_filter_change")
@JsonDeserialize(using = SmFilterChangeDeserializer.class)
public class SmFilterChange implements Cloneable {
	
	private Integer id;
	private Contract contract;
	private String contractNumber;
	private Integer f1Mt;
	private Date f1Prev;
	private Date f1Last;
	private Date f1Next;
	private Integer f2Mt;
	private Date f2Prev;
	private Date f2Last;
	private Date f2Next;
	private Integer f3Mt;
	private Date f3Prev;
	private Date f3Last;
	private Date f3Next;
	private Integer f4Mt;
	private Date f4Prev;
	private Date f4Last;
	private Date f4Next;
	private Integer f5Mt;
	private Date f5Prev;
	private Date f5Last;
	private Date f5Next;
	private Integer prMt;
	private Date prPrev;
	private Date prLast;
	private Date prNext;
	private Boolean enabled;
	private Calendar cpuDate;
	
	@Transient
	private String exploiterFio;
	@Transient
	private String caremanFio;
	@Transient
	private String serialNumber;
	@Transient
	private String address;
	@Transient
	private String phone;
	@Transient
	private Date sdate;
	@Transient
	private Long customerId;
	@Transient 
	private Long contractId;
	@Transient
	private String customerIin;
	@Transient
	private Integer payment;
	@Transient
	private String currency;
	
	public SmFilterChange() {
	}

	public SmFilterChange(Integer id, Contract contract, String contractNumber, Integer f1Mt, Date f1Prev, Date f1Last, Date f1Next, Integer f2Mt,
		Date f2Prev, Date f2Last, Date f2Next, Integer f3Mt, Date f3Prev, Date f3Last, Date f3Next, Integer f4Mt,
		Date f4Prev, Date f4Last, Date f4Next, Integer f5Mt, Date f5Prev, Date f5Last, Date f5Next, Integer prMt,
		Date prPrev, Date prLast, Date prNext, Boolean enabled) {
	this.id = id;
	this.contract = contract;
	this.contractNumber = contractNumber;
	this.f1Mt = f1Mt;
	this.f1Prev = f1Prev;
	this.f1Last = f1Last;
	this.f1Next = f1Next;
	this.f2Mt = f2Mt;
	this.f2Prev = f2Prev;
	this.f2Last = f2Last;
	this.f2Next = f2Next;
	this.f3Mt = f3Mt;
	this.f3Prev = f3Prev;
	this.f3Last = f3Last;
	this.f3Next = f3Next;
	this.f4Mt = f4Mt;
	this.f4Prev = f4Prev;
	this.f4Last = f4Last;
	this.f4Next = f4Next;
	this.f5Mt = f5Mt;
	this.f5Prev = f5Prev;
	this.f5Last = f5Last;
	this.f5Next = f5Next;
	this.prMt = prMt;
	this.prPrev = prPrev;
	this.prLast = prLast;
	this.prNext = prNext;
	this.enabled = enabled;
}


	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CONTRACT", nullable = false)
	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}
	
	
	@Column(name = "contract_number", length = 20)
	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	@Column(name = "F1_MT")
	public Integer getF1Mt() {
		return this.f1Mt;
	}

	public void setF1Mt(Integer f1Mt) {
		this.f1Mt = f1Mt;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "F1_LAST", length = 10)
	public Date getF1Last() {
		return this.f1Last;
	}

	public void setF1Last(Date f1Last) {
		this.f1Last = f1Last;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "F1_NEXT", length = 10)
	public Date getF1Next() {
		return this.f1Next;
	}

	public void setF1Next(Date f1Next) {
		this.f1Next = f1Next;
	}

	@Column(name = "F2_MT")
	public Integer getF2Mt() {
		return this.f2Mt;
	}

	public void setF2Mt(Integer f2Mt) {
		this.f2Mt = f2Mt;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "F2_LAST", length = 10)
	public Date getF2Last() {
		return this.f2Last;
	}

	public void setF2Last(Date f2Last) {
		this.f2Last = f2Last;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "F2_NEXT", length = 10)
	public Date getF2Next() {
		return this.f2Next;
	}

	public void setF2Next(Date f2Next) {
		this.f2Next = f2Next;
	}

	@Column(name = "F3_MT")
	public Integer getF3Mt() {
		return this.f3Mt;
	}

	public void setF3Mt(Integer f3Mt) {
		this.f3Mt = f3Mt;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "F3_LAST", length = 10)
	public Date getF3Last() {
		return this.f3Last;
	}

	public void setF3Last(Date f3Last) {
		this.f3Last = f3Last;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "F3_NEXT", length = 10)
	public Date getF3Next() {
		return this.f3Next;
	}

	public void setF3Next(Date f3Next) {
		this.f3Next = f3Next;
	}

	@Column(name = "F4_MT")
	public Integer getF4Mt() {
		return this.f4Mt;
	}

	public void setF4Mt(Integer f4Mt) {
		this.f4Mt = f4Mt;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "F4_LAST", length = 10)
	public Date getF4Last() {
		return this.f4Last;
	}

	public void setF4Last(Date f4Last) {
		this.f4Last = f4Last;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "F4_NEXT", length = 10)
	public Date getF4Next() {
		return this.f4Next;
	}

	public void setF4Next(Date f4Next) {
		this.f4Next = f4Next;
	}

	@Column(name = "F5_MT")
	public Integer getF5Mt() {
		return this.f5Mt;
	}

	public void setF5Mt(Integer f5Mt) {
		this.f5Mt = f5Mt;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "F5_LAST", length = 10)
	public Date getF5Last() {
		return this.f5Last;
	}

	public void setF5Last(Date f5Last) {
		this.f5Last = f5Last;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "F5_NEXT", length = 10)
	public Date getF5Next() {
		return this.f5Next;
	}

	public void setF5Next(Date f5Next) {
		this.f5Next = f5Next;
	}

	@Column(name = "enabled")
	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	// ***************************
	
	@Temporal(TemporalType.DATE)
	@Column(name = "F1_PREV", length = 10)
	public Date getF1Prev() {
		return f1Prev;
	}

	public void setF1Prev(Date f1Prev) {
		this.f1Prev = f1Prev;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "F2_PREV", length = 10)
	public Date getF2Prev() {
		return f2Prev;
	}

	public void setF2Prev(Date f2Prev) {
		this.f2Prev = f2Prev;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "F3_PREV", length = 10)
	public Date getF3Prev() {
		return f3Prev;
	}

	public void setF3Prev(Date f3Prev) {
		this.f3Prev = f3Prev;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "F4_PREV", length = 10)
	public Date getF4Prev() {
		return f4Prev;
	}

	public void setF4Prev(Date f4Prev) {
		this.f4Prev = f4Prev;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "F5_PREV", length = 10)
	public Date getF5Prev() {
		return f5Prev;
	}
	
	public void setF5Prev(Date f5Prev) {
		this.f5Prev = f5Prev;
	}

	// *****************************************************
	
	@Column(name = "PR_MT", length = 10)
	public Integer getPrMt() {
		return prMt;
	}

	public void setPrMt(Integer prMt) {
		this.prMt = prMt;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "PR_PREV", length = 10)
	public Date getPrPrev() {
		return prPrev;
	}

	public void setPrPrev(Date prPrev) {
		this.prPrev = prPrev;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "PR_LAST", length = 10)
	public Date getPrLast() {
		return prLast;
	}

	public void setPrLast(Date prLast) {
		this.prLast = prLast;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "PR_NEXT", length = 10)
	public Date getPrNext() {
		return prNext;
	}

	public void setPrNext(Date prNext) {
		this.prNext = prNext;
	}

	@Override
	public SmFilterChange clone() throws CloneNotSupportedException {
		return (SmFilterChange) super.clone();
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cpu_date")
	public Calendar getCpuDate() {
		return cpuDate;
	}

	public void setCpuDate(Calendar cpuDate) {
		this.cpuDate = cpuDate;
	}

	@Transient
	public String getExploiterFio() {
		return exploiterFio;
	}

	@Transient
	public void setExploiterFio(String exploiterFio) {
		this.exploiterFio = exploiterFio;
	}

	@Transient
	public String getCaremanFio() {
		return caremanFio;
	}

	@Transient
	public void setCaremanFio(String caremanFio) {
		this.caremanFio = caremanFio;
	}

	@Transient
	public String getSerialNumber() {
		return serialNumber;
	}

	@Transient
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	@Transient
	public String getAddress() {
		return address;
	}

	@Transient
	public void setAddress(String address) {
		this.address = address;
	}

	@Transient
	public String getPhone() {
		return phone;
	}

	@Transient
	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Transient
	public Date getSdate() {
		return sdate;
	}

	@Transient
	public void setSdate(Date sdate) {
		this.sdate = sdate;
	}
	
	@Transient
	public Long getCustomerId() {
		return customerId;
	}
	
	@Transient
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	
	@Transient
	public Long getContractId() {
		return contractId;
	}
	
	@Transient
	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}
	
	@Transient
	public String getCustomerIin() {
		return customerIin;
	}
	
	@Transient
	public void setCustomerIin(String customerIin) {
		this.customerIin = customerIin;
	}
	
	@Transient
	public Integer getPayment() {
		return payment;
	}
	
	@Transient
	public void setPayment(Integer payment) {
		this.payment = payment;
	}
	
	@Transient
	public String getCurrency() {
		return currency;
	}
	
	@Transient
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	
	
}
