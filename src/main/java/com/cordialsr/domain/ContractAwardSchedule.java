package com.cordialsr.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "contract_award_schedule")
public class ContractAwardSchedule implements Cloneable {
	
	private Long id;
	private Contract contract;
	private AwardType awardType;
	private Position position;
	private Party party;
	private Date dateSchedule;
	private Date dateAccrued;
	private BigDecimal summ;
	private Currency currency;
	private BigDecimal accrued;
	private String refkey;
	private Boolean closed;
	private Boolean revertOnCancel;
	private Integer porder;
	private BigDecimal revertSumm;
	private BigDecimal deduction;
	
	public ContractAwardSchedule() {

	}
	
	public ContractAwardSchedule(Long id) {
		this.id = id;
	}

	public ContractAwardSchedule(Long id, Contract contract, AwardType awardType, Position position, Party party, Date dateSchedule,
			Date dateAccrued, BigDecimal summ, Currency currency, BigDecimal accrued, String refkey, Boolean closed, Boolean revertOnCancel, Integer porder, 
			BigDecimal revertSumm, BigDecimal deduction) {
		this.id = id;
		this.contract = contract;
		this.awardType = awardType;
		this.position = position;
		this.party = party;
		this.dateSchedule = dateSchedule;
		this.dateAccrued = dateAccrued;
		this.summ = summ;
		this.currency = currency;
		this.accrued = accrued;
		this.refkey = refkey;
		this.closed = closed;
		this.revertOnCancel = revertOnCancel;
		this.porder = porder;
		this.revertSumm = revertSumm;
		this.deduction = deduction;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contract_id", nullable = false)
	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "award_type", nullable = false)
	public AwardType getAwardType() {
		return this.awardType;
	}

	public void setAwardType(AwardType awardType) {
		this.awardType = awardType;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "position")
	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party")
	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "date_schedule", nullable = false, length = 10)
	public Date getDateSchedule() {
		return dateSchedule;
	}

	public void setDateSchedule(Date dateSchedule) {
		this.dateSchedule = dateSchedule;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "date_accrued", nullable = false, length = 10)
	public Date getDateAccrued() {
		return dateAccrued;
	}

	public void setDateAccrued(Date dateAccrued) {
		this.dateAccrued = dateAccrued;
	}

	@Column(name = "summ", precision = 16, scale = 4)
	public BigDecimal getSumm() {
		return summ;
	}

	public void setSumm(BigDecimal summ) {
		this.summ = summ;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currency", nullable = false)
	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	@Column(name = "accrued", precision = 16, scale = 4)
	public BigDecimal getAccrued() {
		return accrued;
	}

	public void setAccrued(BigDecimal accrued) {
		this.accrued = accrued;
	}

	@Column(name = "refkey", length = 20)
	public String getRefkey() {
		return refkey;
	}

	public void setRefkey(String refkey) {
		this.refkey = refkey;
	}

	@Column(name = "closed")
	public Boolean getClosed() {
		return closed;
	}

	public void setClosed(Boolean closed) {
		this.closed = closed;
	}
	
	@Column(name = "revert_on_cancel")
	public Boolean getRevertOnCancel() {
		return revertOnCancel;
	}

	public void setRevertOnCancel(Boolean revertOnCancel) {
		this.revertOnCancel = revertOnCancel;
	}

	@Column(name = "porder")
	public Integer getPorder() {
		return porder;
	}

	public void setPorder(Integer porder) {
		this.porder = porder;
	}

	@Column(name = "revert_summ", precision = 16, scale = 4)
	public BigDecimal getRevertSumm() {
		return revertSumm;
	}

	public void setRevertSumm(BigDecimal revertSumm) {
		this.revertSumm = revertSumm;
	}

	@Column(name = "deduction", precision = 16, scale = 4)
	public BigDecimal getDeduction() {
		return deduction;
	}

	public void setDeduction(BigDecimal deduction) {
		this.deduction = deduction;
	}
	
	
	// --------------------------------------------------------------------------------------
	
	
	@Override
	public ContractAwardSchedule clone() throws CloneNotSupportedException {
		return (ContractAwardSchedule) super.clone();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accrued == null) ? 0 : accrued.hashCode());
		result = prime * result + ((awardType == null || awardType.getName() == null) ? 0 : awardType.getName().hashCode());
		result = prime * result + ((closed == null) ? 0 : closed.hashCode());
		result = prime * result + ((contract == null || contract.getId() == null) ? 0 : contract.getId().hashCode());
		result = prime * result + ((currency == null || currency.getCurrency() == null) ? 0 : currency.getCurrency().hashCode());
		result = prime * result + ((dateAccrued == null) ? 0 : dateAccrued.hashCode());
		result = prime * result + ((dateSchedule == null) ? 0 : dateSchedule.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((party == null || party.getId() == null) ? 0 : party.getId().hashCode());
		result = prime * result + ((position == null || position.getId() == null) ? 0 : position.getId().hashCode());
		result = prime * result + ((refkey == null) ? 0 : refkey.hashCode());
		result = prime * result + ((revertOnCancel == null) ? 0 : revertOnCancel.hashCode());
		result = prime * result + ((summ == null) ? 0 : summ.hashCode());
		result = prime * result + ((revertSumm == null) ? 0 : revertSumm.hashCode());
		result = prime * result + ((deduction == null) ? 0 : deduction.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContractAwardSchedule other = (ContractAwardSchedule) obj;
		if (accrued == null) {
			if (other.accrued != null)
				return false;
		} else if (!accrued.equals(other.accrued))
			return false;
		if (awardType == null) {
			if (other.awardType != null)
				return false;
		} else if (!awardType.getName().equals(other.awardType.getName()))
			return false;
		if (closed == null) {
			if (other.closed != null)
				return false;
		} else if (!closed.equals(other.closed))
			return false;
		if (contract == null) {
			if (other.contract != null)
				return false;
		} else if (!contract.getId().equals(other.contract.getId()))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.getCurrency().equals(other.currency.getCurrency()))
			return false;
		if (dateAccrued == null) {
			if (other.dateAccrued != null)
				return false;
		} else if (!dateAccrued.equals(other.dateAccrued))
			return false;
		if (dateSchedule == null) {
			if (other.dateSchedule != null)
				return false;
		} else if (!dateSchedule.equals(other.dateSchedule))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (party == null) {
			if (other.party != null)
				return false;
		} else if (!party.getId().equals(other.party.getId()))
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.getId().equals(other.position.getId()))
			return false;
		if (refkey == null) {
			if (other.refkey != null)
				return false;
		} else if (!refkey.equals(other.refkey))
			return false;
		if (revertOnCancel == null) {
			if (other.revertOnCancel != null)
				return false;
		} else if (!revertOnCancel.equals(other.revertOnCancel))
			return false;
		if (summ == null) {
			if (other.summ != null)
				return false;
		} else if (!summ.equals(other.summ))
			return false;
		if (revertSumm == null) {
			if (other.revertSumm != null)
				return false;
		} else if (!revertSumm.equals(other.revertSumm))
			return false;
		if (deduction == null) {
			if (other.deduction != null)
				return false;
		} else if (!deduction.equals(other.deduction))
			return false;
		return true;
	}

	
}
