package com.cordialsr.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "inv_item_sn", 
	   uniqueConstraints = @UniqueConstraint(columnNames = {"invoice_item", "serial_number"}))
public class InvItemSn implements Serializable, Cloneable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private InvoiceItem invoiceItem;
	private String serialNumber;
	
	public InvItemSn() {
		
	}
	
	public InvItemSn(String sn) {
		this.serialNumber = sn;
	}
	
	public InvItemSn(InvoiceItem invoiceItem, String serialNumber) {
		this.invoiceItem = invoiceItem;
		this.serialNumber = serialNumber;
	}
	
	public InvItemSn(Long id, InvoiceItem invoiceItem, String serialNumber) {
		this.id = id;
		this.invoiceItem = invoiceItem;
		this.serialNumber = serialNumber;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invoice_item", nullable = false)
	public InvoiceItem getInvoiceItem() {
		return invoiceItem;
	}
	public void setInvoiceItem(InvoiceItem invoiceItem) {
		this.invoiceItem = invoiceItem;
	}
	
	@Column(name = "serial_number", nullable = false, length = 45)
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	@Override
	public InvItemSn clone() throws CloneNotSupportedException {
		return (InvItemSn) super.clone();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((invoiceItem == null || invoiceItem.getId() == null) ? 0 : invoiceItem.getId().hashCode());
		result = prime * result + ((serialNumber == null) ? 0 : serialNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvItemSn other = (InvItemSn) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (invoiceItem == null) {
			if (other.invoiceItem != null)
				return false;
		} else if (invoiceItem.getId() != other.invoiceItem.getId())
			return false;
		if (serialNumber == null) {
			if (other.serialNumber != null)
				return false;
		} else if (!serialNumber.equals(other.serialNumber))
			return false;
		return true;
	}
	
}