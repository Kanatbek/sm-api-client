package com.cordialsr.domain;
// Generated 23.05.2017 12:18:03 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
 
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cordialsr.domain.deserializer.SmContractDeserializer;
import com.cordialsr.domain.reducer.UserReducer;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@JsonDeserialize(using = SmContractDeserializer.class)
@Table(name = "sm_contract")
public class SmContract implements Cloneable, Serializable {
	
	private static final long serialVersionUID = 1L;
		
		private Integer id;
		private Date cdate;
		private String contractNumber;
		private SmBranch smBranch;
		private String customerFio;
		private String iinbin;
		
		private String coordinator;
		private String director;
		private String manager;
		private String dealer;
		private String demosec;
		private String fitter;
		private String careman;
		
		private String model;
		private String sn;
		private Boolean storno;
		private Date stornoDate;
		private String addrReg;
		private String addrFact;
		private String phone;
		private Boolean resigned;	
		private Date resignedDate;
		private Boolean isRent;
		private String info;
		private User updatedUser;
		private Date updatedDate;
		private Inventory inventory;
		
		private String customerId;
		private Boolean isYur;
		private String country;
		private String city;
		private Date birthday;
		private String passportNo;
		private Date passDateIssue;
		private String passIssuedBy;
		private Date passDateExpire;
		
		private BigDecimal price;
		private BigDecimal firstPayment;
		private BigDecimal firstMonthVznos;
		private BigDecimal vznos;
		private BigDecimal paidVznos;
		private BigDecimal lastVznos;
		private Integer monthTerm;
		private String currency;
		
		private String resignConSn;
		private String transfConSn;
		private Date transferDate;
		private Date reissueDate;
		
		private List<SmConSalesPs> salesPs;

	@Column(name = "IS_RENT")
	public Boolean getIsRent() {
		return isRent;
	}

	public void setIsRent(Boolean isRent) {
		this.isRent = isRent;
	}

	public SmContract() {
	}
	
	public SmContract(Integer id) {
		this.id = id;
	}


	public SmContract(SmBranch smBranch, String contractNumber) {
		this.smBranch = smBranch;
		this.contractNumber = contractNumber;
	}

	public SmContract(Integer id, SmBranch smBranch, Date cdate, String contractNumber, String customer, String iinbin,
			String dealer, String phone, String careman, String model, String sn, Boolean storno, String addrReg,
			String addrFact, Boolean resigned, Boolean isRent, String info, User updatedUser, Date updatedDate, Inventory inventory) {
		this.id = id;
		this.smBranch = smBranch;
		this.cdate = cdate;
		this.contractNumber = contractNumber;
		this.customerFio = customer;
		this.iinbin = iinbin;
		this.dealer = dealer;
		this.phone = phone;
		this.careman = careman;
		this.model = model;
		this.sn = sn;
		this.storno = storno;
		this.addrReg = addrReg;
		this.addrFact = addrFact;
		this.resigned = resigned;
		this.isRent = isRent;
		this.info = info;
		this.updatedUser = updatedUser;
		this.updatedDate = updatedDate;
		this.inventory = inventory;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SM_BRANCH_CODE", nullable = false)
	public SmBranch getSmBranch() {
		return this.smBranch;
	}

	public void setSmBranch(SmBranch smBranch) {
		this.smBranch = smBranch;
	}
	
	@Column(name = "resign_con_sn", length = 20)
	public String getResignConSn() {
		return resignConSn;
	}

	public void setResignConSn(String resignConSn) {
		this.resignConSn = resignConSn;
	}

	@Column(name = "transf_con_sn", length = 20)
	public String getTransfConSn() {
		return transfConSn;
	}

	public void setTransfConSn(String transfConSn) {
		this.transfConSn = transfConSn;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "transfer_date", length = 10)
	public Date getTransferDate() {
		return transferDate;
	}

	public void setTransferDate(Date transferDate) {
		this.transferDate = transferDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "reissue_date", length = 10)
	public Date getReissueDate() {
		return reissueDate;
	}

	public void setReissueDate(Date reissueDate) {
		this.reissueDate = reissueDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "CDATE", length = 10)
	public Date getCdate() {
		return this.cdate;
	}

	public void setCdate(Date cdate) {
		this.cdate = cdate;
	}

	@Column(name = "CONTRACT_NUMBER", nullable = false, length = 45)
	public String getContractNumber() {
		return this.contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	@Column(name = "CUSTOMER_FIO", length = 150)
	public String getCustomerFio() {
		return this.customerFio;
	}

	public void setCustomerFio(String customer) {
		this.customerFio = customer;
	}

	@Column(name = "IINBIN", length = 50)
	public String getIinbin() {
		return this.iinbin;
	}

	public void setIinbin(String iinbin) {
		this.iinbin = iinbin;
	}

	@Column(name = "DEALER", length = 100)
	public String getDealer() {
		return this.dealer;
	}

	public void setDealer(String dealer) {
		this.dealer = dealer;
	}

	@Column(name = "PHONE", length = 150)
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "careman", length = 100)
	public String getCareman() {
		return this.careman;
	}

	public void setCareman(String careman) {
		this.careman = careman;
	}

	@Column(name = "MODEL", length = 45)
	public String getModel() {
		return this.model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	@Column(name = "SN", length = 20)
	public String getSn() {
		return this.sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	@Column(name = "STORNO")
	public Boolean getStorno() {
		return this.storno;
	}

	public void setStorno(Boolean storno) {
		this.storno = storno;
	}

	@Column(name = "ADDR_REG", length = 2000)
	public String getAddrReg() {
		return this.addrReg;
	}

	public void setAddrReg(String addrReg) {
		this.addrReg = addrReg;
	}

	@Column(name = "ADDR_FACT", length = 2000)
	public String getAddrFact() {
		return this.addrFact;
	}

	public void setAddrFact(String addrFact) {
		this.addrFact = addrFact;
	}

	@Column(name = "RESIGNED")
	public Boolean getResigned() {
		return this.resigned;
	}

	public void setResigned(Boolean resigned) {
		this.resigned = resigned;
	}

	@Override
	public SmContract clone() throws CloneNotSupportedException {
		return (SmContract) super.clone();
	}

	@Column(name = "INFO", length = 147)
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "updated_user")
	public User getUpdatedUser() {
		return UserReducer.reduceMax(updatedUser);
	}

	public void setUpdatedUser(User updatedUser) {
		this.updatedUser = updatedUser;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "updated_date", length = 10)
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventory")
	public Inventory getInventory() {
		return inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	@Column(name = "vznos", precision = 16, scale = 4)
	public BigDecimal getVznos() {
		return vznos;
	}

	public void setVznos(BigDecimal vznos) {
		this.vznos = vznos;
	}
	
	@Column(name = "FIRST_MONTH_VZNOS", precision = 16, scale = 4)
	public BigDecimal getFirstMonthVznos() {
		return firstMonthVznos;
	}

	public void setFirstMonthVznos(BigDecimal firstMonthVznos) {
		this.firstMonthVznos = firstMonthVznos;
	}

	@Column(name = "fitter")
	public String getFitter() {
		return fitter;
	}

	public void setFitter(String fitter) {
		this.fitter = fitter;
	}

	@Column(name = "director")
	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	@Column(name = "manager")
	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "resigned_date", length = 10)
	public Date getResignedDate() {
		return resignedDate;
	}

	public void setResignedDate(Date resignedDate) {
		this.resignedDate = resignedDate;
	}

	// ********************************************************************************
	
	@Column(name = "customer_id", length = 15)
	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	@Column(name = "is_yur")
	public Boolean getIsYur() {
		return isYur;
	}

	public void setIsYur(Boolean isYur) {
		this.isYur = isYur;
	}

	@Column(name = "country", length = 45)
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name = "city", length = 45)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "birthday", length = 10)
	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	@Column(name = "passport_no", length = 45)
	public String getPassportNo() {
		return passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "pass_date_issue", length = 10)
	public Date getPassDateIssue() {
		return passDateIssue;
	}

	public void setPassDateIssue(Date passDateIssue) {
		this.passDateIssue = passDateIssue;
	}

	@Column(name = "pass_issued_by", length = 45)
	public String getPassIssuedBy() {
		return passIssuedBy;
	}

	public void setPassIssuedBy(String passIssuedBy) {
		this.passIssuedBy = passIssuedBy;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "pass_date_expire", length = 10)
	public Date getPassDateExpire() {
		return passDateExpire;
	}

	public void setPassDateExpire(Date passDateExpire) {
		this.passDateExpire = passDateExpire;
	}

	@Column(name = "coordinator", length = 100)
	public String getCoordinator() {
		return coordinator;
	}

	public void setCoordinator(String coordinator) {
		this.coordinator = coordinator;
	}

	@Column(name = "first_payment", precision = 16, scale = 4)
	public BigDecimal getFirstPayment() {
		return firstPayment;
	}

	public void setFirstPayment(BigDecimal firstPayment) {
		this.firstPayment = firstPayment;
	}

	@Column(name = "month_term")
	public Integer getMonthTerm() {
		return monthTerm;
	}

	public void setMonthTerm(Integer monthTerm) {
		this.monthTerm = monthTerm;
	}

	@Column(name = "paid_installment", precision = 16, scale = 4)
	public BigDecimal getPaidVznos() {
		return paidVznos;
	}

	public void setPaidVznos(BigDecimal paidVznos) {
		this.paidVznos = paidVznos;
	}

	@Column(name = "currency", length = 3)
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Column(name = "demosec", length = 100)
	public String getDemosec() {
		return demosec;
	}

	public void setDemosec(String demosec) {
		this.demosec = demosec;
	}

	@Column(name = "last_vznos", precision = 16, scale = 4)
	public BigDecimal getLastVznos() {
		return lastVznos;
	}

	public void setLastVznos(BigDecimal lastVznos) {
		this.lastVznos = lastVznos;
	}

	@Column(name = "price", precision = 16, scale = 4)
	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "STORNO_DATE", length = 10)
	public Date getStornoDate() {
		return stornoDate;
	}

	public void setStornoDate(Date stornoDate) {
		this.stornoDate = stornoDate;
	}

	@JsonManagedReference
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "smContract", cascade = CascadeType.ALL)
	public List<SmConSalesPs> getSalesPs() {
		return salesPs;
	}

	public void setSalesPs(List<SmConSalesPs> salesPs) {
		this.salesPs = salesPs;
	}
	
}
