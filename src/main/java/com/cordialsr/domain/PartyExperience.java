package com.cordialsr.domain;

import java.util.Date; 

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.cordialsr.domain.deserializer.PartyExperienceDeserializer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "party_experience")
@JsonDeserialize(using = PartyExperienceDeserializer.class)
public class PartyExperience implements java.io.Serializable, Cloneable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Party party;
	private String company;
	private String position;
	private Date dateHired;
	private Date dateFired;
	private String duties;
	private String reason;
	
	public PartyExperience() {
		
	}
	
	public PartyExperience(Long id) {
		this.id = id;
	}
	
	public PartyExperience(Long id, Party party, String company, String position, Date dateHired, Date dateFired, String duties, String reason) {
		this.id = id;
		this.party = party;
		this.company = company;
		this.position = position;
		this.dateHired = dateHired;
		this.dateFired = dateFired;
		this.duties = duties;
		this.reason = reason;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	@Column(name = "id", unique = true)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "company", length = 45)
	public String getCompany() {
		return this.company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party_id")
	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party= party;
	}
	
	@Column(name = "position", length = 45)
	public String getPosition() {
		return this.position;
	}

	public void setPosition(String position) {
		this.position = position;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "date_hired", length = 10)
	public Date getDateHired() {
		return this.dateHired;
	}

	public void setDateHired(Date dateHired) {
		this.dateHired = dateHired;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "date_fired", length = 10)
	public Date getDateFired() {
		return this.dateFired;
	}

	public void setDateFired(Date dateFired) {
		this.dateFired = dateFired;
	}
	
	@Column(name = "duties", length = 45)
	public String getDuties() {
		return this.duties;
	}

	public void setDuties(String duties) {
		this.duties = duties;
	}
	
	@Column(name = "reason", length = 245)
	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
	@Override
	public PartyExperience clone() throws CloneNotSupportedException {
		return (PartyExperience) super.clone();
	}

}
