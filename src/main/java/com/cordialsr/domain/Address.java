package com.cordialsr.domain;
// 03.08.2017 11:54:04 by stalbekr@gmail.com

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;

import java.util.Calendar;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.cordialsr.domain.serializer.AddressSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "address")
//@JsonSerialize(using = AddressSerializer.class)
public class Address implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private AddressType addressType;
	private City city;
	private CityArea cityArea;
	private Country country;
	private Oblast oblast;
	private Party party;
	private String district;
	private String street;
	private String houseNumber;
	private String flatNumber;
	private String zipCode;
	private String info;
	private CrmLead lead;
	private Calendar cpuDate;
	

	public Address() {
	}
	
	public Address(Long id) {
		this.id = id;
	}

	public Address(AddressType addressType, City city, CityArea cityArea, Oblast oblast, String district, String street, CrmLead lead) {
		this.addressType = addressType;
		this.city = city;
		this.cityArea = cityArea;
		this.oblast = oblast;
		this.district = district;
		this.street = street;
		this.lead = lead;
	}

	public Address(Long id, AddressType addressType, City city, CityArea cityArea, Country country, Oblast oblast, Party party,
			String district, String street, String houseNumber, String flatNumber, String zipCode, 
			String info, CrmLead lead) {
		this.id = id;
		this.addressType = addressType;
		this.city = city;
		this.cityArea = cityArea;
		this.country = country;
		this.oblast = oblast;
		this.party = party;
		this.district = district;
		this.street = street;
		this.houseNumber = houseNumber;
		this.flatNumber = flatNumber;
		this.zipCode = zipCode;
		this.info = info;
		this.lead = lead;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "address_type", nullable = false)
	public AddressType getAddressType() {
		return this.addressType;
	}

	public void setAddressType(AddressType addressType) {
		this.addressType = addressType;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "city_id")
	public City getCity() {
		return this.city;
	}

	public void setCity(City city) {
		this.city = city;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "rayon_id")
	public CityArea getCityArea() {
		return this.cityArea;
	}

	public void setCityArea(CityArea cityArea) {
		this.cityArea = cityArea;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "country")
	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "oblast_id")
	public Oblast getOblast() {
		return this.oblast;
	}

	public void setOblast(Oblast oblast) {
		this.oblast = oblast;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party_id")
	public Party getParty() {
		return this.party;
	}

	public void setParty(Party party) {
		this.party = party;
	}
	
	@Column(name = "district", length = 60)
	public String getDistrict() {
		return this.district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	@Column(name = "street", length = 200)
	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	@Column(name = "house_number", length = 5)
	public String getHouseNumber() {
		return this.houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	@Column(name = "flat_number", length = 5)
	public String getFlatNumber() {
		return this.flatNumber;
	}

	public void setFlatNumber(String flatNumber) {
		this.flatNumber = flatNumber;
	}

	@Column(name = "zip_code", length = 12)
	public String getZipCode() {
		return this.zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@Column(name = "info")
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "lead")
	public CrmLead getLead() {
		return lead;
	}

	public void setLead(CrmLead lead) {
		this.lead = lead;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cpu_date")
	public Calendar getCpuDate() {
		return cpuDate;
	}

	public void setCpuDate(Calendar cpuDate) {
		this.cpuDate = cpuDate;
	}
	
	@JsonIgnore
	@Transient
	public String getAddrString() {
		String a = "";
	    if (zipCode != null && zipCode.length() > 0 && !zipCode.equals("null")) {
	        a += (a.length() > 0) ? ", " : "";
	        a += zipCode;
	      }
	      if (city != null && !city.getName().equals("null")) {
	    	a += (a.length() > 0) ? ", " : "";
	        a += "г. " + city.getName();
	      }
	      if (cityArea != null && !cityArea.getName().equals("null")) {
	    	a += (a.length() > 0) ? ", " : "";
	        a += cityArea.getName() + " р-н.";
	      }
	      if (district != null && district.length() > 0 && !district.equals("null")) {
	    	a += (a.length() > 0) ? ", " : "";
	        a += "мкрн. " + district;
	      }
	      if (street != null && street.length() > 0 && !street.equals("null")) {
	    	a += (a.length() > 0) ? ", " : "";
	        a += "ул. " + street;
	      }
	      if (houseNumber != null && houseNumber.length() > 0 && !houseNumber.equals("null")) {
	    	a += (a.length() > 0) ? ", " : "";
	        a += houseNumber;
	      }
	      if (flatNumber != null && flatNumber.length() > 0 && !flatNumber.equals("null")) {
	    	a += (a.length() > 0) ? ", " : "";
	        a += flatNumber;
	      }
		  return a; 
	}

	// *******************************************************************************************************
	
	@Override
	public String toString() {
		return "Address [id=" + id + ", addressType=" + addressType + ", city=" + city + ", cityArea=" + cityArea
				+ ", country=" + country + ", oblast=" + oblast + ", party=" + ((party != null) ? party.getId() : "null")
				+ ", district=" + district + ", street=" + street
				+ ", houseNumber=" + houseNumber + ", flatNumber=" + flatNumber + ", zipCode=" + zipCode 
				+ ", info=" + info + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addressType == null) ? 0 : addressType.getId());
		result = prime * result + ((city == null) ? 0 : city.getId());
		result = prime * result + ((cityArea == null) ? 0 : cityArea.getId());
		result = prime * result + ((country == null) ? 0 : country.getId());
		result = prime * result + ((cpuDate == null) ? 0 : cpuDate.hashCode());
		result = prime * result + ((district == null) ? 0 : district.hashCode());
		result = prime * result + ((flatNumber == null) ? 0 : flatNumber.hashCode());
		result = prime * result + ((houseNumber == null) ? 0 : houseNumber.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((lead == null || lead.getId() == null) ? 0 : lead.getId().hashCode());
		result = prime * result + ((oblast == null) ? 0 : oblast.getId());
		result = prime * result + ((party == null) ? 0 : (party.getId() == null) ? 0 : party.getId().hashCode());
		result = prime * result + ((street == null) ? 0 : street.hashCode());
		result = prime * result + ((zipCode == null) ? 0 : zipCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		if (addressType == null) {
			if (other.addressType != null)
				return false;
		} else if (!addressType.getId().equals(other.addressType.getId()))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.getId().equals(other.city.getId()))
			return false;
		if (cityArea == null) {
			if (other.cityArea != null)
				return false;
		} else if (!cityArea.getId().equals(other.cityArea.getId()))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.getId().equals(other.country.getId()))
			return false;
		if (cpuDate == null) {
			if (other.cpuDate != null)
				return false;
		} else if (!cpuDate.equals(other.cpuDate))
			return false;
		if (district == null) {
			if (other.district != null)
				return false;
		} else if (!district.equals(other.district))
			return false;
		if (flatNumber == null) {
			if (other.flatNumber != null)
				return false;
		} else if (!flatNumber.equals(other.flatNumber))
			return false;
		if (houseNumber == null) {
			if (other.houseNumber != null)
				return false;
		} else if (!houseNumber.equals(other.houseNumber))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (info == null) {
			if (other.info != null)
				return false;
		} else if (!info.equals(other.info))
			return false;
		if (lead == null) {
			if (other.lead != null)
				return false;
		} else if (!lead.getId().equals(other.lead.getId()))
			return false;
		if (oblast == null) {
			if (other.oblast != null)
				return false;
		} else if (!oblast.getId().equals(other.oblast.getId()))
			return false;
		if (party == null) {
			if (other.party != null)
				return false;
		} else if (!party.getId().equals(other.party.getId()))
			return false;
		if (street == null) {
			if (other.street != null)
				return false;
		} else if (!street.equals(other.street))
			return false;
		if (zipCode == null) {
			if (other.zipCode != null)
				return false;
		} else if (!zipCode.equals(other.zipCode))
			return false;
		return true;
	}

	@Override
	public Address clone() throws CloneNotSupportedException {
		return (Address) super.clone();
	}
	
}
