package com.cordialsr.domain.security;
// Generated 10.05.2017 11:48:36 by stalbekr@gmail.com

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "role_transaction_permission")
public class RoleTransactionPermission implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5974238982824420883L;
	private Integer id;
	private Role role;
	private Transaction transaction;
	private Integer permission;

	public RoleTransactionPermission() {
	}

	public RoleTransactionPermission(Role role, Transaction transaction) {
		this.role = role;
		this.transaction = transaction;
	}

	public RoleTransactionPermission(Role role, Transaction transaction, Integer permission) {
		this.role = role;
		this.transaction = transaction;
		this.permission = permission;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "roleid", nullable = false)
	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "transaction_id", nullable = false)
	public Transaction getTransaction() {
		return this.transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	@Column(name = "permission")
	public Integer getPermission() {
		return this.permission;
	}

	public void setPermission(Integer permission) {
		this.permission = permission;
	}

	@Override
	public RoleTransactionPermission clone() throws CloneNotSupportedException {
		return (RoleTransactionPermission) super.clone();
	}	
}
