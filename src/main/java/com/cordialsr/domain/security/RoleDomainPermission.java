package com.cordialsr.domain.security;
// Generated 10.05.2017 11:48:36 by stalbekr@gmail.com

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.cordialsr.domain.Domain;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "role_domain_permission")
public class RoleDomainPermission implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5672468999360100876L;
	private Integer id;
	private Domain domain;
	private Role role;
	private Integer permission;

	public RoleDomainPermission() {
	}

	public RoleDomainPermission(Domain domain, Role role) {
		this.domain = domain;
		this.role = role;
	}

	public RoleDomainPermission(Domain domain, Role role, Integer permission) {
		this.domain = domain;
		this.role = role;
		this.permission = permission;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "domains_id", nullable = false)
	public Domain getDomain() {
		return this.domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "roleid", nullable = false)
	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@Column(name = "permission")
	public Integer getPermission() {
		return this.permission;
	}

	public void setPermission(Integer permission) {
		this.permission = permission;
	}

}
