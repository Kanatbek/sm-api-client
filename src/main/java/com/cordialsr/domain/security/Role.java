package com.cordialsr.domain.security;
// Generated 10.05.2017 11:48:36 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "role")
public class Role implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer roleId;
	private String name;
	private Boolean enabled;
	private Date createdDate;
	private Date updatedDate;
	private String label;
	
	private Set<RoleDomainPermission> roleDomainPermissions = new HashSet<>(0);
	private Set<RoleTransactionPermission> roleTransactionPermissions = new HashSet<>(0);
	private Set<User> users = new HashSet<>(0);

	public Role() {
	}

	public Role(String rolename, Boolean enabled, Date createdDate, Date updatedDate, Set<RoleDomainPermission> roleDomainPermissions,
			Set<RoleTransactionPermission> roleTransactionPermissions, Set<User> users) {
		this.name = rolename;
		this.enabled = enabled;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.roleDomainPermissions = roleDomainPermissions;
		this.roleTransactionPermissions = roleTransactionPermissions;
		this.users = users;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "roleid", unique = true, nullable = false)
	public Integer getRoleId() {
		return this.roleId;
	}

	public void setRoleId(Integer roleid) {
		this.roleId = roleid;
	}

	@Column(name = "rolename", length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String rolename) {
		this.name = rolename;
	}

	@Column(name = "enabled")
	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "created_date", length = 10)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "updated_date", length = 10)
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "role", cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<RoleDomainPermission> getRoleDomainPermissions() {
		return this.roleDomainPermissions;
	}

	public void setRoleDomainPermissions(Set<RoleDomainPermission> roleDomainPermissions) {
		this.roleDomainPermissions = roleDomainPermissions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "role", cascade = CascadeType.ALL, orphanRemoval = true)
	public Set<RoleTransactionPermission> getRoleTransactionPermissions() {
		return this.roleTransactionPermissions;
	}

	public void setRoleTransactionPermissions(Set<RoleTransactionPermission> roleTransactionPermissions) {
		this.roleTransactionPermissions = roleTransactionPermissions;
	}

	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "user_role", joinColumns = {
			@JoinColumn(name = "roleid", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "userid", nullable = false, updatable = false) })
	public Set<User> getUsers() {
		return this.users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	@Column(name = "label", length = 100)
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public Role clone() throws CloneNotSupportedException {
		return (Role) super.clone();
	}	
	
}
