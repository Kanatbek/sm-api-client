package com.cordialsr.domain.security;
// Generated 10.05.2017 11:48:36 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "transaction")
public class Transaction implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String TRCODE_NEW_CONTRACT = "MSNWCON";
	
	private Integer id;
	private String code;
	private String route;
	private String path;
	private String name;
	private String description;
	
	public Transaction() {
	}
	
	public Transaction(Integer id) {
		this.id = id;
	}

	public Transaction(String name, String description, String path) {
		this.name = name;
		this.description = description;
		this.path = path;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description", length = 100)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "path", length = 200)
	public String getPath() {
		return this.path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Column(name = "code", length = 10)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "route", length = 45)
	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	@Override
	public Transaction clone() throws CloneNotSupportedException {
		return (Transaction) super.clone();
	}
	
}
