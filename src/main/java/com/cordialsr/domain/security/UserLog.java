package com.cordialsr.domain.security;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cordialsr.domain.deserializer.AwardDeserializer;
import com.cordialsr.domain.deserializer.UserLogDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@JsonDeserialize(using = UserLogDeserializer.class)
@Table(name = "user_log")
public class UserLog {
	
	public static final String TYPE_LOGIN = "LOGIN";
	public static final String TYPE_LOGOUT = "LOGOUT";
	public static final String TYPE_VIEW = "VIEW";
	public static final String TYPE_SAVE = "SAVE";
	public static final String TYPE_CLEVER = "CLEVER";
	public static final String TYPE_EDIT = "EDIT";
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date", nullable = false)
	private Date date;

	@Temporal(TemporalType.TIME)
	@Column(name = "time", nullable = false)
	private Date time;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private User user;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tr_id")
	private Transaction transaction;
	
	@Column(name = "log_type", nullable = false, length = 10)
	private String logType;
	
	@Column(name = "info", nullable = false, length = 255)
	private String info;
	
	public UserLog() {}

	public UserLog(Long id, Date date, Date time, User user, Transaction transaction, String logType, String info) {
		this.id = id;
		this.date = date;
		this.time = time;
		this.user = user;
		this.transaction = transaction;
		this.logType = logType;
		this.info = info;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	public String getLogType() {
		return logType;
	}

	public void setLogType(String logType) {
		this.logType = logType;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
}
