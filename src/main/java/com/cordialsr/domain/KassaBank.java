package com.cordialsr.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "kassa_bank")
public class KassaBank implements Serializable, Cloneable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Company company;
	private Branch branch;
	private String name;
	private Boolean isBank;
	private Party bank;
	private String accountNumber;
	private Currency currency;
	
	public KassaBank() {
	}
	
	public KassaBank(Integer id) {
		this.id = id;
	}
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company", nullable = false)
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch", nullable = false)
	public Branch getBranch() {
		return branch;
	}
	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	
	@Column(name = "name", length = 45)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "is_bank")
	public Boolean getIsBank() {
		return isBank;
	}
	public void setIsBank(Boolean isBank) {
		this.isBank = isBank;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bank", nullable = false)
	public Party getBank() {
		return bank;
	}
	public void setBank(Party bank) {
		this.bank = bank;
	}
	
	@Column(name = "account_number", length = 45, unique = true)
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currency", nullable = false)
	public Currency getCurrency() {
		return currency;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	@Override
	public KassaBank clone() throws CloneNotSupportedException {
		return (KassaBank) super.clone();
	}
	
}
