package com.cordialsr.domain;
// Generated 23.05.2017 12:18:03 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "sm_con_paid")
public class SmConPayment implements Cloneable {

	private Integer id;
	private Date period;
	private String contractNumber;
	private BigDecimal amount;
	private Boolean monthClosed;
	private String contractCode;
	private Date datePaid;
	private String currency;
		
	public SmConPayment() {
		super();
	}

	public SmConPayment(Integer id, Date period, String contractNumber, BigDecimal amount, Boolean monthClosed,
				String contractCode, Date datePaid, String currency) {
		super();
		this.id = id;
		this.period = period;
		this.contractNumber = contractNumber;
		this.amount = amount;
		this.monthClosed = monthClosed;
		this.contractCode = contractCode;
		this.datePaid = datePaid;
		this.currency = currency;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "period", length = 10)
	public Date getPeriod() {
		return period;
	}

	public void setPeriod(Date period) {
		this.period = period;
	}
	
	@Column(name = "CONTRACT_NUMBER", nullable = false, length = 20)
	public String getContractNumber() {
		return this.contractNumber;
	}
	
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	@Column(name = "month_closed")
	public Boolean getMonthClosed() {
		return monthClosed;
	}

	public void setMonthClosed(Boolean monthClosed) {
		this.monthClosed = monthClosed;
	}

	@Column(name = "amount", precision = 16, scale = 4)
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Column(name = "contract_code", length = 10)
	public String getContractCode() {
		return contractCode;
	}

	public void setContractCode(String contractCode) {
		this.contractCode = contractCode;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "date_paid", length = 10)
	public Date getDatePaid() {
		return datePaid;
	}

	public void setDatePaid(Date datePaid) {
		this.datePaid = datePaid;
	}

	@Column(name = "currency", length = 10)
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
		
}
