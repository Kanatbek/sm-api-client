package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "fin_doc_type")
public class FinDocType implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
//	BI	Приходное поручение Банку
//	BO	Платежное поручение Банку
//	EX	Конвертация валюты
//	KI	Приходный Кассовый Ордер
//	KO	Расходный Кассовый Ордер
//	RB	Квитанция на оплату
//	TR	Перевод Денежных Средств

	public static final String TYPE_BI_BANK_INCOME_ORDER = "BI";
	public static final String TYPE_BO_BANK_RASHOD_ORDER = "BO";
	public static final String TYPE_KI_KASSA_INCOME_ORDER = "KI";
	public static final String TYPE_KO_KASSA_RASHOD_ORDER = "KO";
	public static final String TYPE_EX_EXCHANGE_CURRENCY = "EX";
	public static final String TYPE_RB_RECEIPT_BILL = "RB";
	public static final String TYPE_TR_TRANSFER_MONEY = "TR";
	
//	WI	Прием ТМЗ
//	WO	Передача ТМЗ
//	WG	ГДТ по Импорту ТМЗ
//	WE	Дополнительные расходы по импорту
//	WT	Перемещение ТМЗ

	public static final String TYPE_WI_STOCK_WRITE_IN = "WI";
	public static final String TYPE_WO_STOCK_WRITE_OFF = "WO";
	public static final String TYPE_WG_IMPORT_GTD = "WG";
	public static final String TYPE_WE_IMPORT_EXPENCES = "WE";
	public static final String TYPE_WT_TRANSFER_TMZ = "WT";
	public static final String TYPE_WR_RETURN_TMZ = "WR";
	
//	GC	Документ Продажи	Финансовый документ договора Продажи
//	GR	Документ Аренды	Финансовый документ договора Аренды
	
	public static final String TYPE_GS_GENERAL_SELL_CONTRACT = "GS";
	public static final String TYPE_GR_GENERAL_RENT_CONTRACT = "GR";
	public static final String TYPE_DC_DISCOUNT_COMPANY = "DC";
	// public static final String TYPE_DE_DISCOUNT_EMPLOYEE = "DE";
	public static final String TYPE_CP_CONTRACT_PAYMENT = "CP";
	
	public static final String TYPE_IR_INVOICE_RECIEVABLE = "IR";
	public static final String TYPE_IP_INVOICE_PAYABLE = "IP";
	
	
//	PB	Payroll Bonus	Начисление Бонуса
//	PP	Payroll Premi	Начисление Премии
//	PS	Payroll Salary	Начисление ЗП
//	SO  Зачет дебиторской задолженности со счета сотрудника, Set off accounts receivable from an employee account
	
	public static final String TYPE_PP_PAYROLL_PREMI = "PP";
	public static final String TYPE_PB_PAYROLL_BONUS = "PB";
	public static final String TYPE_PS_PAYROLL_SALARY = "PS";
	public static final String TYPE_SO_SETOFF = "SO";
	
	public static final String TYPE_ST_STORNO = "ST";
	
	public static final String PR_PAYABLE = "P";
	public static final String PR_RECEIVABLE = "R";
	
	private String code;
	private String name;
	private String info;
	private String pr;
	
	public FinDocType() {
	}

	public FinDocType(String code) {
		this.code = code;
	}

	public FinDocType(String code, String name, String info, String pr) {
		this.code = code;
		this.name = name;
		this.info = info;
		this.pr = pr;
	}

	@Id
	@Column(name = "code", unique = true, nullable = false, length = 2)
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "name", length = 100)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "info", length = 255)
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@Column(name = "pr", length = 1)
	public String getPr() {
		return pr;
	}

	public void setPr(String pr) {
		this.pr = pr;
	}

	// ********************************************************************************
	
	@Override
	public String toString() {
		return "FinDocType [code=" + code + ", name=" + name + ", info=" + info + ", pr=" + pr + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((pr == null) ? 0 : pr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinDocType other = (FinDocType) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (info == null) {
			if (other.info != null)
				return false;
		} else if (!info.equals(other.info))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (pr == null) {
			if (other.pr != null)
				return false;
		} else if (!pr.equals(other.pr))
			return false;
		return true;
	}

	@Override
	public FinDocType clone() throws CloneNotSupportedException {
		return (FinDocType) super.clone();
	}
	
}
