package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "fin_activity_type")
public class FinActivityType implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static String TYPE_FINANCE = "FIN";
	public static String TYPE_INVESTMENT = "INV";
	public static String TYPE_OPERATIVE = "OPR";
	
//	FIN	Финансовая деятельность
//	INV	Инвестиционная деятельность
//	OPR	Оперативная деятельность
	
	private String code;
	private String name;
	
	public FinActivityType() {
	}

	public FinActivityType(String code) {
		this.code = code;
	}

	public FinActivityType(String code, String name) {
		this.code = code;
		this.name = name;
	}

	@Id
	@Column(name = "code", unique = true, nullable = false, length = 3)
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "name", length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// ****************************************************************************************************
	
	@Override
	public String toString() {
		return "FinOperType [code=" + code + ", name=" + name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinActivityType other = (FinActivityType) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public FinActivityType clone() throws CloneNotSupportedException {
		return (FinActivityType) super.clone();
	}

	
}

