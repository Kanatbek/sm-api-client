package com.cordialsr.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cordialsr.domain.deserializer.SmCallDeserializer;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "sm_call")
@JsonDeserialize(using = SmCallDeserializer.class)
public class SmCall {

	public static final int STATUS_ANSWER = 1;
	public static final int STATUS_OFF = 2;
	public static final int STATUS_MISSED = 3;
	public static final int STATUS_REJECTED = 4;
	public static final int STATUS_TNB = 5;
	
//	1 ОТВЕТ
//	2 ОТКЛЮЧЕН
//	3 НЕ ТУДА ПОПАЛ
//	4 ОТКЛОНИЛ Звонок
//	5 ТНБ
	
	private Integer id;
	private Company company;
	private Branch branch;
	private Contract contract;
	private SmService service;
	private Party party;
	private PhoneNumber phoneNumber;
	private Date callDate;
	private Date callTime;
	private User user;
	private String info;
	private String responder;
	private Integer status;
	private Boolean isOutgoing;
	private String callNumber;
	
	public SmCall() {
		
	}
	
	public SmCall(Integer id, Company company, Branch branch, Contract contract, SmService service, Party party, PhoneNumber phoneNumber, Date callDate,
			Date callTime, User user, String info, String responder, Integer status, Boolean isOutgoing, String callNumber) {
		this.id = id;
		this.company = company;
		this.branch = branch;
		this.contract = contract;
		this.service = service;
		this.party = party;
		this.phoneNumber = phoneNumber;
		this.callDate = callDate;
		this.callTime = callTime;
		this.user = user;
		this.info = info;
		this.responder = responder;
		this.status = status;
		this.isOutgoing = isOutgoing;
		this.callNumber = callNumber;
	}
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company", nullable = false)
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch", nullable = false)
	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contract_id", nullable = false)
	public Contract getContract() {
		return contract;
	}
	public void setContract(Contract contract) {
		this.contract = contract;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "service_id")
	public SmService getService() {
		return service;
	}
	public void setService(SmService service) {
		this.service = service;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "phone_number", nullable = false)
	public PhoneNumber getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(PhoneNumber phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "call_date", nullable = false, length = 10)
	public Date getCallDate() {
		return callDate;
	}
	public void setCallDate(Date callDate) {
		this.callDate = callDate;
	}
	
	@Temporal(TemporalType.TIME)
	@Column(name = "call_time", nullable = false, length = 10)
	public Date getCallTime() {
		return callTime;
	}
	public void setCallTime(Date callTime) {
		this.callTime = callTime;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user")
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	@Column(name = "info")
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	
	@Column(name = "responder")
	public String getResponder() {
		return responder;
	}
	public void setResponder(String responder) {
		this.responder = responder;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party")
	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}

	@Column(name = "is_outgoing")
	public Boolean getIsOutgoing() {
		return isOutgoing;
	}

	public void setIsOutgoing(Boolean isOutgoing) {
		this.isOutgoing = isOutgoing;
	}

	@Column(name = "call_number")
	public String getCallNumber() {
		return callNumber;
	}

	public void setCallNumber(String callNumber) {
		this.callNumber = callNumber;
	}
	
	
		
}
