package com.cordialsr.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.cordialsr.domain.deserializer.PayrollDeserializer;
import com.cordialsr.domain.reducer.UserReducer;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@JsonDeserialize(using = PayrollDeserializer.class)
@Table(name = "payroll")
public class Payroll {

	public static final String KIND_SALARY = "S";
	public static final String KIND_PREMI = "P";
	public static final String KIND_BONUS = "B";
	
	public static final double AGE_UNTIL_6_MONTH_STORNO_RATIO = 1;
	public static final double AGE_UNTIL_12_MONTH_STORNO_RATIO = 1;
	public static final double AGE_ABOVE_12_MONTH_STORNO_RATIO = 1;
	
//	public static final double AGE_UNTIL_1_MONTH_STORNO_RATIO = 1;
//	public static final double AGE_FROM_1_TO_6_MONTH_STORNO_RATIO = 0.5;
//	public static final double AGE_ABOVE_6_MONTH_STORNO_RATIO = 0;
	
	private Long id;
	private Company company;
	private Branch branch;
	private Party party;
	private Employee employee;
	private String kind;
	private Contract contract;
	private ContractAwardSchedule conAward;
	private Date payrollDate;
	private Date postedDate;
	private BigDecimal sum;
	private Currency currency;
	private Boolean approved;
	private Boolean posted;
	private String refkey;
	private String info;
	private Integer month;
	private Integer year;
	private String trCode;
	private User user;
	private BigDecimal rate;
	private BigDecimal accrued;
	
	@Transient
	private Boolean storno;

	@Transient
	private Integer rent;
	
	@Transient
	private Integer sale;
	
	@Transient
	private Integer total;
	
	@Transient
	private Boolean restricted;
	
	public Payroll() {
		
	}
	
	public Payroll(Long id, Company company, Branch branch, Party party, Employee employee, 
			String kind, Contract contract, ContractAwardSchedule conAward, Date payrollDate, Date postedDate, BigDecimal sum,
			Currency currency, Boolean approved, Boolean restricted, Boolean posted, String refkey, String info,
			Integer month, Integer year, String trCode, User user, BigDecimal rate, BigDecimal accrued) {
		this.id = id;
		this.company = company;
		this.branch = branch;
		this.party = party;
		this.employee = employee;
		this.kind = kind;
		this.contract = contract;
		this.conAward = conAward;
		this.payrollDate = payrollDate;
		this.postedDate = postedDate;
		this.sum = sum;
		this.currency = currency;
		this.approved = approved;
		this.posted = posted;
		this.refkey = refkey;
		this.info = info;
		this.month = month;
		this.year = year;
		this.trCode = trCode;
		this.user = user;
		this.rate = rate;
		this.accrued = accrued;
		this.restricted = restricted;
	}
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company")
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch")
	public Branch getBranch() {
		return branch;
	}
	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party")
	public Party getParty() {
		return party;
	}
	public void setParty(Party party) {
		this.party = party;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "employee")
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	
	@Column(name = "month")
	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	@Column(name = "year")
	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	@Column(name = "kind", length = 1)
	public String getKind() {
		return kind;
	}
	public void setKind(String kind) {
		this.kind = kind;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "payroll_date", length = 10, nullable = false)
	public Date getPayrollDate() {
		return payrollDate;
	}

	public void setPayrollDate(Date payrollDate) {
		this.payrollDate = payrollDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "posted_date", length = 10)
	public Date getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(Date postedDate) {
		this.postedDate = postedDate;
	}
	
	@Column(name = "sum")
	public BigDecimal getSum() {
		return sum;
	}
	
	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currency")
	public Currency getCurrency() {
		return currency;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	
	@Column(name = "approved")
	public Boolean getApproved() {
		return approved;
	}
	public void setApproved(Boolean approved) {
		this.approved = approved;
	}
	
	@Column(name = "posted")
	public Boolean getPosted() {
		return posted;
	}
	public void setPosted(Boolean posted) {
		this.posted = posted;
	}
	
	@Column(name = "refkey", length = 16)
	public String getRefkey() {
		return refkey;
	}

	public void setRefkey(String refkey) {
		this.refkey = refkey;
	}

	@Column(name = "info", length = 155)
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}

	@Column(name = "tr_code", length = 10)
	public String getTrCode() {
		return trCode;
	}

	public void setTrCode(String trCode) {
		this.trCode = trCode;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user")
	public User getUser() {
		return UserReducer.reduceMax(user);
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "rate", precision = 21, scale = 12)
	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	@Column(name = "accrued", precision = 21, scale = 12)
	public BigDecimal getAccrued() {
		return accrued;
	}

	public void setAccrued(BigDecimal accrued) {
		this.accrued = accrued;
	}

//	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contract_id")
	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}
	
	@Transient
	public String getContractNumber() {
		return (contract != null) ? contract.getContractNumber() : null;
	}

//	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "con_award_id")
	public ContractAwardSchedule getConAward() {
		return conAward;
	}

	@Transient
	public void setConAward(ContractAwardSchedule conAward) {
		this.conAward = conAward;
	}

	@Transient
	public Integer getRent() {
		return rent;
	}

	@Transient
	public void setRent(Integer rent) {
		this.rent = rent;
	}

	@Transient
	public Integer getSale() {
		return sale;
	}

	@Transient
	public void setSale(Integer sale) {
		this.sale = sale;
	}

	@Transient
	public Integer getTotal() {
		return total;
	}

	@Transient
	public void setTotal(Integer total) {
		this.total = total;
	}

	@Transient
	public Boolean getRestricted() {
		return restricted;
	}

	@Transient
	public void setRestricted(Boolean restricted) {
		this.restricted = restricted;
	}

	@Transient
	public Boolean getStorno() {
		return storno;
	}

	@Transient
	public void setStorno(Boolean storno) {
		this.storno = storno;
	}
	
}
