package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "fin_entry")
public class FinEntry implements java.io.Serializable, Cloneable {
	
	public static String DC_DEBET = "D";
	public static String DC_CREDIT = "C";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Company company;
	private Branch branch;
	private FinDoc finDoc;
	private String postkey;
	private Party party;
	private String dc;
	private FinGlAccount glAccount;
	private BigDecimal dsumm;
	private Currency dcurrency;
	private BigDecimal rate;
	private BigDecimal wsumm;
	private Currency wcurrency;
	private String info;
	private String docno;
	private Integer month;
	private String year;
	private Date ddate;
	private String refkey;
	private FinCashflowStatement cashflowStatement;
	private FinActivityType finActType;
	private Boolean isTovar;
	private String itemName;
	private Inventory inventory;
	private BigDecimal quantity;
	private Unit unit;
	private BigDecimal price;
	private Integer entrycount; 
	private Department department;
	private InvoiceItem invoiceItem;
	private KassaBank kassaBank; 
	private Date cpuDate;
	private Date updatedDate;
	
	public FinEntry() {
	}

	public FinEntry(Long id) {
		this.id = id;
	}
	
	public FinEntry(String dc) {
		this.dc = dc;
	}

	public FinEntry(Company company, Branch branch, FinDoc finDoc, String postkey, Party party, String dc, FinGlAccount glAccount,  
			BigDecimal dsumm, Currency dcurrency, BigDecimal rate, BigDecimal wsumm, Currency wcurrency, String info, String docno,
			Integer month, String year, Date ddate, String refkey, FinCashflowStatement cashflowStatement,
			FinActivityType finActType, Boolean isTovar, String itemName,
			Inventory inventory, BigDecimal quantity, Unit unit, BigDecimal price, Integer entrycount,
			Department department, InvoiceItem invoiceItem, KassaBank kassaBank) {
		this.company = company;
		this.branch = branch;
		this.finDoc = finDoc;
		this.postkey = postkey;
		this.party = party;
		this.dc = dc;
		this.glAccount = glAccount;
		this.dsumm = dsumm;
		this.dcurrency = dcurrency;
		this.rate = rate;
		this.wsumm = wsumm;
		this.wcurrency = wcurrency;
		this.info = info;
		this.docno = docno;
		this.month = month;
		this.year = year;
		this.ddate = ddate;
		this.refkey = refkey;
		this.cashflowStatement = cashflowStatement;
		this.finActType = finActType;
		this.isTovar = isTovar;
		this.itemName = itemName;
		this.inventory = inventory;
		this.quantity = quantity;
		this.unit = unit;
		this.price = price;
		this.entrycount = entrycount;
		this.department = department;
		this.invoiceItem = invoiceItem;
		this.kassaBank = kassaBank;
	}


	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	// *****************************************************
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company")
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch")
	public Branch getBranch() {
		return this.branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	
	// @JsonManagedReference
	// @JsonBackReference
//	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FIN_DOC_id", nullable = false)
	public FinDoc getFinDoc() {
		return this.finDoc;
	}

	public void setFinDoc(FinDoc finDoc) {
		this.finDoc = finDoc;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party_id")
	public Party getParty() {
		return this.party;
	}

	public void setParty(Party party) {
		this.party = party;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "gl_account")
	public FinGlAccount getGlAccount() {
		return glAccount;
	}

	public void setGlAccount(FinGlAccount glAccount) {
		this.glAccount = glAccount;
	}

	@Column(name = "postkey", length=2)
	public String getPostkey() {
		return postkey;
	}

	public void setPostkey(String postkey) {
		this.postkey = postkey;
	}

	@Column(name = "dc", length=1)
	public String getDc() {
		return dc;
	}

	public void setDc(String dc) {
		this.dc = dc;
	}

	@Column(name = "dsumm", precision = 21, scale = 5)
	public BigDecimal getDsumm() {
		return dsumm;
	}
	
	public void setDsumm(BigDecimal dsumm) {
		this.dsumm = dsumm;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "dcurrency")
	public Currency getDcurrency() {
		return dcurrency;
	}

	public void setDcurrency(Currency dcurrency) {
		this.dcurrency = dcurrency;
	}


	@Column(name = "wsumm", precision = 21, scale = 4)
	public BigDecimal getWsumm() {
		return wsumm;
	}

	public void setWsumm(BigDecimal wsumm) {
		this.wsumm = wsumm;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "wcurrency")
	public Currency getWcurrency() {
		return wcurrency;
	}

	public void setWcurrency(Currency wcurrency) {
		this.wcurrency = wcurrency;
	}
	
	@Column(name = "info")
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
	@Column(name = "docno", length = 16)
	public String getDocno() {
		return docno;
	}

	public void setDocno(String docno) {
		this.docno = docno;
	}
	
	@Column(name = "month")
	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}
	
	@Column(name = "year", length = 4)
	public String getYear() {
		return this.year;
	}

	public void setYear(String year) {
		this.year = year;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "ddate", length = 10)
	public Date getDdate() {
		return ddate;
	}

	public void setDdate(Date ddate) {
		this.ddate = ddate;
	}

	@Column(name = "refkey", length = 20)
	public String getRefkey() {
		return refkey;
	}

	public void setRefkey(String refkey) {
		this.refkey = refkey;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CASHFLOW_STATEMENT")
	public FinCashflowStatement getCashflowStatement() {
		return cashflowStatement;
	}

	public void setCashflowStatement(FinCashflowStatement cashflowStatement) {
		this.cashflowStatement = cashflowStatement;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ACT_TYPE")
	public FinActivityType getFinActType() {
		return finActType;
	}

	public void setFinActType(FinActivityType finActType) {
		this.finActType = finActType;
	}

	@Column(name = "is_tovar")
	public Boolean getIsTovar() {
		return isTovar;
	}

	public void setIsTovar(Boolean isTovar) {
		this.isTovar = isTovar;
	}
	
	@Column(name = "item_name")
	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventory_id")
	public Inventory getInventory() {
		return inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	@Column(name = "quantity", precision = 21, scale = 12)
	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "unit")
	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Column(name = "price", precision = 21, scale = 12)
	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Column(name = "entrycount")
	public Integer getEntrycount() {
		return entrycount;
	}

	public void setEntrycount(Integer entrycount) {
		this.entrycount = entrycount;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "department")
	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invoice_item_id")
	public InvoiceItem getInvoiceItem() {
		return invoiceItem;
	}

	public void setInvoiceItem(InvoiceItem invoiceItem) {
		this.invoiceItem = invoiceItem;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "kassa_bank_id")
	public KassaBank getKassaBank() {
		return kassaBank;
	}

	public void setKassaBank(KassaBank kassaBank) {
		this.kassaBank = kassaBank;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "cpu_date", length = 10)
	public Date getCpuDate() {
		return cpuDate;
	}

	public void setCpuDate(Date cpuDate) {
		this.cpuDate = cpuDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "updated_date", length = 10)
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Column(name = "rate", precision = 21, scale = 12)
	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	@Override
	public FinEntry clone() throws CloneNotSupportedException {
		return (FinEntry) super.clone();
	}
	
}