package com.cordialsr.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "key_skills")
//@JsonDeserialize(using = keySkills.class)
public class KeySkill {
	private Long id;
	private String description;
	
	private Set<Party> parties = new HashSet<>(0);
	
	public KeySkill() {
		
	}
	
	public KeySkill(Long id) {
		this.id = id;
	}
	
	public KeySkill(Long id, String description) {
		this.id = id;
		this.description = description;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	@Column(name = "id", unique = true)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "description", length = 45)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@JsonIgnore	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "party_has_skill", joinColumns = {
			@JoinColumn(name = "key_skill_id", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "party_id", nullable = false, updatable = false) })
	public Set<Party> getParty() {
		return this.parties;
	}

	public void setParty(Set<Party> parties) {
		this.parties = parties;
	}
	
	
	
	@Override
	public KeySkill clone() throws CloneNotSupportedException {
		return (KeySkill) super.clone();
	}
}
