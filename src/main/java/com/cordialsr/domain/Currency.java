package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "currency")
public class Currency implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String CUR_AZN = "AZN";
	public static final String CUR_KZT = "KZT";
	public static final String CUR_USD = "USD";
	public static final String CUR_KGS = "KGS";
	public static final String CUR_UZS = "UZS";
	public static final String CUR_EUR = "EUR";
	public static final String CUR_RUB = "RUB";
	public static final String CUR_TRY = "TRY";
	
	private String currency;
	private String name;
	
	public Currency() {
	}

	public Currency(String currency) {
		this.currency = currency;
	}

	public Currency(String currency, String name) {
		this.currency = currency;
		this.name = name;		
	}

	@Id
	@Column(name = "currency", unique = true, nullable = false, length = 3)
	public String getCurrency() {
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Column(name = "name", length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// ****************************************************************************
	
	@Override
	public String toString() {
		return "Currency [currency=" + currency + ", name=" + name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currency == null) ? 0 : currency.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Currency other = (Currency) obj;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public Currency clone() throws CloneNotSupportedException {
		return (Currency) super.clone();
	}
	
}
