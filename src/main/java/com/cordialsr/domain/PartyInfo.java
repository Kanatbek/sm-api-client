package com.cordialsr.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import static javax.persistence.GenerationType.IDENTITY;


@Entity
@Table(name = "party_info")
public class PartyInfo implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Party party;
	private String addressResidency;
	
	private Boolean conviction;
	
	private Boolean gender;
	private Integer children;
	
	private String advisorFirstname;
	private String advisorLastname;
	
	
	private PhoneNumber advisorPhone;
	
	private Date dateSign;
	private PcLevel pcLevel;
	private String driverLicense;
	
	
	public PartyInfo () {
		
	}
	
	public PartyInfo (Long id) {
		this.id = id;
	}
	
	public PartyInfo(Long id, Party party, String addressResidency, Boolean conviction, 
			String advisorFirstname, String advisorLastname, PhoneNumber advisorPhone, 
			Date dateSign, PcLevel pcLevel, String driverLicense, Boolean gender, Integer children) {
		this.id = id;
		this.party = party;
		this.addressResidency = addressResidency;
		this.conviction = conviction;
		this.advisorFirstname = advisorFirstname;
		this.advisorLastname = advisorLastname;
		this.advisorPhone = advisorPhone;
		this.dateSign = dateSign;
		this.pcLevel = pcLevel;
		this.driverLicense = driverLicense;
		this.gender = gender;
		this.children = children;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "party_id")
	public Party getParty() {
		return this.party;
	}

	public void setParty(Party party) {
		this.party = party;
	}

	@Column(name = "address_residency")
	public String getAddressResidency() {
		return addressResidency;
	}

	public void setAddressResidency(String addressResidency) {
		this.addressResidency = addressResidency;
	}

	@Column(name = "conviction")
	public Boolean getConviction() {
		return conviction;
	}

	public void setConviction(Boolean conviction) {
		this.conviction = conviction;
	}

	@Column(name="advisor_firstname")
	public String getAdvisorFirstname() {
		return advisorFirstname;
	}

	public void setAdvisorFirstname(String advisorFirstname) {
		this.advisorFirstname = advisorFirstname;
	}
	
	@Column(name="advisor_lastname")
	public String getAdvisorLastname() {
		return advisorLastname;
	}

	public void setAdvisorLastname(String advisorLastname) {
		this.advisorLastname = advisorLastname;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "advisor_phone")
	public PhoneNumber getAdvisorPhone() {
		return advisorPhone;
	}

	public void setAdvisorPhone(PhoneNumber advisorPhone) {
		this.advisorPhone = advisorPhone;
	}

	@Column(name = "date_sign")
	public Date getDateSign() {
		return dateSign;
	}

	public void setDateSign(Date dateSign) {
		this.dateSign = dateSign;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pc_level")
	public PcLevel getPcLevel() {
		return pcLevel;
	}

	public void setPcLevel(PcLevel pcLevel) {
		this.pcLevel = pcLevel;
	}
	
	
	@Column(name = "driver_license")
	public String getDriverLicense() {
		return driverLicense;
	}
	
	public void setDriverLicense(String driverLicense) {
		this.driverLicense = driverLicense;
	}
	
	@Column(name = "gender")
	public Boolean getGender() {
		return gender;
	}
	
	public void setGender(Boolean gender) {
		this.gender = gender;
	}
	
	@Column(name = "children")
	public Integer getChildren() {
		return this.children;
	}

	public void setChildren(Integer children) {
		this.children = children;
	}
	

}
