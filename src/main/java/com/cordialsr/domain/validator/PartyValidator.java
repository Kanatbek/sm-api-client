package com.cordialsr.domain.validator;

import java.util.Calendar;

import com.cordialsr.domain.Party;
import com.cordialsr.general.GeneralUtil;

public class PartyValidator {

	public static boolean validateBasic(Party pt) throws Exception {

		if (pt == null)
			throw new Exception("Party is null.");

		if (pt.getIsYur() == null)
			throw new Exception("Please select isYur.");

		Calendar cal = Calendar.getInstance();
		cal.set(2000, 0, 1, 0, 0, 0);
		cal.set(Calendar.MILLISECOND, 1);
		if (cal.after(pt.getDateExpire())) {
			if (pt.getIsYur() == true) {
				if (pt.getCompanyName() == null) {
					throw new Exception("Please fill company name");
				}
			}

			if (pt.getIsYur() == false) {

				if (pt.getFirstname() == null || GeneralUtil.isEmptyString(pt.getFirstname()))
					throw new Exception("Please select Firstname.");

				if (pt.getLastname() == null || GeneralUtil.isEmptyString(pt.getLastname()))
					throw new Exception("Please select Lastname.");

				if (pt.getBirthday() == null)
					throw new Exception("Please select getBirthday.");

				if (pt.getPassportNumber() == null || GeneralUtil.isEmptyString(pt.getPassportNumber())) {
					throw new Exception("Please fill passport number");
				}

				if (pt.getIssuedInstance() == null || GeneralUtil.isEmptyString(pt.getIssuedInstance())) {
					throw new Exception("Please fill issued instance");
				}
				if (pt.getDateIssue() == null) {
					throw new Exception("Please fill Date issue");
				}
			}

			if (pt.getIinBin() == null || GeneralUtil.isEmptyString(pt.getIinBin()))
				throw new Exception("Please select IinBin.");

			if (pt.getCompany() == null || GeneralUtil.isEmptyInteger(pt.getCompany().getId()))
				throw new Exception("Please select company.");

			if (pt.getPartyType() == null || GeneralUtil.isEmptyString(pt.getPartyType().getName()))
				throw new Exception("Please select PartyType.");
		}

		return true;
	}
}
