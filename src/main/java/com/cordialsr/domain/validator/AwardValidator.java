package com.cordialsr.domain.validator;

import com.cordialsr.domain.Award;
import com.cordialsr.domain.AwardCase;
import com.cordialsr.general.GeneralUtil;


public class AwardValidator {

	public static boolean validateBasic(Award fd) throws Exception {

		if (fd == null)
			throw new Exception("Award is null.");

		
		if (fd.getAwardCase() == null || GeneralUtil.isEmptyString(fd.getAwardCase().getCode()))
			throw new Exception("Please select AwardCase.");
		
		if (fd.getAwardType() == null || GeneralUtil.isEmptyString(fd.getAwardType().getName()))
			throw new Exception("Please select AwardType.");
			
		if (fd.getCompany() == null || GeneralUtil.isEmptyInteger(fd.getCompany().getId()))
			throw new Exception("Please select Company.");
		
		if (fd.getAwardCase().getCode().equals(AwardCase.CASE_UNIT) 
				&& (fd.getCurrency() == null || GeneralUtil.isEmptyString(fd.getCurrency().getCurrency())))
			throw new Exception("Please select Currency.");
		
		if (fd.getInvMainCategory() == null || GeneralUtil.isEmptyInteger(fd.getInvMainCategory().getId()))
			throw new Exception("Please select InvMainCategory.");
		
		if (fd.getInvSubCategory() == null || GeneralUtil.isEmptyInteger(fd.getInvSubCategory().getId()))
			throw new Exception("Please select InvSubCategory.");
		
//		if (fd.getInventory() == null || GeneralUtil.isEmptyInteger(fd.getInventory().getId()))
//			throw new Exception("Please select Inventory.");
		
		if (fd.getPosition() == null || GeneralUtil.isEmptyInteger(fd.getPosition().getId()))
			throw new Exception("Please select Position.");
		
		if (fd.getScope() == null)
			throw new Exception("Please select Scope.");
		

		if (fd.getDateStart() == null)
			throw new Exception("Please select DateStart.");
		
		if (fd.getMin() == null )
			throw new Exception("Please select Min.");
		
		if (fd.getMax() == null)
			throw new Exception("Please select Max.");
		
		
		if (fd.getAwardCase().getCode().equals(AwardCase.CASE_UNIT) && fd.getPremiDiv() == null)
			throw new Exception("Please select PremiDiv.");
		
		if (fd.getUserName() == null)
			throw new Exception("Please log in.");
		
		
		return true;
	}
}
