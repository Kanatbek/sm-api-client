package com.cordialsr.domain.validator;

import java.math.BigDecimal;

import com.cordialsr.domain.Payroll;
import com.cordialsr.general.GeneralUtil;
import com.ibm.icu.util.Calendar;

public class PayrollValidator {
	
	private static final Integer PAYROLL_OPEN_DAYS_RANGE = 0;

	public static boolean validateBasic(Payroll p) throws Exception {

		if (p == null)
			throw new Exception("Payroll is null.");

		if (p.getCompany() == null || GeneralUtil.isEmptyInteger(p.getCompany().getId()))
			throw new Exception("Please select Company.");
		
		if (p.getBranch() == null || GeneralUtil.isEmptyInteger(p.getBranch().getId()))
			throw new Exception("Please select Branch.");
		
		if (p.getParty() == null || GeneralUtil.isEmptyLong(p.getParty().getId()))
			throw new Exception("Party is empty.");
		
//		if (p.getEmployee() == null || GeneralUtil.isEmptyLong(p.getEmployee().getId()))
//			throw new Exception("Employee is empty.");
		
		if (GeneralUtil.isEmptyString(p.getKind()))
			throw new Exception("Please specify the kind of payroll.");
		
		if (GeneralUtil.isEmptyInteger(p.getYear()))
			throw new Exception("Please specify Year.");
		
		if (GeneralUtil.isEmptyInteger(p.getYear()))
			throw new Exception("Please specify Month.");
		
		// Date
		
		Calendar cds = Calendar.getInstance();
		cds.set(p.getYear(), (p.getMonth() - 1), 1);
		cds.setTime(GeneralUtil.startOfMonth(cds.getTime()));
		
		Calendar cde = Calendar.getInstance();
		cde.setTime(GeneralUtil.endOfMonth(cds.getTime()));
		cde.add(Calendar.DATE, PAYROLL_OPEN_DAYS_RANGE);
		
		if (p.getPostedDate() != null) {
			Calendar cdp = Calendar.getInstance();
			cdp.setTime(p.getPostedDate());
			
			if (cdp.before(cde) 
					&& cde.get(Calendar.MONTH) == cdp.get(Calendar.MONTH)
					&& cde.get(Calendar.YEAR) == cdp.get(Calendar.YEAR)
					&& !p.getKind().equals(Payroll.KIND_PREMI)
					&& !(p.getSum().compareTo(BigDecimal.ZERO) < 0)) 
				throw new Exception("This month didn't complete yet.");
		}
		
		if (cds.after(p.getPayrollDate()) && p.getKind().equals(Payroll.KIND_SALARY)) 
			throw new Exception("Posting for the future period is restricted.");
		
		// Summ
		
		if (p.getSum() == null || ((p.getStorno() == null || !p.getStorno()) && GeneralUtil.isEmptyOrNegativeSumm(p.getSum())))
			throw new Exception("Sum is empty or negative.");
		
		if (p.getCurrency() == null || GeneralUtil.isEmptyString(p.getCurrency().getCurrency()))
			throw new Exception("Please specify Currency.");
		
		if (GeneralUtil.isEmptyString(p.getTrCode()))
			throw new Exception("Please specify Transaction Code.");
		
		if (p.getUser() == null || GeneralUtil.isEmptyLong(p.getUser().getUserid()))
			throw new Exception("User is empty.");
		
		return true;
	}
	
}
