package com.cordialsr.domain.validator;

import java.util.Date;

import com.cordialsr.domain.SmService;
import com.cordialsr.domain.SmServiceItem;
import com.cordialsr.general.GeneralUtil;

public class ServiceValidator {
	
	public static final int REVERSE_DAYS_LIMIT = 300;
	
	public static boolean validateNewService(SmService service) throws Exception {
		
		if (service == null)
			throw new Exception("Service is empty");
		if (service.getBranch() == null)
			throw new Exception("Branch is empty");
		if (service.getContract() == null || GeneralUtil.isEmptyLong(service.getContract().getId()))
			throw new Exception("Contract is empty");
		if (service.getCareman() == null || GeneralUtil.isEmptyLong(service.getCareman().getId()))
			throw new Exception("Careman is empty");
		if (service.getCustomer() == null || GeneralUtil.isEmptyLong(service.getCustomer().getId()))
			throw new Exception("Customer is empty");
		if (service.getInventory() == null || GeneralUtil.isEmptyInteger(service.getInventory().getId()))
			throw new Exception("Inventory is empty");
		if (service.getUserAdded() == null || GeneralUtil.isEmptyLong(service.getUserAdded().getUserid()))
			throw new Exception("User is empty");
		if (service.getCaremanFio() == null || GeneralUtil.isEmptyString(service.getCaremanFio()))
			throw new Exception("Careman FIO is empty");
		if (service.getType() == null || GeneralUtil.isEmptyString(service.getType()))
			throw new Exception("Service type is empty");
		if (service.getSdate() == null)
			throw new Exception("Date is empty");
		long ageInDays = GeneralUtil.calcAgeInDays(service.getSdate(), new Date());
		if (ageInDays > REVERSE_DAYS_LIMIT)
			throw new Exception("Дата просрочена");
		if (ageInDays < 0)
			throw new Exception("Указана ранняя дата");
		if ((service.getType().equals(service.SERVICE_TYPE_ZAMF) || service.getType().equals(service.SERVICE_TYPE_REMT)) && service.getSmServiceItems().size() == 0)
			throw new Exception("Service Item's are empty");
		
		if (service.getType().equals(service.SERVICE_TYPE_ZAMF) || service.getType().equals(service.SERVICE_TYPE_REMT)) {
			
			for (SmServiceItem si: service.getSmServiceItems()) {
				
				if (service.getType().equals(service.SERVICE_TYPE_ZAMF) && si.getFno() == null) 
					throw new Exception("Filter number is empty");
				if (si.getQuantity() == null || GeneralUtil.isEmptyBigDecimal(si.getQuantity()))
					throw new Exception("Quantity is empty");
//				if (si.getInventory() == null || GeneralUtil.isEmptyInteger(si.getInventory().getId()))
//					throw new Exception("Inventory is empty");
//				if (si.getUnit() == null || GeneralUtil.isEmptyString(si.getUnit().getName()))
//					throw new Exception("Unit is empty");
				if (si.getIsTovar() == null)
					throw new Exception("Sm Service Item is what?! IsTovar bool is empty"); 
			}
			
		}
		
		return true;
	}
		
}
