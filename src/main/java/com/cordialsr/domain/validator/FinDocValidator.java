package com.cordialsr.domain.validator;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.FinDocType;
import com.cordialsr.domain.FinEntry;
import com.cordialsr.general.GeneralUtil;


public class FinDocValidator {

	public static boolean validateBasic(FinDoc fd) throws Exception {

		if (fd == null)
			throw new Exception("FinDoc is null.");

		if (fd.getCompany() == null || GeneralUtil.isEmptyInteger(fd.getCompany().getId()))
			throw new Exception("Please select Company.");

		if (fd.getBranch() == null || GeneralUtil.isEmptyInteger(fd.getBranch().getId()))
			throw new Exception("Please select Branch.");

		if (fd.getWcurrency() == null || GeneralUtil.isEmptyString(fd.getWcurrency().getCurrency()))
			throw new Exception("Please specify Local Currency.");

		if (fd.getDcurrency() == null || GeneralUtil.isEmptyString(fd.getDcurrency().getCurrency()))
			throw new Exception("Please specify Main Currency.");

		if (fd.getDocDate() == null)
			throw new Exception("Please specify Date.");

//		if (fd.getBusarea() == null || GeneralUtil.isEmptyInteger(fd.getBusarea().getId()))
//			throw new Exception("Please specify Business Area.");
		
		if (fd.getDepartment() == null || GeneralUtil.isEmptyInteger(fd.getDepartment().getId()))
			throw new Exception("Please select Department.");

		if (fd.getFinDocType() == null || GeneralUtil.isEmptyString(fd.getFinDocType().getCode()))
			throw new Exception("Please specify FinDocType.");

		if (fd.getMonth() == null || fd.getMonth() < 1 || fd.getMonth() > 12)
			throw new Exception("Please specify correct Month.");

		if (GeneralUtil.isEmptyString(fd.getYear()) || fd.getYear().length() < 4
				|| Integer.valueOf(fd.getYear()) < 2015)
			throw new Exception("Please specify correct Year.");

		if (GeneralUtil.isEmptyString(fd.getTrcode()))
			throw new Exception("Please specify Transaction Code.");

		if (fd.getUser() == null || GeneralUtil.isEmptyLong(fd.getUser().getUserid()))
			throw new Exception("Please specify User.");

		if (GeneralUtil.isEmptyString(fd.getTitle()))
			throw new Exception("Please specify Title.");

		if (fd.getParty() == null || GeneralUtil.isEmptyLong(fd.getParty().getId()))
			throw new Exception("Please select Party for FinDoc.");

		if (fd.getDsumm() == null)
			throw new Exception(fd.getDcurrency().getCurrency() + " Sum is undefined.");

		if (fd.getDsummPaid() == null)
			throw new Exception(fd.getDcurrency().getCurrency() + " Paid sum is undefined.");

		if (fd.getDsumm().compareTo(BigDecimal.ZERO) > 0 && fd.getDsummPaid().compareTo(fd.getDsumm()) > 0)
			throw new Exception(fd.getDcurrency().getCurrency() + " Paid summ is greater than initial Sum.");
		
		if (fd.getWsumm() == null)
			throw new Exception(fd.getWcurrency().getCurrency() + " Sum is undefined.");

		if (fd.getWsummPaid() == null)
			throw new Exception(fd.getWcurrency().getCurrency() + " Paid sum is undefined.");

		if (fd.getWsumm().compareTo(BigDecimal.ZERO) > 0 && fd.getWsummPaid().compareTo(fd.getWsumm()) > 0)
			throw new Exception(fd.getWcurrency().getCurrency() + " Paid summ is greater than initial Sum.");
		
		if (fd.getRate() == null || GeneralUtil.isEmptyOrNegativeSumm(fd.getRate())
				|| fd.getRate().compareTo(new BigDecimal(0)) == 0)
			throw new Exception("Rate is empty or negative or zero.");

		if (fd.getFinEntries() == null || 
				fd.getFinEntries().size() < 2 || fd.getFinEntries().size() % 2 != 0)
			throw new Exception("Wrong count of FinEntries. Must be greater than 1 and be factor of 2.");

		return true;
	}

	public static boolean validateSummAndFinEntries(FinDoc fd) throws Exception {
		BigDecimal bd = fd.getDsumm().multiply(fd.getRate());
		bd = bd.setScale(2, RoundingMode.HALF_EVEN);
		BigDecimal ws = fd.getWsumm().setScale(2, RoundingMode.HALF_EVEN);
		// System.out.println(bd);
		if (bd.compareTo(ws) != 0) {
			throw new Exception("Main currensy sum doesn't satisfy the sum of Local currency. Please check Rate or correct sums!");
		}
		BigDecimal feWsummDe = new BigDecimal(0);
		BigDecimal feDsummDe = new BigDecimal(0);
		BigDecimal feWsummCr = new BigDecimal(0);
		BigDecimal feDsummCr = new BigDecimal(0);
		for (FinEntry fe : fd.getFinEntries()) {
			try {
				if (fe.getDc().equalsIgnoreCase(FinEntry.DC_DEBET)) {
					feWsummDe = feWsummDe.add(fe.getWsumm());
					feDsummDe = feDsummDe.add(fe.getDsumm());
				} else if (fe.getDc().equalsIgnoreCase(FinEntry.DC_CREDIT)) {
					feWsummCr = feWsummCr.add(fe.getWsumm());
					feDsummCr = feDsummCr.add(fe.getDsumm());
				}
				
				if (!fe.getWcurrency().getCurrency().equals(fd.getWcurrency().getCurrency()) && !fd.getFinDocType().getCode().equalsIgnoreCase(FinDocType.TYPE_EX_EXCHANGE_CURRENCY))
					throw new Exception("FinEntry " + fe.getWcurrency().getCurrency() + " doesn't satisfy the currency of FinDoc.");
				if (!fe.getDcurrency().getCurrency().equals(fd.getDcurrency().getCurrency()))
					throw new Exception("FinEntry " + fe.getDcurrency().getCurrency() + " doesn't satisfy the currency of FinDoc.");
				if (fe.getEntrycount() < 2 || fe.getEntrycount() != fd.getFinEntries().size())
					throw new Exception("Wrong EntryCount value on FinEntry.");
				
				if (fd.getFinDocType().getCode().equalsIgnoreCase(FinDocType.TYPE_EX_EXCHANGE_CURRENCY)) {
//					if (fe.getDc().equalsIgnoreCase(FinEntry.DC_CREDIT)) {
//						if (fe.getRate().compareTo(fd.getRate()) != 0)
//							throw new Exception("FinEntry Rate " + fe.getRate().doubleValue() 
//									+ " doesn't satisfy the Rate " + fd.getRate().doubleValue() + " of FinDoc.");
//					}	
				} else {
					if (fe.getRate().compareTo(fd.getRate()) != 0)
						throw new Exception("FinEntry Rate " + fe.getRate().doubleValue() 
								+ " doesn't satisfy the Rate " + fd.getRate().doubleValue() + " of FinDoc.");
				}
				
				validateFinEntry(fe);
			} catch (Exception ex) {
				throw ex;
			}
		}
		if ((feWsummDe.compareTo(feWsummCr) != 0) 
				&& (!fd.getFinDocType().getCode().equalsIgnoreCase(FinDocType.TYPE_EX_EXCHANGE_CURRENCY)))
			throw new Exception("FinEntry Debet WSum " + feWsummDe + " doesn't satisfy the Credit WSumm " + feWsummCr);
		if (feDsummDe.compareTo(feDsummCr) != 0)
			throw new Exception("FinEntry Debet DSum " + feDsummDe + " doesn't satisfy the Credit DSumm " + feDsummCr);
		//BigDecimal dsFe = fe.getWsumm().divide(fe.getRate(), 12, RoundingMode.HALF_EVEN);
		if ((feWsummDe.setScale(2, RoundingMode.HALF_EVEN).compareTo(fd.getWsumm().setScale(2, RoundingMode.HALF_EVEN)) != 0)
				&& (!fd.getFinDocType().getCode().equalsIgnoreCase(FinDocType.TYPE_EX_EXCHANGE_CURRENCY))
				&& (!fd.getFinDocType().getCode().equals(FinDocType.TYPE_GR_GENERAL_RENT_CONTRACT)))
			throw new Exception("FinEntry WSum " + feWsummDe + " doesn't satisfy the WSum " + fd.getWsumm() + " of FinDoc.");
		feDsummDe = feDsummDe.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		BigDecimal fdDs = fd.getDsumm().setScale(2, BigDecimal.ROUND_HALF_EVEN);
		if ((feDsummDe.compareTo(fdDs) != 0 && (!fd.getFinDocType().getCode().equals(FinDocType.TYPE_GR_GENERAL_RENT_CONTRACT)))
				&& (!fd.getFinDocType().getCode().equalsIgnoreCase(FinDocType.TYPE_EX_EXCHANGE_CURRENCY)))
			throw new Exception("FinEntry DSum " + feDsummDe + " doesn't satisfy the DSum " + fdDs + " of FinDoc.");
		
		return true;
	}
	
	public static boolean validateFinEntry(FinEntry fe) throws Exception {

		if (fe == null)
			throw new Exception("FinEntry is null.");

		if (fe.getCompany() == null || GeneralUtil.isEmptyInteger(fe.getCompany().getId()))
			throw new Exception("Please select Company for FinEntry.");

		if (fe.getBranch() == null || GeneralUtil.isEmptyInteger(fe.getBranch().getId()))
			throw new Exception("Please select Branch for FinEntry.");

		if (fe.getWcurrency() == null || GeneralUtil.isEmptyString(fe.getWcurrency().getCurrency()))
			throw new Exception("Please specify Local Currency for FinEntry.");

		if (fe.getDcurrency() == null || GeneralUtil.isEmptyString(fe.getDcurrency().getCurrency()))
			throw new Exception("Please specify Main Currency for FinEntry.");

		if (fe.getDdate() == null)
			throw new Exception("Please specify Date for FinEntry.");

		if (fe.getDsumm() == null)
			throw new Exception("Dsumm value is undefined for FinEntry.");

		if (fe.getWsumm() == null)
			throw new Exception("Wsumm value is undefined for FinEntry.");

		if (fe.getDepartment() == null || GeneralUtil.isEmptyInteger(fe.getDepartment().getId()))
			throw new Exception("Please select Department for FinEntry.");

		if (fe.getMonth() == null || fe.getMonth() < 1 || fe.getMonth() > 12)
			throw new Exception("Please specify correct Month for FinEntry.");

		if (GeneralUtil.isEmptyString(fe.getYear()) || fe.getYear().length() < 4
				|| Integer.valueOf(fe.getYear()) < 2015)
			throw new Exception("Please specify correct Year for FinEntry.");

		if (fe.getGlAccount() == null || GeneralUtil.isEmptyString(fe.getGlAccount().getCode()))
			throw new Exception("Please specify GlAccount for FinEntry.");
		
		if (fe.getDc() == null || GeneralUtil.isEmptyString(fe.getDc()))
			throw new Exception("Please specify D/C indicator for FinEntry.");

		if (GeneralUtil.isEmptyInteger(fe.getEntrycount()))
			throw new Exception("Please specify EntryCount for FinEntry.");

		if (fe.getFinDoc() == null)
			throw new Exception("Please specify FinDoc for FinEntry.");

		if (fe.getParty() == null || GeneralUtil.isEmptyLong(fe.getParty().getId()))
			throw new Exception("Please select Party for FinEntry.");

		if (fe.getIsTovar()) {
			if (fe.getInventory() == null || GeneralUtil.isEmptyInteger(fe.getInventory().getId()))
				throw new Exception("Please specify Inventory for FinEntry, since it is set as 'Material'.");
			if (GeneralUtil.isEmptyString(fe.getItemName()))
				throw new Exception("Please specify ItemName for Material of FinEntry.");
			if (fe.getQuantity() == null || fe.getQuantity().compareTo(new BigDecimal(0)) <= 0)
				throw new Exception("Wrong amount of quantity: " + fe.getQuantity() + " for FinEntry.");
			if (fe.getUnit() == null || GeneralUtil.isEmptyString(fe.getUnit().getName()))
				throw new Exception("Please specify Unit for FinEntry, since it is set as 'Material'.");
			if (GeneralUtil.isEmptyOrNegativeSumm(fe.getPrice()))
				throw new Exception("Please specify Price for FinEntry, since it is set as 'Material'.");
			if (fe.getInvoiceItem() == null || GeneralUtil.isEmptyLong(fe.getInvoiceItem().getId())) {
				throw new Exception("InvoiceItem is null.");
			}
		}

		if (GeneralUtil.isEmptyString(fe.getPostkey()))
			throw new Exception("Please specify Posting Key for FinEntry.");

		if (fe.getGlAccount().getCode().startsWith("101") || fe.getGlAccount().getCode().startsWith("103")){
			if (fe.getKassaBank() == null || GeneralUtil.isEmptyInteger(fe.getKassaBank().getId()))
				throw new Exception("Please select Kassa or Bank Account for FinEntry.");
			if (fe.getFinActType() == null || GeneralUtil.isEmptyString(fe.getFinActType().getCode()))
				throw new Exception("Please specify FinOperType for FinEntry.");
			if (fe.getCashflowStatement() == null || GeneralUtil.isEmptyInteger(fe.getCashflowStatement().getId()))
				throw new Exception("Please specify CashflowStatement for FinEntry.");
		}
		return true;
	}

}
