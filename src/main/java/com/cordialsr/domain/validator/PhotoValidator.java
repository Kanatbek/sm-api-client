package com.cordialsr.domain.validator;

import com.cordialsr.domain.Photo;
import com.cordialsr.general.GeneralUtil;


public class PhotoValidator {

	public static boolean validateBasic(Photo fd) throws Exception {

		if (fd == null)
			throw new Exception("Photo is null.");

		if (fd.getId() == null || GeneralUtil.isEmptyLong(fd.getId()))
			throw new Exception("Please select Id.");

		if (fd.getPhoto() == null )
			throw new Exception("Please select Photo.");

		
		return true;
	}
}
