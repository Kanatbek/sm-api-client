package com.cordialsr.domain.validator;

import com.cordialsr.domain.SmCall;
import com.cordialsr.domain.SmEnquiry;
import com.cordialsr.general.GeneralUtil;

public class SmCallValidator {

	public static boolean validateNewSmCall(SmCall smCall) throws Exception {

		if (smCall.getCallDate() == null)
			throw new Exception("Date is empty");
		if (smCall.getCallTime() == null)
			throw new Exception("Time is empty");
		if (smCall.getService() == null || GeneralUtil.isEmptyLong(smCall.getService().getId()))
			throw new Exception("Service is empty");
		if ((smCall.getStatus() == SmCall.STATUS_ANSWER 
				|| smCall.getStatus() == SmCall.STATUS_MISSED)
				&& (smCall.getInfo() == null || GeneralUtil.isEmptyString(smCall.getInfo())))
			throw new Exception("Info is empty");

		return true;
	}

	public static void validateNewOutgoingCall(SmEnquiry enq)throws Exception {
		
		if (enq.getCompany() == null || GeneralUtil.isEmptyInteger(enq.getCompany().getId()))
			throw new Exception("Company is empty");
		if (enq.getBranch() == null || GeneralUtil.isEmptyInteger(enq.getBranch().getId()))
			throw new Exception("Branch is empty");
		if (enq.getEdate() == null)
			throw new Exception("Date is empty");
		if (enq.getEtime() == null)
			throw new Exception("Time is empty");
		if (enq.getOperator() == null || GeneralUtil.isEmptyLong(enq.getOperator().getUserid()))
			throw new Exception("Operator is empty");
		
		return;
	}

}
