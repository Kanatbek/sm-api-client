package com.cordialsr.domain.validator;

import com.cordialsr.domain.KassaBank;
import com.cordialsr.general.GeneralUtil;

public class KassaBankValidator {

	public static boolean validateBasic(KassaBank kb) throws Exception {

		if (kb == null)
			throw new Exception("Объект пустой.");

		if (kb.getCompany() == null || GeneralUtil.isEmptyInteger(kb.getCompany().getId()))
			throw new Exception("Укажите организацию.");
		
		if (kb.getIsBank() == null)
			throw new Exception("Укажите вид (Касса или Банковский счет).");
		
		if (kb.getCurrency() == null || GeneralUtil.isEmptyString(kb.getCurrency().getCurrency()))
			throw new Exception("Укажите валюту.");
		
		if (!kb.getIsBank() && (kb.getBranch() == null || GeneralUtil.isEmptyInteger(kb.getBranch().getId())))
			throw new Exception("Укажите филиал.");

		if (kb.getName() == null || GeneralUtil.isEmptyString(kb.getName())) 
			throw new Exception("Укажите название.");
		
		if (kb.getIsBank()) {
			if (kb.getAccountNumber() == null || GeneralUtil.isEmptyString(kb.getAccountNumber()))
				throw new Exception("Укажите номер счета.");
			
			if (kb.getBank() == null || GeneralUtil.isEmptyLong(kb.getBank().getId())) 
				throw new Exception("Укажите Банк.");
		}
				
		return true;
	}
	
}
