package com.cordialsr.domain.validator;

import java.util.Date;

import com.cordialsr.domain.Contract;
import com.cordialsr.domain.SmFilterChange;

public class SmFilterChangeValidator {
	
	public static boolean validateNewContract(Contract con) throws Exception {
		
		if (con == null)
			throw new Exception("Contract is empty");
		if (con.getContractItems() == null || con.getContractItems().size() == 0)
			throw new Exception("Contract Items is empty");
		if (con.getContractItems().get(0) == null)
			throw new Exception("First item is empty");
		if (con.getContractItems().get(0).getInventory() == null)
			throw new Exception("Inventory is empty");
		if (con.getContractNumber() == null || con.getContractNumber().length() == 0)
			throw new Exception("Contract Number is empty");
		return true;
	}
	
	public static boolean validateUpdate(SmFilterChange fc) throws Exception {
		
		if (fc.getF1Mt() == null)
			throw new Exception("F1 Mt is empty");

		if (fc.getPrMt() == null)
			throw new Exception("Pr Mt is empty");

		if (fc.getF2Mt() == null)
			throw new Exception("F2 Mt is empty");

		if (fc.getF3Mt() == null)
			throw new Exception("F3 Mt is empty");

		if (fc.getF4Mt() == null)
			throw new Exception("F4 Mt is empty");

		if (fc.getF5Mt() == null)
			throw new Exception("F5 Mt is empty");
		
		return true;
	}

	public static boolean validateDates(Date sdate, Date lastDate) throws Exception {
		return sdate.after(lastDate); 
	}

}
