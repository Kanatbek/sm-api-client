package com.cordialsr.domain.validator;

import java.math.BigDecimal;

import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.OrderHeader;
import com.cordialsr.domain.OrderItem;
import com.cordialsr.general.GeneralUtil;

public class OrderValidator {
	public static boolean validateBasic(OrderHeader oh) throws Exception {
		
		if (oh.getBranch() == null || GeneralUtil.isEmptyInteger(oh.getBranch().getId())) {
			throw new Exception("Please select Branch.");
		}
		if (oh.getCompany() == null || GeneralUtil.isEmptyInteger(oh.getCompany().getId())) {
			throw new Exception("Please select Company.");
		}
		if (oh.getUserAdded() == null || GeneralUtil.isEmptyLong(oh.getUserAdded().getUserid())) {
			throw new Exception("Please select User.");
		}
		if (oh.getData() == null) {
			throw new Exception("Please specify Date.");
		}
		if (oh.getScopeType() == null) {
			throw new Exception("Please specify scope type.");
		}
		if (oh.getOrderStatus() == null) {
			throw new Exception("Please specify status");
		}
		
		if (oh.getOrderItems() == null || oh.getOrderItems().size() == 0) {
			throw new Exception ("Please add Items");
		}
		
		for (OrderItem oi: oh.getOrderItems()) {
			
			if (oi.getInventory() == null || GeneralUtil.isEmptyInteger(oi.getInventory().getId())) {
				throw new Exception ("Please select item into OrderItems");
			}
			
			if (oi.getQuantity() == null || oi.getQuantity().equals(new BigDecimal(0))) {
				throw new Exception ("Please write right quantity");
			}
			if (oi.getUnit() == null) {
				throw new Exception("Unit was not choosen");
			}
		}
		
		return true;
	}
	
	public static boolean validateForDeleteOrder(Invoice invoice) throws Exception {
		
		if (invoice.getBranchImporter() == null || GeneralUtil.isEmptyInteger(invoice.getBranchImporter().getId())) {
			throw new Exception("Please select Branch.");
		}
		if (invoice.getCompany() == null || GeneralUtil.isEmptyInteger(invoice.getCompany().getId())) {
			throw new Exception("Please select Company.");
		}
		return true;
	}
}
