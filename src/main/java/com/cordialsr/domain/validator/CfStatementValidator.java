package com.cordialsr.domain.validator;

import com.cordialsr.domain.FinCashflowStatement;
import com.cordialsr.general.GeneralUtil;

public class CfStatementValidator {
	
	public static boolean validate(FinCashflowStatement cf) throws Exception {
		
		if (cf == null)
			throw new Exception("CashFlow Statement is null.");
			
		if (cf.getFinActType() == null || GeneralUtil.isEmptyString(cf.getFinActType().getCode()))
			throw new Exception("CashFlow Statement is empty.");
		
		if (GeneralUtil.isEmptyString(cf.getName()))
			throw new Exception("CashFlow Statement Name is empty.");
		
//		if (GeneralUtil.isEmptyString(cf.getInfo()))
//			throw new Exception("CashFlow Statement Info is empty.");
		
		return true;
	}
}
