package com.cordialsr.domain.validator;

import com.cordialsr.domain.PlanFact;
import com.cordialsr.general.GeneralUtil;

public class PlanFactValidator {
	public static boolean validateBasic(PlanFact pf) throws Exception {

		if (pf == null) {
			throw new Exception("План пустой"); 
		}
		
		if (pf.getYear() == null) {
			throw new Exception(" Укажите год");
		}
		if (pf.getYear() == null) {
			throw new Exception(" Укажите год");
		}
		if (pf.getMonth() == null) {
			throw new Exception(" Укажите месяц");
		}
		if (pf.getCompany() == null || GeneralUtil.isEmptyInteger(pf.getCompany().getId())) {
			throw new Exception(" Укажите организацию");
		}
		
		if (pf.getPlan() == null) {
			throw new Exception("Укажите план");
		}
		
		if (pf.getFact() == null) {
			throw new Exception("Укажите остаток");
		}
		
		if (pf.getPosition() == null || GeneralUtil.isEmptyInteger(pf.getPosition().getId())) {
			throw new Exception(" Укажите должность");
		}
		
		
		if (pf.getPosition().getName() == "Coordinator") {

			if (pf.getDepartment() == null || GeneralUtil.isEmptyInteger(pf.getDepartment().getId())) {
				throw new Exception(" Укажите департаментa");
			}

			if (pf.getRegion() == null || GeneralUtil.isEmptyInteger(pf.getRegion().getId())) {
				throw new Exception(" Укажите регион");
			}

			if (pf.getBranch() == null || GeneralUtil.isEmptyInteger(pf.getBranch().getId())) {
				throw new Exception(" Укажите филиал");
			}

			if (pf.getCurrency() == null || GeneralUtil.isEmptyString(pf.getCurrency().getCurrency())) {
				throw new Exception("Укажите валюту");
			}	
		}
		
		
		return true;
	}
}
