package com.cordialsr.domain.validator;
import java.util.Date;

import com.cordialsr.domain.Contract;
import com.cordialsr.general.GeneralUtil;


public class ContractValidator {
	
	public static final long DAYS_PERMIT_BACK = 180;

	public static boolean validateBasic(Contract con) throws Exception {

		if (con == null)
			throw new Exception("Контракт пустой.");

		if (con.getCompany() == null || GeneralUtil.isEmptyInteger(con.getCompany().getId()))
			throw new Exception("Укажите организацию.");
		
		long ageInDays = GeneralUtil.calcAgeInDays(con.getDateSigned(), new Date());
				
//		if (ageInDays > DAYS_PERMIT_BACK)
//			throw new Exception("Допустимый срок ввода " + DAYS_PERMIT_BACK + " дней с даты заключния договора.");
		
		if (ageInDays < 0)
			throw new Exception("Неправильная дата.");
		
		if (con.getContractType() == null || GeneralUtil.isEmptyInteger(con.getContractType().getId()))
			throw new Exception("Укажите вид договора.");
		
		if (con.getBranch() == null || GeneralUtil.isEmptyInteger(con.getBranch().getId()))
			throw new Exception("Укажите филиал.");

		if (con.getServiceBranch() == null || GeneralUtil.isEmptyInteger(con.getServiceBranch().getId()))
			throw new Exception("Укажите сервис-филиал.");
					
		if (con.getDirector() == null || GeneralUtil.isEmptyLong(con.getDirector().getId()))
			throw new Exception("Укажите директора.");

		if (con.getCareman() == null || GeneralUtil.isEmptyLong(con.getCareman().getId()))
			throw new Exception("Укажите careman.");
		
		if (con.getCaremanSales() != null && con.getCaremanSales() == false) {
			if (con.getDealer() == null || GeneralUtil.isEmptyLong(con.getDealer().getId()))
				throw new Exception("Укажите дилера.");
		}
		
		if (con.getFitter() == null || GeneralUtil.isEmptyLong(con.getFitter().getId()))
			throw new Exception("Укажите установщика.");

		if (con.getCustomer() == null || GeneralUtil.isEmptyLong(con.getCustomer().getId()))
			throw new Exception("Укажите клиента.");
		
		if (con.getExploiter() == null || GeneralUtil.isEmptyLong(con.getExploiter().getId()))
			throw new Exception("Укажите эксплуататора.");
		
		if (con.getDateSigned() == null )
			throw new Exception("Укажите дату контракта.");
		
//		if (con.getDateSigned() != null) {
//			int d = GeneralUtil.calcAgeInMonth(d1, d2)
//		}

		if (con.getMonth() == null)
			throw new Exception("Неправильный месяц.");
		
		if (con.getDiscount() == null)
			throw new Exception("Скидка пуста.");

		if (con.getSumm() == null)
			throw new Exception("Сумма пуста.");
		
		if (con.getCost() == null)
			throw new Exception("Цена пуста");
	
		if (con.getCurrency() == null || GeneralUtil.isEmptyString(con.getCurrency().getCurrency()))
			throw new Exception("Укажите валюту.");
		
		if (con.getRate() == null)
			throw new Exception("Укажите курс валюты.");
		
		if (con.getFromDealerSumm() == null)
			throw new Exception("Укажите скидку от дилера.");
		
		if (con.getForbuh() == null)
			throw new Exception("Укажите forbuh.");
		
		if (con.getContractItems() == null)
			throw new Exception("Укажите товар контракта.");
		
		return true;
	}
	
	public static boolean validatePriceAndPaymentSchedule(Contract con) throws Exception {
		if (con.getPaymentSchedules() == null)
			throw new Exception("Укажите график оплаты");
		
		if (con.getPaymentSchedules() != null) {
			int totalSumm = 0; 
			int totalPaid = 0;    
			int paid = 0;
			int summ = 0;
			int discount = 0;
			int cost = 0;
			if (con.getPaid() != null) {
				paid = con.getPaid().intValue();	
			}
			if (con.getSumm() != null) {
				summ = con.getSumm().intValue();
			}
			
			if (con.getDiscount() != null) {
				discount = con.getDiscount().intValue();
			}
			if (con.getCost() != null) {
				cost = con.getCost().intValue();
			}
			
			int m = -1;			
			
			if (con.getMonth() != con.getPaymentSchedules().size() - 1)
					throw new Exception("Количество месяцев неправильно");
			
			for (int i = 0; i<=(con.getPaymentSchedules().size())-1; i++) {					
				if (con.getPaymentSchedules().get(i).getPaymentDate().compareTo(con.getDateSigned()) < 0) 
						throw new Exception("Дата оплаты не может быть раньше даты подписания договора");
				
				if (i > 0) {
					if (con.getPaymentSchedules().get(i-1).getPaymentDate()
									.compareTo(con.getPaymentSchedules().get(i).getPaymentDate())  > 0 ) {
						throw new Exception("Дата оплаты неправильно");
					}
				}
				
				totalSumm += con.getPaymentSchedules().get(i).getSumm().intValue();
				totalPaid += con.getPaymentSchedules().get(i).getPaid().intValue();
				if (con.getPaymentSchedules().get(i).getPaymentDate().getMonth() == m) {
					throw new Exception("Дата оплаты неправильно!");
				}
				m = con.getPaymentSchedules().get(i).getPaymentDate().getMonth();

			}
			if (paid != totalPaid ){
				throw new Exception("Оплаченная сумма оплаты неправильно");
			}
			if (cost != totalSumm){
				throw new Exception("Сумма неправильно");
			}
		
			if (summ != (cost - discount)) 
				throw new Exception("Сумма не равна к (Цена - скидка)");
			
			if (summ < 0 || summ < paid || paid < 0) 
				throw new Exception("Сумма неправильно");
			
		}
		
		return true;
	}

}
