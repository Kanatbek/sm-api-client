package com.cordialsr.domain.validator;

import java.math.BigDecimal;

import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.InvoiceItem;
import com.cordialsr.domain.StockIn;
import com.cordialsr.domain.StockOut;
import com.cordialsr.domain.StockStatus;
import com.cordialsr.general.GeneralUtil;

public class StockValidator {
	

	public static boolean validateStock(StockIn stock) throws Exception {
		
		if (stock.getCompany() == null 
				|| GeneralUtil.isEmptyInteger(stock.getCompany().getId()))
			throw new Exception("Company is empty for Stock.");
		
		if (stock.getBranch() == null 
				|| GeneralUtil.isEmptyInteger(stock.getBranch().getId()))
			throw new Exception("Branch is empty for Stock.");

		if (stock.getInventory() == null 
				|| GeneralUtil.isEmptyInteger(stock.getInventory().getId()))
			throw new Exception("Inventory is empty for Stock.");

		if (stock.getQuantity() == null)
			throw new Exception("Quantity is empty for Stock.");

		if (stock.getUnit() == null)
			throw new Exception("Unit is empty for Stock.");

//		if (stock.getFinEntry() == null 
//				|| GeneralUtil.isEmptyInteger(stock.getFinEntry().getId()))
//			throw new Exception("Finentry is empty for Stock.");
//
//		if (stock.getSerialNumber() == null)
//			throw new Exception("Serial Number is empty for Stock.");

//		if (stock.getRefkey() == null)
//			throw new Exception("Refkey is empty for Stock.");

		if (stock.getWsumm() == null)
			throw new Exception("Wsumm is empty for Stock.");
		
		if (stock.getWcurrency() == null)
			throw new Exception("Wcurrency is empty for Stock.");

//		if (stock.getDsumm() == null)
//			throw new Exception("Dsumm is empty for Stock.");
//		
//		if (stock.getDcurrency() == null)
//			throw new Exception("Dcurrency is empty for Stock.");
		
		if (stock.getRate() == null)
			throw new Exception("Rate is empty for Stock.");
		
		return true;
	}
	
	public static Boolean checkStockInSNPersist(Invoice invoice) throws Exception {
		try {
		    int count = 0;
		    int counter = 0;
//		    for (InvoiceItem ii: invoice.getInvoiceItems()) {
//		    	for (StockIn stock: ii.getChildStockIns()) {
//		    		if (stock.getQuantity().signum() < 0 ) {return false;}
//		    		if (stock.getQuantity().equals(new BigDecimal(1)) && stock.getSerialNumber().length() > 0) {
//		    			count ++;
//		    			for (InvoiceItem it: invoice.getInvoiceItems()) {
//		    				for (StockIn si: it.getChildStockIns()) {
//		    					if (si.getSerialNumber().length() > 0 && si.getSerialNumber().equals(stock.getSerialNumber())) {
//			    					counter ++;
//		    					}
//		    				}
//		    			}
//		    		}
//		    	}
//		    }
		    if (count == counter) {
		    	return true;
		    }
		    else {
		    	throw new Exception("Sn does not persist in the Database!");
		    }
		} catch (Exception e) {
			throw e;
		}
	}

	public static boolean validateStockForContract(StockIn stock)throws Exception {
		
		if ((stock.getGenStatus().getId() != StockStatus.STATUS_GEN_NEW 
				&& stock.getGenStatus().getId() != StockStatus.STATUS_GEN_RETURNED) 
				|| (stock.getIntStatus().getId() != StockStatus.STATUS_INT_IN 
				&& stock.getIntStatus().getId() != StockStatus.STATUS_INT_ACCOUNTABLE)) 
			throw new Exception("Stock busy!");
		
		return true;
	}
	
	
	public static boolean validateStockReserved(StockIn stock)throws Exception {
		
		if (stock.getIntStatus().getId() != 102) 
			throw new Exception("Stock not reserved!");
		
		return true;
	}
	
	public static boolean validateStockForQuantityEquals(StockIn stock, StockIn sk)throws Exception {
		
		if (stock.getQuantity().equals(sk.getQuantity())) {
			return true;
		} else if (sk.getQuantity().compareTo(stock.getDsumm()) == -1) {
			throw new Exception("Please try to reload Stock's");
		}
		
		return true;
	}

	public static boolean validateStockForReturnType(Invoice invoice)throws Exception {
		
		if (invoice.getId() != null) {
			return false;
		}
		return true;
	}

	public static boolean validateStockAccountable(StockIn stock)throws Exception {
		
		if (stock.getIntStatus().getId().compareTo(new Integer(105)) != 0 || (stock.getGenStatus().getId().compareTo(new Integer(1)) != 0 && stock.getGenStatus().getId().compareTo(new Integer(4)) != 0)) {
			throw new Exception("Stock not Accountable" + " " + stock.getId());
		}
		return true;
	}

	public static boolean validateInvoiceForAIR(Invoice invoice) throws Exception {
		
		for (InvoiceItem ii: invoice.getInvoiceItems()) {
			for (StockIn sk: ii.getChildStockIns()) {
				if (sk.getId() == null || GeneralUtil.isEmptyLong(sk.getId()))
					return true;
			}
		}
		
		return false;
	}

	public static void validateStockOutForEquals(StockOut origStock, StockOut so) throws Exception{
	
		if (origStock.getQuantity().compareTo(so.getDsumm()) == -1)
			throw new Exception("Количество товаров к возврату больше чем в руках у клиента.");
		
	}

	public static boolean validateStockIn(StockIn stock) throws Exception{
		if (stock.getIntStatus().getId().compareTo(StockStatus.STATUS_INT_IN) != 0 ) {
			throw new Exception("Товар не в складе " + stock.getIntStatus().getId());
		}
		if (stock.getGenStatus().getId().compareTo(StockStatus.STATUS_GEN_NEW) != 0 && stock.getGenStatus().getId().compareTo(StockStatus.STATUS_GEN_RETURNED) != 0) {
			throw new Exception("Товар не доступен!");
		}
		return true;
	}

	
}
