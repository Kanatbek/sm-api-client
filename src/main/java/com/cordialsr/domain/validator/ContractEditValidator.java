package com.cordialsr.domain.validator;
import com.cordialsr.domain.Contract;
import com.cordialsr.general.GeneralUtil;


public class ContractEditValidator {

	public static boolean validateBasic(Contract con) throws Exception {

		if (con == null)
			throw new Exception("Контракт пустой.");

		if (con.getServiceBranch() == null || GeneralUtil.isEmptyInteger(con.getServiceBranch().getId()))
			throw new Exception("Укажите сервис-филиал.");
				
		if (con.getExploiter() == null || GeneralUtil.isEmptyLong(con.getExploiter().getId()))
			throw new Exception("Укажите эксплуататора.");
		
		if (con.getCareman() == null || GeneralUtil.isEmptyLong(con.getCareman().getId()))
			throw new Exception("Укажите careman");
		return true;
	}
}
