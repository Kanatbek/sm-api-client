package com.cordialsr.domain.validator;

import com.cordialsr.domain.SmEnquiry;
import com.cordialsr.general.GeneralUtil;

public class SmEnquiryValidator {

	public static boolean validateNewEnquiry(SmEnquiry enq) throws Exception {
		
		if (enq == null)
			throw new Exception("Service Enquiry is empty");
		
		if (enq.getCompany() == null  || GeneralUtil.isEmptyInteger(enq.getCompany().getId()))
			throw new Exception("Company is empty");
		
		if (enq.getBranch() == null || GeneralUtil.isEmptyInteger(enq.getBranch().getId()))
			throw new Exception("Branch is empty");
		
		if (enq.getOperator() == null || GeneralUtil.isEmptyLong(enq.getOperator().getUserid()))
			throw new Exception("Operator is empty");
		
		if (enq.getEdate() == null)
			throw new Exception("Date is empty");
		
		if (enq.getEtime() == null) 
			throw new Exception("Time is empty");
		
		if ((enq.getContract() == null || GeneralUtil.isEmptyLong(enq.getContract().getId())) && (enq.getParty() == null || GeneralUtil.isEmptyLong(enq.getParty().getId()))
				&& enq.getFullFio() == null)
			throw new Exception("No data about client");
		
		if (enq.getPhone() == null)
			throw new Exception("Number is empty");
		
		if (enq.getInfo() == null) 
			throw new Exception("No information");
		
		if (enq.getStatus() == null) 
			throw new Exception("Status is empty");
		
		return true;
	}

}
