package com.cordialsr.domain.validator;


import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractItem;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.SmService;
import com.cordialsr.domain.SmServiceItem;
import com.cordialsr.general.GeneralUtil;

public class InvoiceValidator {
	
	public static boolean validateBasic(Invoice inv) throws Exception {

		if (inv == null)
			throw new Exception("Invoice is null.");

		if (inv.getCompany() == null || GeneralUtil.isEmptyInteger(inv.getCompany().getId()))
			throw new Exception("Please select Company.");

		if (inv.getBranch() == null || GeneralUtil.isEmptyInteger(inv.getBranch().getId()))
			throw new Exception("Please select Branch.");

//		if (inv.getCurrency() == null || GeneralUtil.isEmptyString(inv.getCurrency().getCurrency()))
//			throw new Exception("Please specify Currency.");

		if (inv.getData() == null)
			throw new Exception("Please specify Date.");

		if (inv.getUserAdded() == null || GeneralUtil.isEmptyLong(inv.getUserAdded().getUserid()))
			throw new Exception("Please specify User.");

		if (inv.getParty() == null || GeneralUtil.isEmptyLong(inv.getParty().getId()))
			throw new Exception("Please select Party.");

		return true;
	}
	
	
	public static boolean validateForContract(Contract cnt) throws Exception {
		
		if (cnt == null)
			throw new Exception("Contract is null");
		
		if (cnt.getCompany() == null || GeneralUtil.isEmptyInteger(cnt.getCompany().getId()))
			throw new Exception("Company is empty ");
		
		if (cnt.getBranch() == null || GeneralUtil.isEmptyInteger(cnt.getBranch().getId()))
			throw new Exception("Branch is empty");
		
		if (cnt.getCustomer() == null || GeneralUtil.isEmptyLong(cnt.getCustomer().getId()))
			throw new Exception("Customer is empty");
		
		if (cnt.getRegisteredUser() == null || GeneralUtil.isEmptyLong(cnt.getRegisteredUser().getUserid()))
			throw new Exception("User is empty");
		
		if (cnt.getDateSigned() == null)
			throw new Exception("Please specify Date.");
		
		for (ContractItem ci: cnt.getContractItems()) {
		
			if (ci.getInventory() == null || GeneralUtil.isEmptyInteger(ci.getInventory().getId())) 
				throw new Exception("Inventory is Empty");
			
			if (ci.getInventory().getInvMainCategory() == null || GeneralUtil.isEmptyInteger(ci.getInventory().getInvMainCategory().getId())) 
				throw new Exception("Inventory's Main Category is Empty");
			
			if (ci.getUnit() == null || GeneralUtil.isEmptyString(ci.getUnit().getName())) 
				throw new Exception("Unit is Empty");

			if (ci.getSerialNumber() != null && ci.getQuantity().intValue() > 1) {
				throw new Exception("Quantity more for one Serial Number");
			} 
			
//			if (ci.getGlCode() == null || GeneralUtil.isEmptyString(ci.getGlCode()))
//				throw new Exception("GlCode is empty");
			
		}
		
		return true;
	}
	
	public static boolean validateService(SmService service) throws Exception {

		if (service == null)
			throw new Exception("Service is Empty");
		if (service.getContract() == null || service.getContract().getCompany() == null || GeneralUtil.isEmptyInteger(service.getContract().getCompany().getId()))
			throw new Exception("Company into Contract is Empty");
		if (service.getBranch() == null || GeneralUtil.isEmptyInteger(service.getContract().getCompany().getId()))
			throw new Exception("Branch is Empty");
		if (service.getSdate() == null)
			throw new Exception("Please specify Date.");
		if (service.getUserAdded() == null)
			throw new Exception("User is Empty");
		if (service.getCustomer() == null || GeneralUtil.isEmptyLong(service.getCustomer().getId()))
			throw new Exception("Customer is Empty");
		if (service.getSmServiceItems().size() == 0) 
			throw new Exception("Service Items is Empty");
		for (SmServiceItem si: service.getSmServiceItems()) {
			if (si.getIsTovar() == true) {
				if (si.getQuantity() == null)
					throw new Exception("Service Item Quantity is Empty");
				if (si.getInventory() == null || GeneralUtil.isEmptyInteger(si.getInventory().getId()))
					throw new Exception("Service Item's Inventory is Empty");
				if (si.getUnit() == null || GeneralUtil.isEmptyString(si.getUnit().getName()))
					throw new Exception("Service Item's Unity is Empty");
			}
		}
		return true;
	}
	
	public static boolean validateForService(SmService service) throws Exception {
		
		if (service == null)
			return false;
		
		if (service.getSmServiceItems().size() == 0) 
			return false;
		
		Integer counter = 0;
		for (SmServiceItem si: service.getSmServiceItems()) {
			if (si.getIsTovar() == true)
				counter++;
		}
	
		if (counter == 0)
			return false;
		
		return true;
	}
	
	public static boolean validateForStockSerNum(Invoice invoice) throws Exception {
	
		if (invoice.getInvoiceItems().size() < 1 || invoice.getInvoiceItems().size() > 1)
			throw new Exception ("Number of InvoiceItems are not 1 ...");
		
		if (invoice.getInvoiceItems().get(0).getChildStockIns().size() < 1) 
			throw new Exception ("Number of stocks are less then 1");
		
		return true;
	}

}
