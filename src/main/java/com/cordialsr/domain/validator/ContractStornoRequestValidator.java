package com.cordialsr.domain.validator;

import java.math.BigDecimal;

import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractStornoRequest;
import com.cordialsr.general.GeneralUtil;

public class ContractStornoRequestValidator {
	
	public static boolean validateBasic(ContractStornoRequest conReq) throws Exception {

		if (conReq == null)
			throw new Exception("Запрос пустой.");

		if (conReq.getReqAuthor() == null || GeneralUtil.isEmptyLong(conReq.getReqAuthor().getUserid()))
			throw new Exception("Укажите автора.");
		
		if (conReq.getCloseDate() == null)
			throw new Exception("Укажите дату расторжения.");
				
		if (conReq.getReturnAmount() == null || GeneralUtil.isEmptyOrNegativeSumm(conReq.getReturnAmount())) {
			throw new Exception("Укажите сумму к возврату. Сумма не может быть меньше 0.");
		}
		
		if (conReq.getContract() == null || GeneralUtil.isEmptyLong(conReq.getContract().getId()))
			throw new Exception("Укажите договор.");
		
		Contract con = conReq.getContract();
		if (con.getPaymentSchedules() != null && con.getPaymentSchedules().size() > 0) {
			BigDecimal first = con.getPaymentSchedules().get(0).getPaid();
			BigDecimal availableAmount = con.getPaid().subtract(first);
			if (availableAmount.doubleValue() < conReq.getReturnAmount().doubleValue()) 
				throw new Exception("Сумма превышает сумму оплаченных взносов.");
		}
		
		if (conReq.getCompany() == null || GeneralUtil.isEmptyInteger(conReq.getCompany().getId()))
			throw new Exception("Укажите организацию.");
		
		if (conReq.getBranch() == null || GeneralUtil.isEmptyInteger(conReq.getBranch().getId()))
			throw new Exception("Укажите филиал.");
		
		return true;
	}
	
	
	public static boolean postValidate(ContractStornoRequest conReq) throws Exception {
		validateBasic(conReq);
		
		if (conReq.getGrantAuthor()== null || GeneralUtil.isEmptyLong(conReq.getGrantAuthor().getUserid()))
			throw new Exception("Укажите автора.");
	
		return true;
	}
	
}
