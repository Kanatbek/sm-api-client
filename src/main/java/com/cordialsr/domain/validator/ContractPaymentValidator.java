package com.cordialsr.domain.validator;

import java.math.BigDecimal;

import com.cordialsr.dto.ContractPaymentDto;
import com.cordialsr.general.GeneralUtil;

public class ContractPaymentValidator {

	public static boolean validateCp(ContractPaymentDto cp) throws Exception {

		if (cp == null)
			throw new Exception("Contract payment is null.");

		if (cp.getBranch() == null 
				|| GeneralUtil.isEmptyInteger(cp.getBranch().getId()))
			throw new Exception("Branch is null.");
		
		if (cp.getContract() == null 
				|| GeneralUtil.isEmptyLong(cp.getContract().getId()))
			throw new Exception("Contract is null.");
		
//		if (cp.getContractPaymentSchedule() == null 
//				|| GeneralUtil.isEmptyLong(cp.getContractPaymentSchedule().getId()))
//			throw new Exception("ContractPaymentSchedule is null.");
		
		if (cp.getPaymentDue() == null 
				|| GeneralUtil.isEmptyOrNegativeSumm(cp.getPaymentDue()))
			throw new Exception("PaymentDue is null or negative");
		
		if (cp.getWsumm() == null 
				|| GeneralUtil.isEmptyOrNegativeSumm(cp.getWsumm()))
			throw new Exception("Wsumm is null or negative");

		if (cp.getDsumm() == null 
				|| GeneralUtil.isEmptyOrNegativeSumm(cp.getDsumm()))
			throw new Exception("Dsumm is null or negative");
		
		if (cp.getRate() == null 
				|| GeneralUtil.isEmptyOrNegativeSumm(cp.getRate()))
			throw new Exception("Rate is null or negative");
		
		if (cp.getTotal() == null 
				|| GeneralUtil.isEmptyOrNegativeSumm(cp.getTotal()))
			throw new Exception("Total sum is null or negative");
		
		if (!cp.getOk())
			throw new Exception("Not validated.");
		
		if (cp.getUser() == null
				|| GeneralUtil.isEmptyLong(cp.getUser().getUserid()))
			throw new Exception("User is null.");
		
		if (cp.getWcurrency() == null
				|| GeneralUtil.isEmptyString(cp.getWcurrency().getCurrency()))
			throw new Exception("Local Currency is null.");
		
		if (cp.getDcurrency() == null
				|| GeneralUtil.isEmptyString(cp.getDcurrency().getCurrency()))
			throw new Exception("Foreign Currency is null.");
		
		if (cp.getPdate() == null)
			throw new Exception("Payment Date is null.");
		
		return true;
	}
	
	public static boolean validateCpSums(ContractPaymentDto cp) throws Exception {
		BigDecimal bd = cp.getWsumm().add(cp.getDsumm().multiply(cp.getRate()));
		
		if (cp.getTotal().compareTo(bd) < 0)
			throw new Exception("Total sum is wrong.");
		
		if (cp.getPaymentDue().compareTo(cp.getTotal()) < 0)
			throw new Exception("Total sum is greater than the PaymentDue summ.");
		
		BigDecimal pDue = cp.getContractPaymentSchedule().getSumm().subtract(cp.getContractPaymentSchedule().getPaid());
		if (pDue.compareTo(cp.getPaymentDue()) != 0) 
			throw new Exception("Different PaymentDue summ.");
		
		if (pDue.compareTo(cp.getTotal()) < 0) 
			throw new Exception("Total sum exceeds the PaymentDue amount.");
		
		return true;
	}
}
