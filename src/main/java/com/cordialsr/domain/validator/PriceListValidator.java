package com.cordialsr.domain.validator;

import com.cordialsr.domain.PriceList;
import com.cordialsr.general.GeneralUtil;


public class PriceListValidator {

	public static boolean validateBasic(PriceList fd) throws Exception {

		if (fd == null)
			throw new Exception("PriceList is null.");
	
		if (fd.getCompany() == null || GeneralUtil.isEmptyInteger(fd.getCompany().getId()))
			throw new Exception("Please select Company.");
		
		if (fd.getInventory() == null || GeneralUtil.isEmptyInteger(fd.getInventory().getId()))
			throw new Exception("Please select Inventory.");
		
		if (fd.getScope() == null)
			throw new Exception("Please select Scope.");

		if (fd.getData() == null)
			throw new Exception("Please select Data.");
		
		if (fd.getPrice() == null)
			throw new Exception("Please select Price.");
		
		if (fd.getCurrency() == null)
			throw new Exception("Please select Currency.");
		
		if (fd.getFirstPayment() == null)
			throw new Exception("Please select FirstPayment.");
		
		if (fd.getMonth() == null)
			throw new Exception("Please select Month.");
		
		if (fd.getDealerDeposit() == null)
			throw new Exception("Please select DealerDeposit.");
		
		if (fd.getActive() == null)
			throw new Exception("Please select Active.");
		if(fd.getPaymentTemplates().isEmpty()) {
			throw new Exception(fd.getPaymentTemplates().toString() + "TEMPLATES");
		}
		
		if (fd.getPaymentTemplates() == null)
			throw new Exception("Please select PaymentTemplates.");
		
		
		return true;
	}
}
