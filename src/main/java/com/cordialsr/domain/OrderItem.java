package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "order_item")
public class OrderItem implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Inventory inventory;
	private OrderHeader orderInv;
	private Unit unit;
	private BigDecimal quantity;
	private BigDecimal unitCbm;
	private BigDecimal unitKg;

	public OrderItem() {
	}

	public OrderItem(Inventory inventory, OrderHeader order, Unit unit) {
		this.inventory = inventory;
		this.orderInv = order;
		this.unit = unit;
	}

	public OrderItem(Inventory inventory, OrderHeader order, Unit unit, BigDecimal quantity,
			BigDecimal unitCbm, BigDecimal unitKg) {
		this.inventory = inventory;
		this.orderInv = order;
		this.unit = unit;
		this.quantity = quantity;
		this.unitCbm = unitCbm;
		this.unitKg = unitKg;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventory_id", nullable = false)
	public Inventory getInventory() {
		return this.inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "order_id", nullable = false)
	public OrderHeader getOrderInv() {
		return this.orderInv;
	}

	public void setOrderInv(OrderHeader order) {
		this.orderInv = order;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "unit", nullable = false)
	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Column(name = "quantity", precision = 12, scale = 2)
	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	@Column(name = "unit_cbm", precision = 12, scale = 2)
	public BigDecimal getUnitCbm() {
		return this.unitCbm;
	}

	public void setUnitCbm(BigDecimal unitCbm) {
		this.unitCbm = unitCbm;
	}

	@Column(name = "unit_kg", precision = 12, scale = 2)
	public BigDecimal getUnitKg() {
		return this.unitKg;
	}

	public void setUnitKg(BigDecimal unitKg) {
		this.unitKg = unitKg;
	}

}
