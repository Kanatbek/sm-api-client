package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "party_type")
public class PartyType implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String TYPE_BANK = "BANK";
	public static final String TYPE_CUSTOMER = "CUSTOMER";
	public static final String TYPE_ORG = "ORG";
	public static final String TYPE_PROVIDER = "PROVIDER";
	public static final String TYPE_STAFF = "STAFF";
	public static final String TYPE_STATE = "STATE";
	
	public static final String TYPE_TRANSPORT = "TRANSPORT";
	public static final String TYPE_REKLAM = "REKLAM";
	public static final String TYPE_SERVICE = "SERVICE";
	public static final String TYPE_TRADE = "TRADE";
	public static final String TYPE_TRAVEL = "TRAVEL";
	public static final String TYPE_CONTRACTOR = "CONTRACTOR";
	public static final String TYPE_LAWYER = "LAWYER";
	
//	BANK	Банк	1	0
//	CONTRACTOR	Подрядчик	1	0
//	CUSTOMER	ПОКУПАТЕЛЬ	0	1
//	LAWYER	Юридическое агентство	1	0
//	ORG	Организация	1	0
//	PROVIDER	ПОСТАВЩИК	1	1
//	REKLAM	Рекламное агентство	1	0
//	SERVICE	Сервисная компания	1	0
//	STAFF	СОТРУДНИК	0	1
//	STATE	Государственное учреждение	1	0
//	TRADE	Торговая организация	1	0
//	TRANSPORT	Транспортная компания	1	0
//	TRAVEL	Туристическое агентство	1	0
				
	
	private String name;
	private String description;
	private Boolean yur;
	private Boolean fiz;
	
	public PartyType() {
	}

	public PartyType(String name) {
		this.name = name;
	}

	@Id
	@Column(name = "name", unique = true, nullable = false, length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "description", length = 95)
	public String getDescription() {
		return this.description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "yur")
	public Boolean getYur() {
		return yur;
	}

	public void setYur(Boolean yur) {
		this.yur = yur;
	}

	@Column(name = "fiz")
	public Boolean getFiz() {
		return fiz;
	}

	public void setFiz(Boolean fiz) {
		this.fiz = fiz;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PartyType other = (PartyType) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public PartyType clone() throws CloneNotSupportedException {
		return (PartyType) super.clone();
	}

	
	
}
