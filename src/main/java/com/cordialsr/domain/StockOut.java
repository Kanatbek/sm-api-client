package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
// @JsonDeserialize(using = StockDeserializer.class)
@Table(name = "stock_out")
public class StockOut implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Company company;
	private Branch branch;
	private Inventory inventory;
	private BigDecimal quantity;
	private Unit unit;
	private InvoiceItem invoiceItem;
	private String serialNumber;
	private StockStatus genStatus;
	private String refkey;
	private BigDecimal wsumm;
	private Currency wcurrency;
	private BigDecimal dsumm;
	private Currency dcurrency;
	private BigDecimal rate;
	private Date dateOut;
	private String glCode;
	private Boolean broken;
	
	public StockOut() {
	}
	
	public StockOut(Long id, Company company, Branch branch, Inventory inventory, BigDecimal quantity, Unit unit,
			InvoiceItem invoiceItem, String serialNumber, StockStatus genStatus, String refkey, BigDecimal wsumm,
			Currency wcurrency, BigDecimal dsumm, Currency dcurrency, BigDecimal rate, Date dateOut, String glCode, Boolean broken) {
		this.id = id;
		this.company = company;
		this.branch = branch;
		this.inventory = inventory;
		this.quantity = quantity;
		this.unit = unit;
		this.invoiceItem = invoiceItem;
		this.serialNumber = serialNumber;
		this.genStatus = genStatus;
		this.refkey = refkey;
		this.wsumm = wsumm;
		this.wcurrency = wcurrency;
		this.dsumm = dsumm;
		this.dcurrency = dcurrency;
		this.rate = rate;
		this.dateOut = dateOut;
		this.glCode = glCode;
		this.broken = broken;
	}



	public StockOut(Inventory inventory, StockStatus genStatus, Unit unit, Branch branch) {
		this.inventory = inventory;
		this.genStatus = genStatus;
		this.unit = unit;
		this.branch = branch;
	}

	
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company")
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch")
	public Branch getBranch() {
		return this.branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventory_id", nullable = false)
	public Inventory getInventory() {
		return this.inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}
	
	@Column(name = "quantity", precision = 13, scale = 2)
	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "unit", nullable = false)
	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}
	
	@Column(name = "serial_number", length = 45)
	public String getSerialNumber() {
		return this.serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "gen_status_id", nullable = false)
	public StockStatus getGenStatus() {
		return genStatus;
	}

	public void setGenStatus(StockStatus genStatus) {
		this.genStatus = genStatus;
	}

	@Column(name = "refkey", length = 20)
	public String getRefkey() {
		return refkey;
	}
	
	public void setRefkey(String refkey) {
		this.refkey = refkey;
	}

	@Column(name = "wsumm", precision = 21, scale = 12)
	public BigDecimal getWsumm() {
		return wsumm;
	}

	public void setWsumm(BigDecimal wsumm) {
		this.wsumm = wsumm;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "wcurrency")
	public Currency getWcurrency() {
		return wcurrency;
	}

	public void setWcurrency(Currency wcurrency) {
		this.wcurrency = wcurrency;
	}

	@Column(name = "dsumm", precision = 21, scale = 12)
	public BigDecimal getDsumm() {
		return dsumm;
	}

	public void setDsumm(BigDecimal dsumm) {
		this.dsumm = dsumm;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "dcurrency")
	public Currency getDcurrency() {
		return dcurrency;
	}

	public void setDcurrency(Currency dcurrency) {
		this.dcurrency = dcurrency;
	}

	@Column(name = "rate", precision = 21, scale = 12)
	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	
	@JsonBackReference
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invoice_item_id")
	public InvoiceItem getInvoiceItem() {
		return invoiceItem;
	}

	public void setInvoiceItem(InvoiceItem invoiceItem) {
		this.invoiceItem = invoiceItem;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "date_out", length = 10)
	public Date getDateOut() {
		return dateOut;
	}

	public void setDateOut(Date dateOut) {
		this.dateOut = dateOut;
	}

	@Column(name = "gl_code", length = 4)
	public String getGlCode() {
		return glCode;
	}

	public void setGlCode(String glCode) {
		this.glCode = glCode;
	}
	
	@Column(name = "is_broken")
	public Boolean getBroken() {
		return broken;
	}

	public void setBroken(Boolean broken) {
		this.broken = broken;
	}

	// *****************************************************************************************************************
	

	@Override
	public String toString() {
		return "StockOut [id=" + id + ", company=" + company + ", branch=" + branch + ", inventory=" + inventory
				+ ", quantity=" + quantity + ", unit=" + unit + ", invoiceItem="
				+ ((invoiceItem != null) ? invoiceItem.getId() : "null") + ", serialNumber=" + serialNumber + ", genStatus=" + genStatus + ", refkey=" + refkey
				+ ", wsumm=" + wsumm + ", wcurrency=" + wcurrency + ", dsumm=" + dsumm + ", dcurrency=" + dcurrency
				+ ", rate=" + rate + ", dateOut=" + dateOut + ", glCode=" + glCode + ", broken=" + broken + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((branch == null || branch.getId() == null) ? 0 : branch.getId().hashCode());
		result = prime * result + ((company == null || company.getId() == null) ? 0 : company.getId().hashCode());
		result = prime * result + ((dateOut == null) ? 0 : dateOut.hashCode());
		result = prime * result + ((dcurrency == null || dcurrency.getCurrency() == null) ? 0 : dcurrency.getCurrency().hashCode());
		result = prime * result + ((dsumm == null) ? 0 : dsumm.hashCode());
		result = prime * result + ((genStatus == null || genStatus.getId() == null) ? 0 : genStatus.getId().hashCode());
		result = prime * result + ((glCode == null) ? 0 : glCode.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((inventory == null || inventory.getId() == null) ? 0 : inventory.getId().hashCode());
		result = prime * result + ((invoiceItem == null || invoiceItem.getId() == null) ? 0 : invoiceItem.getId().hashCode());
		result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
		result = prime * result + ((rate == null) ? 0 : rate.hashCode());
		result = prime * result + ((refkey == null) ? 0 : refkey.hashCode());
		result = prime * result + ((serialNumber == null) ? 0 : serialNumber.hashCode());
		result = prime * result + ((unit == null || unit.getName() == null) ? 0 : unit.getName().hashCode());
		result = prime * result + ((wcurrency == null || wcurrency.getCurrency() == null) ? 0 : wcurrency.getCurrency().hashCode());
		result = prime * result + ((wsumm == null) ? 0 : wsumm.hashCode());
		result = prime * result + ((broken == null) ? 0 : broken.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StockOut other = (StockOut) obj;
		if (branch == null) {
			if (other.branch != null)
				return false;
		} else if (branch.getId() != other.branch.getId())
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (company.getId() != other.company.getId())
			return false;
		if (dateOut == null) {
			if (other.dateOut != null)
				return false;
		} else if (!dateOut.equals(other.dateOut))
			return false;
		if (dcurrency == null) {
			if (other.dcurrency != null)
				return false;
		} else if (!dcurrency.getCurrency().equals(other.dcurrency.getCurrency()))
			return false;
		if (dsumm == null) {
			if (other.dsumm != null)
				return false;
		} else if (!dsumm.equals(other.dsumm))
			return false;
		if (genStatus == null) {
			if (other.genStatus != null)
				return false;
		} else if (genStatus.getId() != other.genStatus.getId())
			return false;
		if (glCode == null) {
			if (other.glCode != null)
				return false;
		} else if (!glCode.equals(other.glCode))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (inventory == null) {
			if (other.inventory != null)
				return false;
		} else if (inventory.getId() != other.inventory.getId())
			return false;
		if (invoiceItem == null) {
			if (other.invoiceItem != null)
				return false;
		} else if (invoiceItem.getId() != other.invoiceItem.getId())
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		if (rate == null) {
			if (other.rate != null)
				return false;
		} else if (!rate.equals(other.rate))
			return false;
		if (refkey == null) {
			if (other.refkey != null)
				return false;
		} else if (!refkey.equals(other.refkey))
			return false;
		if (serialNumber == null) {
			if (other.serialNumber != null)
				return false;
		} else if (!serialNumber.equals(other.serialNumber))
			return false;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.getName().equals(other.unit.getName()))
			return false;
		if (wcurrency == null) {
			if (other.wcurrency != null)
				return false;
		} else if (!wcurrency.getCurrency().equals(other.wcurrency.getCurrency()))
			return false;
		if (wsumm == null) {
			if (other.wsumm != null)
				return false;
		} else if (!wsumm.equals(other.wsumm))
			return false;
		if (broken == null) {
			if (other.broken != null)
				return false;
		} else if (!broken.equals(other.broken))
			return false;
		return true;
	}

	@Override
	public StockOut clone() throws CloneNotSupportedException {
		return (StockOut) super.clone();
	}	
	
}
