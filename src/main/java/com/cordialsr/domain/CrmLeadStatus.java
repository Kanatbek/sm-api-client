package com.cordialsr.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "crm_lead_status")
public class CrmLeadStatus implements java.io.Serializable, Cloneable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String description;
	
	
	public CrmLeadStatus() {
	}


	public CrmLeadStatus(String name, String description) {
		this.name = name;
		this.description = description;
	}


	@Id
	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public CrmLeadStatus clone() throws CloneNotSupportedException {
		return (CrmLeadStatus) super.clone();
	}
	
	
	
}
