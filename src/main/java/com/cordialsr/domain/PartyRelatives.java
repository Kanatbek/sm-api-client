package com.cordialsr.domain;

import java.util.Date;  

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cordialsr.domain.deserializer.PartyRelativeDeserializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import static javax.persistence.GenerationType.IDENTITY;


@Entity
@JsonDeserialize(using = PartyRelativeDeserializer.class)
@Table(name = "party_relatives")
public class PartyRelatives implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Party party;
	private String level;
	private String firstname;
	private String lastname;
	private String phone;
	private String jobPlace;
	private String position;
	private Date birthDate;
	
	public PartyRelatives () {
		
	}
	
	public PartyRelatives(Long id) {
		this.id = id;
	}

	public PartyRelatives(Long id, Party party, String level, String firstname, String lastname, String phone, String jobPlace, String position, Date birthDate) {
		this.id = id;
		this.party = party;
		this.level = level;
		this.firstname = firstname;
		this.lastname = lastname;
		this.phone = phone;
		this.jobPlace = jobPlace;
		this.position = position;
		this.birthDate = birthDate;
	}

	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party_id")
	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}

	@Column(name = "level")
	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	@Column(name = "firstname")
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	
	@Column(name = "lastname")
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}


	@Column(name = "phone")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "job_place")
	public String getJobPlace() {
		return jobPlace;
	}

	public void setJobPlace(String jobPlace) {
		this.jobPlace = jobPlace;
	}

	@Column(name = "position")
	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "birth_date")
	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	
	
	@Override
	public PartyRelatives clone() throws CloneNotSupportedException {
		return (PartyRelatives) super.clone();
	}
	
}
