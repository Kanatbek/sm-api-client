package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "scope")
public class Scope implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String SCOPE_COMPANY = "COMPANY";	
	public static final String SCOPE_REGION = "REGION";
	public static final String SCOPE_COUNTRY = "COUNTRY";
	public static final String SCOPE_CITY = "CITY";
	public static final String SCOPE_BRANCH = "BRANCH";
	
//	BRANCH	По филиалу
//	CITY	По городу
//	COMPANY	По всей компании
//	COUNTRY	Внутри страны
//	REGION	По региону
		
	private String name;
	private String info;
	
	public Scope() {
	}

	public Scope(String name) {
		this.name = name;
	}

	public Scope(String name, String info) {
		this.name = name;
		this.info = info;	
	}

	

	@Id

	@Column(name = "name", unique = true, nullable = false, length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "info")
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@Override
	public Scope clone() throws CloneNotSupportedException {
		return (Scope) super.clone();
	}
	
}
