package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "contract_status")
public class ContractStatus implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final Integer STATUS_NEW = 1;
	public static final Integer STATUS_ACTIVE = 2;
	public static final Integer STATUS_COMPLETE = 3;
	public static final Integer STATUS_RESIGNED = 4;
	public static final Integer STATUS_CANCEL= 5;
	
//	1	NEW	Новый 
//	2	ACTIVE	Действует
//	3	COMPLETE	Завершен
//	4	CANCEL	Отменен
	
	private Integer id;
	private String name;
	private String info;

	public ContractStatus() {
	}

	public ContractStatus(int id) {
		this.id = id;
	}

	public ContractStatus(Integer id, String name, String info) {
		this.id = id;
		this.name = name;
		this.info = info;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "info")
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	// ****************************************************************************************************
	
	@Override
	public String toString() {
		return "ContractStatus [id=" + id + ", name=" + name + ", info=" + info + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContractStatus other = (ContractStatus) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public ContractStatus clone() throws CloneNotSupportedException {
		return (ContractStatus) super.clone();
	}

	
}
