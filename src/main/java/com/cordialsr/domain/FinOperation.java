package com.cordialsr.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.cordialsr.domain.security.Transaction;

@Entity
@Table(name = "fin_operation")
public class FinOperation implements Cloneable {
	
	private Integer id;
	private String name;
	private String glDebet;
	private String glCredit;
	private Transaction transaction;
	private String info;
	private Boolean enabled;
	private Boolean parentMandatory;
	private List<FinCashflowStatement> cfs = new ArrayList<>();
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "name", length = 45)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "gl_debet")
	public String getGlDebet() {
		return glDebet;
	}
	public void setGlDebet(String glDebet) {
		this.glDebet = glDebet;
	}
	
	@Column(name = "gl_credit")
	public String getGlCredit() {
		return glCredit;
	}
	public void setGlCredit(String glCredit) {
		this.glCredit = glCredit;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tr_id")
	public Transaction getTransaction() {
		return transaction;
	}
	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}
	
	@Column(name = "info", length = 255)
	public String getInfo() {
		return info;
	}
	
	public void setInfo(String info) {
		this.info = info;
	}
	
	@Column(name = "enabled")
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "finop_cfsta", joinColumns = {
			@JoinColumn(name = "finop", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "cfsta", nullable = false, updatable = false) })
	public List<FinCashflowStatement> getCfs() {
		return cfs;
	}
	public void setCfs(List<FinCashflowStatement> cfs) {
		this.cfs = cfs;
	}
	
	@Column(name = "parent_mandat")
	public Boolean getParentMandatory() {
		return parentMandatory;
	}
	public void setParentMandatory(Boolean parentMandatory) {
		this.parentMandatory = parentMandatory;
	}
	
	@Override
	public FinOperation clone() throws CloneNotSupportedException {
		return (FinOperation) super.clone();
	}
}

