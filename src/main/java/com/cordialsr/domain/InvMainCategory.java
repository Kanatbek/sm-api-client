package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "inv_main_category")
public class InvMainCategory implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final Integer CAT_TOVAR = 1;
	public static final Integer CAT_ZAPCHAST = 2;
	public static final Integer CAT_ACCESSORY = 3;
	public static final Integer CAT_OTHER = 4;
	
//	1	1	ТОВАР	Товары для продажи
//	2	1	ЗАПЧАСТЬ	Запасные части к товарам
//	3	1	АКСЕССУАР	Аксессуары
//	4	1	ПРОЧИЕ	Прочие средства				
	
	private Integer id;
	private Company company;
	private String name;
	private String info;

	public InvMainCategory() {
	}

	public InvMainCategory(Company company, String name, String info) {
		this.company = company;
		this.name = name;
		this.info = info;
	}

	public InvMainCategory(Integer id) {
		this.id = id;
		}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company")
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Column(name = "name", length = 60)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "info")
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	// ****************************************************************************************************
	
	@Override
	public String toString() {
		return "InvMainCategory [id=" + id + ", company=" + company + ", name=" + name + ", info=" + info + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((company == null || company.getId() == null) ? 0 : company.getId().hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvMainCategory other = (InvMainCategory) obj;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (info == null) {
			if (other.info != null)
				return false;
		} else if (!info.equals(other.info))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public InvMainCategory clone() throws CloneNotSupportedException {
		return (InvMainCategory) super.clone();
	}

	
}
