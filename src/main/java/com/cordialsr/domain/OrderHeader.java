package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cordialsr.domain.deserializer.OrderDeserializer;
import com.cordialsr.domain.reducer.UserReducer;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "order_header")
@JsonDeserialize(using = OrderDeserializer.class)
public class OrderHeader implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Branch branchSupplier;
	private Branch branch;
	private InvoiceStatus orderStatus;
	private Company company;
	private Party party;
	private ScopeType scopeType;
	private User userUpdated;
	private User userAdded;
	private Date data;
	private BigDecimal netWeight;
	private BigDecimal volumeCbm;
	private String orderNumber;
	private Date dateUpdated;
	private Date dateCreated;
	
	
	private List<OrderItem> orderItems = new ArrayList<>(0);

	public OrderHeader() {
	}

	public OrderHeader(int id, Branch branch, InvoiceStatus orderStatus, Company company, ScopeType scopeType,
			User userAdded, Date data) {
		this.id = id;
		this.branch = branch;
		this.orderStatus = orderStatus;
		this.userAdded = userAdded;
		this.company = company;
		this.scopeType = scopeType;
		this.data = data;
	}

	public OrderHeader(Integer id, Branch branchSupplier, Branch branch, InvoiceStatus orderStatus,
			Company company, Party party, ScopeType scopeType, User userUpdated,
			User userAdded, Date data, BigDecimal netWeight, 
			BigDecimal volumeCbm, String orderNumber, Date dateUpdated, Date dateCreated) {
		this.id = id;
		this.branchSupplier = branchSupplier;
		this.branch = branch;
		this.orderStatus = orderStatus;
		this.company = company;
		this.party = party;
		this.scopeType = scopeType;
		this.userUpdated = userUpdated;
		this.userAdded = userAdded;
		this.data = data;
		this.netWeight = netWeight;
		this.volumeCbm = volumeCbm;
		this.orderNumber = orderNumber;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch_supplier")
	public Branch getBranchSupplier() {
		return this.branchSupplier;
	}

	public void setBranchSupplier(Branch branchSupplier) {
		this.branchSupplier = branchSupplier;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch", nullable = false)
	public Branch getBranch() {
		return this.branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "order_status", nullable = false)
	public InvoiceStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(InvoiceStatus orderStatus) {
		this.orderStatus = orderStatus;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company", nullable = false)
	public Company getCompany() {
		return this.company;
	}
	
	public void setCompany(Company company) {
		this.company = company;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "supplier")
	public Party getParty() {
		return this.party;
	}

	public void setParty(Party party) {
		this.party = party;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "scope_type", nullable = false)
	public ScopeType getScopeType() {
		return this.scopeType;
	}

	public void setScopeType(ScopeType scopeType) {
		this.scopeType = scopeType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_updated")
	public User getUserUpdated() {
		return UserReducer.reduceMax(this.userUpdated);
	}

	public void setUserUpdated(User userByUserUpdated) {
		this.userUpdated = userByUserUpdated;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_added", nullable = false)
	public User getUserAdded() {
		return this.userAdded;
	}

	public void setUserAdded(User userByUserAdded) {
		this.userAdded = userByUserAdded;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data", nullable = false, length = 10)
	public Date getData() {
		return this.data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	@Column(name = "net_weight", precision = 12, scale = 2)
	public BigDecimal getNetWeight() {
		return this.netWeight;
	}

	public void setNetWeight(BigDecimal netWeight) {
		this.netWeight = netWeight;
	}

	@Column(name = "volume_cbm", precision = 12, scale = 2)
	public BigDecimal getVolumeCbm() {
		return this.volumeCbm;
	}

	public void setVolumeCbm(BigDecimal volumeCbm) {
		this.volumeCbm = volumeCbm;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "orderInv", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<OrderItem> getOrderItems() {
		return this.orderItems;
	}

	public void setOrderItems(List<OrderItem> orderItems) {
		this.orderItems = orderItems;
	}

	@Column(name = "order_number", length = 45)
	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}	
	
	@Temporal(TemporalType.DATE)
	@Column(name = "date_updated", length = 10)
	public Date getDateUpdated() {
		return this.dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	
	
	@Temporal(TemporalType.DATE)
	@Column(name = "date_created", nullable = false, length = 10)
	public Date getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
}
