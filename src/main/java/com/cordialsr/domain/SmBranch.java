package com.cordialsr.domain;
// Generated 23.05.2017 12:18:03 by stalbekr@gmail.com

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sm_branch")
public class SmBranch {

	private String code;
	private String name;
	private String city;
	private String director;
	private String coordinator;
	private Branch realBranch;
	
	public SmBranch() {
	}

	public SmBranch(String code) {
		this.code = code;
	}

	public SmBranch(String code, String name, String city, String director, String coordinator) {
		this.code = code;
		this.name = name;
		this.city = city;
		this.director = director;
		this.coordinator = coordinator;
	}

	@Id
	@Column(name = "CODE", unique = true, nullable = false, length = 3)
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "NAME", length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "CITY", length = 45)
	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "DIRECTOR", length = 100)
	public String getDirector() {
		return this.director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	@Column(name = "COORDINATOR", length = 100)
	public String getCoordinator() {
		return this.coordinator;
	}

	public void setCoordinator(String coordinator) {
		this.coordinator = coordinator;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="real_branch")
	public Branch getRealBranch() {
		return realBranch;
	}

	public void setRealBranch(Branch realBranch) {
		this.realBranch = realBranch;
	}

	
}
