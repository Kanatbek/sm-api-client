package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "invoice_status")
public class InvoiceStatus implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String STATUS_NEW = "NEW";
	public static final String STATUS_PROCESS = "PROCESS";
	public static final String STATUS_DONE = "DONE";
	public static final String STATUS_CLOSED= "CLOSED";
	public static final String STATUS_CANCELLED = "CANCELLED";
	
//	CANCELLED	ОТМЕНЕН
//	CLOSED	ЗАВЕРШЕН
//	DONE	ОБРАБОТАН
//	NEW	НОВЫЙ
//	PROCESS	В ПРОЦЕССЕ
	
	private String name;
	private String info;
	
	public InvoiceStatus() {
	}

	public InvoiceStatus(String name) {
		this.name = name;
	}

	public InvoiceStatus(String name, String info) {
		this.name = name;
		this.info = info;
	}

	@Id

	@Column(name = "name", unique = true, nullable = false, length = 10)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "info", length = 100)
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	// ****************************************************************************************************
	
	@Override
	public String toString() {
		return "InvoiceStatus [name=" + name + ", info=" + info + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvoiceStatus other = (InvoiceStatus) obj;
		if (info == null) {
			if (other.info != null)
				return false;
		} else if (!info.equals(other.info))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public InvoiceStatus clone() throws CloneNotSupportedException {
		return (InvoiceStatus) super.clone();
	}
	
}
