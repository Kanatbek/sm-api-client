package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cordialsr.domain.security.User;

@Entity
@Table(name = "promotion")
public class Promotion implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Branch branch;
	private City city;
	private Company company;
	private Country country;
	private Currency currency;
	private PromoType promoType;
	private Region region;
	private Scope scope;
	private User user;
	private Date data;
	private Date registeredDate;
	private String name;
	private String info;
	private BigDecimal summ;
	private Inventory inventory;
	private BigDecimal fromDealerSumm;
	private Currency fromDealerCurrency;

	public Promotion() {
	}

	public Promotion(PromoType promoType, Scope scope) {
		this.promoType = promoType;
		this.scope = scope;
	}

	public Promotion(Branch branch, City city, Company company, Country country, Currency currency, PromoType promoType,
			Region region, Scope scope, User user, Date data, Date registeredDate, String name, String info,
			BigDecimal summ, Inventory inventory, BigDecimal fromDealerSumm, Currency fromDealerCurrency) {
		this.branch = branch;
		this.city = city;
		this.company = company;
		this.country = country;
		this.currency = currency;
		this.promoType = promoType;
		this.region = region;
		this.scope = scope;
		this.user = user;
		this.data = data;
		this.registeredDate = registeredDate;
		this.name = name;
		this.info = info;
		this.summ = summ;
		this.inventory = inventory;
		this.fromDealerSumm = fromDealerSumm;
		this.fromDealerCurrency = fromDealerCurrency;
	}

	public Promotion(Integer id) {
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch")
	public Branch getBranch() {
		return this.branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "city")
	public City getCity() {
		return this.city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company")
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "country")
	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currency")
	public Currency getCurrency() {
		return this.currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "promo_type_id", nullable = false)
	public PromoType getPromoType() {
		return this.promoType;
	}

	public void setPromoType(PromoType promoType) {
		this.promoType = promoType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "region")
	public Region getRegion() {
		return this.region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "scope_name", nullable = false)
	public Scope getScope() {
		return this.scope;
	}

	public void setScope(Scope scope) {
		this.scope = scope;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "registered_user")
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data", length = 10)
	public Date getData() {
		return this.data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "registered_date", length = 10)
	public Date getRegisteredDate() {
		return this.registeredDate;
	}

	public void setRegisteredDate(Date registeredDate) {
		this.registeredDate = registeredDate;
	}

	@Column(name = "name", length = 100)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "info")
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@Column(name = "summ", precision = 21)
	public BigDecimal getSumm() {
		return this.summ;
	}

	public void setSumm(BigDecimal summ) {
		this.summ = summ;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventory_id")	
	public Inventory getInventory() {
		return this.inventory;
	}

	public void setInventory(Inventory inventoryId) {
		this.inventory = inventoryId;
	}

	@Column(name = "from_dealer_summ", precision = 21)
	public BigDecimal getFromDealerSumm() {
		return this.fromDealerSumm;
	}

	public void setFromDealerSumm(BigDecimal fromDealerSumm) {
		this.fromDealerSumm = fromDealerSumm;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "from_dealer_currency")	
	public Currency getFromDealerCurrency() {
		return this.fromDealerCurrency;
	}

	public void setFromDealerCurrency(Currency fromDealerCurrency) {
		this.fromDealerCurrency = fromDealerCurrency;
	}

	@Override
	protected Promotion clone() throws CloneNotSupportedException {
		return (Promotion) super.clone();
	}
	
}
