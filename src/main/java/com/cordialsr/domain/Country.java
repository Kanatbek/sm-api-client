package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "country", uniqueConstraints = { @UniqueConstraint(columnNames = "country_code"),
		@UniqueConstraint(columnNames = "name"), @UniqueConstraint(columnNames = "phone_code") })
public class Country implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final int COUNTRY_KZ = 1;
	public static final int COUNTRY_KG = 2;
	public static final int COUNTRY_AZ = 3;
	public static final int COUNTRY_UZ = 4;
	public static final int COUNTRY_RU = 5;
	public static final int COUNTRY_KR = 6;
	public static final int COUNTRY_TR = 7;
	
//	1	Kazakhstan
//	2	Kyrgyzstan
//	3	Azerbaycan
//	4	Uzbekistan
//	5	Russia
//	6	South Korea
//	7	Turkey
	
	private Integer id;
	private Currency secondaryCurrency;
	private Currency localCurrency;
	private String name;
	private String phoneCode;
	private String countryCode;	

	public Country() {
	}

	public Country(int id) {
		this.id = id;
	}
	
	public Country(int id, Currency currencyByLocalCurrency, String name) {
		this.id = id;
		this.localCurrency = currencyByLocalCurrency;
		this.name = name;
	}

	public Country(int id, Currency currencyBySecondaryCurrency, Currency currencyByLocalCurrency, String name,
			String phoneCode, String countryCode) {
		this.id = id;
		this.secondaryCurrency = currencyBySecondaryCurrency;
		this.localCurrency = currencyByLocalCurrency;
		this.name = name;
		this.phoneCode = phoneCode;
		this.countryCode = countryCode;		
	}

	
	
	@Override
	public String toString() {
		return "Country [id=" + id + ", name=" + name + ", phoneCode=" + phoneCode + ", countryCode=" + countryCode
				+ "]";
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "secondary_currency")
	public Currency getSecondaryCurrency() {
		return this.secondaryCurrency;
	}

	public void setSecondaryCurrency(Currency currencyBySecondaryCurrency) {
		this.secondaryCurrency = currencyBySecondaryCurrency;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "local_currency", nullable = false)
	public Currency getLocalCurrency() {
		return this.localCurrency;
	}

	public void setLocalCurrency(Currency currencyByLocalCurrency) {
		this.localCurrency = currencyByLocalCurrency;
	}

	@Column(name = "name", unique = true, nullable = false, length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "phone_code", length = 10)
	public String getPhoneCode() {
		return this.phoneCode;
	}

	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}

	@Column(name = "country_code", unique = true, length = 3)
	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((countryCode == null) ? 0 : countryCode.hashCode());
		result = prime * result + ((localCurrency == null || localCurrency.getCurrency() == null) ? 0 : localCurrency.getCurrency().hashCode());
		result = prime * result + ((secondaryCurrency == null || secondaryCurrency.getCurrency() == null) ? 0 : secondaryCurrency.getCurrency().hashCode());
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((phoneCode == null) ? 0 : phoneCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Country other = (Country) obj;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		if (localCurrency == null) {
			if (other.localCurrency != null)
				return false;
		} else if (!localCurrency.getCurrency().equals(other.localCurrency.getCurrency()))
			return false;
		if (secondaryCurrency == null) {
			if (other.secondaryCurrency != null)
				return false;
		} else if (!secondaryCurrency.getCurrency().equals(other.secondaryCurrency.getCurrency()))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (phoneCode == null) {
			if (other.phoneCode != null)
				return false;
		} else if (!phoneCode.equals(other.phoneCode))
			return false;
		return true;
	}

	@Override
	public Country clone() throws CloneNotSupportedException {
		return (Country) super.clone();
	}

}
