package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cordialsr.domain.deserializer.ContractDeserializer;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@JsonDeserialize(using = ContractDeserializer.class)
@Table(name = "contract")
public class Contract implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Company company;
	private Branch branch;
	private Branch serviceBranch;
	private ContractType contractType;
	private ContractStatus contractStatus;
	private Employee coordinator;
	private Employee demosec;
	private Employee director;
	private Employee manager;
	private Employee careman;
	private Employee dealer;
	private Employee fitter;
	private Employee collector;
	
	private Party customer;
	private Address addrPay;
	private PhoneNumber phonePay;
	private PhoneNumber mobilePay;
	
	private Party exploiter;
	private Address addrFact;
	private PhoneNumber phoneFact;
	private PhoneNumber mobileFact;
	
	private PaymentStatus paymentStatus;
	private ServiceCategory serviceCategory;
	private Date dateSigned;
	private String contractNumber;
	private String refkey;
	private Calendar registeredDate;
	private User registeredUser;
	private Calendar updatedDate;
	private User updatedUser;
	private BigDecimal cost;
	private Integer month;
	private BigDecimal discount;
	private BigDecimal summ;
	private Currency currency;
	private BigDecimal paid;
	private BigDecimal rate;
	private BigDecimal fromDealerSumm;
	private Boolean forbuh;
	private Boolean isRent;
	private String info;
	private Boolean resigned;
	private Date resignedDate;
	private Boolean storno;
	private Date stornoDate;
	
	private Integer oldId;
	private Inventory inventory;
	private String inventorySn;
	private Boolean verified;
	private Boolean caremanSales;
	
	private List<ContractPaymentSchedule> paymentSchedules = new ArrayList<>();
	private List<FinDoc> finDocs = new ArrayList<>();
	private List<ContractItem> contractItems = new ArrayList<>();
	private List<ContractPromos> contractPromos = new ArrayList<>();
	private List<ContractAwardSchedule> cawSchedules = new ArrayList<>();
	private List<ContractHistory> contractHistory = new ArrayList<>();
	private List<SmCall> smCalls = new ArrayList<>();
	
	public Contract() {		
	}
	
	public Contract(Long id) {
		this.id = id;
	}

	public Contract(ContractStatus contractStatus, PaymentStatus paymentStatus, ServiceCategory serviceCategory,
			Date dateSigned, String contractNumber, Calendar registeredDate) {
		this.contractStatus = contractStatus;
		this.paymentStatus = paymentStatus;
		this.serviceCategory = serviceCategory;
		this.dateSigned = dateSigned;
		this.contractNumber = contractNumber;
		this.registeredDate = registeredDate;
	}

	public Contract(Long id, Branch branchByServiceBranch, Branch branchByBranch, Company company, ContractStatus contractStatus,
			ContractType contractType, Currency currency,
			Employee coordinator, Employee demosec, Employee director,
			Employee manager, Employee careman, Employee dealer,
			Employee fitter, Employee collector, Party customer, PaymentStatus paymentStatus, ServiceCategory serviceCategory,
			Date dateSigned, String contractNumber, String refkey, Calendar registeredDate, User registeredUser,
			Calendar updatedDate, BigDecimal cost, Integer month, BigDecimal discount, BigDecimal summ, BigDecimal paid,
			BigDecimal rate, BigDecimal fromDealerSumm, Boolean forbuh, Boolean isRent, Address addrPay,
			PhoneNumber phonePay, PhoneNumber mobilePay,  Party exploiter, Address addrFact, PhoneNumber phoneFact, 
			PhoneNumber mobileFact, Inventory inventory,String inventorySn,
			List<ContractPaymentSchedule>  paymentSchedules, List<FinDoc> finDocs, List<ContractItem> contractItems,
			List<ContractPromos> contractPromos, List<ContractAwardSchedule> cawSchedules, String info, Boolean storno, Date stornoDate, 
			Boolean resigned, Date resignedDate, User updatedUser, Integer oldId, List<ContractHistory> contractHistory, Boolean verified, Boolean caremanSales) {
		this.id = id;
		this.serviceBranch = branchByServiceBranch;
		this.branch = branchByBranch;
		this.company = company;
		this.contractStatus = contractStatus;
		this.contractType = contractType;
		this.currency = currency;
		this.coordinator = coordinator;
		this.demosec = demosec;
		this.director = director;
		this.manager = manager;
		this.careman = careman;
		this.dealer = dealer;
		this.fitter = fitter;
		this.collector = collector;
		this.customer = customer;
		this.paymentStatus = paymentStatus;
		this.serviceCategory = serviceCategory;
		this.dateSigned = dateSigned;
		this.contractNumber = contractNumber;
		this.refkey = refkey;
		this.registeredDate = registeredDate;
		this.registeredUser = registeredUser;
		this.updatedDate = updatedDate;
		this.updatedUser = updatedUser;
		this.cost = cost;
		this.month = month;
		this.discount = discount;
		this.summ = summ;
		this.paid = paid;
		this.rate = rate;
		this.fromDealerSumm = fromDealerSumm;
		this.forbuh = forbuh;
		this.isRent = isRent;
		this.inventory = inventory;
		this.inventorySn = inventorySn;
		
		this.addrPay = addrPay;
		this.phonePay = phonePay;
		this.mobilePay = mobilePay;
		this.exploiter = exploiter;
		this.addrFact = addrFact;
		this.phoneFact = phoneFact;
		this.mobileFact = mobileFact;
		
		this.paymentSchedules = paymentSchedules;
		this.finDocs = finDocs;
		this.contractItems = contractItems;
		this.contractPromos = contractPromos;
		this.cawSchedules = cawSchedules;
		this.info = info;
		this.storno = storno;
		this.stornoDate = stornoDate;
		this.resigned = resigned;
		this.resignedDate = resignedDate;
		this.oldId = oldId;
		this.contractHistory = contractHistory;
		this.verified = verified;
		this.caremanSales = caremanSales;
	}

	public Contract(String currencyItem) {
			this.contractNumber = currencyItem;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "service_branch")
	public Branch getServiceBranch() {
		return this.serviceBranch;
	}

	public void setServiceBranch(Branch branchByServiceBranch) {
		this.serviceBranch = branchByServiceBranch;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch")
	public Branch getBranch() {
		return this.branch;
	}

	public void setBranch(Branch branchByBranch) {
		this.branch = branchByBranch;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company")
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "resigned_date", nullable = false, length = 10)
	public Date getResignedDate() {
		return resignedDate;
	}

	public void setResignedDate(Date resignedDate) {
		this.resignedDate = resignedDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contract_status_id")
	public ContractStatus getContractStatus() {
		return this.contractStatus;
	}

	public void setContractStatus(ContractStatus contractStatus) {
		this.contractStatus = contractStatus;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contract_type")
	public ContractType getContractType() {
		return this.contractType;
	}

	public void setContractType(ContractType contractType) {
		this.contractType = contractType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currency")
	public Currency getCurrency() {
		return this.currency;
	}

	public void setCurrency(Currency currencyByCurrency) {
		this.currency = currencyByCurrency;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "coordinator")
	public Employee getCoordinator() {
		return this.coordinator;
	}

	public void setCoordinator(Employee coordinator) {
		this.coordinator = coordinator;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "demosec")
	public Employee getDemosec() {
		return this.demosec;
	}

	public void setDemosec(Employee demosec) {
		this.demosec = demosec;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "director")
	public Employee getDirector() {
		return this.director;
	}

	public void setDirector(Employee director) {
		this.director = director;
	}
	
	@Column(name = "resigned")
	public Boolean getResigned() {
		return resigned;
	}

	public void setResigned(Boolean resigned) {
		this.resigned = resigned;
	}

	@Column(name = "storno")
	public Boolean getStorno() {
		return storno;
	}

	public void setStorno(Boolean storno) {
		this.storno = storno;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "storno_date", length = 10)
	public Date getStornoDate() {
		return stornoDate;
	}

	public void setStornoDate(Date stornoDate) {
		this.stornoDate = stornoDate;
	}

	
	@Column(name = "old_id")
	public Integer getOldId() {
		return oldId;
	}

	public void setOldId(Integer oldId) {
		this.oldId = oldId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "manager")
	public Employee getManager() {
		return this.manager;
	}

	public void setManager(Employee manager) {
		this.manager = manager;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "careman")
	public Employee getCareman() {
		return this.careman;
	}

	public void setCareman(Employee careman) {
		this.careman = careman;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "dealer")
	public Employee getDealer() {
		return this.dealer;
	}

	public void setDealer(Employee dealer) {
		this.dealer = dealer;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fitter")
	public Employee getFitter() {
		return this.fitter;
	}

	public void setFitter(Employee fitter) {
		this.fitter = fitter;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "collector")
	public Employee getCollector() {
		return this.collector;
	}

	public void setCollector(Employee collector) {
		this.collector = collector;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "customer")
	public Party getCustomer() {
		return this.customer;
	}

	public void setCustomer(Party party) {
		this.customer = party;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "payment_status_id")
	public PaymentStatus getPaymentStatus() {
		return this.paymentStatus;
	}

	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "service_category_id")
	public ServiceCategory getServiceCategory() {
		return this.serviceCategory;
	}

	public void setServiceCategory(ServiceCategory serviceCategory) {
		this.serviceCategory = serviceCategory;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "date_signed", nullable = false, length = 10)
	public Date getDateSigned() {
		return this.dateSigned;
	}

	public void setDateSigned(Date dateSigned) {
		this.dateSigned = dateSigned;
	}

	@Column(name = "contract_number", length = 20)
	public String getContractNumber() {
		return this.contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	@Column(name = "refkey", length = 20)
	public String getRefkey() {
		return this.refkey;
	}

	public void setRefkey(String refkey) {
		this.refkey = refkey;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "registered_date")
	public Calendar getRegisteredDate() {
		return this.registeredDate;
	}

	public void setRegisteredDate(Calendar registeredDate) {
		this.registeredDate = registeredDate;
	}

	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "registered_user")
	public User getRegisteredUser() {
		return registeredUser;
	}

	public void setRegisteredUser(User registeredUser) {
		this.registeredUser = registeredUser;
	}	

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated_date")
	public void setUpdatedDate(Calendar updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Calendar getUpdatedDate() {
		return updatedDate;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "updated_user")
	public User getUpdatedUser() {
		return updatedUser;
	}
	
	public void setUpdatedUser(User updatedUser) {
		this.updatedUser = updatedUser;
	}
	
	@Column(name = "cost", precision = 21)
	public BigDecimal getCost() {
		return this.cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	@Column(name = "month")
	public Integer getMonth() {
		return this.month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	@Column(name = "discount", precision = 21)
	public BigDecimal getDiscount() {
		return this.discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	@Column(name = "summ", precision = 21)
	public BigDecimal getSumm() {
		return this.summ;
	}

	public void setSumm(BigDecimal summ) {
		this.summ = summ;
	}

	@Column(name = "paid", precision = 21)
	public BigDecimal getPaid() {
		return this.paid;
	}

	public void setPaid(BigDecimal paid) {
		this.paid = paid;
	}

	@Column(name = "rate", precision = 21)
	public BigDecimal getRate() {
		return this.rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	@Column(name = "from_dealer_summ", precision = 21)
	public BigDecimal getFromDealerSumm() {
		return this.fromDealerSumm;
	}

	public void setFromDealerSumm(BigDecimal fromDealerSumm) {
		this.fromDealerSumm = fromDealerSumm;
	}
	
	@Column(name = "forbuh")
	public Boolean getForbuh() {
		return forbuh;
	}

	public void setForbuh(Boolean forbuh) {
		this.forbuh = forbuh;
	}
	
	@Column(name = "is_rent")
	public Boolean getIsRent() {
		return isRent;
	}

	public void setIsRent(Boolean isRent) {
		this.isRent = isRent;
	}

	@JsonManagedReference
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "contract",cascade = CascadeType.ALL, orphanRemoval = true)
	public List<ContractPaymentSchedule> getPaymentSchedules() {
		return this.paymentSchedules;
	}

	public void setPaymentSchedules(List<ContractPaymentSchedule> paymentSchedules) {
		this.paymentSchedules = paymentSchedules;
	}
	
	@JsonManagedReference
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "contract", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<ContractHistory> getContractHistory() {
		return this.contractHistory;
	}
	
	public void setContractHistory(List<ContractHistory> contractHistory) {
		this.contractHistory = contractHistory;
	}

	@JsonManagedReference
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "contract", orphanRemoval = true)
	public List<FinDoc> getFinDocs() {
		return this.finDocs;
	}

	public void setFinDocs(List<FinDoc> finDocs) {
		this.finDocs = finDocs;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "contract", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<ContractItem> getContractItems() {
		return this.contractItems;
	}

	public void setContractItems(List<ContractItem> contractItems) {
		this.contractItems = contractItems;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "contract", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<ContractPromos> getContractPromos() {
		return this.contractPromos;
	}

	public void setContractPromos(List<ContractPromos> contractPromoses) {
		this.contractPromos = contractPromoses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "contract", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<ContractAwardSchedule> getCawSchedules() {
		return this.cawSchedules;
	}

	public void setCawSchedules(List<ContractAwardSchedule> cawSchedules) {
		this.cawSchedules = cawSchedules;
	}
	
	@Column(name = "careman_sales")
	public Boolean getCaremanSales() {
		return caremanSales;
	}

	public void setCaremanSales(Boolean caremanSales) {
		this.caremanSales = caremanSales;
	}

	// ******************************************************************************************************************************
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "addr_pay")
	public Address getAddrPay() {
		return addrPay;
	}

	public void setAddrPay(Address addrPay) {
		this.addrPay = addrPay;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "phone_pay")
	public PhoneNumber getPhonePay() {
		return phonePay;
	}

	public void setPhonePay(PhoneNumber phonePay) {
		this.phonePay = phonePay;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "mobile_pay")
	public PhoneNumber getMobilePay() {
		return mobilePay;
	}

	public void setMobilePay(PhoneNumber mobilePay) {
		this.mobilePay = mobilePay;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "exploiter")
	public Party getExploiter() {
		return exploiter;
	}

	public void setExploiter(Party exploiter) {
		this.exploiter = exploiter;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "addr_fact")
	public Address getAddrFact() {
		return addrFact;
	}

	public void setAddrFact(Address addrFact) {
		this.addrFact = addrFact;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "phone_fact")
	public PhoneNumber getPhoneFact() {
		return phoneFact;
	}

	public void setPhoneFact(PhoneNumber phoneFact) {
		this.phoneFact = phoneFact;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventory")
	public Inventory getInventory() {
		return inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	@Column(name = "inventory_sn", length = 45)
	public String getInventorySn() {
		return inventorySn;
	}

	public void setInventorySn(String inventorySn) {
		this.inventorySn = inventorySn;
	}

	@Column(name = "info", length = 255)
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "mobile_fact")
	public PhoneNumber getMobileFact() {
		return mobileFact;
	}

	public void setMobileFact(PhoneNumber mobileFact) {
		this.mobileFact = mobileFact;
	}

	@Column(name = "verified")
	public Boolean getVerified() {
		return verified;
	}

	public void setVerified(Boolean verified) {
		this.verified = verified;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "contract")
	public List<SmCall> getSmCalls() {
		return smCalls;
	}

	public void setSmCalls(List<SmCall> smCalls) {
		this.smCalls = smCalls;
	}
	
	// ******************************************************************************************************************************

	@Override
	public String toString() {
		return "Contract [id=" + id + ", company=" + company + ", branch=" + branch + ", serviceBranch=" + serviceBranch
				+ ", contractType=" + contractType + ", contractStatus=" + contractStatus + ", coordinator="
				+ coordinator + ", demosec=" + demosec + ", director=" + director + ", manager=" + manager
				+ ", careman=" + careman + ", dealer=" + dealer + ", fitter=" + fitter + ", customer=" + customer
				+ ", addrPay=" + addrPay + ", phonePay=" + phonePay + ", mobilePay=" + mobilePay + ", exploiter="
				+ exploiter + ", addrFact=" + addrFact + ", phoneFact=" + phoneFact + ", mobileFact=" + mobileFact
				+ ", paymentStatus=" + paymentStatus + ", serviceCategory=" + serviceCategory + ", dateSigned="
				+ dateSigned + ", contractNumber=" + contractNumber + ", refkey=" + refkey + ", registeredDate="
				+ registeredDate + ", registeredUser=" + registeredUser + ", updatedDate=" + updatedDate + ", cost="
				+ cost + ", month=" + month + ", discount=" + discount + ", summ=" + summ + ", currency=" + currency
				+ ", paid=" + paid + ", rate=" + rate + ", fromDealerSumm=" + fromDealerSumm 
				+ ", forbuh=" + forbuh + ", isRent=" + isRent + ", paymentSchedules="
				+ paymentSchedules + ", contractItems=" + contractItems + ", contractPromos="
				+ contractPromos + ", contractHistory=" + contractHistory + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addrFact == null || addrFact.getId() == null) ? 0 : addrFact.getId().hashCode());
		result = prime * result + ((addrPay == null || addrPay.getId() == null) ? 0 : addrPay.getId().hashCode());
		result = prime * result + ((branch == null || branch.getId() == null) ? 0 : branch.getId().hashCode());
		result = prime * result + ((careman == null || careman.getId() == null) ? 0 : careman.getId().hashCode());
		result = prime * result + ((company == null || company.getId() == null) ? 0 : company.getId().hashCode());
		result = prime * result + ((contractItems == null) ? 0 : contractItems.hashCode());
		result = prime * result + ((contractNumber == null) ? 0 : contractNumber.hashCode());
		result = prime * result + ((contractPromos == null) ? 0 : contractPromos.hashCode());
		result = prime * result + ((contractStatus == null || contractStatus.getId() == null) ? 0 : contractStatus.getId().hashCode());
		result = prime * result + ((contractType == null || contractType.getId() == null) ? 0 : contractType.getId().hashCode());
		result = prime * result + ((coordinator == null || coordinator.getId() == null) ? 0 : coordinator.getId().hashCode());
		result = prime * result + ((cost == null) ? 0 : cost.hashCode());
		result = prime * result + ((currency == null || currency.getCurrency() == null) ? 0 : currency.getCurrency().hashCode());
		result = prime * result + ((customer == null || customer.getId() == null) ? 0 : customer.getId().hashCode());
		result = prime * result + ((dateSigned == null) ? 0 : dateSigned.hashCode());
		result = prime * result + ((updatedDate == null) ? 0 : updatedDate.hashCode());
		result = prime * result + ((dealer == null || dealer.getId() == null) ? 0 : dealer.getId().hashCode());
		result = prime * result + ((demosec == null || demosec.getId() == null) ? 0 : demosec.getId().hashCode());
		result = prime * result + ((director == null || director.getId() == null) ? 0 : director.getId().hashCode());
		result = prime * result + ((discount == null) ? 0 : discount.hashCode());
		result = prime * result + ((exploiter == null || exploiter.getId() == null) ? 0 : exploiter.getId().hashCode());
		result = prime * result + ((fitter == null || fitter.getId() == null) ? 0 : fitter.getId().hashCode());
		result = prime * result + ((collector == null) ? 0 : collector.getId().hashCode());
		result = prime * result + ((forbuh == null) ? 0 : forbuh.hashCode());
		result = prime * result + ((fromDealerSumm == null) ? 0 : fromDealerSumm.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((isRent == null) ? 0 : isRent.hashCode());
		result = prime * result + ((manager == null || manager.getId() == null) ? 0 : manager.getId().hashCode());
		result = prime * result + ((mobileFact == null || mobileFact.getId() == null) ? 0 : mobileFact.getId().hashCode());
		result = prime * result + ((mobilePay == null || mobilePay.getId() == null) ? 0 : mobilePay.getId().hashCode());
		result = prime * result + ((month == null) ? 0 : month.hashCode());
		result = prime * result + ((paid == null) ? 0 : paid.hashCode());
		result = prime * result + ((paymentSchedules == null) ? 0 : paymentSchedules.size());
		result = prime * result + ((paymentStatus == null || paymentStatus.getId() == null) ? 0 : paymentStatus.getId().hashCode());
		result = prime * result + ((phoneFact == null || phoneFact.getId() == null) ? 0 : phoneFact.getId().hashCode());
		result = prime * result + ((phonePay == null || phonePay.getId() == null) ? 0 : phonePay.getId().hashCode());
		result = prime * result + ((rate == null) ? 0 : rate.hashCode());
		result = prime * result + ((refkey == null) ? 0 : refkey.hashCode());
		result = prime * result + ((registeredDate == null) ? 0 : registeredDate.hashCode());
		result = prime * result + ((registeredUser == null || registeredUser.getUserid() == null) ? 0 : registeredUser.getUserid().hashCode());
		result = prime * result + ((serviceBranch == null || serviceBranch.getId() == null) ? 0 : serviceBranch.getId().hashCode());
		result = prime * result + ((serviceCategory == null || serviceCategory.getId() == null) ? 0 : serviceCategory.getId().hashCode());
		result = prime * result + ((summ == null) ? 0 : summ.hashCode());
		result = prime * result + ((contractHistory == null) ? 0 : contractHistory.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contract other = (Contract) obj;
		if (addrFact == null) {
			if (other.addrFact != null)
				return false;
		} else if (!addrFact.getId().equals(other.addrFact.getId()))
			return false;
		if (addrPay == null) {
			if (other.addrPay != null)
				return false;
		} else if (!addrPay.getId().equals(other.addrPay.getId()))
			return false;
		if (branch == null) {
			if (other.branch != null)
				return false;
		} else if (!branch.getId().equals(other.branch.getId()))
			return false;
		if (careman == null) {
			if (other.careman != null)
				return false;
		} else if (!careman.getId().equals(other.careman.getId()))
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.getId().equals(other.company.getId()))
			return false;
		if (contractItems == null) {
			if (other.contractItems != null)
				return false;
		} else if (!contractItems.equals(other.contractItems))
			return false;
		if (contractNumber == null) {
			if (other.contractNumber != null)
				return false;
		} else if (!contractNumber.equals(other.contractNumber))
			return false;
		if (contractPromos == null) {
			if (other.contractPromos != null)
				return false;
		} else if (!contractPromos.equals(other.contractPromos))
			return false;
		if (contractStatus == null) {
			if (other.contractStatus != null)
				return false;
		} else if (!contractStatus.getId().equals(other.contractStatus.getId()))
			return false;
		if (contractType == null) {
			if (other.contractType != null)
				return false;
		} else if (!contractType.getId().equals(other.contractType.getId()))
			return false;
		if (coordinator == null) {
			if (other.coordinator != null)
				return false;
		} else if (!coordinator.getId().equals(other.coordinator.getId()))
			return false;
		if (cost == null) {
			if (other.cost != null)
				return false;
		} else if (!cost.equals(other.cost))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.getCurrency().equals(other.currency.getCurrency()))
			return false;
		if (customer == null) {
			if (other.customer != null)
				return false;
		} else if (!customer.getId().equals(other.customer.getId()))
			return false;
		if (dateSigned == null) {
			if (other.dateSigned != null)
				return false;
		} else if (!dateSigned.equals(other.dateSigned))
			return false;
		if (updatedDate == null) {
			if (other.updatedDate != null)
				return false;
		} else if (!updatedDate.equals(other.updatedDate))
			return false;
		if (dealer == null) {
			if (other.dealer != null)
				return false;
		} else if (!dealer.getId().equals(other.dealer.getId()))
			return false;
		if (demosec == null) {
			if (other.demosec != null)
				return false;
		} else if (!demosec.getId().equals(other.demosec.getId()))
			return false;
		if (director == null) {
			if (other.director != null)
				return false;
		} else if (!director.getId().equals(other.director.getId()))
			return false;
		if (discount == null) {
			if (other.discount != null)
				return false;
		} else if (!discount.equals(other.discount))
			return false;
		if (exploiter == null) {
			if (other.exploiter != null)
				return false;
		} else if (!exploiter.getId().equals(other.exploiter.getId()))
			return false;
		if (fitter == null) {
			if (other.fitter != null)
				return false;
		} else if (!fitter.getId().equals(other.fitter.getId()))
			return false;
		if (collector == null) {
			if (other.collector != null)
				return false;
		} else if (!collector.getId().equals(other.collector.getId()))
			return false;
		if (forbuh == null) {
			if (other.forbuh != null)
				return false;
		} else if (!forbuh.equals(other.forbuh))
			return false;
		if (fromDealerSumm == null) {
			if (other.fromDealerSumm != null)
				return false;
		} else if (!fromDealerSumm.equals(other.fromDealerSumm))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isRent == null) {
			if (other.isRent != null)
				return false;
		} else if (!isRent.equals(other.isRent))
			return false;
		if (manager == null) {
			if (other.manager != null)
				return false;
		} else if (!manager.getId().equals(other.manager.getId()))
			return false;
		if (mobileFact == null) {
			if (other.mobileFact != null)
				return false;
		} else if (!mobileFact.getId().equals(other.mobileFact.getId()))
			return false;
		if (mobilePay == null) {
			if (other.mobilePay != null)
				return false;
		} else if (!mobilePay.getId().equals(other.mobilePay.getId()))
			return false;
		if (month == null) {
			if (other.month != null)
				return false;
		} else if (!month.equals(other.month))
			return false;
		if (paid == null) {
			if (other.paid != null)
				return false;
		} else if (!paid.equals(other.paid))
			return false;
		if (contractHistory == null) {
			if (other.contractHistory != null)
				return false;
		} else if (!contractHistory.equals(other.contractHistory))
			return false;
		if (paymentSchedules == null) {
			if (other.paymentSchedules != null)
				return false;
		} else if (!(paymentSchedules.size() == other.paymentSchedules.size()))
			return false;
		if (paymentStatus == null) {
			if (other.paymentStatus != null)
				return false;
		} else if (!paymentStatus.getId().equals(other.paymentStatus.getId()))
			return false;
		if (phoneFact == null) {
			if (other.phoneFact != null)
				return false;
		} else if (!phoneFact.getId().equals(other.phoneFact.getId()))
			return false;
		if (phonePay == null) {
			if (other.phonePay != null)
				return false;
		} else if (!phonePay.getId().equals(other.phonePay.getId()))
			return false;
		if (rate == null) {
			if (other.rate != null)
				return false;
		} else if (!rate.equals(other.rate))
			return false;
		if (refkey == null) {
			if (other.refkey != null)
				return false;
		} else if (!refkey.equals(other.refkey))
			return false;
		if (registeredDate == null) {
			if (other.registeredDate != null)
				return false;
		} else if (!registeredDate.equals(other.registeredDate))
			return false;
		if (registeredUser == null) {
			if (other.registeredUser != null)
				return false;
		} else if (!registeredUser.getUserid().equals(other.registeredUser.getUserid()))
			return false;
		if (serviceBranch == null) {
			if (other.serviceBranch != null)
				return false;
		} else if (!serviceBranch.getId().equals(other.serviceBranch.getId()))
			return false;
		if (serviceCategory == null) {
			if (other.serviceCategory != null)
				return false;
		} else if (!serviceCategory.getId().equals(other.serviceCategory.getId()))
			return false;
		if (summ == null) {
			if (other.summ != null)
				return false;
		} else if (!summ.equals(other.summ))
			return false;
		return true;
	}

	@Override
	public Contract clone() throws CloneNotSupportedException {
		return (Contract) super.clone();
	}

		
}
