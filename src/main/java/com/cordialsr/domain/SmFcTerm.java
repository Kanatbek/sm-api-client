package com.cordialsr.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "sm_fc_term")
public class SmFcTerm {
	
	private Integer id;
	private Company company;
	private Date sdate;
	private Integer prof;
	private Integer f1;
	private Integer f2;
	private Integer f3;
	private Integer f4;
	private Integer f5;
	private Inventory inventory;
	
	public SmFcTerm() {
		
	}
	
	public SmFcTerm(Integer id, Company company, Date sdate, Integer prof, Integer f1, Integer f2, Integer f3,
			Integer f4, Integer f5, Inventory inventory) {
		this.id = id;
		this.company = company;
		this.sdate = sdate;
		this.prof = prof;
		this.f1 = f1;
		this.f2 = f2;
		this.f3 = f3;
		this.f4 = f4;
		this.f5 = f5;
		this.inventory = inventory;
	}
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company_id", nullable = false)
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data", length = 10)
	public Date getSdate() {
		return sdate;
	}
	public void setSdate(Date sdate) {
		this.sdate = sdate;
	}

	@Column(name = "pr")
	public Integer getProf() {
		return prof;
	}
	public void setProf(Integer prof) {
		this.prof = prof;
	}
	
	@Column(name = "f1")
	public Integer getF1() {
		return f1;
	}
	public void setF1(Integer f1) {
		this.f1 = f1;
	}
	
	@Column(name = "f2")
	public Integer getF2() {
		return f2;
	}
	public void setF2(Integer f2) {
		this.f2 = f2;
	}
	
	@Column(name = "f3")
	public Integer getF3() {
		return f3;
	}
	public void setF3(Integer f3) {
		this.f3 = f3;
	}
	
	@Column(name = "f4")
	public Integer getF4() {
		return f4;
	}
	public void setF4(Integer f4) {
		this.f4 = f4;
	}
	
	@Column(name = "f5")
	public Integer getF5() {
		return f5;
	}
	public void setF5(Integer f5) {
		this.f5 = f5;
	}
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventory", nullable = false)
	public Inventory getInventory() {
		return inventory;
	}
	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}
	
	

}
