package com.cordialsr.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "crm_report")
//@JsonDeserialize(using = ReportDeserializer.class)
public class Report implements java.io.Serializable, Cloneable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Branch branch;
	private Company company;
	private Party party;
	private Integer year;
	private Integer month;
	private Integer dealerNumber;
	private Integer demosecNumber;
	private Integer groupNumber;
	private Integer demoToday;
	private Integer demoCurrentMonth;
	private Integer demoPreviousMonth;
	private Integer salesToday;
	private Integer salesCurrentMonth;
	private Integer salesPreviousMonth;
	
	private Integer salesDifferenceBtwMonth;
	private BigDecimal plan;
	private Integer  planPercent;
	private Integer  totalPlanPercent;
	private Integer totalRegionPlan;

	private Integer  forMonthDemoPerSale;
	private Integer  forPreviousMonthDemoPerSale;
	private Integer  forTodayDemoPerDealer;
	private Integer  forCurrentMonthDemoPerDealer;
	private Integer  forCurrentMonthSalePerDealer;
	private String region;  
	
	public Report () {
		
	}
	
	public Report(Long id, Branch branch, Company company, Party party, Integer year, Integer month, Integer dealerNumber, Integer demosecNumber,
			Integer groupNumber, Integer demoToday, Integer demoCurrentMonth, Integer demoPreviousMonth,
			Integer salesToday, Integer salesCurrentMonth, Integer salesPreviousMonth, Integer salesDifferenceBtwMonth,
			BigDecimal plan, Integer  planPercent, Integer  totalPlanPercent, Integer totalRegionPlan, Integer  forMonthDemoPerSale,
			Integer  forPreviousMonthDemoPerSale, Integer  forTodayDemoPerDealer, Integer  forCurrentMonthDemoPerDealer, 
			Integer  forCurrentMonthSalePerDealer, String region) {
		this.id = id;
		this.branch = branch;
		this.company = company;
		this.party = party;
		this.year = year;
		this.month = month;
		this.dealerNumber = dealerNumber;
		this.demosecNumber = demosecNumber;
		this.groupNumber = groupNumber;
		this.demoToday = demoToday;
		this.demoCurrentMonth = demoCurrentMonth;
		this.demoPreviousMonth = demoPreviousMonth;
		this.salesToday = salesToday;
		this.salesCurrentMonth = salesCurrentMonth;
		this.salesPreviousMonth = salesPreviousMonth;
		this.salesDifferenceBtwMonth = salesDifferenceBtwMonth;
		this.plan = plan;
		this.planPercent = planPercent;
		this.totalPlanPercent = totalPlanPercent;
		this.totalRegionPlan = totalRegionPlan;

		this.forMonthDemoPerSale = forMonthDemoPerSale;
		this.forPreviousMonthDemoPerSale = forPreviousMonthDemoPerSale;
		this.forTodayDemoPerDealer = forTodayDemoPerDealer;
		this.forCurrentMonthDemoPerDealer = forCurrentMonthDemoPerDealer;
		this.forCurrentMonthSalePerDealer = forCurrentMonthSalePerDealer;
		this.region = region;
	}



	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch", nullable = false)	
	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company", nullable = false)	
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party", nullable = false)	
	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}
	
	@Column(name = "year")
	public Integer getYear() {
		return year;
	}
	
	public void setYear(Integer year) {
		this.year = year;
	}
	
	@Column(name = "month")
	public Integer getMonth() {
		return month;
	}
	
	public void setMonth(Integer month) {
		this.month = month;
	}

	@Column(name = "dealer_number")
	public Integer getDealerNumber() {
		return dealerNumber;
	}

	public void setDealerNumber(Integer dealerNumber) {
		this.dealerNumber = dealerNumber;
	}

	@Column(name = "demosec_number")
	public Integer getDemosecNumber() {
		return demosecNumber;
	}

	public void setDemosecNumber(Integer demosecNumber) {
		this.demosecNumber = demosecNumber;
	}

	@Column(name = "group_number")
	public Integer getGroupNumber() {
		return groupNumber;
	}

	public void setGroupNumber(Integer groupNumber) {
		this.groupNumber = groupNumber;
	}

	@Column(name = "demo_today")
	public Integer getDemoToday() {
		return demoToday;
	}

	public void setDemoToday(Integer demoToday) {
		this.demoToday = demoToday;
	}

	@Column(name = "demo_current_month")
	public Integer getDemoCurrentMonth() {
		return demoCurrentMonth;
	}

	public void setDemoCurrentMonth(Integer demoCurrentMonth) {
		this.demoCurrentMonth = demoCurrentMonth;
	}

	@Column(name = "demo_previous_month")
	public Integer getDemoPreviousMonth() {
		return demoPreviousMonth;
	}

	public void setDemoPreviousMonth(Integer demoPreviousMonth) {
		this.demoPreviousMonth = demoPreviousMonth;
	}

	@Column(name = "sales_today")
	public Integer getSalesToday() {
		return salesToday;
	}

	public void setSalesToday(Integer salesToday) {
		this.salesToday = salesToday;
	}

	@Column(name = "sales_current_month")
	public Integer getSalesCurrentMonth() {
		return salesCurrentMonth;
	}

	public void setSalesCurrentMonth(Integer salesCurrentMonth) {
		this.salesCurrentMonth = salesCurrentMonth;
	}

	@Column(name = "sales_previous_month")
	public Integer getSalesPreviousMonth() {
		return salesPreviousMonth;
	}

	public void setSalesPreviousMonth(Integer salesPreviousMonth) {
		this.salesPreviousMonth = salesPreviousMonth;
	}
	
	@Column(name = "sales_difference_btw_month")
	public Integer getSalesDifferenceBtwMonth() {
		return salesDifferenceBtwMonth;
	}

	public void setSalesDifferenceBtwMonth(Integer salesDifferenceBtwMonth) {
		this.salesDifferenceBtwMonth = salesDifferenceBtwMonth;
	}
	
	@Column(name = "plan")
	public BigDecimal getPlan() {
		return plan;
	}

	public void setPlan(BigDecimal plan) {
		this.plan = plan;
	}
	
	@Column(name = "planPercent")
	public Integer getPlanPercent() {
		return planPercent;
	}

	public void setPlanPercent(Integer planPercent) {
		this.planPercent = planPercent;
	}
	
	@Column(name = "total_plan_percent")
	public Integer getTotalPlanPercent() {
		return totalPlanPercent;
	}

	public void setTotalPlanPercent(Integer totalPlanPercent) {
		this.totalPlanPercent = totalPlanPercent;
	}

	@Column(name = "total_region_plan")
	public Integer getTotalRegionPlan() {
		return totalRegionPlan;
	}

	public void setTotalRegionPlan(Integer totalRegionPlan) {
		this.totalRegionPlan = totalRegionPlan;
	}
	
	@Column(name = "for_month_demo_per_sale")
	public Integer getForMonthDemoPerSale() {
		return forMonthDemoPerSale;
	}

	public void setForMonthDemoPerSale(Integer forMonthDemoPerSale) {
		this.forMonthDemoPerSale = forMonthDemoPerSale;
	}
	
	@Column(name = "for_previous_month_demo_per_sale")
	public Integer getForPreviousMonthDemoPerSale() {
		return forPreviousMonthDemoPerSale;
	}

	public void setForPreviousMonthDemoPerSale(Integer forPreviousMonthDemoPerSale) {
		this.forPreviousMonthDemoPerSale = forPreviousMonthDemoPerSale;
	}

	
	@Column(name = "for_today_demo_per_dealer")
	public Integer getForTodayDemoPerDealer() {
		return forTodayDemoPerDealer;
	}

	public void setForTodayDemoPerDealer(Integer forTodayDemoPerDealer) {
		this.forTodayDemoPerDealer = forTodayDemoPerDealer;
	}


	@Column(name = "for_current_month_demo_per_dealer")
	public Integer getForCurrentMonthDemoPerDealer() {
		return forCurrentMonthDemoPerDealer;
	}

	public void setForCurrentMonthDemoPerDealer(Integer forCurrentMonthDemoPerDealer) {
		this.forCurrentMonthDemoPerDealer = forCurrentMonthDemoPerDealer;
	}
	
	@Column(name = "for_current_month_sale_per_dealer")
	public Integer getForCurrentMonthSalePerDealer() {
		return forCurrentMonthSalePerDealer;
	}

	public void setForCurrentMonthSalePerDealer(Integer forCurrentMonthSalePerDealer) {
		this.forCurrentMonthSalePerDealer = forCurrentMonthSalePerDealer;
	}
	
	@Column(name = "region")
	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	
	
	

}
