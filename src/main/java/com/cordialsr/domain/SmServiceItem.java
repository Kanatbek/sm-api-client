package com.cordialsr.domain;
// Generated 23.05.2017 12:18:03 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "sm_service_item")
public class SmServiceItem {
	private Long id;
	private SmService smService;
	private String itemName;
	private Integer fno;
	private BigDecimal quantity;
	private Inventory inventory;
	private BigDecimal price;
	private Unit unit;
	private BigDecimal cost;
	private Boolean isTovar;

	public SmServiceItem() {
	}

	public SmServiceItem(Long id) {
		this.id = id;
	}
	
	public SmServiceItem(SmService smService) {
		this.smService = smService;
	}

	public SmServiceItem(Long id, SmService smService, String itemName, Integer fno, BigDecimal quantity, 
			Inventory inventory, BigDecimal price, Unit unit, BigDecimal cost, Boolean isTovar) {
		this.id = id;
		this.smService = smService;
		this.itemName = itemName;
		this.fno = fno;
		this.quantity = quantity;
		this.inventory = inventory;
		this.price = price;
		this.unit = unit;
		this.cost = cost;
		this.isTovar = isTovar;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SERVICE_id", nullable = false)
	public SmService getSmService() {
		return this.smService;
	}

	public void setSmService(SmService smService) {
		this.smService = smService;
	}

	@Column(name = "ITEM_NAME", length = 100)
	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	@Column(name = "FNO")
	public Integer getFno() {
		return this.fno;
	}
	
	public void setFno(Integer fno) {
		this.fno = fno;
	}

	@Column(name = "QUANTITY", precision = 12, scale = 2)
	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventory")
	public Inventory getInventory() {
		return inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	@Column(name = "price", precision = 21, scale = 12)
	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "unit")
	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Column(name = "cost", precision = 21, scale = 12)
	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	@Column(name = "is_tovar")
	public Boolean getIsTovar() {
		return isTovar;
	}

	public void setIsTovar(Boolean isTovar) {
		this.isTovar = isTovar;
	}
	
	
	

}
