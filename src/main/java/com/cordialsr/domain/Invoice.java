package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cordialsr.domain.deserializer.InvoiceDeserializer;
import com.cordialsr.domain.reducer.PartyReducer;
import com.cordialsr.domain.reducer.UserReducer;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "invoice")
@JsonDeserialize(using = InvoiceDeserializer.class)
public class Invoice implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final int INVOICE_TYPE_IMPORT = 1;
	public static final int INVOICE_TYPE_TRANSFER = 2;
	public static final int INVOICE_TYPE_RENT = 3;
	public static final int INVOICE_TYPE_SELL = 4;
	public static final int INVOICE_TYPE_RETURN = 5;
	public static final int INVOICE_TYPE_WRITEOFF = 6;
	public static final int INVOICE_TYPE_BUY = 7;
	public static final int INVOICE_TYPE_ACCOUNTABLE = 8;
	public static final int INVOICE_TYPE_SERVICE = 9;
	public static final int INVOICE_TYPE_AIR = 10;
	
	private Long id;
	private Branch branchImporter;
	private Branch branch;
	private Company company;
	private Currency currency;
	private Incoterm incoterm;
	private InvoiceStatus invoiceStatus;
	private Party party;
	private Party bankPartner;
	private ScopeType scopeType;
	private User userAdded;
	private User userUpdated;
	private Date data;
	private String invoiceNumber;
	private BigDecimal cost;
	private BigDecimal taxRate;
	private BigDecimal taxSum;
	private BigDecimal totalCost;
	private BigDecimal paid;
	private BigDecimal netWeight;
	private BigDecimal grossWeight;
	private BigDecimal volumeCbm;
	private Integer invoiceType;
	private Long docId;
	private String docno;
	private Boolean storno;
	
	private Calendar dateCreated;
	private Calendar dateUpdated;
	private String refkey;
	
	
	private Set<InvoiceTrace> invoiceTraces = new HashSet<>(0);
	private List<InvoiceItem> invoiceItems = new ArrayList<>(0);
	
	public Invoice() {
	}
	
	public Invoice(Long id) {
		this.id = id;
	}

	public Invoice(Long id, Branch branchImporter, Branch branch, Company company,
			InvoiceStatus invoiceStatus, ScopeType scopeType, User userAdded, Date data, Long docId, String docno) {
		this.id = id;
		this.branchImporter = branchImporter;
		this.branch = branch;
		this.company = company;
		this.invoiceStatus = invoiceStatus;
		this.scopeType = scopeType;
		this.userAdded = userAdded;
		this.data = data;
		this.docId = docId;
		this.docno = docno;
	}

	public Invoice(Branch branchImporter, Branch branch,
			Company company, Currency currency, Incoterm incoterm, InvoiceStatus invoiceStatus,
			Party bankPartner, Party supplier, ScopeType scopeType,
			User userAdded, User userUpdated, Date data, String invoiceNumber, BigDecimal cost,
			BigDecimal taxSum, BigDecimal taxRate, BigDecimal totalCost,
			BigDecimal paid, BigDecimal netWeight, 
			BigDecimal grossWeight, BigDecimal volumeCbm, Set<InvoiceTrace> invoiceTraces,
			List<InvoiceItem> invoiceItems, Integer invoiceType, Long docId, String docno, Boolean storno) {	
		this.branchImporter = branchImporter;
		this.branch = branch;
		this.company = company;
		this.currency = currency;
		this.incoterm = incoterm;
		this.invoiceStatus = invoiceStatus;
		this.bankPartner = bankPartner;
		this.party = supplier;
		this.scopeType = scopeType;
		this.userAdded = userAdded;
		this.userUpdated = userUpdated;
		this.data = data;
		this.invoiceNumber = invoiceNumber;
		this.cost = cost;
		this.taxSum = taxSum;
		this.taxRate = taxRate;
		this.totalCost = totalCost;
		this.paid = paid;
		this.netWeight = netWeight;
		this.grossWeight = grossWeight;
		this.volumeCbm = volumeCbm;
		this.invoiceTraces = invoiceTraces;
		this.invoiceItems = invoiceItems;
		this.invoiceType = invoiceType;
		this.docId = docId;
		this.docno = docno;
		this.storno = storno;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch_importer", nullable = false)
	public Branch getBranchImporter() {
		return this.branchImporter;
	}

	public void setBranchImporter(Branch branchByBranchImporter) {
		this.branchImporter = branchByBranchImporter;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch", nullable = false)
	public Branch getBranch() {
		return this.branch;
	}

	public void setBranch(Branch branchByBranch) {
		this.branch = branchByBranch;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company", nullable = false)
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currency")
	public Currency getCurrency() {
		return this.currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "incoterm_code")
	public Incoterm getIncoterm() {
		return this.incoterm;
	}

	public void setIncoterm(Incoterm incoterm) {
		this.incoterm = incoterm;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invoice_status", nullable = false)
	public InvoiceStatus getInvoiceStatus() {
		return this.invoiceStatus;
	}

	public void setInvoiceStatus(InvoiceStatus invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "bank_partner")
	public Party getBankPartner() {
		return PartyReducer.reduceMax(this.bankPartner);
	}

	public void setBankPartner(Party partyByBankPartner) {
		this.bankPartner = partyByBankPartner;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party")
	public Party getParty() {
		return PartyReducer.reduceMax(this.party);
	}

	public void setParty(Party partyBySupplier) {
		this.party = partyBySupplier;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "scope_type", nullable = false)
	public ScopeType getScopeType() {
		return this.scopeType;
	}

	public void setScopeType(ScopeType scopeType) {
		this.scopeType = scopeType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_added", nullable = false)
	public User getUserAdded() {
		return UserReducer.reduceMax(this.userAdded);
	}

	public void setUserAdded(User userByUserAdded) {
		this.userAdded = userByUserAdded;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_updated")
	public User getUserUpdated() {
		return UserReducer.reduceMax(this.userUpdated);
	}

	public void setUserUpdated(User userByUserUpdated) {
		this.userUpdated = userByUserUpdated;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data", nullable = false, length = 10)
	public Date getData() {
		return this.data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	@Column(name = "invoice_number", length = 45)
	public String getInvoiceNumber() {
		return this.invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	@Column(name = "cost", precision = 21)
	public BigDecimal getCost() {
		return this.cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	@Column(name = "tax_sum", precision = 21)
	public BigDecimal getTaxSum() {
		return this.taxSum;
	}

	public void setTaxSum(BigDecimal taxSum) {
		this.taxSum = taxSum;
	}

	@Column(name = "tax_rate", precision = 12)
	public BigDecimal getTaxRate() {
		return this.taxRate;
	}

	public void setTaxRate(BigDecimal taxRate) {
		this.taxRate = taxRate;
	}

	@Column(name = "total_cost", precision = 21)
	public BigDecimal getTotalCost() {
		return this.totalCost;
	}

	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}

	@Column(name = "paid", precision = 21)
	public BigDecimal getPaid() {
		return this.paid;
	}

	public void setPaid(BigDecimal paid) {
		this.paid = paid;
	}

	@Column(name = "net_weight", precision = 12, scale = 12)
	public BigDecimal getNetWeight() {
		return this.netWeight;
	}

	public void setNetWeight(BigDecimal netWeight) {
		this.netWeight = netWeight;
	}

	@Column(name = "gross_weight", precision = 12, scale = 12)
	public BigDecimal getGrossWeight() {
		return this.grossWeight;
	}

	public void setGrossWeight(BigDecimal grossWeight) {
		this.grossWeight = grossWeight;
	}

	@Column(name = "volume_cbm", precision = 12, scale = 12)
	public BigDecimal getVolumeCbm() {
		return this.volumeCbm;
	}

	public void setVolumeCbm(BigDecimal volumeCbm) {
		this.volumeCbm = volumeCbm;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "invoice")
	public Set<InvoiceTrace> getInvoiceTraces() {
		return this.invoiceTraces;
	}

	public void setInvoiceTraces(Set<InvoiceTrace> invoiceTraces) {
		this.invoiceTraces = invoiceTraces;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "invoice", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<InvoiceItem> getInvoiceItems() {
		return this.invoiceItems;
	}

	public void setInvoiceItems(List<InvoiceItem> invoiceItems) {
		this.invoiceItems = invoiceItems;
	}

	@Column(name="invoice_type", nullable = false)
	public Integer getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(Integer invoiceType) {
		this.invoiceType = invoiceType;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_created", nullable = false)
	public Calendar getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Calendar dateCreated) {
		this.dateCreated = dateCreated;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_updated")
	public Calendar getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Calendar dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	@Column(name="refkey", length = 20)
	public String getRefkey() {
		return refkey;
	}

	public void setRefkey(String refkey) {
		this.refkey = refkey;
	}
	
	@Column(name="doc_id")
	public Long getDocId() {
		return docId;
	}

	public void setDocId(Long docId) {
		this.docId = docId;
	}

	@Column(name="docno", length = 20)
	public String getDocno() {
		return docno;
	}

	public void setDocno(String docno) {
		this.docno = docno;
	}

	@Column(name="storno")
	public Boolean getStorno() {
		return storno;
	}

	public void setStorno(Boolean storno) {
		this.storno = storno;
	}
	// *****************************************************************************************
	
	
	@Override
	public String toString() {
		return "Invoice [id=" + id + ", branchByBranchImporter=" + branchImporter + ", branchByBranch="
				+ branch + ", company=" + company
				+ ", currency=" + currency + ", incoterm=" + incoterm + ", invoiceStatus=" + invoiceStatus
				+ ", partyByBankPartner=" + bankPartner + ", partyBySupplier=" + party
				+ ", scopeType=" + scopeType + ", userByUserAdded="
				+ userAdded + ", userByUserUpdated=" + userUpdated + ", data=" + data + ", invoiceNumber="
				+ invoiceNumber + ", cost=" + cost + ", taxSum=" + taxSum
				+ ", taxRate=" + taxRate + ", totalCost=" + totalCost + ", paid="
				+ paid + ", netWeight=" + netWeight + ", grossWeight="
				+ grossWeight + ", volumeCbm=" + volumeCbm + ", invoiceType=" + invoiceType + ", invoiceItems="
				+ invoiceItems + "]";
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bankPartner == null || bankPartner.getId() == null) ? 0 : bankPartner.getId().hashCode());
		result = prime * result + ((branch == null || branch.getId() == null) ? 0 : branch.getId().hashCode());
		result = prime * result + ((branchImporter == null || branchImporter.getId() == null) ? 0 : branchImporter.getId().hashCode());
		result = prime * result + ((company == null || company.getId() == null) ? 0 : company.getId().hashCode());
		result = prime * result + ((cost == null) ? 0 : cost.hashCode());
		result = prime * result + ((currency == null || currency.getCurrency() == null) ? 0 : currency.getCurrency().hashCode());
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((dateCreated == null) ? 0 : dateCreated.hashCode());
		result = prime * result + ((dateUpdated == null) ? 0 : dateUpdated.hashCode());
		result = prime * result + ((grossWeight == null) ? 0 : grossWeight.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((incoterm == null || incoterm.getCode() == null) ? 0 : incoterm.getCode().hashCode());
		result = prime * result + ((invoiceItems == null) ? 0 : invoiceItems.hashCode());
		result = prime * result + ((invoiceNumber == null) ? 0 : invoiceNumber.hashCode());
		result = prime * result + ((invoiceStatus == null || invoiceStatus.getName() == null) ? 0 : invoiceStatus.getName().hashCode());
		result = prime * result + ((invoiceTraces == null) ? 0 : invoiceTraces.hashCode());
		result = prime * result + ((invoiceType == null) ? 0 : invoiceType.hashCode());
		result = prime * result + ((netWeight == null) ? 0 : netWeight.hashCode());
		result = prime * result + ((paid == null) ? 0 : paid.hashCode());
		result = prime * result + ((party == null || party.getId() == null) ? 0 : party.getId().hashCode());
		result = prime * result + ((refkey == null) ? 0 : refkey.hashCode());
		result = prime * result + ((scopeType == null || scopeType.getTypeCode() == null) ? 0 : scopeType.getTypeCode().hashCode());
		result = prime * result + ((taxRate == null) ? 0 : taxRate.hashCode());
		result = prime * result + ((taxSum == null) ? 0 : taxSum.hashCode());
		result = prime * result + ((totalCost == null) ? 0 : totalCost.hashCode());
		result = prime * result + ((userAdded == null || userAdded.getUserid() == null) ? 0 : userAdded.getUserid().hashCode());
		result = prime * result + ((userUpdated == null || userUpdated.getUserid() == null) ? 0 : userUpdated.getUserid().hashCode());
		result = prime * result + ((volumeCbm == null) ? 0 : volumeCbm.hashCode());
		result = prime * result + ((docId == null) ? 0 : docId.hashCode());
		result = prime * result + ((docno == null) ? 0 : docno.hashCode());
		result = prime * result + ((storno == null) ? 0 : storno.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Invoice other = (Invoice) obj;
		if (bankPartner == null) {
			if (other.bankPartner != null)
				return false;
		} else if (!bankPartner.getId().equals(other.bankPartner.getId()))
			return false;
		if (branch == null) {
			if (other.branch != null)
				return false;
		} else if (!branch.getId().equals(other.branch.getId()))
			return false;
		if (branchImporter == null) {
			if (other.branchImporter != null)
				return false;
		} else if (!branchImporter.getId().equals(other.branchImporter.getId()))
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.getId().equals(other.company.getId()))
			return false;
		if (cost == null) {
			if (other.cost != null)
				return false;
		} else if (!cost.equals(other.cost))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.getCurrency().equals(other.currency.getCurrency()))
			return false;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (dateCreated == null) {
			if (other.dateCreated != null)
				return false;
		} else if (!dateCreated.equals(other.dateCreated))
			return false;
		if (dateUpdated == null) {
			if (other.dateUpdated != null)
				return false;
		} else if (!dateUpdated.equals(other.dateUpdated))
			return false;
		if (grossWeight == null) {
			if (other.grossWeight != null)
				return false;
		} else if (!grossWeight.equals(other.grossWeight))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (incoterm == null) {
			if (other.incoterm != null)
				return false;
		} else if (!incoterm.getCode().equals(other.incoterm.getCode()))
			return false;
		if (invoiceItems == null) {
			if (other.invoiceItems != null)
				return false;
		} else if (!invoiceItems.equals(other.invoiceItems))
			return false;
		if (invoiceNumber == null) {
			if (other.invoiceNumber != null)
				return false;
		} else if (!invoiceNumber.equals(other.invoiceNumber))
			return false;
		if (invoiceStatus == null) {
			if (other.invoiceStatus != null)
				return false;
		} else if (!invoiceStatus.getName().equals(other.invoiceStatus.getName()))
			return false;
		if (invoiceTraces == null) {
			if (other.invoiceTraces != null)
				return false;
		} else if (!invoiceTraces.equals(other.invoiceTraces))
			return false;
		if (invoiceType == null) {
			if (other.invoiceType != null)
				return false;
		} else if (!invoiceType.equals(other.invoiceType))
			return false;
		if (netWeight == null) {
			if (other.netWeight != null)
				return false;
		} else if (!netWeight.equals(other.netWeight))
			return false;
		if (paid == null) {
			if (other.paid != null)
				return false;
		} else if (!paid.equals(other.paid))
			return false;
		if (party == null) {
			if (other.party != null)
				return false;
		} else if (!party.getId().equals(other.party.getId()))
			return false;
		if (refkey == null) {
			if (other.refkey != null)
				return false;
		} else if (!refkey.equals(other.refkey))
			return false;
		if (scopeType == null) {
			if (other.scopeType != null)
				return false;
		} else if (!scopeType.getTypeCode().equals(other.scopeType.getTypeCode()))
			return false;
		if (taxRate == null) {
			if (other.taxRate != null)
				return false;
		} else if (!taxRate.equals(other.taxRate))
			return false;
		if (taxSum == null) {
			if (other.taxSum != null)
				return false;
		} else if (!taxSum.equals(other.taxSum))
			return false;
		if (totalCost == null) {
			if (other.totalCost != null)
				return false;
		} else if (!totalCost.equals(other.totalCost))
			return false;
		if (userAdded == null) {
			if (other.userAdded != null)
				return false;
		} else if (!userAdded.getUserid().equals(other.userAdded.getUserid()))
			return false;
		if (userUpdated == null) {
			if (other.userUpdated != null)
				return false;
		} else if (!userUpdated.getUserid().equals(other.userUpdated.getUserid()))
			return false;
		if (volumeCbm == null) {
			if (other.volumeCbm != null)
				return false;
		} else if (!volumeCbm.equals(other.volumeCbm))
			return false;
		if (docId == null) {
			if (other.docId != null)
				return false;
		} else if (!docId.equals(other.docId))
			return false;
		if (docno == null) {
			if (other.docno != null)
				return false;
		} else if (!docno.equals(other.docno))
			return false;
		if (storno == null) {
			if (other.storno != null)
				return false;
		} else if (!storno.equals(other.storno))
			return false;
		return true;
	}

	@Override
	public Invoice clone() throws CloneNotSupportedException {
		return (Invoice) super.clone();
	}

}
