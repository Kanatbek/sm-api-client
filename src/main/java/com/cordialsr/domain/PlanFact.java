package com.cordialsr.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.cordialsr.domain.deserializer.PlanFactDeserializer;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@JsonDeserialize(using = PlanFactDeserializer.class)
@Table(name = "plan_fact")
public class PlanFact implements Serializable, Cloneable{
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer year;
	private Integer month;
	private Company company;
	private Department department;
	private Region region;
	private Branch branch;
	private Position position;
	private Party party;
	private BigDecimal plan;
	private BigDecimal fact;
	private Date cpuDate;
	private Currency currency;
	private User userCreated;
	private Date updatedDate;
	private User updatedUser;
	private Boolean isOverdue; 
	
	private BigDecimal remain;
	private BigDecimal paidPast;
	private BigDecimal paidCurrent;
	private BigDecimal paidAhead;
	
	public PlanFact() {
	}
	
	public PlanFact(Integer id) {
		this.id = id;
	}
	
	public PlanFact(Integer id, Integer year, Integer month, Company company, Department department, Region region,
			Branch branch, Position position, Party party, BigDecimal plan, BigDecimal fact, Date cpuDate,
			Currency currency, User userCreated, Date updatedDate, User updatedUser, Boolean isOverdue) {
		this.id = id;
		this.year = year;
		this.month = month;
		this.company = company;
		this.department = department;
		this.region = region;
		this.branch = branch;
		this.position = position;
		this.party = party;
		this.plan = plan;
		this.fact = fact;
		this.cpuDate = cpuDate;
		this.currency = currency;
		this.userCreated = userCreated;
		this.updatedDate = updatedDate;
		this.updatedUser = updatedUser;
		this.isOverdue = isOverdue;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party")
	public Party getParty() {
		return this.party;
	}
	
	public void setParty(Party party) {
		this.party = party;
	}

	@Column(name = "plan", precision = 21, scale = 12)
	public BigDecimal getPlan() {
		return this.plan;
	}
	
	public void setPlan(BigDecimal plan) {
		this.plan = plan;
	}
	
	@Column(name = "fact", precision = 21, scale = 12)
	public BigDecimal getFact() {
		return this.fact;
	}
	
	public void setFact(BigDecimal fact) {
		this.fact = fact;
	}
	
	@Column(name = "is_overdue")
	public Boolean getIsOverdue() {
		return isOverdue;
	}

	public void setIsOverdue(Boolean isOverdue) {
		this.isOverdue = isOverdue;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "cpu_date", length = 10)
	public Date getCpuDate() {
		return this.cpuDate;
	}
	
	public void setCpuDate(Date date) {
		this.cpuDate = date;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currency")
	public Currency getCurrency() {
		return this.currency;
	}
	
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	@Column(name = "year")
	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	@Column(name = "month")
	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company")
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "department")
	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "region")
	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch")
	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "position")
	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}
	
	
	@Transient
	public BigDecimal getRemain() {
		return remain;
	}

	@Transient
	public void setRemain(BigDecimal remain) {
		this.remain = remain;
	}

	@Transient
	public BigDecimal getPaidPast() {
		return paidPast;
	}

	@Transient
	public void setPaidPast(BigDecimal paidPast) {
		this.paidPast = paidPast;
	}

	@Transient
	public BigDecimal getPaidCurrent() {
		return paidCurrent;
	}

	@Transient
	public void setPaidCurrent(BigDecimal paidCurrent) {
		this.paidCurrent = paidCurrent;
	}

	@Transient
	public BigDecimal getPaidAhead() {
		return paidAhead;
	}

	@Transient
	public void setPaidAhead(BigDecimal paidAhead) {
		this.paidAhead = paidAhead;
	}

	

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((branch == null || branch.getId() == null) ? 0 : branch.getId().hashCode());
		result = prime * result + ((company == null || company.getId() == null) ? 0 : company.getId().hashCode());
		result = prime * result + ((cpuDate == null) ? 0 : cpuDate.hashCode());
		result = prime * result + ((currency == null || currency.getCurrency() == null) ? 0 : currency.getCurrency().hashCode());
		result = prime * result + ((department == null || department.getId() == null) ? 0 : department.getId().hashCode());
		result = prime * result + ((fact == null) ? 0 : fact.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((isOverdue == null) ? 0 : isOverdue.hashCode());
		result = prime * result + ((month == null) ? 0 : month.hashCode());
		result = prime * result + ((paidAhead == null) ? 0 : paidAhead.hashCode());
		result = prime * result + ((paidCurrent == null) ? 0 : paidCurrent.hashCode());
		result = prime * result + ((paidPast == null) ? 0 : paidPast.hashCode());
		result = prime * result + ((party == null || party.getId() == null) ? 0 : party.getId().hashCode());
		result = prime * result + ((plan == null) ? 0 : plan.hashCode());
		result = prime * result + ((position == null || position.getId() == null) ? 0 : position.getId().hashCode());
		result = prime * result + ((region == null || region.getId() == null) ? 0 : region.getId().hashCode());
		result = prime * result + ((remain == null) ? 0 : remain.hashCode());
		result = prime * result + ((updatedDate == null) ? 0 : updatedDate.hashCode());
		result = prime * result + ((updatedUser == null || updatedUser.getUserid() == null) ? 0 : updatedUser.getUserid().hashCode());
		result = prime * result + ((userCreated == null || userCreated.getUserid() == null) ? 0 : userCreated.getUserid().hashCode());
		result = prime * result + ((year == null) ? 0 : year.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanFact other = (PlanFact) obj;
		if (branch == null) {
			if (other.branch != null)
				return false;
		} else if (!branch.getId().equals(other.branch.getId()))
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.getId().equals(other.company.getId()))
			return false;
		if (cpuDate == null) {
			if (other.cpuDate != null)
				return false;
		} else if (!cpuDate.equals(other.cpuDate))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.getCurrency().equals(other.currency.getCurrency()))
			return false;
		if (department == null) {
			if (other.department != null)
				return false;
		} else if (!department.getId().equals(other.department.getId()))
			return false;
		if (fact == null) {
			if (other.fact != null)
				return false;
		} else if (!fact.equals(other.fact))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isOverdue == null) {
			if (other.isOverdue != null)
				return false;
		} else if (!isOverdue.equals(other.isOverdue))
			return false;
		if (month == null) {
			if (other.month != null)
				return false;
		} else if (!month.equals(other.month))
			return false;
		if (paidAhead == null) {
			if (other.paidAhead != null)
				return false;
		} else if (!paidAhead.equals(other.paidAhead))
			return false;
		if (paidCurrent == null) {
			if (other.paidCurrent != null)
				return false;
		} else if (!paidCurrent.equals(other.paidCurrent))
			return false;
		if (paidPast == null) {
			if (other.paidPast != null)
				return false;
		} else if (!paidPast.equals(other.paidPast))
			return false;
		if (party == null) {
			if (other.party != null)
				return false;
		} else if (!party.getId().equals(other.party.getId()))
			return false;
		if (plan == null) {
			if (other.plan != null)
				return false;
		} else if (!plan.equals(other.plan))
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.getId().equals(other.position.getId()))
			return false;
		if (region == null) {
			if (other.region != null)
				return false;
		} else if (!region.getId().equals(other.region.getId()))
			return false;
		if (remain == null) {
			if (other.remain != null)
				return false;
		} else if (!remain.equals(other.remain))
			return false;
		if (updatedDate == null) {
			if (other.updatedDate != null)
				return false;
		} else if (!updatedDate.equals(other.updatedDate))
			return false;
		if (updatedUser == null) {
			if (other.updatedUser != null)
				return false;
		} else if (!updatedUser.getUserid().equals(other.updatedUser.getUserid()))
			return false;
		if (userCreated == null) {
			if (other.userCreated != null)
				return false;
		} else if (!userCreated.getUserid().equals(other.userCreated.getUserid()))
			return false;
		if (year == null) {
			if (other.year != null)
				return false;
		} else if (!year.equals(other.year))
			return false;
		return true;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_created")
	public User getUserCreated() {
		return userCreated;
	}

	public void setUserCreated(User userCreated) {
		this.userCreated = userCreated;
	}	
	
	@Temporal(TemporalType.DATE)
	@JoinColumn(name = "updated_date")
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "updated_user")
	public User getUpdatedUser() {
		return updatedUser;
	}

	public void setUpdatedUser(User updatedUser) {
		this.updatedUser = updatedUser;
	}	
	

	@Override
	public PlanFact clone() throws CloneNotSupportedException {
		return (PlanFact) super.clone();
	}
	
}
