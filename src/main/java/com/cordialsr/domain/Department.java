package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "department")
public class Department implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public final static Integer DEP_IT = 1;
	public final static Integer DEP_MARKETING_AND_SALES = 2;
	public final static Integer DEP_FINANCE = 3;
	public final static Integer DEP_SERVICE = 4;
	public final static Integer DEP_LOGISTICS = 5;
	public final static Integer DEP_ADMINISTRATION = 6;
	
	private Integer id;
	private Company company;
	private String name;
	private Set<Region> regions = new HashSet<>(0);
	
	public Department() {
	}

	public Department(Integer id) {
		this.id = id;
	}

	public Department(int id, Company company, String name) {
		this.id = id;
		this.company = company;
		this.name = name;
	}

	public Department(int id, Company company, String name, Set<Region> regions) {
		this.id = id;
		this.company = company;
		this.name = name;
		this.regions = regions;
	}

	public Department(int id) {
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company_id", nullable = false)
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Column(name = "name", nullable = false, length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "department")
	public Set<Region> getRegions() {
		return this.regions;
	}

	public void setRegions(Set<Region> regions) {
		this.regions = regions;
	}

	
	// ******************************************************************
	
	@Override
	public String toString() {
		return "Department [id=" + id + ", company=" + company + ", name=" + name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((company == null || company.getId() == null) ? 0 : company.getId().hashCode());
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Department other = (Department) obj;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public Department clone() throws CloneNotSupportedException {
		return (Department) super.clone();
	}
	
}
