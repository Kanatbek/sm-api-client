package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "branch")
public class Branch implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private City city;
	private Company company;
	private Country country;
	private String branchName;
	private BranchType branchType;
	
	private Set<Region> regions = new HashSet<>(0);
	
	
	public Branch() {
	}

	public Branch(Integer id) {
		this.id = id;
	}

	public Branch(City city, Company company, Country country, String branchName) {
		this.city = city;
		this.company = company;
		this.country = country;
		this.branchName = branchName;
	}

	public Branch(City city, Company company, Country country, String branchName, Set<Region> regions) {
		this.city = city;
		this.company = company;
		this.country = country;
		this.branchName = branchName;		
		this.regions = regions;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "city_id", nullable = false)
	public City getCity() {
		return this.city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company_id", nullable = false)
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "country_id", nullable = false)
	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@Column(name = "branch_name", nullable = false, length = 45)
	public String getBranchName() {
		return this.branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	@JsonIgnore	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "region_has_branch", joinColumns = {
			@JoinColumn(name = "branch_id", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "region_id", nullable = false, updatable = false) })
	public Set<Region> getRegions() {
		return this.regions;
	}

	public void setRegions(Set<Region> regions) {
		this.regions = regions;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "branch_type")
	public BranchType getBranchType() {
		return branchType;
	}

	public void setBranchType(BranchType branchType) {
		this.branchType = branchType;
	}

	@Override
	public String toString() {
		return "Branch [id=" + id + ", city=" + city + ", company=" + company + ", country=" + country + ", branchName="
				+ branchName + ", branchType=" + branchType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((branchName == null) ? 0 : branchName.hashCode());
		result = prime * result + ((branchType == null) ? 0 : branchType.hashCode());
		result = prime * result + ((city == null || city.getId() == null) ? 0 : city.getId().hashCode());
		result = prime * result + ((company == null || company.getId() == null) ? 0 : company.getId().hashCode());
		result = prime * result + ((country == null || country.getId() == null) ? 0 : country.getId().hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Branch other = (Branch) obj;
		if (branchName == null) {
			if (other.branchName != null)
				return false;
		} else if (!branchName.equals(other.branchName))
			return false;
		if (branchType == null) {
			if (other.branchType != null)
				return false;
		} else if (!branchType.equals(other.branchType))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.getId().equals(other.city.getId()))
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.getId().equals(other.company.getId()))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.getId().equals(other.country.getId()))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public Branch clone() throws CloneNotSupportedException {
		return (Branch) super.clone();
	}
}
