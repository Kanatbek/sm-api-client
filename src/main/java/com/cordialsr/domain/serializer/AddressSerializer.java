package com.cordialsr.domain.serializer;

import java.io.IOException;

import com.cordialsr.domain.Address;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class AddressSerializer extends JsonSerializer<Address>{

	@Override
	public void serialize(Address value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
	    jgen.writeStartObject();
        jgen.writeNumberField("id", value.getId());
        jgen.writeObjectField("addressType", value.getAddressType());
        jgen.writeObjectField("country", value.getCountry());
        jgen.writeObjectField("oblast", value.getOblast());
        jgen.writeObjectField("city", value.getCity());
        jgen.writeObjectField("cityArea", value.getCityArea());
        jgen.writeStringField("district", value.getDistrict());
        jgen.writeStringField("street", value.getStreet());
        jgen.writeStringField("houseNumber", value.getHouseNumber());
        jgen.writeStringField("flatNumber", value.getFlatNumber());
        jgen.writeStringField("zipCode", value.getZipCode());
        jgen.writeStringField("info", value.getInfo());
        jgen.writeObjectField("cpuDate", value.getCpuDate());
        jgen.writeEndObject();
	}
	
}
