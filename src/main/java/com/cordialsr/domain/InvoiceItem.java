package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "invoice_item")
public class InvoiceItem implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Inventory inventory;
	private Invoice invoice;
	private Unit unit;
	private BigDecimal quantity;
	private BigDecimal price;
	private Currency currency;
	private BigDecimal cost;
	private BigDecimal unitCbm;
	private BigDecimal unitKg;
	
	List<InvItemSn> childInvSns = new ArrayList<>();
	List<StockIn> childStockIns = new ArrayList<>();
	List<StockOut> childStockOuts = new ArrayList<>();
	
	public InvoiceItem() {
		
	}
	
	public InvoiceItem(Long id) {
		this.id = id;
	}


	public InvoiceItem(Inventory inventory, Invoice invoice, Unit unit) {
		this.inventory = inventory;
		this.invoice = invoice;
		this.unit = unit;
	}

	public InvoiceItem(Inventory inventory, Invoice invoice, Unit unit, BigDecimal quantity, BigDecimal price,
			Currency currency, BigDecimal cost, BigDecimal unitCbm, BigDecimal unitKg) {
		this.inventory = inventory;
		this.invoice = invoice;
		this.unit = unit;
		this.quantity = quantity;
		this.price = price;
		this.currency = currency;
		this.cost = cost;
		this.unitCbm = unitCbm;
		this.unitKg = unitKg;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventory_id", nullable = false)
	public Inventory getInventory() {
		return this.inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invoice_id", nullable = false)
	public Invoice getInvoice() {
		return this.invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "unit", nullable = false)
	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Column(name = "quantity", precision = 13, scale = 2)
	public BigDecimal getQuantity() {
		return this.quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	@Column(name = "price", precision = 21)
	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currency")	
	public Currency getCurrency() {
		return this.currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	@Column(name = "cost", precision = 21)
	public BigDecimal getCost() {
		return this.cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	@Column(name = "unit_cbm", precision = 13, scale = 2)
	public BigDecimal getUnitCbm() {
		return this.unitCbm;
	}

	public void setUnitCbm(BigDecimal unitCbm) {
		this.unitCbm = unitCbm;
	}

	@Column(name = "unit_kg", precision = 13, scale = 2)
	public BigDecimal getUnitKg() {
		return this.unitKg;
	}

	public void setUnitKg(BigDecimal unitKg) {
		this.unitKg = unitKg;
	}

	@Override
	public InvoiceItem clone() throws CloneNotSupportedException {
		return (InvoiceItem) super.clone();
	}	

	// @JsonIgnore
	@JsonManagedReference
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "invoiceItem", cascade = CascadeType.ALL)
	public List<StockIn> getChildStockIns() {
		return childStockIns;
	}

	public void setChildStockIns(List<StockIn> childStocks) {
		this.childStockIns = childStocks;
	}
	
	// @JsonIgnore
	@JsonManagedReference
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "invoiceItem", cascade = CascadeType.ALL)
	public List<StockOut> getChildStockOuts() {
		return childStockOuts;
	}

	public void setChildStockOuts(List<StockOut> childStockOuts) {
		this.childStockOuts = childStockOuts;
	}

	@JsonManagedReference
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "invoiceItem", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<InvItemSn> getChildInvSns() {
		return childInvSns;
	}

	public void setChildInvSns(List<InvItemSn> childInvSns) {
		this.childInvSns = childInvSns;
	}

	// ************************************************************************************************************************
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((childInvSns == null) ? 0 : childInvSns.hashCode());
		result = prime * result + ((childStockIns == null) ? 0 : childStockIns.hashCode());
		result = prime * result + ((childStockOuts == null) ? 0 : childStockOuts.hashCode());
		result = prime * result + ((cost == null) ? 0 : cost.hashCode());
		result = prime * result + ((currency == null || currency.getCurrency() == null) ? 0 : currency.getCurrency().hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((inventory == null || inventory.getId() == null) ? 0 : inventory.getId().hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
		result = prime * result + ((unit == null || unit.getName() == null) ? 0 : unit.getName().hashCode());
		result = prime * result + ((unitCbm == null) ? 0 : unitCbm.hashCode());
		result = prime * result + ((unitKg == null) ? 0 : unitKg.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvoiceItem other = (InvoiceItem) obj;
		if (childInvSns == null) {
			if (other.childInvSns != null)
				return false;
		} else if (!childInvSns.equals(other.childInvSns))
			return false;
		if (childStockIns == null) {
			if (other.childStockIns != null)
				return false;
		} else if (!childStockIns.equals(other.childStockIns))
			return false;
		if (childStockOuts == null) {
			if (other.childStockOuts != null)
				return false;
		} else if (!childStockOuts.equals(other.childStockOuts))
			return false;
		if (cost == null) {
			if (other.cost != null)
				return false;
		} else if (!cost.equals(other.cost))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.getCurrency().equals(other.currency.getCurrency()))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (inventory == null) {
			if (other.inventory != null)
				return false;
		} else if (!inventory.getId().equals(other.inventory.getId()))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.getName().equals(other.unit.getName()))
			return false;
		if (unitCbm == null) {
			if (other.unitCbm != null)
				return false;
		} else if (!unitCbm.equals(other.unitCbm))
			return false;
		if (unitKg == null) {
			if (other.unitKg != null)
				return false;
		} else if (!unitKg.equals(other.unitKg))
			return false;
		return true;
	}	
}
