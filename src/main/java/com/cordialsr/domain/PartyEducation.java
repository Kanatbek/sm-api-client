package com.cordialsr.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cordialsr.domain.deserializer.PartyEducationDeserializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity

@JsonDeserialize(using = PartyEducationDeserializer.class)
@Table(name = "party_education")
public class PartyEducation implements java.io.Serializable, Cloneable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Party party;
	private EducationLevel level;
	private String academy;
	private String faculty;
	private String specialization;
	private Date dateStart;
	private Date dateEnd;
	private Boolean isAcademic;
	
	public PartyEducation() {
		
	}
	
	public PartyEducation(Long id) {
		this.id = id;
	}

	public PartyEducation(Long id, Party party, EducationLevel level, String academy, 
						  String faculty, String specialization, Date dateStart, Date dateEnd, Boolean isAcademic) {
		this.id = id;
		this.party = party;
		this.level = level;
		this.academy = academy;
		this.faculty = faculty;
		this.specialization = specialization;
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
		this.isAcademic = isAcademic;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
	@Column(name="id", unique = true)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party_id")
	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "level")
	public EducationLevel getLevel() {
		return level;
	}

	public void setLevel(EducationLevel level) {
		this.level = level;
	}
	
	@Column(name = "academy", length = 45)
	public String getAcademy() {
		return academy;
	}
	
	public void setAcademy(String academy) {
		this.academy = academy;
	}
	
	@Column(name = "faculty", length = 45)
	public String getFaculty() {
		return faculty;
	}
	
	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}
	
	@Column(name = "specialization", length = 45)
	public String getSpecialization() {
		return specialization;
	}
	
	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}
	
	@Column(name = "is_academic")
	public Boolean getIsAcademic() {
		return isAcademic;
	}
	
	public void setIsAcademic(Boolean isAcademic) {
		this.isAcademic = isAcademic;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "date_start", length = 10)
	public Date getDateStart() {
		return this.dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "date_end", length = 10)
	public Date getDateEnd() {
		return this.dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}
	
	@Override
	public PartyEducation clone() throws CloneNotSupportedException {
		return (PartyEducation) super.clone();
	}
}
