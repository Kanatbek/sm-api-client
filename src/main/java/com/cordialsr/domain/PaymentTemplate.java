package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "payment_template")
public class PaymentTemplate implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	
	@JsonIgnore
	private PriceList priceList;
	
	private Integer paymentOrder;
	private BigDecimal summ;
	private Integer month;

	public PaymentTemplate() {
	}

	public PaymentTemplate(PriceList priceList) {
		this.priceList = priceList;
	}

	public PaymentTemplate(PriceList priceList, Integer order, BigDecimal summ, Integer month) {
		this.priceList = priceList;
		this.paymentOrder = order;
		this.summ = summ;
		this.month = month;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "price_id", nullable = false)
	public PriceList getPriceList() {
		return this.priceList;
	}

	public void setPriceList(PriceList priceList) {
		this.priceList = priceList;
	}

	@Column(name = "payment_order")
	public Integer getPaymentOrder() {
		return this.paymentOrder;
	}

	public void setPaymentOrder(Integer order) {
		this.paymentOrder = order;
	}

	@Column(name = "summ", precision = 14, scale = 2)
	public BigDecimal getSumm() {
		return this.summ;
	}

	public void setSumm(BigDecimal summ) {
		this.summ = summ;
	}

	@Column(name = "month")
	public Integer getMonth() {
		return this.month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	@Override
	public PaymentTemplate clone() throws CloneNotSupportedException {
		return (PaymentTemplate) super.clone();
	}

	
}
