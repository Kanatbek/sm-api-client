package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.cordialsr.domain.deserializer.FinDocDeserializer;
import com.cordialsr.domain.security.User;
import com.cordialsr.general.GeneralUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@JsonDeserialize(using = FinDocDeserializer.class)
@Table(name = "fin_doc")
public class FinDoc implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Company company;
	private Branch branch;
	private String title;
	private String info;
	private String docno;
	private String year;
	private String refkey;
	private String reftyp;
	private Integer month;
	private FinDocType finDocType;
	private Date cpuDate;
	private Date docDate;
	private Department department;
	private Boolean storno;
	private FinDoc parentFindoc;
	private String stDocno;
	private String stYear;
	private User user;
	private Party party;
	private Contract contract;
	private Integer paymentOrder;
	private Date paymentDue;
	private Boolean isMain;
	private BigDecimal dsumm;
	private Currency dcurrency;
	private BigDecimal rate;
	private BigDecimal wsumm;
	private Currency wcurrency;
	private BigDecimal dsummPaid;
	private BigDecimal wsummPaid;
	private Boolean closed;
	private Boolean forbuh;
	private String trcode;
	private Invoice invoice;
	private String refkeyMain;
	private Date updatedDate;
	private Date cpuTime;
	private Employee employee;
	
	private List<FinEntry> finEntries = new ArrayList<>();
	private Set<FinDoc> childFinDocs = new HashSet<>(0);
	
	public FinDoc() {
	}
	
	public FinDoc(Long id) {
		this.id = id;
	}

	public FinDoc(Company company, Branch branch, String header, String info, String docno, String year, String refkey,
			String reftyp, Integer month, FinDocType finDocType, Date cpuDate, Date docDate, Department department,
			Boolean storno, FinDoc parentFindoc, String stDocno, String stYear, User user, Party party,
			Contract contract, Integer paymentOrder, Boolean isMain, BigDecimal dsumm, Currency dcurrency, BigDecimal rate,
			BigDecimal wsumm, Currency wcurrency, BigDecimal dsummPaid, BigDecimal wsummPaid, Boolean closed,
			Boolean forbuh, String trcode, Invoice invoice, String refkeyMain,
			List<FinEntry> finEntries) {
		this.company = company;
		this.branch = branch;
		this.title = header;
		this.info = info;
		this.docno = docno;
		this.year = year;
		this.refkey = refkey;
		this.reftyp = reftyp;
		this.month = month;
		this.finDocType = finDocType;
		this.cpuDate = cpuDate;
		this.docDate = docDate;
		this.department = department;
		this.storno = storno;
		this.parentFindoc = parentFindoc;
		this.stDocno = stDocno;
		this.stYear = stYear;
		this.user = user;
		this.party = party;
		this.contract = contract;
		this.paymentOrder = paymentOrder;
		this.isMain = isMain;
		this.dsumm = dsumm;
		this.dcurrency = dcurrency;
		this.rate = rate;
		this.wsumm = wsumm;
		this.wcurrency = wcurrency;
		this.dsummPaid = dsummPaid;
		this.wsummPaid = wsummPaid;
		this.closed = closed;
		this.forbuh = forbuh;
		this.trcode = trcode;
		this.invoice = invoice;
		this.refkeyMain = refkeyMain;
		this.finEntries = finEntries;
	}

	// *************************************************************************

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company")
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch")
	public Branch getBranch() {
		return this.branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	
	
	@Column(name = "header", length = 100)
	public String getTitle() {
		return title;
	}

	public void setTitle(String header) {
		this.title = header;
	}
	
	
	@Column(name = "info", length = 255)
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
	
	@Column(name = "docno", length = 16)
	public String getDocno() {
		return docno;
	}

	public void setDocno(String docno) {
		this.docno = docno;
	}
	
	
	@Column(name = "year", length = 4)
	public String getYear() {
		return this.year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	
	@Column(name = "refkey", length = 20)
	public String getRefkey() {
		return refkey;
	}

	public void setRefkey(String refkey) {
		this.refkey = refkey;
	}


	@Column(name = "reftyp", length = 5)
	public String getReftyp() {
		return reftyp;
	}

	public void setReftyp(String reftyp) {
		this.reftyp = reftyp;
	}
	
	
	@Column(name = "month")
	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "findoc_type", nullable = false)
	public FinDocType getFinDocType() {
		return this.finDocType;
	}

	public void setFinDocType(FinDocType finDocType) {
		this.finDocType = finDocType;
	}
	
	
	@Temporal(TemporalType.DATE)
	@Column(name = "cpu_date", length = 10)
	public Date getCpuDate() {
		return cpuDate;
	}

	public void setCpuDate(Date cpuDate) {
		this.cpuDate = cpuDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "updated_date", length = 10)
	public Date getUpdatedDate() {
		return updatedDate;		
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "doc_date", length = 10)
	public Date getDocDate() {
		return docDate;
	}

	public void setDocDate(Date docDate) {
		this.docDate = docDate;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "payment_due", length = 10)
	public Date getPaymentDue() {
		return paymentDue;
	}

	public void setPaymentDue(Date paymentDue) {
		this.paymentDue = paymentDue;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "department", nullable = false)
	public Department getDepartment() {
		return this.department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}
	
	
	@Column(name = "storno")
	public Boolean getStorno() {
		return this.storno;
	}

	public void setStorno(Boolean storno) {
		this.storno = storno;
	}
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_findoc")
	public FinDoc getParentFindoc() {
		return parentFindoc;
	}

	public void setParentFindoc(FinDoc parentFindoc) {
		this.parentFindoc = parentFindoc;
	}

	@JsonIgnore	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "parentFindoc")
	public Set<FinDoc> getChildFinDocs() {
		return childFinDocs;
	}

	public void setChildFinDocs(Set<FinDoc> childFinDocs) {
		this.childFinDocs = childFinDocs;
	}
	
	@Column(name = "st_docno", length = 10)
	public String getStDocno() {
		return stDocno;
	}

	public void setStDocno(String stDocno) {
		this.stDocno = stDocno;
	}


	@Column(name = "st_year", length = 4)
	public String getStYear() {
		return stYear;
	}

	public void setStYear(String stYear) {
		this.stYear = stYear;
	}

	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user")
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party_id")
	public Party getParty() {
		return this.party;
	}

	public void setParty(Party party) {
		this.party = party;
	}
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "employee_id")
	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contract_id")
	public Contract getContract() {
		return this.contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}
	
	
	@Column(name = "payment_order")
	public Integer getPaymentOrder() {
		return this.paymentOrder;
	}

	public void setPaymentOrder(Integer order) {
		this.paymentOrder = order;
	}
	
	@Column(name = "is_main", nullable = false)
	public Boolean getIsMain() {
		return isMain;
	}

	public void setIsMain(Boolean isMain) {
		this.isMain = isMain;
	}
	
	@Column(name = "dsumm", precision = 31, scale = 12)
	public BigDecimal getDsumm() {
		return dsumm;
	}

	public void setDsumm(BigDecimal dsumm) {
		this.dsumm = dsumm;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "dcurrency")
	public Currency getDcurrency() {
		return dcurrency;
	}

	public void setDcurrency(Currency dcurrency) {
		this.dcurrency = dcurrency;
	}


	@Column(name = "wsumm", precision = 21, scale = 4)
	public BigDecimal getWsumm() {
		return wsumm;
	}

	public void setWsumm(BigDecimal wsumm) {
		this.wsumm = wsumm;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "wcurrency")
	public Currency getWcurrency() {
		return wcurrency;
	}

	public void setWcurrency(Currency wcurrency) {
		this.wcurrency = wcurrency;
	}


	@Column(name = "dsumm_paid", precision = 21, scale = 2)
	public BigDecimal getDsummPaid() {
		return dsummPaid;
	}

	public void setDsummPaid(BigDecimal dsummPaid) {
		this.dsummPaid = dsummPaid;
	}


	@Column(name = "wsumm_paid", precision = 21, scale = 2)
	public BigDecimal getWsummPaid() {
		return wsummPaid;
	}

	public void setWsummPaid(BigDecimal wsummPaid) {
		this.wsummPaid = wsummPaid;
	}

	
	@Column(name = "rate", precision = 21, scale = 12)
	public BigDecimal getRate() {
		return this.rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	
	
	@Column(name = "closed")
	public Boolean getClosed() {
		return closed;
	}

	public void setClosed(Boolean closed) {
		this.closed = closed;
	}


	@Column(name = "forbuh")
	public Boolean getForbuh() {
		return forbuh;
	}

	public void setForbuh(Boolean forbuh) {
		this.forbuh = forbuh;
	}


	@Column(name = "trcode", length = 10)
	public String getTrcode() {
		return trcode;
	}

	public void setTrcode(String trcode) {
		this.trcode = trcode;
	}
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invoice_id")
	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}


	@Column(name = "refkey_main", length = 20)
	public String getRefkeyMain() {
		return refkeyMain;
	}

	public void setRefkeyMain(String refkeyMain) {
		this.refkeyMain = refkeyMain;
	}

	// @JsonBackReference
	@JsonManagedReference
	@JsonIgnore
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "finDoc", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<FinEntry> getFinEntries() {
		return finEntries;
	}

	public void setFinEntries(List<FinEntry> finEntries) {
		this.finEntries = finEntries;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "cpu_time", nullable = false)
	public Date getCpuTime() {
		return cpuTime;
	}

	public void setCpuTime(Date cpuTime) {
		this.cpuTime = cpuTime;
	}

	// **************************************************************************************
	
	@Override
	public String toString() {
		return "FinDoc [id=" + id + ", company=" + company + ", branch=" + branch + ", header=" + title + ", info="
				+ info + ", docno=" + docno + ", year=" + year + ", refkey=" + refkey + ", reftyp=" + reftyp
				+ ", month=" + month + ", finDocType=" + finDocType + ", cpuDate=" + cpuDate + ", docDate=" + docDate
				+ ", department=" + department + ", storno=" + storno + ", stDocno=" + stDocno + ", stYear=" + stYear
				+ ", user=" + ((user != null) ? user.getUsername() : "null") 
				+ ", party=" + ((party != null) ? party.getFullFIO() : "null") 
				+ ", contract=" + ((contract != null) ? contract.getContractNumber() : "null") + ", order=" + paymentOrder + ", isMain="
				+ isMain + ", dsumm=" + dsumm + ", dcurrency=" + dcurrency + ", rate=" + rate + ", wsumm=" + wsumm
				+ ", wcurrency=" + wcurrency + ", dsummPaid=" + dsummPaid + ", wsummPaid=" + wsummPaid + ", closed="
				+ closed + ", forbuh=" + forbuh + ", trcode=" + trcode + ", invoice=" + invoice
				+ ", refkeyMain=" + refkeyMain + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((branch == null || branch.getId() == null) ? 0 : branch.getId().hashCode());
		result = prime * result + ((childFinDocs == null) ? 0 : childFinDocs.hashCode());
		result = prime * result + ((closed == null) ? 0 : closed.hashCode());
		result = prime * result + ((company == null || company.getId() == null) ? 0 : company.getId().hashCode());
		result = prime * result + ((contract == null || contract.getId() == null) ? 0 : contract.getId().hashCode());
		result = prime * result + ((cpuDate == null) ? 0 : cpuDate.hashCode());
		result = prime * result + ((dcurrency == null || dcurrency.getCurrency() == null) ? 0 : dcurrency.getCurrency().hashCode());
		result = prime * result + ((department == null || department.getId() == null) ? 0 : department.getId().hashCode());
		result = prime * result + ((docDate == null) ? 0 : docDate.hashCode());
		result = prime * result + ((docno == null) ? 0 : docno.hashCode());
		result = prime * result + ((dsumm == null) ? 0 : dsumm.hashCode());
		result = prime * result + ((dsummPaid == null) ? 0 : dsummPaid.hashCode());
		result = prime * result + ((finDocType == null || finDocType.getCode() == null) ? 0 : finDocType.getCode().hashCode());
		result = prime * result + ((finEntries == null) ? 0 : finEntries.hashCode());
		result = prime * result + ((forbuh == null) ? 0 : forbuh.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((invoice == null || invoice.getId() == null) ? 0 : invoice.getId().hashCode());
		result = prime * result + ((isMain == null) ? 0 : isMain.hashCode());
		result = prime * result + ((month == null) ? 0 : month.hashCode());
		result = prime * result + ((paymentOrder == null) ? 0 : paymentOrder.hashCode());
		result = prime * result + ((party == null || party.getId() == null) ? 0 : party.getId().hashCode());
		result = prime * result + ((rate == null) ? 0 : rate.hashCode());
		result = prime * result + ((refkey == null) ? 0 : refkey.hashCode());
		result = prime * result + ((refkeyMain == null) ? 0 : refkeyMain.hashCode());
		result = prime * result + ((reftyp == null) ? 0 : reftyp.hashCode());
		result = prime * result + ((stDocno == null) ? 0 : stDocno.hashCode());
		result = prime * result + ((stYear == null) ? 0 : stYear.hashCode());
		result = prime * result + ((storno == null) ? 0 : storno.hashCode());
		result = prime * result + ((trcode == null) ? 0 : trcode.hashCode());
		result = prime * result + ((user == null || user.getUserid() == null) ? 0 : user.getUserid().hashCode());
		result = prime * result + ((wcurrency == null || wcurrency.getCurrency() == null) ? 0 : wcurrency.getCurrency().hashCode());
		result = prime * result + ((wsumm == null) ? 0 : wsumm.hashCode());
		result = prime * result + ((wsummPaid == null) ? 0 : wsummPaid.hashCode());
		result = prime * result + ((year == null) ? 0 : year.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinDoc other = (FinDoc) obj;
		if (branch == null) {
			if (other.branch != null)
				return false;
		} else if (!branch.equals(other.branch))
			return false;
		if (childFinDocs == null) {
			if (other.childFinDocs != null)
				return false;
		} else if (!childFinDocs.equals(other.childFinDocs))
			return false;
		if (closed == null) {
			if (other.closed != null)
				return false;
		} else if (!closed.equals(other.closed))
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		if (contract == null) {
			if (other.contract != null)
				return false;
		} else if (!contract.equals(other.contract))
			return false;
		if (cpuDate == null) {
			if (other.cpuDate != null)
				return false;
		} else if (!cpuDate.equals(other.cpuDate))
			return false;
		if (dcurrency == null) {
			if (other.dcurrency != null)
				return false;
		} else if (!dcurrency.equals(other.dcurrency))
			return false;
		if (department == null) {
			if (other.department != null)
				return false;
		} else if (!department.equals(other.department))
			return false;
		if (docDate == null) {
			if (other.docDate != null)
				return false;
		} else if (!docDate.equals(other.docDate))
			return false;
		if (docno == null) {
			if (other.docno != null)
				return false;
		} else if (!docno.equals(other.docno))
			return false;
		if (dsumm == null) {
			if (other.dsumm != null)
				return false;
		} else if (!dsumm.equals(other.dsumm))
			return false;
		if (dsummPaid == null) {
			if (other.dsummPaid != null)
				return false;
		} else if (!dsummPaid.equals(other.dsummPaid))
			return false;
		if (finDocType == null) {
			if (other.finDocType != null)
				return false;
		} else if (!finDocType.equals(other.finDocType))
			return false;
		if (finEntries == null) {
			if (other.finEntries != null)
				return false;
		} else if (!finEntries.equals(other.finEntries))
			return false;
		if (forbuh == null) {
			if (other.forbuh != null)
				return false;
		} else if (!forbuh.equals(other.forbuh))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (info == null) {
			if (other.info != null)
				return false;
		} else if (!info.equals(other.info))
			return false;
		if (invoice == null) {
			if (other.invoice != null)
				return false;
		} else if (!invoice.equals(other.invoice))
			return false;
		if (isMain == null) {
			if (other.isMain != null)
				return false;
		} else if (!isMain.equals(other.isMain))
			return false;
		if (month == null) {
			if (other.month != null)
				return false;
		} else if (!month.equals(other.month))
			return false;
		if (paymentOrder == null) {
			if (other.paymentOrder != null)
				return false;
		} else if (!paymentOrder.equals(other.paymentOrder))
			return false;
		if (parentFindoc == null) {
			if (other.parentFindoc != null)
				return false;
		} else if (parentFindoc.getId() != other.parentFindoc.getId())
			return false;
		if (party == null) {
			if (other.party != null)
				return false;
		} else if (!party.equals(other.party))
			return false;
		if (rate == null) {
			if (other.rate != null)
				return false;
		} else if (!rate.equals(other.rate))
			return false;
		if (refkey == null) {
			if (other.refkey != null)
				return false;
		} else if (!refkey.equals(other.refkey))
			return false;
		if (refkeyMain == null) {
			if (other.refkeyMain != null)
				return false;
		} else if (!refkeyMain.equals(other.refkeyMain))
			return false;
		if (reftyp == null) {
			if (other.reftyp != null)
				return false;
		} else if (!reftyp.equals(other.reftyp))
			return false;
		if (stDocno == null) {
			if (other.stDocno != null)
				return false;
		} else if (!stDocno.equals(other.stDocno))
			return false;
		if (stYear == null) {
			if (other.stYear != null)
				return false;
		} else if (!stYear.equals(other.stYear))
			return false;
		if (storno == null) {
			if (other.storno != null)
				return false;
		} else if (!storno.equals(other.storno))
			return false;
		if (trcode == null) {
			if (other.trcode != null)
				return false;
		} else if (!trcode.equals(other.trcode))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		if (wcurrency == null) {
			if (other.wcurrency != null)
				return false;
		} else if (!wcurrency.equals(other.wcurrency))
			return false;
		if (wsumm == null) {
			if (other.wsumm != null)
				return false;
		} else if (!wsumm.equals(other.wsumm))
			return false;
		if (wsummPaid == null) {
			if (other.wsummPaid != null)
				return false;
		} else if (!wsummPaid.equals(other.wsummPaid))
			return false;
		if (year == null) {
			if (other.year != null)
				return false;
		} else if (!year.equals(other.year))
			return false;
		return true;
	}		

	@Override
	public FinDoc clone() throws CloneNotSupportedException {
		return (FinDoc) super.clone();
	}
}
