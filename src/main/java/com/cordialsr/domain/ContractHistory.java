package com.cordialsr.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cordialsr.domain.deserializer.ContractHistoryDeserializer;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@JsonDeserialize(using = ContractHistoryDeserializer.class)
@Table(name = "contract_history")
public class ContractHistory implements java.io.Serializable, Cloneable {
	
	private static final long serialVersionUID = 1L;
	private Long id;
	private Contract contract;
	private String field;
	private String title;
	private Date updatedDate;
	private User updatedUser;
	private String oldValue;
	private String newValue;
	private Long oldId;
	private Long newId;
	
	public static final String FIELD_SN = "INVSN";

	 
	public ContractHistory () {
		
	}
	
	public ContractHistory(Long id, Contract contract, String field, String title, Date updatedDate, User updatedUser,
			String oldValue, String newValue, Long oldId, Long newId) {
		this.id = id;
		this.contract = contract;
		this.field = field;
		this.title = title;
		this.updatedDate = updatedDate;
		this.updatedUser = updatedUser;
		this.oldValue = oldValue;
		this.newValue = newValue;
		this.oldId = oldId;
		this.newId = newId;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contract_id", nullable = false)
	public Contract getContract() {
		return contract;
	}
	public void setContract(Contract contract) {
		this.contract = contract;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "updated_date", nullable = false, length = 10)
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "updated_user", nullable = false)
	public User getUpdatedUser() {
		return updatedUser;
	}
	public void setUpdatedUser(User updatedUser) {
		this.updatedUser = updatedUser;
	}
	
	@Column(name = "field", length = 10, nullable = false)
	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	@Column(name = "title", length = 45, nullable = false) 
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "old_value", length = 255, nullable = false)
	public String getOldValue() {
		return oldValue;
	}

	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}
	
	@Column(name = "new_value", length = 255, nullable = false)
	public String getNewValue() {
		return newValue;
	}

	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}
	
	@Column(name = "old_id")
	public Long getOldId() {
		return oldId;
	}

	public void setOldId(Long oldId) {
		this.oldId = oldId;
	}

	@Column(name = "new_id")
	public Long getNewId() {
		return newId;
	}

	public void setNewId(Long newId) {
		this.newId = newId;
	}

	@Override
	public ContractHistory clone() throws CloneNotSupportedException {
		return (ContractHistory) super.clone();
	}
	
}
