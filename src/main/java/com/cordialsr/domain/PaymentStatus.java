package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "payment_status")
public class PaymentStatus implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static Integer PSTAT_REGULAR = 1;
	public static Integer PSTAT_OVERDUE = 2;
	public static Integer PSTAT_PROBLEM = 3;
	public static Integer PSTAT_PAID = 4;
	
//	1	REGULAR	По графику
//	2	OVERDUE	Просроченные платежи (до 3-х месяцев)
//	3	PROBLEM	Проблемный (больше 3-х месяцев)
//	4	PAID	Оплачен полностью
	
	private Integer id;
	private String name;
	private String info;

	public PaymentStatus() {
	}

	public PaymentStatus(int id) {
		this.id = id;
	}

	public PaymentStatus(int id, String name, String info) {
		this.id = id;
		this.name = name;
		this.info = info;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "info")
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	// ****************************************************************************************************
	
	@Override
	public String toString() {
		return "PaymentStatus [id=" + id + ", name=" + name + ", info=" + info + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentStatus other = (PaymentStatus) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public PaymentStatus clone() throws CloneNotSupportedException {
		return (PaymentStatus) super.clone();
	}
	
}
