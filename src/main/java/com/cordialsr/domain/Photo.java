package com.cordialsr.domain;

import java.io.Serializable;
import java.sql.Blob;
import java.sql.SQLException;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.DatatypeConverter;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "photo")
public class Photo implements Serializable, Cloneable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Blob photo;
	private Party party;
	
	public Photo() {}
	
	public Photo(Long id) {
		this.id = id;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonIgnore
	@Column(name = "photo")
	public Blob getPhoto() {
		return photo;
	}

	public void setPhoto(Blob photo) {
		this.photo = photo;
	}

	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "photo", cascade = CascadeType.ALL)
	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}

	@JsonIgnore
	@Transient
	public String getPhotoString() throws SQLException {

		String str = "";
		if(this.photo != null) {
			Integer blobLength = (int) this.photo.length();  
			byte[] blobAsBytes = this.photo.getBytes(1, blobLength);
			str = DatatypeConverter.printBase64Binary(blobAsBytes);
			photo.free();
		}
		return str;
	
	}

	@Override
	public Photo clone() throws CloneNotSupportedException {
		return (Photo) super.clone();
	}
	
}
