package com.cordialsr.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "crm_reason")
public class CrmReason implements java.io.Serializable, Cloneable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String code;
	private String description;
	
	public CrmReason() {
	}

	public CrmReason(String code, String description) {
		this.code = code;
		this.description = description;
	}

	@Id
	@Column(name = "code")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public CrmReason clone() throws CloneNotSupportedException {
		return (CrmReason) super.clone();
	}
}
