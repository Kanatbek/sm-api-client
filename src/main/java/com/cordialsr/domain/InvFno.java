package com.cordialsr.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "inv_fno", 
	   uniqueConstraints = @UniqueConstraint(columnNames = {"inventory", "fno"}))
public class InvFno implements Serializable, Cloneable {

private static final long serialVersionUID = 1L;
	
	private Long id;
	private Inventory inventory;
	private Integer fno;
	
	public InvFno() {
		
	}
	
	public InvFno(Inventory inventory, Integer fno) {
		this.inventory = inventory;
		this.fno = fno;
	}
	
	public InvFno(Long id, Inventory inventory, Integer fno) {
		this.id = id;
		this.inventory = inventory;
		this.fno = fno;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventory", nullable = false)
	public Inventory getInventory() {
		return inventory;
	}
	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}
	
	@Column(name = "fno", nullable = false)
	public Integer getFno() {
		return fno;
	}
	public void setFno(Integer fno) {
		this.fno = fno;
	}

	@Override
	public InvFno clone() throws CloneNotSupportedException {
		return (InvFno) super.clone();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((inventory == null || inventory.getId() == null) ? 0 : inventory.getId().hashCode());
		result = prime * result + ((fno == null) ? 0 : fno.hashCode());
		return result;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvFno other = (InvFno) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (inventory == null) {
			if (other.inventory != null)
				return false;
		} else if (inventory.getId() != other.inventory.getId())
			return false;
		if (fno == null) {
			if (other.fno != null)
				return false;
		} else if (!fno.equals(other.fno))
			return false;
		return true;
	}
	
}
