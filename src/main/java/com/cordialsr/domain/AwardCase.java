package com.cordialsr.domain;
// 03.08.2017 11:54:04 by stalbekr@gmail.com

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "award_case")
public class AwardCase implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String CASE_SUMM = "SUMM";
	public static final String CASE_UNIT = "UNIT";
	
	private String code;
	private String name;
	private String info;

	public AwardCase() {
	}
	
	public AwardCase(String code) {
		this.code = code;
	}

	public AwardCase(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public AwardCase(String code, String name, String info) {
		this.code = code;
		this.name = name;
		this.info = info;
	}

	@Id

	@Column(name = "code", unique = true, nullable = false, length = 4)
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "name", nullable = false, length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "info")
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
}
