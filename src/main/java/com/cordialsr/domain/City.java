package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "city")
public class City implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Oblast oblast;
	private String name;
	private String phoneCode;
	
	public City() {
	}
	
	public City(Integer id) {
		this.id = id;
	}

	public City(Oblast oblast, String name) {
		this.oblast = oblast;
		this.name = name;
	}

	public City(Oblast oblast, String name, String phoneCode) {
		this.oblast = oblast;
		this.name = name;
		this.phoneCode = phoneCode;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "oblast_id")
	public Oblast getOblast() {
		return this.oblast;
	}

	public void setOblast(Oblast oblast) {
		this.oblast = oblast;
	}

	@Column(name = "name", nullable = false, length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "phone_code", length = 10)
	public String getPhoneCode() {
		return this.phoneCode;
	}

	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}

	// ****************************************************************************************************
	
	@Override
	public String toString() {
		return "City [id=" + id + ", oblast=" + oblast + ", name=" + name + ", phoneCode=" + phoneCode + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((oblast == null || oblast.getId() == null) ? 0 : oblast.getId().hashCode());
		result = prime * result + ((phoneCode == null) ? 0 : phoneCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		City other = (City) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (oblast == null) {
			if (other.oblast != null)
				return false;
		} else if (!oblast.getId().equals(other.oblast.getId()))
			return false;
		if (phoneCode == null) {
			if (other.phoneCode != null)
				return false;
		} else if (!phoneCode.equals(other.phoneCode))
			return false;
		return true;
	}

	@Override
	public City clone() throws CloneNotSupportedException {
		return (City) super.clone();
	}

}
