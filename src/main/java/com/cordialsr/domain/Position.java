package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "position")
public class Position implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final int POS_DEALER = 1;
	public static final int POS_DEMOSEC = 2;
	public static final int POS_MANAGER = 3;
	public static final int POS_COORDINATOR = 4;
	public static final int POS_DIRECTOR = 5;
	public static final int POS_CAREMAN = 11;
	public static final int POS_FITTER = 28;
	public static final int POS_CHIEF_COORDINATOR = 30;
	public static final int POS_COLLECTOR = 31;
	
//	1	Dealer	Дилер
//	2	Demo-Secretary	Демо-Секретарь
//	3	Manager	Менеджер
//	4	Coordinator	Кординатор
//	5	Director	Директор
//	11	Careman	Careman
	
//	'6', 'CEO', 'Генеральный Директор'
//	'7', 'CIO', 'Руководитель IT'
//	'8', 'Software Developer', 'Программист разработчик'
//	'9', 'Graphic Designer', 'Дизайнер'
//	'10', 'IT Specialist', 'Системный администратор'
//	'12', 'Office-manager', 'Офис-менеджер'
//	'13', 'CFO', 'Финансовый Директор'
//	'14', 'Engineer', 'Инженер'
//	'15', 'Logistics Operator', 'Логист'
//	'16', 'Courier', 'Курьер'
//	'17', 'Call-Center Operator', 'Оператор Call-Center'
//	'18', 'Lawyer', 'Юрист'
//	'19', 'Trainer', 'Обучатель'
//	'20', 'Завхоз', 'Завхоз'
//	'21', 'Повар', 'Повар'
//	'22', 'Снабженец', 'Снабженец'
//	'23', 'Технический персонал', NULL
//	'24', 'Водитель', NULL
//	'25', 'Ассистент CEO', NULL
//	'26', 'Ассистент', NULL
//	'27', 'Охранник', NULL
//	'28', 'Установщик', NULL
//	'29', 'Финансист', NULL

	
	private Integer id;
	private String name;
	private String description;
	private Department department;
	
	public Position() {
	}

	public Position(String name) {
		this.name = name;
	}

	public Position(String name, String description, Department department) {
		this.name = name;
		this.description = description;
		this.department = department;
	}

	public Position(Integer id) {
			this.id = id;	
		}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description")
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "department_id")
	public Department getDepartment() {
		return this.department;
	}
	
	public void setDepartment(Department department) {
		this.department = department;
	}
	
	// ****************************************************************************************************
	
	@Override
	public String toString() {
		return "Position [id=" + id + ", name=" + name + ", description=" + description + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public Position clone() throws CloneNotSupportedException {
		return (Position) super.clone();
	}

	
}
