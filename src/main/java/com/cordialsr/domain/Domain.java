package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.cordialsr.domain.security.RoleDomainPermission;

@Entity
@Table(name = "domain")
public class Domain implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String code;
	private String name;
	private String description;
	private Set<RoleDomainPermission> roleDomainPermissions = new HashSet<>(0);

	public Domain() {
	}

	public Domain(String code, String name, String description, Set<RoleDomainPermission>  roleDomainPermissions) {
		this.code = code;
		this.name = name;
		this.description = description;
		this.roleDomainPermissions = roleDomainPermissions;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "code", length = 10)
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "name", length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description", length = 100)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "domain")
	public Set<RoleDomainPermission> getRoleDomainPermissions() {
		return this.roleDomainPermissions;
	}

	public void setRoleDomainPermissions(Set<RoleDomainPermission> roleDomainPermissions) {
		this.roleDomainPermissions = roleDomainPermissions;
	}

}
