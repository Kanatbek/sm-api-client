package com.cordialsr.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "sm_visit")
public class SmVisit implements Cloneable {

	
	private Long id;
	private Employee employee;
	private Party party;
	private Date inTime;
	private Date outTime;
	private Date inDate;
	private BigDecimal longitude;
	private BigDecimal latitude;
	
	public SmVisit() {
		
	}
	
	public SmVisit(Long id) {
		this.id = id;
	}
	
	public SmVisit(Long id, Employee employee, Party party, Date inTime, Date outTime, BigDecimal longitude, BigDecimal latitude, Date inDate) {
		this.id = id;
		this.employee = employee;
		this.party = party;
		this.inTime = inTime;
		this.outTime = outTime;
		this.longitude = longitude;
		this.latitude = latitude;
		this.inDate = inDate;
	}
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empl_id")
	public Employee getEmployee() {
		return employee;
	}
	
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party_id")
	public Party getParty() {
		return party;
	}
	
	public void setParty(Party party) {
		this.party = party;
	}
	
	@Temporal(TemporalType.TIME)
	@Column(name = "in_time", length = 10)
	public Date getInTime() {
		return inTime;
	}

	public void setInTime(Date inTime) {
		this.inTime = inTime;
	}
	
	@Temporal(TemporalType.TIME)
	@Column(name = "out_time", length = 10)
	public Date getOutTime() {
		return outTime;
	}
	
	public void setOutTime(Date outTime) {
		this.outTime = outTime;
	}
	
	@Column(name = "longitude")
	public BigDecimal getLongitude() {
		return longitude;
	}
	
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}
	
	@Column(name = "latitude")
	public BigDecimal getLatitude() {
		return latitude;
	}
	
	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}
	
	@Temporal(TemporalType.DATE)
	@Column(name = "in_date", length = 10)
	public Date getInDate() {
		return inDate;
	}
	
	public void setInDate(Date inDate) {
		this.inDate = inDate;
	}

	@Override
	public SmVisit clone() throws CloneNotSupportedException {
		return (SmVisit) super.clone();
	}
}
