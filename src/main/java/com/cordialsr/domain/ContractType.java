package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "contract_type")
public class ContractType implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Branch branch;
	private City city;
	private Company company;
	private Country country;
	private Region region;
	private String name;
	private String info;
	private String scope;
	private Integer maxItems;
	private Inventory inventory;
	private Boolean os;
	
	public ContractType() {
	}

	public ContractType(Integer brId) {
		this.id = brId;
	}

	public ContractType(Branch branch, City city, Company company, Country country, Region region, String name,
			String info, String scope, Integer maxItems, 
			Inventory inventory, Boolean os) {
		this.branch = branch;
		this.city = city;
		this.company = company;
		this.country = country;
		this.region = region;
		this.name = name;
		this.info = info;
		this.scope = scope;
		this.maxItems = maxItems;
		this.inventory = inventory;
		this.os = os;
	}

		
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch")
	public Branch getBranch() {
		return this.branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "city")
	public City getCity() {
		return this.city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company")
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "country")
	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "region")
	public Region getRegion() {
		return this.region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	@Column(name = "name", length = 100)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "info")
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@Column(name = "scope", nullable = false, length = 45)
	public String getScope() {
		return this.scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	@Column(name = "max_items")
	public Integer getMaxItems() {
		return this.maxItems;
	}

	public void setMaxItems(Integer maxItems) {
		this.maxItems = maxItems;
	}

	@Column(name = "os")
	public Boolean getOs() {
		return os;
	}

	public void setOs(Boolean os) {
		this.os = os;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventory_id")
	public Inventory getInventory() {
		return inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}


	// ****************************************************************************************************
	
	
	@Override
	public String toString() {
		return "ContractType [id=" + id + ", branch=" + branch + ", city=" + city + ", company=" + company
				+ ", country=" + country + ", region=" + region + ", name=" + name + ", info=" + info + ", scope="
				+ scope + ", maxItems=" + maxItems + ", inventory = " + inventory + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((branch == null || branch.getId() == null) ? 0 : branch.getId().hashCode());
		result = prime * result + ((city == null || city.getId() == null) ? 0 : city.getId().hashCode());
		result = prime * result + ((company == null || company.getId() == null) ? 0 : company.getId().hashCode());
		result = prime * result + ((country == null || country.getId() == null) ? 0 : country.getId().hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((maxItems == null) ? 0 : maxItems.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((region == null || region.getId() == null) ? 0 : region.getId().hashCode());
		result = prime * result + ((scope == null) ? 0 : scope.hashCode());
		result = prime * result + ((inventory == null || inventory.getId() == null) ? 0 : inventory.getId().hashCode());
		
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContractType other = (ContractType) obj;
		if (branch == null) {
			if (other.branch != null)
				return false;
		} else if (!branch.getId().equals(other.branch.getId()))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.getId().equals(other.city.getId()))
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.getId().equals(other.company.getId()))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.getId().equals(other.country.getId()))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (info == null) {
			if (other.info != null)
				return false;
		} else if (!info.equals(other.info))
			return false;
		if (maxItems == null) {
			if (other.maxItems != null)
				return false;
		} else if (!maxItems.equals(other.maxItems))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (region == null) {
			if (other.region != null)
				return false;
		} else if (!region.getId().equals(other.region.getId()))
			return false;
		if (scope == null) {
			if (other.scope != null)
				return false;
		} else if (!scope.equals(other.scope))
			return false;
		if (inventory == null) {
			if (other.inventory != null)
				return false;
		} else if (!inventory.getId().equals(other.inventory.getId()))
			return false;
		
		return true;
	}

	@Override
	public ContractType clone() throws CloneNotSupportedException {
		return (ContractType) super.clone();
	}
	
	
}
