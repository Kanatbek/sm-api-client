package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "unit")
public class Unit implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String UNIT_CANTIMETER = "CM";
	public static final String UNIT_LITER = "L";
	public static final String UNIT_METER = "M";
	public static final String UNIT_SQUARE_METER = "M2";
	public static final String UNIT_CUBIC_METER = "M3";
	public static final String UNIT_PCS = "PCS";
	
//	CM	Сантиметр	Длина
//	L	Литр	Объем
//	M	Метр	Длина
//	M2	Квадратный Метр	Площадь
//	M3	Кубический Метр	Объем
//	PCS	Штук	Количество
	
	private String name;
	private String fullName;
	private String info;
	
	public Unit() {
	}

	public Unit(String name) {
		this.name = name;
	}

	public Unit(String name, String fullName, String info) {
		this.name = name;
		this.fullName = fullName;
		this.info = info;
	}

	@Id

	@Column(name = "name", unique = true, nullable = false, length = 5)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "full_name", length = 100)
	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@Column(name = "info")
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	

	// ****************************************************************************************************
	
	@Override
	public String toString() {
		return "Unit [name=" + name + ", fullName=" + fullName + ", info=" + info + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fullName == null) ? 0 : fullName.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Unit other = (Unit) obj;
		if (fullName == null) {
			if (other.fullName != null)
				return false;
		} else if (!fullName.equals(other.fullName))
			return false;
		if (info == null) {
			if (other.info != null)
				return false;
		} else if (!info.equals(other.info))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public Unit clone() throws CloneNotSupportedException {
		return (Unit) super.clone();
	}
	
}
