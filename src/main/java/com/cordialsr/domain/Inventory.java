package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.cordialsr.domain.deserializer.InventoryDeserializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "inventory", uniqueConstraints = @UniqueConstraint(columnNames = "code"))
@JsonDeserialize(using = InventoryDeserializer.class)
public class Inventory implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Company company;
	private InvMainCategory invMainCategory;
	private InvSubCategory invSubCategory;
	private Manufacturer manufacturer;
	private Unit unit;
	private String name;
	private String model;
	private String code;
	private BigDecimal volumeCbm;
	private BigDecimal weightKg;
	
	private List<Inventory> spares = new ArrayList<>();
	private List<Inventory> parents = new ArrayList<>();
	private List<InvFno> invFno = new ArrayList<>();
	
	public Inventory() {
	}
	
	public Inventory(int id) {
		this.id = id;
	}

	
	public Inventory(int id , InvMainCategory invMainCategory) {
		this.id = id;
		this.invMainCategory = invMainCategory;
	}
	
	public Inventory(Company company, InvMainCategory invMainCategory, InvSubCategory invSubCategory,
			Manufacturer manufacturer, Unit unit, String name, String code) {
		this.company = company;
		this.invMainCategory = invMainCategory;
		this.invSubCategory = invSubCategory;
		this.manufacturer = manufacturer;
		this.unit = unit;
		this.name = name;
		this.code = code;
	}

	public Inventory(Company company, InvMainCategory invMainCategory, InvSubCategory invSubCategory,
			Manufacturer manufacturer, Unit unit, String name, String model, String code,
			BigDecimal volumeCbm, BigDecimal weightKg) {
		this.company = company;
		this.invMainCategory = invMainCategory;
		this.invSubCategory = invSubCategory;
		this.manufacturer = manufacturer;
		this.unit = unit;
		this.name = name;
		this.model = model;
		this.code = code;
		this.volumeCbm = volumeCbm;
		this.weightKg = weightKg;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company", nullable = false)
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "main_category_id", nullable = false)
	public InvMainCategory getInvMainCategory() {
		return this.invMainCategory;
	}

	public void setInvMainCategory(InvMainCategory invMainCategory) {
		this.invMainCategory = invMainCategory;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sub_category_id", nullable = false)
	public InvSubCategory getInvSubCategory() {
		return this.invSubCategory;
	}

	public void setInvSubCategory(InvSubCategory invSubCategory) {
		this.invSubCategory = invSubCategory;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "manufacturer", nullable = false)
	public Manufacturer getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(Manufacturer manufacturer) {
		this.manufacturer = manufacturer;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "unit", nullable = false)
	public Unit getUnit() {
		return this.unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	@Column(name = "name", nullable = false, length = 100)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "model", length = 60)
	public String getModel() {
		return this.model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	@Column(name = "code", unique = true, nullable = false, length = 20)
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "volume_cbm", precision = 12, scale = 4)
	public BigDecimal getVolumeCbm() {
		return this.volumeCbm;
	}

	public void setVolumeCbm(BigDecimal volumeCbm) {
		this.volumeCbm = volumeCbm;
	}

	@Column(name = "weight_kg", precision = 12, scale = 4)
	public BigDecimal getWeightKg() {
		return this.weightKg;
	}

	public void setWeightKg(BigDecimal weightKg) {
		this.weightKg = weightKg;
	}

	// ************************************************************************************************************
	
	@JsonIgnore	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "inventory_spares", joinColumns = {
			@JoinColumn(name = "parent", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "spare", nullable = false, updatable = false) })
	public List<Inventory> getSpares() {
		return spares;
	}

	public void setSpares(List<Inventory> spares) {
		this.spares = spares;
	}

	
	@JsonIgnore	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "inventory_spares", joinColumns = {
			@JoinColumn(name = "spare", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "parent", nullable = false, updatable = false) })
	public List<Inventory> getParents() {
		return parents;
	}

	public void setParents(List<Inventory> parents) {
		this.parents = parents;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "inventory", cascade = CascadeType.ALL, orphanRemoval = true)
	public List<InvFno> getInvFno() {
		return invFno;
	}

	public void setInvFno(List<InvFno> invFno) {
		this.invFno = invFno;
	}
	
	// ************************************************************************************************************
		
	@Override
	public Inventory clone() throws CloneNotSupportedException {
		return (Inventory) super.clone();
	}

	@Override
	public String toString() {
		return "Inventory [id=" + id + ", company=" + company + ", invMainCategory=" + invMainCategory
				+ ", invSubCategory=" + invSubCategory + ", manufacturer="
				+ manufacturer + ", unit=" + unit + ", name=" + name + ", model=" + model + ", code=" + code
				+ ", volumeCbm=" + volumeCbm + ", weightKg=" + weightKg + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((company == null || company.getId() == null) ? 0 : company.getId().hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((invMainCategory == null || invMainCategory.getId() == null) ? 0 : invMainCategory.getId().hashCode());
		result = prime * result + ((invSubCategory == null || invSubCategory.getId() == null) ? 0 : invSubCategory.getId().hashCode());
		result = prime * result + ((manufacturer == null || manufacturer.getName() == null) ? 0 : manufacturer.getName().hashCode());
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((unit == null || unit.getName() == null) ? 0 : unit.getName().hashCode());
		result = prime * result + ((volumeCbm == null) ? 0 : volumeCbm.hashCode());
		result = prime * result + ((weightKg == null) ? 0 : weightKg.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Inventory other = (Inventory) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (invMainCategory == null) {
			if (other.invMainCategory != null)
				return false;
		} else if (!invMainCategory.equals(other.invMainCategory))
			return false;
		if (invSubCategory == null) {
			if (other.invSubCategory != null)
				return false;
		} else if (!invSubCategory.equals(other.invSubCategory))
			return false;
		if (manufacturer == null) {
			if (other.manufacturer != null)
				return false;
		} else if (!manufacturer.equals(other.manufacturer))
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.equals(other.unit))
			return false;
		if (volumeCbm == null) {
			if (other.volumeCbm != null)
				return false;
		} else if (!volumeCbm.equals(other.volumeCbm))
			return false;
		if (weightKg == null) {
			if (other.weightKg != null)
				return false;
		} else if (!weightKg.equals(other.weightKg))
			return false;
		return true;
	}

	
}
