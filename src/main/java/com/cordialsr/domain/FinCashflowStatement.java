package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "fin_cashflow_statement")
public class FinCashflowStatement implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final Integer CF_STA_MONTHLY_PAYMENT = 42;
	public static final Integer CF_STA_FIRST_PAYMENT = 43;
	public static final Integer CF_EXCHANGE = 61;

//	42	OPR	10	ВЗНОСЫ	Деньги, полученные от клиентов
//	43	OPR	10	ПЕРВОНАЧАЛЬНЫЙ	Деньги, полученные от клиентов от продажи

	public static final Integer CF_STA_SALARY_1 = 1;
	public static final Integer CF_STA_PREMI_68 = 68;
	public static final Integer CF_STA_BONUS_69 = 69;
	public static final Integer CF_STA_AVANS_59 = 59;

//	1	OPR	ЗАРПЛАТА	ЗАРПЛАТА
//	68	OPR	ПРЕМИЯ	Премия от продаж, выполненной работы
//	69	OPR	БОНУС	Бонусы от продаж, выполненного плана
//	59	OPR	АВАНС	Авансы выданные офисным сотрудникам в подотчет
	
	public static final Integer CF_STA_DISCOUNT_STAFF_70 = 70;
	public static final Integer CF_STA_DISCOUNT_COMPANY_71 = 71;
	
//	'70', 'OPR', 'Скидка Дилера', 'Скидка клиенту со счета Сотрудника', '0', '0'
//	'71', 'OPR', 'Скидка Компании', 'Скидка клиенту от Компании', '0', '0'

	
	private Integer id;
	private FinActivityType finActType;
	private String name;
	private String info;
	
	public FinCashflowStatement() {
	}

	public FinCashflowStatement(Integer id) {
		this.id = id;
	}

	public FinCashflowStatement(FinActivityType finOperType) {
		this.finActType = finOperType;
	}

	public FinCashflowStatement(FinActivityType finOperType, String name) {
		this.finActType = finOperType;
		this.name = name;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ACT_TYPE", nullable = false)
	public FinActivityType getFinActType() {
		return finActType;
	}

	public void setFinActType(FinActivityType finActType) {
		this.finActType = finActType;
	}
	
	@Column(name = "name", length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "info", length = 255)
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@Override
	public String toString() {
		return "FinCashflowStatement [id=" + id + ", finOperType=" + finActType + ", name=" + name
				+ ", info=" + info + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((finActType == null || finActType.getCode() == null) ? 0 : finActType.getCode().hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinCashflowStatement other = (FinCashflowStatement) obj;
		if (finActType == null) {
			if (other.finActType != null)
				return false;
		} else if (!finActType.equals(other.finActType))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (info == null) {
			if (other.info != null)
				return false;
		} else if (!info.equals(other.info))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public FinCashflowStatement clone() throws CloneNotSupportedException {
		return (FinCashflowStatement) super.clone();
	}
	
}
