package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "inv_sub_category")
public class InvSubCategory implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final Integer SUBCAT_WATER_PURIFYING_SYSTEM = 1;
	public static final Integer SUBCAT_AIR_PURIFYING_SYSTEM = 2;
	public static final Integer SUBCAT_SPARE_PART = 3;
	public static final Integer SUBCAT_FILTER = 4;
	public static final Integer SUBCAT_ACCESSORY = 5;
	public static final Integer SUBCAT_CUISINE_SET = 6;
	public static final Integer SUBCAT_OTHER = 7;
	public static final Integer SUBCAT_VACUUM_CLEANER = 8;
	
//	1	WATER PURIFYING SYSTEM	Сисетма Очистки Воды	1
//	2	AIR PURIFYING SYSTEM	Система Очистки Воздуха	1
//	3	SPARE PART	Запасная часть	2
//	4	FILTER	Фильтр	2
//	5	ACCESSORY	Аксессуар	3
//	6	CUISINE SET	Кухонный Набор	4
//	7	OTHER	Прочие Товары	4
//	8	HOUSE CLEANING SYSTEM	Пылесос	1
				
	
	private Integer id;
	private InvMainCategory invMainCategory;
	private String name;
	private String info;
	
	public InvSubCategory() {
	}

	public InvSubCategory(InvMainCategory invMainCategory) {
		this.invMainCategory = invMainCategory;
	}

	public InvSubCategory(InvMainCategory invMainCategory, String name, String info) {
		this.invMainCategory = invMainCategory;
		this.name = name;
		this.info = info;
	}

	public InvSubCategory(Integer id) {
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "main_category_id", nullable = false)
	public InvMainCategory getInvMainCategory() {
		return this.invMainCategory;
	}

	public void setInvMainCategory(InvMainCategory invMainCategory) {
		this.invMainCategory = invMainCategory;
	}

	@Column(name = "name", length = 60)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "info")
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	// ****************************************************************************************************
	
	@Override
	public String toString() {
		return "InvSubCategory [id=" + id + ", invMainCategory=" + invMainCategory + ", name=" + name + ", info=" + info
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((invMainCategory == null || invMainCategory.getId() == null) ? 0 : invMainCategory.getId().hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvSubCategory other = (InvSubCategory) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (info == null) {
			if (other.info != null)
				return false;
		} else if (!info.equals(other.info))
			return false;
		if (invMainCategory == null) {
			if (other.invMainCategory != null)
				return false;
		} else if (!invMainCategory.equals(other.invMainCategory))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public InvSubCategory clone() throws CloneNotSupportedException {
		return (InvSubCategory) super.clone();
	}

	
}
