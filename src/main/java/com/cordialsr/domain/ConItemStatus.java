package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "con_item_status")
public class ConItemStatus implements java.io.Serializable, Cloneable {

	public static final Integer STATUS_NEW = 1; 
	public static final Integer STATUS_ISSUED = 2;
	public static final Integer STATUS_INSTALLED = 3;
	public static final Integer STATUS_RETURNED = 4;
	
//	1	NEW	Новый
//	2	ISSUED	Выдан
//	3	INSTALLED	Установлен
//	4	RETURNED    Возврат
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	private String info;

	public ConItemStatus() {
	}

	public ConItemStatus(int id) {
		this.id = id;
	}

	public ConItemStatus(int id, String name, String info) {
		this.id = id;
		this.name = name;
		this.info = info;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "info")
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@Override
	public ConItemStatus clone() throws CloneNotSupportedException {
		return (ConItemStatus) super.clone();
	}

	
}
