package com.cordialsr.domain;
// Generated 23.05.2017 12:18:03 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cordialsr.domain.deserializer.SmServiceDeserializer;
import com.cordialsr.domain.reducer.UserReducer;
import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "sm_service")
@JsonDeserialize(using = SmServiceDeserializer.class)
public class SmService implements Cloneable {
	

	public static final String SERVICE_TYPE_ZAMF = "ZAMF";
	public static final String SERVICE_TYPE_PROF = "PROF";
	public static final String SERVICE_TYPE_REMT = "REMT";
	
	
	private Long id;
	private Company company;
	private Branch branch;
	private String type;
	private Date sdate;
	private Date stime;
	private Date itime;
	private String caremanFio;
	private Boolean storno;
	private String info;
	private Contract contract;
	private Employee careman;
	private Party customer;
	private BigDecimal cost;
	private BigDecimal discount;
	private BigDecimal summ;
	private BigDecimal paid;
	private Currency currency;
	private BigDecimal premi;
	private Currency premiCurrecny;
	private Inventory inventory;
	private String refkey;
	private User userAdded;
	private Boolean verified;
	private Integer mark;
	private SmEnquiry smEnquiry;
	private String serviceNumber;
	private Boolean overdue;
	private Boolean isEarly;
	private SmVisit smVisit;
	
	private Set<SmServiceItem> smServiceItems = new HashSet<>();
	private List<SmCall> smCalls = new ArrayList<>();
	
	public SmService() {
	}
	
	public SmService(Long id) {
		this.id = id;
	}

	public SmService(Long id, Company company, Branch branch, String type, Date sdate, Date stime, Date itime,
			String caremanFio, Boolean storno, Set<SmServiceItem> smServiceItems, String info, Contract contract,
			Employee careman, Party customer, BigDecimal cost, BigDecimal discount, BigDecimal summ, BigDecimal paid,
			Currency currency, BigDecimal premi, Currency premiCurrecny, Inventory inventory, String refkey, 
			User userAdded, Boolean verified, String serviceNumber, Integer mark, SmEnquiry smEnquiry, Boolean overdue, Boolean isEarly, SmVisit smVisit) {
		this.id = id;
		this.company = company;
		this.branch = branch;
		this.type = type;
		this.sdate = sdate;
		this.stime = stime;
		this.itime = itime;
		this.caremanFio = caremanFio;
		this.storno = storno;
		this.smServiceItems = smServiceItems;
		this.info = info;
		this.contract = contract;
		this.careman = careman;
		this.customer = customer;
		this.cost = cost;
		this.discount = discount;
		this.summ = summ;
		this.paid = paid;
		this.currency = currency;
		this.premi = premi;
		this.premiCurrecny = premiCurrecny;
		this.inventory = inventory;
		this.refkey = refkey;
		this.userAdded = userAdded;
		this.verified = verified;
		this.serviceNumber = serviceNumber;
		this.mark = mark;
		this.smEnquiry = smEnquiry;
		this.overdue = overdue;
		this.isEarly = isEarly;
		this.smVisit = smVisit;
	}
	

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "refkey")
	public String getRefkey() {
		return refkey;
	}

	public void setRefkey(String refkey) {
		this.refkey = refkey;
	}

	@Column(name = "info")
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}


	@Column(name = "TYPE", length = 45)
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "SDATE", length = 10)
	public Date getSdate() {
		return this.sdate;
	}

	public void setSdate(Date sdate) {
		this.sdate = sdate;
	}

	@Temporal(TemporalType.TIME)
	@Column(name = "STIME", length = 10)
	public Date getStime() {
		return stime;
	}

	public void setStime(Date stime) {
		this.stime = stime;
	}

	
	@Temporal(TemporalType.TIME)
	@Column(name = "ITIME", length = 10)
	public Date getItime() {
		return itime;
	}

	public void setItime(Date itime) {
		this.itime = itime;
	}
	
	@Column(name = "CAREMAN_FIO", length = 100)
	public String getCaremanFio() {
		return this.caremanFio;
	}

	public void setCaremanFio(String caremanFio) {
		this.caremanFio = caremanFio;
	}

	@Column(name = "STORNO")
	public Boolean getStorno() {
		return this.storno;
	}

	public void setStorno(Boolean storno) {
		this.storno = storno;
	}

	//@JsonIgnore
	//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "smService", cascade = CascadeType.MERGE, orphanRemoval = true)
	public Set<SmServiceItem> getSmServiceItems() {
		return this.smServiceItems;
	}

	public void setSmServiceItems(Set<SmServiceItem> smServiceItems) {
		this.smServiceItems = smServiceItems;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CONTRACT")
	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "careman")
	public Employee getCareman() {
		return careman;
	}

	public void setCareman(Employee careman) {
		this.careman = careman;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "customer")
	public Party getCustomer() {
		return customer;
	}

	public void setCustomer(Party customer) {
		this.customer = customer;
	}

	@Column(name = "cost", precision = 21, scale = 12)
	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	@Column(name = "discount", precision = 21, scale = 12)
	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	@Column(name = "summ", precision = 21, scale = 12)
	public BigDecimal getSumm() {
		return summ;
	}

	public void setSumm(BigDecimal summ) {
		this.summ = summ;
	}

	@Column(name = "paid", precision = 21, scale = 12)
	public BigDecimal getPaid() {
		return paid;
	}

	public void setPaid(BigDecimal paid) {
		this.paid = paid;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currency")
	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	@Column(name = "premi", precision = 21, scale = 12)
	public BigDecimal getPremi() {
		return premi;
	}

	public void setPremi(BigDecimal premi) {
		this.premi = premi;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "premi_currency")
	public Currency getPremiCurrecny() {
		return premiCurrecny;
	}

	public void setPremiCurrecny(Currency premiCurrecny) {
		this.premiCurrecny = premiCurrecny;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company")
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch")
	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventory")
	public Inventory getInventory() {
		return inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_added", nullable = false)
	public User getUserAdded() {
		return UserReducer.reduceMax(this.userAdded);
	}

	public void setUserAdded(User userAdded) {
		this.userAdded = userAdded;
	}


	@Column(name = "verified")
	public Boolean getVerified() {
		return verified;
	}
	public void setVerified(Boolean verified) {
		this.verified = verified;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "service")
	public List<SmCall> getSmCalls() {
		return smCalls;
	}

	public void setSmCalls(List<SmCall> smCalls) {
		this.smCalls = smCalls;
	}

	@Column(name = "service_number")
	public String getServiceNumber() {
		return serviceNumber;
	}

	public void setServiceNumber(String serviceNumber) {
		this.serviceNumber = serviceNumber;
	}

	@Column(name = "mark")
	public Integer getMark() {
		return mark;
	}

	public void setMark(Integer mark) {
		this.mark = mark;
	}

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "enquiry_id", nullable = false)
	public SmEnquiry getSmEnquiry() {
		return smEnquiry;
	}

	public void setSmEnquiry(SmEnquiry smEnquiry) {
		this.smEnquiry = smEnquiry;
	}

	@Column(name = "overdue")
	public Boolean getOverdue() {
		return overdue;
	}

	public void setOverdue(Boolean overdue) {
		this.overdue = overdue;
	}

	@Column(name = "early")
	public Boolean getIsEarly() {
		return isEarly;
	}

	public void setIsEarly(Boolean isEarly) {
		this.isEarly = isEarly;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sm_visit")
	public SmVisit getSmVisit() {
		return smVisit;
	}
	 
	public void setSmVisit(SmVisit smVisit) {
		this.smVisit = smVisit;
	}

	@Override
	public SmService clone() throws CloneNotSupportedException {
		return (SmService) super.clone();
	}
	
	
}
