package com.cordialsr.domain.factory;

import com.cordialsr.domain.StockIn;
import com.cordialsr.domain.StockOut;

public class StockFactory {
	
	public static StockOut fromStockInToOut(StockIn si)throws Exception {
		try {
			StockOut so = new StockOut();
			if (si.getCompany() != null) 
				so.setCompany(si.getCompany());
			if (si.getBranch() != null)
				so.setBranch(si.getBranch());
			if (si.getBroken() != null)
				so.setBroken(si.getBroken());
			if (si.getDateIn() != null)
				so.setDateOut(si.getDateIn());
			if (si.getDcurrency() != null)
				so.setDcurrency(si.getDcurrency());
			if (si.getDsumm() != null)
				so.setDsumm(si.getDsumm());
			if (si.getGenStatus() != null)
				so.setGenStatus(si.getGenStatus());
			if (si.getGlCode() != null)
				so.setGlCode(si.getGlCode());
			if (si.getId() != null)
				so.setId(si.getId());
			if (si.getInventory() != null)
				so.setInventory(si.getInventory());
			if (si.getInvoiceItem() != null)
				so.setInvoiceItem(si.getInvoiceItem());
			if (si.getQuantity() != null)
				so.setQuantity(si.getQuantity());
			if (si.getRate() != null)
				so.setRate(si.getRate());
			if (si.getWsumm() != null)
				so.setWsumm(si.getWsumm());
			if (si.getWcurrency() != null)
				so.setWcurrency(si.getWcurrency());
			if (si.getUnit() != null)
				so.setUnit(si.getUnit());
			if (si.getSerialNumber() != null)
				so.setSerialNumber(si.getSerialNumber());
			if (si.getRefkey() != null)
				so.setRefkey(si.getRefkey());
			
			return so;
		} catch(Exception ex) {
			throw ex;
		}
	}
	public static StockIn fromStockOutToIn(StockOut so)throws Exception {
		try {
			StockIn si = new StockIn();
			if (so.getCompany() != null)
				si.setCompany(so.getCompany());
			if (so.getBranch() != null)
				si.setBranch(so.getBranch());
			if (so.getBroken() != null)
				si.setBroken(so.getBroken());
			if (so.getDateOut() != null)
				si.setDateIn(so.getDateOut());
			if (so.getDcurrency() != null)
				si.setDcurrency(so.getDcurrency());
			if (so.getDsumm() != null)
				si.setDsumm(so.getDsumm());
			if (so.getGenStatus() != null)
				si.setGenStatus(so.getGenStatus());
			if (so.getGlCode() != null)
				si.setGlCode(so.getGlCode());
			if (so.getId() != null)
				si.setId(so.getId());
			if (so.getInventory() != null)
				si.setInventory(so.getInventory());
			if (so.getInvoiceItem() != null)
				si.setInvoiceItem(so.getInvoiceItem());
			if (so.getQuantity() != null)
				si.setQuantity(so.getQuantity());
			if (so.getRate() != null)
				si.setRate(so.getRate());
			if (so.getWsumm() != null)
				si.setWsumm(so.getWsumm());
			if (so.getWcurrency() != null)
				si.setWcurrency(so.getWcurrency());
			if (so.getUnit() != null)
				si.setUnit(so.getUnit());
			if (so.getSerialNumber() != null)
				si.setSerialNumber(so.getSerialNumber());
			if (so.getRefkey() != null)
				si.setRefkey(so.getRefkey());
			
			return si;
		} catch(Exception ex) {
			throw ex;
		}
	}
}
