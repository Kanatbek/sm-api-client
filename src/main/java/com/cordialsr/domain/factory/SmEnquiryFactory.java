package com.cordialsr.domain.factory;

import com.cordialsr.domain.SmEnquiry;
import com.cordialsr.domain.security.User;
import com.ibm.icu.text.SimpleDateFormat;

public class SmEnquiryFactory {

	public static String createHTML(SmEnquiry enquiry, User user) {
		SimpleDateFormat formatForDateNow = new SimpleDateFormat("yyyy.MM.dd, hh:mm:ss");
		String etype = null;
		if (enquiry.getEtype().equals(SmEnquiry.TYPE_DEMONTAZH)) {
			etype = "Демонтаж";
		} else if (enquiry.getEtype().equals(SmEnquiry.TYPE_SERVICE)) {
			etype = "Сервис";
		} else {
			etype = "Покупка";
		}
	
		String message = "<html>"
		+ "<body style='max-width: 500px;'>"
		+ "<div class='row-top' style='height: 100px; background-color: #2f4050; border-radius: 10px 10px 10px 10px'>"
		+ "<a href='http://cordialerp.dyndns.org:4200/#/main'><img src='cid:cordial_logo' style='width: 150px; height: 60px; margin-left: 20px; margin-top: 15px'></a>"
		+ "<h2 style='float: right; color: #e6f7ff; font-family: Agency FB; margin-right: 40px; margin-top: 40px;'><strong>" + user.getLastName() + " " + user.getFirstName() + "</strong></h2>"
		+ "</div>"
		+ "<div class='row-body' style='min-height: 500px; border: 0.5px solid #2f4050; border-radius: 10px;'>"
		+ "<div class='body' style='max-width: 300px; margin: auto;'>"
		+ "<div class='stype' style='width: 100%; text-align: center'>"
		+ "<h2 style='display: inline-block;'><strong>Дата:</strong></h2>"
		+ "<h3 style='display: inline-block; color: #800000; margin-left: 30px'>" + formatForDateNow.format(enquiry.getEdate()) + "</h3>"
		+ "</div>"
		+ "<hr>"

		+ "<div class='branch' style='width: 100%; text-align: center'>"
		+ "<h2 style='display: inline-block;'><strong>Филиал:</strong></h2>"
		+ "<h3 style='display: inline-block; color: #800000; margin-left: 30px'>" + enquiry.getBranch().getBranchName() + "</h3>"
		+ "</div>"
		+ "<hr>"

		+ "<div class='stype' style='width: 100%; text-align: center'>"
		+ "<h2 style='display: inline-block;'><strong>Тип:</strong></h2>"
		+ "<h3 style='display: inline-block; color: #800000; margin-left: 30px'>" + etype + "</h3>"
		+ "</div>"
		+ "<hr>"

		+ "<div class='stype' style='width: 100%; text-align: center'>"
		+ "<h2 style='display: inline-block;'><strong>Статус:</strong></h2>"

		+ "<h3 style='display: inline-block; color: #800000; margin-left: 30px'>Срочно!!!</h3>"
		+ "</div>"

		+ "<hr>"
		+ "<div class='stype' style='width: 100%; text-align: center'>"
		+ "<h2 style='display: inline-block;'><strong>ФИО:</strong></h2>"
		+ "<h3 style='display: inline-block; color: #800000; margin-left: 30px'>" + enquiry.getFullFio() + "</h3>"
		+ "</div>"

		+ "<hr>"
		+ "<div class='stype' style='width: 100%; text-align: center'>"
		+ "<h2 style='display: inline-block;'><strong>Телефон:</strong></h2>"
		+ "<h3 style='display: inline-block; color: #800000; margin-left: 30px'>" + enquiry.getPhone() + "</h3>"
		+ "</div>"

		+ "<hr>"
		+ "<div class='stype' style='width: 100%; text-align: center'>"
		+ "<h2 style='display: inline-block;'><strong>Информация:</strong></h2>"
		+ "<h3 style='display: inline-block; color: #800000; margin-left: 30px'>" + enquiry.getInfo() + "</h3>"
		+ "</div>"
		+ "</div>"
		+ "</div>"
		+ "<div class='row-footer'>"
		+ "<strong style='float: right; color: #2f4050; margin-right: 30px; margin-top: 20px'>Cordial Group of Co. © 2018</strong>"
		+ "</div>"
		+ "</body>"
		+ "</html>";
		
		return message;
	}
	
}
