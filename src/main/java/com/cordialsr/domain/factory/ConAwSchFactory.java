package com.cordialsr.domain.factory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.cordialsr.domain.Award;
import com.cordialsr.domain.AwardTemplate;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractAwardSchedule;
import com.cordialsr.domain.Employee;
import com.cordialsr.general.GeneralUtil;

public class ConAwSchFactory {

	public static List<ContractAwardSchedule> constructCawLFor(Contract con, Employee empl, Award award, BigDecimal accruedSumm) {
		List<ContractAwardSchedule> cawL = new ArrayList<>();
		for (AwardTemplate awt : award.getAwardTemplates()) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(con.getDateSigned());
			cal.add(Calendar.MONTH, awt.getPayrollMonth());
			for (int i = 0; i < awt.getMonth(); i++) {
				ContractAwardSchedule caw = new ContractAwardSchedule();
				caw.setAwardType(award.getAwardType());
				caw.setCurrency(award.getCurrency());
				caw.setContract(con);
				
				caw.setDateSchedule(cal.getTime());
				caw.setSumm(awt.getSumm());
				BigDecimal toAccrue = (accruedSumm.compareTo(awt.getSumm()) > 0) ? awt.getSumm() : accruedSumm;
				accruedSumm = accruedSumm.subtract(toAccrue);
				caw.setAccrued(toAccrue);
				if (!GeneralUtil.isEmptyBigDecimal(caw.getAccrued())) {
					caw.setDateAccrued(cal.getTime());
				}
				caw.setClosed(caw.getAccrued().compareTo(caw.getSumm()) >= 0);
				caw.setParty(empl.getParty());
				caw.setPosition(empl.getPosition());
				// caw.setRefkey(refkey);
				caw.setRevertOnCancel(awt.getRevertOnCancel());
				cawL.add(caw);
				cal.add(Calendar.MONTH, 1);
			}
		}
		return cawL;
	}
	
	public static List<ContractAwardSchedule> getContractAwL(Contract con, Integer pos, String awt, Long pid) {
		List<ContractAwardSchedule> cawL = new ArrayList<>();
		for (ContractAwardSchedule caw : con.getCawSchedules()) {
			if (caw.getParty().getId().equals(pid)
					&& caw.getAwardType().getName().equals(awt)
					&& caw.getPosition().getId() == pos) {
				cawL.add(caw);
			}
		}
		return cawL;
	}
	
}
