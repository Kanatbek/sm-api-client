package com.cordialsr.domain.factory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractItem;
import com.cordialsr.domain.ContractPaymentSchedule;
import com.cordialsr.domain.Department;
import com.cordialsr.domain.FinActivityType;
import com.cordialsr.domain.FinCashflowStatement;
import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.FinDocType;
import com.cordialsr.domain.FinEntry;
import com.cordialsr.domain.FinGlAccount;
import com.cordialsr.domain.InvItemSn;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.InvoiceItem;
import com.cordialsr.domain.KassaBank;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.Payroll;
import com.cordialsr.domain.StockIn;
import com.cordialsr.domain.StockOut;
import com.cordialsr.domain.security.User;
import com.cordialsr.dto.ContractPaymentDto;
import com.cordialsr.general.GeneralUtil;

public class FinDocFactory {

	public static FinDoc constructFdFromInvoice(FinDoc newFd, Invoice invoice) throws Exception {
		try {
			newFd.setCompany(invoice.getCompany());
			newFd.setClosed(false);
			newFd.setCpuDate(Calendar.getInstance().getTime());
			newFd.setDcurrency(invoice.getCompany().getCurrency());
			newFd.setDepartment(new Department(Department.DEP_LOGISTICS));
			newFd.setForbuh(true); // Need to clarify
			newFd.setStorno(false);
			Calendar cal = Calendar.getInstance();
			cal.setTime(newFd.getDocDate());
			newFd.setYear(String.valueOf(cal.get(Calendar.YEAR)));
			newFd.setMonth(cal.get(Calendar.MONTH) + 1);
			newFd.setInvoice(invoice);
			return newFd;
		} catch (Exception e) {
			throw e;
		}
	}
	
	public static FinDoc constructFdForStockImporter(FinDoc newFd, Invoice invoice) throws Exception {
		try {
			if (invoice.getBankPartner() != null 
					&& !GeneralUtil.isEmptyLong(invoice.getBankPartner().getId())) {
				newFd.setParty(invoice.getBankPartner());
			} else {
				newFd.setParty(invoice.getParty());
			}
			
			newFd.setBranch(invoice.getBranchImporter());
			newFd.setFinDocType(new FinDocType(FinDocType.TYPE_WI_STOCK_WRITE_IN));
			newFd.setIsMain(true);
			return newFd;
		} catch (Exception e) {
			throw e;
		}
	}
	
	public static FinDoc constructFdForWEandWG(FinDoc newFd, Invoice invoice, FinDoc fdMain) throws Exception {
		try {
			newFd.setDocDate(fdMain.getDocDate());
			newFd = constructFdFromInvoice(newFd, invoice);
			newFd.setBranch(invoice.getBranchImporter());
			
			BigDecimal dsumm = newFd.getWsumm().divide(newFd.getRate(), 12, RoundingMode.HALF_EVEN);
			newFd.setDsumm(dsumm);
			newFd.setDsummPaid(new BigDecimal(0));
			newFd.setWsummPaid(new BigDecimal(0));
			newFd.setDocDate(fdMain.getDocDate());
			newFd.setIsMain(false);
			newFd.setTrcode(fdMain.getTrcode());
			newFd.setUser(fdMain.getUser());
			newFd.setParentFindoc(fdMain);
			
			/* Incoming data for findoc WG
			 * finDocType = "WG";
			 * rate = rate2
			 * title = [{"GP": "Гос. пошлина"},
			 * 			{"TS": "Таможенные сборы"},
			 * 			{"VAT": "НДС на покупку"},
			 * 			{"AK": "Акциз"}]
			 * wsumm = sum of finEntries' wsumm;
			 * wcurrency = set on front 
			 */
			
			List<String> glL = getDebetGlCodesFromFd(fdMain);
			Integer entryCount = glL.size() * 2;
			
			// Debet
			FinEntry feDebet = formFinEntry(newFd);
			feDebet.setDc(FinEntry.DC_DEBET);
			feDebet.setDepartment(new Department(Department.DEP_LOGISTICS));
			feDebet.setFinActType(new FinActivityType(FinActivityType.TYPE_OPERATIVE));
			feDebet.setIsTovar(false);
			feDebet.setEntrycount(entryCount);
			feDebet.setInfo("");
			
			// Credit
			FinEntry feCredit = new FinEntry();
			BeanUtils.copyProperties(feDebet, feCredit);
			feCredit.setDc(FinEntry.DC_CREDIT);
			
			if (newFd.getFinDocType().getCode().equals(FinDocType.TYPE_WG_IMPORT_GTD)) {
				String title = "";
				FinGlAccount gl = new FinGlAccount();
				if (newFd.getDocno().equalsIgnoreCase("GP")) {
					title = "ГТД - Гос. пошлина";
					gl.setCode(FinGlAccount.P_ST_OTHER_LIABILITIES_3190);
				} else if (newFd.getDocno().equalsIgnoreCase("TS")) {
					title = "ГТД - Таможенные сборы";
					gl.setCode(FinGlAccount.P_ST_OTHER_LIABILITIES_3190);
				} else if (newFd.getDocno().equalsIgnoreCase("VAT")) {
					title = "ГТД - НДС на покупку";
					gl.setCode(FinGlAccount.P_ST_VALUE_ADDED_TAX_3130);
				} else if (newFd.getDocno().equalsIgnoreCase("AK")) {
					title = "ГТД - Акциз";
					gl.setCode(FinGlAccount.P_ST_EXCISES_3140);
				} else {
					throw new Exception("Unknown GTD type!");
				}
				feCredit.setGlAccount(gl);
				newFd.setTitle(title);
			} else if (newFd.getFinDocType().getCode().equals(FinDocType.TYPE_WE_IMPORT_EXPENCES)) {
				feCredit.setGlAccount(new FinGlAccount(FinGlAccount.P_ST_PROVIDER_3310));
			}
			
			// Calc accrued ratio
			List<FinEntry> feSet = new ArrayList<>();
			Integer pk = 1;
			BigDecimal wsumTotal = new BigDecimal(0);
			BigDecimal dsumTotal = new BigDecimal(0);
			for (String code: glL) {
				BigDecimal sum = calcDebetSumByGl(fdMain, code);
				BigDecimal ws = (sum.divide(fdMain.getWsumm(), 12, RoundingMode.HALF_EVEN)).multiply(newFd.getWsumm());
				
				FinEntry feDb = feDebet.clone();
				feDb.setPostkey(pk.toString());
				feDb.setGlAccount(new FinGlAccount(code));
				feDb.setWsumm(ws);
				feDb.setRate(newFd.getRate());
				BigDecimal ds = ws.divide(newFd.getRate(), 12, RoundingMode.HALF_EVEN);
				feDb.setDsumm(ds);
				
				FinEntry feCr = feCredit.clone();
				feCr.setPostkey(pk.toString());
				feCr.setWsumm(feDb.getWsumm());
				feCr.setRate(feCr.getRate());
				feCr.setDsumm(feCr.getDsumm());
				
				wsumTotal = wsumTotal.add(feDb.getWsumm());
				dsumTotal = dsumTotal.add(feDb.getDsumm());
				
				feSet.add(feDb);
				feSet.add(feCr);
				pk++;
			}
			
			if (newFd.getWsumm().setScale(2, RoundingMode.HALF_EVEN).compareTo(wsumTotal.setScale(2, RoundingMode.HALF_EVEN)) != 0) {
				throw new Exception("Total Wsum (" + wsumTotal.toString() + ") of finentries is different from FinDoc Wsum (" + newFd.getWsumm().toString() + ").");
			} else if (newFd.getDsumm().setScale(2, RoundingMode.HALF_EVEN).compareTo(dsumTotal.setScale(2, RoundingMode.HALF_EVEN)) != 0) {
				throw new Exception("Total Dsum (" + dsumTotal.toString() + ") of finentries is different from FinDoc Dsum (" + newFd.getDsumm().toString() + ").");
			}
			
			newFd.setFinEntries(feSet);
			return newFd;
		} catch (Exception e) {
			throw e;
		}
	}
	
	private static BigDecimal calcDebetSumByGl(FinDoc fd, String gl) {
		BigDecimal bd = new BigDecimal(0);
		for (FinEntry fe: fd.getFinEntries()) {
			if (fe.getDc().equals(FinEntry.DC_DEBET) 
					&& fe.getGlAccount().getCode().equals(gl)) {
				bd = bd.add(fe.getWsumm());
			}
		}
		return bd;
	} 
	
	private static List<String> getDebetGlCodesFromFd(FinDoc fd) {
		List<String> glList = new ArrayList<>();
		for (FinEntry fe: fd.getFinEntries()) {
			if (fe.getDc().equals(FinEntry.DC_DEBET)) {
				if (glList.contains(fe.getGlAccount().getCode())) {
					continue;
				}
				glList.add(fe.getGlAccount().getCode());
			}
		}
		return glList;
	} 
	
	// ***************************************************************************************************
	
	private static void validateStockSummForFinEntry(FinEntry fe, Invoice invoice) throws Exception {
		if (fe.getDc().equals(FinEntry.DC_DEBET)) {
			BigDecimal q = new BigDecimal(0);
			for (InvoiceItem ii: invoice.getInvoiceItems()) {
				if (ii.getId().compareTo(fe.getInvoiceItem().getId()) == 0) {
					fe.setInvoiceItem(ii);
					for(StockIn si: ii.getChildStockIns()) {
						if (si.getQuantity() != null 
								&& si.getGlCode().equals(fe.getGlAccount().getCode())) {
							q = q.add(si.getQuantity());
						}
					}
					break;
				}
			}
			if (fe.getQuantity().compareTo(q) != 0) {
				throw new Exception("Stock's count is different from quantity.");
			}
		}
	}
	
	public static FinDoc constructFdForStockInImport(FinDoc newFd, Invoice invoice) throws Exception {
		
		newFd = constructFdFromInvoice(newFd, invoice);
		newFd = constructFdForStockImporter(newFd, invoice);
		newFd.setTitle("Импорт ТМЗ");
		
		/* Incoming data for findoc WI
		 * finDocType = "WI";
		 * rate = rate1
		 * wsumm = invoice.cost
		 * wcurrency = invoice.currency  
		 */
		
		BigDecimal dsumm = newFd.getWsumm().divide(newFd.getRate(), 12, RoundingMode.HALF_EVEN);
		newFd.setDsumm(dsumm);
		newFd.setDsummPaid(new BigDecimal(0));
		newFd.setWsummPaid(new BigDecimal(0));
		newFd.setWcurrency(invoice.getCurrency());
		
		Integer entryCount = newFd.getFinEntries().size() * 2;
		List<FinEntry> feSet = new ArrayList<>();
		Integer pk = 1;
		
		for (FinEntry fe: newFd.getFinEntries()) {
			fe.setRate(newFd.getRate());
			validateStockSummForFinEntry(fe, invoice);
			fe.setCompany(newFd.getCompany());
			fe.setBranch(newFd.getBranch());
			fe.setCpuDate(Calendar.getInstance().getTime());
			fe.setDcurrency(fe.getCompany().getCurrency());
			fe.setDdate(newFd.getDocDate());
			fe.setDepartment(new Department(Department.DEP_LOGISTICS));
			BigDecimal ds = fe.getWsumm().divide(newFd.getRate(), 12, RoundingMode.HALF_EVEN);
			fe.setDsumm(ds);
			
			fe.setEntrycount(entryCount);
			fe.setFinDoc(newFd);
			fe.setFinActType(new FinActivityType(FinActivityType.TYPE_OPERATIVE));
			// fe.setGlAccount(new FinGlAccount(FinGlAccount.A_ST_GOODS_1330));
			// fe.setInfo(info);
			// fe.setInventory(inventory);
			// fe.setInvoiceItem(invoiceItem);  +++
			fe.setIsTovar(true);
			fe.setItemName(fe.getInventory().getName());
			fe.setMonth(Calendar.getInstance().get(Calendar.MONTH) + 1);
			fe.setParty(newFd.getParty());
			fe.setPostkey(pk.toString());
			// fe.setPrice(price);
			// fe.setQuantity(quantity);
			// fe.setRefkey(refkey);
			fe.setDc(FinEntry.DC_DEBET);
			// fe.setUnit(unit);
			fe.setWcurrency(newFd.getWcurrency());
			// fe.setWsumm(wsumm); +++
			fe.setYear(String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
			
			FinEntry feCredit = new FinEntry();
			BeanUtils.copyProperties(fe, feCredit);
			feCredit.setDc(FinEntry.DC_CREDIT);
			
			if (invoice.getBankPartner() != null 
					&& !GeneralUtil.isEmptyLong(invoice.getBankPartner().getId())) {
				feCredit.setGlAccount(new FinGlAccount(FinGlAccount.P_LT_BANK_LOANS_4010));
			} else {
				feCredit.setGlAccount(new FinGlAccount(FinGlAccount.P_ST_PROVIDER_3310));
			}
			
			feSet.add(fe);
			feSet.add(feCredit);
			pk++;
		}
		newFd.setFinEntries(feSet);
		return newFd;
	}
	
	// ***************************************************************************************************
	
	public static FinDoc constructFdForStockOutToCustomer(FinDoc mainFd, Invoice invoice, List<StockOut> newStockOutList, String trCode, Long userId) throws Exception {
		FinDoc newFd = new FinDoc();
		newFd.setDocDate(invoice.getDateUpdated().getTime());
		newFd = constructFdFromInvoice(newFd, invoice);
		newFd = constructFdForStockImporter(newFd, invoice);
		newFd.setFinDocType(new FinDocType(FinDocType.TYPE_WO_STOCK_WRITE_OFF));
		newFd.setTitle("Списание товара клиенту");
		newFd.setWcurrency(mainFd.getDcurrency());
		newFd.setRate(new BigDecimal(1));
		newFd.setDcurrency(mainFd.getDcurrency());
		newFd.setTrcode(trCode);
		newFd.setUser(new User(userId));
		newFd.setStorno(false);
		newFd.setParentFindoc(mainFd);
		newFd.setRefkeyMain(mainFd.getRefkey());
		newFd.setIsMain(false);
		newFd.setClosed(true);
		newFd.setContract(mainFd.getContract());
		
		Integer entryCount = newStockOutList.size() * 2;
		List<FinEntry> feSet = new ArrayList<>();
		Integer pk = 1;
		
		BigDecimal wsumm = new BigDecimal(0);
		for (StockOut so: newStockOutList) {
			wsumm = wsumm.add(so.getDsumm());
			
			FinEntry fe = formFinEntry(newFd);
			fe.setRate(newFd.getRate());
			fe.setDc(FinEntry.DC_CREDIT);
			fe.setEntrycount(entryCount);
			fe.setFinActType(new FinActivityType(FinActivityType.TYPE_OPERATIVE));
			fe.setIsTovar(true);
			fe.setPostkey(pk.toString());
			fe.setInvoiceItem(so.getInvoiceItem());
			
			fe.setPrice(so.getDsumm());
			fe.setQuantity(so.getQuantity());
			fe.setUnit(so.getUnit());
			fe.setDsumm(so.getDsumm().multiply(so.getQuantity()));
			fe.setWsumm(fe.getDsumm());
			fe.setRate(newFd.getRate());
			fe.setGlAccount(new FinGlAccount(so.getGlCode()));
			fe.setInventory(so.getInventory());
			fe.setItemName(fe.getInventory().getName());
			
			FinEntry feDebet = new FinEntry();
			BeanUtils.copyProperties(fe, feDebet);
			feDebet.setDc(FinEntry.DC_DEBET);
			
			if (so.getGlCode().equals(FinGlAccount.A_ST_GOODS_1330)) {
				feDebet.setGlAccount(new FinGlAccount(FinGlAccount.A_EX_COST_OF_SALES_7010));
			} 
			else if (so.getGlCode().equals(FinGlAccount.A_LT_FIXED_ASSETS_2410)) {
				continue;
//				 feDebet.setGlAccount(new FinGlAccount(FinGlAccount.A_EX_ASSETS_RETIREMENT_COSTS_7410));
			} 
			else {
				throw new Exception("Item GL Account [" + so.getGlCode() + "] is different from '1330'.");
			}
			
			feSet.add(fe);
			feSet.add(feDebet);
			pk++;
		}
		
		newFd.setWsumm(wsumm);
		newFd.setDsumm(wsumm);
		newFd.setDsummPaid(new BigDecimal(0));
		newFd.setWsummPaid(new BigDecimal(0));
		
		newFd.setFinEntries(feSet);
		return newFd;
	}
	
	// *****************************************************************************************************************
	
	public static FinDoc constructFdForStockReturnFromCustomer(FinDoc mainFd, Invoice invoice, List<StockIn> newStockInList, String trCode, Long userId) throws Exception {
		FinDoc newFd = new FinDoc();
		newFd.setDocDate(invoice.getDateCreated().getTime());
		newFd = constructFdFromInvoice(newFd, invoice);
		newFd.setFinDocType(new FinDocType(FinDocType.TYPE_WR_RETURN_TMZ));
		newFd.setTitle("Возврат ТМЗ от клиента");
		newFd.setWcurrency(mainFd.getDcurrency());
		newFd.setRate(new BigDecimal(1));
		newFd.setDcurrency(mainFd.getDcurrency());
		newFd.setTrcode(trCode);
		newFd.setUser(new User(userId));
		newFd.setStorno(false);
		newFd.setParentFindoc(mainFd);
		newFd.setRefkeyMain(mainFd.getRefkey());
		newFd.setIsMain(false);
		newFd.setClosed(true);
		newFd.setContract(mainFd.getContract());
		
		Integer entryCount = newStockInList.size() * 2;
		List<FinEntry> feSet = new ArrayList<>();
		Integer pk = 1;
		
		BigDecimal wsumm = new BigDecimal(0);
		for (StockIn si: newStockInList) {
			wsumm = wsumm.add(si.getDsumm());
			
			FinEntry fe = formFinEntry(newFd);
			fe.setRate(newFd.getRate());
			fe.setDc(FinEntry.DC_DEBET);
			fe.setEntrycount(entryCount);
			fe.setFinActType(new FinActivityType(FinActivityType.TYPE_OPERATIVE));
			fe.setIsTovar(true);
			fe.setPostkey(pk.toString());
			fe.setInvoiceItem(si.getInvoiceItem());
			
			fe.setPrice(si.getDsumm());
			fe.setQuantity(si.getQuantity());
			fe.setUnit(si.getUnit());
			fe.setDsumm(si.getDsumm().multiply(si.getQuantity()));
			fe.setWsumm(fe.getDsumm());
			fe.setRate(newFd.getRate());
			fe.setGlAccount(new FinGlAccount(si.getGlCode()));
			fe.setInventory(si.getInventory());
			fe.setItemName(fe.getInventory().getName());
			
			FinEntry feCredit = new FinEntry();
			BeanUtils.copyProperties(fe, feCredit);
			feCredit.setDc(FinEntry.DC_CREDIT);
			
			if (si.getGlCode().equals(FinGlAccount.A_ST_GOODS_1330)) {
				feCredit.setGlAccount(new FinGlAccount(FinGlAccount.P_IN_RETURN_OF_SOLD_PRODUCTS_6020));
			} 
			else if (si.getGlCode().equals(FinGlAccount.A_LT_FIXED_ASSETS_2410)) {
				continue;
//				feCredit.setGlAccount(new FinGlAccount(FinGlAccount.A_EX_ASSETS_RETIREMENT_COSTS_7410));
			} 
			else {
				throw new Exception("Item GL Account [" + si.getGlCode() + "] is different from '1330'.");
			}
			
			feSet.add(fe);
			feSet.add(feCredit);
			pk++;
		}
		
		newFd.setWsumm(wsumm);
		newFd.setDsumm(wsumm);
		newFd.setDsummPaid(new BigDecimal(0));
		newFd.setWsummPaid(new BigDecimal(0));
		
		newFd.setFinEntries(feSet);
		return newFd;
	}
	
	// ***********************************************************************************************************
	
	public static FinDoc constructFdForStockInTransfer(FinDoc newFd, Invoice invoice) throws Exception {
		
		newFd = constructFdFromInvoice(newFd, invoice);
		newFd = constructFdForStockImporter(newFd, invoice);
		newFd.setTitle("Перемещение ТМЗ");
		newFd.setRate(new BigDecimal(1));
		newFd.setDcurrency(newFd.getCompany().getCurrency());
		newFd.setWcurrency(newFd.getCompany().getCurrency());
		
		Integer entryCount = newFd.getFinEntries().size() * 2;
		List<FinEntry> feSet = new ArrayList<>();
		Integer pk = 1;
		BigDecimal summ = new BigDecimal(0);
		
		for (FinEntry fe: newFd.getFinEntries()) {
			fe.setRate(newFd.getRate());
			validateStockSummForFinEntry(fe, invoice);
			fe.setBranch(invoice.getBranchImporter());
			fe.setCompany(invoice.getCompany());
			fe.setCpuDate(Calendar.getInstance().getTime());
			fe.setDcurrency(newFd.getDcurrency());
			fe.setDdate(newFd.getDocDate());
			fe.setDepartment(new Department(Department.DEP_LOGISTICS));
			
			fe.setEntrycount(entryCount);
			fe.setFinDoc(newFd);
			fe.setFinActType(new FinActivityType(FinActivityType.TYPE_OPERATIVE));
			// fe.setGlAccount(new FinGlAccount(FinGlAccount.A_ST_GOODS_1330));
			// fe.setInfo(info);
			// fe.setInventory(inventory);
			fe.setIsTovar(true);
			fe.setItemName(fe.getInventory().getName());
			fe.setMonth(Calendar.getInstance().get(Calendar.MONTH) + 1);
			fe.setParty(newFd.getParty());
			fe.setPostkey(pk.toString());
			// fe.setPrice(price);
			// fe.setQuantity(quantity);
			// fe.setRefkey(refkey);
			fe.setDc(FinEntry.DC_DEBET);
			// fe.setUnit(unit);
			fe.setWcurrency(newFd.getWcurrency());
			fe.setYear(String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
			// fe.setChildStockIns(fe.getInvoiceItem().getChildStockIns());
			
			BigDecimal ds = new BigDecimal(0);
			List<InvItemSn> iiSnL = new ArrayList<>();
			for (StockIn s: fe.getInvoiceItem().getChildStockIns()) {
				ds = ds.add(s.getDsumm().multiply(s.getQuantity()));
			}
			fe.getInvoiceItem().setChildInvSns(iiSnL);
			
			fe.setWsumm(ds);
			// BigDecimal ds = fe.getWsumm().divide(newFd.getRate(), 12, RoundingMode.HALF_EVEN);
			fe.setDsumm(ds);
			summ = summ.add(fe.getDsumm());
			
			FinEntry feCredit = new FinEntry();
			BeanUtils.copyProperties(fe, feCredit);
			feCredit.setDc(FinEntry.DC_CREDIT);
			feCredit.setGlAccount(fe.getGlAccount());
			feCredit.setBranch(invoice.getBranch());
			feSet.add(fe);
			feSet.add(feCredit);
			pk++;
		}
		newFd.setDsumm(summ);
		newFd.setWsumm(summ);
		newFd.setDsummPaid(newFd.getDsumm());
		newFd.setWsummPaid(newFd.getWsumm());
		newFd.setClosed(true);
		newFd.setRate(new BigDecimal(1));
		newFd.setFinEntries(feSet);
		return newFd;
	}
	
	// ***********************************************************************************************************
	
	public static FinEntry formFinEntry(FinDoc newFd) throws Exception {
		try {
			FinEntry fe = new FinEntry();
			fe.setFinDoc(newFd);
			fe.setCompany(newFd.getCompany());
			fe.setBranch(newFd.getBranch());
			fe.setParty(newFd.getParty());
			fe.setDepartment(newFd.getDepartment());
			
			fe.setWcurrency(newFd.getWcurrency());
			fe.setDcurrency(fe.getCompany().getCurrency());
			fe.setWsumm(newFd.getWsumm());
			fe.setRate(newFd.getRate());
			if (fe.getWsumm() != null && fe.getRate() != null) {
				BigDecimal ds = fe.getWsumm().divide(newFd.getRate(), 12, RoundingMode.HALF_EVEN);
				fe.setDsumm(ds);
			}
			
			fe.setDdate(newFd.getDocDate());
			fe.setMonth(newFd.getMonth());
			fe.setYear(newFd.getYear());
			fe.setCpuDate(Calendar.getInstance().getTime());
			return fe;
		} catch (Exception e) {
			throw e;
		}
	}

	// ***********************************************************************************************************
		
	public static FinDoc constructFdFromContractGeneral(Contract con, BigDecimal rate, ContractPaymentSchedule ps, int paymentOrder) throws Exception {
		try {
			FinDoc fd = constructFdFromContract(con);
			fd.setRate(rate);
			fd.setIsMain(true);
			
			if (ps.getIsFirstpayment()) {
				String ctype = "Продажи";
				fd.setFinDocType(new FinDocType(FinDocType.TYPE_GS_GENERAL_SELL_CONTRACT));
				if (con.getIsRent()) { 
					ctype = "Аренды";
					fd.setFinDocType(new FinDocType(FinDocType.TYPE_GR_GENERAL_RENT_CONTRACT));					
				}
				fd.setTitle("Финансовый документ " + ctype);
				
				fd.setIsMain(true);
			} else {
				fd.setTitle("Счет на оплату покупателю");
				fd.setFinDocType(new FinDocType(FinDocType.TYPE_IR_INVOICE_RECIEVABLE));
				fd.setInfo(paymentOrder + " Взнос");
				fd.setIsMain(false);
			}
			fd.setTrcode("MSNWCON");
			fd.setPaymentOrder(paymentOrder);
			fd.setPaymentDue(ps.getPaymentDate());
			
			fd.setDocDate(ps.getPaymentDate());
			fd.setWsumm(ps.getSumm());
			BigDecimal ds = fd.getWsumm().divide(fd.getRate(), 12, RoundingMode.HALF_EVEN);
			fd.setDsumm(ds);
			
			fd.setWsummPaid(ps.getPaid());
			BigDecimal dsp = fd.getWsummPaid().divide(fd.getRate(), 12, RoundingMode.HALF_EVEN);
			fd.setDsumm(dsp);
			
			// Construct FinEntries
			Integer pk = 1;
			List<FinEntry> feList = new ArrayList<>();
			FinEntry fe = new FinEntry(FinEntry.DC_DEBET);
			fe = formFinEntry(fd);
			fe.setGlAccount(new FinGlAccount(FinGlAccount.A_ST_RECEIVABLES_FROM_CUSTOMER_1210));
			fe.setIsTovar(false);
			if (ps.getIsFirstpayment()) {
				fe.setCashflowStatement(new FinCashflowStatement(FinCashflowStatement.CF_STA_FIRST_PAYMENT));
			} else {
				fe.setCashflowStatement(new FinCashflowStatement(FinCashflowStatement.CF_STA_MONTHLY_PAYMENT));
			}
			// credit glCode = 6010
			Integer ec = 2;
			fe.setEntrycount(ec); // calc
			fe.setFinActType(new FinActivityType(FinActivityType.TYPE_OPERATIVE));
			for (ContractItem ci: con.getContractItems()) {
				fe.setPostkey(pk.toString());
				// fe.setIsTovar(true);
				fe.setInventory(ci.getInventory());
				fe.setItemName(ci.getInventory().getName());
				fe.setQuantity(ci.getQuantity());
				fe.setPrice(ci.getPrice());
				fe.setUnit(ci.getUnit());
				//Assign summs && currencies && rate ...
				fe.setWsumm(fd.getWsumm());
				BigDecimal ds2 = fe.getWsumm().divide(fe.getRate(), 12, RoundingMode.HALF_EVEN);
				fe.setDsumm(ds2);
				FinEntry feDb = fe.clone();
				feDb.setDc(FinEntry.DC_DEBET);
				FinEntry feCr = fe.clone();
				feCr.setDc(FinEntry.DC_CREDIT);
				feCr.setGlAccount(new FinGlAccount(FinGlAccount.P_IN_REVENUE_6010));
				feList.add(feDb);
				feList.add(feCr);
				pk++;
			}
			fd.setFinEntries(feList);
			return fd;
		} catch (Exception e) {
			throw e;
		}
	}
	
	public static FinDoc constructFdFromContract(Contract con) {
		try {
			FinDoc fd = new FinDoc();
			fd.setCompany(con.getCompany());
			fd.setBranch(con.getBranch());
			fd.setContract(con);
			fd.setDepartment(new Department(Department.DEP_MARKETING_AND_SALES));
			fd.setForbuh(con.getForbuh());
			fd.setParty(con.getCustomer());
			
			fd.setDocDate(con.getDateSigned());
			Calendar cal = Calendar.getInstance();
			cal.setTime(fd.getDocDate());
			fd.setYear(String.valueOf(cal.get(Calendar.YEAR)));
			fd.setMonth(cal.get(Calendar.MONTH) + 1);
			
			fd.setDcurrency(fd.getCompany().getCurrency());
			fd.setWcurrency(con.getCurrency());
			fd.setRate(con.getRate());
			
			fd.setWsummPaid(new BigDecimal(0));
			fd.setDsummPaid(new BigDecimal(0));
			fd.setDsumm(new BigDecimal(0));
			fd.setWsumm(new BigDecimal(0));
//			if (!GeneralUtil.isEmptyBigDecimal(con.getPaid())) {
//				fd.setWsummPaid(con.getPaid());
//				if (GeneralUtil.isEmptyBigDecimal(fd.getRate())) {
//					BigDecimal dPaid = fd.getWsummPaid().divide(fd.getRate(), 12, RoundingMode.HALF_EVEN);
//					fd.setDsummPaid(dPaid);
//				}
//			}
			
			fd.setUser(con.getRegisteredUser());
			fd.setStorno(false);
			fd.setClosed(false);
			
			return fd;	
		} catch(Exception e) {
			throw e;
		}
	}
	
	public static FinDoc constructDiscountFdFromContractSales(Contract con) throws Exception {
		try {
			FinDoc fd = constructFdFromContract(con);
			
			fd.setTitle("Скидка компании от Продажи");
			fd.setFinDocType(new FinDocType(FinDocType.TYPE_DC_DISCOUNT_COMPANY));
			fd.setIsMain(false);
			fd.setTrcode("MSNWCON");
			fd.setWsummPaid(new BigDecimal(0));
			fd.setDsummPaid(new BigDecimal(0));
			// fd.setBusarea(busarea);
						
			fd.setWsumm(con.getDiscount());
			BigDecimal ds = fd.getWsumm().divide(fd.getRate(), 12, RoundingMode.HALF_EVEN);
			fd.setDsumm(ds);
			
			// Construct FinEntries
			Integer pk = 1;
			List<FinEntry> feList = new ArrayList<>();
			FinEntry fe = new FinEntry(FinEntry.DC_DEBET);
			fe = formFinEntry(fd);
			fe.setIsTovar(false);
			fe.setEntrycount(2);
			fe.setFinActType(new FinActivityType(FinActivityType.TYPE_OPERATIVE));
			fe.setCashflowStatement(new FinCashflowStatement(FinCashflowStatement.CF_STA_DISCOUNT_COMPANY_71));
			
			// check for discount && add discoutn finEntries if exist 
						// if discount from sell price is > 0, then add entry with debet glCode = 6030 & credit glCode = 1210.
						// For company_discount - Debet 6030 the customer & for Credit the 1210 the customer.
						
			if (!GeneralUtil.isEmptyBigDecimal(con.getDiscount())) {
				fe.setPostkey(pk.toString());
				//Assign summs && currencies && rate ...
				fe.setWsumm(con.getDiscount());
				BigDecimal ds2 = fe.getWsumm().divide(fe.getRate(), 12, RoundingMode.HALF_EVEN);
				fe.setDsumm(ds2);
				FinEntry feDb = fe.clone();
				feDb.setDc(FinEntry.DC_DEBET);
				feDb.setGlAccount(new FinGlAccount(FinGlAccount.P_IN_DISCOUNT_FROM_SALES_PRICE_6030));
				FinEntry feCr = fe.clone();
				feCr.setDc(FinEntry.DC_CREDIT);
				feCr.setGlAccount(new FinGlAccount(FinGlAccount.A_ST_RECEIVABLES_FROM_CUSTOMER_1210));
				feList.add(feDb);
				feList.add(feCr);
			}
			
			fd.setFinEntries(feList);
			return fd;
		} catch (Exception e) {
			throw e;
		}
	}
	
	public static FinDoc constructDealerDiscountFdFromContractSales(Contract con) throws Exception {
		try {
			FinDoc fd = constructFdFromContract(con);
			
			fd.setTitle("Зачет дебиторской задолженности клиента со счета сотрудника");
			fd.setFinDocType(new FinDocType(FinDocType.TYPE_SO_SETOFF));
			fd.setIsMain(false);
			fd.setTrcode("MSNWCON");
			fd.setWsummPaid(new BigDecimal(0));
			fd.setDsummPaid(new BigDecimal(0));
			// fd.setBusarea(busarea);
						
			fd.setWsumm(con.getFromDealerSumm());
			BigDecimal ds = fd.getWsumm().divide(fd.getRate(), 12, RoundingMode.HALF_EVEN);
			fd.setDsumm(ds);
			
			// Construct FinEntries
			Integer pk = 1;
			List<FinEntry> feList = new ArrayList<>();
			FinEntry fe = new FinEntry(FinEntry.DC_DEBET);
			fe = formFinEntry(fd);
			fe.setIsTovar(false);
			fe.setEntrycount(2);
			fe.setFinActType(new FinActivityType(FinActivityType.TYPE_OPERATIVE));
			fe.setCashflowStatement(new FinCashflowStatement(FinCashflowStatement.CF_STA_DISCOUNT_STAFF_70));
			
			// For dealer_discount - Debet 3350 the dealer.party & for Credit the 1210 the customer.
			if (!GeneralUtil.isEmptyBigDecimal(con.getFromDealerSumm())) {
				fe.setPostkey(pk.toString());
				//Assign summs && currencies && rate ...
				fe.setWsumm(con.getFromDealerSumm());
				BigDecimal ds2 = fe.getWsumm().divide(fe.getRate(), 12, RoundingMode.HALF_EVEN);
				fe.setDsumm(ds2);
				FinEntry feDb = fe.clone();
				feDb.setDc(FinEntry.DC_DEBET);
				feDb.setGlAccount(new FinGlAccount(FinGlAccount.P_ST_PAYROLL_DEBT_3350));
				feDb.setParty(con.getDealer().getParty());
				FinEntry feCr = fe.clone();
				feCr.setDc(FinEntry.DC_CREDIT);
				feCr.setGlAccount(new FinGlAccount(FinGlAccount.A_ST_RECEIVABLES_FROM_CUSTOMER_1210));
				feList.add(feDb);
				feList.add(feCr);
			}
			
			fd.setFinEntries(feList);
			return fd;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// *********************************************************************************************************************
	
	public static FinDoc assembleConCashPaymentFdFromCp(ContractPaymentDto cp, KassaBank kb, FinDoc gsFd) throws Exception {
		try {
			FinDoc fd = constructFdFromContract(cp.getContract());
			fd.setTitle("Оплата от покупателя");
			fd.setFinDocType(new FinDocType(FinDocType.TYPE_CP_CONTRACT_PAYMENT));
			fd.setIsMain(false);
			fd.setTrcode(cp.getTrCode());
			// fd.setBusarea(busarea);
			fd.setEmployee(cp.getContract().getCollector());
			fd.setBranch(cp.getContract().getServiceBranch());
			fd.setDocDate(cp.getPdate());
			fd.setDsumm(new BigDecimal(0));
			fd.setWcurrency(kb.getCurrency());
			fd.setDcurrency(cp.getDcurrency());
			fd.setParentFindoc(gsFd);
			fd.setContract(cp.getContract());
			fd.setRefkeyMain(gsFd.getRefkey());
			fd.setPaymentOrder(cp.getContractPaymentSchedule().getPaymentOrder());
			fd.setPaymentDue(cp.getContractPaymentSchedule().getPaymentDate());
			fd.setUser(cp.getUser());
			
			String info = "Оплата от: Договор №" + cp.getContract().getContractNumber();
			if (!GeneralUtil.isEmptyBigDecimal(cp.getWsumm()))
					info += " | " + cp.getWsumm() + " " + cp.getWcurrency().getCurrency();
			if (!GeneralUtil.isEmptyBigDecimal(cp.getDsumm()))
				info += " | " + cp.getDsumm() + " " + cp.getDcurrency().getCurrency();
			fd.setInfo(info);
			
			if (kb.getCurrency().getCurrency().equals(cp.getWcurrency().getCurrency())) {
				fd.setRate(cp.getContract().getRate());
				fd.setWsumm(cp.getWsumm());
				BigDecimal ds = fd.getWsumm().divide(fd.getRate(), 12, RoundingMode.HALF_EVEN);
				fd.setDsumm(ds);
			} else if (kb.getCurrency().getCurrency().equals(cp.getDcurrency().getCurrency())) {
				fd.setRate(new BigDecimal(1));
				fd.setWsumm(cp.getDsumm());
				fd.setDsumm(cp.getDsumm());
			}
			fd.setWsummPaid(fd.getWsumm());
			fd.setDsummPaid(fd.getDsumm());
			
			// Construct FinEntries
			Integer pk = 1;
			List<FinEntry> feList = new ArrayList<>();
			FinEntry fe = new FinEntry(FinEntry.DC_DEBET);
			fe = formFinEntry(fd);
			fe.setIsTovar(false);
			fe.setEntrycount(2);
			fe.setFinActType(new FinActivityType(FinActivityType.TYPE_OPERATIVE));
			fe.setInfo(info);
			
			// For Sales contract payment - Debet 1010 the contract.party & for Credit the 1210 the customer.
			fe.setPostkey(pk.toString());
			fe.setWsumm(fd.getWsumm());
			fe.setDsumm(fd.getDsumm());
			if (cp.getContractPaymentSchedule().getIsFirstpayment()) {
				fe.setCashflowStatement(new FinCashflowStatement(FinCashflowStatement.CF_STA_FIRST_PAYMENT));	
			} else {
				fe.setCashflowStatement(new FinCashflowStatement(FinCashflowStatement.CF_STA_MONTHLY_PAYMENT));	
			}
			fe.setKassaBank(kb);
			fe.setParty(fd.getParty());
			
			FinEntry feDb = fe.clone();
			feDb.setDc(FinEntry.DC_DEBET);
			feDb.setGlAccount(new FinGlAccount(FinGlAccount.A_ST_CASH_ON_HAND_1010));
			
			FinEntry feCr = fe.clone();
			feCr.setDc(FinEntry.DC_CREDIT);
			feCr.setGlAccount(new FinGlAccount(FinGlAccount.A_ST_RECEIVABLES_FROM_CUSTOMER_1210));
			
			feList.add(feDb);
			feList.add(feCr);
			
			fd.setFinEntries(feList);
			return fd;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// *********************************************************************************************************************
	
	private static FinDoc constructFdFromPayroll(Payroll p) {
		try {
			FinDoc fd = new FinDoc();
			fd.setCompany(p.getCompany());
			fd.setBranch(p.getBranch());
//			fd.setContract(con);
			if (p.getEmployee() != null) {
				fd.setDepartment(p.getEmployee().getDepartment());	
			} else {
				fd.setDepartment(new Department(Department.DEP_ADMINISTRATION));
			}
//			fd.setForbuh(con.getForbuh());
			fd.setParty(p.getParty());
			
			fd.setDocDate(p.getPayrollDate());
			Calendar cal = Calendar.getInstance();
			cal.setTime(fd.getDocDate());
			fd.setYear(String.valueOf(cal.get(Calendar.YEAR)));
			fd.setMonth(cal.get(Calendar.MONTH) + 1);
			
			fd.setDcurrency(fd.getCompany().getCurrency());
			fd.setWcurrency(p.getCurrency());
			fd.setRate(p.getRate());
			fd.setDsummPaid(new BigDecimal(0));
			fd.setWsummPaid(new BigDecimal(0));
			
			fd.setUser(p.getUser());
			fd.setStorno(false);
			fd.setClosed(false);
			fd.setIsMain(true);
			fd.setTrcode(p.getTrCode());
			
			return fd;
		} catch (Exception e) {
			throw e;
		}
	}
	
	public static FinDoc assemblePayrollFd(Payroll p) throws Exception {
		try {
			FinDoc fd = constructFdFromPayroll(p);
			
			String kind = "ЗП";
			FinDocType fdType = new FinDocType(FinDocType.TYPE_PS_PAYROLL_SALARY);
			FinGlAccount dtAccount = new FinGlAccount(FinGlAccount.A_EX_ADMIN_EXPENSES_7210);
			if (p.getEmployee() != null 
				&& p.getEmployee().getDepartment().getId() == Department.DEP_MARKETING_AND_SALES)
				dtAccount = new FinGlAccount(FinGlAccount.A_EX_EXPENSES_FOR_SALES_7110);
			if (p.getKind().equals(Payroll.KIND_PREMI)) {
				kind = "Премии";
				fdType = new FinDocType(FinDocType.TYPE_PP_PAYROLL_PREMI);
				dtAccount = new FinGlAccount(FinGlAccount.A_EX_EXPENSES_FOR_SALES_7110);
			}
			else if (p.getKind().equals(Payroll.KIND_BONUS)) {
				kind = "Бонуса";
				fdType = new FinDocType(FinDocType.TYPE_PB_PAYROLL_BONUS);
				dtAccount = new FinGlAccount(FinGlAccount.A_EX_EXPENSES_FOR_SALES_7110);
			}
			String header = "Начисление " + kind;
			fd.setTitle(header);
			fd.setFinDocType(fdType);
			String info = p.getInfo();
			info += (info != null && info.length() > 0) ? " | " : "";
			info += "За месяц " + p.getMonth() + "-" + p.getYear();
			if (!GeneralUtil.isEmptyBigDecimal(p.getSum())) {
				info += " | " + p.getSum().setScale(2, RoundingMode.HALF_EVEN) + " " + p.getCurrency().getCurrency();
			}		
			if (p.getKind().equals(Payroll.KIND_SALARY) && p.getEmployee() != null) {
				info += " | Должность: " + p.getEmployee().getPosition().getName();
			}
			// fd.getWsumm().divide(fd.getRate(), 12, RoundingMode.HALF_EVEN);
			fd.setInfo(info);
			
			fd.setRate(p.getRate());
			fd.setWsumm(p.getSum());
			BigDecimal ds = fd.getWsumm().divide(fd.getRate(), 12, RoundingMode.HALF_EVEN);
			fd.setDsumm(ds);
			
			// Construct FinEntries
			Integer pk = 1;
			List<FinEntry> feList = new ArrayList<>();
			FinEntry fe = new FinEntry(FinEntry.DC_DEBET);
			fe = formFinEntry(fd);
			fe.setIsTovar(false);
			fe.setEntrycount(2);
			fe.setFinActType(new FinActivityType(FinActivityType.TYPE_OPERATIVE));
			fe.setInfo(info);
			if (p.getKind().equals(Payroll.KIND_SALARY)) {
				fe.setCashflowStatement(new FinCashflowStatement(FinCashflowStatement.CF_STA_SALARY_1));				
			} else if (p.getKind().equals(Payroll.KIND_PREMI)) {
				fe.setCashflowStatement(new FinCashflowStatement(FinCashflowStatement.CF_STA_PREMI_68));				
			} else if (p.getKind().equals(Payroll.KIND_BONUS)) {
				fe.setCashflowStatement(new FinCashflowStatement(FinCashflowStatement.CF_STA_BONUS_69));				
			}
			
			// Payroll - Debet Admin 7210 / Sales 7110 the party & for Credit the 3350 the party.
			fe.setPostkey(pk.toString());
			fe.setWsumm(fd.getWsumm());
			fe.setDsumm(fd.getDsumm());
			FinEntry feDb = fe.clone();
			feDb.setDc(FinEntry.DC_DEBET);
			feDb.setGlAccount(dtAccount);
			feDb.setParty(fd.getParty());
			
			FinEntry feCr = fe.clone();
			feCr.setDc(FinEntry.DC_CREDIT);
			feCr.setGlAccount(new FinGlAccount(FinGlAccount.P_ST_PAYROLL_DEBT_3350));
			
			feList.add(feDb);
			feList.add(feCr);
			
			fd.setFinEntries(feList);
			return fd;
		} catch (Exception e) {
			throw e;
		}
	}
	
	
	public static FinDoc assembleCashPremiPaymentFd(Contract con,
			KassaBank kb, BigDecimal amount, Party party, Date date, BigDecimal rate, User user, String trCode) throws Exception {
		try {
			FinDoc fd = constructNewCashOutFd(kb, amount, party, date, rate, user, trCode, Department.DEP_MARKETING_AND_SALES);
			
			FinDocType fdType = new FinDocType(FinDocType.TYPE_PP_PAYROLL_PREMI);
			
			String header = "Выдача Премии ";
			fd.setTitle(header);
			fd.setFinDocType(fdType);
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			String info = header + " за " + con.getContractNumber();
			info += (info != null && info.length() > 0) ? " | " : "";
			info += "За месяц " + (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR);
			if (!GeneralUtil.isEmptyBigDecimal(amount)) {
				info += " | " + amount.setScale(2, RoundingMode.HALF_EVEN) + " " + kb.getCurrency().getCurrency();
			}
			info += " | Должность: Дилер";
			fd.setInfo(info);
			
			fd.setRate(rate);
			fd.setWsumm(amount);
			BigDecimal ds = fd.getWsumm().divide(fd.getRate(), 12, RoundingMode.HALF_EVEN);
			fd.setDsumm(ds);
			
			// Construct FinEntries
			Integer pk = 1;
			List<FinEntry> feList = new ArrayList<>();
			FinEntry fe = new FinEntry(FinEntry.DC_DEBET);
			fe = formFinEntry(fd);
			fe.setIsTovar(false);
			fe.setEntrycount(2);
			fe.setFinActType(new FinActivityType(FinActivityType.TYPE_OPERATIVE));
			fe.setInfo(info);
			fe.setCashflowStatement(new FinCashflowStatement(FinCashflowStatement.CF_STA_PREMI_68));
			fe.setKassaBank(kb);
			
			// Debet 3350 & for Credit 1010.
			FinGlAccount dtAccount = new FinGlAccount(FinGlAccount.P_ST_PAYROLL_DEBT_3350);
			fe.setPostkey(pk.toString());
			fe.setWsumm(fd.getWsumm());
			fe.setDsumm(fd.getDsumm());
			FinEntry feDb = fe.clone();
			feDb.setDc(FinEntry.DC_DEBET);
			feDb.setGlAccount(dtAccount);
			feDb.setParty(fd.getParty());
			
			FinEntry feCr = fe.clone();
			feCr.setDc(FinEntry.DC_CREDIT);
			feCr.setGlAccount(new FinGlAccount(FinGlAccount.A_ST_CASH_ON_HAND_1010));
			
			feList.add(feDb);
			feList.add(feCr);
			
			fd.setFinEntries(feList);
			return fd;
		} catch (Exception e) {
			throw e;
		}
	}
	
	private static FinDoc constructNewCashOutFd(
			KassaBank kb, BigDecimal amount, Party party, Date date, BigDecimal rate, User user, String trCode, Integer depId) {
		try {
			FinDoc fd = new FinDoc();
			fd.setCompany(kb.getCompany());
			fd.setBranch(kb.getBranch());
			fd.setDepartment(new Department(depId));
			
//			fd.setForbuh(con.getForbuh());
			fd.setParty(party);
			
			fd.setDocDate(date);
			Calendar cal = Calendar.getInstance();
			cal.setTime(fd.getDocDate());
			fd.setYear(String.valueOf(cal.get(Calendar.YEAR)));
			fd.setMonth(cal.get(Calendar.MONTH) + 1);
			
			fd.setDcurrency(fd.getCompany().getCurrency());
			fd.setWcurrency(kb.getCurrency());
			fd.setRate(rate);
			fd.setDsummPaid(new BigDecimal(0));
			fd.setWsummPaid(new BigDecimal(0));
			
			fd.setUser(user);
			fd.setStorno(false);
			fd.setClosed(false);
			fd.setIsMain(true);
			fd.setTrcode(trCode);
			
			return fd;
		} catch (Exception e) {
			throw e;
		}
	}
}