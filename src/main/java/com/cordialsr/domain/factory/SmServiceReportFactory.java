package com.cordialsr.domain.factory;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cordialsr.domain.SmCall;
import com.cordialsr.domain.SmService;

public class SmServiceReportFactory {


	private static final DateFormat jd = new SimpleDateFormat("yyyy/MM/dd");
	
	public static String createExcelFile (List<SmService> services, ArrayList<SmCall> calls, Date sDate) throws Exception {
		
		String[] moun = {"January", "February", "March", "April", "May", "June", "July ", "August", "September", "October", "November", "December"};	
		Date date = new Date();
		
		 XSSFWorkbook workbook = new XSSFWorkbook();
		 
		 Object[][] datatypes = {
	                {"1", "Bad"},
	                {"3", "Good"},
	                {"5", "Excellent"}
	        };

	        XSSFSheet sheet = workbook.createSheet(moun[sDate.getMonth()] + " Report");
	        sheet.setColumnWidth(1, 8000);
	        sheet.setColumnWidth(2, 10000);
	        sheet.setColumnWidth(3, 4000);
	        sheet.setColumnWidth(4, 4000);
	        sheet.setColumnWidth(5, 4000);
	        sheet.setColumnWidth(6, 4000);
	        sheet.setColumnWidth(7, 4000);

	        sheet.setColumnWidth(9, 4000);
	        sheet.setColumnWidth(10, 10000);
	        sheet.setColumnWidth(11, 5000);
	        
	        XSSFFont mainfont = workbook.createFont();
	        mainfont.setFontHeightInPoints((short) 18);
	        mainfont.setBold(true);
	        
	        XSSFFont font = workbook.createFont();
	        font.setFontHeightInPoints((short) 13);
	        font.setFontName("IMPACT");
	        
	        XSSFFont cellfont = workbook.createFont();
	        cellfont.setFontHeightInPoints((short) 10);
	        
	        XSSFFont totalfont = workbook.createFont();
	        totalfont.setFontHeightInPoints((short) 12);
	        totalfont.setColor(HSSFColor.WHITE.index);
	        
	        XSSFCellStyle mainStyle = workbook.createCellStyle();
	        mainStyle.setFont(mainfont);
	        
	        XSSFCellStyle caremanStyle = workbook.createCellStyle();
	        caremanStyle.setFillForegroundColor(IndexedColors.LEMON_CHIFFON.getIndex());
	        caremanStyle.setFillPattern(CellStyle.SOLID_FOREGROUND); 
	        caremanStyle.setAlignment(caremanStyle.ALIGN_CENTER);
	        caremanStyle.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
	        caremanStyle.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
	        caremanStyle.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
	        caremanStyle.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
	        caremanStyle.setFont(font);
	        
	        XSSFCellStyle descripStyle = workbook.createCellStyle();
	        descripStyle.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
	        descripStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
	        descripStyle.setAlignment(descripStyle.ALIGN_CENTER);
	        descripStyle.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
	        descripStyle.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
	        descripStyle.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
	        descripStyle.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
	        descripStyle.setFont(font);
	        
	        XSSFCellStyle cellsStyle = workbook.createCellStyle();
	        cellsStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
	        cellsStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
	        cellsStyle.setAlignment(cellsStyle.ALIGN_CENTER);
	        cellsStyle.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
	        cellsStyle.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
	        cellsStyle.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
	        cellsStyle.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
	        cellsStyle.setFont(cellfont);
	        
	        XSSFCellStyle totalsStyle = workbook.createCellStyle();
	        totalsStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
	        totalsStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
	        totalsStyle.setAlignment(cellsStyle.ALIGN_CENTER);
	        totalsStyle.setFont(totalfont);
	        
	        Row headRow = sheet.createRow(0);
	        Row heRow = sheet.createRow(9);
	        
	        Cell headCell = headRow.createCell(1);
	        headCell.setCellValue((String) "Careman Happy Call Report");
	        headCell.setCellStyle(mainStyle);
	        
	        Cell cellDate = headRow.createCell(7);
	        cellDate.setCellValue(jd.format(date));
	        
	        
	        Cell cellHeaDes = headRow.createCell(9);
	        cellHeaDes.setCellValue((String) "Grade Description");
	        	        
	        Row hrow = sheet.createRow(2);
	        Cell cellbra = hrow.createCell(1);
	        cellbra.setCellValue((String) "Branch");
	        cellbra.setCellStyle(caremanStyle);
	        Cell cellcar = hrow.createCell(2);
	        cellcar.setCellValue((String) "Careman");
	        cellcar.setCellStyle(caremanStyle);
	        Cell cellunc = hrow.createCell(3);
	        cellunc.setCellValue((String) "Unconfirmed");
	        cellunc.setCellStyle(caremanStyle);
	        Cell cellcon = hrow.createCell(4);
	        cellcon.setCellValue((String) "Confirmed");
	        cellcon.setCellStyle(caremanStyle);
	        Cell celltotq = hrow.createCell(5);
	        celltotq.setCellValue((String) "Total Quantity");
	        celltotq.setCellStyle(caremanStyle);
	        Cell celltotg = hrow.createCell(6);
	        celltotg.setCellValue((String) "Total Grade");
	        celltotg.setCellStyle(caremanStyle);
	        Cell cellave = hrow.createCell(7);
	        cellave.setCellValue((String) "Av.Grade");
	        cellave.setCellStyle(caremanStyle);
	        
	        Cell cellgra = hrow.createCell(9);
	        cellgra.setCellValue((String) "Grade");
	        cellgra.setCellStyle(descripStyle);
	        Cell celldes = hrow.createCell(10);
	        celldes.setCellValue((String) "Description");
	        celldes.setCellStyle(descripStyle);
	        
	        int rowNum = 3;
	        
	        for (Object[] obj: datatypes) {
	        	 Row row = sheet.createRow(rowNum++);
	        	 int colNum = 9;
	        	 for (Object data: obj) {
	        		 Cell cell = row.createCell(colNum++);
	        		 if (data instanceof String) {
	        			 cell.setCellValue((String) data);
	        		 } else {
	        			 cell.setCellValue((String) "Error");
	        		 }
	        		 cell.setCellStyle(cellsStyle);
	        	 }
	        }
	        
	        Cell cellOpe = heRow.createCell(9);
	        cellOpe.setCellValue((String) "Operator Calls");
	        
	        rowNum = 12;
	        
	        Row row12 = sheet.createRow(11);
	        
        	Cell cellInd12 = row12.createCell(9);
        	cellInd12.setCellValue("#");
        	cellInd12.setCellStyle(caremanStyle);
        	
        	Cell cellOper12 = row12.createCell(10);
        	cellOper12.setCellValue("Operator"); 
        	cellOper12.setCellStyle(caremanStyle);
        	
        	Cell cellQuan12 = row12.createCell(11);
        	cellQuan12.setCellValue("Call Quantity"); 
        	cellQuan12.setCellStyle(caremanStyle);
	        
        	Integer sumOfCalls = 0;
        	
	        for (SmCall call: calls) {
	        	Row row = sheet.createRow(rowNum++);
	        	Cell cellInd = row.createCell(9);
	        	cellInd.setCellValue(rowNum - 12); 
	        	cellInd.setCellStyle(cellsStyle);
	        	
	        	Cell cellOper = row.createCell(10);
	        	cellOper.setCellValue((String) call.getUser().getLastName() + " " + call.getUser().getFirstName()); 
	        	cellOper.setCellStyle(cellsStyle);
	        	
	        	Cell cellQuan = row.createCell(11);
	        	cellQuan.setCellValue((Integer)call.getId()); 
	        	cellQuan.setCellStyle(cellsStyle);
	        	
	        	sumOfCalls += call.getId();
	        }
	        
	        Row totalCallrow = sheet.createRow(rowNum);
        	Cell cellInd = totalCallrow.createCell(10);
        	cellInd.setCellValue((String) "TOTAL:"); 
        	cellInd.setCellStyle(totalsStyle);
        	
        	Cell cellOper = totalCallrow.createCell(11);
        	cellOper.setCellValue((Integer) sumOfCalls); 
        	cellOper.setCellStyle(totalsStyle);
        	
        	Cell cellQuan = totalCallrow.createCell(9); 
        	cellQuan.setCellStyle(totalsStyle);
        	
	        rowNum = 3;

	        Double sumOfUnc = 0.0;
	        Double sumOfCon = 0.0;
	        Double sumOfTot = 0.0;
	        Double sumOfGra = 0.0; 
	        
	        for (SmService service: services) {
	        	Row row = null;
	        	if (sheet.getRow(rowNum) == null) {
	        		row = sheet.createRow(rowNum++);
	        	} else {
	        		row = sheet.getRow(rowNum++);
	        	}
	            
	            Cell cell = row.createCell(0);
	            cell.setCellValue((Integer) (rowNum - 3));
	            cell.setCellStyle(cellsStyle);
	            
	            Cell bracell = row.createCell(1);
	            bracell.setCellValue((String) service.getBranch().getBranchName());
	            bracell.setCellStyle(cellsStyle);
	            
	            Cell carcell = row.createCell(2);
		        carcell.setCellValue((String) service.getCaremanFio());
		        carcell.setCellStyle(cellsStyle);
		        
		        Cell unccell = row.createCell(3);
		        unccell.setCellValue(service.getSumm().subtract(service.getPaid()).doubleValue());
		        sumOfUnc += service.getSumm().subtract(service.getPaid()).doubleValue();
		        unccell.setCellStyle(cellsStyle);
		        
		        Cell concell = row.createCell(4);
		        concell.setCellValue(service.getPaid().doubleValue());
		        sumOfCon += service.getPaid().doubleValue();
		        concell.setCellStyle(cellsStyle);
		        
		        Cell totqcell = row.createCell(5);
		        totqcell.setCellValue(service.getSumm().doubleValue());
		        sumOfTot += service.getSumm().doubleValue();
		        totqcell.setCellStyle(cellsStyle);
		        
		        Cell totgcell = row.createCell(6);
		        if (service.getMark() == null) {
		        	totgcell.setCellValue((Integer) 0);
		        } else {
		        	totgcell.setCellValue(service.getMark().doubleValue());
		        	sumOfGra += service.getMark().doubleValue();
		        }
		        totgcell.setCellStyle(cellsStyle);
		        
		        Cell avecell = row.createCell(7);
		        if (service.getMark() == null || service.getPaid() == null || service.getPaid().equals(new BigDecimal(0))) {
		        	avecell.setCellValue((Integer) 0);
		        } else {
		        	Double c = service.getMark().doubleValue() / service.getPaid().doubleValue();
		        	Double tot = new BigDecimal(c).setScale(2, RoundingMode.HALF_UP).doubleValue();
		        	avecell.setCellValue((Double) tot);
		        }
		        avecell.setCellStyle(cellsStyle);
	        }
	        
	        Row row = null;
        	if (sheet.getRow(rowNum) == null) {
        		row = sheet.createRow(rowNum++);
        	} else {
        		row = sheet.getRow(rowNum++);
        	}
	        
	        Cell cell = row.createCell(1);
	        cell.setCellValue("TOTAL:");
            cell.setCellStyle(totalsStyle);
      

	        Cell cellCareman = row.createCell(0);
	        cellCareman.setCellStyle(totalsStyle);
            
	        Cell cellBranch = row.createCell(2);
	        cellBranch.setCellStyle(totalsStyle);
            
	        Cell unccell = row.createCell(3);
	        unccell.setCellValue(sumOfUnc);
	        unccell.setCellStyle(totalsStyle);
	        
	        Cell concell = row.createCell(4);
	        concell.setCellValue(sumOfCon);
	        concell.setCellStyle(totalsStyle);
	        
	        Cell totqcell = row.createCell(5);
	        totqcell.setCellValue(sumOfTot);
	        totqcell.setCellStyle(totalsStyle);
	        
	        Cell totgcell = row.createCell(6);
	        totgcell.setCellValue(sumOfGra);
	        totgcell.setCellStyle(totalsStyle);
	        
	        Cell avecell = row.createCell(7);
	        if (sumOfGra == 0.0 || sumOfCon == 0.0) {
	        	avecell.setCellValue((Integer) 0);
	        } else {
	        	Double c = sumOfGra / sumOfCon;
	        	Double tot = new BigDecimal(c).setScale(2, RoundingMode.HALF_UP).doubleValue();
	        	avecell.setCellValue((Double) tot);
	        }
	        avecell.setCellStyle(totalsStyle);
	        
		try {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				workbook.write(baos);
				workbook.close();
				byte[] file =  baos.toByteArray();
				String base64 = DatatypeConverter.printBase64Binary(file);
				return base64;
	        } catch (FileNotFoundException e) {
	            throw e;
	        } catch (IOException e) {
	        	throw e;
	        } catch(Exception ex) {
			throw ex;
		}
		
	}
	
}
