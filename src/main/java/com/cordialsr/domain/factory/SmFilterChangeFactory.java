package com.cordialsr.domain.factory;

import java.util.List;

import com.cordialsr.domain.Address;
import com.cordialsr.domain.PhoneNumber;
import com.cordialsr.domain.SmFilterChange;

public class SmFilterChangeFactory {

	public static List<SmFilterChange> createFilterChangeDto(List<SmFilterChange> fcList) throws Exception {
		try {
			for (SmFilterChange fc: fcList) {
				if (fc.getContract().getCareman() != null && fc.getContract().getCareman().getParty() != null) {
					fc.setCaremanFio(fc.getContract().getCareman().getParty().getFullFIO());	
					fc.setCustomerIin(fc.getContract().getCustomer().getIinBin());
				}
				int paid = fc.getContract().getPaid() != null ? fc.getContract().getPaid().intValue() : 0;				
				int cost = fc.getContract().getCost() != null ? fc.getContract().getCost().intValue() : 0;
				fc.setCurrency(fc.getContract().getCurrency() != null ? fc.getContract().getCurrency().getCurrency() : "");
				fc.setPayment(cost - paid);
				fc.setExploiterFio(fc.getContract().getExploiter().getFullFIO());
				fc.setSerialNumber(fc.getContract().getInventorySn());
				fc.setSdate(fc.getContract().getDateSigned());
				
				if (fc.getContract() != null) {
					Address adr = fc.getContract().getAddrFact();
					if (adr != null) {
						fc.setAddress(adr.getAddrString());						
					}					
										
					String pn = "";
					for (PhoneNumber ph: fc.getContract().getExploiter().getPhoneNumbers()) {
						if (ph.getPhNumber() != null && ph.getPhNumber().length() > 0) {
							pn += (pn.length() > 1) ? ", " : "";
							pn += ph.getPhNumber();
						}
					}
					fc.setPhone(pn);
				}
				
				fc.setContract(null);
			}
			
			return fcList;
		} catch (Exception ex) {
			throw ex;
		}
	}

}
