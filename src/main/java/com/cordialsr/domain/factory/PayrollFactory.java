package com.cordialsr.domain.factory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.cordialsr.Dao.ContractAwardScheduleDao;
import com.cordialsr.domain.AwardType;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractAwardSchedule;
import com.cordialsr.domain.ContractPaymentSchedule;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.Payroll;
import com.cordialsr.domain.Position;
import com.cordialsr.domain.security.Transaction;
import com.cordialsr.general.GeneralUtil;

public class PayrollFactory {

	@Autowired
	ContractAwardScheduleDao cawDao;
	
	public static Payroll constructPayrollFromContract(Contract con, ContractAwardSchedule caw, Employee empl) throws Exception {
		try {
			
			String info = caw.getPosition().getName() + " | ";
			String conType = (con.getIsRent()) ? "Аренды" : "Продажи";
			info += "Дог. " + conType + " №: " + con.getContractNumber();
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(caw.getDateSchedule());
			Integer month = cal.get(Calendar.MONTH) + 1;
			Integer year = cal.get(Calendar.YEAR);
			
			Payroll newPrl = formPayrollFromEmployee(empl);
			newPrl.setContract(con);
			newPrl.setTrCode(Transaction.TRCODE_NEW_CONTRACT);
			newPrl.setUser(con.getRegisteredUser());
			newPrl.setConAward(caw);
			newPrl.setPayrollDate(caw.getDateSchedule());
			newPrl.setSum(caw.getSumm());
			newPrl.setAccrued(new BigDecimal(0));
			newPrl.setCurrency(caw.getCurrency());
			newPrl.setMonth(month);
			newPrl.setYear(year);
			newPrl.setInfo(info);
			
			String kind = (caw.getAwardType().getName().equals(AwardType.TYPE_BONUS)) 
					? Payroll.KIND_BONUS 
							: (caw.getAwardType().getName().equals(AwardType.TYPE_PREMI)) 
							? Payroll.KIND_PREMI : null;
			
			newPrl.setKind(kind);
			
			return newPrl;
		} catch(Exception e) {
			throw e;
		}
	}
	
	public static Payroll formPayrollFromEmployee(Employee e) {
		Payroll newPrl = new Payroll();
		newPrl.setEmployee(e);
		newPrl.setParty(e.getParty());
		newPrl.setCompany(e.getCompany());
		newPrl.setBranch(e.getBranch());
		newPrl.setSum(e.getSalary());
		newPrl.setCurrency(e.getCurrency());
		// newPrl.setKind(kind);
		// newPrl.setPostedDate(new Date());
		newPrl.setApproved(false);
		newPrl.setPosted(false);
		return newPrl;
	}
	
	public static Employee getEmployeeFromContract(Contract c, Position p) {
		Employee e = null;
		switch (p.getId()) {
			case Position.POS_DEALER: 
				e = c.getDealer();
				break;
			case Position.POS_COORDINATOR: 
				e = c.getCoordinator();
				break;
			case Position.POS_CAREMAN: 
				e = c.getCareman();
				break;
			case Position.POS_DEMOSEC: 
				e = c.getDemosec();
				break;
			case Position.POS_DIRECTOR: 
				e = c.getDirector();
				break;
			case Position.POS_FITTER: 
				e = c.getFitter();
				break;
			case Position.POS_MANAGER: 
				e = c.getManager();
				break;
		}
		return e;
	}
	
	public static String generateInfo(Payroll p, Integer porder) {
		try {
			String info = p.getInfo();
			if (info == null) info = "";
			// SimpleDateFormat f = new SimpleDateFormat("dd-mm-yyyy");
			String at = "";
			switch (p.getKind()) {
				case Payroll.KIND_SALARY : 
					at = "ЗП";
					break;
				case Payroll.KIND_PREMI : 
					at = "ПРЕМИЯ";
					break;
				case Payroll.KIND_BONUS : 
					at = "БОНУС";
					break;
			}
			info += info.length() > 0 ? " | " : "";
			info += at;
			if (p.getKind().equals(Payroll.KIND_SALARY) && !GeneralUtil.isEmptyInteger(p.getMonth()) && !GeneralUtil.isEmptyInteger(p.getYear())) {
				info += " за " + p.getMonth() + "-" + p.getYear();
			}
			if (!GeneralUtil.isEmptyBigDecimal(p.getSum()))
					info += " | " + p.getSum().setScale(2, RoundingMode.HALF_EVEN) + " " + p.getCurrency().getCurrency();
			
			if (porder != null && porder > 0) info += " | " + porder + " " + at;
			
			
			return info;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// Check for Contract overdue sum existence (2nd option)
	public static boolean conOdSumExist(Contract con, Date de) {
		for (ContractPaymentSchedule ps : con.getPaymentSchedules()) {
			if (ps.getPaymentDate().compareTo(de) <= 0
					&& ps.getSumm().compareTo(ps.getPaid()) > 0) {
				return true;
			}
		}
		return false;
	}
	
	public static Payroll findPostedPremiPayroll(List<Payroll> prlL, ContractAwardSchedule caw) {
		for (Payroll p: prlL) {
			if (p.getConAward().getId().equals(caw.getId())) {
				return p;
			}
		}
		return null;
	}
	
	public static Payroll payrollContains(List<Payroll> prlL, Long emplId) {
		for (Payroll p: prlL) {
			if (p.getEmployee().getId().equals(emplId)) {
				return p;
			}
		}
		return null;
	}
}
