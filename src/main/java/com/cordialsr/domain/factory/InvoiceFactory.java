package com.cordialsr.domain.factory;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.cordialsr.domain.InvItemSn;
import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.InvoiceItem;
import com.cordialsr.domain.InvoiceStatus;
import com.cordialsr.domain.StockIn;
import com.cordialsr.domain.StockOut;
import com.cordialsr.dto.InstoreDto;
import com.cordialsr.dto.ReportDto;

public class InvoiceFactory {
	
	public static InstoreDto constructImportInvoice(InstoreDto instore, Invoice invoice) throws Exception {
		try {
			if (invoice.getInvoiceStatus().getName().equals(InvoiceStatus.STATUS_NEW)) {
				return instore;
			} else {
				instore = checkReportInventory(instore, invoice);
				if (invoice.getInvoiceStatus().getName().equals(InvoiceStatus.STATUS_PROCESS)) {
					instore = addForTransferIn(instore, invoice);
				} else if (invoice.getInvoiceStatus().getName().equals(InvoiceStatus.STATUS_CLOSED)) {
					instore = addForPosting(instore, invoice);
				}
				
				return instore;
			}
		} catch(Exception ex) {
			throw ex;
		}
	}
	
	public static InstoreDto constructTransferInvoice(InstoreDto instore, Invoice invoice, int count) throws Exception {
		try {
			if (invoice.getInvoiceStatus().getName().equals(InvoiceStatus.STATUS_NEW)) {
				return instore;
			} else {
				instore = checkReportInventory(instore, invoice);
				
				if (invoice.getInvoiceStatus().getName().equals(InvoiceStatus.STATUS_PROCESS)) {
					if (count == 1) {instore = addForTransferIn(instore, invoice);}
					if (count == 2) {instore = addForTransferOut(instore, invoice);}
				} else if (invoice.getInvoiceStatus().getName().equals(InvoiceStatus.STATUS_CLOSED)) {
					if (count == 1) {instore = addForPosting(instore, invoice);}
					if (count == 2) {instore = addForGetting(instore, invoice);}
				}
				
				return instore;
			}
		} catch(Exception ex) {
			throw ex;
		}
	}
	
	
	public static InstoreDto constructReturnInvoice(InstoreDto instore, Invoice invoice) throws Exception {
		try {
				
			if (invoice.getInvoiceStatus().getName().equals(InvoiceStatus.STATUS_CLOSED)) {
				instore = checkReportInventory(instore, invoice);
				if (invoice.getDocId() == null) {
					instore = addForAccountReturn(instore, invoice);
				} else {
					instore = addForReturn(instore, invoice);
				}
			}	
			return instore;
			
		} catch(Exception ex) {
			throw ex;
		}
	}
	public static InstoreDto constructServiceInvoice(InstoreDto instore, Invoice invoice) throws Exception {
		try {
			
			if (invoice.getInvoiceStatus().getName().equals(InvoiceStatus.STATUS_CLOSED)) {
				instore = checkReportInventory(instore, invoice);
				instore = addForSellRent(instore, invoice);
			}
			
			return instore;
		} catch(Exception ex) {
			throw ex;
		}
	}
	
	public static InstoreDto constructSellInvoice(InstoreDto instore, Invoice invoice) throws Exception {
		try {
		
			instore = checkReportInventory(instore, invoice);
			instore = addForSellRent(instore, invoice);
			
			return instore;
		} catch(Exception ex) {
			throw ex;
		}
	}
	
	public static InstoreDto constructRentInvoice(InstoreDto instore, Invoice invoice) throws Exception {
		try {
			instore = checkReportInventory(instore, invoice);
			instore = addForSellRent(instore, invoice);
			
			return instore;
		} catch(Exception ex) {
			throw ex;
		}
	}
	
	public static InstoreDto constructAccountableInvoice(InstoreDto instore, Invoice invoice) throws Exception {
		try {
			
			if (invoice.getInvoiceStatus().getName().equals(InvoiceStatus.STATUS_NEW)) {
				return instore;
			}
			instore = checkReportInventory(instore, invoice);
			instore = addForAccountable(instore, invoice);
			
			return instore;
		} catch(Exception ex) {
			throw ex;
		}
	}
	
	public static InstoreDto constructWriteOffInvoice(InstoreDto instore, Invoice invoice) throws Exception {
		try {
			if (invoice.getInvoiceStatus().getName().equals(InvoiceStatus.STATUS_NEW)) {
				return instore;
			}
			instore = checkReportInventory(instore, invoice);
			instore = addForWriteOff(instore, invoice);
			
			return instore;
		} catch(Exception ex) {
			throw ex;
		}
	}
	
	/////////////////////////////////////////////////// Adding ReportDto with Inventory ......
	private static InstoreDto checkReportInventory(InstoreDto instore, Invoice invoice) {
		
		for (InvoiceItem ii: invoice.getInvoiceItems()) {
			Integer count = 0;
			for (ReportDto report: instore.getReports()) {
				if (report.getInventory().getId() == ii.getInventory().getId()) {
					count ++;
					break;
				}
			}
			if (count == 0) {
				ReportDto report = new ReportDto();
				
				Inventory inven = new Inventory();
				inven.setId(ii.getInventory().getId());
				inven.setName(ii.getInventory().getName());
				inven.setCode(ii.getInventory().getCode());
				
				report.setInventory(inven);
				report.setAccountable(new BigDecimal(0));
				report.setBalance(new BigDecimal(0));
				report.setGetting(new BigDecimal(0));
				report.setPosting(new BigDecimal(0));
				report.setReturned(new BigDecimal(0));
				report.setTransferIn(new BigDecimal(0));
				report.setTransferOut(new BigDecimal(0));
				report.setClient(new BigDecimal(0));
				report.setWriteOff(new BigDecimal(0));
				report.setAccountReturn(new BigDecimal(0));
				instore.getReports().add(report);
			}
		}
		
		return instore;
	}
	//////////////////////////////////////////// Adding for TransferIn ......
	
	private static InstoreDto addForTransferIn (InstoreDto instore, Invoice invoice) {
		
		for (InvoiceItem ii: invoice.getInvoiceItems()) {
			for (ReportDto rd: instore.getReports()) {
				if (ii.getInventory().getId() == rd.getInventory().getId()) {
					rd.setTransferIn(ii.getQuantity().add(rd.getTransferIn()));
					instore.setTransferIn(ii.getQuantity().add(instore.getTransferIn()));
					break;
				}
			}
		}
		return instore;
	}
	
	//////////////////////////////////////////// Adding for TransferOut ......
	
	private static InstoreDto addForTransferOut(InstoreDto instore, Invoice invoice) {
		
		for (InvoiceItem ii: invoice.getInvoiceItems()) {
			for (ReportDto rd: instore.getReports()) {
				if (ii.getInventory().getId() == rd.getInventory().getId()) {
					rd.setTransferOut(ii.getQuantity().add(rd.getTransferOut()));
					instore.setTransferOut(ii.getQuantity().add(instore.getTransferOut()));
					break;
				}
			}
		}
		return instore;
	}
	//////////////////////////////////////////// Adding for WriteOff ......
	
	private static InstoreDto addForWriteOff(InstoreDto instore, Invoice invoice) {
		
		for (InvoiceItem ii: invoice.getInvoiceItems()) {
			for (ReportDto rd: instore.getReports()) {
				if (ii.getInventory().getId() == rd.getInventory().getId()) {
					for (StockOut sk: ii.getChildStockOuts()) {
						rd.setWriteOff(sk.getQuantity().add(rd.getWriteOff()));
						instore.setWriteOff(sk.getQuantity().add(instore.getWriteOff()));
					}
					break;
				}
			}
		}
		return instore;
	}
	
	///////////////////////////////////////// Adding for Posting ........
	private static InstoreDto addForPosting (InstoreDto instore, Invoice invoice) {
		
		for (InvoiceItem ii: invoice.getInvoiceItems()) {
			for (ReportDto rd: instore.getReports()) {
				if (ii.getInventory().getId() == rd.getInventory().getId()) {
					rd.setPosting(ii.getQuantity().add(rd.getPosting()));
					instore.setPosting(ii.getQuantity().add(instore.getPosting()));
					break;
				}
			}
		}
		return instore;
	}
	
	private static InstoreDto addForGetting (InstoreDto instore, Invoice invoice) {
		
		for (InvoiceItem ii: invoice.getInvoiceItems()) {
			for (ReportDto rd: instore.getReports()) {
				if (ii.getInventory().getId() == rd.getInventory().getId()) {
					rd.setGetting(ii.getQuantity().add(rd.getGetting()));
					instore.setGetting(ii.getQuantity().add(instore.getGetting()));
					break;
				}
			}
		}
		return instore;
	}
	
	private static InstoreDto addForSellRent(InstoreDto instore, Invoice invoice) {
		
		for (InvoiceItem ii: invoice.getInvoiceItems()) {
			for (ReportDto rd: instore.getReports()) {
				if (ii.getInventory().getId() == rd.getInventory().getId()) {
				
					rd.setClient(ii.getQuantity().add(rd.getClient()));
					instore.setClient(ii.getQuantity().add(instore.getClient()));
					
					break;
				}
			}
		}
		return instore;
	}
	
	private static InstoreDto addForAccountable(InstoreDto instore, Invoice invoice) {
		
		for (InvoiceItem ii: invoice.getInvoiceItems()) {
			for (ReportDto rd: instore.getReports()) {
				if (ii.getInventory().getId() == rd.getInventory().getId()) {
					rd.setAccountable(ii.getQuantity().add(rd.getAccountable()));
					instore.setAccountable(ii.getQuantity().add(instore.getAccountable()));
					break;
				}
			}
		}
		return instore;
	}
	
	private static InstoreDto addForReturn(InstoreDto instore, Invoice invoice) {
		
		for (InvoiceItem ii: invoice.getInvoiceItems()) {
			for (ReportDto rd: instore.getReports()) {
				if (ii.getInventory().getId() == rd.getInventory().getId()) {
					rd.setReturned(ii.getQuantity().add(rd.getReturned()));
					instore.setReturned(ii.getQuantity().add(instore.getReturned()));
					break;
				}
			}
		}
		return instore;
	}
	
	private static InstoreDto addForAccountReturn(InstoreDto instore, Invoice invoice) {
		
		for (InvoiceItem ii: invoice.getInvoiceItems()) {
			for (ReportDto rd: instore.getReports()) {
				if (ii.getInventory().getId() == rd.getInventory().getId()) {
					rd.setAccountReturn(ii.getQuantity().add(rd.getAccountReturn()));
					instore.setAccountReturn(ii.getQuantity().add(instore.getAccountReturn()));
					break;
				}
			}
		}
		return instore;
	}

	public static Invoice constructReturnInvoiceService(Invoice invoice)throws Exception {
		try {
			Invoice newInv = new Invoice();
			newInv.setCompany(invoice.getCompany());
			newInv.setBranch(invoice.getBranch());
			newInv.setBranchImporter(invoice.getBranchImporter());
			newInv.setData(invoice.getData());
			newInv.setScopeType(invoice.getScopeType());
			newInv.setParty(invoice.getParty());
			newInv.setInvoiceStatus(new InvoiceStatus(InvoiceStatus.STATUS_CLOSED));
			newInv.setUserAdded(invoice.getUserAdded());
			newInv.setInvoiceType(Invoice.INVOICE_TYPE_RETURN);
			newInv.setDateCreated(invoice.getDateCreated());
			if (invoice.getDocId() != null) {
				newInv.setDocId(invoice.getDocId());
			}
			if (invoice.getDocno() != null) {
				newInv.setDocno(invoice.getDocno());
			}
			newInv.setStorno(false);
			ArrayList<InvoiceItem> invItem = new ArrayList<InvoiceItem>();
			for (InvoiceItem ii: invoice.getInvoiceItems()) {
				InvoiceItem invIt = new InvoiceItem();
				invIt.setId(null);
				invIt.setInvoice(newInv);
				invIt.setInventory(ii.getInventory());
				invIt.setQuantity(ii.getQuantity());
				invIt.setUnit(ii.getUnit());
				invIt.setChildStockIns(new ArrayList<StockIn>());
				invIt.setChildStockOuts(new ArrayList<StockOut>());
				invIt.setChildInvSns(new ArrayList<InvItemSn>());
				invItem.add(invIt);
			}
			newInv.setInvoiceItems(invItem);

			return newInv;
		} catch(Exception ex) {
			throw ex;
		}
	}
	
}