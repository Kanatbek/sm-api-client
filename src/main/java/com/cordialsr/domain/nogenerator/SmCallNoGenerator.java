package com.cordialsr.domain.nogenerator;

import com.cordialsr.domain.SmCall;

public class SmCallNoGenerator {

	public static String generateSmCallNo(SmCall call) {
		
		String callNo = "";
		String com = call.getCompany().getId().toString();

		String br = call.getBranch().getId().toString();
		if (br.length() < 2)
			br = "0" + br;
		
		String idHex = Long.toHexString(call.getId());
		for (int i = 0; idHex.length() < 7; i++) {
			idHex = "0" + idHex;
		}

		callNo = com + br + idHex.toUpperCase();
		return callNo;
	}

}
