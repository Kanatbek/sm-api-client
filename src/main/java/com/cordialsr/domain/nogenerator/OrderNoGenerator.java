package com.cordialsr.domain.nogenerator;

import com.cordialsr.domain.OrderHeader;

public class OrderNoGenerator {
	public static String generateOrderNo(OrderHeader order) {
		String invNo = "";
		String com = order.getCompany().getId().toString();
		// if (com.length() < 2) com = "0" + com;

		String br = order.getBranch().getId().toString();
		if (br.length() < 2)
			br = "0" + br;

		String idHex = Long.toHexString(order.getId());
		for (int i = 0; idHex.length() < 7; i++) {
			idHex = "0" + idHex;
		}

		invNo = com + br + idHex.toUpperCase();
		return invNo;
	}
}
