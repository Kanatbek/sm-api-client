package com.cordialsr.domain.nogenerator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.cordialsr.domain.FinDoc;

public class FinDocNoGenerator {

	public static String generateFinDocNo(FinDoc newFinDoc) {
		String docNo = "";
		String com = newFinDoc.getCompany().getId().toString();
		// if (com.length() < 2) com = "0" + com;

		String br = newFinDoc.getBranch().getId().toString();
		if (br.length() < 2)
			br = "0" + br;

		DateFormat df = new SimpleDateFormat("ddMM");
		Calendar today = Calendar.getInstance();
		// System.out.println("TODAY: " + today);
		String ddMM = df.format(today.getTime());
		// System.out.println("Formatted Date: " + ddMM);
		// String year = String.valueOf(today.get(Calendar.YEAR));

		String idHex = Long.toHexString(newFinDoc.getId());
		for (int i = 0; idHex.length() < 8; i++) {
			idHex = "0" + idHex;
		}

		docNo = com + br + ddMM + idHex.toUpperCase();
		return docNo;
	}
	
	public static String generateRefkey(FinDoc newFinDoc) {
		String refkey = "";
		refkey = newFinDoc.getDocno() + newFinDoc.getYear();
		return refkey;
	}
	
}
