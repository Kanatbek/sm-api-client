package com.cordialsr.domain.nogenerator;

import com.cordialsr.domain.SmService;

public class SmServiceNoGenerator {

	public static String generateServiceNo(SmService service) {
		String serNo = "";
		// String com = service.getContract().getId().toString();
		// if (com.length() < 2) com = "0" + com;

		String br = service.getBranch().getId().toString();
		if (br.length() < 2)
			br = "0" + br;

		String idHex = Long.toHexString(service.getId());
		for (int i = 0; idHex.length() < 5; i++) {
			idHex = "0" + idHex;
		}

		serNo = br + idHex.toUpperCase();
		return serNo;
	}
	
}
