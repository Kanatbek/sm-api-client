package com.cordialsr.domain.nogenerator;

import com.cordialsr.domain.Invoice;

public class InvoiceNoGenerator {

	public static String generateInvoiceNo(Invoice newInvoice) {
		String invNo = "";
		String com = newInvoice.getCompany().getId().toString();
		// if (com.length() < 2) com = "0" + com;

		String br = newInvoice.getBranch().getId().toString();
		if (br.length() < 2)
			br = "0" + br;

		String idHex = Long.toHexString(newInvoice.getId());
		for (int i = 0; idHex.length() < 7; i++) {
			idHex = "0" + idHex;
		}

		invNo = com + br + idHex.toUpperCase();
		return invNo;
	}
}
