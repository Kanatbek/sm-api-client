package com.cordialsr.domain.nogenerator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.cordialsr.domain.Contract;

public class ContractNoGenerator {

	public static String generateContractNo(Contract newContract) {
		String docNo = "";
		String com = newContract.getCompany().getId().toString();
		// if (com.length() < 2) com = "0" + com;

		String br = newContract.getBranch().getId().toString();
		if (br.length() < 2)
			br = "0" + br;

		DateFormat df = new SimpleDateFormat("ddMM");
		Calendar today = Calendar.getInstance();
		// System.out.println("TODAY: " + today);
		// String ddMM = df.format(today.getTime());
		// System.out.println("Formatted Date: " + ddMM);
		// String year = String.valueOf(today.get(Calendar.YEAR));

		String idHex = Long.toHexString(newContract.getId());
		for (int i = 0; idHex.length() < 6; i++) {
			idHex = "0" + idHex;
		}

//		docNo = com + br + ddMM + idHex.toUpperCase();
		docNo = com + br + "-" + idHex.toUpperCase();
		return docNo;
	}
	//generateRefkey
	
}
