package com.cordialsr.domain.nogenerator;

import com.cordialsr.domain.SmEnquiry;

public class SmEnquiryNoGenerator {

	public static String generateSmEnquiryNo(SmEnquiry newEnquiry) {
		String enqNo = "";
		String com = newEnquiry.getCompany().getId().toString();

		String br = newEnquiry.getBranch().getId().toString();
		if (br.length() < 2)
			br = "0" + br;
		
		String idHex = Long.toHexString(newEnquiry.getId());
		for (int i = 0; idHex.length() < 7; i++) {
			idHex = "0" + idHex;
		}

		enqNo = com + br + idHex.toUpperCase();
		return enqNo;
	}


}
