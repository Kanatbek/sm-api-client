package com.cordialsr.domain;
// Generated 23.05.2017 12:18:03 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "sm_con_sales_rem_ps")
public class SmConSalesPs implements Cloneable {

	private Integer id;
	private Date period;
	private SmContract smContract;
	private BigDecimal amount;
		
	public SmConSalesPs() {
		super();
	}

	
	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "period", length = 10)
	public Date getPeriod() {
		return period;
	}

	public void setPeriod(Date period) {
		this.period = period;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "contract", referencedColumnName = "CONTRACT_NUMBER")
	public SmContract getSmContract() {
		return smContract;
	}

	public void setSmContract(SmContract smContract) {
		this.smContract = smContract;
	}
	
	
	@Column(name = "amount", precision = 16, scale = 4)
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
		
}
