package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "invoice_trace")
public class InvoiceTrace implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Invoice invoice;
	private Location location;
	private User user;
	private Date data;
	private String info;

	public InvoiceTrace() {
	}

	public InvoiceTrace(Invoice invoice, Location location) {
		this.invoice = invoice;
		this.location = location;
	}

	public InvoiceTrace(Invoice invoice, Location location, User user, Date data, String info) {
		this.invoice = invoice;
		this.location = location;
		this.user = user;
		this.data = data;
		this.info = info;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invoice_id", nullable = false)
	public Invoice getInvoice() {
		return this.invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "location_id", nullable = false)
	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user")
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data", length = 10)
	public Date getData() {
		return this.data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	@Column(name = "info")
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((invoice == null || invoice.getId() == null) ? 0 : invoice.getId().hashCode());
		result = prime * result + ((location == null || location.getId() == null) ? 0 : location.getId().hashCode());
		result = prime * result + ((user == null || user.getUserid() == null) ? 0 : user.getUserid().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvoiceTrace other = (InvoiceTrace) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (info == null) {
			if (other.info != null)
				return false;
		} else if (!info.equals(other.info))
			return false;
		if (invoice == null) {
			if (other.invoice != null)
				return false;
		} else if (!invoice.getId().equals(other.invoice.getId()))
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.getId().equals(other.location.getId()))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.getUserid().equals(other.user.getUserid()))
			return false;
		return true;
	}

	@Override
	public InvoiceTrace clone() throws CloneNotSupportedException {
		return (InvoiceTrace) super.clone();
	}
	
}
