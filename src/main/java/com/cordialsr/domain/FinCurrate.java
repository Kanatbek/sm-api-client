package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.cordialsr.domain.security.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "fin_currate")
public class FinCurrate implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Branch branch;
	private City city;
	private Company company;
	private Country country;
	private Currency mainCurrency;
	private Currency secCurrency;
	private Region region;
	private Scope scope;
	private ScopeType scopeType;
	private BigDecimal rate;
	private Date rdate;
	private Date cpuDate;	
	private User userAuthor;

	public FinCurrate() {
	}

	public FinCurrate(ScopeType scopeType) {
		this.scopeType = scopeType;
	}

	public FinCurrate(Branch branch, City city, Company company, Country country, Currency mainCurrency,
			Currency secCurrency, Region region, Scope scope, ScopeType scopeType, BigDecimal rate,
			Date data) {
		this.branch = branch;
		this.city = city;
		this.company = company;
		this.country = country;
		this.mainCurrency = mainCurrency;
		this.secCurrency = secCurrency;
		this.region = region;
		this.scope = scope;
		this.scopeType = scopeType;
		this.rate = rate;
		this.rdate = data;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "branch")
	public Branch getBranch() {
		return this.branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "city")
	public City getCity() {
		return this.city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "company")
	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "country")
	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "main_currency")
	public Currency getMainCurrency() {
		return this.mainCurrency;
	}

	public void setMainCurrency(Currency mainCurrency) {
		this.mainCurrency = mainCurrency;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sec_currency")
	public Currency getSecCurrency() {
		return this.secCurrency;
	}

	public void setSecCurrency(Currency secCurrency) {
		this.secCurrency = secCurrency;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "region")
	public Region getRegion() {
		return this.region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "scope")
	public Scope getScope() {
		return this.scope;
	}

	public void setScope(Scope scope) {
		this.scope = scope;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "scope_type", nullable = false)
	public ScopeType getScopeType() {
		return this.scopeType;
	}

	public void setScopeType(ScopeType scopeType) {
		this.scopeType = scopeType;
	}

	@Column(name = "rate", precision = 21, scale = 12)
	public BigDecimal getRate() {
		return this.rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "data", length = 10)
	public Date getRdate() {
		return this.rdate;
	}

	public void setRdate(Date data) {
		this.rdate = data;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "cpu_date", length = 10)
	public Date getCpuDate() {
		return cpuDate;
	}

	public void setCpuDate(Date cpuDate) {
		this.cpuDate = cpuDate;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_author")
	public User getUserAuthor() {
		return userAuthor;
	}

	public void setUserAuthor(User userAuthor) {
		this.userAuthor = userAuthor;
	}
}
