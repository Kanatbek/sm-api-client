package com.cordialsr.domain;
// Generated 03.08.2017 11:54:04 by stalbekr@gmail.com

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "service_category")
public class ServiceCategory implements java.io.Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static Integer CAT_GREEN = 1;
	public static Integer CAT_YELLOW = 2;
	public static Integer CAT_RED = 3;
	public static Integer CAT_BLACK = 4;
	
	private Integer id;
	private String name;
	private String info;

	public ServiceCategory() {
	}

	public ServiceCategory(int id) {
		this.id = id;
	}

	public ServiceCategory(int id, String name, String info) {
		this.id = id;
		this.name = name;
		this.info = info;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", length = 45)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "info")
	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	// ****************************************************************************************************
	
	@Override
	public String toString() {
		return "ServiceCategory [id=" + id + ", name=" + name + ", info=" + info + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((info == null) ? 0 : info.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServiceCategory other = (ServiceCategory) obj;
		if (id != other.id)
			return false;
		if (info == null) {
			if (other.info != null)
				return false;
		} else if (!info.equals(other.info))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public ServiceCategory clone() throws CloneNotSupportedException {
		return (ServiceCategory) super.clone();
	}

}
