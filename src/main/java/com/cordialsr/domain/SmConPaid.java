package com.cordialsr.domain;
// Generated 23.05.2017 12:18:03 by stalbekr@gmail.com

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "sm_con_paid_sum")
public class SmConPaid implements Cloneable {

	private Integer id;
	private String contractNumber;
	private BigDecimal amount;
	private String currency;
		
	public SmConPaid() {
		super();
	}

	public SmConPaid(Integer id, String contractNumber, BigDecimal amount,
				String currency) {
		super();
		this.id = id;
		this.contractNumber = contractNumber;
		this.amount = amount;
		this.currency = currency;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "CONTRACT_NUMBER", nullable = false, length = 20)
	public String getContractNumber() {
		return this.contractNumber;
	}
	
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	@Column(name = "amount", precision = 16, scale = 4)
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Column(name = "currency", length = 10)
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
		
}
