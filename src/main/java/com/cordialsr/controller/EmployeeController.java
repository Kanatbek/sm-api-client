package com.cordialsr.controller;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.Dao.BranchDao;
import com.cordialsr.Dao.CompanyDao;
import com.cordialsr.Dao.CurrencyDao;
import com.cordialsr.Dao.DepartmentDao;
import com.cordialsr.Dao.EmployeeDao;
import com.cordialsr.Dao.PartyDao;
import com.cordialsr.Dao.PositionDao;
import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Department;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.Position;
import com.cordialsr.domain.reducer.ContractReducer;
import com.cordialsr.domain.reducer.EmployeeReducer;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.EmployeeService;

@RepositoryRestController
@RequestMapping(value = "employees")
public class EmployeeController  {
	
	private static final String RESPONSE_REASON_HEADER = "REASON";
	
	@Autowired
	private EmployeeDao employeeDao;
	
	@Autowired
	private PartyDao partyDao;
	
	@Autowired
	private CompanyDao companyDao;
	
	@Autowired
	private BranchDao branchDao;
	
	@Autowired
	private DepartmentDao departmentDao;
	
	@Autowired
	private PositionDao positionDao;
	
	@Autowired
	private CurrencyDao currencyDao;
	
	@Autowired
	private EmployeeService emplService;
	
	@RequestMapping(value="/getEmpl", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Object> getEmpl(
				@RequestParam("cid") Integer cid,
	    		@RequestParam("bid") Integer bid,
	    		@RequestParam("pos") Integer pos,
	    		@RequestParam("dep") Integer dep
	    		) {
		try {
			List<Employee> empl = null;
			if (!GeneralUtil.isEmptyInteger(dep)) {
				empl = employeeDao.findAllByComBrDep(cid, bid, pos, dep);
			} else {
				empl = employeeDao.findAllByComBr(cid, bid, pos);
			}
			
			if (!empl.isEmpty()){
				return ResponseEntity.status(HttpStatus.OK).body(empl);
			} else {
				return ResponseEntity.status(HttpStatus.OK).body("Not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
	}
	
	 //**************************************************************************************************************************************

	
	@RequestMapping(value = "{employeeId}",
            method = {RequestMethod.PATCH, RequestMethod.PUT})	
	public @ResponseBody
    ResponseEntity<PersistentEntityResource> updateEmployee(@PathVariable("employeeId") Long employeeId,
                                                                @RequestBody Employee employeeBody,
                                                                PersistentEntityResourceAssembler persistentEntityResourceAssembler) {
        HttpStatus status = HttpStatus.OK;
        String reason = null;
        boolean headerNotEmpty = false;
        Employee changedEmployee = employeeDao.findOne(employeeId);
 
        if (employeeId != null) {
            if (changedEmployee == null) {
                return new ResponseEntity<>(
                        persistentEntityResourceAssembler.toResource(changedEmployee), HttpStatus.NO_CONTENT);
            }
            // contains values to be updated, passed via JSON
            Employee newEmployee = employeeBody;
            if (newEmployee != null) {
            		
            		
            		changedEmployee = update(changedEmployee, newEmployee);

            } else {
                reason = "not enough data to update the Employee [" + employeeId + "]";
            }
        }
 
        // We have a non-empty reason - we have denied to carry out an operation.
        // In this case we attach a custom header.
        headerNotEmpty = StringUtils.isNotBlank(reason);
        if (headerNotEmpty) {
            status = HttpStatus.NO_CONTENT; // should be treated as error code on the front-end side
        }
 
        // header string is supposed to be blank for successful operations
        HttpHeaders headers = new HttpHeaders();
        if (headerNotEmpty) {
            headers.add(RESPONSE_REASON_HEADER, reason);
        }
        return new ResponseEntity<>(
                persistentEntityResourceAssembler.toResource(changedEmployee),
                headers,
                status);
    }

	
	//**************************************************************************************************************************************

	public Employee updateEmployee(Employee employee) throws Exception {
		try {
//			EmployeeValidater.validateBasic(employee);
			return employeeDao.save(employee);
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	// *************************************************************************************************************************************
	
	
	 @Transactional
	    private Employee update(Employee changedEmployee, Employee newEmployee) {
	 
	    	// newParty is created based on JSON from request
	  
	    	try {
	    		changedEmployee = newEmployee.clone();
			} catch (CloneNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
	        if (newEmployee.getCompany() != null) {
	        	Company companyNew = companyDao.findOne(newEmployee.getCompany().getId());
	        	changedEmployee.setCompany(companyNew);
	        }
	        
	        if (newEmployee.getParty() != null) {
	        	Party partyNew = partyDao.findOne(newEmployee.getParty().getId());
	        	changedEmployee.setParty(partyNew);
	        }
	        
	        if (newEmployee.getBranch() != null) {
	        	Branch branchNew = branchDao.findOne(newEmployee.getBranch().getId());
	        	changedEmployee.setBranch(branchNew);
	        }
	        
	        if (newEmployee.getDepartment() != null) {
	        	Department depNew = departmentDao.findOne(newEmployee.getDepartment().getId());
	        	changedEmployee.setDepartment(depNew);
	        }
	        
	        if (newEmployee.getPosition() != null) {
	        	Position positionNew = positionDao.findOne(newEmployee.getPosition().getId());
	        	changedEmployee.setPosition(positionNew);
	        }
	        
	        if (newEmployee.getCurrency() != null) {
	        	Currency currencyNew = currencyDao.findOne(newEmployee.getCurrency().getCurrency());
	        	changedEmployee.setCurrency(currencyNew);
	        }

	        if(newEmployee.getAccountableTo() != null) {
	        	changedEmployee.setAccountableTo(newEmployee.getAccountableTo());
	        }
	        
	        if (newEmployee.getSalary() != null) {
	        changedEmployee.setSalary(newEmployee.getSalary());
	        }
	        changedEmployee = employeeDao.save(changedEmployee);
	        return changedEmployee;
	    }
	 
	 //**************************************************************************************************************************************
	 
	 @RequestMapping(value="/check", method = RequestMethod.GET,
				produces = MediaTypes.HAL_JSON_VALUE)
		@ResponseBody
		public ResponseEntity<Object> check(
					@RequestParam("empId") Long empId,
					@RequestParam("cId") Integer cId,
		    		@RequestParam("brId") Integer brId,
		    		@RequestParam("posId") Integer posId,
		    		@RequestParam("depId") Integer depId
		    		) {
			try {
				List<Employee> empl = null;
					empl = employeeDao.getSameEmpl(empId, cId, brId, depId, posId);	
					
					
					
				if (!empl.isEmpty()){
					return ResponseEntity.status(HttpStatus.OK).body(empl);
				} else {
					return ResponseEntity.status(HttpStatus.OK).body("Not found");
				}
			} catch (Exception e) {
				e.printStackTrace();
				return ResponseEntity
	                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
	                    .body(e.getMessage());
			}
		}
	
	 	
	 // ******************************************************************************************************************************************
	 
	 @RequestMapping(value = "/getEmployeeList", method = RequestMethod.GET,
				produces = MediaTypes.HAL_JSON_VALUE)
		@ResponseBody
	    public ResponseEntity<Object> getContractList(
	    		    @RequestParam(value="cid", required=true) Integer cid,
	    		    @RequestParam(value="bid", required=false) Integer bid,
	    		    @RequestParam(value="posId", required=false) Integer posId,
	    		    @RequestParam(value="sortBy", required=false) String sortBy,
	    		    @RequestParam(value="fired", required=false) Boolean fired,
	    		    @RequestParam(value="filter", required=false) String filter,
	    		    Pageable pageable
	    		) {
			try {
				
				Page<Employee> pages = emplService.getEmployeeList(cid, bid, posId, sortBy, fired, filter, pageable);
				List<Employee> emplL = new ArrayList<>();
				for (Employee empl : pages) {
					Employee c = EmployeeReducer.reduceMed(empl);
					emplL.add(c);
				}
				Page<Employee> res = new PageImpl<Employee>(emplL, pageable, pages.getTotalElements());  
				return ResponseEntity.status(HttpStatus.OK).body(res);
			} catch (Exception e) {
				e.printStackTrace();
				return ResponseEntity
	                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
	                    .body(e.getMessage());
			}
	    }
	 
	 
	 // **********************************************************************************************************************************************

		@RequestMapping(value = "/getHierarchyManagers", method = RequestMethod.GET,
				produces = MediaTypes.HAL_JSON_VALUE)
		@ResponseBody
	    public ResponseEntity<Object> getManagerList(
	    		    @RequestParam(value="cid", required=true) Integer cid,
	    		    @RequestParam(value="bid", required=true) Integer bid,
	    		    @RequestParam(value="dateStart", required=false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateStart,
	    		    @RequestParam(value="dateEnd", required=false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateEnd,
	    		    @RequestParam(value="filter", required=false) String filter,
	    		    Pageable pageable
	    		) {
			try {
				Page<Employee> employees = emplService.getAllManager(cid, bid, dateStart, dateEnd, filter, pageable); 
				
				List<Employee> emplL = new ArrayList<>();
				for (Employee empl : employees) {
					Employee c = EmployeeReducer.reduceMed(empl);
					emplL.add(c);
				}
				Page<Employee> res = new PageImpl<Employee>(emplL, pageable, employees.getTotalElements());  
				return ResponseEntity.status(HttpStatus.OK).body(res);
			} catch (Exception e) {
				e.printStackTrace();
				return ResponseEntity
	                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
	                    .body(e.getMessage());
			}
	    }
		
		
		 // **********************************************************************************************************************************************
		
		
		@RequestMapping(value = "/findDealerByManager", method = RequestMethod.GET,
				produces = MediaTypes.HAL_JSON_VALUE)
		@ResponseBody
	    public ResponseEntity<Object> getDealerContractList(
	    		    @RequestParam(value="emplId", required=true) Long emplId,
	    		    @RequestParam(value="dateStart", required=true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateStart,
	    		    @RequestParam(value="dateEnd", required=true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateEnd
	    		) {
			try {
				List<Employee> dealerList = emplService.getDealerList(emplId, dateStart, dateEnd);
				List<Employee> emplL = new ArrayList<>();
				for (Employee empl : dealerList) {
					Employee c = EmployeeReducer.reduceMin(empl);
					emplL.add(c);
				}
				return ResponseEntity.status(HttpStatus.OK).body(dealerList);
			} catch (Exception e) {
				e.printStackTrace();
				return ResponseEntity
	                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
	                    .body(e.getMessage());
			}
	    }
	 
	 

}
