package com.cordialsr.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.SmEnquiry;
import com.cordialsr.service.SmEnquiryService;

@RepositoryRestController
@RequestMapping(value = "smEnquiryService")
public class SmEnquiryController {

	@Autowired
	SmEnquiryService smEnqSer;
	
	@RequestMapping(value = "/newEnquiry", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Object> saveSmService(@RequestBody SmEnquiry newEnquiry) {
		
		String reason = null;
		SmEnquiry created = null;
		
		 if (newEnquiry != null) {
	        	ImmutablePair<SmEnquiry, String> result = saveEnquiry(newEnquiry);
	            reason = result.getRight();            
	            created = result.getLeft();
	        } else {
	            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Service to be created is null").body(newEnquiry);
	        }
	 
	        if (reason == null) {
	            return ResponseEntity.status(HttpStatus.CREATED).body(created);
	        } else {
	            return ResponseEntity
	                    .status(HttpStatus.BAD_REQUEST)
	                    .body(reason);
	        }
	 }
	

	///////////////////////////////////////////////////////////////////////////////////
	
	@RequestMapping(value = "/getAllBySample", method = RequestMethod.POST,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getAllByExample(@RequestBody SmEnquiry se) {
		try {
			List<SmEnquiry> seL = smEnqSer.getAllByExample(se);
			return ResponseEntity.status(HttpStatus.OK).body(seL);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	////////////////////////////////////////////////////// private classes ....

	@Transactional(rollbackFor = Exception.class)
	private ImmutablePair<SmEnquiry, String> saveEnquiry(SmEnquiry newEnquiry) {

		String reason = null;
		SmEnquiry enqSer = null;

		try {

			enqSer = smEnqSer.saveNewEnquiry(newEnquiry);
			
		} catch (Exception ex) {
			ex.printStackTrace();
			reason = "Error: ";
        	reason += ex.getMessage();
		}
		return new ImmutablePair<SmEnquiry, String>(enqSer, reason);
	}
}
