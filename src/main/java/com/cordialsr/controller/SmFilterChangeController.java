package com.cordialsr.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.SmFilterChange;
import com.cordialsr.domain.factory.SmFilterChangeFactory;
import com.cordialsr.service.SmFilterChangeService;

@RepositoryRestController
@RequestMapping(value = "smFilterChanges")
public class SmFilterChangeController {
	
	@Autowired
	SmFilterChangeService filterService;
	
	@RequestMapping(value = "/changeSmFilter",
            method = {RequestMethod.PATCH, RequestMethod.PUT},
            produces = MediaTypes.HAL_JSON_VALUE)	
	public @ResponseBody
    ResponseEntity<Object> updateSmFilter(@RequestBody SmFilterChange filter) {
        
		 	SmFilterChange toBeCreated = filter;
		 	SmFilterChange created = null;
	        String reason = null;
	        
	        if (toBeCreated != null) {
	        	ImmutablePair<SmFilterChange, String> result = updateFilterChange(filter);
	            reason = result.getRight();            
	            created = result.getLeft();
	        } else {
	            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Filter Change to be created is null").body(toBeCreated);
	        }
	 
	        if (reason == null) {
	            return ResponseEntity.status(HttpStatus.CREATED).body(created);
	        } else {
	            return ResponseEntity
	                    .status(HttpStatus.BAD_REQUEST)
	                    .body(reason);
	        }
		
	}
	
	/////////////////////////////////////////////////////////Take data for period of filter change 
	
	@RequestMapping(value = "/loadSmFilterChanges",
            method = {RequestMethod.PATCH, RequestMethod.GET},
            produces = MediaTypes.HAL_JSON_VALUE)	
	public @ResponseBody
    ResponseEntity<Object> takeDataToLoad(
    		@RequestParam("cid") Integer cid,
    		@RequestParam("bid") Integer bid,
    		@RequestParam("bool") Boolean bool,
    		@DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam("from") Date from,
    		@DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam("to") Date to,
			@RequestParam("care") Long eid) {
		
		List<SmFilterChange> filterDto = null;
	 	List<SmFilterChange> fcList = null;
        String reason = null;
        if (cid != null && bid != null && from !=null && to != null) {
        	ImmutablePair<List<SmFilterChange>, String> result = loadFilterChanges(cid, bid, bool, from, to, eid);
            reason = result.getRight();            
            fcList = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "GET data is null").body(cid);
        }
        if (reason != null) {
        	 return ResponseEntity
	                    .status(HttpStatus.BAD_REQUEST)
	                    .body(reason);
        }
        
        ImmutablePair<List<SmFilterChange>, String> result = createFilteChangeDto(fcList);
        
        filterDto = result.getLeft();
        reason = result.getRight();
        
        if (reason != null) {
        	return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
		
        return ResponseEntity.status(HttpStatus.CREATED).body(filterDto);
	}
	
	/////////////////////////////////////////////////////// Take data by Serila Number	
	
	@RequestMapping(value = "/loadSmFiChBySerNum",
            method = {RequestMethod.PATCH, RequestMethod.GET},
            produces = MediaTypes.HAL_JSON_VALUE)	
	public @ResponseBody
    ResponseEntity<Object> takeDataToLoad(
    		@RequestParam("ser_num") String ser_num) {
		
		
	 	SmFilterChange newFC = null;
	 	SmFilterChange filterDto = null;
        
        try {
        	if (ser_num != null && ser_num.length() > 0) {          
            	newFC = filterService.findBySerialNumber(ser_num);
        	} else {
            	return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "GET data is null").body(ser_num);
        	}
        	List<SmFilterChange> res = new ArrayList<SmFilterChange>();
        	res.add(newFC);
        	res = SmFilterChangeFactory.createFilterChangeDto(res);
            filterDto = res.get(0);
        	
        } catch (Exception ex) {
        	 return ResponseEntity
	                    .status(HttpStatus.BAD_REQUEST)
	                    .body("Error: " + ex);
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(filterDto);
	}
	
	////////////////////////////////////////// Creating Dto ..............................
	
	private ImmutablePair<List<SmFilterChange>, String> createFilteChangeDto(List<SmFilterChange> created) {
		String reason = null;
		List<SmFilterChange> filter = null;
		try {
			filter = SmFilterChangeFactory.createFilterChangeDto(created);
		} catch (Exception ex) {
			ex.printStackTrace();
			reason = "Error " + ex.getMessage(); 
		}
		return new ImmutablePair<List<SmFilterChange>, String>(filter, reason);
	}

	///////////////////////////////////////// Connect with SmFilterChange Service 
	
	private ImmutablePair<List<SmFilterChange>, String> loadFilterChanges(Integer cid, Integer bid, Boolean bool, Date from, Date to, Long eid) {
		String reason = null;
		List<SmFilterChange> filter = null;
		try {
			filter = filterService.loadFilterChanges(cid, bid, bool, from, to, eid);
		} catch(Exception ex) {
			ex.printStackTrace();
			reason = "Error " + ex.getMessage();
		}
		
		return new ImmutablePair<List<SmFilterChange>, String>(filter, reason);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private ImmutablePair<SmFilterChange, String> updateFilterChange(SmFilterChange filter) {
		String reason = null;
		try {
			filter = filterService.updateFilterChange(filter);
		} catch(Exception ex) {
			ex.printStackTrace();
        	reason = "Error ";
        	reason += ex.getMessage();
		}
		
		return new ImmutablePair<SmFilterChange, String>(filter, reason);
	}

}
