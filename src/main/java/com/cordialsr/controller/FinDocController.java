package com.cordialsr.controller;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractHistory;
import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.reducer.ContractReducer;
import com.cordialsr.domain.reducer.FinDocReducer;
import com.cordialsr.dto.ContractPaymentDto;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.ContractPostRefactorService;
import com.cordialsr.service.FinDocService;

@RepositoryRestController
@RequestMapping(value = "findocs")
public class FinDocController {

	@Autowired
	FinDocService fdService;
	
	@Autowired 
	ContractPostRefactorService cprService;
	
	@RequestMapping(value = "/findfd/{fid}", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getFdById(
    		@PathVariable("fid") Long fid) {
		try {
			FinDoc fd = fdService.getOneFinDoc(fid);
			fd = FinDocReducer.reduceFinDocForView(fd);
			return ResponseEntity.status(HttpStatus.OK).body(fd);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// *******************************************************************************************************
	
	@RequestMapping(value = "/newfindoc", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Object> saveFinDoc(@RequestBody FinDoc fdBody) {
        FinDoc fdToBeCreated = fdBody;
        FinDoc createdFd;
        String reason = null;
        
        if (fdToBeCreated != null) {
        	ImmutablePair<FinDoc, String> result = saveTheFd(fdToBeCreated);
            reason = result.getRight();            
            createdFd = result.getLeft();
            createdFd = FinDocReducer.reduceFinDocForView(createdFd);
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "FinDoc to be created is null").body(fdToBeCreated);
        }
 
        if (createdFd != null && createdFd.getId() != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdFd);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<FinDoc, String> saveTheFd(FinDoc changedFd) {
        String reason = null;

        try {
        	fdService.saveFinDoc(changedFd);
        	
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();        	
        }
        
        return new ImmutablePair<FinDoc, String>(changedFd, reason);
    }
	
	// ******************************************************************************************
	
	@RequestMapping(value = "/postConPayments", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> postConPayments(@RequestBody ContractPaymentDto[] cpDto) {
		String reason = null;
        
        if (cpDto != null && cpDto.length > 0) {
        	ImmutablePair<ContractPaymentDto[], String> result = postContractPayments(cpDto);
            reason = result.getRight();            
            cpDto = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Contract to be created is null").body(cpDto);
        }
        
        if (GeneralUtil.isEmptyString(reason) && cpDto != null) {
        	for (ContractPaymentDto cp: cpDto) {
        		cp.setContract(ContractReducer.reduceMax(cp.getContract()));
        	}
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(cpDto);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<ContractPaymentDto[], String> postContractPayments(ContractPaymentDto[] cpDto) {
        String reason = null;
        try {
        	cpDto = fdService.doPostContractPayments(cpDto);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();
        }
        return new ImmutablePair<ContractPaymentDto[], String>(cpDto, reason);
    }
	
	
	// ******************************************************************************************
	
		@RequestMapping(value = "/postConPayment", method = RequestMethod.POST, 
	            produces = MediaTypes.HAL_JSON_VALUE)
		@ResponseBody 
		public ResponseEntity<Object> postConPayment(@RequestBody ContractPaymentDto cpDto) {
			String reason = null;
	        
	        if (cpDto != null && cpDto.getContract() != null) {
	        	ImmutablePair<ContractPaymentDto, String> result = postContractPayment(cpDto);
	            reason = result.getRight();            
	            cpDto = result.getLeft();
	        } else {
	            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Contract to be created is null").body(cpDto);
	        }
	        
	        if (GeneralUtil.isEmptyString(reason) && cpDto != null) {
	        		cpDto.setContract(ContractReducer.reduceMax(cpDto.getContract()));
	            return ResponseEntity.status(HttpStatus.ACCEPTED).body(cpDto);
	        } else {
	            return ResponseEntity
	                    .status(HttpStatus.BAD_REQUEST)
	                    .body(reason);
	        }
	    }
		
		@Transactional(rollbackFor = Exception.class)
	    private ImmutablePair<ContractPaymentDto, String> postContractPayment(ContractPaymentDto cpDto) {
	        String reason = null;
	        try {
	        	cpDto = fdService.doPostContractPayment(cpDto);
	        } catch(Exception ex) {
	        	ex.printStackTrace();
	        	reason = ex.getMessage();
	        }
	        return new ImmutablePair<ContractPaymentDto, String>(cpDto, reason);
	    }
	
	
	// ******************************************************************************************
	
		@RequestMapping(value = "/stornoContractPayment", method = RequestMethod.POST, 
	            produces = MediaTypes.HAL_JSON_VALUE)
		@ResponseBody 
		public ResponseEntity<Object> stornoPayment(@RequestBody ContractHistory conHistory) {
			String reason = null;
			
	        if (conHistory != null) {
	        	ImmutablePair<Boolean, String> result = stornoContractPayment(conHistory);
	            reason = result.getRight();
	            Boolean res = result.getLeft();
	        } else {
	            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Contract to be created is null").body(conHistory);
	        }
	        
	        if (GeneralUtil.isEmptyString(reason) && conHistory != null) {
	            return ResponseEntity.status(HttpStatus.ACCEPTED).body(conHistory);
	        } else {
	            return ResponseEntity
	                    .status(HttpStatus.NOT_IMPLEMENTED)
	                    .body(reason);
	        }
	    }
		
		@Transactional(rollbackFor = Exception.class)
	    private ImmutablePair<Boolean, String> stornoContractPayment(ContractHistory conHistory) {
	        String reason = null;
	        Boolean res = false;
	        try {
	        	res = cprService.stornoContractPaymentFromFront(conHistory);
	        } catch(Exception ex) {
	        	ex.printStackTrace();
	        	reason = ex.getMessage();
	        }
	        return new ImmutablePair<Boolean, String>(res, reason);
	    }
	
	
		
		
	// ******************************************************************************************
	
		@RequestMapping(value = "/stornoCustomerPayment", method = RequestMethod.POST, 
	            produces = MediaTypes.HAL_JSON_VALUE)
		@ResponseBody 
		public ResponseEntity<Object> customerStornoPayment(@RequestBody ContractHistory conHistory) {
			String reason = null;
			
	        if (conHistory != null) {
	        	ImmutablePair<Boolean, String> result = stornoCustomerPayment(conHistory);
	            reason = result.getRight();
	            Boolean res = result.getLeft();
	        } else {
	            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Contract to be created is null").body(conHistory);
	        }
	        
	        if (GeneralUtil.isEmptyString(reason) && conHistory != null) {
	            return ResponseEntity.status(HttpStatus.ACCEPTED).body(conHistory);
	        } else {
	            return ResponseEntity
	                    .status(HttpStatus.NOT_IMPLEMENTED)
	                    .body(reason);
	        }
	    }
		
		@Transactional(rollbackFor = Exception.class)
	    private ImmutablePair<Boolean, String> stornoCustomerPayment(ContractHistory conHistory) {
	        String reason = null;
	        Boolean res = false;
	        try {
	        	res = cprService.stornoCustomerPayment(conHistory);
	        } catch(Exception ex) {
	        	ex.printStackTrace();
	        	reason = ex.getMessage();
	        }
	        return new ImmutablePair<Boolean, String>(res, reason);
	    }
	
}
