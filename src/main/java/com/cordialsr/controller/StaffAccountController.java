package com.cordialsr.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cordialsr.domain.FinEntry;
import com.cordialsr.domain.reducer.FinEntryReducer;
import com.cordialsr.service.StaffAccountService;

@RestController
@RequestMapping(value = "stfacc")
public class StaffAccountController {
	
	@Autowired
	StaffAccountService stfaccService;
	
	@RequestMapping(value = "/getStaffBalance", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getStaffBalance(
    		@RequestParam("cid") Integer cid,
    		@RequestParam("bid") Integer bid,
    		@DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam("dt") Date dt
    		) {
		try {
			Object sbl = stfaccService.getStaffBalanceByBranch(cid, bid, dt);			
			if (sbl == null) {
				return ResponseEntity.status(HttpStatus.OK).body("Payroll not found!");
			}
			return ResponseEntity.status(HttpStatus.OK).body(sbl);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// ******************************************************************************************
	
	@RequestMapping(value = "/getStaffJournal", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getStaffJournall(
    		@RequestParam("cid") Integer cid,
    		@RequestParam("bid") Integer bid,
    		@RequestParam("sid") Long sid,
    		@RequestParam("cur") String currency,
    		@RequestParam(value = "format", required = false) String format,
    		@DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam("ds") Date ds,
    		@DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam("de") Date de
    		) {
		try {
			if (format != null) {
				String pdf = stfaccService.getSbJournalFile(cid, bid, sid, currency, ds, de, format);
				if (pdf == null) {
					return ResponseEntity.status(HttpStatus.OK).body("Report not found!");
				}
				return ResponseEntity.status(HttpStatus.OK).body("{ \"pdf\": \"" + pdf + "\"}");
			} else {
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				List<FinEntry> feL = stfaccService.getSbJournal(cid, bid, sid, currency, df.format(ds), df.format(de));
				for (FinEntry fe: feL) {
					FinEntryReducer.reduceFeForView(fe);
				}
				return ResponseEntity.status(HttpStatus.OK).body(feL);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
}
