package com.cordialsr.controller;

import java.sql.Time;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.Dao.CrmDemoDao;
import com.cordialsr.Dao.CrmLeadDao;
import com.cordialsr.domain.CrmDemo;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.security.User;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.general.exception.DaoException;
import com.cordialsr.service.CrmDemoService;
import com.cordialsr.service.CrmLeadService;

@RepositoryRestController
@RequestMapping(value = "demos")
public class CrmDemoController {
	
	@Autowired
	CrmDemoDao demoDao;
	
	@Autowired
	CrmDemoService demoService;
	
	@RequestMapping(value = "/newDemo", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> saveDemo(@RequestBody CrmDemo newDemo) {
		CrmDemo createdDemo;
        String reason = null;
        
        if (newDemo != null) {
        	ImmutablePair<CrmDemo, String> result = saveTheDemo(newDemo);
            reason = DaoException.justifyMessage(result.getRight());            
            createdDemo = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Demo to be created is null").body(newDemo);
        }
 
        if (GeneralUtil.isEmptyString(reason) && createdDemo != null && createdDemo.getId() != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdDemo);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<CrmDemo, String> saveTheDemo(CrmDemo newDemo) {
        String reason = null;
        try {
        	demoService.saveDemo(newDemo);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();        	
        }        
        return new ImmutablePair<CrmDemo, String>(newDemo, reason);
    }
	
	// ***********************************************************************************************************
	
	
		@RequestMapping(value = "{demoId}",
	            method = {RequestMethod.PATCH, RequestMethod.PUT},
	            produces = MediaTypes.HAL_JSON_VALUE)	
		public @ResponseBody
	    ResponseEntity<Object> updateDemo(@PathVariable("demoId") Long demoId, @RequestBody CrmDemo demo) {
	        String reason = null;        
	        if (demoId != null) {
	            if (demo != null) { 
	                ImmutablePair<CrmDemo, String> result = updateTheDemo(demo);
	                reason = result.getRight();
	                demo = result.getLeft();
	            } else {
	                reason = "not enough data to update the Demo [" + demoId + "]";
	            }
	        }
	        if (StringUtils.isNotBlank(reason)) {
	        	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
	        }
	        return ResponseEntity.status(HttpStatus.OK).body(demo);
	    }
		
		@Transactional(rollbackFor = Exception.class)
	    private ImmutablePair<CrmDemo, String> updateTheDemo(CrmDemo demo) {
	        String reason = null;
	        try {
	        	demo = demoService.updateDemo(demo);
	        } catch(Exception ex) {
	        	ex.printStackTrace();
	        	reason = ex.getMessage();
	        }        
	        return new ImmutablePair<CrmDemo, String>(demo, reason);
	    }
		
		//**************************************************************************
	
}
