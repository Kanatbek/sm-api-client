package com.cordialsr.controller;

import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.StockIn;
import com.cordialsr.domain.reducer.StockInReducer;
import com.cordialsr.service.FinDocService;
import com.cordialsr.service.StockService;
import org.springframework.transaction.annotation.Transactional;

@RepositoryRestController
@RequestMapping(value = "stocks")
public class StockController {
	
	@Autowired
	StockService stockInService;
	
	@Autowired
	FinDocService finDocService;
	
	// Поиск со склада
	@RequestMapping(value = "/stiByGlInv/{cid}/{bid}/{gl}/{inv}/{cnt}/{sta}/{prt}", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> stiByGlInv(
    		@PathVariable("cid") Integer cid,
    		@PathVariable("bid") Integer bid,
    		@PathVariable("gl") String gl,
    		@PathVariable("inv") Integer inv,
    		@PathVariable("cnt") Integer cnt,
    		@PathVariable("sta") Integer sta,
    		@PathVariable("prt") Integer prt) {
		try {
			List<StockIn> mt = stockInService.getStockInByGlAndInv(cid, bid, gl, inv, cnt, sta, prt);
			for (StockIn si: mt) {
				si = StockInReducer.reduceStockIn(si);
			}
			return ResponseEntity.status(HttpStatus.OK).body(mt);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// *******************************************************************************************************
	
	// При оприходовании / Импорт / Трансфер 
	@RequestMapping(value = "/newstock", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    ResponseEntity<Object> newStockIn(@RequestBody FinDoc[] finDocs) throws Exception {
		String reason = null;
		Boolean bool = false;
		if (finDocs != null) {
			ImmutablePair<Boolean, String> result = saveNewStockIn(finDocs);
			bool = result.getLeft();
			reason = result.getRight();
		}
		else {
			reason = "finDoc is null";
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
		}
		if (reason != null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
		}
		if (bool)
			return ResponseEntity.status(HttpStatus.CREATED).body(bool);
		else return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Unsuccesfull!");
	}

	// Выдача / Клиенту по контракту, Сервис	
	@RequestMapping(value = "/stockout", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Object> outStocks(@RequestBody Invoice invBody) {
		String reason = null;
		Boolean success = false; 
		if (invBody != null) {
			ImmutablePair<Boolean, String> result = saveNewStockOutOperation(invBody);
			reason = result.getRight();
			success = result.getLeft();
		}
		else {
			reason = "Invoice is null";
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
		}
		if (reason != null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(success);
	}
	
	///$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	
	// Возврат с рук сотрудников из подотчета
	@RequestMapping(value = "/stockstatus", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    ResponseEntity<Object> changeStockStatus(@RequestBody StockIn stock) throws Exception {
	
		String reason = null;
		
		if (stock != null) {

			ImmutablePair<StockIn, String> result = changeStockOperation(stock);
			
			reason = result.getRight();
			stock = result.getLeft();
			
		} else {
			reason = "Stock is null";
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);	
		}
		if (reason == null) {
			return ResponseEntity.status(HttpStatus.CREATED).body(stock);
		}
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
	}


	// *******************************************************************************************************
	
	// Документы склада	
	@RequestMapping(value = "/listfindoc", method = RequestMethod.GET, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    ResponseEntity<Object> listOfFinDoc(@RequestParam("companyId") int comId,@RequestParam("branchId") int braId) throws Exception {
		
		System.out.println(comId);
		System.out.println(braId);

		return ResponseEntity.status(HttpStatus.CREATED).body("Work");
	}
	
	@Transactional(rollbackFor = Exception.class)	
	private ImmutablePair<Boolean, String> saveNewStockIn(FinDoc[] finDocs) {
		String result = null;
		Boolean bool = false;
		try {
			bool = stockInService.saveNewStockIns(finDocs);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	result = ex.getMessage();        	
        }
		return new ImmutablePair<Boolean, String>(bool, result);
	}
	
	@Transactional(rollbackFor = Exception.class)	
	private ImmutablePair<Boolean, String> saveNewStockOutOperation(Invoice invoice) {
		String result = null;
		Boolean bool = false;
		try {
			bool = stockInService.saveNewStocksOutOperation(invoice);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	result = ex.getMessage();        	
        }
		return new ImmutablePair<Boolean, String>(bool, result);
	}
	
	@Transactional(rollbackFor = Exception.class)
	private ImmutablePair<StockIn, String> changeStockOperation(StockIn stock) {
		String result = null;
		StockIn bool = null;
		try {
			bool = stockInService.changeStockInByStatus(stock);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	result = ex.getMessage();        	
        }
		return new ImmutablePair<StockIn, String>(bool, result);
	}
	
}