package com.cordialsr.controller;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.Region;
import com.cordialsr.general.exception.DaoException;
import com.cordialsr.service.RegionService;

@RepositoryRestController
@RequestMapping(value = "regions")
public class RegionController {

	@Autowired
	RegionService regionService;
	
	@RequestMapping(value = "/newregion", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Object> saveNewRegion(@RequestBody Region newRegion) {
        Region createdReg;
        String reason = null;
        
        if (newRegion != null) {
        	ImmutablePair<Region, String> result = saveTheFd(newRegion);
            reason = DaoException.justifyMessage(result.getRight());            
            createdReg = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Region to be created is null").body(newRegion);
        }
 
        if (createdReg != null && createdReg.getId() != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdReg);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional
    private ImmutablePair<Region, String> saveTheFd(Region newRegion) {
        String reason = null;
        try {
        	regionService.saveNewRegion(newRegion);
        } catch(Exception ex) {
        	reason = ex.getMessage();        	
        }
        return new ImmutablePair<Region, String>(newRegion, reason);
    }
	
	// ************************************************************************************************************************
	
	@RequestMapping(value = "{id}",
            method = {RequestMethod.PATCH, RequestMethod.PUT},
            produces = MediaTypes.HAL_JSON_VALUE)	
	public @ResponseBody
    ResponseEntity<Object> updateRegion(@PathVariable("id") Integer regionId, @RequestBody Region region) {
        String reason = null;        
        if (regionId != null) {
            if (region != null) { 
                ImmutablePair<Region, String> result = updateTheRegion(region);
                reason = result.getRight();
                region = result.getLeft();
            } else {
                reason = "not enough data to update the Region [" + regionId + "]";
            }
        }
        if (StringUtils.isNotBlank(reason)) {
        	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
        }
        return ResponseEntity.status(HttpStatus.OK).body(region);
    }
	
	@Transactional
    private ImmutablePair<Region, String> updateTheRegion(Region region) {
        String reason = null;
        try {
        	region = regionService.saveRegionChanges(region);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();
        }        
        return new ImmutablePair<Region, String>(region, reason);
    }
}
