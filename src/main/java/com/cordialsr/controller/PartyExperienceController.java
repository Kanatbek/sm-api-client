package com.cordialsr.controller;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.PartyExperience;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.general.exception.DaoException;
import com.cordialsr.service.PartyExperienceService;
import com.cordialsr.service.PartyService;

@RepositoryRestController
@RequestMapping(value = "experiences")
public class PartyExperienceController {

	@Autowired
	PartyExperienceService expService;
	
	@Autowired
	PartyService partyDao;
	
	@RequestMapping(value = "/new", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> saveExperience(@RequestBody PartyExperience newExperience) {
		PartyExperience createdPartyExperience;
        String reason = null;
        
        if (newExperience != null) {
        	ImmutablePair<PartyExperience, String> result = saveTheExperience(newExperience);
            reason = DaoException.justifyMessage(result.getRight());
            createdPartyExperience = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Experience to be created is null").body(newExperience);
        }
 
        if (GeneralUtil.isEmptyString(reason) && createdPartyExperience != null && createdPartyExperience.getId() != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdPartyExperience);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }	
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<PartyExperience, String> saveTheExperience(PartyExperience newExperience) {
        String reason = null;
        try {
        	expService.saveNewExperience(newExperience);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();        	
        }        
        return new ImmutablePair<PartyExperience, String>(newExperience, reason);
    }
	
	// *******************************************************************************************************
	
}
