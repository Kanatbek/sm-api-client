package com.cordialsr.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.SmVisitCollector;
import com.cordialsr.dto.SmVisitCollectorDto;
import com.cordialsr.service.SmVisitCollectorService;

@RepositoryRestController
@RequestMapping(value = "smVisitCollector")
public class SmVisitCollectorController {
	
	@Autowired
	SmVisitCollectorService smVisitService;

	@RequestMapping(value = "/newVisit", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Object> newVisit(
    		@RequestParam("inDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date inDate,
    		@RequestParam("inTime") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date inTime,
    		@RequestParam("outTime") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date outTime,
    		@RequestParam("emplId") Long emplId,
    		@RequestParam("partyId") Long partyId,
    		@RequestParam("summ") BigDecimal summ,
    		@RequestParam("longitude") BigDecimal longitude,
    		@RequestParam("latitude") BigDecimal latitude,
    		@RequestParam("contractId") Long contractId,
    		@RequestParam("info") String info) {
        String reason = null;
        SmVisitCollector created = null;
        ImmutablePair<SmVisitCollector, String> result = newVisitCollector(inDate, inTime, outTime, emplId, partyId,
        		summ, longitude, latitude, contractId, info);
            reason = result.getRight();            
            created = result.getLeft();
        if (reason == null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(created);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional(rollbackFor = Exception.class)
	private ImmutablePair<SmVisitCollector, String> newVisitCollector(Date inDate, Date inTime, Date outTime, 
													Long emplId,Long partyId, BigDecimal summ, BigDecimal longitude, 
													BigDecimal latitude, Long contractId, String info) {
		SmVisitCollector service = null;
		String reason = null;
		try { 
			SmVisitCollector dService = smVisitService.newCollectorVisit(inDate, inTime, outTime, emplId, partyId,
																		summ, contractId, longitude, latitude, info);
			if (dService != null) {
				service = dService;
			}		
		} catch (Exception ex) {
			ex.printStackTrace();
			reason = "Error ";
        	reason += ex.getMessage();
		}
		return new ImmutablePair<SmVisitCollector, String>(service, reason);
	}
	
	
	//**************************************************************************************************************************************
	
	
	@RequestMapping(value="getVisitList", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
@ResponseBody
public ResponseEntity<Object> getVisitList(
		@RequestParam("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
		@RequestParam("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate,
		@RequestParam("emplId") Long emplId,
	    Pageable pageable
	    ) {
	try {
		List<SmVisitCollectorDto> visits = smVisitService.getVisits(startDate, endDate, emplId);
		
		
		return ResponseEntity.status(HttpStatus.OK).body(visits);
	} catch (Exception e) {
		e.printStackTrace();
		return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(e.getMessage());
	}
}
	
	// ******************************************************************************************************************************************************
	
	@RequestMapping(value="getVisitByCustomer", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
@ResponseBody
public ResponseEntity<Object> getVisitByCustomer(
		@RequestParam("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
		@RequestParam("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate,
		@RequestParam("emplId") Long emplId,
		@RequestParam("customerId") Long customerId
	    ) {
	try {
		List<SmVisitCollectorDto> visits = smVisitService.getCustomersVisit(startDate, endDate, emplId, customerId);
		
		
		return ResponseEntity.status(HttpStatus.OK).body(visits);
	} catch (Exception e) {
		e.printStackTrace();
		return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(e.getMessage());
	}
}
	
	// ********************************************************************************************************************************************************

	
}
