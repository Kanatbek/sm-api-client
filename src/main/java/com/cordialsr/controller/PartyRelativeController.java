package com.cordialsr.controller;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.PartyExperience;
import com.cordialsr.domain.PartyRelatives;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.general.exception.DaoException;
import com.cordialsr.service.PartyRelativesService;

@RepositoryRestController
@RequestMapping(value = "relatives")
public class PartyRelativeController {
	
	@Autowired
	PartyRelativesService relativeService;
	
	
	@RequestMapping(value = "/new", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> saveRelative(@RequestBody PartyRelatives newRelative) {
		PartyRelatives createdPartyExperience;
        String reason = null;
        
        if (newRelative != null) {
        	ImmutablePair<PartyRelatives, String> result = saveTheRelative(newRelative);
            reason = DaoException.justifyMessage(result.getRight());
            createdPartyExperience = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Experience to be created is null").body(newRelative);
        }
 
        if (GeneralUtil.isEmptyString(reason) && createdPartyExperience != null && createdPartyExperience.getId() != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdPartyExperience);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }	
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<PartyRelatives, String> saveTheRelative(PartyRelatives newRelative) {
        String reason = null;
        try {
        	relativeService.saveNewRelative(newRelative);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();        	
        }        
        return new ImmutablePair<PartyRelatives, String>(newRelative, reason);
    }
	
	// *******************************************************************************************************

}
