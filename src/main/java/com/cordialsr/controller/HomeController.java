package com.cordialsr.controller;

import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cordialsr.domain.security.Role;
import com.cordialsr.domain.security.User;
import com.cordialsr.security.repository.RoleRepository;
import com.cordialsr.security.service.UserService;

@Controller
public class HomeController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleRepository roleDao;
	
	@RequestMapping("/")
	public String home() {
		return "redirect:/index";
	}
	
	@RequestMapping("/index")
	public String index() {
		return "index";
	}
	
	@RequestMapping("/lgt2")
	public String lgt2() {
		
		return "index";
	}
	
	@RequestMapping("/login")
	public String login() {
		return "redirect:/index";
	}
	
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String signup(Model model) {
        User user = new User();

        model.addAttribute("user", user);

        return "signup";
    }
	
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String signupPost(@ModelAttribute("user") User user,  Model model) {

		if(userService.checkUserExists(user.getUsername(), user.getEmail()))  {

            if (userService.checkEmailExists(user.getEmail())) {
                model.addAttribute("emailExists", true);
            }

            if (userService.checkUsernameExists(user.getUsername())) {
                model.addAttribute("usernameExists", true);
            }

            return "signup";
        } else {
        	 Set<Role> userRoles = new HashSet<>();
             userRoles.add(roleDao.findByName("ROLE_USER"));
             try {
            	 userService.createUser(user, userRoles);
             } catch (Exception e) {
            	 e.printStackTrace();
             }

            return "redirect:/";
        }
    }
	
	@RequestMapping("/userFront") 
	public String userFront(Principal principal, Model model) {
		User user  = userService.findByUsername(principal.getName());
		
//		model.addAttribute("primaryAccount", primaryAccount);
//		model.addAttribute("savingsAccount", savingsAccount);
		
		return "userFront";
	}
	
}
