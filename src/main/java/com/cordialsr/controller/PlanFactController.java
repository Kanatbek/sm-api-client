package com.cordialsr.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.Dao.PlanFactDao;
import com.cordialsr.domain.PlanFact;
import com.cordialsr.dto.mini.PlanFactMiniDto;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.general.exception.DaoException;
import com.cordialsr.service.PlanFactService;

@RepositoryRestController
@RequestMapping(value = "pfcon")
public class PlanFactController {

	@Autowired
	PlanFactService pfService;
	
	@Autowired
	PlanFactDao pfDao;
	
	private ModelMapper modelMapper = new ModelMapper();
	
	@RequestMapping(value = "/getPlanFact", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getFdById(
    		@RequestParam("cid") Integer cid,
    		@RequestParam("bid") Integer bid,
    		@RequestParam("pos") Integer pos,
    		@RequestParam("dt") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt
    		) {
		try {
			List<PlanFact> conps = pfService.getPlanFact(cid, bid, pos, dt);
			List<PlanFactMiniDto> res = new ArrayList<>();
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			for (PlanFact pf: conps) {
				PlanFactMiniDto pp = new PlanFactMiniDto();
				modelMapper.map(pf, pp);
				res.add(pp);
			}
			if (conps == null) {
				return ResponseEntity.status(HttpStatus.OK).body("PlanFact not found!");
			}
			return ResponseEntity.status(HttpStatus.OK).body(res);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// ***********************************************************************************************************
	
	@RequestMapping(value = "/getCollectorPlan", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getCollectorPlan(
    		@RequestParam("cid") Integer cid,
    		@RequestParam("bid") Integer bid,
    		@RequestParam("pos") Integer pos,
    		@RequestParam("partyId") Long partyId,
    		@RequestParam("dt") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt
    		) {
		try {
			List<PlanFact> conps = pfService.getPlanFact(cid, bid, pos, dt);
			List<PlanFactMiniDto> res = new ArrayList<>();
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			for (PlanFact pf: conps) {
				
				PlanFactMiniDto pp = new PlanFactMiniDto();
				modelMapper.map(pf, pp);
					if (pp.getParty() != null && pp.getParty().getId() != null && pp.getParty().getId().equals(partyId)) {
						res.add(pp);
					}
			}
			if (conps == null) {
				return ResponseEntity.status(HttpStatus.OK).body("PlanFact not found!");
			}
			return ResponseEntity.status(HttpStatus.OK).body(res);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// ***********************************************************************************************************
	
	@RequestMapping(value = "/getPlan", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getPlan(
    		@RequestParam("cid") Integer cid,
    		@RequestParam("bid") Integer bid,
    		@RequestParam("pos") Integer pos,
    		@RequestParam("dt") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt
    		) {
		try {
			List<PlanFact> conps = pfService.getPlan(cid, bid, pos, dt);
			List<PlanFactMiniDto> res = new ArrayList<>();
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			for (PlanFact pf: conps) {
				PlanFactMiniDto pp = new PlanFactMiniDto();
				modelMapper.map(pf, pp);
				res.add(pp);
			}
			if (conps == null) {
				return ResponseEntity.status(HttpStatus.OK).body("PlanFact not found!");
			}
			return ResponseEntity.status(HttpStatus.OK).body(res);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// ***********************************************************************************************************
	
	@RequestMapping(value = "/getCaremanPlan", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getCaremanPlan(
    		@RequestParam("cid") Integer cid,
    		@RequestParam("bid") Integer bid,
    		@RequestParam("caremanId") Long caremanId,
    		@RequestParam("dt") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt
    		) {
		try {
			List<PlanFact> conps = pfService.getCaremanPlanFact(cid, bid, caremanId, dt);
			List<PlanFactMiniDto> res = new ArrayList<>();
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			for (PlanFact pf: conps) {
				PlanFactMiniDto pp = new PlanFactMiniDto();
				modelMapper.map(pf, pp);
				res.add(pp);
			}
			if (conps == null) {
				return ResponseEntity.status(HttpStatus.OK).body("ContractPlan not found!");
			}
			return ResponseEntity.status(HttpStatus.OK).body(res);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// ***********************************************************************************************************
	
	@RequestMapping(value = "/newPlanFact", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> savePlanFact(@RequestBody List<PlanFact> plCreated) {
		List<PlanFact> createdpf;

        String reason = null;
        
        if (plCreated != null && plCreated.size() > 0) {
        	ImmutablePair<List<PlanFact>, String> result = savePF(plCreated);
        	
            reason = DaoException.justifyMessage(result.getRight());            
            createdpf = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "PlanFact to be created is null").body(plCreated);
        }
 
        if (GeneralUtil.isEmptyString(reason) && createdpf != null && createdpf.size() > 0) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdpf);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }

	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<List<PlanFact>, String> savePF(List<PlanFact> newPf) {
        String reason = null;
        List<PlanFact> createdpf = null;
        try {
        	createdpf = pfService.saveAllPlanFacts(newPf);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();        	
        }
        return new ImmutablePair<List<PlanFact>, String>(createdpf, reason);
    }
	
	
	// ***************************************************************************************************************************************************
	
//	@RequestMapping(value = "/getCollectorPlan", method = RequestMethod.GET,
//			produces = MediaTypes.HAL_JSON_VALUE)
//	@ResponseBody
//    public ResponseEntity<Object> getCollectorPlan(
//    		@RequestParam("collectorId") Long collectorId,
//    		@RequestParam("start_date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateStart,
//    		@RequestParam("end_date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateEnd
//    		) {
//		try {
//			PlanFact conps = pfService.getCollectorPlanFact(collectorId, dateStart, dateEnd);
////			List<PlanFactMiniDto> res = new ArrayList<>();
////			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
////				PlanFactMiniDto pp = new PlanFactMiniDto();
////				modelMapper.map(conps, pp);
////				res.add(pp);
//			if (conps == null) {
//				return ResponseEntity.status(HttpStatus.OK).body("ContractPlan not found!");
//			}
//			return ResponseEntity.status(HttpStatus.OK).body(conps);
//		} catch (Exception e) {
//			e.printStackTrace();
//			return ResponseEntity
//                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
//                    .body(e.getMessage());
//		}
//    }
}
