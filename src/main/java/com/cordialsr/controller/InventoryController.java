package com.cordialsr.controller;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.Award;
import com.cordialsr.domain.Inventory;
import com.cordialsr.dto.mini.PlanFactMiniDto;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.general.exception.DaoException;
import com.cordialsr.service.InventoryService;

@RepositoryRestController
@RequestMapping(value = "inventories")
public class InventoryController {

	@Autowired
	private InventoryService invService;

	/////////////////////////////////////////////// SAVE NEW INVENTORY ////////////////////////////////////////////////////////////////

	@RequestMapping(value = "/new", method = RequestMethod.POST, produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Object> newInventory(@RequestBody Inventory newInv) {
		Inventory createdInv;
		String reason = null;

		if (newInv != null) {
			ImmutablePair<Inventory, String> result = saveTheInventory(newInv);
			reason = result.getRight();
			createdInv = result.getLeft();
		} else {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Award to be created is null")
					.body(newInv);
		}

		if (GeneralUtil.isEmptyString(reason) && createdInv != null && createdInv.getId() != null) {
			return ResponseEntity.status(HttpStatus.CREATED).body(createdInv);
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
		}
	}

	@Transactional
	private ImmutablePair<Inventory, String> saveTheInventory(Inventory newInv) {
		String reason = null;
		Inventory savedInv = null;
		try {
			savedInv = invService.saveNewInventory(newInv);
		} catch (Exception ex) {
			ex.printStackTrace();
			reason = ex.getMessage();
		}
		return new ImmutablePair<Inventory, String>(savedInv, reason);
	}

	/////////////////////////////////////////////// UPDATE INVENTORY //////////////////////////////////////////////////////////////////

	@RequestMapping(value = "{id}", method = RequestMethod.PUT, produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Object> updateInventory(
			@PathVariable("id") Integer inventoryId, @RequestBody Inventory newInv) {
		String reason = null;
		Inventory updatedInv = null; 
		if (inventoryId != null) {
			if (newInv != null) {
				if (inventoryId == newInv.getId()) {
					ImmutablePair<Inventory, String> result = update(newInv);
					reason = result.getRight();
					updatedInv = result.getLeft();					
				} else {
					reason = "Incoming Id is different from the id of body object.";
				}
			} else {
				reason = "not enough data to update the Inventory [" + inventoryId + "]";
			}
		}
		
		if (GeneralUtil.isEmptyString(reason) && updatedInv != null && updatedInv.getId() != null) {
			return ResponseEntity.status(HttpStatus.OK).body(updatedInv);
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
		}
	}

	@Transactional
	private ImmutablePair<Inventory, String> update(Inventory newInventory) {
		String reason = null;
		Inventory changedInventory = null;
		try {
			changedInventory = invService.updateInventory(newInventory);
		} catch (Exception e) {
			e.printStackTrace();
			reason = e.getMessage();
		}
		return new ImmutablePair<Inventory, String>(changedInventory, reason);
	}	
	
	
/////////////////////////////////////////////// GET INVENTORY //////////////////////////////////////////////////////////////////

@RequestMapping(value = "/getInventory", method = RequestMethod.GET, produces = MediaTypes.HAL_JSON_VALUE)
@ResponseBody
	public ResponseEntity<Object> getInventoryByCompany(
			@RequestParam("cid") Integer cId) {	
	List<Inventory> res = new ArrayList<>();
	
	try {
		List<Inventory> inventoryList = invService.getInventoryByCompany(cId);
		if (inventoryList != null) {
			for (Inventory inv: inventoryList) {
				res.add(inv);
			}
		}
		return ResponseEntity.status(HttpStatus.OK).body(res);
	} catch (Exception e){
		e.printStackTrace();
		return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(e.getMessage());
	}
}
}	
