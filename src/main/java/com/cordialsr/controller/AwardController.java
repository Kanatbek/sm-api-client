	package com.cordialsr.controller;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.Award;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.general.exception.DaoException;
import com.cordialsr.service.AwardService;

@RepositoryRestController
@RequestMapping(value = "awards")
public class AwardController {
	
	@Autowired
	AwardService awardService;
	
	@RequestMapping(value = "/getPrByScPrior/{cid}", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getFdById(
    		@PathVariable("cid") Integer cid,
    		@RequestParam(value = "brid") Integer brid,
    		@RequestParam(value = "did") Integer did,
    		@RequestParam(value = "pos") Integer pos,
    		@RequestParam(value = "subcat", required = false) Integer subcat,
    		@RequestParam(value = "invid", required = false) Integer invid,
    		@RequestParam(value = "month", required = false) Integer month,
    		@RequestParam(value = "dt") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt,
    		@RequestParam(value = "isRent", required = false) Boolean isRent
    		) {
		try {
			Award aw = awardService.getPremi(cid, did, pos, brid, subcat, invid, dt, month, isRent);
			
			if (aw == null) {
				return ResponseEntity.status(HttpStatus.OK).body("Award not found!");
			}
			aw.getInvMainCategory().setCompany(null);
			aw.getInvSubCategory().setInvMainCategory(null);
			if (aw.getDepartment() != null) {
				aw.getDepartment().setCompany(null);	
			}
			return ResponseEntity.status(HttpStatus.OK).body(aw);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// *********************************************************************************************
	
	@RequestMapping(value = "/new", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> saveAward(@RequestBody Award newAward) {
		Award createdAward;
        String reason = null;
        
        if (newAward != null) {
        	ImmutablePair<Award, String> result = saveTheAward(newAward);
            reason = DaoException.justifyMessage(result.getRight());            
            createdAward = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Award to be created is null").body(newAward);
        }
 
        if (GeneralUtil.isEmptyString(reason) && createdAward != null && createdAward.getId() != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdAward);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<Award, String> saveTheAward(Award newAward) {
        String reason = null;
        try {
        	awardService.saveNewAward(newAward);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();        	
        }        
        return new ImmutablePair<Award, String>(newAward, reason);
    }
	
	// ***********************************************************************************************************
	
		@RequestMapping(value = "/getAllBySample", method = RequestMethod.POST,
				produces = MediaTypes.HAL_JSON_VALUE)
		@ResponseBody
	    public ResponseEntity<Object> getAllByExample(@RequestBody Award aw) {
			try {
				List<Award> awL = awardService.getAllByExample(aw);
				return ResponseEntity.status(HttpStatus.OK).body(awL);
			} catch (Exception e) {
				e.printStackTrace();
				return ResponseEntity
	                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
	                    .body(e.getMessage());
			}
	    }
		
		// ***********************************************************************************************************

		
		@RequestMapping(value = "{awardId}",
	            method = {RequestMethod.PATCH, RequestMethod.PUT},
	            produces = MediaTypes.HAL_JSON_VALUE)	
		public @ResponseBody
	    ResponseEntity<Object> updateAward(@PathVariable("awardId") Integer awardId, @RequestBody Award aw) {
	        String reason = null;        
	        if (awardId != null) {
	            if (aw != null) { 
	                ImmutablePair<Award, String> result = updateTheAward(aw);
	                reason = result.getRight();
	                aw = result.getLeft();
	                
	            } else {
	                reason = "not enough data to update the Award [" + awardId + "]";
	            }
	        }
	        
	        if (StringUtils.isNotBlank(reason)) {
	        	
	        	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
	        }
	        return ResponseEntity.status(HttpStatus.OK).body("Successfully updated!");
	    }
		
		@Transactional(rollbackFor = Exception.class)
	    private ImmutablePair<Award, String> updateTheAward(Award aw) {
	        String reason = null;
	        try {
	        	aw = awardService.updateAward(aw);
	        } catch(Exception ex) {
	        	ex.printStackTrace();
	        	reason = ex.getMessage();
	        }        
	        return new ImmutablePair<Award, String>(aw, reason);
	    }
}
