package com.cordialsr.controller;

import java.util.ArrayList;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.InvItemSn;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.reducer.InvoiceReducer;
import com.cordialsr.service.InvItemSnService;

@RepositoryRestController
@RequestMapping(value = "invItemSns")
public class InvItemSnController {

	@Autowired
	InvItemSnService invItemSnService;
	
	@RequestMapping(value = "/dataBySerNum", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Object> findInvItemSnByInvItem(@RequestBody InvItemSn invItemSn) {
		ArrayList<Invoice> newItemSn = new ArrayList<Invoice>();
        String reason = null;
        
        if (invItemSn != null) {
        	ImmutablePair<ArrayList<Invoice>, String> result = checkInvItemSn(invItemSn);
            reason = result.getRight();            
            newItemSn = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "InvItemSn to be created is null").body(invItemSn);
        }
        if (reason == null) {
        	boolean bool = false;
        	for (Invoice inv: newItemSn) {
        		if (bool) {
        			inv = InvoiceReducer.reduceForSerNumReport(inv);
        		}
        		bool = true;
        	}
            return ResponseEntity.status(HttpStatus.CREATED).body(newItemSn);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
////////////////////////////////////////////////////////////////////////////// SERVICE TYPE .......................................................
	
	private ImmutablePair<ArrayList<Invoice>, String> checkInvItemSn(InvItemSn invItemSn) {
		String reason = null;
		ArrayList<Invoice> invItemSns = new ArrayList<Invoice>();
	    try {
	    	invItemSns = invItemSnService.checkInvItemSns(invItemSn);
	    } catch(Exception ex) {
	       	ex.printStackTrace();
	       	reason = "Error: " + ex.getMessage();
	    }
	    return new ImmutablePair<ArrayList<Invoice>, String>(invItemSns, reason);
	}
	
}
