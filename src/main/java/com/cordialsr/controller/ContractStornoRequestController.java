package com.cordialsr.controller;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractStornoRequest;
import com.cordialsr.domain.reducer.ContractReducer;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.general.exception.DaoException;
import com.cordialsr.service.ContractStornoRequestService;

@RepositoryRestController
@RequestMapping(value = "contractStornoRequests")
public class ContractStornoRequestController {
	
	@Autowired
	ContractStornoRequestService reqService;
	
	@RequestMapping(value = "/new", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> saveContractStornoRequest(@RequestBody ContractStornoRequest newContractStornoRequest) {
		ContractStornoRequest createdContractStornoRequest;
        String reason = null;
        
        if (newContractStornoRequest != null) {
        	ImmutablePair<ContractStornoRequest, String> result = saveTheContractStornoRequest(newContractStornoRequest);
            reason = DaoException.justifyMessage(result.getRight());            
            createdContractStornoRequest = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "ContractStornoRequest to be created is null").body(newContractStornoRequest);
        }
 
        if (GeneralUtil.isEmptyString(reason) && createdContractStornoRequest != null && createdContractStornoRequest.getId() != null) {
        	createdContractStornoRequest.setContract(ContractReducer.reduceMin(createdContractStornoRequest.getContract()));
            return ResponseEntity.status(HttpStatus.CREATED).body(createdContractStornoRequest);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<ContractStornoRequest, String> saveTheContractStornoRequest(ContractStornoRequest newContractStornoRequest) {
        String reason = null;
        try {
        	reqService.saveNewContractStornoRequest(newContractStornoRequest);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();        	
        }        
        return new ImmutablePair<ContractStornoRequest, String>(newContractStornoRequest, reason);
    }
	
	
	// *******************************************************************************************************************************************
	
	
	@RequestMapping(value = "{conRequestId}",
            method = {RequestMethod.PATCH, RequestMethod.PUT},
            produces = MediaTypes.HAL_JSON_VALUE)	
	public @ResponseBody
    ResponseEntity<Object> updateContractStornoRequest(@PathVariable("conRequestId") Long conRequestId, @RequestBody ContractStornoRequest conRequest) {
        String reason = null;        
        if (conRequestId != null) {
            if (conRequest != null) { 
                ImmutablePair<ContractStornoRequest, String> result = updateTheContractStornoRequest(conRequest);
                reason = result.getRight();
                conRequest = result.getLeft();
                
            } else {
                reason = "not enough data to update the ContractStornoRequest [" + conRequestId + "]";
            }
        }
        if (StringUtils.isNotBlank(reason)) {
        	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
        }
        return ResponseEntity.status(HttpStatus.OK).body("Successfully updated!");
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<ContractStornoRequest, String> updateTheContractStornoRequest(ContractStornoRequest conRequest) {
        String reason = null;
        try {
        	conRequest = reqService.updateContractStornoRequest(conRequest);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();
        }        
        return new ImmutablePair<ContractStornoRequest, String>(conRequest, reason);
    }
	
	
	// ********************************************************************************************************************************************
	
//	@RequestMapping(value = "cancelContractStorno",
//            method = {RequestMethod.PATCH, RequestMethod.PUT},
//            produces = MediaTypes.HAL_JSON_VALUE)	
//	public @ResponseBody
//    ResponseEntity<Object> cancelContractStornoRequest(@PathVariable("conId") Long conId) {
//        String reason = null;        
//        if (conId != null) {
//                ImmutablePair<Contract, String> result = cancelContractStorno(conId);
//                reason = result.getRight();
//            } else {
//                reason = "not enough data to update the ContractStornoRequest [" + conId + "]";
//        }
//        if (StringUtils.isNotBlank(reason)) {
//        	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
//        }
//        return ResponseEntity.status(HttpStatus.OK).body("Successfully updated!");
//    }
//	
//	@Transactional(rollbackFor = Exception.class)
//    private ImmutablePair<Contract, String> cancelContractStorno(Long conId) {
//        String reason = null;
//        Contract contract = null;
//        try {
//        	contract  = reqService.cancelContractStorno(conId);
//        } catch(Exception ex) {
//        	ex.printStackTrace();
//        	reason = ex.getMessage();
//        }        
//        return new ImmutablePair<Contract, String>(contract, reason);
//    }
	
	
	
}
