package com.cordialsr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.security.Menu;
import com.cordialsr.service.MenuService;

@RepositoryRestController
@RequestMapping(value = "menus")
public class MenuController {

	@Autowired
	MenuService menuService;
	
	@RequestMapping(value = "/getMenuTreeByUser/{username}", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getMenuTreeByUser(@PathVariable("username") String username) {
		try {
			Iterable<Menu> mt = menuService.getMenuTreeByUsername(username);
			return ResponseEntity.status(HttpStatus.OK).body(mt);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
}
