package com.cordialsr.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.Dao.ContractDao;
import com.cordialsr.Dao.PartyDao;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractHistory;
import com.cordialsr.domain.CrmLead;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.Position;
import com.cordialsr.domain.reducer.BranchReducer;
import com.cordialsr.domain.reducer.ContractReducer;
import com.cordialsr.dto.ContractPaymentDto;
import com.cordialsr.dto.ContractPaymentListDto;
import com.cordialsr.dto.Money;
import com.cordialsr.dto.mini.ContractMiniDto;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.general.exception.DaoException;
import com.cordialsr.service.ContractService;
import com.cordialsr.service.ContractStornoRequestService;
import com.cordialsr.service.EmployeeService;

@RepositoryRestController
@RequestMapping(value = "contracts")
public class ContractController {
	
	@Autowired
	ContractService contractService;
	
	@Autowired
	EmployeeService emplService;
	
	@Autowired
	ContractDao conDao;
	
	@Autowired
	PartyDao partyDao;
	
	@Autowired
	ContractStornoRequestService conRequestService;
	
	@RequestMapping(value = "/newcontract", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> saveContract(@RequestBody Contract conToBeCreated) {
		Contract createdcon;
        String reason = null;
        
        if (conToBeCreated != null) {
        	ImmutablePair<Contract, String> result = saveTheCon(conToBeCreated);
            reason = DaoException.justifyMessage(result.getRight());            
            createdcon = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Contract to be created is null").body(conToBeCreated);
        }
 
        if (GeneralUtil.isEmptyString(reason) && createdcon != null && createdcon.getId() != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdcon);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<Contract, String> saveTheCon(Contract newCon) {
        String reason = null;
        Contract createdCon = null;
        try {
        	createdCon = contractService.saveContract(newCon);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();        	
        }
        return new ImmutablePair<Contract, String>(createdCon, reason);
    }
	
	// *****************************************************************************************

	
	@RequestMapping(value = "{contractId}",
            method = {RequestMethod.PATCH, RequestMethod.PUT},
            produces = MediaTypes.HAL_JSON_VALUE)	
	public @ResponseBody
    ResponseEntity<Object> updateContract(@PathVariable("contractId") Long contractId, @RequestBody Contract contract) {
        String reason = null;        
        if (contractId != null) {
            if (contract != null) { 
                ImmutablePair<Contract, String> result = updateTheContract(contract);
                reason = result.getRight();
                contract = result.getLeft();
                
            } else {
                reason = "not enough data to update the Contract [" + contractId + "]";
            }
        }
        if (StringUtils.isNotBlank(reason)) {
        	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
        }
        return ResponseEntity.status(HttpStatus.OK).body("Successfully updated!");
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<Contract, String> updateTheContract(Contract contract) {
        String reason = null;
        try {
        	contract = contractService.updateContract(contract);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();
        }        
        return new ImmutablePair<Contract, String>(contract, reason);
    }
	
	// ******************************************************************************************
	
	@RequestMapping(value = "reissue/{contractId}",
            method = {RequestMethod.PATCH, RequestMethod.PUT},
            produces = MediaTypes.HAL_JSON_VALUE)	
	public @ResponseBody
    ResponseEntity<Object> reissueContract(@PathVariable("contractId") Long contractId, @RequestBody Contract contract) {
        String reason = null;        
        if (contractId != null) {
            if (contract != null) { 
                ImmutablePair<Contract, String> result = reissueTheContract(contract);
                reason = result.getRight();
                contract = result.getLeft();
            } else {
                reason = "not enough data to update the Contract [" + contractId + "]";
            }
        }
        if (StringUtils.isNotBlank(reason)) {
        	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
        }
        return ResponseEntity.status(HttpStatus.OK).body("Successfully updated!");
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<Contract, String> reissueTheContract(Contract contract) {
        String reason = null;
        try {
        	contract = contractService.reissueContract(contract);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();
        }        
        return new ImmutablePair<Contract, String>(contract, reason);
    }
	
	// *****************************************************************************************
	
	@RequestMapping(value = "/getCollectorPaymentGraph", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getCollectorPaymentGraph(
    		    @RequestParam(value="cid", required=true) Integer cid,
    		    @RequestParam(value="bid", required=true) Integer bid,
    		    @RequestParam(value="sid", required=false) Long sid,
    		    @RequestParam(value="rent", required=false) Boolean rent,
    		    @RequestParam(value="sale", required=false) Boolean sale,
    		    @RequestParam(value="dte", required=true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date dte	    		
    		) {
		try {
			List<ContractPaymentListDto> cpsL = contractService.getCollectorPaymentGraph(cid, bid, sid, dte, rent, sale);
//			List<String> conList = new ArrayList();
//			List<String> customerList = new ArrayList();
//			List<String> iinList = new ArrayList();
//			List<BigDecimal> totalSumm = new ArrayList();
//			List<BigDecimal> expired = new ArrayList();
//			List<BigDecimal> current = new ArrayList();
//			List<BigDecimal> paymentDue = new ArrayList();
			
//			for (ContractPaymentListDto cps: cpsL) {
//				conList.add(cps.getContract().getContractNumber());
//				customerList.add(cps.getContract().getCustomer().getFullFIO());
//				iinList.add(cps.getContract().getCustomer().getIinBin());
//				totalSumm.add(cps.getRemain());
//				expired.add(cps.getOverdue());
//				current.add(cps.getCurrentSumm());
//				paymentDue.add(cps.getPaymentDue());
//				
//			}
			
			
			return ResponseEntity.status(HttpStatus.OK).body(cpsL);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// *****************************************************************************************
	
		@RequestMapping(value = "/getCollectorForMobile", method = RequestMethod.GET,
				produces = MediaTypes.HAL_JSON_VALUE)
		@ResponseBody
	    public ResponseEntity<Object> getCollectorForMobile(
	    		    @RequestParam(value="cid", required=true) Integer cid,
	    		    @RequestParam(value="bid", required=true) Integer bid,
	    		    @RequestParam(value="sid", required=false) Long sid,
	    		    @RequestParam(value="rent", required=false) Boolean rent,
	    		    @RequestParam(value="sale", required=false) Boolean sale,
	    		    @RequestParam(value="dte", required=true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date dte	    		
	    		) {
			try {
				List<ContractPaymentListDto> cpsL = contractService.getCollectorsList(cid, bid, sid, dte, rent, sale);
				return ResponseEntity.status(HttpStatus.OK).body(cpsL);
			} catch (Exception e) {
				e.printStackTrace();
				return ResponseEntity
	                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
	                    .body(e.getMessage());
			}
	    }
	
	// *****************************************************************************************
	
	@RequestMapping(value = "/getAllReceivables", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getAllReceivables(
    		    @RequestParam(value="cid", required=true) Integer cid,
    		    @RequestParam(value="bid", required=true) Integer bid,
    		    @RequestParam(value="dte", required=true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date dte,
    		    @RequestParam(value="contract", required=false) Long contractId,
    		    @RequestParam(value="customer", required=false) Long customerId,
    		    @RequestParam(value="collector", required=false) Long collectorId,
    		    @RequestParam(value="dealer", required=false) Long dealerId
    		) {
		try {
			List<ContractPaymentDto> cpL = contractService.getAllReceivables(cid, bid, dte, contractId, customerId, collectorId, dealerId);
			for (ContractPaymentDto cp : cpL) {
				cp.setBranch(BranchReducer.reduceMax(cp.getBranch()));
				cp.setContract(ContractReducer.reduceMed(cp.getContract()));
			}
			return ResponseEntity.status(HttpStatus.OK).body(cpL);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
		
	// *****************************************************************************************
	
	@RequestMapping(value = "/getContractList", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getContractList(
    		    @RequestParam(value="cid", required=true) Integer cid,
    		    @RequestParam(value="bid", required=true) Integer bid,
    		    @RequestParam(value="storno", required=false) Boolean storno,
    		    @RequestParam(value="servbr", required=false) Boolean servbr,
    		    @RequestParam(value="isRent", required=false) Boolean isRent,
    		    @RequestParam(value="empl", required=false) Long empl,
    		    @RequestParam(value="dts", required=false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date dts,
    		    @RequestParam(value="dte", required=false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date dte,
    		    @RequestParam(value="caremanSales", required=false) Boolean caremanSales,
    		    @RequestParam(value="filter", required=false) String filter,
    		    Pageable pageable
    		) {
		try {
			Page<Contract> pages = contractService.getContractList(cid, bid, storno, servbr, isRent, empl, dts, dte, caremanSales, filter, pageable);
			List<Contract> conL = new ArrayList<>();
			for (Contract con : pages) {
				Contract c = ContractReducer.reduceMed(con);
				conL.add(c);
			}
			Page<Contract> res = new PageImpl<Contract>(conL, pageable, pages.getTotalElements());  
			return ResponseEntity.status(HttpStatus.OK).body(res);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
//	 ========================================================================================================================================
	
	@RequestMapping(value = "/saveContractChanges", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> saveMassiveContracts(@RequestBody Contract[] contracts, 
											@RequestParam(value="careman", required=false) Long careman,
											@RequestParam(value="collector", required=false) Long collector) {
		Contract[] changedContract;
        String reason = null;
        
        if (contracts != null) {
        	ImmutablePair<Contract[], String> result = saveTheContracts(contracts, careman, collector);
            reason = DaoException.justifyMessage(result.getRight());            
            changedContract = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "No contracts").body(contracts);
        }
 
        if (GeneralUtil.isEmptyString(reason) && changedContract != null && changedContract.length > 0) {
            return ResponseEntity.status(HttpStatus.CREATED).body(changedContract);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<Contract[], String> saveTheContracts(Contract[] contracts,
    															Long careman, Long collector) {
        String reason = null;
        try {
        	contractService.saveContractsChange(contracts, careman, collector);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();        	
        }        
        return new ImmutablePair<Contract[], String>(contracts, reason);
    }
	
// ===============================================================================================================================================
	
	@RequestMapping(value = "cancelContractStorno",
            method = {RequestMethod.PATCH, RequestMethod.PUT},
            produces = MediaTypes.HAL_JSON_VALUE)	
	public @ResponseBody
    ResponseEntity<Object> cancelContractStornoRequest(@RequestBody Long contractId) {
        String reason = null; 
       
            if (contractId != null) { 
                ImmutablePair<Long, String> result = cancelContractStorno(contractId);
                reason = result.getRight();
                contractId = result.getLeft();
                
            } else {
                reason = "not enough data to update the Contract [";
            }
        if (StringUtils.isNotBlank(reason)) {
        	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
        }
        return ResponseEntity.status(HttpStatus.OK).body("Successfully updated!");
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<Long, String> cancelContractStorno(Long conId) {
        String reason = null;
        Contract contract = null;
        try {
        	contract = conRequestService.cancelContractStorno(conId);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();
        }        
        return new ImmutablePair<Long, String>(conId, reason);
    }


	// *****************************************************************************************
	
	@RequestMapping(value = "/newConHistory", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> saveContractHistory(@RequestBody ContractHistory conHistory) {
		ContractHistory createdcon;
        String reason = null;
        
        if (conHistory != null) {
        	ImmutablePair<ContractHistory, String> result = saveTheConHistory(conHistory);
            reason = DaoException.justifyMessage(result.getRight());            
            createdcon = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Contract to be created is null").body(conHistory);
        }
 
        if (GeneralUtil.isEmptyString(reason) && createdcon != null && createdcon.getId() != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdcon);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<ContractHistory, String> saveTheConHistory(ContractHistory newConHistory) {
        String reason = null;
        ContractHistory createdCon = null;
        try {
        	createdCon = contractService.saveContractHistory(newConHistory);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();        	
        }
        return new ImmutablePair<ContractHistory, String>(createdCon, reason);
    }
	
	// *****************************************************************************************

	
	@RequestMapping(value = "/getCustomerPaymentDueByIIN", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getCustomerPaymentDueByIIN(
    		    @RequestParam(value="iinBin", required=false) String iinBin,
    		    @RequestParam(value="dateEnd", required=false) @DateTimeFormat(pattern = "yyyy-MM-dd") Calendar dateEnd,
    		    @RequestParam(value="curr", required=false) String curr,
    		    @RequestParam(value="rental", required=false) Boolean rental,
    		    @RequestParam(value="sales", required=false) Boolean sales
    		) {
		try {
			Money money = contractService.getCustomerPaymentDueByIIN(iinBin, dateEnd, curr, rental, sales);
			return ResponseEntity.status(HttpStatus.OK).body(money);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// ******************************************************************************************
	
	@RequestMapping(value = "/getCustomerPaymentDueByID", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Money> getCustomerPaymentDueByID(
    		    @RequestParam(value="customerId", required=false) Long customerId,
    		    @RequestParam(value="dateEnd", required=false) @DateTimeFormat(pattern = "yyyy-MM-dd") Calendar dateEnd,
    		    @RequestParam(value="currency", required=false) String currency,
    		    @RequestParam(value="rental", required=false) Boolean rental,
    		    @RequestParam(value="sales", required=false) Boolean sales
    		) {
		try {
			
			Money money = contractService.getCustomerPaymentDueByID(customerId, dateEnd, currency, rental, sales);
			return ResponseEntity.status(HttpStatus.OK).body(money);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
    }
	
	
	// ******************************************************************************************
	
		@RequestMapping(value = "/getCustomerContractSummByID", method = RequestMethod.GET,
				produces = MediaTypes.HAL_JSON_VALUE)
		@ResponseBody
	    public ResponseEntity<Money> getCustomerContractSummByID(
	    		    @RequestParam(value="customerId", required=false) Long customerId,
	    		    @RequestParam(value="dateEnd", required=false) @DateTimeFormat(pattern = "yyyy-MM-dd") Calendar dateEnd,
	    		    @RequestParam(value="currency", required=false) String currency,
	    		    @RequestParam(value="rental", required=false) Boolean rental,
	    		    @RequestParam(value="sales", required=false) Boolean sales
	    		) {
			try {
				
				Money money = contractService.getCustomerContractSummByID(customerId, dateEnd, currency, rental, sales);
				return ResponseEntity.status(HttpStatus.OK).body(money);
			} catch (Exception e) {
				e.printStackTrace();
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
			}
	    }
	
	// *****************************************************************************************
	
		@RequestMapping(value = "/changeContractEmployee", method = RequestMethod.POST, 
	            produces = MediaTypes.HAL_JSON_VALUE)
		@ResponseBody 
		public ResponseEntity<Object> changeContractEmployee(
						@RequestBody ContractHistory conHistory,
						@RequestParam(value="contractId", required = true) Long contractId,
						@RequestParam(value="newEmplId", required = true) Long newEmplId,
						@RequestParam(value="position", required = true) Integer position) {
			Contract contract = conDao.findcid(contractId);
	        Party newParty = new Party();
	        Employee oldEmpl = new Employee();
			newParty = partyDao.findPartyByEmployee(newEmplId);
			String reason = null;
	        
	        
	        if (contract != null) {
	        	if (position == Position.POS_CAREMAN) {
	        		oldEmpl = contract.getCareman();
	        	} else if (position == Position.POS_COLLECTOR) {
	        		oldEmpl = contract.getCollector();
	        	} else if (position == Position.POS_COORDINATOR) {
	        		oldEmpl = contract.getCoordinator();
	        	} else if (position == Position.POS_DEALER) {
	        		oldEmpl = contract.getDealer();
	        	} else if (position == Position.POS_DEMOSEC) {
	        		oldEmpl = contract.getDemosec();
	        	} else if (position == Position.POS_DIRECTOR) {
	        		oldEmpl = contract.getDirector();
	        	} else if (position == Position.POS_FITTER) {
	        		oldEmpl = contract.getFitter();
	        	} else if (position == Position.POS_MANAGER) {
	        		oldEmpl = contract.getManager();
	        	}
	        	
	        	ImmutablePair<Contract, String> result = staffChange(conHistory, contractId, 
	        											(newParty != null ? newParty.getId() : null) , newEmplId,
	        											(oldEmpl != null ? oldEmpl.getId() : null), position);
	            reason = DaoException.justifyMessage(result.getRight());            
	            contract = result.getLeft();
	        } else {
	            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Contract to be created is null").body(contract);
	        }
	 
	        if (GeneralUtil.isEmptyString(reason) && contract != null && contract.getId() != null) {
	            return ResponseEntity.status(HttpStatus.CREATED).body(contract);
	        } else {
	            return ResponseEntity
	                    .status(HttpStatus.BAD_REQUEST)
	                    .body(reason);
	        }
	    }
		
		@Transactional(rollbackFor = Exception.class)
	    private ImmutablePair<Contract, String> staffChange(ContractHistory newConHistory, Long contractId,
	    											Long newPartyId, Long newEmplId, Long oldEmplId, Integer position) {
	        String reason = null;
	        Contract contract = null;
	        try {
	        	contract = contractService.changeContractEmployee(newConHistory, contractId, newPartyId, newEmplId, oldEmplId,position);
	        } catch(Exception ex) {
	        	ex.printStackTrace();
	        	reason = ex.getMessage();        	
	        }
	        return new ImmutablePair<Contract, String>(contract, reason);
	    }
		
		// *****************************************************************************************
		
//		@RequestMapping(value = "/getContractList", method = RequestMethod.GET,
//				produces = MediaTypes.HAL_JSON_VALUE)
//		@ResponseBody
//	    public ResponseEntity<Object> getContractListByCustomerId(
//	    		    @RequestParam(value="customerId", required=true) Long customerId
//	    		) {
//			try {
//				List<Contract> contracts = contractService.getContractByCustomerId(customerId);
//				List<Contract> conL = new ArrayList<>();
//				for (Contract con : contracts) {
//					Contract c = ContractReducer.reduceMed(con);
//					conL.add(c);
//				}
//				List<Contract> res = conL;  
//				return ResponseEntity.status(HttpStatus.OK).body(res);
//			} catch (Exception e) {
//				e.printStackTrace();
//				return ResponseEntity
//	                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
//	                    .body(e.getMessage());
//			}
//	    }

}
