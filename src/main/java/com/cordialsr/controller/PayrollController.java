package com.cordialsr.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.Payroll;
import com.cordialsr.domain.PlanFact;
import com.cordialsr.domain.reducer.PayrollReducer;
import com.cordialsr.dto.BlobDto;
import com.cordialsr.dto.mini.PlanFactMiniDto;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.PayrollService;

@RepositoryRestController
@RequestMapping(value = "payrolls")
public class PayrollController {
	
	@Autowired
	PayrollService prlService;
	
	private ModelMapper modelMapper = new ModelMapper();
	
	@RequestMapping(value = "/getPrlByMon", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getFdById(
    		@RequestParam("cid") Integer cid,
    		@RequestParam("bid") Integer bid,
    		@RequestParam("knd") String knd,
    		@RequestParam(value = "pos", required = false) Integer pos,
    		@RequestParam(value = "dbt", required = false) Boolean dbt,
    		@RequestParam("mon") Integer mon,
    		@RequestParam("year") Integer year
    		) {
		try {
			List<Payroll> prlL = prlService.getPayrollsForMonth(cid, bid, knd, pos, dbt, mon, year);
//			List<PayrollMiniDto> res = new ArrayList<>();
//			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
//			for (PlanFact pf: conps) {
//				PlanFactMiniDto pp = new PlanFactMiniDto();
//				modelMapper.map(pf, pp);
//				res.add(pp);
//			}
			
			for (Payroll p:prlL) {
				PayrollReducer.reducePayroll(p);
			}
			
			if (prlL == null) {
				return ResponseEntity.status(HttpStatus.OK).body("Payroll not found!");
			}
			return ResponseEntity.status(HttpStatus.OK).body(prlL);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// *********************************************************************************************
	
	@RequestMapping(value = "/genBonSch", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> generateBonusSchedule(
    		@RequestParam("cid") Integer cid,
    		@RequestParam("bid") Integer bid,
    		@RequestParam(value = "pos", required = false) Integer pos,
    		@RequestParam("mon") Integer mon,
    		@RequestParam("year") Integer year
    		) {
		try {
			List<Payroll> prlL = prlService.generateBonusSchedule(cid, bid, pos, mon, year);
			for (Payroll p:prlL) {
				PayrollReducer.reducePayroll(p);
			}
			
			if (prlL == null) {
				return ResponseEntity.status(HttpStatus.OK).body("Payroll not found!");
			}
			return ResponseEntity.status(HttpStatus.OK).body(prlL);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// *********************************************************************************************
	
	@RequestMapping(value = "/doPostPayrolls", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> postPayrolls(@RequestParam("cid") Integer cid, @RequestBody List<Payroll> prlL) {
		String reason = null;
        
        if (prlL != null && prlL.size() > 0) {
        	ImmutablePair<List<Payroll>, String> result = doPostPayrolls(cid, prlL);
            reason = result.getRight();            
            prlL = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Payroll list is null or empty.").body(prlL);
        }
 
        if (GeneralUtil.isEmptyString(reason) && prlL != null && prlL.size() > 0) {
        	for (Payroll p:prlL) {
				PayrollReducer.reducePayroll(p);
			}
            return ResponseEntity.status(HttpStatus.CREATED).body(prlL);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<List<Payroll>, String> doPostPayrolls(Integer cid, List<Payroll> prlL) {
        String reason = null;
        List<Payroll> postedPrls = null;
        try {
        	postedPrls = prlService.doPostPayrolls(cid, prlL);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();
        }
        return new ImmutablePair<List<Payroll>, String>(postedPrls, reason);
    }
	
	// *********************************************************************************************
	
	@RequestMapping(value = "/prlxls", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> payrollExcel(
			@RequestParam("cid") Integer cid, 
			@RequestParam("bid") Integer bid, 
			@RequestParam("month") Integer month, 
			@RequestParam("year") Integer year, 
			@RequestBody BlobDto xlsBlob) {
		String reason = null;
		List<Payroll> prlL = null;
		if (xlsBlob != null) {
			String xls64 = xlsBlob.getBin64();
			if (xls64 != null) {
	        	ImmutablePair<List<Payroll>, String> result = getPayrollsFromXls(cid, bid, month, year, xls64);
	            reason = result.getRight();
	            prlL = result.getLeft();
	        } else {
	            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Payroll list is null or empty.").body(prlL);
	        }
		}
		if (GeneralUtil.isEmptyString(reason) && prlL != null) {
        	for (Payroll p:prlL) {
				PayrollReducer.reducePayroll(p);
			}
            return ResponseEntity.status(HttpStatus.CREATED).body(prlL);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<List<Payroll>, String> getPayrollsFromXls(Integer cid, Integer bid, Integer month, Integer year, String xls64) {
        String reason = null;
        List<Payroll> prlL = null;
        try {
        	prlL = prlService.getPayrollsFromXls(cid, bid, month, year, xls64);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();
        }
        return new ImmutablePair<List<Payroll>, String>(prlL, reason);
    }
}
