package com.cordialsr.controller;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.SmContract;
import com.cordialsr.service.SmContractService;

@RepositoryRestController
@RequestMapping(value = "smcon")
public class SmConController {

	@Autowired
	SmContractService smcService;
	
	@RequestMapping(value = "/actCancCount", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getActiveCancelCount() {
		try {
			Object o = smcService.getActiveCancelledCount();
			return ResponseEntity.status(HttpStatus.OK).body(o);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// *******************************************************************************************************
	
	@RequestMapping(value = "{id}",
            method = {RequestMethod.PATCH, RequestMethod.PUT},
            produces = MediaTypes.HAL_JSON_VALUE)	
	public @ResponseBody
    ResponseEntity<Object> updateSmContract(@PathVariable("id") Integer conId, @RequestBody SmContract smc) {
        String reason = null;        
        if (conId != null) {
            if (smc != null) { 
                ImmutablePair<SmContract, String> result = updateTheSmc(smc);
                reason = result.getRight();
                smc = result.getLeft();
            } else {
                reason = "not enough data to update the SmContract [" + conId + "]";
            }
        }
        if (StringUtils.isNotBlank(reason)) {
        	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
        }
        return ResponseEntity.status(HttpStatus.OK).body(smc);
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<SmContract, String> updateTheSmc(SmContract newSmc) {
        String reason = null;
        try {
        	newSmc = smcService.updateSmContract(newSmc);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();
        }        
        return new ImmutablePair<SmContract, String>(newSmc, reason);
    }
	
}
