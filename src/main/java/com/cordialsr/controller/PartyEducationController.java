package com.cordialsr.controller;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.PartyEducation;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.general.exception.DaoException;
import com.cordialsr.service.PartyEducationService;

@RepositoryRestController
@RequestMapping(value = "educations")
public class PartyEducationController {

	
	@Autowired
	PartyEducationService eduService;
	
	@RequestMapping(value = "/new", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> saveEducation(@RequestBody PartyEducation newEducation) {
		PartyEducation createdPartyEducation;
        String reason = null;
        
        if (newEducation != null) {
        	ImmutablePair<PartyEducation, String> result = saveTheEducation(newEducation);
            reason = DaoException.justifyMessage(result.getRight());
            createdPartyEducation = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Education to be created is null").body(newEducation);
        }
 
        if (GeneralUtil.isEmptyString(reason) && createdPartyEducation != null && createdPartyEducation.getId() != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdPartyEducation);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }	
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<PartyEducation, String> saveTheEducation(PartyEducation newEducation) {
        String reason = null;
        try {
        	eduService.saveNewEducation(newEducation);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();        	
        }        
        return new ImmutablePair<PartyEducation, String>(newEducation, reason);
    }
}
