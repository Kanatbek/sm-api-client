package com.cordialsr.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.FinEntry;
import com.cordialsr.domain.KassaBank;
import com.cordialsr.domain.reducer.FinEntryReducer;
import com.cordialsr.service.KassaBankService;

@RepositoryRestController
@RequestMapping(value = "kb")
public class KassaBankController {
	
	@Autowired
	KassaBankService kbService;
	
	@RequestMapping(value = "/blnc/{kbid}", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getCashBalance(
    		@PathVariable("kbid") Integer kbid) {
		try {
			Object o = kbService.getKassaBalance(kbid);
			return ResponseEntity.status(HttpStatus.OK).body(o);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	
	@RequestMapping(value = "/blnc/{kbid}/{date}", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getCashBalanceByDate(
    		@PathVariable("kbid") Integer kbid,
    		@PathVariable("date") String date) {
		try {
			Object o = kbService.getKassaBalanceByDate(kbid, date);
			return ResponseEntity.status(HttpStatus.OK).body(o);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// ********************************************************************************************************************
	
	@RequestMapping(value = "/fes/{kbid}/{ds}/{de}", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getCashBookEntries(
    		@PathVariable("kbid") Integer kbid,
    		@PathVariable("ds") String ds,
    		@PathVariable("de") String de) {
		try {
			List<FinEntry> fes = kbService.getKbFes(kbid, ds, de);
			for (FinEntry fe: fes) {
				fe = FinEntryReducer.reduceFeForView(fe);
			}
			return ResponseEntity.status(HttpStatus.OK).body(fes);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// ********************************************************************************************************************
	
	@RequestMapping(value = "/new", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> saveKassaBank(@RequestBody KassaBank newKb) {
		KassaBank createdKb;
        String reason = null;
        
        if (newKb != null) {
        	ImmutablePair<KassaBank, String> result = saveTheKb(newKb);
            reason = result.getRight();
            createdKb = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Contract to be created is null").body(newKb);
        }
 
        if (createdKb != null && createdKb.getId() != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdKb);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional
    private ImmutablePair<KassaBank, String> saveTheKb(KassaBank newKb) {
        String reason = null;
        try {
        	kbService.saveNewKb(newKb);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();        	
        }        
        return new ImmutablePair<KassaBank, String>(newKb, reason);
    }
}
