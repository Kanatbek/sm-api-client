package com.cordialsr.controller;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.reducer.InvoiceReducer;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.InvoiceService;

@RepositoryRestController
@RequestMapping(value = "invoices")
public class InvoiceController {

	@Autowired
	InvoiceService invoiceService;
	
	//////////////////////////////////////////// New - Invoices ...
	
	@RequestMapping(value = "/newInvoice", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Object> saveNewInvoice(
    		@RequestBody Invoice invBody,
    		@RequestParam("trCode") String trCode,
    		@RequestParam("userId") Long userId
    	) {
        Invoice toBeCreated = invBody;
        Invoice created;
        String reason = null;
        
        if (toBeCreated != null) {
        	ImmutablePair<Invoice, String> result = saveTheInv(toBeCreated, trCode, userId);
            reason = result.getRight();            
            created = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Invoice to be created is null").body(toBeCreated);
        }
 
        if (reason == null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(created);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@RequestMapping(value = "/invoiceReturn", method = RequestMethod.PUT, 
            produces = MediaTypes.HAL_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Object> newInvoiceReturn(@RequestBody Invoice invBody) {
		
        Invoice created;
        String reason = null;
        
        if (invBody == null) {
        	return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Invoice to be created is null").body(invBody);
        } else if (invBody.getId() == null) {
        	return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Invoice id is null").body(invBody);
        } else {
        	ImmutablePair<Invoice, String> result = invoiceReturn(invBody);
            reason = result.getRight();            
            created = result.getLeft();
        }
        if (reason == null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(created);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	/////////////////////////////////// Invoice Return service ....
	@Transactional(rollbackFor = Exception.class)
	private ImmutablePair<Invoice, String> invoiceReturn(Invoice newInv) {
		 String reason = null;
	        try {
	        	newInv = invoiceService.returnInvoice(newInv);
	        } catch(Exception ex) {
	        	ex.printStackTrace();
	        	reason = "Error: " + ex.getMessage();
	        }
	        return new ImmutablePair<Invoice, String>(newInv, reason);
	}


	///////////////////////////////////// Spatially for posting ..........
	
	@RequestMapping(value = "/forposting/{id}", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getSingleInvoiceById(@PathVariable("id") Long invId) {
		try {
			Invoice invoice = invoiceService.getInvoiceById(invId);
			invoice  = InvoiceReducer.reduceInvoiceForPosting(invoice);
			
			if (invoice != null && !GeneralUtil.isEmptyLong(invoice.getId())) {
				return ResponseEntity.status(HttpStatus.OK).body(invoice);
			} else {
				return ResponseEntity
	                    .status(HttpStatus.NOT_FOUND)
	                    .body("Invoice [" + invId + "] not found.");
			}			
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
   //////////////////////////////////////////////// Report for Services ............
	
	@RequestMapping(value = "/serviceReport", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getInvoiceForServices(@RequestParam("cid") Integer cid,
    												    @RequestParam("bid") Integer bid,
    		     @DateTimeFormat(pattern = "yyyy-MM-dd")@RequestParam("from") Date from,
    			 @DateTimeFormat(pattern = "yyyy-MM-dd")@RequestParam("to") Date to) {
		try {
			
			Invoice invoice  = invoiceService.getInvoiceForServices(cid, bid, from, to);
			
			if (invoice != null) {
				return ResponseEntity.status(HttpStatus.OK).body(invoice);
			} else {
				return ResponseEntity
	                    .status(HttpStatus.NOT_FOUND)
	                    .body("Invoices for repost not found.");
			}			
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	
	 ///////////////////////////////////////////////////// PUT METHOD /////////////////////////
	
	@RequestMapping(value = "{id}",
            method = {RequestMethod.PATCH, RequestMethod.PUT},
            produces = MediaTypes.HAL_JSON_VALUE)	
	public @ResponseBody
    ResponseEntity<Object> updateInvoice(@PathVariable("id") Long invId, @RequestBody Invoice invBody) {
        String reason = null;
        
        Invoice changedInv;
		try {
			changedInv = invoiceService.getInvoiceById(invId);
		} catch (Exception e) {
			return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
		}
 
        if (invId != null) {
            if (changedInv == null) {
                return ResponseEntity
                        .status(HttpStatus.NO_CONTENT)
                        .body("Update is impossible for a transient entity!");
            }
            Invoice newInv = invBody;
            if (newInv != null) { 
                ImmutablePair<Invoice, String> result = updateTheInv(newInv);
                reason = result.getRight();                
            } else {
                reason = "not enough data to update the Invoice [" + invId + "]";
            }
        }
        if (StringUtils.isNotBlank(reason)) {
        	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
        }
        return ResponseEntity.status(HttpStatus.OK).body(changedInv);
    }

	
	 //////////////////////////////////////////////////////////////// Connection with service ///////////////////////////////////////////////////////////////////////////////////
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<Invoice, String> saveTheInv(Invoice changedFd, String trCode, Long userId) {
        String reason = null;
        try {
        	changedFd = invoiceService.invoiceToType(changedFd, trCode, userId);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = "Error: " + ex.getMessage();        	
        }
        return new ImmutablePair<Invoice, String>(changedFd, reason);
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<Invoice, String> updateTheInv(Invoice newInv) {
        String reason = null;
        try {
        	newInv = invoiceService.updateInvoice(newInv);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = "Error: " + ex.getMessage();
        }
        return new ImmutablePair<Invoice, String>(newInv, reason);
    }
}
