package com.cordialsr.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.Dao.PartyDao;
import com.cordialsr.domain.Party;
import com.cordialsr.service.PartyService;

@RepositoryRestController
@RequestMapping(value = "parties")
public class PartyController {

	@Autowired
	private PartyService partyService;
	
	@Autowired
	private PartyDao partyDao;
	
	
	@RequestMapping(value = "/party", method = {RequestMethod.PATCH, RequestMethod.POST},
            produces = MediaTypes.HAL_JSON_VALUE)	
	public @ResponseBody
    ResponseEntity<Object> saveParty(@RequestBody Party party) {
        try {
			String reason = null;
			if (party != null) {
			    ImmutablePair<Party, String> result = saveTheParty(party);
			    reason = result.getRight();
			    party = result.getLeft();
			} else {
				reason = "Party is null.";
			}
			if (StringUtils.isNotBlank(reason)) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
			}
			return ResponseEntity.status(HttpStatus.OK).body(party);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<Party, String> saveTheParty(Party party) {
        String reason = null;
        try {
        	party = partyService.saveParty(party);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();
        }        
        return new ImmutablePair<Party, String>(party, reason);
    }
	
	// *******************************************************************************************************
	
	@RequestMapping(value = "{partyId}",
            method = {RequestMethod.PATCH, RequestMethod.PUT},
            produces = MediaTypes.HAL_JSON_VALUE)	
	public @ResponseBody
    ResponseEntity<Object> updateParty(@PathVariable("partyId") Long partyId, @RequestBody Party party) {
        String reason = null;        
        if (partyId != null) {
            if (party != null) { 
                ImmutablePair<Party, String> result = updateTheParty(party);
                reason = result.getRight();
                party = result.getLeft();
            } else {
                reason = "not enough data to update the Party [" + partyId + "]";
            }
        }
        if (StringUtils.isNotBlank(reason)) {
        	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
        }
        return ResponseEntity.status(HttpStatus.OK).body(party);
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<Party, String> updateTheParty(Party party) {
        String reason = null;
        try {
        	party = partyService.updateParty(party);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();
        }        
        return new ImmutablePair<Party, String>(party, reason);
    }
	
	// *******************************************************************************************************

	
	@RequestMapping(value = "/checkIin/{iinBin}", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getPartyIinBin(
    		@PathVariable("iinBin") String iinBin) {
		try {

			Party party = partyDao.findPartyIinBin(iinBin);
			
			if (party == null) {
				return ResponseEntity.status(HttpStatus.OK).body(true);
			} else return ResponseEntity.status(HttpStatus.OK).body(false);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// ********************************************************************************************************
	
	@RequestMapping(value="getEmployeeList", method = RequestMethod.GET,
				produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Object> getEmployeeList(
			@RequestParam(value = "cid", required = true) Integer cid,
			@RequestParam(value = "bid", required = false) Integer bid,
			@RequestParam(value = "posId", required = false) Integer posId,
			@RequestParam(value = "fired", required = false) Boolean fired,
			@RequestParam(value = "sortBy", required = false) String sortBy,
			@RequestParam(value = "filter", required=false) String filter,
		    Pageable pageable
		    ) {
		try {
			Page<Party> pages = partyService.getEmployeeList(cid, bid, posId,fired,  sortBy, filter, pageable);
			List<Party> emplL = new ArrayList<>();
				for (Party party : pages) {
					emplL.add(party);
				}
			
			Page<Party> res = new PageImpl<Party>(emplL, pageable, pages.getTotalElements());
			return ResponseEntity.status(HttpStatus.OK).body(res);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
	}
	

}
