package com.cordialsr.controller;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.FinCurrate;
import com.cordialsr.domain.reducer.UserReducer;
import com.cordialsr.dto.Money;
import com.cordialsr.general.exception.DaoException;
import com.cordialsr.service.CurrateService;

@Controller
@RequestMapping(value = "currates")
public class CurrateController {

	@Autowired
	CurrateService crService;
	
	@RequestMapping(value = "/new", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> saveCurrate(@RequestBody FinCurrate newCr) {
		FinCurrate createdCr;
        String reason = null;
        
        if (newCr != null) {
        	ImmutablePair<FinCurrate, String> result = saveTheCr(newCr);
            reason = DaoException.justifyMessage(result.getRight());            
            createdCr = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Currency rate to be created is null").body(newCr);
        }
 
        if (createdCr != null && createdCr.getId() != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdCr);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional
    private ImmutablePair<FinCurrate, String> saveTheCr(FinCurrate newCr) {
        String reason = null;
        try {
        	crService.saveNewCurrate(newCr);
        } catch(Exception ex) {
        	reason = ex.getMessage();        	
        }        
        return new ImmutablePair<FinCurrate, String>(newCr, reason);
    }

	// ********************************************************************************************************************
	
	@Autowired
	CurrateService rateService;
	
	@RequestMapping(value = "/getLastNBRates", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getNBRates(@RequestParam("bid") Integer bid) {
		try {
			List<Money> rates = rateService.getLastNbRates(bid);
			return ResponseEntity.status(HttpStatus.OK).body(rates);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// ********************************************************************************************************************

	
	@RequestMapping(value = "/getExtRate", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getExtRate(
    		@RequestParam("cid") Integer cid,
    		@RequestParam("mc") String mc,
    		@RequestParam("sc") String sc,
    		@DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam("dt") Date dt
    		) {
		try {
			FinCurrate rate = rateService.getRateExt(cid, mc, sc, dt);

			if (rate != null) {
				rate.setUserAuthor(UserReducer.reduceMax(rate.getUserAuthor()));
				return ResponseEntity.status(HttpStatus.OK).body(rate);
			}
			else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Not found.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	
	// ******************************************************************************************************************
	
	@RequestMapping(value = "/getIntRate", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getIntRate(
    		@RequestParam("cid") Integer cid,
    		@RequestParam("mc") String mc,
    		@RequestParam("sc") String sc,
    		@DateTimeFormat(pattern = "yyyy-MM-dd") @RequestParam("dt") Date dt
    		) {
		try {
			FinCurrate rate = rateService.getRateInt(cid, mc, sc, dt);
			
			if (rate != null) {
				rate.setUserAuthor(UserReducer.reduceMax(rate.getUserAuthor()));
				return ResponseEntity.status(HttpStatus.OK).body(rate);
			}
			else {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Not found.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// ******************************************************************************************************************
	
		@RequestMapping(value = "/getCurrentRates", method = RequestMethod.GET,
				produces = MediaTypes.HAL_JSON_VALUE)
		@ResponseBody
	    public ResponseEntity<Object> getCurrenctRates(
	    		@RequestParam("bid") Integer bid
	    		) {
			try {
				
				List<FinCurrate> rateList = rateService.getCurrentRates(bid);
				
				if (rateList != null) {
					return ResponseEntity.status(HttpStatus.OK).body(rateList);
				}
				else {
					return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Not found.");
				}
			} catch (Exception e) {
				e.printStackTrace();
				return ResponseEntity
	                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
	                    .body(e.getMessage());
			}
	    }
}
