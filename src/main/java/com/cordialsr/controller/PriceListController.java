package com.cordialsr.controller;

import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.PriceList;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.general.exception.DaoException;
import com.cordialsr.service.PriceListService;

@RepositoryRestController
@RequestMapping(value = "priceLists")
public class PriceListController {
	
	@Autowired
	PriceListService plService;
	
	
	@RequestMapping(value = "/new", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> saveNewPl(@RequestBody PriceList newPl) {
		PriceList createdPl;
        String reason = null;
        
        if (newPl != null) {
        	ImmutablePair<PriceList, String> result = saveThePl(newPl);
            reason = DaoException.justifyMessage(result.getRight());            
            createdPl = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Price to be created is null").body(newPl);
        }
 
        if (GeneralUtil.isEmptyString(reason) && createdPl != null && createdPl.getId() != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdPl);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional
    private ImmutablePair<PriceList, String> saveThePl(PriceList newPl) {
        String reason = null;
        try {
        	plService.saveNewPriceList(newPl);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();        	
        }        
        return new ImmutablePair<PriceList, String>(newPl, reason);
    }
	
	// ***********************************************************************************************************
	
	@RequestMapping(value = "/getAllBySample", method = RequestMethod.POST,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getAllByExample(@RequestBody PriceList pl) {
		try {
			List<PriceList> plL = plService.getAllByExample(pl);
			return ResponseEntity.status(HttpStatus.OK).body(plL);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// ***********************************************************************************************************
	
	@RequestMapping(value = "/getPriceByScPrior/{cid}", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getPriceByScopePriority(
    		@PathVariable("cid") Integer cid,
    		@RequestParam("bid") Integer bid,
    		@RequestParam("inv") Integer inv,
    		@RequestParam("isRent") Boolean isRent,
    		@RequestParam("isYur") Boolean isYur,
    		@RequestParam(value = "dt") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt
    	) {
		try {
			List<PriceList> plL = plService.getPriceByScopePriority(cid, bid, inv, isRent, isYur, dt);
			return ResponseEntity.status(HttpStatus.OK).body(plL);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	
	// **********************************************************************************************************************************************************
	@RequestMapping(value = "{priceId}",
            method = {RequestMethod.PATCH, RequestMethod.PUT},
            produces = MediaTypes.HAL_JSON_VALUE)	
	public @ResponseBody
    ResponseEntity<Object> updateAward(@PathVariable("priceId") Integer priceListId, @RequestBody PriceList pList) {
        String reason = null;        
        if (priceListId != null) {
            if (pList != null) { 
                ImmutablePair<PriceList, String> result = updateThePriceList(pList);
                reason = result.getRight();
                pList = result.getLeft();
                
            } else {
                reason = "not enough data to update the PriceList [" + priceListId + "]";
            }
        }
        
        if (StringUtils.isNotBlank(reason)) {
        	
        	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
        }
        return ResponseEntity.status(HttpStatus.OK).body("Successfully updated!");
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<PriceList, String> updateThePriceList(PriceList pList) {
        String reason = null;
        try {
        	pList = plService.updatePriceList(pList);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();
        }        
        return new ImmutablePair<PriceList, String>(pList, reason);
    }
}
