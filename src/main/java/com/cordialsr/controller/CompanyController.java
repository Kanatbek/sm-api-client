package com.cordialsr.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.service.CompanyService;

@RepositoryRestController
@RequestMapping(value = "companies")
public class CompanyController {
	
	@Autowired
	CompanyService companyService;
	
	
	@RequestMapping(value = "/getCompanyContractNumber", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getCompanyContractNumber(
    		    @RequestParam(value="cid", required=true) Integer cid,
    		    @RequestParam(value="dateStart", required=true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateStart
    		) {
		try {
			Integer contractNumber = companyService.getCompanyContractNumber(cid, dateStart);
			return ResponseEntity.status(HttpStatus.OK).body(contractNumber);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }

}
