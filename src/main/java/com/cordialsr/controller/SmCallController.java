package com.cordialsr.controller;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.SmCall;
import com.cordialsr.service.SmCallService;

@RepositoryRestController
@RequestMapping(value = "smCallService")
public class SmCallController {
	
	@Autowired
	SmCallService smCallService;

	@RequestMapping(value = "/newCallService", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Object> saveSmService(@RequestBody SmCall newCall,
    											@RequestParam("verify") Integer verify,
    											@RequestParam("level") Integer level) {
        String reason = null;
        SmCall created = null;
        if (newCall != null) {
        	ImmutablePair<SmCall, String> result = saveCall(newCall, verify, level);
            reason = result.getRight();            
            created = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Service to be created is null").body(newCall);
        }
 
        if (reason == null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(created);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
	@Transactional(rollbackFor = Exception.class)
	private ImmutablePair<SmCall, String> saveCall(SmCall newCall, Integer verify, Integer level) {
		String reason = null;
		SmCall callSer = null;
		try {
			callSer = smCallService.newSmCall(newCall, verify, level);
		} catch (Exception ex) {
			ex.printStackTrace();
			reason = "Error";
        	reason += ex.getMessage();
		}
		return new ImmutablePair<SmCall, String>(callSer, reason);
	}
	
}
