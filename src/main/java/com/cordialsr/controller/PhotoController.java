package com.cordialsr.controller;

import javax.transaction.Transactional;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.Photo;
import com.cordialsr.general.exception.DaoException;
import com.cordialsr.service.PhotoService;

@RepositoryRestController
@RequestMapping(value = "photos")
public class PhotoController {
	
	@Autowired
	PhotoService photoService;
	
	@RequestMapping(value = "/newphoto", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> savePhoto(@RequestBody Photo photoToBeCreated) {
		Photo createdphoto;
        String reason = null;
        
        if (photoToBeCreated != null) {
        	ImmutablePair<Photo, String> result = saveTheCon(photoToBeCreated);
            reason = DaoException.justifyMessage(result.getRight());            
            createdphoto = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Photo to be created is null").body(photoToBeCreated);
        }
 
        if (createdphoto != null && createdphoto.getId() != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdphoto);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional
    private ImmutablePair<Photo, String> saveTheCon(Photo changedPhoto) {
        String reason = null;
        try {
        	photoService.savePhoto(changedPhoto);
        } catch(Exception ex) {
        	reason = ex.getMessage();        	
        }
        return new ImmutablePair<Photo, String>(changedPhoto, reason);
    }

}
