package com.cordialsr.controller;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.Dao.CrmLeadDao;
import com.cordialsr.domain.CrmLead;
import com.cordialsr.domain.Party;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.general.exception.DaoException;
import com.cordialsr.service.CrmLeadService;

@RepositoryRestController
@RequestMapping(value = "leads")
public class CrmLeadController {
	
	@Autowired
	CrmLeadService leadService;
	
	@RequestMapping(value = "/newLead", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> saveCharge(@RequestBody CrmLead newLead) {
		CrmLead createdLead;
        String reason = null;
        
        if (newLead != null) {
        	ImmutablePair<CrmLead, String> result = saveTheLead(newLead);
            reason = DaoException.justifyMessage(result.getRight());            
            createdLead = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Lead to be created is null").body(newLead);
        }
 
        if (GeneralUtil.isEmptyString(reason) && createdLead != null && createdLead.getId() != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdLead);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<CrmLead, String> saveTheLead(CrmLead newLead) {
        String reason = null;
        try {
        	leadService.saveLead(newLead);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();        	
        }        
        return new ImmutablePair<CrmLead, String>(newLead, reason);
    }

	// ***********************************************************************************************************
	
	
	@RequestMapping(value = "{leadId}",
            method = {RequestMethod.PATCH, RequestMethod.PUT},
            produces = MediaTypes.HAL_JSON_VALUE)	
	public @ResponseBody
    ResponseEntity<Object> updateLead(@PathVariable("leadId") Long leadId, @RequestBody CrmLead lead) {
        String reason = null;        
        if (leadId != null) {
            if (lead != null) { 
                ImmutablePair<CrmLead, String> result = updateTheLead(lead);
                reason = result.getRight();
                lead = result.getLeft();
            } else {
                reason = "not enough data to update the Lead [" + leadId + "]";
            }
        }
        if (StringUtils.isNotBlank(reason)) {
        	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
        }
        return ResponseEntity.status(HttpStatus.OK).body(lead);
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<CrmLead, String> updateTheLead(CrmLead lead) {
        String reason = null;
        try {
        	lead = leadService.updateLead(lead);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();
        }        
        return new ImmutablePair<CrmLead, String>(lead, reason);
    }
	
	//**************************************************************************
	
	@RequestMapping(value = "/newLeads", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> saveLeads(@RequestBody CrmLead[] newLeads) {
		CrmLead[] createdLead;
        String reason = null;
        
        if (newLeads != null) {
        	ImmutablePair<CrmLead[], String> result = saveTheLeads(newLeads);
            reason = DaoException.justifyMessage(result.getRight());            
            createdLead = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Lead to be created is null").body(newLeads);
        }
 
        if (GeneralUtil.isEmptyString(reason) && createdLead != null && createdLead.length > 0) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdLead);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<CrmLead[], String> saveTheLeads(CrmLead[] newLeads) {
        String reason = null;
        try {
        	leadService.saveLeads(newLeads);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();        	
        }        
        return new ImmutablePair<CrmLead[], String>(newLeads, reason);
    }

	// ***********************************************************************************************************
}
