package com.cordialsr.controller;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.FinCashflowStatement;
import com.cordialsr.domain.FinOperation;
import com.cordialsr.service.FinRefService;

@Controller
@RequestMapping(value = "finref")
public class FinRefController {

	@Autowired
	FinRefService finRefService;
	
	@RequestMapping(value = "/finoper", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> saveNewFinOp(@RequestBody FinOperation newFo) {
		FinOperation createdFo;
        String reason = null;
        
        if (newFo != null) {
        	ImmutablePair<FinOperation, String> result = saveTheCr(newFo);
            reason = result.getRight();            
            createdFo = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "FinOperation to be created is null").body(newFo);
        }
 
        if (createdFo != null && createdFo.getId() != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdFo);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<FinOperation, String> saveTheCr(FinOperation newFo) {
        String reason = null;
        FinOperation updFo = null;
        try {
        	updFo = finRefService.saveFinOper(newFo);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();        	
        }        
        return new ImmutablePair<FinOperation, String>(updFo, reason);
    }
	
	// *******************************************************************************************************
	
	@RequestMapping(value = "/finoper", method = RequestMethod.PUT, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> updateFinOp(@RequestBody FinOperation newFo) {
		FinOperation updatedFo;
        String reason = null;
        
        if (newFo != null) {
        	ImmutablePair<FinOperation, String> result = updateTheFo(newFo);
            reason = result.getRight();            
            updatedFo = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "FinOperation is null").body(newFo);
        }
 
        if (updatedFo != null && updatedFo.getId() != null) {
            return ResponseEntity.status(HttpStatus.OK).body(updatedFo);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<FinOperation, String> updateTheFo(FinOperation newFo) {
        String reason = null;
        FinOperation updFo = null;
        try {
        	updFo = finRefService.saveFinOper(newFo);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();        	
        }        
        return new ImmutablePair<FinOperation, String>(updFo, reason);
    }
	
	// *********************************************************************************************
	
	@RequestMapping(value = "/cfsta", method = RequestMethod.PUT, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody 
	public ResponseEntity<Object> updateFinCf(@RequestBody FinCashflowStatement newCf) {
		FinCashflowStatement updatedCf;
        String reason = null;
        
        if (newCf != null) {
        	ImmutablePair<FinCashflowStatement, String> result = updateTheCf(newCf);
            reason = result.getRight();            
            updatedCf = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "CashFlow Statement is null").body(newCf);
        }
 
        if (updatedCf != null && updatedCf.getId() != null) {
            return ResponseEntity.status(HttpStatus.OK).body(updatedCf);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional(rollbackFor = Exception.class)
    private ImmutablePair<FinCashflowStatement, String> updateTheCf(FinCashflowStatement newCf) {
        String reason = null;
        FinCashflowStatement updatedCf = null;
        try {
        	updatedCf = finRefService.saveCf(newCf);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();
        }        
        return new ImmutablePair<FinCashflowStatement, String>(updatedCf, reason);
    }
}
