package com.cordialsr.controller;


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.SmService;
import com.cordialsr.domain.reducer.ContractReducer;
import com.cordialsr.domain.reducer.EmployeeReducer;
import com.cordialsr.report.service.ServiceReportService;
import com.cordialsr.service.SmServService;

@RepositoryRestController
@RequestMapping(value = "smServices")
public class SmServiceController {
	
	@Autowired
	SmServService smSerService;
	
	@RequestMapping(value = "/newService", method = RequestMethod.POST,
            produces = MediaTypes.HAL_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Object> saveSmService(@RequestBody SmService service) {
        String reason = null;
        SmService created = null;
        if (service != null) {
        	ImmutablePair<SmService, String> result = saveService(service);
            reason = result.getRight();            
            created = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Service to be created is null").body(service);
        }
 
        if (reason == null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(created);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	

	////////////////////////////////////////////////////////////////////////////////////////
	
	
	@RequestMapping(value = "/newShortService", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Object> saveSmShortService(
    		@RequestParam("contractNumber") String contractNumber,
    		@RequestParam("sdate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date sdate,
    		@RequestParam("stime") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date sTime,
    		@RequestParam("itime") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date iTime,
    		@RequestParam("fno1") Integer fno1,
    		@RequestParam("fno2") Integer fno2,
    		@RequestParam("fno3") Integer fno3,
    		@RequestParam("fno4") Integer fno4,
    		@RequestParam("fno5") Integer fno5,
    		@RequestParam("type") String type, 
    		@RequestParam("info") String info,
    		@RequestParam("longitude") BigDecimal longitude,
    		@RequestParam("latitude") BigDecimal latitude,
    		@RequestParam("username") String username) {
        String reason = null;
        String created = null;
        if (contractNumber != null) {
        	ImmutablePair<String, String> result = convertService(contractNumber, sdate, sTime, iTime, fno1,
        															fno2, fno3, fno4, fno5, type, info, username, longitude, latitude);
            reason = result.getRight();            
            created = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Service to be created is null").body(convertService(contractNumber, sdate, sTime, iTime, fno1,
        															fno2, fno3, fno4, fno5, type, info, username, longitude, latitude));
        }
 
        if (reason == null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(created);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	
	////////////////////////////////////////////////////////////////////////////////////////
	
	
	@RequestMapping(value = "{id}",
            method = {RequestMethod.PATCH, RequestMethod.PUT},
            produces = MediaTypes.HAL_JSON_VALUE)	
	public @ResponseBody
    ResponseEntity<Object> updateServiceChanges(@PathVariable("id") Long smId, @RequestBody SmService service) {
        
		String reason = null;
        SmService created = null;
        if (smId == null) {
        	return ResponseEntity.status(HttpStatus.BAD_REQUEST).header("reason", "ID is null").body(service);
        }
        if (service != null) {
        	ImmutablePair<SmService, String> result = updateService(smId, service);
            reason = result.getRight();            
            created = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Service to be created is null").body(service);
        }
 
        if (reason == null) {
        	created.setCareman(EmployeeReducer.reduceEmployee(created.getCareman(), 1));
        	created.setContract(ContractReducer.reduceMed(created.getContract()));
            return ResponseEntity.status(HttpStatus.CREATED).body(created);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@RequestMapping(value = "/cancelService",
            method = {RequestMethod.PATCH, RequestMethod.PUT},
            produces = MediaTypes.HAL_JSON_VALUE)	
	public @ResponseBody
    ResponseEntity<Object> cancelSmService(@RequestBody SmService service) {
        
		String reason = null;
        SmService created = null;
        if (service == null) {
        	return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Service to be created is null").body(service);
        } else if (service.getId() == null) {
        	return ResponseEntity.status(HttpStatus.BAD_REQUEST).header("reason", "ID is null").body(service);
        } else {
        	ImmutablePair<SmService, String> result = smServiceCancel(service);
            reason = result.getRight();            
            created = result.getLeft();
        }
        if (reason == null) {
        	created.setCareman(EmployeeReducer.reduceEmployee(created.getCareman(), 1));
        	created.setContract(ContractReducer.reduceMed(created.getContract()));
            return ResponseEntity.status(HttpStatus.OK).body(created);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	@Transactional(rollbackFor = Exception.class)
	private ImmutablePair<SmService, String> smServiceCancel(SmService service) {
		String reason = null;
		try { 
			service = smSerService.cancelNewService(service);
		} catch (Exception ex) {
			ex.printStackTrace();
			reason = "Error ";
        	reason += ex.getMessage();
		}
		return new ImmutablePair<SmService, String>(service, reason);
	}
	
	
	

	@Transactional(rollbackFor = Exception.class)
	private ImmutablePair<SmService, String> updateService(Long smId, SmService service) {
		String reason = null;
		try { 
			service = smSerService.updateNewService(smId, service);
		} catch (Exception ex) {
			ex.printStackTrace();
			reason = "Error ";
        	reason += ex.getMessage();
		}
		return new ImmutablePair<SmService, String>(service, reason);
	}

	
	@Transactional(rollbackFor = Exception.class)
	private ImmutablePair<String, String> convertService(String contractNumber, Date sdate, Date sTime, Date iTime, Integer fno1,
															Integer fno2, Integer fno3, Integer fno4, Integer fno5,
															String type, String info, String username, BigDecimal longitude, BigDecimal latitude) {
		String service = null;
		String reason = null;
		try { 
			String dService = smSerService.convertToService(contractNumber, sdate, sTime, iTime, fno1, fno2, fno3, fno4, fno5
															, type, info, username, longitude, latitude);
			
			if (dService != null && dService.length() > 0) {
				service = dService;
			} else {
				service = "Success";
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			reason = "Error ";
        	reason += ex.getMessage();
		}
		return new ImmutablePair<String, String>(service, reason);
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	private ImmutablePair<SmService, String> saveService(SmService service) {
		String reason = null;
		try { 
			service = smSerService.saveNewService(service);
		} catch (Exception ex) {
			ex.printStackTrace();
			reason = "Error ";
        	reason += ex.getMessage();
		}
		return new ImmutablePair<SmService, String>(service, reason);
	}
	
	@Autowired
	ServiceReportService servRepService; 
	
	@Transactional(rollbackFor = Exception.class)
	private ImmutablePair<List<SmService>, String> newReportService(Integer cid, Date fromDate, Date toDate) {
		String reason = null;
		List<SmService> service = null;
		try { 
			service = servRepService.reportNewService(cid, fromDate, toDate);
		} catch (Exception ex) {
			ex.printStackTrace();
			reason = "Error ";
        	reason += ex.getMessage();
		}
		return new ImmutablePair<List<SmService>, String>(service, reason);
	}
	
	///////////////////// I do not what is it?!
//	@RequestMapping(value = "/smzflistmon",
//			params = {"brid", "data", "mode"},
//			method = RequestMethod.GET)
//    public ResponseEntity<Iterable<SmFilterChange>> smzflist(
//    		@RequestParam(value = "brid") Long brId,
//    		@RequestParam(value = "data") Date data,
//    		@RequestParam(value = "mode") int mode) {
//				
//		return new ResponseEntity<>(smfcService.findAll(), HttpStatus.OK);
//    }
//	
	
}
