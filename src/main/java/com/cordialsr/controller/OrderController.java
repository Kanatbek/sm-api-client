package com.cordialsr.controller;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.OrderHeader;
import com.cordialsr.service.OrderService;

@RepositoryRestController
@RequestMapping(value = "orders")
public class OrderController {

	@Autowired
	OrderService orderService;
	
	@RequestMapping(value = "/neworder", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    ResponseEntity<Object> newStockIn(@RequestBody OrderHeader ordHead) throws Exception {
		String reason = null;
		Boolean bool = false;
		if (ordHead != null) {
			ImmutablePair<Boolean, String> result = saveNewOrder(ordHead);
			bool = result.getLeft();
			reason = result.getRight();
		}
		else {
			reason = "Order is null";
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
		}
		if (reason != null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
		}
		if (bool)
			return ResponseEntity.status(HttpStatus.CREATED).body(bool);
		else return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Unsuccesfull!");
	}

	
	///////////// Method of save order ..........
	@Transactional(rollbackFor = Exception.class)
	private ImmutablePair<Boolean, String> saveNewOrder(OrderHeader ordHead) {
		String result = null;
		Boolean bool = false;
		try {
			bool = orderService.saveNewOrders(ordHead);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	result = ex.getMessage();        	
        }
		return new ImmutablePair<Boolean, String>(bool, result);
	}
	
}
