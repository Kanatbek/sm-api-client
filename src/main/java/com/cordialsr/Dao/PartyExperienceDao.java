package com.cordialsr.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.PartyExperience;

@RepositoryRestResource(collectionResourceRel = "party_experience", path = "party_experience")
public interface PartyExperienceDao extends CrudRepository<PartyExperience, Long> {

}
