package com.cordialsr.Dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;

import com.cordialsr.Dao.custrepo.ConAwardRepo;
import com.cordialsr.domain.ContractAwardSchedule;

@RepositoryRestResource(collectionResourceRel = "conawsch", path = "conawsch")
public interface ContractAwardScheduleDao extends ConAwardRepo, CrudRepository<ContractAwardSchedule, Long> {
	
	@Query("from " + 
			"ContractAwardSchedule a " + 
			"where " +
			"a.contract.company.id = ?1 " +
			"and a.contract.branch.id = ?2 " +
			"and a.awardType.name = ?3 " + 
			"and YEAR(a.dateSchedule) = YEAR(?4) " +
			"and MONTH(a.dateSchedule) = MONTH(?4) " +
			"and a.contract.storno = false " + 
			"and coalesce(a.accrued, 0) < a.summ")
	List<ContractAwardSchedule> getAccruableAwardList(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("awt") String awt,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("de") Date de
		);
	
	@Query("from " + 
			"ContractAwardSchedule a " + 
			"where " +
			"a.contract.company.id = ?1 " +
			"and a.contract.branch.id = ?2 " +
			"and a.awardType.name = ?3 " + 
			"and a.position.id = ?4 " +
			"and YEAR(a.dateSchedule) = YEAR(?5) " +
			"and MONTH(a.dateSchedule) = MONTH(?5) " +
			"and a.contract.storno = false " + 
			"and coalesce(a.accrued, 0) < a.summ")
	List<ContractAwardSchedule> getAccruableAwardListByPosBr(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("awt") String awt,
			@Param("pos") Integer pos,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("de") Date de
		);
	
	@Query("from " + 
			"ContractAwardSchedule a " + 
			"where " +
			"a.contract.company.id = ?1 " +
			"and a.awardType.name = ?2 " + 
			"and a.position.id = ?3 " +
			"and YEAR(a.dateSchedule) = YEAR(?4) " +
			"and MONTH(a.dateSchedule) = MONTH(?4) " +
			"and a.contract.storno = false " + 
			"and coalesce(a.accrued, 0) < a.summ")
	List<ContractAwardSchedule> getAccruableAwardListByPos(
			@Param("cid") Integer cid,
			@Param("awt") String awt,
			@Param("pos") Integer pos,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("de") Date de
		);
	
	@Query("from " + 
			"ContractAwardSchedule a " + 
			"where " +
			"a.contract.id = ?1 " +
			"and a.position.id = ?2 ")
	List<ContractAwardSchedule> getCawByContract(
			@Param("cid") Long cid,
			@Param("position") Integer position);
	
}
