package com.cordialsr.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.projection.MenuProjection;
import com.cordialsr.domain.security.Menu;

@RepositoryRestResource(collectionResourceRel = "menu", path = "menu",
						excerptProjection = MenuProjection.class)
public interface MenuDao extends CrudRepository<Menu, Integer>{

	@Query("from Menu m where m.level = 1 order by m.id asc")
	Iterable<Menu> getAllTree();
	
	@Query("from Menu m where m.level = ?1 order by m.id asc")
	List<Menu> getAllByLevel(Integer level);
	
	@Query("from Menu m where m.parentMenu.id = ?1 order by m.id asc")
	List<Menu> getAllByParentId(Integer parentId);
	
}
