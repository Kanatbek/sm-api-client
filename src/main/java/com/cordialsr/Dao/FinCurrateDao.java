package com.cordialsr.Dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;

import com.cordialsr.domain.FinCurrate;
import com.cordialsr.domain.projection.FinCurrateProjection;

@RepositoryRestResource(collectionResourceRel = "currate", path = "currate",
					excerptProjection = FinCurrateProjection.class)
public interface FinCurrateDao extends JpaRepository<FinCurrate, Integer>{

	@Query("from FinCurrate c where c.company.id = ?1")
	List<FinCurrate> getAllRatesByCom(@Param("cid") Integer cid);
	
	@Query("from FinCurrate c where "
			+ "c.company.id = ?1 "
			+ "and (c.mainCurrency.currency = ?2)")
	List<FinCurrate> getAllRatesByComMc(
			@Param("cid") Integer cid,
			@Param("mc") String mc);
	
	@Query("from FinCurrate c where "
			+ "c.company.id = ?1 "
			+ "and (c.secCurrency.currency = ?2)")
	List<FinCurrate> getAllRatesByComSc(
			@Param("cid") Integer cid,
			@Param("sc") String sc);
	
	@Query("from FinCurrate c where "
			+ "c.company.id = ?1 "
			+ "and (c.mainCurrency.currency = ?2 and c.secCurrency.currency = ?3)")
	List<FinCurrate> getAllRatesByComMcSc(
			@Param("cid") Integer cid,
			@Param("mc") String mc,
			@Param("sc") String sc);
	
	@Query(value = "Select * from Fin_Currate c where c.company = ?1"
			+ " and c.data <= CURRENT_DATE"
			+ " and c.main_currency = ?2"
			+ " and c.sec_currency = ?3"
			+ " and c.scope_type = 'EXT'"
			+ " ORDER BY c.data DESC LIMIT 1",
			nativeQuery = true)
	FinCurrate extRate(@Param("comId") Integer comId, 
			@Param("mc") String mc,
			@Param("sc") String sc);
	
	@Query(value = "Select * from Fin_Currate c where c.company = ?1"
			+ " and c.data <= CURRENT_DATE"
			+ " and c.main_currency = ?2"
			+ " and c.sec_currency = ?3"
			+ " and c.data <= ?4"
			+ " and c.scope_type = 'EXT'"
			+ " ORDER BY c.data DESC, c.id DESC LIMIT 1",
			nativeQuery = true)
	FinCurrate extRateByDate(@Param("comId") Integer comId, 
			@Param("mc") String mc,
			@Param("sc") String sc,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("dt") Date dt);
	
	@Query(value = "Select * from Fin_Currate c where c.company = ?1"
			+ " and c.data <= CURRENT_DATE"
			+ " and c.main_currency = ?2"
			+ " and c.sec_currency = ?3"
			+ " and c.data <= ?4"
			+ " and c.scope_type = 'INT'"
			+ " ORDER BY c.data DESC, c.id DESC LIMIT 1",
			nativeQuery = true)
	FinCurrate intRateByDate(@Param("comId") Integer comId, 
			@Param("mc") String mc,
			@Param("sc") String sc,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("dt") Date dt);
	
	@Query(value = "Select * from Fin_Currate c where c.company = ?1"
			+ " and c.data <= CURRENT_DATE"
			+ " and c.main_currency = ?2"
			+ " and c.sec_currency = ?3"
			+ " and c.scope_type = 'INT'"
			+ " ORDER BY c.data DESC LIMIT 1",
			nativeQuery = true)
	FinCurrate intRate(@Param("comId") Integer comId, 
			@Param("mc") String mc,
			@Param("sc") String sc);
	
	@Query("from FinCurrate c where c.company.id = ?1"
			+ " and c.country.id = ?2"
			+ " and c.rdate <= CURRENT_DATE"
			+ " and c.scopeType.typeCode = 'EXT'")
	List<FinCurrate> getExtRate(@Param("comId") Integer comId, 
			@Param("cntry") Integer country);
	
	@Query(value = "Select * from Fin_Currate c where c.company = ?1"
			+ " and c.data <= CURRENT_DATE"
			+ " and c.scope_type = ?2"
			+ " group by c.main_currency, c.sec_currency"
			+ " ORDER BY c.data DESC",
			nativeQuery = true)
	List<FinCurrate> allActualRatesByComSt(@Param("cid") Integer cid, @Param("st") String st);
	
	@Query(value = "Select * from Fin_Currate c where"
			+ " c.data = CURRENT_DATE"
			+ " and c.main_currency = ?2"
			+ " and c.sec_currency = ?1"
			+ " ORDER BY c.data DESC, c.id DESC LIMIT 1",
			nativeQuery = true)
	List<FinCurrate> getCurrentDaysRate( 
			@Param("sc") String sc,
			@Param("mc") String mc);
	
	@Query(value = "Select * from Fin_Currate c "
			+ "having c.id = (select r.id from fin_currate r  " + 
							"	where r.company = c.company  " + 
							"   and r.scope_type = c.scope_type " + 
							"   and r.main_currency = c.main_currency " + 
							"   and r.sec_currency = c.sec_currency " + 
							"   and r.data <= CURRENT_DATE " + 
							"   order by r.data desc limit 1)",
			nativeQuery = true)
	List<FinCurrate> allActualRates();
	
	
}
