package com.cordialsr.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.CrmDemo;
import com.cordialsr.domain.CrmLead;
import com.cordialsr.domain.Oblast;

@RepositoryRestResource(collectionResourceRel = "lead", path = "lead")
public interface CrmLeadDao extends JpaRepository<CrmLead, Long>{

	@Query(value = "Select * from crm_lead",
			nativeQuery=true)
			Iterable<CrmLead>findAllLeads();
	
	@Query("from CrmLead c where "
			+ " c.id = ?1")
			CrmLead getLeadById(@Param("leadId") Long leadId );
	
	@Query("from CrmLead c where"
			+ " c.company.id = ?1 ")
			Iterable<CrmLead> getByCompany(@Param("cid") Integer cid);
	
	@Query("from CrmLead c where"
			+ " c.company.id = ?1"
			+ " and c.branch.id = ?2 ")
			Iterable<CrmLead> getByCompanyBranch(@Param("cid") Integer cid,
									   			 @Param("bid") Integer bid);
	
	@Query("from CrmLead c where "
			+ " c.branch.id = ?1 ")
			Iterable<CrmLead> getByBranch(@Param("bid") Integer bid);
	
	@Query("from CrmLead c where"
			+ " c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and c.dealer.id = ?3 ")
			Iterable<CrmLead> getByCompanyBranchDealer(@Param("cid") Integer cid,
									   			 @Param("bid") Integer bid,
									   			 @Param("dealerId") Long dealerId);
	
	@Query("from CrmLead c where"
			+ " c.company.id = ?1"
			+ " and c.dealer.id = ?2 ")
			Iterable<CrmLead> getByCompanyDealer(@Param("cid") Integer cid,
									   			 @Param("dealerId") Long dealerId);
	
	@Query("from CrmLead c where"
			+ " c.dealer.id = ?1 ")
			Iterable<CrmLead> getByDealer(@Param("dealerId") Long dealerId);
	
	@Query("from CrmLead c where"
			+ " c.branch.id = ?1"
			+ " and c.dealer.id = ?2 ")
			Iterable<CrmLead> getByBranchDealer(@Param("bid") Integer bid,
													   @Param("dealerId") Long dealerId);
}
