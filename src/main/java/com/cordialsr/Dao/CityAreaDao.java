package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.CityArea;

@RepositoryRestResource(collectionResourceRel = "cityArea", path = "cityArea")
public interface CityAreaDao extends CrudRepository<CityArea, Integer> {


	@Query("from CityArea r where "
			+ "r.city.id = ?1")
			Iterable<CityArea>findAllByCityArea(@Param("cityArea") Integer cityArea);
}
