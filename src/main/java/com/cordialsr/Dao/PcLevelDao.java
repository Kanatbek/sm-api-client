package com.cordialsr.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.PcLevel;

@RepositoryRestResource(collectionResourceRel = "pcLevel", path = "pcLevel")
public interface PcLevelDao extends CrudRepository<PcLevel, Integer>{
	@Query("from PcLevel c where "
			+ "c.id > 0")
			List<PcLevel> findAll();
}
