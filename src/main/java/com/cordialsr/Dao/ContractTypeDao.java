package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.ContractType;
import com.cordialsr.domain.projection.ContractTypeProjection;


@RepositoryRestResource(collectionResourceRel = "contractType", path = "contractType", 
						excerptProjection=ContractTypeProjection.class)
public interface ContractTypeDao extends CrudRepository<ContractType, Integer> {
	@Query("from ContractType c where "
			+ "c.id = ?1")
	ContractType findById(@Param("id") Integer contractId);
	
	@Query("from ContractType c where "
			+ "c.company.id = ?1 "
			+ "and c.inventory.id = ?2")
	ContractType findByComBrInv(Integer cid, Integer inv);
	
	@Query("from ContractType c where "
			+ "c.company.id = ?1")
	Iterable<ContractType> allByCompany(@Param("cid") Integer cd);
}
