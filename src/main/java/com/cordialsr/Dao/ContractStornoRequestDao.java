package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.ContractStornoRequest;
import com.cordialsr.domain.projection.ContractStornoRequestProjection;

@RepositoryRestResource(collectionResourceRel = "contractStornoRequest", path = "contractStornoRequest", 
					excerptProjection = ContractStornoRequestProjection.class)
public interface ContractStornoRequestDao extends CrudRepository<ContractStornoRequest, Long>{

	@Query("from ContractStornoRequest c "
			+ "where c.company.id = ?1 ")
	Iterable<ContractStornoRequest> findByCid(
			@Param("cid") Integer cid);
	
	@Query("from ContractStornoRequest c "
			+ "where c.company.id = ?1 "
			+ "and c.branch.id = ?2")
	Iterable<ContractStornoRequest> findByCidBid(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid);
	
	@Query("from ContractStornoRequest c "
			+ "where c.id = ?1" )
	ContractStornoRequest findById(
			@Param("reqId") Long reqId);
	
	@Query(value = "select * from contract_storno_request c "
			+ "where c.contract_id = ?1 "
			+ "and c.status = 2 "
			+ "limit 1",
			nativeQuery = true)
	ContractStornoRequest findByContractId(
			@Param("conId") Long conId);
	
	@Query("from ContractStornoRequest c "
			+ "where c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "and c.status = ?3 ")
	Iterable<ContractStornoRequest> findByCidBidStatus(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("status") Integer status);
	
	@Query("from ContractStornoRequest c "
			+ "where c.company.id = ?1 "
			+ "and c.status = ?2")
	Iterable<ContractStornoRequest> findByCidStatus(
			@Param("cid") Integer cid,
			@Param("status") Integer status);
	
	
	
	
	
	
}
