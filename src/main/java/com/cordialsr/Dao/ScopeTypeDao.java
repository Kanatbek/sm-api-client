package com.cordialsr.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.ScopeType;
import com.cordialsr.domain.projection.ScopeTypeProjection;

@RepositoryRestResource(collectionResourceRel = "scopeType", path = "scopeType",
						excerptProjection = ScopeTypeProjection.class)
public interface ScopeTypeDao extends CrudRepository<ScopeType, String>{
	
}
