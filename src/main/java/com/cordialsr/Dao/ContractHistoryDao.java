package com.cordialsr.Dao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.ContractHistory;
import com.cordialsr.domain.projection.ContractHistoryProjection;

@RepositoryRestResource(collectionResourceRel = "contract_history", path = "contract_history",
						excerptProjection = ContractHistoryProjection.class)
public interface ContractHistoryDao extends CrudRepository<ContractHistory, Integer>{

}
