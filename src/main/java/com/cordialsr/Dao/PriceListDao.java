package com.cordialsr.Dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.City;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractType;
import com.cordialsr.domain.Country;
import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.PriceList;
import com.cordialsr.domain.Region;
import com.cordialsr.domain.Scope;
import com.cordialsr.domain.projection.PriceListProjection;

@RepositoryRestResource(collectionResourceRel = "priceList", path = "priceList",
						excerptProjection = PriceListProjection.class)
public interface PriceListDao extends JpaRepository<PriceList, Integer> {
	
	@Query("from PriceList p where "
	+ "p.id = ?1")
	PriceList findById(@Param("prId") Integer prId);
	
	@Query(value = "Select * from price_list c "
			+ "where c.company = :cid "
			+ "and c.active = true ",
			nativeQuery = true)
	Iterable<PriceList> getPriceByCompany(
			@Param("cid") Integer cid);
	
	@Query(value = "Select * from price_list c "
			+ "where c.company = :cid "
			+ "and c.inventory_id = :inv "
			+ "and c.active = true ",
			nativeQuery = true)
	Iterable<PriceList> getPrByCidInvId(
			@Param("cid") Integer cid,
			@Param("inv") Integer inv);

	@Query(value = "Select * from price_list c "
			+ "where c.company = :cid "
			+ "and c.inventory_id = :inv "
			+ "and c.is_rent = :isrent "
			+ "and c.is_sell = :issell "
			+ "and c.active = true ",
			nativeQuery = true)
	Iterable<PriceList> getPrByRent(
			@Param("cid") Integer cid,
			@Param("inv") Integer inv,
			@Param("isrent") Boolean isrent,
			@Param("issell") Boolean issell);
	
	@Query("from PriceList p where p.inventory.id = ?1 "
			+ "and p.isRent = ?2 "
			+ "and (p.forYur = ?3 "
			+ "or p.forFiz = ?4)")
	Iterable<PriceList> findByContractTypeId(@Param("ctype") Integer ctype, 
											 @Param("isrent") Boolean isrent,
											 @Param("isyur") Boolean isyur,
											 @Param("isfiz") Boolean isfiz);
	
	@Query(value = "Select * from price_list c "
			+ "where c.company = :cid "
			+ "and c.inventory_id = :inv "
			+ "and c.is_rent = :isrent "
			+ "and c.for_yur = :forYur "
			+ "and c.for_fiz = :forFiz "
			+ "and c.active = true ",
			nativeQuery = true)
	Iterable<PriceList> getPriceLists(
			@Param("cid") Integer cid, 
			@Param("inv") Integer inv,
			@Param("isrent") Boolean isrent,
			@Param("forYur") Boolean forYur,
			@Param("forFiz") Boolean forFiz);
	
	@Query("from PriceList c where "
			+ "c.company.id = ?1 "
			+ "and "
			+ "(c.scope.name like %?2% "
			+ "or c.company.name like %?2% "
			+ "or c.inventory.name like %?2% "
			+ "or c.inventory.code like %?2% "
			+ ")"
			+ "and c.active = true")
	Iterable<PriceList> filterByComp( 
			@Param("cid") Integer cid,
			@Param("filter") String filter);	
	
	@Query("from PriceList c where "
			+ "c.company.id = ?1 "
			+ "and c.inventory.id = ?2 "
			+ "and "
			+ "(c.scope.name like %?3% "
			+ "or c.company.name like %?3% "
			+ "or c.inventory.name like %?3% "
			+ "or c.inventory.model like %?3% "
			+ "or c.inventory.code like %?3% "
			+ ")"
			+ "and c.active = true")
	Iterable<PriceList> filterByInv( 
			@Param("cid") Integer cid,
			@Param("inv") Integer inv,
			@Param("filter") String filter);	
	
	// ***************************************************************
	
	@Query(value = "Select * from price_list p " + 
			"where " + 
			"p.company = ?1 " + 
			"and p.inventory_id = ?2 " + 
			"and p.active = 1 " + 
			"and p.is_rent = 1 " + 
			"and p.for_yur = 1 " + 
			"and ( " +
			"(p.scope = 'COMPANY') " + 
			"or (p.scope = 'BRANCH' and p.branch = ?3) " +  
			"or (p.scope = 'CITY' and p.city = ?4) " + 
			"or (p.scope = 'COUNTRY' and p.country = ?5) " + 
			"or (p.scope = 'REGION' and p.region = ?6) " + 
			")" + 
			"and p.data <= ?7 " + 
			"order by p.branch desc, p.city desc, p.country desc, p.region desc, p.data desc;", nativeQuery = true)
	List<PriceList> getRentPlForYurByScPriority(
			Integer cid,
			Integer inv, 
			Integer bid, 
			Integer city,
			Integer cnid,
			Integer regid,
			Date dt);
	
	@Query(value = "Select * from price_list p " + 
			"where " + 
			"p.company = ?1 " + 
			"and p.inventory_id = ?2 " + 
			"and p.active = 1 " + 
			"and p.is_rent = 1 " + 
			"and p.for_fiz = 1 " + 
			"and ( " +
			"(p.scope = 'COMPANY') " + 
			"or (p.scope = 'BRANCH' and p.branch = ?3) " +  
			"or (p.scope = 'CITY' and p.city = ?4) " + 
			"or (p.scope = 'COUNTRY' and p.country = ?5) " + 
			"or (p.scope = 'REGION' and p.region = ?6) " + 
			")" + 
			"and p.data <= ?7 " + 
			"order by p.branch desc, p.city desc, p.country desc, p.region desc, p.data desc;", nativeQuery = true)
	List<PriceList> getRentPlForFizByScPriority(
			Integer cid,
			Integer inv, 
			Integer bid, 
			Integer city,
			Integer cnid,
			Integer regid,
			Date dt);
	
	@Query(value = "Select * from price_list p " + 
			"where " + 
			"p.company = ?1 " + 
			"and p.inventory_id = ?2 " + 
			"and p.active = 1 " + 
			"and p.is_rent = 0 " + 
			"and p.for_yur = 1 " + 
			"and ( " +
			"(p.scope = 'COMPANY') " + 
			"or (p.scope = 'BRANCH' and p.branch = ?3) " +  
			"or (p.scope = 'CITY' and p.city = ?4) " + 
			"or (p.scope = 'COUNTRY' and p.country = ?5) " + 
			"or (p.scope = 'REGION' and p.region = ?6) " + 
			")" + 
			"and p.data <= ?7 " + 
			"order by p.branch desc, p.city desc, p.country desc, p.region desc, p.data desc;", nativeQuery = true)
	List<PriceList> getSalesPlForYurByScPriority(
			Integer cid,
			Integer inv, 
			Integer bid, 
			Integer city,
			Integer cnid,
			Integer regid,
			Date dt);
	
	@Query(value = "Select * from price_list p " + 
			"where " + 
			"p.company = ?1 " + 
			"and p.inventory_id = ?2 " + 
			"and p.active = 1 " + 
			"and p.is_rent = 0 " + 
			"and p.for_fiz = 1 " + 
			"and ( " +
			"(p.scope = 'COMPANY') " + 
			"or (p.scope = 'BRANCH' and p.branch = ?3) " + 
			"or (p.scope = 'CITY' and p.city = ?4) " + 
			"or (p.scope = 'COUNTRY' and p.country = ?5) " + 
			"or (p.scope = 'REGION' and p.region = ?6) " + 
			")" + 
			"and p.data <= ?7 " + 
			"order by p.branch desc, p.city desc, p.country desc, p.region desc, p.data desc;", nativeQuery = true)
	List<PriceList> getSalesPlForFizByScPriority(
			Integer cid,
			Integer inv, 
			Integer bid, 
			Integer city,
			Integer cnid,
			Integer regid,
			Date dt);
}
