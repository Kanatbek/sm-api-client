package com.cordialsr.Dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.SmVisit;

@RepositoryRestResource(collectionResourceRel = "smVisit", path = "smvisit")
public interface SmVisitDao extends PagingAndSortingRepository<SmVisit, Long> {
	
}
