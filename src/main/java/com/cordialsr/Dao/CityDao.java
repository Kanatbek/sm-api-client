package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.City;

@RepositoryRestResource(collectionResourceRel = "city", path = "city")
public interface CityDao extends CrudRepository<City, Integer> {
	
	@Query("from City c where "
			+ "c.oblast.id = ?1")
			Iterable<City> findAllByOblast(@Param("oblast") Integer oblast);
	
	@Query("from City c where "
			+ "c.oblast.country.id = ?1 "
			+ "order by c.name")
			Iterable<City> findAllByCountry(@Param("country") Integer country);
}
