package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Component;

import com.cordialsr.domain.Company;
import com.cordialsr.domain.projection.CompanyInlineProjection;

@RepositoryRestResource(collectionResourceRel = "company", path = "company",
						excerptProjection = CompanyInlineProjection.class)
@Component("CompanyDao")
public interface CompanyDao extends CrudRepository<Company, Integer> {
	@Query(value = "Select * from Company c where c.id = ?1"
			+ " limit 1", 
			nativeQuery=true)
	Company findcid(@Param("cid") Integer cid);
	
}
