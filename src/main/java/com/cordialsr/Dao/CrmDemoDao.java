package com.cordialsr.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.CrmDemo;
import com.cordialsr.domain.CrmLead;

@RepositoryRestResource(collectionResourceRel = "demo", path = "demo")
public interface CrmDemoDao extends CrudRepository<CrmDemo, Integer> {
	

	@Query("select count(c) from CrmDemo c where "
			+ "c.branch = ?1 "
			+ "and EXTRACT(DAY FROM c.demoDate) = ?2 "
			+ "and EXTRACT(MONTH FROM c.demoDate) = ?3 "
			+ "and EXTRACT(YEAR FROM c.demoDate) = ?4")
			int findByDay(@Param("br") Branch br,
						  @Param("day") Integer day,
						  @Param("month") Integer month,
						  @Param("year") Integer year);
	
	@Query("select count(c) from CrmDemo c where "
			+ "c.branch = ?1 "
			+ "and EXTRACT(MONTH FROM c.demoDate) = ?2 "
			+ "and EXTRACT(YEAR FROM c.demoDate) = ?3 ")
			int findByMonth(@Param("br") Branch br,
							@Param("month") Integer month,
							@Param("year") Integer year);
	
	@Query(value = "Select * from crm_demo",
			nativeQuery=true)
			Iterable<CrmDemo>findAllDemos();
	
	@Query("from CrmDemo c where "
			+ " c.id = ?1")
			CrmDemo getDemoById(@Param("demoId") Integer demoId );
	
	@Query("from CrmDemo c where"
			+ " c.company.id = ?1 ")
			Iterable<CrmDemo> getByCompany(@Param("cid") Integer cid);
	
	@Query("from CrmDemo c where"
			+ " c.company.id = ?1"
			+ " and c.branch.id = ?2 ")
			Iterable<CrmDemo> getByCompanyBranch(@Param("cid") Integer cid,
									   			 @Param("bid") Integer bid);
	
	@Query("from CrmDemo c where"
			+ " c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and c.dealer.id = ?3 ")
			Iterable<CrmDemo> getByCompanyBranchDealer(@Param("cid") Integer cid,
									   			 @Param("bid") Integer bid,
									   			 @Param("dealerId") Long dealerId);
	
	@Query("from CrmDemo c where"
			+ " c.company.id = ?1"
			+ " and c.dealer.id = ?2 ")
			Iterable<CrmDemo> getByCompanyDealer(@Param("cid") Integer cid,
									   			 @Param("dealerId") Long dealerId);
	
	@Query("from CrmDemo c where"
			+ " c.dealer.id = ?1 ")
			Iterable<CrmDemo> getByDealer(@Param("dealerId") Long dealerId);
	
	@Query("from CrmDemo c where"
			+ " c.branch.id = ?1"
			+ " and c.dealer.id = ?2 ")
			Iterable<CrmDemo> getByBranchDealer(@Param("bid") Integer bid,
													   @Param("dealerId") Long dealerId);

									   			 
	@Query("from CrmDemo c where "
			+ " c.branch.id = ?1 ")
			Iterable<CrmDemo> getByBranch(@Param("bid") Integer bid);

}
