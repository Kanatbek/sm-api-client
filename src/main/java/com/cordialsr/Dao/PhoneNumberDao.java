package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.PhoneNumber;

@RepositoryRestResource(collectionResourceRel = "phone", path = "phone")
public interface PhoneNumberDao extends CrudRepository<PhoneNumber, Long> {
	@Query("from PhoneNumber c where "
			+ "c.id = ?1")
	PhoneNumber findById(@Param("id") Long phoneId);
	
}
