package com.cordialsr.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.Country;
import com.cordialsr.domain.projection.CountryProjection;

@RepositoryRestResource(collectionResourceRel = "country", path = "country",
			excerptProjection = CountryProjection.class)
public interface CountryDao extends CrudRepository<Country, Integer>{
	
}
