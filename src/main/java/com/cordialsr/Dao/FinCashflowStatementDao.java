package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.repository.query.Param;
import com.cordialsr.domain.FinCashflowStatement;
import com.cordialsr.domain.projection.FinCfStatProjection;

@RepositoryRestResource(collectionResourceRel = "fincfsta", path = "fincfsta", 
						excerptProjection = FinCfStatProjection.class)
public interface FinCashflowStatementDao extends CrudRepository<FinCashflowStatement, Integer>{
	
//	@Query("from FinCashflowStatement c where c.finCashflowCategory.id = ?1")
//	Iterable<FinCashflowStatement> allByCat(
//			@Param("cat") Integer cat);

}
