package com.cordialsr.Dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.Resources;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.projection.ContractProjection;
import com.cordialsr.dto.ContractDto;
import com.cordialsr.dto.mini.ContractMiniDto;


@RepositoryRestResource(collectionResourceRel = "contract", path = "contract", 
						excerptProjection=ContractProjection.class)
public interface ContractDao extends JpaRepository<Contract, Long> {
	
	@Query("from Contract c "
			+ "where c.company.id = ?1 "
			+ "and c.contractStatus.id <= ?2 ")
	Page<Contract> getAllByCom(
			@Param("cid") Integer cid,
			@Param("cStatus") Integer cStatus,
			Pageable pageRequest);
	
	@Query("from Contract c "
			+ "where c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "and c.contractStatus.id <= ?3 ")
	Page<Contract> getAllByBr(
			@Param("cid") Integer cid, 
			@Param("bid") Integer bid, 
			@Param("cStatus") Integer cStatus,
			Pageable pageRequest);
	
	//getAllByDates
	@Query(value = "Select * from Contract c "
			+ "where c.company = :cid "
			+ "and (c.date_signed between :dt1 and :dt2) "
			+ "and c.contract_status_id <= :cStatus "
			+ "ORDER BY ?#{#pageable} ",
			nativeQuery = true)
	Page<Contract> getAllByDates(
			@Param("cid") Integer cid, 
			@Param("cStatus") Integer cStatus,
			@Param("dt1") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt1,
			@Param("dt2") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt2,
			Pageable pageRequest);
	
	@Query(value = "Select * from Contract c "
			+ "where c.company = :cid "
			+ "and c.branch = :bid "
			+ "and c.service_branch = :servicebid "
			+ "and (c.date_signed between :dt1 and :dt2) "
			+ "and c.contract_status_id <= :cStatus "
			+ "ORDER BY ?#{#pageable} ",
			nativeQuery = true)
	Page<Contract> getAllByDatesBoth(
			@Param("cid") Integer cid, 
			@Param("bid") Integer bid,
			@Param("servicebid") Integer servicebid,
			@Param("cStatus") Integer cStatus,
			@Param("dt1") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt1,
			@Param("dt2") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt2,
			Pageable pageRequest);
	
	@Query(value = "Select * from Contract c "
			+ "where c.company = :cid "
			+ "and c.branch = :bid "
			+ "and (c.date_signed between :dt1 and :dt2) "
			+ "and c.contract_status_id <= :cStatus "
			+ "ORDER BY ?#{#pageable} ",
			nativeQuery = true)
	Page<Contract> getAllByDatesBid(
			@Param("cid") Integer cid, 
			@Param("bid") Integer bid,
			@Param("cStatus") Integer cStatus,
			@Param("dt1") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt1,
			@Param("dt2") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt2,
			Pageable pageRequest);
	
	@Query(value = "Select * from Contract c "
			+ "where c.company = :cid "
			+ "and c.service_branch = :servicebid "
			+ "and (c.date_signed between :dt1 and :dt2) "
			+ "and c.contract_status_id <= :cStatus "
			+ "ORDER BY ?#{#pageable} ",
			nativeQuery = true)
	Page<Contract> getAllByDatesServ(
			@Param("cid") Integer cid,
			@Param("servicebid") Integer servicebid,
			@Param("cStatus") Integer cStatus,
			@Param("dt1") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt1,
			@Param("dt2") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt2,
			Pageable pageRequest);
	
	@Query(value = "Select * from Contract c "
			+ "where c.company = :cid "
			+ "and c.branch = :bid "
			+ "and c.service_branch = :servicebid "
			+ "and c.date_signed < :dt2 "
			+ "and c.contract_status_id <= :cStatus "
			+ "ORDER BY ?#{#pageable} ",
			nativeQuery = true)
	Page<Contract> getAllPrevious(
			@Param("cid") Integer cid, 
			@Param("bid") Integer bid,
			@Param("servicebid") Integer servicebid,
			@Param("cStatus") Integer cStatus,
			@Param("dt2") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt2,
			Pageable pageRequest);
	
	@Query(value = "Select * from Contract c "
			+ "where c.company = :cid "
			+ "and c.branch = :bid "
			+ "and c.service_branch = :servicebid "
			+ "and c.contract_status_id <= :cStatus "
			+ "ORDER BY ?#{#pageable} ",
			nativeQuery = true)
	Page<Contract> getAllNoDateComBidServBid(
			@Param("cid") Integer cid, 
			@Param("bid") Integer bid,
			@Param("servicebid") Integer servicebid,
			@Param("cStatus") Integer cStatus,
			Pageable pageRequest);
	
	@Query(value = "Select * from Contract c "
			+ "where c.company = :cid "
			+ "and c.service_branch = :servicebid "
			+ "and c.contract_status_id <= :cStatus "
			+ "ORDER BY ?#{#pageable} ",
			nativeQuery = true)
	Page<Contract> getAllNoDateComServBid(
			@Param("cid") Integer cid, 
			@Param("servicebid") Integer servicebid,
			@Param("cStatus") Integer cStatus,
			Pageable pageRequest);
	
	@Query(value = "Select * from Contract c "
			+ "where c.company = :cid "
			+ "and c.branch = :bid "
			+ "and c.contract_status_id <= :cStatus "
			+ "ORDER BY ?#{#pageable} ",
			nativeQuery = true)
	Page<Contract> getAllNoDateComBid(
			@Param("cid") Integer cid, 
			@Param("bid") Integer bid,
			@Param("cStatus") Integer cStatus,
			Pageable pageRequest);
	
	
	// contractCounts
	@Query("select count(c) from Contract c "
			+ "where c.manager.id = ?1 "
			+ "and c.dateSigned between ?2 and ?3")
	Integer getCountManagerContract(
			@Param("empl") Long empl,
			@Param("dateStart") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateStart,
			@Param("dateEnd") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateEnd);
	
	@Query(value = "select count(*) from Contract c "
			+ "left join Employee e on e.id = c.manager "
			+ "where e.company_id = :cid "
			+ "and e.branch_id = :bid "
			+ "and c.date_signed between :dateStart and :dateEnd",
			nativeQuery = true)
	int findContractNumberByBranch(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("dateStart") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateStart,
			@Param("dateEnd") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateEnd);
	
	@Query( value = "select count(*) from Contract c "
			+ "left join Employee e on e.id = c.manager "
			+ "where c.company = :cid "
			+ "and c.date_signed between :dateStart and :dateEnd",
			nativeQuery = true)
	int findContractNumberByCompany(
			@Param("cid") Integer cid,
			@Param("dateStart") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateStart,
			@Param("dateEnd") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateEnd);
	
	
	// **********************************************************************************************************************************************
	
	
	@Query("select count(c) from Contract c "
			+ "where c.dealer.id = ?1 "
			+ "and c.manager.id = ?2 "
			+ "and c.dateSigned between ?3 and ?4")
	Integer getCountDealerContract(
			@Param("dealerId") Long dealerId,
			@Param("managerId") Long managerId,
			@Param("dateStart") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateStart,
			@Param("dateEnd") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateEnd);
	
	
	
	// getAllByDate
	@Query(value = "Select * from Contract c "
			+ "where c.company = :cid "
			+ "and c.branch = :bid "
			+ "and c.service_branch = :servicebid "
			+ "and c.date_signed >= :dt1 "
			+ "and c.contract_status_id <= :cStatus "
			+ "ORDER BY ?#{#pageable} ",
			nativeQuery = true)
	Page<Contract> getAllByDateBoth(
			@Param("cid") Integer cid, 
			@Param("bid") Integer bid,
			@Param("servicebid") Integer servicebid,
			@Param("cStatus") Integer cStatus,
			@Param("dt1") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt1,
			Pageable pageRequest);
	
	@Query(value = "Select * from Contract c "
			+ "where c.company = :cid "
			+ "and c.service_branch = :servicebid "
			+ "and c.date_signed >= :dt1 "
			+ "and c.contract_status_id <= :cStatus "
			+ "ORDER BY ?#{#pageable} ",
			nativeQuery = true)
	Page<Contract> getAllByDateServiceBid(
			@Param("cid") Integer cid, 
			@Param("servicebid") Integer servicebid,
			@Param("cStatus") Integer cStatus,
			@Param("dt1") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt1,
			Pageable pageRequest);
	
	@Query(value = "Select * from Contract c "
			+ "where c.company = :cid "
			+ "and c.branch = :bid "
			+ "and c.date_signed >= :dt1 "
			+ "and c.contract_status_id <= :cStatus "
			+ "ORDER BY ?#{#pageable} ",
			nativeQuery = true)
	Page<Contract> getAllByDateBid(
			@Param("cid") Integer cid, 
			@Param("bid") Integer bid,
			@Param("cStatus") Integer cStatus,
			@Param("dt1") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt1,
			Pageable pageRequest);
	
	@Query(value = "Select * from Contract c "
			+ "where c.company = :cid "
			+ "and c.date_signed >= :dt1 "
			+ "and c.contract_status_id <= :cStatus "
			+ "ORDER BY ?#{#pageable} ",
			nativeQuery = true)
	Page<Contract> getAllByDate(
			@Param("cid") Integer cid,
			@Param("cStatus") Integer cStatus,
			@Param("dt1") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt1,
			Pageable pageRequest);
	
	// ************************************************************************************************

	// filter
	@Query("from Contract c where "
			+ "(c.contractNumber like %?1% "
			
			+ "or c.refkey like %?1% "
			+ "or c.contractType.name like %?1% "
			
			+ "or c.inventorySn like %?1% "
			
			+ "or c.customer.firstname like %?1% "
			+ "or c.customer.lastname like %?1% "
			+ "or c.customer.middlename like %?1% "
			+ "or c.customer.iinBin like %?1% "
			+ "or c.customer.companyName like %?1% "
			+ "or c.customer.passportNumber like %?1% "
			+ "or c.customer.iik like %?1% "
			+ "or c.customer.bik like %?1% "
			
			+ "or c.exploiter.firstname like %?1% "
			+ "or c.exploiter.lastname like %?1% "
			+ "or c.exploiter.middlename like %?1% "
			+ "or c.exploiter.iinBin like %?1% "
			+ "or c.exploiter.companyName like %?1% "
			+ "or c.exploiter.passportNumber like %?1% "
			+ "or c.exploiter.iik like %?1% "
			+ "or c.exploiter.bik like %?1% "

			+ "or c.dealer.party.firstname like %?1% "
			+ "or c.dealer.party.lastname like %?1% "
			+ "or c.dealer.party.middlename like %?1% "
			+ "or c.dealer.party.iinBin like %?1% "			
			
			+ ")")
	Page<Contract> filter( 
			@Param("filter") String filter,
			Pageable pageRequest);
	
	
	
	@Query("from Contract c where "
			+ "c.company.id = ?1 "
			+ "and c.contractStatus.id <= ?2 "
			+ "and "
			+ "(c.contractNumber like %?3% "
			+ "or c.inventorySn like %?3% "
			+ "or c.refkey like %?3% "
			+ "or c.contractType.name like %?3% "
			
			+ "or c.customer.firstname like %?3% "
			+ "or c.customer.lastname like %?3% "
			+ "or c.customer.middlename like %?3% "
			+ "or c.customer.iinBin like %?3% "
			+ "or c.customer.companyName like %?3% "
			+ "or c.customer.passportNumber like %?3% "
			+ "or c.customer.iik like %?3% "
			+ "or c.customer.bik like %?3% "
			
			+ "or c.exploiter.firstname like %?3% "
			+ "or c.exploiter.lastname like %?3% "
			+ "or c.exploiter.middlename like %?3% "
			+ "or c.exploiter.iinBin like %?3% "
			+ "or c.exploiter.companyName like %?3% "
			+ "or c.exploiter.passportNumber like %?3% "
			+ "or c.exploiter.iik like %?3% "
			+ "or c.exploiter.bik like %?3% "

			+ "or c.dealer.party.firstname like %?3% "
			+ "or c.dealer.party.lastname like %?3% "
			+ "or c.dealer.party.middlename like %?3% "
			+ "or c.dealer.party.iinBin like %?3% "
			+ ") ")
	Page<Contract> filter1( 
			@Param("cid") Integer cid,
			@Param("cStatus") Integer cStatus,
			@Param("filter") String filter,
			Pageable pageRequest);
	
	@Query("from Contract c "
			+ "where "
			+ "c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			// + "and c.storno = 0 "
			+ "and "
			+ "(c.contractNumber like %?3% "
			+ "or c.contractType.name like %?3% "
			
			+ "or c.inventorySn like %?3% "
			
			+ "or c.customer.firstname like %?3% "
			+ "or c.customer.lastname like %?3% "
			+ "or c.customer.middlename like %?3% "
			+ "or c.customer.iinBin like %?3% "
			+ "or c.customer.companyName like %?3% "
			+ "or c.customer.passportNumber like %?3% "
			+ "or c.customer.iik like %?3% "
			+ "or c.customer.bik like %?3% "
			
//			+ "or c.phonePay.phNumber like %?3% "
//			+ "or c.mobilePay.phNumber like %?3% "
//			
			+ "or c.exploiter.firstname like %?3% "
			+ "or c.exploiter.lastname like %?3% "
			+ "or c.exploiter.middlename like %?3% "
			+ "or c.exploiter.iinBin like %?3% "
			+ "or c.exploiter.companyName like %?3% "
			+ "or c.exploiter.passportNumber like %?3% "
			+ "or c.exploiter.iik like %?3% "
			+ "or c.exploiter.bik like %?3% "
			
//			+ "or c.phoneFact.phNumber like %?3% "
//			+ "or c.mobileFact.phNumber like %?3% "

			+ "or c.dealer.party.firstname like %?3% "
			+ "or c.dealer.party.lastname like %?3% "
			+ "or c.dealer.party.middlename like %?3% "
			+ ") ")
	Page<Contract> filter12( 
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("filter") String filter,  Pageable pageRequest);
	
	
	@Query("from Contract c "
//			+ "left join phoneNumber f on f.party = c.customer "
			+ "where "
			+ "c.company.id = ?1 "
			+ "and c.serviceBranch.id = ?2 "
			// + "and c.storno = 0 "
			+ "and "
			+ "(c.contractNumber like %?3% "
			+ "or c.contractType.name like %?3% "
			
			+ "or c.inventorySn like %?3% "
			
			+ "or c.customer.firstname like %?3% "
			+ "or c.customer.lastname like %?3% "
			+ "or c.customer.middlename like %?3% "
			+ "or c.customer.iinBin like %?3% "
			+ "or c.customer.companyName like %?3% "
			+ "or c.customer.passportNumber like %?3% "
			+ "or c.customer.iik like %?3% "
			+ "or c.customer.bik like %?3% "
//			+ "or c.phonePay.phNumber like %?3% "
//			+ "or c.mobilePay.phNumber like %?3% "
			
			+ "or c.exploiter.firstname like %?3% "
			+ "or c.exploiter.lastname like %?3% "
			+ "or c.exploiter.middlename like %?3% "
			+ "or c.exploiter.iinBin like %?3% "
			+ "or c.exploiter.companyName like %?3% "
			+ "or c.exploiter.passportNumber like %?3% "
			+ "or c.exploiter.iik like %?3% "
			+ "or c.exploiter.bik like %?3% "
//			+ "or c.phoneFact.phNumber like %?3% "
//			+ "or c.mobilePay.phNumber like %?3% "

			+ "or c.dealer.party.firstname like %?3% "
			+ "or c.dealer.party.lastname like %?3% "
			+ "or c.dealer.party.middlename like %?3% "
			+ ") ")
	Page<Contract> filter12Service( 
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("filter") String filter,  Pageable pageRequest);
	
	
	@Query("from Contract c "
			+ "where "
			+ "c.company.id = ?1 "
			// + "and c.storno = 0 "
			+ "and "
			+ "(c.contractNumber like %?2% "
			+ "or c.contractType.name like %?2% "
			
			+ "or c.inventorySn like %?2% "
			
			+ "or c.customer.firstname like %?2% "
			+ "or c.customer.lastname like %?2% "
			+ "or c.customer.middlename like %?2% "
			+ "or c.customer.iinBin like %?2% "
			+ "or c.customer.companyName like %?2% "
			+ "or c.customer.passportNumber like %?2% "
			+ "or c.customer.iik like %?2% "
			+ "or c.customer.bik like %?2% "
//			+ "or c.phonePay.phNumber like %?2% "
//			+ "or c.mobilePay.phNumber like %?2% "
			
			+ "or c.exploiter.firstname like %?2% "
			+ "or c.exploiter.lastname like %?2% "
			+ "or c.exploiter.middlename like %?2% "
			+ "or c.exploiter.iinBin like %?2% "
			+ "or c.exploiter.companyName like %?2% "
			+ "or c.exploiter.passportNumber like %?2% "
			+ "or c.exploiter.iik like %?2% "
			+ "or c.exploiter.bik like %?2% "
//			+ "or c.phoneFact.phNumber like %?2% "
//			+ "or c.mobilePay.phNumber like %?2% "

			+ "or c.dealer.party.firstname like %?2% "
			+ "or c.dealer.party.lastname like %?2% "
			+ "or c.dealer.party.middlename like %?2% "
			+ ") ")
	Page<Contract> filterCid( 
			@Param("cid") Integer cid,
			@Param("filter") String filter,  Pageable pageRequest);
	
	@Query("from Contract c where "
			+ "c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "and c.serviceBranch.id = ?3 "
			+ "and c.contractStatus.id <= ?4 "
			+ "and "
			+ "(c.contractNumber like %?5% "
			+ "or c.contractType.name like %?5% "
			
			+ "or c.inventorySn like %?5% "
			
			+ "or c.customer.firstname like %?5% "
			+ "or c.customer.lastname like %?5% "
			+ "or c.customer.middlename like %?5% "
			+ "or c.customer.iinBin like %?5% "
			+ "or c.customer.companyName like %?5% "
			+ "or c.customer.passportNumber like %?5% "
			+ "or c.customer.iik like %?5% "
			+ "or c.customer.bik like %?5% "
			
			+ "or c.exploiter.firstname like %?5% "
			+ "or c.exploiter.lastname like %?5% "
			+ "or c.exploiter.middlename like %?5% "
			+ "or c.exploiter.iinBin like %?5% "
			+ "or c.exploiter.companyName like %?5% "
			+ "or c.exploiter.passportNumber like %?5% "
			+ "or c.exploiter.iik like %?5% "
			+ "or c.exploiter.bik like %?5% "

			+ "or c.dealer.party.firstname like %?5% "
			+ "or c.dealer.party.lastname like %?5% "
			+ "or c.dealer.party.middlename like %?5% "
			+ ") ")
	Page<Contract> filter123( 
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("servicebid") Integer servicebid,
			@Param("cStatus") Integer cStatus,
			@Param("filter") String filter,  Pageable pageRequest);
	
	
	
	@Query("from Contract c where "
			+ "c.company.id = ?1 "
			+ "and c.serviceBranch.id = ?2 "
			+ "and c.contractStatus.id <= ?3 "
			+ "and "
			+ "(c.contractNumber like %?4% "
			+ "or c.contractType.name like %?4% "
			
			+ "or c.inventorySn like %?4% "
			
			+ "or c.customer.firstname like %?4% "
			+ "or c.customer.lastname like %?4% "
			+ "or c.customer.middlename like %?4% "
			+ "or c.customer.iinBin like %?4% "
			+ "or c.customer.companyName like %?4% "
			+ "or c.customer.passportNumber like %?4% "
			+ "or c.customer.iik like %?4% "
			+ "or c.customer.bik like %?4% "
			
			+ "or c.exploiter.firstname like %?4% "
			+ "or c.exploiter.lastname like %?4% "
			+ "or c.exploiter.middlename like %?4% "
			+ "or c.exploiter.iinBin like %?4% "
			+ "or c.exploiter.companyName like %?4% "
			+ "or c.exploiter.passportNumber like %?4% "
			+ "or c.exploiter.iik like %?4% "
			+ "or c.exploiter.bik like %?4% "

			+ "or c.dealer.party.firstname like %?4% "
			+ "or c.dealer.party.lastname like %?4% "
			+ "or c.dealer.party.middlename like %?4% "
			+ ") ")
	Page<Contract> filter13( 
			@Param("cid") Integer cid,
			@Param("servicebid") Integer servicebid,
			@Param("cStatus") Integer cStatus,
			@Param("filter") String filter,  Pageable pageRequest);
	
	
	// **********************************************************************************************
	
	
	
	@Query("from Contract c where c.id = ?1")
	Contract findcid(@Param("id") Long id);
	
	@Query(value = "Select c.* from Contract c " + 
			"left join contract_payment_schedule p on p.contract_id = c.id " + 
			"where c.company = ?1 " + 
			"and c.service_branch = ?2 " + 
			// "and c.collector is null " + 
			"and p.payment_date <= ?3 " + 
			"and (p.summ - p.paid > 0) " +
			"and coalesce(c.storno, 0) = 0 " +
			"group by c.id;",
			nativeQuery = true)
	List<Contract> getReceivablesForDateByServiceBranch(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("dte") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dte);
	
	@Query(value = "Select c.* from Contract c " + 
			"left join contract_payment_schedule p on p.contract_id = c.id " + 
			"where c.company = ?1 " + 
			"and c.service_branch = ?2 " + 
			"and c.collector = ?3 " + 
			"and p.payment_date <= ?4 " + 
			"and (p.summ - p.paid > 0) " + 
			"and coalesce(c.storno, 0) = 0 " +
			"group by c.id;",
			nativeQuery = true)
	List<Contract> getReceivablesForDateByCollector(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("sid") Long sid,
			@Param("dte") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dte);
	
	@Query(value = "Select c.* from Contract c " + 
			"left join contract_payment_schedule p on p.contract_id = c.id " + 
			"where c.company = ?1 " + 
			"and c.branch = ?2 " + 
			"and c.dealer = ?3 " + 
			"and p.payment_date <= ?4 " + 
			"and (p.summ - p.paid > 0) " + 
			"and c.storno = 0 " +
			"group by c.id;",
			nativeQuery = true)
	List<Contract> getReceivablesForDateByDealer(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("sid") Long sid,
			@Param("dte") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dte);
	
	@Query(value = "Select c.* from Contract c " + 
			"left join contract_payment_schedule p on p.contract_id = c.id " + 
			"where c.company = ?1 " + 
			"and c.branch = ?2 " + 
			"and c.customer = ?3 " + 
			"and p.payment_date <= ?4 " + 
			"and (p.summ - p.paid > 0) " + 
			"and c.storno = 0 " +
			"group by c.id;",
			nativeQuery = true)
	List<Contract> getReceivablesForDateByCustomer(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("cusId") Long cusId,
			@Param("dte") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dte);
	
	@Query("from Contract c " +
			"where c.company.id = ?1 " +
			"and c.branch.id = ?2")
	List<Contract> getContractByCidBid(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid);
	
	@Query("from Contract c " +
			"where c.company.id = ?1 " +
			"and c.branch.id = ?2 " +
			"and MONTH(c.dateSigned) = ?3 " +
			"and YEAR(c.dateSigned) = ?4 ")
	List<Contract> getContractByCidBidMonth(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("bid") Integer month,
			@Param("bid") Integer year);
	
	@Query("from Contract c " +
			"where c.company.id = ?1 " +
			"and c.contractNumber = ?2")
	Contract getContractByCn(
			@Param("cid") Integer cid,
			@Param("cn") String  cn);
	
	@Query("from Contract c " +
			"where c.company.id = ?1 " +
			"and c.branch.id = ?2 " +
			"and c.contractNumber = ?3")
	Contract getContractByCnCidBid(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("cn") String  cn);
	
	@Query("from Contract c " +
			"where c.company.id = ?1 " +
			"and c.branch.id = ?2 " +
			"and c.dealer.id = ?3 " +
			"and c.isRent = ?4 " +
			"and c.storno = 0 " +
			"and c.dateSigned between ?5 and ?6") 
	List<Contract> getDealerContractsFor(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("eid") Long  eid,
			@Param("ir") Boolean ir,
			@Param("ds") @DateTimeFormat(pattern = "yyyy-MM-dd") Date ds,
			@Param("de") @DateTimeFormat(pattern = "yyyy-MM-dd") Date de);
	
	@Query("from Contract c " +
			"where c.company.id = ?1 " +
			"and c.branch.id = ?2 " +
			"and c.demosec.id = ?3 " +
			"and c.isRent = ?4 " +
			"and c.storno = 0 " +
			"and c.dateSigned between ?5 and ?6") 
	List<Contract> getDemosecContractsFor(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("eid") Long  eid,
			@Param("ir") Boolean ir,
			@Param("ds") @DateTimeFormat(pattern = "yyyy-MM-dd") Date ds,
			@Param("de") @DateTimeFormat(pattern = "yyyy-MM-dd") Date de);
	
	@Query("from Contract c " +
			"where c.company.id = ?1 " +
			"and c.branch.id = ?2 " +
			"and c.manager.id = ?3 " +
			"and c.isRent = ?4 " +
			"and c.storno = 0 " +
			"and c.dateSigned between ?5 and ?6") 
	List<Contract> getManagerContractsFor(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("eid") Long  eid,
			@Param("ir") Boolean ir,
			@Param("ds") @DateTimeFormat(pattern = "yyyy-MM-dd") Date ds,
			@Param("de") @DateTimeFormat(pattern = "yyyy-MM-dd") Date de);
	
	@Query("from Contract c " +
			"where c.company.id = ?1 " +
			"and c.branch.id = ?2 " +
			"and c.director.id = ?3 " +
			"and c.isRent = ?4 " +
			"and c.storno = 0 " +
			"and c.dateSigned between ?5 and ?6") 
	List<Contract> getDirectorContractsFor(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("eid") Long  eid,
			@Param("ir") Boolean ir,
			@Param("ds") @DateTimeFormat(pattern = "yyyy-MM-dd") Date ds,
			@Param("de") @DateTimeFormat(pattern = "yyyy-MM-dd") Date de);
	
	@Query("from Contract c " +
			"where c.company.id = ?1 " +
			"and c.coordinator.id = ?2 " +
			"and c.isRent = ?3 " +
			"and c.storno = 0 " +
			"and c.dateSigned between ?4 and ?5") 
	List<Contract> getCoordinatorContractsFor(
			@Param("cid") Integer cid,
			@Param("eid") Long  eid,
			@Param("ir") Boolean ir,
			@Param("ds") @DateTimeFormat(pattern = "yyyy-MM-dd") Date ds,
			@Param("de") @DateTimeFormat(pattern = "yyyy-MM-dd") Date de);
	
	
	// ************************************************************************************************
	
	@Query("select count(e) from Contract e where e.branch = ?1 "
			+ "and EXTRACT(DAY FROM e.dateSigned) = ?2 "
			+ "and EXTRACT(MONTH FROM e.dateSigned) = ?3 "
			+ "and EXTRACT(YEAR FROM e.dateSigned) = ?4 "
			+ "and e.contractStatus != 4")
			int findByDay(@Param("br") Branch br,
						  @Param("day") Integer day,
						  @Param("month") Integer month,
						  @Param("year") Integer year);
	
	@Query("select count(c) from Contract c where "
			+ "c.branch = ?1 "
			+ "and EXTRACT(MONTH FROM c.dateSigned) = ?2 "
			+ "and EXTRACT(YEAR FROM c.dateSigned) = ?3 "
			+ "and c.contractStatus != 4")
			int findByMonth(@Param("br") Branch br,
							@Param("month") Integer month,
							@Param("year") Integer year);
	
	// ************************************************************************************************
	
	// getContractNumber
	
	@Query("select count(c) from Contract c " +
			"where c.company.id = ?1 " +
			"and c.branch.id = ?2 " +
			"and c.storno = 0 " +
			"and c.month >= ?5 " +
			"and c.month <= ?6 " +
			"and c.isRent = ?7 " +
			"and c.dateSigned between ?3 and ?4") 
	int getDirectorContractQuantity(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("ds") @DateTimeFormat(pattern = "yyyy-MM-dd") Date ds,
			@Param("de") @DateTimeFormat(pattern = "yyyy-MM-dd") Date de,
			@Param("tmin") Integer tmin,
			@Param("tmax") Integer tmax,
			@Param("isRent") Boolean isRent);
	
	
	
	@Query("select count(c) from Contract c " +
			"where c.company.id = ?1 " +
			"and c.branch.id = ?2 " +
			"and c.manager.id = ?3 " +
			"and c.storno = 0 " +
			"and c.month >= ?6 " +
			"and c.month <= ?7 " +
			"and c.isRent = ?8 " +
			"and c.dateSigned between ?4 and ?5") 
	int getManagerContractQuantity(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("eid") Long  eid,
			@Param("ds") @DateTimeFormat(pattern = "yyyy-MM-dd") Date ds,
			@Param("de") @DateTimeFormat(pattern = "yyyy-MM-dd") Date de,
			@Param("tmin") Integer tmin,
			@Param("tmax") Integer tmax,
			@Param("isRent") Boolean isRent);
	
	@Query("select count(c) from Contract c " +
			"where c.company.id = ?1 " +
			"and c.branch.id = ?2 " +
			"and c.manager is null " +
			"and c.storno = 0 " +
			"and c.month >= ?5 " +
			"and c.month <= ?6 " +
			"and c.isRent = ?7 " +
			"and c.dateSigned between ?3 and ?4") 
	int getNoManagerContractQuantity(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
//			@Param("eid") Long  eid,
			@Param("ds") @DateTimeFormat(pattern = "yyyy-MM-dd") Date ds,
			@Param("de") @DateTimeFormat(pattern = "yyyy-MM-dd") Date de,
			@Param("tmin") Integer tmin,
			@Param("tmax") Integer tmax,
			@Param("isRent") Boolean isRent);
	
	@Query("select count(c) from Contract c " +
			"where c.company.id = ?1 " +
			"and c.branch.id = ?2 " +
			"and c.dealer.id = ?3 " +
			"and c.storno = 0 " +
			"and c.month >= ?6 " +
			"and c.month <= ?7 " +
			"and c.isRent = ?8 " +
			"and c.dateSigned between ?4 and ?5") 
	int getDealerContractQuantity(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("eid") Long  eid,
			@Param("ds") @DateTimeFormat(pattern = "yyyy-MM-dd") Date ds,
			@Param("de") @DateTimeFormat(pattern = "yyyy-MM-dd") Date de,
			@Param("tmin") Integer tmin,
			@Param("tmax") Integer tmax,
			@Param("isRent") Boolean isRent);
	
	
	
	// ************************************************************************************************
	
		@Query(value = "select * from contract c " + 
				"left join party p on c.customer = p.id " + 
				"where p.iin_bin = ?1 " +
				"and coalesce(c.storno, 0) = 0", nativeQuery = true)
		List<Contract> getAllContractsByIinBin(@Param("iinBin") String iinBin);
		
		@Query(value = "select c.* from contract c " + 
				"where c.customer = ?1 " +
				"and coalesce(c.storno, 0) = 0", nativeQuery = true)
		List<Contract> getAllContractsByCustomerId(@Param("customerId") Long customerId);
		
		@Query(value = "select c.* from contract c " + 
				"where c.customer = ?1 " +
				"and coalesce(c.storno, 0) = 0", nativeQuery = true)
		List<Contract> getAllConByCustomerId(@Param("customerId") Long customerId);
		
		@Query(value = "select c.* from contract c " 
				+ " where "
				 + " c.branch not in (21, 24, 28, 17, 27, 1) "
				 + " and (c.refkey is null or c.refkey = '') "
				 // + " and c.contract_number = 'K01-00000974' "
				 + " limit ?1",
				nativeQuery = true)
		List<Contract> getNewContractsWithRefkeyNullFirstN(Integer n);

		@Query("from Contract c " +
				"where " +
				"c.contractNumber = ?1")
		Contract findByContractNumber(@Param("cn") String  cn);
		
		
		
	// **********************************************************************************************************************************************************************************
		
		@Query(value = "select * from contract c "
				+ "where c.company = ?1 "
				+ "and c.storno = 0 "
				+ "and c.date_signed between ?2 and ?3 "
				+ "group by c.branch",
				nativeQuery = true)
		List<Contract> getAllDirectorByContract(
				@Param("cid") Integer cid,
				@Param("ds") @DateTimeFormat(pattern = "yyyy-MM-dd") Date ds,
				@Param("de") @DateTimeFormat(pattern = "yyyy-MM-dd") Date de);
		

		@Query(value = "select * from contract c "
				+ "left join Employee e on c.manager = e.id "
				+ "where c.company = ?1 "
				+ "and c.branch = ?2 "
				+ "and c.storno = 0 "
				+ "and c.date_signed between ?3 and ?4 "
				+ "group by c.manager ",
				nativeQuery = true)
		List<Contract> getAllManagerByDate(
				@Param("cid") Integer cid,
				@Param("bid") Integer bid,
				@Param("ds") @DateTimeFormat(pattern = "yyyy-MM-dd") Date ds,
				@Param("de") @DateTimeFormat(pattern = "yyyy-MM-dd") Date de);
		
		@Query(value = "select * from contract c "
				+ "where c.company = ?1 "
				+ "and c.branch = ?2 "
				+ "and c.storno = 0 "
				+ "and c.date_signed between ?3 and ?4 "
				+ "group by c.dealer ",
				nativeQuery = true)
		List<Contract> getAllDealerByDate(
				@Param("cid") Integer cid,
				@Param("bid") Integer bid,
				@Param("ds") @DateTimeFormat(pattern = "yyyy-MM-dd") Date ds,
				@Param("de") @DateTimeFormat(pattern = "yyyy-MM-dd") Date de);
		
		
		@Query(value = "select * from Contract c "
				+ "where c.company = ?1 "
				+ "and c.branch = ?2 "
				+ "and c.manager = ?3 "
				+ "and c.storno = 0 "
				+ "and c.date_signed between ?4 and ?5 "
				+ "group by c.dealer",
				nativeQuery = true)
		List<Contract> getAllContractByManager(
				@Param("cid") Integer cid,
				@Param("bid") Integer bid,
				@Param("emplId") Long emplId,
				@Param("ds") @DateTimeFormat(pattern = "yyyy-MM-dd") Date ds,
				@Param("de") @DateTimeFormat(pattern = "yyyy-MM-dd") Date de);
		
		@Query(value = "select * from Contract c "
				+ "where c.company = ?1 "
				+ "and c.branch = ?2 "
				+ "and c.director = ?3 "
				+ "and c.storno = 0 "
				+ "and c.date_signed between ?4 and ?5 "
				+ "group by c.manager",
				nativeQuery = true)
		List<Contract> getAllContractByDirector(
				@Param("cid") Integer cid,
				@Param("bid") Integer bid,
				@Param("emplId") Long emplId,
				@Param("ds") @DateTimeFormat(pattern = "yyyy-MM-dd") Date ds,
				@Param("de") @DateTimeFormat(pattern = "yyyy-MM-dd") Date de);
		
		// **********************************************************************
	
		@Query(value = "select * from Contract c "
				+ "where c.company = ?1 "
				+ "and c.branch = ?2 "
				+ "and c.storno = 0 "
				+ "and c.old_id is not null", 
				nativeQuery = true)
		List<Contract> getAllMigratedContracts(
				@Param("cid") Integer cid, 
				@Param("bid") Integer bid);
		
		@Query(value = "select * from Contract c "
				+ "where c.company = ?1 "
				+ "and c.branch = ?2 "
				+ "and c.date_signed between ?3 and ?4 "
				+ "and c.storno = 0 "
				+ "and coalesce(c.resigned, 0) = 0 "
				+ "and c.old_id is not null", 
				nativeQuery = true)
		List<Contract> getAllMigratedContractsBtw(
				@Param("cid") Integer cid, 
				@Param("bid") Integer bid,
				@Param("ds") @DateTimeFormat(pattern = "yyyy-MM-dd") Date ds,
				@Param("de") @DateTimeFormat(pattern = "yyyy-MM-dd") Date de);

		// **********************************************************************
		
		@Query(value = "select count(c.id) from Contract c "
				+ "where c.company = ?1 "
				+ "and c.customer = ?2 "
				+ "and c.currency = ?3 "
				+ "and c.date_signed < ?4 "
				+ "and coalesce(c.storno, 0) = 0", 
				nativeQuery = true)
		Integer getCustomerContractsCountUntil(
				Integer cid, 
				Long customerId, 
				String currency,
				Date dateEnd);
		
		@Query(value = "select count(c.id) from Contract c "
				+ "where c.company = ?1 "
				+ "and c.customer = ?2 "
				+ "and c.currency = ?3 "
				+ "and c.date_signed >= ?4 "
				+ "and coalesce(c.storno, 0) = 0", 
				nativeQuery = true)
		Integer getCustomerContractsCountSince(
				Integer cid, 
				Long customerId, 
				String currency,
				Date dateStart);
		
		// ***********************************************************************
		
		@Query(value = "select * from Contract c "
				+ "where c.inventory_sn = ?1 "
				+ "limit 1",
				nativeQuery = true) 
		Contract getContractByInventorySn(@Param("invSn") String invSn);
		
		@Query(value = "select * from Contract c "
				+ "where c.old_id = ?1 "
				+ "limit 1",
				nativeQuery = true) 
		Contract getContractByOldId(@Param("oldId") Integer oldId);
}
