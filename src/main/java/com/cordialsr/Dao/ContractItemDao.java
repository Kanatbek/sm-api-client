package com.cordialsr.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.ContractItem;

@RepositoryRestResource(collectionResourceRel = "conitem", path = "conitem")
public interface ContractItemDao extends CrudRepository<ContractItem, Long> {

}
