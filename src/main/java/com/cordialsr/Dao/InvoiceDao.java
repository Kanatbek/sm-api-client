package com.cordialsr.Dao;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;

import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.projection.InvoiceProjection;

@RepositoryRestResource(collectionResourceRel = "invoice", path = "invoice",
						excerptProjection = InvoiceProjection.class)
public interface InvoiceDao extends CrudRepository<Invoice, Long>{
	
	@Query("from Invoice i where i.company.id = ?1")
	Iterable<Invoice> allByCompany(@Param("comId") Integer comId);
	
	@Query(value = "Select * from invoice c where c.id = ?1 LIMIT 1",
	nativeQuery = true)
	Invoice findinv(@Param("id") Long id);
	
	@Query(value = "Select * from invoice i where i.invoice_number is null",
			nativeQuery = true)
	List<Invoice>  findAllInvNUmNull();

	//////////////////////////////////////// FIND Invoice by doc_id
	
	@Query(value = "Select * from invoice c where c.doc_id = ?1 and c.invoice_type = ?2 LIMIT 1",
			nativeQuery = true)
	Invoice findByDocId(@Param("id") Long id,
						@Param("type") Integer type);
	//////////////////////////////////////// Find Invoice by docno ...
	
	@Query(value = "Select * from invoice c where c.docno = ?1 and c.invoice_type = ?2 LIMIT 1",
			nativeQuery = true)
	Invoice findByDocno(@Param("docno") String docno,
						@Param("type") Integer type);
	
	
	//////////////////////////////////////// SEARCH ................
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1" 
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceList(@Param("cid") Integer cid,
			 Pageable pageRequest);
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.invoiceStatus.name = ?2" 
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListStatus(@Param("cid") Integer cid,
										   @Param("status") String status,
											 Pageable pageRequest);
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2" 
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran1(@Param("cid") Integer cid,
										  @Param("b1id") Integer b1id,
											 Pageable pageRequest);
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branchImporter.id = ?2" 
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran2(@Param("cid") Integer cid,
										  @Param("b2id") Integer b2id,
											 Pageable pageRequest);
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.invoiceType = ?2" 
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListType(@Param("cid") Integer cid,
										  @Param("type") Integer type,
											 Pageable pageRequest);
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and c.branchImporter.id = ?3" 
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran1Bran2(@Param("cid") Integer cid,
										   	   @Param("b1id") Integer b1id,
											   @Param("b2id") Integer b2id,
												 Pageable pageRequest);
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and c.invoiceStatus.name = ?3" 
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran1Status(@Param("cid") Integer cid,
										   	   @Param("b1id") Integer b1id,
											   @Param("status") String status,
												 Pageable pageRequest);
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1 " 
			+ " and c.branch.id = ?2 "
			+ " and c.invoiceStatus.name = ?3 " 
			+ " and c.invoiceType in (3, 4) "
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran1StatusSellRent(@Param("cid") Integer cid,
										   	   	 @Param("b1id") Integer b1id,
											     @Param("status") String status,
												 Pageable pageRequest);
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1 " 
			+ " and c.branchImporter.id = ?2 "
			+ " and c.invoiceStatus.name = ?3 " 
			+ " and c.invoiceType in (1, 2) "
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran2StatusImpTrans(@Param("cid") Integer cid,
										   	   	 	@Param("b2id") Integer b1id,
										   	   	 	@Param("status") String status,
										   	   	 	Pageable pageRequest);
	
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and c.invoiceType = ?3" 
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran1Type(@Param("cid") Integer cid,
										   	   @Param("b1id") Integer b1id,
											   @Param("type") Integer type,
												 Pageable pageRequest);
	
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branchImporter.id = ?2"
			+ " and c.invoiceStatus.name = ?3" 
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran2Status(@Param("cid") Integer cid,
											   @Param("b2id") Integer b2id,
											   @Param("status") String status,
												 Pageable pageRequest);
	
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branchImporter.id = ?2"
			+ " and invoiceType = ?3" 
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran2Type(@Param("cid") Integer cid,
											   @Param("b2id") Integer b2id,
											   @Param("type") Integer type,
												 Pageable pageRequest);
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.invoiceStatus.name = ?2"
			+ " and c.invoiceType = ?3" 
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListStatusType(@Param("cid") Integer cid,
											    @Param("status") String sname,
											    @Param("type") Integer type,
												 Pageable pageRequest);
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and c.branchImporter.id = ?3"
			+ " and c.invoiceStatus.name = ?4" 
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran1Bran2Status(@Param("cid") Integer cid,
												   @Param("b1id") Integer b1id,
												   @Param("b2id") Integer b2id,
												   @Param("status") String sname,
													 Pageable pageRequest);
	

	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and c.branchImporter.id = ?3"
			+ " and c.invoiceType = ?4" 
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran1Bran2Type(@Param("cid") Integer cid,
												   @Param("b1id") Integer b1id,
												   @Param("b2id") Integer b2id,
												   @Param("type") Integer type,
													 Pageable pageRequest);
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and c.invoiceStatus.name = ?3"
			+ " and c.invoiceType = ?4" 
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran1StatusType(@Param("cid") Integer cid,
												   @Param("b1id") Integer b1id,
												   @Param("status") String status,
												   @Param("type") Integer type,
													 Pageable pageRequest);
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branchImporter.id = ?2"
			+ " and c.invoiceStatus.name = ?3"
			+ " and c.invoiceType = ?4" 
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran2StatusType(@Param("cid") Integer cid,
												   @Param("b2id") Integer b2id,
												   @Param("status") String status,
												   @Param("type") Integer type,
													 Pageable pageRequest);
	
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and c.branchImporter.id = ?3"
			+ " and c.invoiceStatus.name = ?4"
			+ " and c.invoiceType = ?5" 
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran1Bran2StatusType(@Param("cid") Integer cid,
														@Param("b1id") Integer b1id,
														@Param("b2id") Integer b2id,
														@Param("status") String sname,
														@Param("type") Integer type,
														 Pageable pageRequest);

	/////////////////////////////////////////////////////// With Filter ..........................
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and "
			+ "(c.invoiceNumber like %?2% "
			+ "or c.refkey like %?2% "
			+ "or c.docno like %?2% "
			+ "or c.docId like %?2% "
			
			+ "or c.party.firstname like %?2% "
			+ "or c.party.lastname like %?2% "
			+ "or c.party.middlename like %?2% "
			+ "or c.party.iinBin like %?2% "
			+ "or c.party.companyName like %?2% "
			+ "or c.party.passportNumber like %?2% "
			+ ")"
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListFilter(@Param("cid") Integer cid,
									   @Param("flt") String flt,
			 					 	   Pageable pageRequest);
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.invoiceStatus.name = ?2" 
			+ " and "
			+ "(c.invoiceNumber like %?3% "
			+ "or c.refkey like %?3% "
			+ "or c.docno like %?3% "
			+ "or c.docId like %?3% "
			
			+ "or c.party.firstname like %?3% "
			+ "or c.party.lastname like %?3% "
			+ "or c.party.middlename like %?3% "
			+ "or c.party.iinBin like %?3% "
			+ "or c.party.companyName like %?3% "
			+ "or c.party.passportNumber like %?3% "
			+ ")"
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListStatusFilter(@Param("cid") Integer cid,
										     @Param("status") String status,
										     @Param("flt") String flt,
											 Pageable pageRequest);
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2" 
			+ " and "
			+ "(c.invoiceNumber like %?3% "
			+ "or c.refkey like %?3% "
			+ "or c.docno like %?3% "
			+ "or c.docId like %?3% "
			
			+ "or c.party.firstname like %?3% "
			+ "or c.party.lastname like %?3% "
			+ "or c.party.middlename like %?3% "
			+ "or c.party.iinBin like %?3% "
			+ "or c.party.companyName like %?3% "
			+ "or c.party.passportNumber like %?3% "
			+ ")"
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran1Filter(@Param("cid") Integer cid,
										    @Param("b1id") Integer b1id,
										    @Param("flt") String flt,
											 Pageable pageRequest);
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branchImporter.id = ?2" 
			+ " and "
			+ "(c.invoiceNumber like %?3% "
			+ "or c.refkey like %?3% "
			+ "or c.docno like %?3% "
			+ "or c.docId like %?3% "
			
			+ "or c.party.firstname like %?3% "
			+ "or c.party.lastname like %?3% "
			+ "or c.party.middlename like %?3% "
			+ "or c.party.iinBin like %?3% "
			+ "or c.party.companyName like %?3% "
			+ "or c.party.passportNumber like %?3% "
			+ ")"
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran2Filter(@Param("cid") Integer cid,
										    @Param("b2id") Integer b2id,
										    @Param("flt") String flt,
											 Pageable pageRequest);
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.invoiceType = ?2" 
			+ " and "
			+ "(c.invoiceNumber like %?3% "
			+ "or c.refkey like %?3% "
			+ "or c.docno like %?3% "
			+ "or c.docId like %?3% "
			
			+ "or c.party.firstname like %?3% "
			+ "or c.party.lastname like %?3% "
			+ "or c.party.middlename like %?3% "
			+ "or c.party.iinBin like %?3% "
			+ "or c.party.companyName like %?3% "
			+ "or c.party.passportNumber like %?3% "
			+ ")"
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListTypeFilter(@Param("cid") Integer cid,
										   @Param("type") Integer type,
										   @Param("flt") String flt,
											 Pageable pageRequest);
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and c.branchImporter.id = ?3" 
			+ " and "
			+ "(c.invoiceNumber like %?4% "
			+ "or c.refkey like %?4% "
			+ "or c.docno like %?4% "
			+ "or c.docId like %?4% "
			
			+ "or c.party.firstname like %?4% "
			+ "or c.party.lastname like %?4% "
			+ "or c.party.middlename like %?4% "
			+ "or c.party.iinBin like %?4% "
			+ "or c.party.companyName like %?4% "
			+ "or c.party.passportNumber like %?4% "
			+ ")"
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran1Bran2Filter(@Param("cid") Integer cid,
										   	     @Param("b1id") Integer b1id,
											     @Param("b2id") Integer b2id,
											     @Param("flt") String flt,
												 Pageable pageRequest);
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and c.invoiceStatus.name = ?3" 
			+ " and "
			+ "(c.invoiceNumber like %?4% "
			+ "or c.refkey like %?4% "
			+ "or c.docno like %?4% "
			+ "or c.docId like %?4% "
			
			+ "or c.party.firstname like %?4% "
			+ "or c.party.lastname like %?4% "
			+ "or c.party.middlename like %?4% "
			+ "or c.party.iinBin like %?4% "
			+ "or c.party.companyName like %?4% "
			+ "or c.party.passportNumber like %?4% "
			+ ")"
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran1StatusFilter(@Param("cid") Integer cid,
										   	      @Param("b1id") Integer b1id,
											      @Param("status") String status,
											      @Param("flt") String flt,
												  Pageable pageRequest);
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and c.invoiceStatus.name = ?3"
			+ " and c.invoiceType in (3, 4) "
			+ " and "
			+ "(c.invoiceNumber like %?4% "
			+ "or c.refkey like %?4% "
			+ "or c.docno like %?4% "
			+ "or c.docId like %?4% "
			
			+ "or c.party.firstname like %?4% "
			+ "or c.party.lastname like %?4% "
			+ "or c.party.middlename like %?4% "
			+ "or c.party.iinBin like %?4% "
			+ "or c.party.companyName like %?4% "
			+ "or c.party.passportNumber like %?4% "
			+ ")"
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran1StatusSellRentFilter(@Param("cid") Integer cid,
										   	      @Param("b1id") Integer b1id,
											      @Param("status") String status,
											      @Param("flt") String flt,
												  Pageable pageRequest);
	
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branchImporter.id = ?2"
			+ " and c.invoiceStatus.name = ?3"
			+ " and c.invoiceType in (1, 2) "
			+ " and "
			+ "(c.invoiceNumber like %?4% "
			+ "or c.refkey like %?4% "
			+ "or c.docno like %?4% "
			+ "or c.docId like %?4% "
			
			+ "or c.party.firstname like %?4% "
			+ "or c.party.lastname like %?4% "
			+ "or c.party.middlename like %?4% "
			+ "or c.party.iinBin like %?4% "
			+ "or c.party.companyName like %?4% "
			+ "or c.party.passportNumber like %?4% "
			+ ")"
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran2StatusImpTransFilter(@Param("cid") Integer cid,
										   	      	      @Param("b2id") Integer b1id,
										   	      	      @Param("status") String status,
										   	      	      @Param("flt") String flt,
										   	      	      Pageable pageRequest);
	
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and c.invoiceType = ?3" 
			+ " and "
			+ "(c.invoiceNumber like %?4% "
			+ "or c.refkey like %?4% "
			+ "or c.docno like %?4% "
			+ "or c.docId like %?4% "
			
			+ "or c.party.firstname like %?4% "
			+ "or c.party.lastname like %?4% "
			+ "or c.party.middlename like %?4% "
			+ "or c.party.iinBin like %?4% "
			+ "or c.party.companyName like %?4% "
			+ "or c.party.passportNumber like %?4% "
			+ ")"
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran1TypeFilter(@Param("cid") Integer cid,
										   	    @Param("b1id") Integer b1id,
											    @Param("type") Integer type,
											    @Param("flt") String flt,
												 Pageable pageRequest);
	
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branchImporter.id = ?2"
			+ " and c.invoiceStatus.name = ?3" 
			+ " and "
			+ "(c.invoiceNumber like %?4% "
			+ "or c.refkey like %?4% "
			+ "or c.docno like %?4% "
			+ "or c.docId like %?4% "
			
			+ "or c.party.firstname like %?4% "
			+ "or c.party.lastname like %?4% "
			+ "or c.party.middlename like %?4% "
			+ "or c.party.iinBin like %?4% "
			+ "or c.party.companyName like %?4% "
			+ "or c.party.passportNumber like %?4% "
			+ ")"
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran2StatusFilter(@Param("cid") Integer cid,
											      @Param("b2id") Integer b2id,
											      @Param("status") String status,
											      @Param("flt") String flt,
												  Pageable pageRequest);
	
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branchImporter.id = ?2"
			+ " and invoiceType = ?3" 
			+ " and "
			+ "(c.invoiceNumber like %?4% "
			+ "or c.refkey like %?4% "
			+ "or c.docno like %?4% "
			+ "or c.docId like %?4% "
			
			+ "or c.party.firstname like %?4% "
			+ "or c.party.lastname like %?4% "
			+ "or c.party.middlename like %?4% "
			+ "or c.party.iinBin like %?4% "
			+ "or c.party.companyName like %?4% "
			+ "or c.party.passportNumber like %?4% "
			+ ")"
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran2TypeFilter(@Param("cid") Integer cid,
											    @Param("b2id") Integer b2id,
											    @Param("type") Integer type,
											    @Param("flt") String flt,
												 Pageable pageRequest);
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.invoiceStatus.name = ?2"
			+ " and c.invoiceType = ?3" 
			+ " and "
			+ "(c.invoiceNumber like %?4% "
			+ "or c.refkey like %?4% "
			+ "or c.docno like %?4% "
			+ "or c.docId like %?4% "
			
			+ "or c.party.firstname like %?4% "
			+ "or c.party.lastname like %?4% "
			+ "or c.party.middlename like %?4% "
			+ "or c.party.iinBin like %?4% "
			+ "or c.party.companyName like %?4% "
			+ "or c.party.passportNumber like %?4% "
			+ ")"
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListStatusTypeFilter(@Param("cid") Integer cid,
											     @Param("status") String sname,
											     @Param("type") Integer type,
												 @Param("flt") String flt,
												 Pageable pageRequest);
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and c.branchImporter.id = ?3"
			+ " and c.invoiceStatus.name = ?4" 
			+ " and "
			+ "(c.invoiceNumber like %?5% "
			+ "or c.refkey like %?5% "
			+ "or c.docno like %?5% "
			+ "or c.docId like %?5% "
			
			+ "or c.party.firstname like %?5% "
			+ "or c.party.lastname like %?5% "
			+ "or c.party.middlename like %?5% "
			+ "or c.party.iinBin like %?5% "
			+ "or c.party.companyName like %?5% "
			+ "or c.party.passportNumber like %?5% "
			+ ")"
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran1Bran2StatusFilter(@Param("cid") Integer cid,
												       @Param("b1id") Integer b1id,
												       @Param("b2id") Integer b2id,
												       @Param("status") String sname,
												       @Param("flt") String flt,
													   Pageable pageRequest);
	

	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and c.branchImporter.id = ?3"
			+ " and c.invoiceType = ?4" 
			+ " and "
			+ "(c.invoiceNumber like %?5% "
			+ "or c.refkey like %?5% "
			+ "or c.docno like %?5% "
			+ "or c.docId like %?5% "
			
			+ "or c.party.firstname like %?5% "
			+ "or c.party.lastname like %?5% "
			+ "or c.party.middlename like %?5% "
			+ "or c.party.iinBin like %?5% "
			+ "or c.party.companyName like %?5% "
			+ "or c.party.passportNumber like %?5% "
			+ ")"
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran1Bran2TypeFilter(@Param("cid") Integer cid,
												     @Param("b1id") Integer b1id,
												     @Param("b2id") Integer b2id,
												     @Param("type") Integer type,
												     @Param("flt") String flt,
													 Pageable pageRequest);
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and c.invoiceStatus.name = ?3"
			+ " and c.invoiceType = ?4" 
			+ " and "
			+ "(c.invoiceNumber like %?5% "
			+ "or c.refkey like %?5% "
			+ "or c.docno like %?5% "
			+ "or c.docId like %?5% "
			
			+ "or c.party.firstname like %?5% "
			+ "or c.party.lastname like %?5% "
			+ "or c.party.middlename like %?5% "
			+ "or c.party.iinBin like %?5% "
			+ "or c.party.companyName like %?5% "
			+ "or c.party.passportNumber like %?5% "
			+ ")"
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran1StatusTypeFilter(@Param("cid") Integer cid,
												      @Param("b1id") Integer b1id,
												      @Param("status") String status,
												      @Param("type") Integer type,
												      @Param("flt") String flt,
													  Pageable pageRequest);
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branchImporter.id = ?2"
			+ " and c.invoiceStatus.name = ?3"
			+ " and c.invoiceType = ?4" 
			+ " and "
			+ "(c.invoiceNumber like %?5% "
			+ "or c.refkey like %?5% "
			+ "or c.docno like %?5% "
			+ "or c.docId like %?5% "
			
			+ "or c.party.firstname like %?5% "
			+ "or c.party.lastname like %?5% "
			+ "or c.party.middlename like %?5% "
			+ "or c.party.iinBin like %?5% "
			+ "or c.party.companyName like %?5% "
			+ "or c.party.passportNumber like %?5% "
			+ ")"
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran2StatusTypeFilter(@Param("cid") Integer cid,
												      @Param("b2id") Integer b2id,
												      @Param("status") String status,
												      @Param("type") Integer type,
													  @Param("flt") String flt,
													  Pageable pageRequest);
	
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and c.branchImporter.id = ?3"
			+ " and c.invoiceStatus.name = ?4"
			+ " and c.invoiceType = ?5" 
			+ " and "
			+ "(c.invoiceNumber like %?6% "
			+ "or c.refkey like %?6% "
			+ "or c.docno like %?6% "
			+ "or c.docId like %?6% "
			
			+ "or c.party.firstname like %?6% "
			+ "or c.party.lastname like %?6% "
			+ "or c.party.middlename like %?6% "
			+ "or c.party.iinBin like %?6% "
			+ "or c.party.companyName like %?6% "
			+ "or c.party.passportNumber like %?6% "
			+ ")"
			+ " ORDER BY id DESC")
	Page<Invoice> getInvoiceListBran1Bran2StatusTypeFilter(@Param("cid") Integer cid,
														   @Param("b1id") Integer b1id,
														   @Param("b2id") Integer b2id,
														   @Param("status") String sname,
														   @Param("type") Integer type,
														   @Param("flt") String flt,
														   Pageable pageRequest);
	
	///////////////////////////////////////////////////////not Main query .................... 
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branchImporter.id = ?2"
			+ " and c.invoiceType in (1 , 2)"
			+ " and c.invoiceStatus = 'PROCESS'")
	Iterable<Invoice> getInvoiceOrderList(@Param("cid") Integer cid,
			@Param("bid") Integer bid);
	
	

	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and c.party.id = ?3"
			+ " and c.invoiceType = 8"
			+ " and c.invoiceStatus = 'CLOSED'")
	Iterable<Invoice> getInvoiceListAccountable(@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("party") Long party);
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Query(value = "from Invoice c "
					+ " where c.company.id = ?1"
					+ " and c.storno = 0"
					+ " and c.data between ?2 and ?3")
	ArrayList<Invoice> getInvoicesToPeriodFromTo(@Param("cid") Integer cid,
		  @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("from") Date from,
		  @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("to") Date to);


	@Query(value = "from Invoice c "
				 + " where c.company.id = ?1"
				 + " and (c.branch.id = ?2"
				 + " or c.branchImporter.id = ?2)"
				 + " and c.data between ?3 and ?4")
	ArrayList<Invoice> getInvoicesToPeriodBranFromTo(@Param("cid") Integer cid,
												   	 @Param("bid") Integer bid,
	          @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("from") Date from,
			  @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("to") Date to);
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Query(value = "from Invoice c "
			+ " where c.company.id = ?1"
			+ " and c.invoiceType = ?4"
			+ " and c.data between ?2 and ?3")
	ArrayList<Invoice> getInvoicesForServices(@Param("cid") Integer cid,
	   @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("from") Date from,
	   @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("to") Date to,
											  @Param("type") Integer type); 
	
	
	@Query(value = "from Invoice c "
			 + " where c.company.id = ?1"
			 + " and c.invoiceType = ?5"
			 + " and c.branch.id = ?2"
			 + " and c.data between ?3 and ?4")
	ArrayList<Invoice> getInvoicesForServicesByBranch (@Param("cid") Integer cid,
			       								       @Param("bid") Integer bid,
	            @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("from") Date from,
		        @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("to") Date to,
				  									   @Param("type") Integer type);
	
	@Query(value = "from Invoice c "
			 + " where c.company.id = ?1"
			 + " and c.invoiceType = ?6"
			 + " and c.party.id = ?3"
			 + " and c.branch.id = ?2"
			 + " and c.data between ?4 and ?5")
	ArrayList<Invoice> getInvoicesForServicesByBranchParty(@Param("cid") Integer cid,
			       								           @Param("bid") Integer bid,
			       								           @Param("prt") Long prt,
	                @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("from") Date from,
		            @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("to") Date to,
				  									       @Param("type") Integer type);
	
/////////////////////////////////////////////////////////////////// Invoice Report ...............................................................
	
	@Query(value = "from Invoice c "
			 + " where c.company.id = ?1"
			 + " and c.invoiceType = ?6"
			 + " and c.party.id = ?3"
			 + " and c.branch.id = ?2"
			 + " and c.data between ?4 and ?5")
	ArrayList<Invoice> getInvoiceForStockReports(@Param("cid") Integer cid,
			       								 @Param("bid") Integer bid,
			       								 @Param("inv") Integer inv);
	
	////////////////////////////////////////////////////////// All Invoices By Docno ..............................................................

	@Query(value = "Select * from invoice c where c.docno = ?1",
			nativeQuery = true)
	List<Invoice> findAllInvoiceDocno(@Param("docno") String docno);
}

