package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.SmFcTerm;
import com.cordialsr.domain.projection.SmFcTermProjection;

@RepositoryRestResource(collectionResourceRel = "smfcterm", path = "smfcterm",
excerptProjection = SmFcTermProjection.class)
public interface SmFcTermDao extends CrudRepository<SmFcTerm, Integer> {
	
	@Query(value = "from SmFcTerm c "
			+ " where c.inventory.id = ?1")
	Iterable<SmFcTerm> findOneByInventory(@Param("inv") Integer inv);
	

}
