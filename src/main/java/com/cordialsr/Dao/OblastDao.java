package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.City;
import com.cordialsr.domain.Oblast;

@RepositoryRestResource(collectionResourceRel = "oblast", path = "oblast")

public interface OblastDao extends PagingAndSortingRepository<Oblast, Integer>{

	@Query("from Oblast o where "
			+ "o.country.id = ?1")
			Iterable<Oblast>findAllByCountry(@Param("country") int country);
}
