package com.cordialsr.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.projection.InventoryInlineProjection;

@RepositoryRestResource(collectionResourceRel = "inventory", path = "inventory",
						excerptProjection = InventoryInlineProjection.class)
public interface InventoryDao extends CrudRepository<Inventory, Integer>{
	
	@Query("from Inventory i where "
			+ "i.company.id = ?1")
			Iterable<Inventory> findAllByCompany(@Param("company") Integer company);
	
	@Query("from Inventory i where "
			+ "i.company.id = ?1 "
			+ "and i.invMainCategory.id = 1")
			Iterable<Inventory> findAllByCompanyByItem(@Param("company") Integer company);

	@Query(value = "SELECT i.* from Inventory i "
			+ "left join inventory_spares s on s.spare = i.id "
			+ "where s.parent = ?1 "
			+ "and i.main_category_id = 2", 
			nativeQuery = true)
			Iterable<Inventory> findAllByInventoryAndMainCategory(@Param("inv") Integer inv);
	
	
	@Query(value = "SELECT i.* FROM inventory i " 
			+ "left join inventory_spares s on s.spare = i.id "  
			+ "where s.parent = ?1 "  
			+ "and i.sub_category_id = ?2", 
			nativeQuery = true)
			List<Inventory> findAllSparesByParentId(@Param("inv") Integer inv, 
													@Param("subcat") Integer subcat);
	
	boolean existsByCode(String code);
	Inventory getByCode(String code);
}
