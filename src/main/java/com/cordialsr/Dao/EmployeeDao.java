package com.cordialsr.Dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;

import com.cordialsr.domain.Company;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.Position;
import com.cordialsr.domain.projection.EmployeeProjection;

@RepositoryRestResource(collectionResourceRel = "employee", path = "employee",
						excerptProjection = EmployeeProjection.class)
public interface EmployeeDao extends JpaRepository<Employee, Long> {
	
	List<Employee> findByCompany(@Param("company") Company company);
	
	@Query("from Employee c where "
			+ "c.company.id = ?1")
			Iterable<Employee> findAllByCompany(@Param("company") Integer company);
	
	@Query("from Employee c where "
			+ "c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "and c.position.id = ?3 "
			+ "and c.party.lastname = ?4 "
			+ "and c.party.firstname = ?5 "
			+ "and c.party.middlename = ?6 ")
			List<Employee> findByFIO(@Param("cid") Integer cid,
					@Param("bid") Integer bid,
					@Param("pos") Integer pos,
					@Param("last") String last,
					@Param("first") String first, 
					@Param("middle") String middle);
	
	@Query("from Employee c where "
			+ "c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "and c.position.id = ?3 "
			+ "and c.party.lastname = ?4 "
			+ "and c.party.firstname = ?5 "
			+ "and c.party.middlename is null")
	List<Employee> findByFI(@Param("cid") Integer cid,
					@Param("bid") Integer bid,
					@Param("pos") Integer pos,
					@Param("last") String last,
					@Param("first") String first);
	
	@Query("from Employee c where "
			+ "c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "and c.position.id = ?3 "
			+ "and c.party.firstname = ?4 "
			+ "and c.party.lastname is null "
			+ "and c.party.middlename is null")
	List<Employee> findByF(@Param("cid") Integer cid,
					@Param("bid") Integer bid,
					@Param("pos") Integer pos,
					@Param("first") String first);
	
	@Query("from Employee c where "
			+ "c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "and c.position.id = ?3 "
			+ "and c.party.firstname is null "
			+ "and c.party.lastname = ?4 "
			+ "and c.party.middlename is null")
	List<Employee> findByL(@Param("cid") Integer cid,
					@Param("bid") Integer bid,
					@Param("pos") Integer pos,
					@Param("last") String last);
	
	@Query(value = "Select * from Employee c where "
			+ "c.region = ?1 "
			+ "and c.position_id = ?2 "
			+ "limit 1",
			nativeQuery=true)
			Employee findByRegionPos(@Param("rid") Integer rid, 
									 @Param("pid") Integer pid);
	
	@Query(value = "Select * from Employee c where "
			+ "c.branch_id = ?1 "
			+ "and c.position_id = ?2 ",
			nativeQuery=true)
			List<Employee> findByBrPos(@Param("bid") Integer bid, 
									       @Param("pid") Integer pid);
	
	
	@Query(value = "Select count(c.id) from Employee c where "
			+ "c.branch_id = ?1 "
			+ "and c.position_id = ?2 "
			+ "and ?3 between c.date_hired and COALESCE(c.date_fired, '2100-12-12')",
			nativeQuery=true)
			int numberByBrPos(@Param("bid") Integer bid, 
									       @Param("pid") Integer pid,
									       @Param("dt") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt);
	
	@Query("from Employee c where "
			+ "c.party.id = ?1")
			List<Employee> findAllByParty(@Param("pid") Long pid);
	
	@Query("from Employee c where "
			+ "c.party.id = ?1 "
			+ "and c.branch.id = ?2")
			List<Employee> findAllByPartyAndBranch(
					@Param("pid") Long pid,
					@Param("pid") Integer bid
			);
	
	@Query("from Employee c where "
			+ "c.id = ?1")
			Employee findById(@Param("eid") Long eid);
	
	@Query(value = "Select * from Employee c where "
			+ "c.company_id = ?1 "
			+ "and c.branch_id = ?2 "
			+ "and c.position_id = ?3 "
			+ "and c.department = ?4 "
			+ "and c.date_fired is null",
			nativeQuery = true)
	List<Employee> findAllByComBrDep(
			@Param("company") Integer company,
			@Param("branch") Integer branch,
			@Param("position") Integer position,
			@Param("department") Integer department);
	
	@Query(value = "Select * from Employee c where "
			+ "c.company_id = ?1 "
			+ "and c.branch_id = ?2 "
			+ "and c.position_id = ?3 "
			+ "and c.date_fired is null",
			nativeQuery = true)
	List<Employee> findAllByComBr(
			@Param("company") Integer company,
			@Param("branch") Integer branch,
			@Param("position") Integer position);

	@Query("from Employee c where "
			+ "c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "and c.dateFired is null "
			+ "and c.salary > 0")
	List<Employee> findAllAcutalByComBr(
			@Param("company") Integer company,
			@Param("branch") Integer branch
		);
	
	@Query("from Employee c where "
			+ "c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "and (c.accountableTo is null "
			+ "or not c.accountableTo.branch.id = ?2) "
			+ "and c.dateFired is null ")
	List<Employee> findAllEmployees(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid
		);
	
	@Query("from Employee c where "
			+ "c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "and c.dateFired is null ")
	List<Employee> findAllStaffByComBr(
			@Param("company") Integer company,
			@Param("branch") Integer branch
		);
	
	@Query("from Employee c where "
			+ "c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "and c.position.id = ?3 "
			+ "and coalesce(c.dateFired, '2099-12-31') >= ?4 " )
	List<Employee> findAllStaffByComBrPos(
			@Param("company") Integer company,
			@Param("branch") Integer branch,
			@Param("position") Integer position,
			@Param("dt") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt
		);
	
	@Query(value = "Select * from Employee c where "
			+ "c.branch_id = ?1 "
			+ "and c.date_fired is null "
			+ "and c.position_id = " + Position.POS_DIRECTOR
			+ " limit 1",
			nativeQuery = true)
	Employee findDirByBranch(
			@Param("branch") Integer branch
		);
	
	@Query(value = "Select * from Employee c where "
			+ "c.company_id = ?1 "
			+ "and c.position_id = " + Position.POS_MANAGER
			+ " and c.accountable_to = ?2 "
			+ "and coalesce(c.date_fired, '2099-12-31') >= ?3 "
			+ "group by c.party_id",
			nativeQuery = true)
	List<Employee> findManagerByDir(
			@Param("company") Integer company,
			@Param("director") Long director,
			@Param("ds") @DateTimeFormat(pattern = "yyyy-MM-dd") Date ds
		);
	

	@Query(value = "Select * from Employee c where "
			+ "c.company_id = ?1 "
			+ "and c.position_id = " + Position.POS_DEALER
			+ " and c.accountable_to = ?2 "
			+ "and coalesce(c.date_fired, '2099-12-31') >= ?3 "
			+ "group by c.party_id",
			nativeQuery = true)
	List<Employee> findDealerByManager(
			@Param("company") Integer company,
			@Param("manager") Long manager,
			@Param("ds") @DateTimeFormat(pattern = "yyyy-MM-dd") Date ds
		);
	
	@Query(value = "Select * from Employee c where "
			+ "c.company_id = ?1 "
			+ "and c.branch_id = ?2 "
			+ "and coalesce(c.date_fired, '2099-12-31') >= ?3 "
			+ "and c.position_id = " + Position.POS_MANAGER
			+ " GROUP BY c.party_id ",
			nativeQuery = true)
	List<Employee> findManagerByPos(
			@Param("company") Integer company,
			@Param("branch") Integer branch,
			@Param("ds") @DateTimeFormat(pattern = "yyyy-MM-dd") Date ds
		);
	
	@Query(value = "Select * from Employee e "
			+ "left join Contract c on c.manager = e.id "
			+ "where "
			+ "c.company = ?1 "
			+ "and c.branch = ?2 "
			+ "and c.date_signed between ?3 and ?4 "
			+ "GROUP BY e.party_id ",
			nativeQuery = true)
	List<Employee> findEmployeeByContract(
			@Param("company") Integer company,
			@Param("branch") Integer branch,
			@Param("ds") @DateTimeFormat(pattern = "yyyy-MM-dd") Date ds,
			@Param("de") @DateTimeFormat(pattern = "yyyy-MM-dd") Date de
		);
	
	@Query(value = "Select * from Employee c where "
			+ "c.company_id = ?1 "
			+ "and c.branch_id = ?2 "
			+ "and coalesce(c.date_fired, '2099-12-31') >= ?3 "
			+ "and c.position_id = " + Position.POS_DEALER
			+ " GROUP BY c.party_id ",
			nativeQuery = true)
	List<Employee> findAllDealerByCid(
			@Param("company") Integer company,
			@Param("branch") Integer branch,
			@Param("ds") @DateTimeFormat(pattern = "yyyy-MM-dd") Date ds
			
		);
	
	@Query(value = "Select e.* " + 
			"from Employee e " + 
			"left join region_has_branch rb on rb.branch_id = ?1 " + 
			"left join region r on rb.region_id = r.id " + 
			"left join party p on p.id = e.party_id " + 
			"where " + 
			"e.region = r.id " + 
			"and r.department = ?2 " + 
			"and coalesce(e.date_fired, '2099-12-31') >= ?3 " + 
			"and e.position_id = " + Position.POS_COORDINATOR +
			" limit 1",
			nativeQuery=true)
	Employee getCoordinatorByBranchDep(
			@Param("branch") Integer branch,
			@Param("dep") Integer dep,
			@Param("de") @DateTimeFormat(pattern = "yyyy-MM-dd") Date de
		);
	
	@Query(value = "Select count(c.id) from Employee c where "
			+ "c.party_id = ?1 "
			+ "and c.date_fired is null",
			nativeQuery=true)
	int numberOfEmployee(@Param("pid") Long pid);

	
	// ***********************************************************************************

	
	@Query(value = "Select * from Employee c where "
			+ "c.company_id = :cid "
			+ "and c.date_fired is null "
			+ "GROUP BY c.id "
			+ "ORDER BY ?#{#pageable}",
			nativeQuery=true)
	Page<Employee> getAllEmployeesCom(
			@Param("cid") Integer cid,
			Pageable pageable
		);
	
	@Query(value = "Select * from Employee c where "
			+ "c.company_id = :cid "
			+ "and c.date_fired is null "
			+ "and c.branch_id = :bid "
			+ "GROUP BY c.id "
			+ "ORDER BY ?#{#pageable}",
			nativeQuery=true)
	Page<Employee> getAllEmployeesComBr(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			Pageable pageable
		);
	
	@Query(value = "Select * from Employee c where "
			+ "c.company_id = :cid "
			+ "and c.branch_id = :bid "
			+ "and c.date_fired is null "
			+ "and c.position_id = :pos " 
			+ "GROUP BY c.id "
			+ "ORDER BY ?#{#pageable}", nativeQuery = true)
	Page<Employee> getAllEmployeesComBrPos(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("pos") Integer pos,
			Pageable pageable
		);
	
	@Query(value = "Select * from Employee c where "
			+ "c.company_id = :cid "
			+ "and c.date_fired is null "
			+ "and c.position_id = :pos " 
			+ "GROUP BY c.id "
			+ "ORDER BY ?#{#pageable}", nativeQuery = true)
	Page<Employee> getAllEmployeesComPos(
			@Param("cid") Integer cid,
			@Param("pos") Integer pos,
			Pageable pageable
		);
	
	
	@Query(value = "Select * from Employee e " 
			+ "where e.company_id = :cid "
			+ "and e.branch_id = :bid "
			+ "and e.date_hired <= :date "
			+ "and (e.date_fired is null "
			+ "or e.date_fired >= :date) "
			+ "and (e.accountable_to is null " 
			+ "or exists (Select a.branch_id from Employee a " 
			+ "where a.id = e.accountable_to " 
			+ "and a.branch_id <> e.branch_id) ) "
			+ "ORDER BY ?#{#pageable}", nativeQuery = true)
	Page<Employee> hierarchyList(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date,
			Pageable pageable);
	
	@Query(value = "Select * from Employee e "
			+ "left join Contract c on c.manager = e.id " 
			+ "where e.company_id = :cid "
			+ "and e.branch_id = :bid "
			+ "and e.position_id = :pos "
			+ "and c.date_signed between :dateStart and :dateEnd "
			+ "group by c.manager "
			+ "ORDER BY ?#{#pageable}", nativeQuery = true)
	Page<Employee> findAllManager(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("pos") Integer pos,
			@Param("dateStart") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateStart,
			@Param("dateEnd") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateEnd,
			Pageable pageable);
	
	
	@Query(value = "Select * from Employee e "
			+ "join Contract c on c.manager = :emplId " 
			+ "where c.dealer = e.id "
			+ "and c.date_signed between :dateStart and :dateEnd "
			+ "group by e.id",
			nativeQuery = true)
	List<Employee> findAllDealerByManager(
			@Param("emplId") Long emplId,
			@Param("dateStart") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateStart,
			@Param("dateEnd") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateEnd);
	
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "left join phone_number f on f.party_id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.branch_id = :bid "
			+ "and "
			+ "(p.firstname like %:filter% "
			+ "or p.lastname like %:filter% "
			+ "or p.iin_bin like %:filter% "
			+ "or p.middlename like %:filter% "
			+ "or p.email like %:filter% "
			+ "or p.passport_number like %:filter% "
			+ "or p.iik like %:filter% "
			+ "or p.bik like %:filter% "
			+ "or p.company_name like %:filter% "
			+ "or p.old_id like %:filter% "
			+ "or f.number like %:filter% "
			+ ") "
			+ "and e.date_hired <= :date "
			+ "and (e.date_fired is null "
			+ "or e.date_fired >= :date) " 
			+ "and (e.accountable_to is null " 
			+ "or exists (Select a.branch_id from Employee a " 
			+ "where a.id = e.accountable_to " 
			+ "and a.branch_id <> e.branch_id) ) "
			+ "group by e.party_id "
			+ "ORDER BY ?#{#pageable}", nativeQuery = true)
	Page<Employee> hierarchyListFilter(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("filter") String filter,
			@Param("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date,
			Pageable pageable);
	
	
	@Query(value = "Select * from Employee e " 
			+ "where e.accountable_to = :emplId "
			+ "and coalesce(e.date_fired, '2100-12-20') >= :date "
			+ "and e.date_hired <= :date"
			, nativeQuery = true)
	List<Employee> findAllChild(
			@Param("emplId") Integer emplId,
			@Param("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date);
	
	// Filters
	@Query(value = "Select * from Employee c "
			+ "left join party p on p.id = c.party_id "
			+ "left join phone_number f on f.party_id = c.party_id "
			+ "where c.company_id = :cid "
			+ "and c.date_fired is null "
			+ "and "
			+ "(p.firstname like %:filter% "
			+ "or p.lastname like %:filter% "
			+ "or p.middlename like %:filter% "
			+ "or p.iin_bin like %:filter% "
			+ "or p.company_name like %:filter% "
			+ "or p.passport_number like %:filter% "
			+ "or p.iik like %:filter% "
			+ "or p.bik like %:filter% "
			+ "or f.number like %:filter% "
			+ ") "
			+ "GROUP BY p.id "
			+ "ORDER BY ?#{#pageable}", 
			nativeQuery = true)
	Page<Employee> getAllEmployeesFilterCom(
			@Param("cid") Integer cid,
			@Param("filter") String filter,
			Pageable pageable
		);
	
	@Query(value = "Select * from Employee c "
			+ "left join party p on p.id = c.party_id "
			+ "left join phone_number f on f.party_id = c.party_id "
			+ "where c.company_id = :cid "
			+ "and c.branch_id = :bid "
			+ "and c.date_fired is null "
			+ "and "
			+ "(p.firstname like %:filter% "
			+ "or p.lastname like %:filter% "
			+ "or p.middlename like %:filter% "
			+ "or p.iin_bin like %:filter% "
			+ "or p.company_name like %:filter% "
			+ "or p.passport_number like %:filter% "
			+ "or p.iik like %:filter% "
			+ "or p.bik like %:filter% "
			+ "or f.number like %:filter% "
			+ ") "
			+ "GROUP BY p.id "
			+ "ORDER BY ?#{#pageable}", 
			nativeQuery = true)
	Page<Employee> getAllEmployeesFilterComBr(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("filter") String filter,
			Pageable pageable
		);
	
	@Query(value = "Select * from Employee c "
			+ "left join party p on p.id = c.party_id "
			+ "left join phone_number f on f.party_id = c.party_id "
			+ "where c.company_id = :cid "
			+ "and c.branch_id = :bid "
			+ "and c.date_fired is null "
			+ "and c.position_id = :pos "
			+ "and "
			+ "(p.firstname like %:filter% "
			+ "or p.lastname like %:filter% "
			+ "or p.middlename like %:filter% "
			+ "or p.iin_bin like %:filter% "
			+ "or p.company_name like %:filter% "
			+ "or p.passport_number like %:filter% "
			+ "or p.iik like %:filter% "
			+ "or p.bik like %:filter% "
			+ "or f.number like %:filter% "
			+ ") "
			+ "GROUP BY p.id "
			+ "ORDER BY ?#{#pageable}", 
			nativeQuery = true)
	Page<Employee> getAllEmployeesFilterComBrPos(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("pos") Integer pos,
			@Param("filter") String filter,
			Pageable pageable
		);
	
	@Query(value = "Select * from Employee c "
			+ "left join party p on p.id = c.party_id "
			+ "left join phone_number f on f.party_id = c.party_id "
			+ "where c.company_id = :cid "
			+ "and c.date_fired is null "
			+ "and c.position_id = :pos "
			+ "and "
			+ "(p.firstname like %:filter% "
			+ "or p.lastname like %:filter% "
			+ "or p.middlename like %:filter% "
			+ "or p.iin_bin like %:filter% "
			+ "or p.company_name like %:filter% "
			+ "or p.passport_number like %:filter% "
			+ "or p.iik like %:filter% "
			+ "or p.bik like %:filter% "
			+ "or f.number like %:filter% "
			+ ") "
			+ "GROUP BY p.id "
			+ "ORDER BY ?#{#pageable}", 
			nativeQuery = true)
	Page<Employee> getAllEmployeesFilterComPos(
			@Param("cid") Integer cid,
			@Param("pos") Integer pos,
			@Param("filter") String filter,
			Pageable pageable
		);
	@Query(value = "Select * from Employee c where "
			+ "c.party_id = ?1 "
			+ "and c.company_id = ?2 "
			+ "and c.branch_id = ?3 "
			+ "and c.department = ?4 "
			+ "and c.position_id = ?5 "
			+ "and c.date_fired is null",
			nativeQuery = true)
	List<Employee> getSameEmpl(
			@Param("empId") Long empId,
			@Param("cId") Integer cId,
			@Param("brId") Integer brId,
			@Param("depId") Integer depId,
			@Param("posId") Integer posId
		);
	
	//***********************************************************
	
	//FILTER
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "left join phone_number f on f.party_id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and "
			+ "(p.firstname like %:filter% "
			+ "or p.lastname like %:filter% "
			+ "or p.iin_bin like %:filter% "
			+ "or p.middlename like %:filter% "
			+ "or p.email like %:filter% "
			+ "or p.passport_number like %:filter% "
			+ "or p.iik like %:filter% "
			+ "or p.bik like %:filter% "
			+ "or p.company_name like %:filter% "
			+ "or p.old_id like %:filter% "
			+ "or f.number like %:filter% "
			+ ") "
			+ "group by e.party_id", 
			nativeQuery = true)
	List<Employee> filterEmplByCid(
			@Param("cid") Integer cid,
			@Param("filter") String filter);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "left join phone_number f on f.party_id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and "
			+ "(p.firstname like %:filter% "
			+ "or p.lastname like %:filter% "
			+ "or p.iin_bin like %:filter% "
			+ "or p.middlename like %:filter% "
			+ "or p.email like %:filter% "
			+ "or p.passport_number like %:filter% "
			+ "or p.iik like %:filter% "
			+ "or p.bik like %:filter% "
			+ "or p.company_name like %:filter% "
			+ "or p.old_id like %:filter% "
			+ "or f.number like %:filter% "
			+ ") "
			+ "and e.date_fired is null "
			+ "group by e.party_id", 
			nativeQuery = true)
	List<Employee> filterEmplByCidFired(
			@Param("cid") Integer cid,
			@Param("filter") String filter);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "left join phone_number f on f.party_id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.branch_id = :bid "
			+ "and "
			+ "(p.firstname like %:filter% "
			+ "or p.lastname like %:filter% "
			+ "or p.iin_bin like %:filter% "
			+ "or p.middlename like %:filter% "
			+ "or p.email like %:filter% "
			+ "or p.passport_number like %:filter% "
			+ "or p.iik like %:filter% "
			+ "or p.bik like %:filter% "
			+ "or p.company_name like %:filter% "
			+ "or p.old_id like %:filter% "
			+ "or f.number like %:filter% "
			+ ") "
			+ "group by e.party_id", 
			nativeQuery = true)
	List<Employee> filterEmplByCidBid(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("filter") String filter);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "left join phone_number f on f.party_id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.branch_id = :bid "
			+ "and "
			+ "(p.firstname like %:filter% "
			+ "or p.lastname like %:filter% "
			+ "or p.iin_bin like %:filter% "
			+ "or p.middlename like %:filter% "
			+ "or p.email like %:filter% "
			+ "or p.passport_number like %:filter% "
			+ "or p.iik like %:filter% "
			+ "or p.bik like %:filter% "
			+ "or p.company_name like %:filter% "
			+ "or p.old_id like %:filter% "
			+ "or f.number like %:filter% "
			+ ") "
			+ "and e.date_fired is null "
			+ "group by e.party_id", 
			nativeQuery = true)
	List<Employee> filterEmplByCidBidFired(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("filter") String filter);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "left join phone_number f on f.party_id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.position_id = :posId "
			+ "and "
			+ "(p.firstname like %:filter% "
			+ "or p.lastname like %:filter% "
			+ "or p.iin_bin like %:filter% "
			+ "or p.middlename like %:filter% "
			+ "or p.email like %:filter% "
			+ "or p.passport_number like %:filter% "
			+ "or p.iik like %:filter% "
			+ "or p.bik like %:filter% "
			+ "or p.company_name like %:filter% "
			+ "or p.old_id like %:filter% "
			+ "or f.number like %:filter% "
			+ ") "
			+ "group by e.party_id", 
			nativeQuery = true)
	List<Employee> filterEmplByCidPos(
			@Param("cid") Integer cid,
			@Param("posId") Integer posId,
			@Param("filter") String filter);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "left join phone_number f on f.party_id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.position_id = :posId "
			+ "and "
			+ "(p.firstname like %:filter% "
			+ "or p.lastname like %:filter% "
			+ "or p.iin_bin like %:filter% "
			+ "or p.middlename like %:filter% "
			+ "or p.email like %:filter% "
			+ "or p.passport_number like %:filter% "
			+ "or p.iik like %:filter% "
			+ "or p.bik like %:filter% "
			+ "or p.company_name like %:filter% "
			+ "or p.old_id like %:filter% "
			+ "or f.number like %:filter% "
			+ ") "
			+ "and e.date_fired is null"
			+ "group by e.party_id", 
			nativeQuery = true)
	List<Employee> filterEmplByCidPosFired(
			@Param("cid") Integer cid,
			@Param("posId") Integer posId,
			@Param("filter") String filter);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "left join phone_number f on f.party_id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.branch_id = :bid "
			+ "and e.position_id = :posId "
			+ "and "
			+ "(p.firstname like %:filter% "
			+ "or p.lastname like %:filter% "
			+ "or p.iin_bin like %:filter% "
			+ "or p.middlename like %:filter% "
			+ "or p.email like %:filter% "
			+ "or p.passport_number like %:filter% "
			+ "or p.iik like %:filter% "
			+ "or p.bik like %:filter% "
			+ "or p.company_name like %:filter% "
			+ "or p.old_id like %:filter% "
			+ "or f.number like %:filter% "
			+ ") "
			+ "group by e.party_id", 
			nativeQuery = true)
	List<Employee> filterEmplByCidBidPos(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("posId") Integer posId,
			@Param("filter") String filter);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "left join phone_number f on f.party_id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.branch_id = :bid "
			+ "and e.position_id = :posId "
			+ "and "
			+ "(p.firstname like %:filter% "
			+ "or p.lastname like %:filter% "
			+ "or p.iin_bin like %:filter% "
			+ "or p.middlename like %:filter% "
			+ "or p.email like %:filter% "
			+ "or p.passport_number like %:filter% "
			+ "or p.iik like %:filter% "
			+ "or p.bik like %:filter% "
			+ "or p.company_name like %:filter% "
			+ "or p.old_id like %:filter% "
			+ "or f.number like %:filter% "
			+ ") "
			+ "and e.date_fired is null "
			+ "group by e.party_id", 
			nativeQuery = true)
	List<Employee> filterEmplByCidBidPosFired(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("posId") Integer posId,
			@Param("filter") String filter);
	
	
	// SORT BY FIRSTNAME
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "group by e.party_id "
			+ "order by p.firstname", 
			nativeQuery = true)
	List<Employee> sortEmplByFirstnameCid(
			@Param("cid") Integer cid);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.date_fired is null "
			+ "group by e.party_id "
			+ "order by p.firstname", 
			nativeQuery = true)
	List<Employee> sortEmplByFirstnameCidFired(
			@Param("cid") Integer cid);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.branch_id = :bid "
			+ "group by e.party_id "
			+ "order by p.firstname", 
			nativeQuery = true)
	List<Employee> sortEmplByFirstnameCidBid(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.branch_id = :bid "
			+ "and e.date_fired is null "
			+ "group by e.party_id "
			+ "order by p.firstname", 
			nativeQuery = true)
	List<Employee> sortEmplByFirstnameCidBidFired(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.position_id = :posId "
			+ "group by e.party_id "
			+ "order by p.firstname", 
			nativeQuery = true)
	List<Employee> sortEmplByFirstnameCidPos(
			@Param("cid") Integer cid,
			@Param("posId") Integer posId);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.position_id = :posId "
			+ "and e.date_fired is null "
			+ "group by e.party_id "
			+ "order by p.firstname", 
			nativeQuery = true)
	List<Employee> sortEmplByFirstnameCidPosFired(
			@Param("cid") Integer cid,
			@Param("posId") Integer posId);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.branch_id = :bid "
			+ "and e.position_id = :posId "
			+ "group by e.party_id "
			+ "order by p.firstname", 
			nativeQuery = true)
	List<Employee> sortEmplByFirstnameCidBidPos(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("posId") Integer posId);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.branch_id = :bid "
			+ "and e.position_id = :posId "
			+ "and e.date_fired is null "
			+ "group by e.party_id "
			+ "order by p.firstname", 
			nativeQuery = true)
	List<Employee> sortEmplByFirstnameCidBidPosFired(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("posId") Integer posId);
	
	// FILTER BY LASTNAME
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "group by e.party_id "
			+ "order by p.lastname", 
			nativeQuery = true)
	List<Employee> sortEmplByLastnameCid(
			@Param("cid") Integer cid);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.date_fired is null "
			+ "group by e.party_id "
			+ "order by p.lastname", 
			nativeQuery = true)
	List<Employee> sortEmplByLastnameCidFired(
			@Param("cid") Integer cid);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.branch_id = :bid "
			+ "group by e.party_id "
			+ "order by p.lastname", 
			nativeQuery = true)
	List<Employee> sortEmplByLastnameCidBid(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.branch_id = :bid "
			+ "and e.date_fired is null "
			+ "group by e.party_id "
			+ "order by p.lastname", 
			nativeQuery = true)
	List<Employee> sortEmplByLastnameCidBidFired(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.position_id = :posId "
			+ "group by e.party_id "
			+ "order by p.lastname", 
			nativeQuery = true)
	List<Employee> sortEmplByLastnameCidPos(
			@Param("cid") Integer cid,
			@Param("posId") Integer posId);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.position_id = :posId "
			+ "and e.date_fired is null "
			+ "group by e.party_id "
			+ "order by p.lastname", 
			nativeQuery = true)
	List<Employee> sortEmplByLastnameCidPosFired(
			@Param("cid") Integer cid,
			@Param("posId") Integer posId);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.branch_id = :bid "
			+ "and e.position_id = :posId "
			+ "group by e.party_id "
			+ "order by p.lastname", 
			nativeQuery = true)
	List<Employee> sortEmplByLastnameCidBidPos(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("posId") Integer posId);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.branch_id = :bid "
			+ "and e.position_id = :posId "
			+ "and e.date_fired is null "
			+ "group by e.party_id "
			+ "order by p.lastname", 
			nativeQuery = true)
	List<Employee> sortEmplByLastnameCidBidPosFired(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("posId") Integer posId);
	
	// GET EMPLOYEE LIST
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "group by e.party_id ",
			nativeQuery = true)
	List<Employee> getEmplByCid(
			@Param("cid") Integer cid);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.date_fired is null "
			+ "group by e.party_id ",
			nativeQuery = true)
	List<Employee> getEmplByCidFired(
			@Param("cid") Integer cid);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.branch_id = :bid "
			+ "group by e.party_id ",
			nativeQuery = true)
	List<Employee> getEmplByCidBid(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.branch_id = :bid "
			+ "and e.date_fired is null "
			+ "group by e.party_id ",
			nativeQuery = true)
	List<Employee> getEmplByCidBidFired(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.position_id = :posId "
			+ "group by e.party_id ", 
			nativeQuery = true)
	List<Employee> getEmplByCidPos(
			@Param("cid") Integer cid,
			@Param("posId") Integer posId);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.position_id = :posId "
			+ "and e.date_fired is null "
			+ "group by e.party_id ", 
			nativeQuery = true)
	List<Employee> getEmplByCidPosFired(
			@Param("cid") Integer cid,
			@Param("posId") Integer posId);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.branch_id = :bid "
			+ "and e.position_id = :posId "
			+ "group by e.party_id ", 
			nativeQuery = true)
	List<Employee> getEmplByCidBidPos(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("posId") Integer posId);
	
	@Query(value = "Select * from Employee e " 
			+ "left join party p on p.id = e.party_id "
			+ "where e.company_id = :cid "
			+ "and e.branch_id = :bid "
			+ "and e.position_id = :posId "
			+ "and e.date_fired is null "
			+ "group by e.party_id ", 
			nativeQuery = true)
	List<Employee> getEmplByCidBidPosFired(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("posId") Integer posId);
	
	@Query(value = "Select * from Employee c where "
			+ "c.party_id = ?1 "
			+ "and c.date_fired is null "
			+ "and c.position_id = " + Position.POS_CAREMAN
			+ " limit 1",
			nativeQuery = true)
	Employee findCaremanByPid(
			@Param("party") Integer party
		);
	
	@Query(value = "Select * from Employee c where "
			+ "c.party_id = ?1 "
			+ "and c.date_fired is null "
			+ "and c.position_id = " + Position.POS_COLLECTOR
			+ " limit 1",
			nativeQuery = true)
	Employee findCollectorByPid(
			@Param("party") Integer party
		);
	
}
