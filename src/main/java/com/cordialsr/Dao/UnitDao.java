package com.cordialsr.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.Unit;

@RepositoryRestResource(collectionResourceRel = "unit", path = "unit")
public interface UnitDao extends CrudRepository<Unit, String>{

}
