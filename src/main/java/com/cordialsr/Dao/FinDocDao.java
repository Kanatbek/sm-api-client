package com.cordialsr.Dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.format.annotation.DateTimeFormat;

import com.cordialsr.domain.Contract;
import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.FinDocType;
import com.cordialsr.domain.projection.FinDocProjectionList;

@RepositoryRestResource(collectionResourceRel = "findoc", path = "findoc",
						excerptProjection = FinDocProjectionList.class)
public interface FinDocDao extends JpaRepository<FinDoc, Long> {

	@Query(value = "Select * from Fin_doc c where c.id = ?1 LIMIT 1",
			nativeQuery = true)
	FinDoc findfd(@Param("id") Long id);

	List<FinDoc> findAllByContract(Contract contract);
	
	@Query(value = "select * from fin_doc f " + 
			"where " + 
			"f.contract_id = ?1 " +
			"and f.findoc_type in ('GR', 'GS', 'IR') " +
			"and (f.refkey = ?2 " + 
			"or f.refkey_main = ?2) " + 
			"and f.id > 0",
			nativeQuery = true)
	List<FinDoc> getStornoContractFinDocs(Long conId, String refkey);
	
	@Query(value = "select * from fin_doc f " +
			"left join payroll p on p.refkey = f.refkey " +
			"left join contract c on c.id = p.contract_id " + 
			"where " + 
			"p.sum < 0 " +
			"and f.refkey = ?2 " +
			"and p.contract_id = ?1",
			nativeQuery= true)
	List<FinDoc> getFinDocPayrollLess(Long conId, String refkey);
	
	@Query("from FinDoc c where "
			+ "c.invoice.id = ?1 "
			+ "and (c.finDocType.code = '" + FinDocType.TYPE_GS_GENERAL_SELL_CONTRACT 
			+ "' or c.finDocType.code = '" + FinDocType.TYPE_GR_GENERAL_RENT_CONTRACT  
			+ "') and c.contract.contractStatus.id <> 4 "
			+ "and c.storno = false")
	FinDoc findByInvoiceId(Long invoiceId);
	
	@Query("from FinDoc c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " ORDER BY c.docDate DESC")
	Page<FinDoc> getFinDocList(@Param("cid") Integer cid,
			@Param("bid") Integer bid, 
			Pageable pageRequest);
	
	@Query("from FinDoc c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and (c.party.iinBin like %?3% "
			+ " or c.party.firstname like %?3% "
			+ " or c.party.middlename like %?3% "
			+ " or c.party.lastname like %?3% "
			+ " or c.refkey like %?3% "
			+ " or c.title like %?3% "
			+ " or c.info like %?3% "
			+ " or c.wcurrency.currency like %?3% "
			+ " or c.user.firstName like %?3% "
			+ " or c.user.lastName like %?3%) "
			+ " ORDER BY c.docDate DESC")
	Page<FinDoc> filterCidBid(@Param("cid") Integer cid,
			@Param("bid") Integer bid, 
			@Param("filter") String filter,
			Pageable pageRequest);
	
	@Query("from FinDoc c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " ORDER BY c.docDate DESC")
	Page<FinDoc> getFinDocListForKb(@Param("cid") Integer cid,
			@Param("bid") Integer bid, 
			Pageable pageRequest);
	
	
	@Query("from FinDoc c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and c.docDate >= ?3"			
			+ " ORDER BY c.docDate DESC")
	Page<FinDoc> getFinDocListFrom(@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("ds") Date ds,
			Pageable pageRequest);
	
	
	@Query("from FinDoc c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and c.docDate <= ?3"			
			+ " ORDER BY c.docDate DESC")
	Page<FinDoc> getFinDocListTill(@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("de") Date de,
			Pageable pageRequest);
	
	@Query("from FinDoc c "
			+ " where c.company.id = ?1"
			+ " and c.branch.id = ?2"
			+ " and c.docDate >= ?3"
			+ " and c.docDate <= ?4"
			+ " ORDER BY c.docDate DESC")
	Page<FinDoc> getFinDocListFromTill(@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("ds") Date ds,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("de") Date de,
			Pageable pageRequest);
	
	@Query("from FinDoc c " + 
			"where c.company.id = ?1 " + 
			"and c.branch.id = ?2 " + 
			"and c.party.id = ?3 " + 
			"and c.finDocType.pr = ?4 "
			+ "and c.wsumm > c.wsummPaid "
			+ "and c.storno = 0")
	Page<FinDoc> getActivePR(@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("pid") Long pid,
			@Param("pr") String pr,
			Pageable pageRequest);
	
	@Query(value = "SELECT * FROM fin_doc c " + 
			"where c.company = ?1 " + 
			"and c.branch = ?2 " + 
			"and (c.findoc_type = 'WI' " + 
			"or c.findoc_type = 'WO');",
			nativeQuery = true)
	Iterable<FinDoc> getFdListForStock(@Param("cid") Integer cid,
			@Param("bid") Integer bid);
	
	@RestResource(exported = false)
	@Query("from FinDoc c " + 
			"where c.contract.id = ?1 " + 
			"and c.finDocType.code in ('" + FinDocType.TYPE_GR_GENERAL_RENT_CONTRACT 
			+ "', '" + FinDocType.TYPE_GS_GENERAL_SELL_CONTRACT + "') " + 
			"and c.storno = 0")
	FinDoc getGsByContract(Long cid);
	
	@RestResource(exported = false)
	@Query("from FinDoc c " + 
			"where c.refkey = ?1 ")
	FinDoc findByRefkey(String refkey);	
	
	@RestResource(exported = false)
	@Query("from FinDoc c " +
			"where c.refkeyMain = ?1 " +
			"and c.finDocType.code = ?2 ")
	List<FinDoc> getFdByTypeAndRefkeyMain(String refkey, String fdType);
	
	@Query(value = "Select c.* from FIN_DOC c "
			+ " where c.company = ?1"
			+ " and c.branch = ?2"
			+ " and c.findoc_type = ?3 "  
			+ " and exists (" 
			+ "	select e.id from FIN_ENTRY e " 
			+ "    where e.fin_doc_id = c.id " 
			+ "    and e.gl_account = '3351'" 
			+ "    and e.dc = 'C'" 
			+ " )"
			+ " and c.doc_date >= ?4"
			+ " and c.doc_date <= ?5"
			+ " ORDER BY c.doc_date DESC",
			nativeQuery = true)
	List<FinDoc> getTempPayrollList(@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("fdtype") String fdtype,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("ds") Date ds,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("de") Date de);
	
	// ******************************************************************************************
	
	@RestResource(exported = false)
	@Query(value = "ALTER TABLE fin_doc AUTO_INCREMENT = 1",
			nativeQuery = true)
	void resetAutoIncrement();
}
