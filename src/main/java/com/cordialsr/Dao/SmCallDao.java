package com.cordialsr.Dao;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;

import com.cordialsr.domain.SmCall;
import com.cordialsr.domain.projection.SmCallProjection;

@RepositoryRestResource(collectionResourceRel = "smCall", path = "smCall",
							excerptProjection = SmCallProjection.class)
public interface SmCallDao extends CrudRepository<SmCall, Integer>{

	@Query("from SmCall c where c.id = ?1")
	SmCall findById(@Param("id") Integer id);
	
	@Query("from SmCall c " + 
			"where c.company.id = ?1 " + 
			"and c.branch.id = ?2 " +
			"ORDER BY id DESC")
	Page<SmCall> findAllByBranch(@Param("cid") Integer cid,
									  @Param("bid") Integer bid,
									  Pageable pageRequest);

	@Query("from SmCall c " + 
			"where c.company.id = ?1 " + 
			"and c.branch.id = ?2 " +
			"and c.callDate >= ?3 " +
			"ORDER BY id DESC")
	Page<SmCall> findAllByBranchFrom(@Param("cid") Integer cid,
   									     @Param("bid") Integer bid,
 @DateTimeFormat(pattern = "yyyy-MM-dd") @Param("from") Date from,
 											Pageable pageRequest);
	
	@Query("from SmCall c " + 
			"where c.company.id = ?1 " + 
			"and c.branch.id = ?2 " +
			"and c.callDate <= ?3 " +
			"ORDER BY id DESC")
	Page<SmCall> findAllByBranchTo(@Param("cid") Integer cid,
									   @Param("bid") Integer bid,
@DateTimeFormat(pattern = "yyyy-MM-dd")@Param("to") Date to,
										Pageable pageRequest);
	
	@Query("from SmCall c " + 
			"where c.company.id = ?1 " + 
			"and c.branch.id = ?2 " +
			"and c.callDate between ?3 and ?4 " +
			"ORDER BY id DESC")
	Page<SmCall> findAllByBranchFromTo(@Param("cid") Integer cid,
									  @Param("bid") Integer bid,
@DateTimeFormat(pattern = "yyyy-MM-dd")@Param("from") Date from,
@DateTimeFormat(pattern = "yyyy-MM-dd")@Param("to") Date to,
											Pageable pageRequest);
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Query(value = "SELECT user, company, branch, contract_id, service_id, party, phone_number, call_date, call_time, info, responder, status, is_outgoing, call_number, "
			+ "count(*) as id "
			+ "from sm_call s "
			+ "where s.company = ?1 "
			+ "and s.call_date between ?2 and ?3 "
			+ "group by s.user",
			nativeQuery = true)
	ArrayList<SmCall> findAllForReport(@Param("cid") Integer cid,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("from") Date from,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("to") Date to);
}
