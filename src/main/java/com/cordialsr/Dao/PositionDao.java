package com.cordialsr.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.Position;

@RepositoryRestResource(collectionResourceRel = "position", path = "position")
public interface PositionDao extends CrudRepository<Position, Integer>{

}
