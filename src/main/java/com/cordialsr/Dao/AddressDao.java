package com.cordialsr.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.Address;
import com.cordialsr.domain.projection.AddressInlineProjection;

@RepositoryRestResource(collectionResourceRel = "address", path = "address",
						excerptProjection = AddressInlineProjection.class)
public interface AddressDao extends CrudRepository<Address, Long>{

}
