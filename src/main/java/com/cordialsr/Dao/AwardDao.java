package com.cordialsr.Dao;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;

import com.cordialsr.domain.Award;
import com.cordialsr.domain.projection.AwardProjection;

@RepositoryRestResource(collectionResourceRel = "award", path = "award",
						excerptProjection = AwardProjection.class)
public interface AwardDao extends JpaRepository<Award, Integer> {
	
	@Query("from Award c where "
			+ "c.id = ?1")
			Award findById(@Param("awId") Integer awId);
	
	@Query("from Award c where "
			+ "c.position.id = ?1")
			Iterable<Award> findAllByPosition(@Param("position") Integer position);
	
	@Query(value = "Select * from award c "
			+ "where "
			+ "c.award_type = 'PREMI' "
			+ "and c.award_case = 'TERM' "
			+ "and c.company = ?1 "
			+ "and c.department = ?2 "
			+ "and c.position = ?3 "
			+ "and ("
			+ " (c.scope = 'BRANCH' and c.branch = ?4) "
			+ " or (c.scope = 'CITY' and c.city = ?5) "
			+ " or (c.scope = 'COUNTRY' and c.country = ?6) "
			+ " or (c.scope = 'REGION' and c.region = ?7) "
			+ ") "
			+ " and ("
			+ "if (?9 is null, c.inventory_id is null, c.inventory_id = ?9) "
			+ "and c.inv_sub_category = ?8"
			+ ") "
			+ "and c.date_start <= ?10 "
			+ "and coalesce(c.date_end, '2099-12-31') > ?10 "
			+ "and c.enabled = 1 "
			+ "and c.min <= ?11 "
			+ "and c.max >= ?11 "
			+ "and c.for_sale = 1 "
			+ "order by c.branch desc, c.city desc, c.country desc, c.region desc, c.date_start desc limit 1",
			nativeQuery = true)
			Award findSalesPremiByScopePriority(
					@Param("cid") Integer cid,
					@Param("did") Integer did,
					@Param("pos") Integer pos,
					@Param("brid") Integer brid,
					@Param("city") Integer city,
					@Param("cnid") Integer cnid,
					@Param("reg") Integer reg,
					@Param("subcat") Integer subcat,
					@Param("invid") Integer invid,
					@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("dt") Date dt,
					@Param("month") Integer month
				);
	
	@Query(value = "Select * from award c "
			+ "where "
			+ "c.award_type = 'PREMI' "
			+ "and c.award_case = 'TERM' "
			+ "and c.company = ?1 "
			+ "and c.department = ?2 "
			+ "and c.position = ?3 "
			+ "and ("
			+ " (c.scope = 'BRANCH' and c.branch = ?4) "
			+ " or (c.scope = 'CITY' and c.city = ?5) "
			+ " or (c.scope = 'COUNTRY' and c.country = ?6) "
			+ " or (c.scope = 'REGION' and c.region = ?7) "
			+ ") "
			+ " and ("
			+ "if (?9 is null, c.inventory_id is null, c.inventory_id = ?9) "
			+ "and c.inv_sub_category = ?8"
			+ ") "
			+ "and c.date_start <= ?10 "
			+ "and coalesce(c.date_end, '2099-12-31') > ?10 "
			+ "and c.enabled = 1 "
			+ "and c.min <= ?11 "
			+ "and c.max >= ?11 "
			+ "and c.for_rent = 1 "
			+ "order by c.branch desc, c.city desc, c.country desc, c.region desc, c.date_start desc limit 1",
			nativeQuery = true)
			Award findRentPremiByScopePriority(
					@Param("cid") Integer cid,
					@Param("did") Integer did,
					@Param("pos") Integer pos,
					@Param("brid") Integer brid,
					@Param("city") Integer city,
					@Param("cnid") Integer cnid,
					@Param("reg") Integer reg,
					@Param("subcat") Integer subcat,
					@Param("invid") Integer invid,
					@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("dt") Date dt,
					@Param("month") Integer month
				);
	
	// **********************************************************************************************************
	
	@Query(value = "select a.* " + 
			"from award a " + 
			"where " + 
			"a.company = ?1 " + 
			"and a.country = ?2 " + 
			"and a.award_type = ?3 " + 
			"and a.position = ?4 " + 
			"and a.min <= ?5 " + 
			"and a.max >= ?5 " + 
			"and a.date_start <= ?6 " +
			"and coalesce(a.date_end, '2099-12-31') > ?6 " +
			"and a.enabled = 1 " +
			"order by a.date_start desc limit 1;",
			nativeQuery = true)
			Award getBonusRule(
					@Param("cid") Integer cid,
					@Param("cnt") Integer cnt,
					@Param("awt") String awt,
					@Param("pos") Integer pos,
					@Param("srt") Integer srt,
					@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("dt") Date dt
				);
	
	@Query(value = "select a.* " + 
			"from award a " + 
			"where " + 
			"a.company = ?1 " + 
			"and a.country = ?2 " + 
			"and a.award_type = ?3 " + 
			"and a.position = ?4 " + 
			"and a.min <= ?5 " + 
			"and a.max >= ?5 " + 
			"and a.for_rent = 1 " +
			"and a.date_start <= ?6 " +
			"and coalesce(a.date_end, '2099-12-31') > ?6 " +
			"and a.enabled = 1 " +
			"order by a.date_start desc limit 1;",
			nativeQuery = true)
			Award getBonusRuleForRent(
					@Param("cid") Integer cid,
					@Param("cnt") Integer cnt,
					@Param("awt") String awt,
					@Param("pos") Integer pos,
					@Param("srt") Integer srt,
					@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("dt") Date dt
				);
	
	@Query(value = "select a.* " + 
			"from award a " + 
			"where " + 
			"a.company = ?1 " + 
			"and a.country = ?2 " + 
			"and a.award_type = ?3 " + 
			"and a.position = ?4 " + 
			"and a.min <= ?5 " + 
			"and a.max >= ?5 " + 
			"and a.for_sale = 1 " +
			"and a.date_start <= ?6 " +
			"and coalesce(a.date_end, '2099-12-31') > ?6 " +
			"and a.enabled = 1 " +
			"order by a.date_start desc limit 1;",
			nativeQuery = true)
			Award getBonusRuleForSale(
					@Param("cid") Integer cid,
					@Param("cnt") Integer cnt,
					@Param("awt") String awt,
					@Param("pos") Integer pos,
					@Param("srt") Integer srt,
					@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("dt") Date dt
				);
		
	@Query(value = "Select * from award c "
			+ "where c.company = :cid "
			+ "and c.position = :posId "
			+ "and c.inv_main_category = :invId "
			+ "and c.for_sale = :forSales "
			+ "and c.for_rent = :forRent ",
			nativeQuery = true)
	Iterable<Award> loadAwardList(
			@Param("cid") Integer cid, 
			@Param("posId") Integer posId, 
			@Param("invId") Integer invId,
			@Param("forSales") Boolean forSales, 
			@Param("forRent") Boolean forRent);
	
	@Query(value = "Select * from award c "
			+ "where c.company = :cid "
			+ "and c.position = :posId "
			+ "and c.inv_main_category = :invId "
			+ "and c.for_sale = :forSales ",
			nativeQuery = true)
	Iterable<Award> loadAwardBySales(
			@Param("cid") Integer cid, 
			@Param("posId") Integer posId, 
			@Param("invId") Integer invId,
			@Param("forSales") Boolean forSales);
	
	@Query(value = "Select * from award c "
			+ "where c.company = :cid "
			+ "and c.position = :posId "
			+ "and c.inv_main_category = :invId "
			+ "and c.for_rent = :forRent ",
			nativeQuery = true)
	Iterable<Award> loadAwardByRent(
			@Param("cid") Integer cid, 
			@Param("posId") Integer posId, 
			@Param("invId") Integer invId,
			@Param("forRent") Boolean forRent);
	
	@Query(value = "Select * from award c "
			+ "where c.company = :cid "
			+ "and c.position = :posId "
			+ "and c.inv_main_category = :invId ",
			nativeQuery = true)
	Iterable<Award> loadAwardByPosInv(
			@Param("cid") Integer cid, 
			@Param("posId") Integer posId, 
			@Param("invId") Integer invId);
	
	@Query(value = "Select * from award c "
			+ "where c.company = :cid "
			+ "and c.position = :posId ",
			nativeQuery = true)
	Iterable<Award> loadAwardByPos(
			@Param("cid") Integer cid, 
			@Param("posId") Integer posId);
	
	@Query(value = "Select * from award c "
			+ "where c.company = :cid "
			+ "and c.inv_main_category = :invId ",
			nativeQuery = true)
	Iterable<Award> loadAwardByInv(
			@Param("cid") Integer cid, 
			@Param("invId") Integer invId);
	
	@Query(value = "Select * from award c "
			+ "where c.company = :cid ",
			nativeQuery = true)
	Iterable<Award> loadAwardByCom(
			@Param("cid") Integer cid);
	
	@Query("from Award c where "
			+ "("
			+ "c.scope.name like %?1% "
			+ "or c.company.name like %?1% "
			+ "or c.inventory.name like %?1% "
			+ "or c.inventory.model like %?1% "
			+ "or c.inventory.code like %?1% "
			
			+ "or c.awardCase.name like %?1% "
			+ "or c.awardCase.code like %?1% "
			
			+ "or c.awardType.name like %?1% "
			
			+ "or c.invMainCategory.name like %?1% "
			+ "or c.invSubCategory.name like %?1% "
			
			+ "or c.position.name like %?1% "
			
			+ "or c.department.name like %?1% "
			+ ")")
	Iterable<Award> filter( 
			@Param("filter") String filter);	
	
}
