package com.cordialsr.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.PartyRelatives;

@RepositoryRestResource(collectionResourceRel = "party_relative", path = "party_relative")
public interface PartyRelativesDao extends CrudRepository<PartyRelatives, Long>{

}
