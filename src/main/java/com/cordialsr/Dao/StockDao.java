package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.StockIn;
import com.cordialsr.domain.projection.StockProjection;

@RepositoryRestResource(collectionResourceRel = "stock", path = "stock",
							excerptProjection = StockProjection.class)
			public interface StockDao extends CrudRepository<StockIn, Long>{

	@Query(value = "SELECT * FROM stock_in c " + 
			"where c.company = ?1 " + 
			"and c.branch = ?2",
			nativeQuery = true)
	Iterable<StockIn> findListOfStock(@Param("cid") Integer cid,
			@Param("bid") Integer bid);
	
	
	// 7 - Retrieve available stock_in by inv
	@Query(value = "SELECT * "
			+ "from stock_in s "
			+ "where s.company = ?1 "
			+ "and s.branch = ?2 "
			+ "and s.inventory_id = ?4 "
			+ "and s.gen_status_id in (1, 4) "
			+ "and s.int_status_id = 101 "
			+ "and s.gl_code = ?3 "
			+ "Limit ?5",
			nativeQuery = true)
	Iterable<StockIn> getStockInByGlAndInv(@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("gl") String gl,
			@Param("inv") Integer inv,
			@Param("cnt") Integer cnt);
	
	// 8 - Also with Status ......................................
	
	@Query(value = "SELECT * "
			+ "from stock_in s "
			+ "where s.company = ?1 "
			+ "and s.branch = ?2 "
			+ "and s.inventory_id = ?4 "
			+ "and s.gen_status_id in (1, 4) "
			+ "and s.int_status_id = 101 "
			+ "and s.gl_code = ?3 "
			+ "and s.int_status_id = ?6 "
			+ "Limit ?5",
			nativeQuery = true)
	Iterable<StockIn> getStockInByGlAndInvSta(@Param("cid") Integer cid,
											  @Param("bid") Integer bid,
       										  @Param("gl") String gl,
											  @Param("inv") Integer inv,
											  @Param("cnt") Integer cnt,
											  @Param("sta") Integer sta);
	
	// 9 - Also with Status and Party ............................
	
	@Query(value = "SELECT s.* "
			+ "FROM invoice i " 
			+ "left join invoice_item ii on ii.invoice_id = i.id "  
			+ "left join stock_in s on s.invoice_item_id = ii.id "  
			+ "where  "  
			+ "s.company = ?1 "  
			+ "and s.branch = ?2 "
			+ "and s.gl_code = ?3 "
			+ "and s.gen_status_id in (1, 4) "
			+ "and s.inventory_id = ?4 "
			+ "and s.int_status_id = ?6 "
			+ "and i.party = ?7 "
			+ "Limit ?5",
			nativeQuery = true)
	Iterable<StockIn> getStockInByGlAndInvStaPrt(@Param("cid") Integer cid,
											  @Param("bid") Integer bid,
       										  @Param("gl") String gl,
											  @Param("inv") Integer inv,
											  @Param("cnt") Integer cnt,
											  @Param("sta") Integer sta,
											  @Param("prt") Integer prt);
	
	// ********************************************************************************
	
	@Query(value = "SELECT s.company, s.branch, s.inventory_id, "
			+ "s.gen_status_id, s.int_status_id, sum(s.quantity) as quantity, s.unit, s.date_in, "
			+ "s.id, s.dcurrency, s.wcurrency, s.dsumm, s.wsumm, s.serial_number, "
			+ "s.refkey, s.rate, s.invoice_item_id, gl_code "
			+ "from stock_in s "
			+ "where s.company = ?1 "
			+ "and s.branch = ?2 "
			+ "and s.gen_status_id in (1, 4) "
			+ "and s.int_status_id = 101 "
			+ "group by s.inventory_id, s.gen_status_id, s.gl_code",
			nativeQuery = true)
	Iterable<StockIn> getStockInByGl(@Param("cid") Integer cid,
			@Param("bid") Integer bid);
	//................................................................................
	
	
	@Query(value = "SELECT s.company, s.branch, s.inventory_id, "
			+ "s.gen_status_id, s.int_status_id, sum(s.quantity) as quantity, s.unit, s.date_in, "
			+ "s.id, s.dcurrency, s.wcurrency, s.dsumm, s.wsumm, s.serial_number, "
			+ "s.refkey, s.rate, s.invoice_item_id, gl_code "
			+ "from stock_in s "
			+ "where s.company = ?1 "
			+ "and s.branch = ?2 "
			+ "and s.inventory_id = ?3 "
			+ "and s.gen_status_id in (1, 4) "
			+ "and s.int_status_id = 101 "
			+ "group by s.inventory_id, s.gen_status_id, s.gl_code",
			nativeQuery = true)
	Iterable<StockIn> getStockInByGlInv(@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("inv") Integer inv);
	
	
	
	//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	

	@Query(value = "SELECT * from stock_in s "
			+ "where s.company = ?1 "
			+ "and s.branch = ?2 "
			+ "and s.inventory_id = ?3 "
			+ "and s.gen_status_id in (1, 4) "
			+ "and s.int_status_id = 101",
			nativeQuery = true)
	Iterable<StockIn> getStockInByGlByInv(@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("inv") Integer inv);
	
	// **************************************************************************
	
	@Query(value = "SELECT * FROM stock_in c "  
			+ "where c.company = ?1 "  
			+ "and c.branch = ?2 "
			+ "and c.inventory_id = ?3 "
			+ "and c.serial_number = ?4 "
			+ "LIMIT 1",
			nativeQuery = true)
	StockIn findBySerialNumber(@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("inv") Integer inv,
			@Param("ser_num") String ser_num);

	// ********************************************************************************

	@Query(value = "SELECT * FROM stock_in c "  
			+ "where c.serial_number = ?1 "
			+ "LIMIT 1",
			nativeQuery = true)
	StockIn findByJustSerialNumber(@Param("ser_num") String ser_num);

	
	// *********************************************************************************

	@Query(value = "SELECT  " + 
			"	s.* " + 
			"	FROM invoice i " + 
			"	left join invoice_item ii on ii.invoice_id = i.id " + 
			"	left join stock_in s on s.invoice_item_id = ii.id " + 
			"	where  " + 
			"	s.company = ?1 " + 
			"	and s.branch = ?2 " + 
			"	and i.party = ?3 " + 
			"	and s.int_status_id = 105",
			nativeQuery = true)
	Iterable<StockIn> getStaffAccountableStocks(@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("staff") Long staff);
	
	@Query(value = "SELECT  " + 
			"	s.* " + 
			"	FROM invoice i " + 
			"	left join invoice_item ii on ii.invoice_id = i.id " + 
			"	left join stock_in s on s.invoice_item_id = ii.id " + 
			"	where  " + 
			"	s.company = ?1 " + 
			"	and s.branch = ?2 " + 
			"	and i.party = ?3 " + 
			"	and s.inventory_id  = ?4 " + 
			"	and s.int_status_id = 105",
			nativeQuery = true)
	Iterable<StockIn> getStaffAccountableStocksByInv(@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("staff") Long staff,
			@Param("inv") Integer inv);
	
	@Query(value = "SELECT  " + 
			"	s.* " + 
			"	FROM invoice i " + 
			"	left join invoice_item ii on ii.invoice_id = i.id " + 
			"	left join stock_in s on s.invoice_item_id = ii.id " + 
			"	left join inventory inv on inv.id = s.inventory_id " + 
			"	left join inventory_spares sp on sp.spare = inv.id " + 
			"	where  " + 
			"	s.company = ?1 " + 
			"	and s.branch = ?2 " + 
			"	and i.party = ?3 " + 
			"	and inv.sub_category_id = ?4 " + 
			"	and sp.parent  = ?5 " + 
			"	and s.int_status_id = 105",
			nativeQuery = true)
	StockIn getStaffAccountableStocksByCat(@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("staff") Long staff,
			@Param("subcat") Integer subcat,
			@Param("parent") Integer parent);
	
	
	
	
	
	
	
	
	///////////////////////////////////////////////////////////////////////////////Instores quaries ................
	
	///////////////////first ////////////////////////////////////////////////////////////////
	
	@Query(value = "SELECT s.company, s.branch, s.inventory_id, "
			+ "s.gen_status_id, s.int_status_id, sum(s.quantity) as quantity, s.unit, s.date_in, s.is_broken, "
			+ "s.id, s.dcurrency, s.wcurrency, s.dsumm, s.wsumm, s.serial_number, "
			+ "s.refkey, s.rate, s.invoice_item_id, gl_code "
			+ "from stock_in s "
			+ "where s.company = ?1 "
			+ "and s.branch = ?2 "
			+ "and s.gen_status_id in (1, 4) "
			+ "and s.int_status_id != 103 "
			+ "group by s.inventory_id, s.gl_code",
			nativeQuery = true)
	Iterable<StockIn> getStockInsByBranch(@Param("cid") Integer cid,
			@Param("bid") Integer bid);
	
	
	///////////////////////////////////////////Second ////////////////////////////////////////////////////
	
	@Query(value = "SELECT s.company, s.branch, s.inventory_id, " 
			+ "s.gen_status_id, s.int_status_id, sum(s.quantity) as quantity, s.unit, s.date_in, s.is_broken, "  
            + "s.id, s.dcurrency, s.wcurrency, s.dsumm, s.wsumm, s.serial_number, " 
			+ "s.refkey, s.rate, s.invoice_item_id, gl_code "
			+ "from stock_in s "
			+ "where s.company = ?1 "
			+ "and s.branch = ?2 "
			+ "and s.inventory_id = ?3 "
			+ "and s.int_status_id != 103 "
			+ "group by s.inventory_id, s.gl_code",
			nativeQuery = true)
	Iterable<StockIn> getStockInsByBranchInv(@Param("cid") Integer cid,
											 @Param("bid") Integer bid,
											 @Param("inv") Integer inv);
	
	//////////////////////////////////////////// Theird ///////////////////
	
	@Query(value = "SELECT s.company, s.branch, s.inventory_id, "
			+ "s.gen_status_id, s.int_status_id, sum(s.quantity) as quantity, s.unit, s.date_in, s.is_broken, "
			+ "s.id, s.dcurrency, s.wcurrency, s.dsumm, s.wsumm, s.serial_number, "
			+ "s.refkey, s.rate, s.invoice_item_id, gl_code "
			+ "FROM stock_in s "
			+ "where s.company = ?1 "
			+ "and s.branch = ?2 "
			+ "and s.int_status_id = ?3 "
			+ "group by s.inventory_id, s.gl_code",
			nativeQuery = true)
Iterable<StockIn> getStockInsByBranchStatus(@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("status") Integer status);
	
	
	////////////////////////////////////////////// Forth ////////////////////////

	
	@Query(value = "SELECT " + 
			"s.company, s.branch, s.inventory_id, " + 
			"s.gen_status_id, s.int_status_id, sum(s.quantity) as quantity, s.unit, s.date_in, s.is_broken, " + 
			"s.id, s.dcurrency, s.wcurrency, s.dsumm, s.wsumm, s.serial_number, " + 
			"s.refkey, s.rate, s.invoice_item_id, gl_code " + 
			"FROM stock_in s " + 
			"where  " +
			"s.company = ?1 " +
			"and s.branch = ?2 " +
			"and s.inventory_id = ?3 " +
			"and s.int_status_id = ?4 " +
			"group by s.inventory_id, s.gl_code",
			nativeQuery = true)
	Iterable<StockIn> getStockInsByBranchInvStatus(@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("inv") Integer inv,
			@Param("status") Integer status);

  //////////////////////////////////////////////////// Fiveth /////////////////


	@Query(value = "SELECT s.company, s.branch, s.inventory_id, "
			+ "s.gen_status_id, s.int_status_id, sum(s.quantity) as quantity, s.unit, s.date_in, s.is_broken, "
			+ "s.id, s.dcurrency, s.wcurrency, s.dsumm, s.wsumm, s.serial_number, "
			+ "s.refkey, s.rate, s.invoice_item_id, gl_code "
			+ "FROM invoice i " 
			+ "left join invoice_item ii on ii.invoice_id = i.id "  
			+ "left join stock_in s on s.invoice_item_id = ii.id "  
			+ "where  "  
			+ "s.company = ?1 "  
			+ "and s.branch = ?2 "  
			+ "and i.party = ?4 "   
			+ "and s.int_status_id = ?3 "
			+ "group by s.inventory_id, s.gl_code",
			nativeQuery = true)
	Iterable<StockIn> getStockInsByBranchStatusParty(@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("status") Integer status,
			@Param("party") Integer party);
	
	/////////////////////////////////////////////sixth //////////////////////////////////////////////////////
	
	@Query(value = "SELECT s.company, s.branch, s.inventory_id, "
			+ "s.gen_status_id, s.int_status_id, sum(s.quantity) as quantity, s.unit, s.date_in, s.is_broken, "
			+ "s.id, s.dcurrency, s.wcurrency, s.dsumm, s.wsumm, s.serial_number, "
			+ "s.refkey, s.rate, s.invoice_item_id, gl_code "
			+ "FROM invoice i " 
			+ "left join invoice_item ii on ii.invoice_id = i.id "  
			+ "left join stock_in s on s.invoice_item_id = ii.id "  
			+ "where  "  
			+ "s.company = ?1 "  
			+ "and s.branch = ?2 " 
			+ "and s.inventory_id = ?3 " 
			+ "and i.party = ?5 "   
			+ "and s.int_status_id = ?4 "
			+ "group by s.inventory_id, s.gl_code",
			nativeQuery = true)
	Iterable<StockIn> getStockInsByBranchInvStatusParty(@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("inv") Integer inv,
			@Param("status") Integer status,
			@Param("party") Integer party);
	
	///////////////////////////////////////////////////////////////////////////////////////
	
	@Query(value = "SELECT s.* "
			+ "FROM Stock_in s " 
			+ "where  "
			+ "s.company = ?1 "  
			+ "and s.branch = ?2 " 
			+ "and s.inventory_id = ?3",
			nativeQuery = true)
	Iterable<StockIn> getStocksForContract(@Param("cid") Integer cid,
										   @Param("bid") Integer bid,
			                               @Param("inv") Integer inv);
	
	/////////////////////////////////////////////////////////////////////////////////////////
	@Query(value = "SELECT s.* "
			+ "FROM Stock_in s " 
			+ "where  "  
			+ "s.company = ?1 "  
			+ "and s.branch = ?2 " 
			+ "and s.inventory_id = ?3 "
			+ "and s.int_status_id = ?4",
			nativeQuery = true)
	Iterable<StockIn> getStocksForContractStatus(@Param("cid") Integer cid,
										   		 @Param("bid") Integer bid,
										   		 @Param("inv") Integer inv,
										   		 @Param("status") Integer status);
	
	/////////////////////////////////////////////////////////////////////////////////////////

	@Query(value = "SELECT s.* "
			+ "FROM invoice i " 
			+ "left join invoice_item ii on ii.invoice_id = i.id "  
			+ "left join stock_in s on s.invoice_item_id = ii.id "  
			+ "where  "  
			+ "s.company = ?1 "  
			+ "and s.branch = ?2 " 
			+ "and s.inventory_id = ?3 " 
			+ "and i.party = ?4 "   
			+ "and s.int_status_id = ?5",
			nativeQuery = true)
	Iterable<StockIn> getStocksForContractPartyStatus(@Param("cid") Integer cid,
													  @Param("bid") Integer bid,
						                              @Param("inv") Integer inv,
						                              @Param("party") Integer party,
						                              @Param("status") Integer status);
	
	////////////////////////////////////////////////////// Instore Reports .....................................
	
	@Query(value = "SELECT "
			+ " id, company, branch, inventory_id, unit, invoice_item_id, serial_number, "
			+ " gen_status_id, int_status_id, refkey, wsumm, wcurrency, rate, dsumm, dcurrency, "
			+ " date_in, gl_code, is_broken, count(s.quantity) as quantity "
			+ " FROM stock_in s"
			+ " where"
			+ " s.company = ?1"
			+ " and s.branch = ?2"
			+ " and s.inventory_id = ?3"
			+ " and s.int_status_id in (101, 105)"
			+ " group by inventory_id",
			nativeQuery = true)
	StockIn getStocksForReport(@Param("cid") Integer cid,
							   @Param("bid") Integer bid,
							   @Param("inv") Integer inv);
	
}