package com.cordialsr.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.EducationLevel;

@RepositoryRestResource(collectionResourceRel = "educationLevel", path = "educationLevel")

public interface EducationLevelDao extends CrudRepository<EducationLevel, Integer>{
	@Query("from EducationLevel c where "
			+ "c.id > 0")
			List<EducationLevel> findAll();
}
