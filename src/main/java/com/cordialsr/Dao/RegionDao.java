package com.cordialsr.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.Region;
import com.cordialsr.domain.projection.RegionProjection;

@RepositoryRestResource(collectionResourceRel = "region", path = "region", 
						excerptProjection = RegionProjection.class)
public interface RegionDao extends CrudRepository<Region, Integer> {

	@Query(value = "SELECT * FROM region r " + 
			"left join region_has_branch b on b.region_id = r.id " + 
			"where " + 
			"b.branch_id = ?1 " + 
			"and r.department = ?2 " +
			"limit 1",
			nativeQuery = true)
	Region getRegionByBrAndDep(Integer branch, Integer dep);
			
	
	@Query(value = "SELECT * FROM region r " + 
			"where " + 
			"r.company_id = ?1 " + 
			"and r.department = ?2",
			nativeQuery = true)
	List<Region> getAllRegionByComAndDep(Integer company, Integer dep);
	
}
