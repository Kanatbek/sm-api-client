package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.InvMainCategory;

@RepositoryRestResource(collectionResourceRel = "invMainCategory", path = "invMainCategory")
public interface InvMainCategoryDao extends CrudRepository<InvMainCategory, Integer>{
	
	@Query("from InvMainCategory i where "
			+ "i.company.id = ?1")
			Iterable<InvMainCategory>findAllByCompany(@Param("company") Integer company);
	
}
