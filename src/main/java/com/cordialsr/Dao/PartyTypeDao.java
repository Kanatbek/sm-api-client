package com.cordialsr.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.PartyType;
import com.cordialsr.domain.projection.PartyTypeInlineProjection;
@RepositoryRestResource(collectionResourceRel = "partyType", path = "partyType",
						excerptProjection = PartyTypeInlineProjection.class)
public interface PartyTypeDao extends CrudRepository<PartyType, String>  {
	
}
