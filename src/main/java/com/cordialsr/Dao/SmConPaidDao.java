package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cordialsr.domain.SmConPaid;

public interface SmConPaidDao extends JpaRepository<SmConPaid, Integer> {
	
	@Query("select c from SmConPaid c where c.contractNumber = :conNo")
	SmConPaid finByConNo(@Param("conNo") String conNo);
	
}
