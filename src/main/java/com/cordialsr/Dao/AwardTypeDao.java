package com.cordialsr.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.AwardType;

@RepositoryRestResource(collectionResourceRel = "awardType", path = "awardType")
public interface AwardTypeDao extends CrudRepository<AwardType, Integer> {

}
