package com.cordialsr.Dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.SmEnquiry;
import com.cordialsr.domain.projection.SmEnquiryProjection;

@RepositoryRestResource(collectionResourceRel = "smEnquiry", path = "smEnquiry",
								excerptProjection = SmEnquiryProjection.class)
public interface SmEnquiryDao extends JpaRepository<SmEnquiry, Long>{

	
	@Query("from SmEnquiry c where c.id = ?1")
	SmEnquiry findOneById(@Param("eid") Long eid);
	
	@Query("from SmEnquiry c where c.company.id = ?1"
	+ " ORDER BY id DESC")
	Page<SmEnquiry> findAllEnq(@Param("cid") Integer cid,
			                     Pageable pageRequest);
	
	@Query("from SmEnquiry c where c.company.id = ?1 "
		+ "and c.branch.id = ?2"
		+ " ORDER BY id DESC")
	Page<SmEnquiry> findAllEnqBranch(@Param("cid") Integer cid,
									@Param("bid") Integer bid,
			                     Pageable pageRequest);
	
	
	@Query("from SmEnquiry c where c.company.id = ?1 "
			+ "and c.etype = ?2"
			+ " ORDER BY id DESC")
	Page<SmEnquiry> findAllEnqType(@Param("cid") Integer cid,
                                   @Param("tp") Integer tp,
			                     Pageable pageRequest);


	@Query("from SmEnquiry c where c.company.id = ?1 "
			+ "and c.status = ?2"
			+ " ORDER BY id DESC")
	Page<SmEnquiry> findAllEnqStatus(@Param("cid") Integer cid,
                                 @Param("status") Integer status,
			                     Pageable pageRequest);
	

	@Query("from SmEnquiry c where c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "and c.etype = ?3"
			+ " ORDER BY id DESC")
	Page<SmEnquiry> findAllEnqBranchType(@Param("cid") Integer cid,
										 @Param("bid") Integer bid,
                                         @Param("tp") Integer tp,
			                     Pageable pageRequest);
	
	
	@Query("from SmEnquiry c where c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "and c.status = ?3"
			+ " ORDER BY id DESC")
	Page<SmEnquiry> findAllEnqBranchStatus(@Param("cid") Integer cid,
										   @Param("bid") Integer bid,
                                           @Param("status") Integer status,
			                     Pageable pageRequest);
	
	
	@Query("from SmEnquiry c where c.company.id = ?1 "
			+ "and c.etype = ?2 "
			+ "and c.status = ?3"
			+ " ORDER BY id DESC")
	Page<SmEnquiry> findAllEnqTypeStatus(@Param("cid") Integer cid,
                                     @Param("tp") Integer tp,
                                     @Param("status") Integer status,
			                     Pageable pageRequest);
	
	
	@Query("from SmEnquiry c where c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "and c.etype = ?3 "
			+ "and c.status = ?4"
			+ " ORDER BY id DESC")
	Page<SmEnquiry> findAllEnqBranchTypeStatus(@Param("cid") Integer cid,
									       @Param("bid") Integer bid,
                                           @Param("tp") Integer tp,
                                           @Param("status") Integer status,
			                     Pageable pageRequest);
}
