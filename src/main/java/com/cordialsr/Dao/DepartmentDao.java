package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.Department;

@RepositoryRestResource(collectionResourceRel = "department", path = "department")
public interface DepartmentDao extends CrudRepository<Department, Integer>{
	
	@Query("from Department c where c.company.id = ?1")
	Iterable<Department> findAllByCompany(
					@Param("companyId") Integer companyId);
	
}
