package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.StockStatus;

@RepositoryRestResource(collectionResourceRel = "status", path = "status")
public interface StockStatusDao extends CrudRepository<StockStatus, Integer>{
	
	@Query(value = "SELECT * from stock_status s "
			+ "where s.type = ?1",
			nativeQuery = true)
	Iterable<StockStatus> getStatuesByType(@Param("tp") String tp);
	
}
