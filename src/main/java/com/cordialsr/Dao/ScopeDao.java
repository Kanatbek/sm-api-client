package com.cordialsr.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.Scope;

@RepositoryRestResource(collectionResourceRel = "scope", path = "scope")
public interface ScopeDao extends CrudRepository<Scope, String>{

}
