package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.PaymentTemplate;
import com.cordialsr.domain.projection.PriceListProjection;

@RepositoryRestResource(collectionResourceRel = "paymentTemplate", path = "paymentTemplate")
public interface PaymentTemplateDao extends CrudRepository<PaymentTemplate, Integer> {
	
	@Query("from PriceList p where "
	+ "p.id = ?1")
	PaymentTemplate findById(@Param("id") Integer id);
	
	@Query("from PaymentTemplate p where p.priceList.id = ?1")
	Iterable<PaymentTemplate> findByContractTypeId(@Param("ctype") Integer ctype);
	
}
