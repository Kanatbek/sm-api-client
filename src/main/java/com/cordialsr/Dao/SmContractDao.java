package com.cordialsr.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.SmBranch;
import com.cordialsr.domain.SmContract;
import com.cordialsr.domain.projection.SmContractProjection;

@RepositoryRestResource(collectionResourceRel = "smContract", path = "smContract", 
						excerptProjection = SmContractProjection.class)
public interface SmContractDao extends PagingAndSortingRepository<SmContract, Integer> {
	SmContract findByContractNumber(@Param("contractNumber") String contractNumber);
	List<SmContract> findBySmBranch(@Param("smBranch") SmBranch smBranch);
	List<SmContract> findBySn(@Param("sn") String sn);
	
	@Query(value = "select * from Sm_Contract c "
			+ "where c.sm_branch_code not in ('U01', 'K01', 'K02') "
			// + "where c.id > 71006 "
			// + "and c.cdate < '2018-05-01' "
			+ "limit ?1, ?2", nativeQuery = true)
	List<SmContract> findFirstN(Integer i, Integer n);
	
	@Query("from SmContract c where lower(c.customerFio) like CONCAT('%', lower(:customer), '%')")
	public Iterable<SmContract> findByCustomer(@Param("customer") String customer);

	@Query("from SmContract c where "
	+ "c.smBranch.code = ?1")
	Iterable<SmContract> findAllBySmBranch(@Param("inBranch") String inBranch);
	
	@Query("from SmContract c where "
			+ "c.smBranch.realBranch.id = ?1")
			Iterable<SmContract> findAllByBranch(@Param("inBranch") Integer inBranch);
	
	@Query("from SmContract c where "
			+ "c.storno = false and c.smBranch.code = ?1")
			Iterable<SmContract> findActiveByBranch(@Param("inBranch") String inBranch);
	
//	@Query("from SmContract c where "
//			+ "c.storno = true and c.smBranch.code = ?1")
//			Iterable<SmContract> findAllByBranch(@Param("inBranch") String inBranch);
}
