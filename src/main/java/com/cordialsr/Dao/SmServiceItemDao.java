package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.SmServiceItem;

@RepositoryRestResource(collectionResourceRel = "smServiceItem", path = "smServiceItem")
public interface SmServiceItemDao extends CrudRepository<SmServiceItem, Long> {
	@Query("from SmServiceItem c where "
			+ "c.id = ?1")
			SmServiceItem findById(@Param("itemId") Long itemId);
}

