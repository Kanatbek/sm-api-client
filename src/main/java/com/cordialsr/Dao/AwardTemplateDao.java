package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.AwardTemplate;

// @RepositoryRestResource(collectionResourceRel = "awardTemplate", path = "awardTemplate")
public interface AwardTemplateDao extends CrudRepository<AwardTemplate, Integer> {
		
	@Query("from AwardTemplate c where "
			+ "c.award.id = ?1")
			Iterable<AwardTemplate> findAllByAward(@Param("award") Integer awardId);
	
}
