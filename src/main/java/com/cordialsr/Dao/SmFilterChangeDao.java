package com.cordialsr.Dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;

import com.cordialsr.Dao.custrepo.SmfcRepo;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.SmFilterChange;
import com.cordialsr.domain.projection.SmFCInlineContractProjection;

@RepositoryRestResource(collectionResourceRel = "smFilterChange", 
						path = "smFilterChange", 
						excerptProjection = SmFCInlineContractProjection.class)
public interface SmFilterChangeDao extends JpaRepository<SmFilterChange, Integer>, SmfcRepo {
	
	@Query(value = "select f.* from sm_filter_change as f "  
			+ "left join cordialsr_test.contract as c on f.CONTRACT = c.id "  
			+ "where c.inventory_sn = ?1 limit 1",
				nativeQuery = true)
			SmFilterChange findBySN(@Param("ser_num") String ser_num);
	
	@Query("from SmFilterChange c where "
			+ "(c.f1Next < ?1 "
	+ "or c.f2Next < ?1 "
	+ "or c.f3Next < ?1 "
	+ "or c.f4Next < ?1 "
	+ "or c.f5Next < ?1 "
	+ "or c.prNext < ?1) "
	+ "and not "
	+ "(c.f1Next between ?1 and ?2 "
	+ "or c.f2Next between ?1 and ?2 "
	+ "or c.f3Next between ?1 and ?2 "
	+ "or c.f4Next between ?1 and ?2 "
	+ "or c.f5Next between ?1 and ?2) "
	+ "and c.enabled = 1 and c.contract.company.id = ?3")
	List<SmFilterChange> findByDatesOverdue(
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("from") Date from,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("to") Date to,
			@Param("cid") Integer cid);
	
	
	@Query("from SmFilterChange c where "
			+ "(c.f1Next between ?1 and ?2 "
	+ "or c.f2Next between ?1 and ?2 "
	+ "or c.f3Next between ?1 and ?2 "
	+ "or c.f4Next between ?1 and ?2 "
	+ "or c.f5Next between ?1 and ?2 "
	+ "or c.prNext between ?1 and ?2) "
	+ "and c.enabled = 1 and c.contract.careman.id = ?3")
	List<SmFilterChange> findByDatesCaremanBetween(
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("from") Date from,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("to") Date to,
			@Param("careman") Long careman);
	
	
	@Query("from SmFilterChange c where "
			+ "(c.f1Next between ?1 and ?2 "
	+ "or c.f2Next between ?1 and ?2 "
	+ "or c.f3Next between ?1 and ?2 "
	+ "or c.f4Next between ?1 and ?2 "
	+ "or c.f5Next between ?1 and ?2 "
	+ "or c.prNext between ?1 and ?2) "
	+ "and c.enabled = 1 and c.contract.company.id = ?3")
	List<SmFilterChange> findByDatesBetween(
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("from") Date from,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("to") Date to,
			@Param("cid") Integer cid);
	
	
	@Query("from SmFilterChange c where "
			+ "(c.f1Next < ?1 "
	+ "or c.f2Next < ?1 "
	+ "or c.f3Next < ?1 "
	+ "or c.f4Next < ?1 "
	+ "or c.f5Next < ?1 "
	+ "or c.prNext < ?1) "
	+ "and not "
	+ "(c.f1Next between ?1 and ?2 "
	+ "or c.f2Next between ?1 and ?2 "
	+ "or c.f3Next between ?1 and ?2 "
	+ "or c.f4Next between ?1 and ?2 "
	+ "or c.f5Next between ?1 and ?2) "
	+ "and c.enabled = 1 and c.contract.serviceBranch.id = ?3 and c.contract.company.id = ?4")
	List<SmFilterChange> findByDatesOverdueBranch(
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("from") Date from,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("to") Date to,
			@Param("bid") Integer bid,
			@Param("cid") Integer cid);
	
	
	@Query("from SmFilterChange c where "
			+ "(c.f1Next between ?1 and ?2 "
	+ "or c.f2Next between ?1 and ?2 "
	+ "or c.f3Next between ?1 and ?2 "
	+ "or c.f4Next between ?1 and ?2 "
	+ "or c.f5Next between ?1 and ?2 "
	+ "or c.prNext between ?1 and ?2) "
	+ "and c.enabled = 1 and c.contract.serviceBranch.id = ?3 and c.contract.company.id = ?4")
	List<SmFilterChange> findByDatesBetweenBranch(
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("from") Date from,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("to") Date to,
			@Param("bid") Integer bid,
			@Param("cid") Integer cid);
	
	///////////////////////////////with Careman//////////////////////////////////////////////////////
	
	
	@Query("from SmFilterChange c where "
			+ "(c.f1Next < ?1 "
	+ "or c.f2Next < ?1 "
	+ "or c.f3Next < ?1 "
	+ "or c.f4Next < ?1 "
	+ "or c.f5Next < ?1 "
	+ "or c.prNext < ?1) "
	+ "and not "
	+ "(c.f1Next between ?1 and ?2 "
	+ "or c.f2Next between ?1 and ?2 "
	+ "or c.f3Next between ?1 and ?2 "
	+ "or c.f4Next between ?1 and ?2 "
	+ "or c.f5Next between ?1 and ?2) "
	+ "and c.enabled = 1 and c.contract.company.id = ?3 "
	+ "and c.contract.careman.id = ?4")
	List<SmFilterChange> findByDatesOverdueCareman(
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("from") Date from,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("to") Date to,
			@Param("cid") Integer cid,
			@Param("eid") Long eid);
	
	
	@Query("from SmFilterChange c where "
			+ "(c.f1Next between ?1 and ?2 "
	+ "or c.f2Next between ?1 and ?2 "
	+ "or c.f3Next between ?1 and ?2 "
	+ "or c.f4Next between ?1 and ?2 "
	+ "or c.f5Next between ?1 and ?2 "
	+ "or c.prNext between ?1 and ?2) "
	+ "and c.enabled = 1 and c.contract.company.id = ?3 "
	+ "and c.contract.careman.id = ?4")
	List<SmFilterChange> findByDatesBetweenCareman(
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("from") Date from,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("to") Date to,
			@Param("cid") Integer cid,
			@Param("eid") Long eid);
	
	
	///////////////////////////////////////////////////////////////////////////////////////////////
	@Query("from SmFilterChange c where "
			+ "(c.f1Next < ?1 "
	+ "or c.f2Next < ?1 "
	+ "or c.f3Next < ?1 "
	+ "or c.f4Next < ?1 "
	+ "or c.f5Next < ?1 "
	+ "or c.prNext < ?1) "
	+ "and not "
	+ "(c.f1Next between ?1 and ?2 "
	+ "or c.f2Next between ?1 and ?2 "
	+ "or c.f3Next between ?1 and ?2 "
	+ "or c.f4Next between ?1 and ?2 "
	+ "or c.f5Next between ?1 and ?2) "
	+ "and c.enabled = 1 and c.contract.serviceBranch.id = ?3 and c.contract.company.id = ?4 "
	+ "and c.contract.careman.id = ?5")
	List<SmFilterChange> findByDatesOverdueBranchCareman(
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("from") Date from,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("to") Date to,
			@Param("bid") Integer bid,
			@Param("cid") Integer cid,
			@Param("eid") Long eid);
	
	
	@Query("from SmFilterChange c where "
			+ "(c.f1Next between ?1 and ?2 "
	+ "or c.f2Next between ?1 and ?2 "
	+ "or c.f3Next between ?1 and ?2 "
	+ "or c.f4Next between ?1 and ?2 "
	+ "or c.f5Next between ?1 and ?2 "
	+ "or c.prNext between ?1 and ?2) "
	+ "and c.enabled = 1 and c.contract.serviceBranch.id = ?3 and c.contract.company.id = ?4 "
	+ "and c.contract.careman.id = ?5")
	List<SmFilterChange> findByDatesBetweenBranchCareman(
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("from") Date from,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("to") Date to,
			@Param("bid") Integer bid,
			@Param("cid") Integer cid,
			@Param("eid") Long eid);
	
	
	///////////////////////////////////////////////////////////////////////////////////////
	@Query(value = "select " + 
			"fc.* " + 
			"from contract c " + 
			"left join sm_filter_change fc on c.id = fc.CONTRACT " + 
			"left join employee e on e.id = c.careman " + 
			"left join party cus on c.customer = cus.id " + 
			"left join party car on e.party_id = car.id " + 
			"where " + 
			"c.contract_number like %:filter% " + 
			"or c.inventory_sn like %:filter% " + 
			"or cus.firstname like %:filter% " + 
			"or cus.lastname like %:filter% " + 
			"or cus.middlename like %:filter% " + 
			"or coalesce(car.firstname, '') like %:filter% " + 
			"or coalesce(car.lastname, '') like %:filter% " + 
			"or coalesce(car.middlename, '') like %:filter% " +
			"ORDER BY ?#{#pageable}", 
			nativeQuery = true)
	Page<SmFilterChange> findByFilter(
					@Param("filter") String filter, Pageable pageable);
	
	@Query("from SmFilterChange c where c.contract.id = ?1")
	SmFilterChange findByContractId(@Param("con") Long con);


	@Query("from SmFilterChange c where "
	+ "(YEAR(c.f1Next) = ?1 and MONTH(c.f1Next) = ?2 "
	+ "or YEAR(c.f2Next) = ?1 and MONTH(c.f2Next) = ?2 "
	+ "or YEAR(c.f3Next) = ?1 and MONTH(c.f3Next) = ?2  "
	+ "or YEAR(c.f4Next) = ?1 and MONTH(c.f4Next) = ?2  "
	+ "or YEAR(c.f5Next) = ?1 and MONTH(c.f5Next) = ?2  "
	+ "or YEAR(c.prNext) = ?1 and MONTH(c.prNext) = ?2 ) "
	+ "and c.enabled = 1 and c.contract.serviceBranch.id = ?3 and c.contract.company.id = ?4 "
	+ "and c.contract.careman.id = ?5")
	List<SmFilterChange> findByMonthBranchCareman(Integer year, Integer month, Integer bid, Integer cid, Long eid);
	
	@Query("from SmFilterChange c where ("
			   + "(YEAR(c.f1Next) < ?1 or YEAR(c.f1Next) = ?1 and MONTH(c.f1Next) < ?2) "
			+ "or (YEAR(c.f2Next) < ?1 or YEAR(c.f2Next) = ?1 and MONTH(c.f2Next) < ?2) "
			+ "or (YEAR(c.f3Next) < ?1 or YEAR(c.f3Next) = ?1 and MONTH(c.f3Next) < ?2) "
			+ "or (YEAR(c.f4Next) < ?1 or YEAR(c.f4Next) = ?1 and MONTH(c.f4Next) < ?2) "
			+ "or (YEAR(c.f5Next) < ?1 or YEAR(c.f5Next) = ?1 and MONTH(c.f5Next) < ?2) "
			+ "or (YEAR(c.prNext) < ?1 or YEAR(c.prNext) = ?1 and MONTH(c.prNext) < ?2) ) "
			+ "and not ( "
			+ "   (YEAR(c.f1Next) = ?1 and MONTH(c.f1Next) = ?2) " 
			+ "or (YEAR(c.f2Next) = ?1 and MONTH(c.f2Next) = ?2) "  
			+ "or (YEAR(c.f3Next) = ?1 and MONTH(c.f3Next) = ?2) "  
			+ "or (YEAR(c.f4Next) = ?1 and MONTH(c.f4Next) = ?2) "  
			+ "or (YEAR(c.f5Next) = ?1 and MONTH(c.f5Next) = ?2) "
			+ ") "
			+ "and c.enabled = 1 and c.contract.serviceBranch.id = ?3 and c.contract.company.id = ?4 "
			+ "and c.contract.careman.id = ?5")
			List<SmFilterChange> findOverdueByMonthBranchCareman(Integer year, Integer month, Integer bid, Integer cid, Long eid);
	 
}
