package com.cordialsr.Dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.format.annotation.DateTimeFormat;

import com.cordialsr.domain.Company;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.PartyType;
import com.cordialsr.domain.projection.PartyProjection;

@RepositoryRestResource(collectionResourceRel = "party", path = "party",
						excerptProjection = PartyProjection.class)
public interface PartyDao extends JpaRepository<Party, Long>  {
	
	List<Party> findByCompany(@Param("company") Company company);
	
	@Query("select (case when count(c.id)>0 then 1 else 0 end) as res "
			+ "from Party c where c.iinBin = ?1")
	boolean existsByIinBin(String iinBin);

	@Query("from Party c where "
			+ "c.company.id = ?1 "
			+ "and c.companyName = ?2")
	List<Party> findByCompanyName(Integer cid, String companyName);
	
	@Query("from Party c where "
			+ "c.company.id = ?1 "
			+ "and c.partyType.name = ?2 "
			+ "and c.lastname = ?3 "
			+ "and c.firstname = ?4 "
			+ "and c.middlename = ?5")
	List<Party> findByTypeFIO(Integer cid, String pt, String f, String i, String o);
	
	@Query("from Party c where "
			+ "c.company.id = ?1 "
			+ "and c.partyType.name = ?2 "
			+ "and c.lastname = ?3 "
			+ "and c.firstname = ?4 "
			+ "and c.middlename is null")
	List<Party> findByTypeFI(Integer cid, String pt, String f, String i);
	
	@Query("from Party c where "
			+ "c.company.id = ?1 "
			+ "and c.partyType.name = ?2 "
			+ "and c.firstname = ?3 "
			+ "and c.lastname is null "
			+ "and c.middlename is null")
	List<Party> findByTypeF(Integer cid, String pt, String f);
	
	@Query("from Party c where "
			+ "c.company.id = ?1 "
			+ "and c.partyType.name = ?2 "
			+ "and c.firstname is null "
			+ "and c.lastname = ?3 "
			+ "and c.middlename is null")
	List<Party> findByTypeL(Integer cid, String pt, String f);
	
	@Query("from Party c where "
			+ "c.company.id = ?1 "
			+ "and c.iinBin = ?2 ")
	Party findByIinBin(Integer cid, String iinBin);
	
	@Query("from Party c where "
			+ "c.company.id = ?1 "
			+ "and c.iinBin = ?2 ")
	Party findCustomerByIinBin(@Param("cid") Integer cid, @Param("iinBin") String iinBin);
	
	@Query(value = "select * from Party c where "
			+ "c.iin_bin = :iinBin " ,
			nativeQuery = true)
	Party findPartyIinBin(@Param("iinBin") String iinBin);
	
	@Query("from Party c where "
			+ "c.company.id = ?1 "
			+ "and c.lastname = ?2 "
			+ "and c.firstname = ?3 "
			+ "and c.middlename = ?4 ")
	List<Party> findByExactFIO(@Param("cid") Integer cid,
					@Param("last") String last,
					@Param("first") String first,
					@Param("middle") String middle);
	
	@Query("from Party c where "
			+ "c.company.id = ?1 "
			+ "and c.lastname = ?2 "
			+ "and c.firstname = ?3 ")
	List<Party> findByExactFI(@Param("cid") Integer cid,
					@Param("last") String last,
					@Param("first") String first);
	
	@Query("from Party c where "
			+ "c.company.id = ?1")
			Iterable<Party> findAllByCompany(@Param("company") Integer company);

	@Query("from Party c where "
			+ "c.id = ?1")
			Party findAllByParty(@Param("pid") Long pid);
	
	@Query(value = "select * from Party c "
			+ "left join Employee e on e.party_id = c.id "
			+ "where e.id = ?1",
			nativeQuery = true)
			Party findPartyByEmployee(@Param("emplId") Long emplId);
	
	@Query(value = "Select * from Party c "
			+ "where c.company = :cid "
			+ "GROUP BY c.id "
			+ "ORDER BY ?#{#pageable} ", 
			nativeQuery = true)
	Page<Party> findAllByComId( 
			@Param("cid") Long cid,
			Pageable pageable);
	
	@Query(value = "Select * from Party c "
			+ "where c.company = :cid "
			+ "and c.party_type = :pt "
			+ "GROUP BY c.id "
			+ "ORDER BY ?#{#pageable} ", 
			nativeQuery = true)
	Page<Party> findAllByComIdType( 
			@Param("cid") Long cid,
			@Param("pt") String pt,
			Pageable pageable);
	
	@Query(value = "Select * from Party c "
			+ "left join employee p on p.party_id = c.id "
			+ "where c.company = :cid "
			+ "and p.branch_id = :bid "
			+ "and c.party_type = :pt "
			+ "GROUP BY c.id "
			+ "ORDER BY ?#{#pageable} ", 
			nativeQuery = true)
	Page<Party> findAllByPartyTypeBr( 
			@Param("cid") Long cid,
			@Param("bid") Integer bid,
			@Param("pt") String pt,
			Pageable pageable);
	
	// ******************************************************************************************
	
	// Employee List
	
	@Query(value = "Select * from Party p " + 
			"left join employee e on e.party_id = p.id " + 
			"where p.company = :cid " +
			"and p.party_type = '" + PartyType.TYPE_STAFF + "' " + 
			// "and e.date_fired is null " +
			"GROUP BY e.party_id " +
			"ORDER BY ?#{#pageable}", 
			nativeQuery = true)
	Page<Party> findAllEmployees( 
			@Param("cid") Integer cid,
			Pageable pageable);
	
	@Query(value = "Select * from Party p " + 
			"left join employee e on e.party_id = p.id " + 
			"where p.company = :cid " + 
			"and e.branch_id = :bid " +  
			"and p.party_type = '" + PartyType.TYPE_STAFF + "' " + 
			// "and e.date_fired is null " +
			"GROUP BY e.party_id " +
			"ORDER BY ?#{#pageable}", 
			nativeQuery = true)
	Page<Party> findAllEmployeesBr( 
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			Pageable pageable);
	
	@Query(value = "Select * from Party p " + 
			"left join employee e on e.party_id = p.id " + 
			"where p.company = :cid " + 
			"and e.branch_id = :bid " + 
			"and p.party_type = '" + PartyType.TYPE_STAFF + "' " + 
			"and e.position_id = :pos " + 
			// "and e.date_fired is null " +
			"ORDER BY ?#{#pageable}", 
			nativeQuery = true)
	Page<Party> findAllEmployeesBrPos( 
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("pos") Integer pos,
			Pageable pageable);
	
	@Query(value = "Select * from Party p " + 
			"left join employee e on e.party_id = p.id " + 
			"where p.company = :cid " + 
			"and p.party_type = '" + PartyType.TYPE_STAFF + "' " + 
			"and e.position_id = :pos " + 
			// "and e.date_fired is null " +
			"ORDER BY ?#{#pageable}", 
			nativeQuery = true)
	Page<Party> findAllEmployeesPos( 
			@Param("cid") Integer cid,
			@Param("pos") Integer pos,
			Pageable pageable);
	
	
	
	// Filters
	@Query(value = "Select * from Party p " + 
			"where p.company = :cid  " + 
			"and "
			+ "(p.firstname like %:filter% "
			+ "or p.lastname like %:filter% "
			+ "or p.middlename like %:filter% "
			+ "or p.iin_bin like %:filter% "
			+ "or p.company_name like %:filter% "
			+ "or p.passport_number like %:filter% "
			+ "or p.iik like %:filter% "
			+ "or p.bik like %:filter% "
			+ ") " +
			"ORDER BY ?#{#pageable}", 
			nativeQuery = true)
	Page<Party> filter(
			@Param("cid") Integer cid,
			@Param("filter") String filter,  Pageable pageable);
	
	@Query(value = "Select * from Party p " + 
			"where p.company = :cid  " + 
			"and p.party_type = :pt " +
			"and "
			+ "(p.firstname like %:filter% "
			+ "or p.lastname like %:filter% "
			+ "or p.middlename like %:filter% "
			+ "or p.iin_bin like %:filter% "
			+ "or p.company_name like %:filter% "
			+ "or p.passport_number like %:filter% "
			+ "or p.iik like %:filter% "
			+ "or p.bik like %:filter% "
			+ ") " +
			"ORDER BY ?#{#pageable}", 
			nativeQuery = true)
	Page<Party> filterPt(
			@Param("cid") Integer cid,
			@Param("pt") String pt,
			@Param("filter") String filter,  Pageable pageable);
	
	@Query(value = "Select * from Party p " + 
			"left join employee e on e.party_id = p.id " + 
			"where p.company = :cid  " + 
			"and p.party_type = '" + PartyType.TYPE_STAFF + "' " + 
			// "and e.date_fired is null " +
			"and "
			+ "(p.firstname like %:filter% "
			+ "or p.lastname like %:filter% "
			+ "or p.middlename like %:filter% "
			+ "or p.iin_bin like %:filter% "
			+ "or p.company_name like %:filter% "
			+ "or p.passport_number like %:filter% "
			+ "or p.iik like %:filter% "
			+ "or p.bik like %:filter% "
			+ ") " +
			"GROUP BY e.party_id " +
			"ORDER BY ?#{#pageable}", 
			nativeQuery = true)
	Page<Party> filterEmpl(
			@Param("cid") Integer cid,
			@Param("filter") String filter,  Pageable pageable);
	
	@Query(value = "Select * from Party p " + 
			"left join employee e on e.party_id = p.id " + 
			"where p.company = :cid  " + 
			"and e.branch_id = :bid " + 
			"and p.party_type = '" + PartyType.TYPE_STAFF + "' " + 
			// "and e.date_fired is null " +
			"and "
			+ "(p.firstname like %:filter% "
			+ "or p.lastname like %:filter% "
			+ "or p.middlename like %:filter% "
			+ "or p.iin_bin like %:filter% "
			+ "or p.company_name like %:filter% "
			+ "or p.passport_number like %:filter% "
			+ "or p.iik like %:filter% "
			+ "or p.bik like %:filter% "
			+ ") " +
			"GROUP BY e.party_id " +
			"ORDER BY ?#{#pageable}", 
			nativeQuery = true)
	Page<Party> filterEmplBr(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("filter") String filter,  Pageable pageable);
	
	@Query(value = "Select * from Party p " + 
			"left join employee e on e.party_id = p.id " + 
			"where p.company = :cid " + 
			"and e.branch_id = :bid " + 
			"and p.party_type = '" + PartyType.TYPE_STAFF + "' " + 
			"and e.position_id = :pos " + 
			// "and e.date_fired is null " +
			"and "
			+ "(p.firstname like %:filter% "
			+ "or p.lastname like %:filter% "
			+ "or p.middlename like %:filter% "
			+ "or p.iin_bin like %:filter% "
			+ "or p.company_name like %:filter% "
			+ "or p.passport_number like %:filter% "
			+ "or p.iik like %:filter% "
			+ "or p.bik like %:filter% "
			+ ") " + 
			"GROUP BY e.party_id " +
			"ORDER BY ?#{#pageable}", 
			nativeQuery = true)
	Page<Party> filterEmplBrPos( 
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@Param("pos") Integer pos,
			@Param("filter") String filter,  Pageable pageable);
	
	@Query(value = "Select * from Party p " + 
			"left join employee e on e.party_id = p.id " + 
			"where p.company = :cid " + 
			"and p.party_type = '" + PartyType.TYPE_STAFF + "' " + 
			"and e.position_id = :pos " + 
			// "and e.date_fired is null " +
			"and "
			+ "(p.firstname like %:filter% "
			+ "or p.lastname like %:filter% "
			+ "or p.middlename like %:filter% "
			+ "or p.iin_bin like %:filter% "
			+ "or p.company_name like %:filter% "
			+ "or p.passport_number like %:filter% "
			+ "or p.iik like %:filter% "
			+ "or p.bik like %:filter% "
			+ ") " + 
			"GROUP BY e.party_id " +
			"ORDER BY ?#{#pageable}", 
			nativeQuery = true)
	Page<Party> filterEmplPos( 
			@Param("cid") Integer cid,
			@Param("pos") Integer pos,
			@Param("filter") String filter,  Pageable pageable);
	 	
	
	
	//***********************************************************
	
	
		//FILTER
		@Query(value = "Select * from Party p " 
				+ "left join phone_number f on f.party_id = p.id "
				+ "where p.company = :cid "
				+ "and "
				+ "(p.firstname like %:filter% "
				+ "or p.lastname like %:filter% "
				+ "or p.iin_bin like %:filter% "
				+ "or p.middlename like %:filter% "
				+ "or p.email like %:filter% "
				+ "or p.passport_number like %:filter% "
				+ "or p.iik like %:filter% "
				+ "or p.bik like %:filter% "
				+ "or p.company_name like %:filter% "
				+ "or p.old_id like %:filter% "
				+ "or f.number like %:filter% "
				+ ") "
				+ "group by p.id "
				+ "ORDER BY ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> filterPartyByCid(
				@Param("cid") Integer cid,
				@Param("filter") String filter,
				Pageable pageable);
		
		@Query(value = "Select * from Party p "
				+ "left join employee e on e.party_id = p.id " 
				+ "left join phone_number f on f.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.date_fired is null "
				+ "and "
				+ "(p.firstname like %:filter% "
				+ "or p.lastname like %:filter% "
				+ "or p.iin_bin like %:filter% "
				+ "or p.middlename like %:filter% "
				+ "or p.email like %:filter% "
				+ "or p.passport_number like %:filter% "
				+ "or p.iik like %:filter% "
				+ "or p.bik like %:filter% "
				+ "or p.company_name like %:filter% "
				+ "or p.old_id like %:filter% "
				+ "or f.number like %:filter% "
				+ ") "
				+ "group by p.id "
				+ "ORDER BY ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> filterPartyByCidFired(
				@Param("cid") Integer cid,
				@Param("filter") String filter,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join phone_number f on f.party_id = p.id "
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.branch_id = :bid "
				+ "and "
				+ "(p.firstname like %:filter% "
				+ "or p.lastname like %:filter% "
				+ "or p.iin_bin like %:filter% "
				+ "or p.middlename like %:filter% "
				+ "or p.email like %:filter% "
				+ "or p.passport_number like %:filter% "
				+ "or p.iik like %:filter% "
				+ "or p.bik like %:filter% "
				+ "or p.company_name like %:filter% "
				+ "or p.old_id like %:filter% "
				+ "or f.number like %:filter% "
				+ ") "
				+ "group by p.id "
				+ "ORDER BY ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> filterPartyByCidBid(
				@Param("cid") Integer cid,
				@Param("bid") Integer bid,
				@Param("filter") String filter,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join phone_number f on f.party_id = p.id "
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.branch_id = :bid "
				+ "and e.date_fired is null "
				+ "and "
				+ "(p.firstname like %:filter% "
				+ "or p.lastname like %:filter% "
				+ "or p.iin_bin like %:filter% "
				+ "or p.middlename like %:filter% "
				+ "or p.email like %:filter% "
				+ "or p.passport_number like %:filter% "
				+ "or p.iik like %:filter% "
				+ "or p.bik like %:filter% "
				+ "or p.company_name like %:filter% "
				+ "or p.old_id like %:filter% "
				+ "or f.number like %:filter% "
				+ ") "
				+ "group by p.id "
				+ "ORDER BY ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> filterPartyByCidBidFired(
				@Param("cid") Integer cid,
				@Param("bid") Integer bid,
				@Param("filter") String filter,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "left join phone_number f on f.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.position_id = :posId "
				+ "and e.date_fired is null "
				+ "and "
				+ "(p.firstname like %:filter% "
				+ "or p.lastname like %:filter% "
				+ "or p.iin_bin like %:filter% "
				+ "or p.middlename like %:filter% "
				+ "or p.email like %:filter% "
				+ "or p.passport_number like %:filter% "
				+ "or p.iik like %:filter% "
				+ "or p.bik like %:filter% "
				+ "or p.company_name like %:filter% "
				+ "or p.old_id like %:filter% "
				+ "or f.number like %:filter% "
				+ ") "
				+ "group by p.id "
				+ "ORDER BY ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> filterPartyByCidPos(
				@Param("cid") Integer cid,
				@Param("posId") Integer posId,
				@Param("filter") String filter,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "left join phone_number f on f.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.position_id = :posId "
				+ "and "
				+ "(p.firstname like %:filter% "
				+ "or p.lastname like %:filter% "
				+ "or p.iin_bin like %:filter% "
				+ "or p.middlename like %:filter% "
				+ "or p.email like %:filter% "
				+ "or p.passport_number like %:filter% "
				+ "or p.iik like %:filter% "
				+ "or p.bik like %:filter% "
				+ "or p.company_name like %:filter% "
				+ "or p.old_id like %:filter% "
				+ "or f.number like %:filter% "
				+ ") "
				+ "group by p.id "
				+ "ORDER BY ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> filterPartyByCidPosFired(
				@Param("cid") Integer cid,
				@Param("posId") Integer posId,
				@Param("filter") String filter,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "left join phone_number f on f.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.branch_id = :bid "
				+ "and e.position_id = :posId "
				+ "and e.date_fired is null "
				+ "and "
				+ "(p.firstname like %:filter% "
				+ "or p.lastname like %:filter% "
				+ "or p.iin_bin like %:filter% "
				+ "or p.middlename like %:filter% "
				+ "or p.email like %:filter% "
				+ "or p.passport_number like %:filter% "
				+ "or p.iik like %:filter% "
				+ "or p.bik like %:filter% "
				+ "or p.company_name like %:filter% "
				+ "or p.old_id like %:filter% "
				+ "or f.number like %:filter% "
				+ ") "
				+ "group by p.id "
				+ "ORDER BY ?#{#pageable}",
				nativeQuery = true)
		Page<Party> filterPartyByCidBidPos(
				@Param("cid") Integer cid,
				@Param("bid") Integer bid,
				@Param("posId") Integer posId,
				@Param("filter") String filter,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "left join phone_number f on f.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.branch_id = :bid "
				+ "and e.position_id = :posId "
				+ "and "
				+ "(p.firstname like %:filter% "
				+ "or p.lastname like %:filter% "
				+ "or p.iin_bin like %:filter% "
				+ "or p.middlename like %:filter% "
				+ "or p.email like %:filter% "
				+ "or p.passport_number like %:filter% "
				+ "or p.iik like %:filter% "
				+ "or p.bik like %:filter% "
				+ "or p.company_name like %:filter% "
				+ "or p.old_id like %:filter% "
				+ "or f.number like %:filter% "
				+ ") "
				+ "group by p.id "
				+ "ORDER BY ?#{#pageable}",
				nativeQuery = true)
		Page<Party> filterPartyByCidBidPosFired(
				@Param("cid") Integer cid,
				@Param("bid") Integer bid,
				@Param("posId") Integer posId,
				@Param("filter") String filter,
				Pageable pageable);
	
		
		// SORT BY FIRSTNAME
		@Query(value = "Select * from Party p "
				+ "left join employee e on e.party_id = p.id " 
				+ "where p.company = :cid "
				+ "and e.date_fired is null "
				+ "group by p.id "
				+ "order by p.firstname, ?#{#pageable}",
				nativeQuery = true)
		Page<Party> sortPartyByFirstnameCid(
				@Param("cid") Integer cid,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "where p.company = :cid "
				+ "group by p.id "
				+ "order by p.firstname, ?#{#pageable}",
				nativeQuery = true)
		Page<Party> sortPartyByFirstnameCidFired(
				@Param("cid") Integer cid,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.branch_id = :bid "
				+ "and e.date_fired is null "
				+ "group by p.id "
				+ "order by p.firstname, ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> sortPartyByFirstnameCidBid(
				@Param("cid") Integer cid,
				@Param("bid") Integer bid,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.branch_id = :bid "
				+ "group by p.id "
				+ "order by p.firstname, ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> sortPartyByFirstnameCidBidFired(
				@Param("cid") Integer cid,
				@Param("bid") Integer bid,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.position_id = :posId "
				+ "and e.date_fired is null "
				+ "group by p.id "
				+ "order by p.firstname, ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> sortPartyByFirstnameCidPos(
				@Param("cid") Integer cid,
				@Param("posId") Integer posId,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.position_id = :posId "
				+ "group by p.id "
				+ "order by p.firstname, ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> sortPartyByFirstnameCidPosFired(
				@Param("cid") Integer cid,
				@Param("posId") Integer posId,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.branch_id = :bid "
				+ "and e.position_id = :posId "
				+ "and e.date_fired is null "
				+ "group by p.id "
				+ "order by p.firstname, ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> sortPartyByFirstnameCidBidPos(
				@Param("cid") Integer cid,
				@Param("bid") Integer bid,
				@Param("posId") Integer posId,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.branch_id = :bid "
				+ "and e.position_id = :posId "
				+ "group by p.id "
				+ "order by p.firstname, ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> sortPartyByFirstnameCidBidPosFired(
				@Param("cid") Integer cid,
				@Param("bid") Integer bid,
				@Param("posId") Integer posId,
				Pageable pageable);
		
		// FILTER BY LASTNAME
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.date_fired is null "
				+ "group by p.id "
				+ "order by p.lastname, ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> sortPartyByLastnameCid(
				@Param("cid") Integer cid,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "group by p.id "
				+ "order by p.lastname, ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> sortPartyByLastnameCidFired(
				@Param("cid") Integer cid,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.branch_id = :bid "
				+ "group by p.id "
				+ "order by p.lastname, ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> sortPartyByLastnameCidBid(
				@Param("cid") Integer cid,
				@Param("bid") Integer bid,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.branch_id = :bid "
				+ "and e.date_fired is null "
				+ "group by p.id "
				+ "order by p.lastname, ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> sortPartyByLastnameCidBidFired(
				@Param("cid") Integer cid,
				@Param("bid") Integer bid,
				Pageable pageable);
		
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.position_id = :posId "
				+ "and e.date_fired is null "
				+ "group by p.id "
				+ "order by p.lastname, ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> sortPartyByLastnameCidPos(
				@Param("cid") Integer cid,
				@Param("posId") Integer posId,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.position_id = :posId "
				+ "group by p.id "
				+ "order by p.lastname, ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> sortPartyByLastnameCidPosFired(
				@Param("cid") Integer cid,
				@Param("posId") Integer posId,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.branch_id = :bid "
				+ "and e.position_id = :posId "
				+ "and e.date_fired is null "
				+ "group by p.id "
				+ "order by p.lastname, ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> sortPartyByLastnameCidBidPos(
				@Param("cid") Integer cid,
				@Param("bid") Integer bid,
				@Param("posId") Integer posId,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.branch_id = :bid "
				+ "and e.position_id = :posId "
				+ "group by p.id "
				+ "order by p.lastname, ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> sortPartyByLastnameCidBidPosFired(
				@Param("cid") Integer cid,
				@Param("bid") Integer bid,
				@Param("posId") Integer posId,
				Pageable pageable);
		
		
		
		// GET EMPLOYEE LIST
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.date_fired is null "
				+ "group by p.id "
				+ "ORDER BY ?#{#pageable}",
				nativeQuery = true)
		Page<Party> getPartyByCid(
				@Param("cid") Integer cid,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "group by p.id "
				+ "ORDER BY ?#{#pageable}",
				nativeQuery = true)
		Page<Party> getPartyByCidFired(
				@Param("cid") Integer cid,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.branch_id = :bid "
				+ "and e.date_fired is null "
				+ "group by p.id "
				+ "ORDER BY ?#{#pageable}",
				nativeQuery = true)
		Page<Party> getPartyByCidBid(
				@Param("cid") Integer cid,
				@Param("bid") Integer bid,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.branch_id = :bid "
				+ "group by p.id "
				+ "ORDER BY ?#{#pageable}",
				nativeQuery = true)
		Page<Party> getPartyByCidBidFired(
				@Param("cid") Integer cid,
				@Param("bid") Integer bid,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.position_id = :posId "
				+ "and e.date_fired is null "
				+ "group by p.id "
				+ "ORDER BY ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> getPartyByCidPos(
				@Param("cid") Integer cid,
				@Param("posId") Integer posId,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.position_id = :posId "
				+ "group by p.id "
				+ "ORDER BY ?#{#pageable}", 
				nativeQuery = true)
		Page<Party> getPartyByCidPosFired(
				@Param("cid") Integer cid,
				@Param("posId") Integer posId,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.branch_id = :bid "
				+ "and e.position_id = :posId "
				+ "group by p.id "
				+ "ORDER BY ?#{#pageable}",
				nativeQuery = true)
		Page<Party> getPartyByCidBidPos(
				@Param("cid") Integer cid,
				@Param("bid") Integer bid,
				@Param("posId") Integer posId,
				Pageable pageable);
		
		@Query(value = "Select * from Party p " 
				+ "left join employee e on e.party_id = p.id "
				+ "where p.company = :cid "
				+ "and e.branch_id = :bid "
				+ "and e.position_id = :posId "
				+ "and e.date_fired is null "
				+ "group by p.id "
				+ "ORDER BY ?#{#pageable}",
				nativeQuery = true)
		Page<Party> getPartyByCidBidPosFired(
				@Param("cid") Integer cid,
				@Param("bid") Integer bid,
				@Param("posId") Integer posId,
				Pageable pageable);
		
		
	// *****************************************************************************
	
	
	
	@Query(value = "SELECT * FROM party p where "
			+ "p.company = ?1 "
			+ "and p.party_type = ?2",
			nativeQuery = true)
	Iterable<Party> findAllByPt(@Param("cid") Integer cid, 
			@Param("pt") String pt);
	
//	select p.* from party p
//	where 
//	exists (select e.* from employee e 
//			where 
//	        e.party_id = p.id
//	        and e.company_id = 1
//			and e.branch_id = 2);
	
	@Query("from Party p where "
			+ "exists (select e.id from Employee e "
					+ "where e.party.id = p.id "
					+ "and e.company.id = ?1 "
					+ "and e.branch.id = ?2 "
					+ "and e.dateHired <= ?3)")
	Iterable<Party> getAllStaffByBranch( 
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("dt") Date dt);
	

	@Query(value = "SELECT * FROM party p where "
			+ "p.company = ?1 "
			+ "and p.passport_number = ?2 "
//			+ "and p.residency = " + Country.COUNTRY_UZ
			+ " order by p.id desc "
			+ "limit 1",
			nativeQuery = true)
	Party findByPassportNoUZ(@Param("cid") Integer cid, 
			@Param("pno") String pno);
	
	@Query("from Party c "
			+ "where "
			+ "c.company = ?1 and "
			+ "c.original is not null")
	List<Party> getDuplicatedStaffList(Integer cid);
	
	// Working with
	// Method = GET
	// http://localhost:12014/party/search/modifyParty?id=1&partyType=BANK		
	//	@Transactional
	//	@Modifying(clearAutomatically = true)
	//	@Query("update Party p set p.partyType.name =:partyType where p.id =:id")
	//	void modifyParty(@Param("id") Integer id, @Param("partyType") String partyType);
	
	// delete operation isn't exposed via rest
    @Override
    @RestResource(exported = false)
    void delete(Long id);
 
    @Override
    @RestResource(exported = false)
    void delete(Party entity);
 
    // We don't expose this method via rest here as we want to extend the logic.
    // It is exposed in PartyController.
	//    @Override
	//    @RestResource(exported=false)
	//    Party save(Party party);	
}
	 