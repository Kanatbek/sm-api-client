package com.cordialsr.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.AwardCase;

@RepositoryRestResource(collectionResourceRel = "awardCase", path = "awardCase")
public interface AwardCaseDao extends CrudRepository<AwardCase, Integer> {

}
