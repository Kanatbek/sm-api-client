package com.cordialsr.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.Photo;
import com.cordialsr.domain.projection.PhotoProjection;


@RepositoryRestResource(collectionResourceRel = "photo", path = "photo", 
						excerptProjection=PhotoProjection.class)
public interface PhotoDao extends CrudRepository<Photo, Long> {
}
