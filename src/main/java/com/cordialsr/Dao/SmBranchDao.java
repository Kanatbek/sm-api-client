
package com.cordialsr.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.SmBranch;

@RepositoryRestResource(collectionResourceRel = "smBranch", path = "smBranch")
public interface SmBranchDao extends CrudRepository<SmBranch, String> {

}
