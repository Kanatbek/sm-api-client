package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.CrmReason;

@RepositoryRestResource(collectionResourceRel = "reason", path = "reason")
public interface CrmReasonDao extends JpaRepository<CrmReason, String>{

}
