package com.cordialsr.Dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;

import com.cordialsr.domain.SmVisitCollector;

@RepositoryRestResource(collectionResourceRel = "smVisitCollector", path = "smVisitCollector")
public interface SmVisitCollectorDao extends CrudRepository<SmVisitCollector, Long>{
		
	@Query(value = "Select * from sm_visit_collector c where "
			+ "(c.in_date between :dt1 and :dt2) "
			+ "and c.empl_id = :emplId",
			nativeQuery = true)
	List<SmVisitCollector> getVisits(
			@Param("dt1") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt1,
			@Param("dt2") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt2,
			@Param("emplId") Long emplId);
	
	@Query(value = "select * from sm_visit_collector c "
			+ "left join contract e on e.id = c.contract_id "
			+ "where (c.in_date between :dateStart and :dateEnd) "
			+ "and e.customer = :customerId "
			+ "and c.empl_id = :emplId ",
			nativeQuery = true)
	List<SmVisitCollector> getCustomersVisit(
			@Param("dateStart") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateStart,
			@Param("dateEnd") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dateEnd,
			@Param("emplId") Long emplId,
			@Param("customerId") Long customerId);
}
