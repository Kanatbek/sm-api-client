package com.cordialsr.Dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.AddressType;

@RepositoryRestResource(collectionResourceRel = "addressType", path = "addressType")
public interface AddressTypeDao extends PagingAndSortingRepository<AddressType, Integer>  {
	
}
	 