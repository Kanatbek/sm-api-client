package com.cordialsr.Dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;

import com.cordialsr.Dao.custrepo.FinReportRepo;
import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.FinEntry;
import com.cordialsr.domain.projection.FinEntryProjection;

@RepositoryRestResource(collectionResourceRel = "finentry", path = "finentry",
						excerptProjection = FinEntryProjection.class)
public interface FinEntryDao extends JpaRepository<FinEntry, Long>, FinReportRepo {
	
	@Query("from FinEntry c where c.finDoc.id = ?1")
	Iterable<FinEntry> allByFd(@Param("fdId") Long fdId);
	
	@Query("from FinEntry c where c.finDoc.id = ?1")
	List<FinEntry> allByFinDoc(@Param("fdId") Long fdId);
	
	@Query(value = "SELECT * FROM fin_entry c " +
			"left join fin_doc f on f.id = c.fin_doc_id " + 
			"where " +
			"coalesce(f.storno, 0) = 0 " +
			"and c.kassa_bank_id = ?1 and c.gl_account in ('1010', '1030') " +
			"order by c.ddate asc",
			nativeQuery = true)
	List<FinEntry> allByKb(@Param("kb") Integer kb);

	@Query(value = "select * from fin_entry f " +
			"left join payroll p on p.refkey = f.refkey " +
			"left join contract c on c.id = p.contract_id " + 
			"where " + 
			"p.sum < 0 " +
			"and f.refkey = ?2 " +
			"and p.contract_id = ?1",
			nativeQuery = true)
	List<FinEntry> getFinEntryPayrollLess(Long conId, String refkey);
	
	@Query(value = "SELECT * FROM fin_entry c " +
			"left join fin_doc f on f.id = c.fin_doc_id " + 
			"where " +
			"coalesce(f.storno, 0) = 0 " +
			"and c.kassa_bank_id = ?1 and c.gl_account in ('1010', '1030') " +
			"and c.ddate >= ?2 " +
			"order by c.ddate asc",
			nativeQuery = true)
	List<FinEntry> allByKbFrom(@Param("kb") Integer kb,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("ds") Date ds);
	
	@Query(value = "SELECT * FROM fin_entry c " +
			"left join fin_doc f on f.id = c.fin_doc_id " + 
			"where " +
			"coalesce(f.storno, 0) = 0 " +
			"and c.kassa_bank_id = ?1 and c.gl_account in ('1010', '1030') " +
			"and c.ddate <= ?2 " +
			"order by c.ddate asc",
			nativeQuery = true)
	List<FinEntry> allByKbTill(@Param("kb") Integer kb,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("de") Date de);
	
	@Query(value = "SELECT * FROM fin_entry c " +
			"left join fin_doc f on f.id = c.fin_doc_id " + 
			"where " +
			"coalesce(f.storno, 0) = 0 " +
			"and c.kassa_bank_id = ?1 and c.gl_account in ('1010', '1030') " +
			"and c.ddate >= ?2 " +
			"and c.ddate <= ?3 " +
			"order by c.ddate asc",
			nativeQuery = true)
	List<FinEntry> allByKbFromTill(@Param("kb") Integer kb,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("ds") Date ds,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("de") Date de);
	
	// ************************************************************************************************
	
	@Query(value = "SELECT * FROM fin_entry c " + 
			"where "
			+ "c.company = ?1 "
			+ "and c.party_id = ?2 "
			+ "and c.wcurrency = ?3 " 
			+ "and c.gl_account = '3350' " +
			"order by c.ddate asc",
			nativeQuery = true)
	List<FinEntry> allBySb(
			@Param("cid") Integer cid,
			@Param("sb") Long sb,
			@Param("cur") String cur);
	
	@Query(value = "SELECT * FROM fin_entry c " + 
			"where "
			+ "c.company = ?1 "
			+ "and c.party_id = ?2 "
			+ "and c.wcurrency = ?3 " 
			+ "and c.gl_account = '3350' " +
			"and c.ddate >= ?4 " +
			"order by c.ddate asc",
			nativeQuery = true)
	List<FinEntry> allBySbFrom(
			@Param("cid") Integer cid,
			@Param("sb") Long sb,
			@Param("cur") String cur,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("ds") Date ds);
	
	@Query(value = "SELECT * FROM fin_entry c " + 
			"where "
			+ "c.company = ?1 "
			+ "and c.party_id = ?2 "
			+ "and c.wcurrency = ?3 " 
			+ "and c.gl_account = '3350' " +
			"and c.ddate <= ?4 " +
			"order by c.ddate asc",
			nativeQuery = true)
	List<FinEntry> allBySbTill(
			@Param("cid") Integer cid,
			@Param("sb") Long sb,
			@Param("cur") String cur,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("de") Date de);
	
	@Query(value = "SELECT * FROM fin_entry c " + 
			"where "
			+ "c.company = ?1 "
			+ "and c.party_id = ?2 "
			+ "and c.wcurrency = ?3 " 
			+ "and c.gl_account = '3350' " +
			"and c.ddate >= ?4 " +
			"and c.ddate <= ?5 " +
			"order by c.ddate asc",
			nativeQuery = true)
	List<FinEntry> allBySbFromTill(
			@Param("cid") Integer cid,
			@Param("sb") Long sb,
			@Param("cur") String cur,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("ds") Date ds,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("de") Date de);
	
}
