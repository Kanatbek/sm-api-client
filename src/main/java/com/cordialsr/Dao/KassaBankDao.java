package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.Dao.custrepo.KassaBankRepo;
import com.cordialsr.domain.KassaBank;
import com.cordialsr.domain.projection.KassaBankProjection;

@RepositoryRestResource(collectionResourceRel = "kassabank", path = "kassabank", 
excerptProjection = KassaBankProjection.class)
public interface KassaBankDao extends PagingAndSortingRepository<KassaBank, Integer>, KassaBankRepo {

	@Query("from KassaBank c where c.company.id = ?1"
			+ " and c.isBank = false")
	Iterable<KassaBank> allKaByCo(
					@Param("cid") Integer cid);	
	
	@Query("from KassaBank c where c.branch.id = ?1 and c.isBank = false")
	Iterable<KassaBank> allKaByBr(
					@Param("bid") Integer bid);
	
	@Query("from KassaBank c where c.branch.id = ?1 and c.isBank = true")
	Iterable<KassaBank> allBaByBr(
					@Param("bid") Integer bid);
	
	@Query("from KassaBank c where c.isBank = true and c.company.id = ?1")
	Iterable<KassaBank> allBaByCo(
					@Param("cid") Integer cid);
	
	@Query("from KassaBank c where c.company.id = ?1")
	Iterable<KassaBank> allKaBaByCo(
					@Param("cid") Integer cid);
	
	// @Query("from KassaBank c where (c.isBank = true and c.company.id = ?1)"
	@Query("from KassaBank c where c.company.id = ?1 and c.branch.id = ?2")
	Iterable<KassaBank> allKaBaByCoBr(
					@Param("cid") Integer cid,
					@Param("bid") Integer bid);
	
	@Query(value = "select * from kassa_bank c "
			+ "where c.branch = ?1 "
			+ "and c.currency = ?2 "
			+ "and c.is_bank = false "
			+ "order by c.id asc "
			+ "limit 1 ", 
			nativeQuery = true)
	KassaBank getKassaByBrAndCur(
					@Param("bid") Integer bid,
					@Param("cur") String cur);
	
	@Query(value = "select k.* from kassa_bank k "
			+ "where k.account_number = ?1 "
			+ "order by k.id asc "
			+ "limit 1", 
			nativeQuery = true)
	KassaBank findByAccountNumber(String accountNumber);
	
	@Query(value = "select k.* from kassa_bank k "
			+ "where k.account_number = ?1 "
			+ "order by k.id asc "
			+ "limit 1", 
			nativeQuery = true)
	KassaBank findByAccountNumberByParam(@Param("accountNumber") String accountNumber);
	
	@Query("from KassaBank c where c.branch.id = ?1 "
			+ "and c.currency.currency = ?2 "
			+ "and c.isBank = true")
	KassaBank getBankAccountByBrAndCur(
					@Param("bid") Integer bid,
					@Param("cur") String cur);
}
