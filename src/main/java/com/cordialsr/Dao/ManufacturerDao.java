package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.Manufacturer;

@RepositoryRestResource(collectionResourceRel = "manufacturer", path = "manufacturer")
public interface ManufacturerDao extends CrudRepository<Manufacturer, String>{
	
	@Query("from Manufacturer i where "
			+ "i.company.id = ?1")
			Iterable<Manufacturer>findAllByCompany(@Param("company") Integer company);
}
