package com.cordialsr.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.OrderItem;
import com.cordialsr.domain.projection.OrderItemProjection;

@RepositoryRestResource(collectionResourceRel = "orderItem", path = "orderItem",
						excerptProjection = OrderItemProjection.class)
public interface OrderItemDao extends CrudRepository<OrderItem, Integer>{
	

	
}