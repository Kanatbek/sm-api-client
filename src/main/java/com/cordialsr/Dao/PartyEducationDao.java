package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.PartyEducation;

@RepositoryRestResource(collectionResourceRel = "party_education", path = "party_education")
public interface PartyEducationDao extends JpaRepository<PartyEducation, Long>{

}
