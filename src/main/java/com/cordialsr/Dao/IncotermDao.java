package com.cordialsr.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.Incoterm;

@RepositoryRestResource(collectionResourceRel = "incoterm", path = "incoterm")
public interface IncotermDao extends CrudRepository<Incoterm, String>{

}
