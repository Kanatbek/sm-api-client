package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.InvSubCategory;

@RepositoryRestResource(collectionResourceRel = "invSubCategory", path = "invSubCategory")
public interface InvSubCategoryDao extends CrudRepository<InvSubCategory, Integer>{
	
	@Query("from InvSubCategory i where "
			+ "i.invMainCategory.id = ?1")
			Iterable<InvSubCategory>findAllByInvMainCategory(@Param("invMainCategory") Integer invMainCategory);
}
