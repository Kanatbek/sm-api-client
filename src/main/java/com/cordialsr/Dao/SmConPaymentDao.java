package com.cordialsr.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cordialsr.domain.SmConPayment;


public interface SmConPaymentDao extends JpaRepository<SmConPayment, Long>{

	@Query("select p from SmConPayment p where p.contractNumber = :conNo")
	List<SmConPayment> getSmPaymentsByContractNumber(@Param("conNo") String conNo);
	
}
