package com.cordialsr.Dao;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.InvItemSn;
import com.cordialsr.domain.projection.InvItemSnProjection;

@RepositoryRestResource(collectionResourceRel = "iisn", path = "iisn", excerptProjection = InvItemSnProjection.class)
public interface InvItemSnDao extends CrudRepository<InvItemSn, Long> {

	@Query("from InvItemSn i where i.invoiceItem.id = ?1")
	ArrayList<InvItemSn> findByInvItem(@Param("invItem") Long invItemId);

	@Query("from InvItemSn i where i.serialNumber = ?1")
	ArrayList<InvItemSn> findBySerNum(@Param("serNum") String serialNumber);
	
}
