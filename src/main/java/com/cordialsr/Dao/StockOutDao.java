package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.StockOut;
import com.cordialsr.domain.projection.StockOutProjection;

@RepositoryRestResource(collectionResourceRel = "sout", path = "sout",
						excerptProjection = StockOutProjection.class)
public interface StockOutDao extends CrudRepository<StockOut, Long> {

	@Query(value = "SELECT * FROM stock_out c "  
			+ "where c.serial_number = ?1 "
			+ "LIMIT 1",
			nativeQuery = true)
	StockOut findByJustSerialNumber(@Param("ser_num") String ser_num);

}
