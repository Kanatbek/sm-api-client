package com.cordialsr.Dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;

import com.cordialsr.Dao.custrepo.ConPayScheduleRepo;
import com.cordialsr.domain.ContractPaymentSchedule;
import com.cordialsr.domain.projection.ContractPaymentScheduleProjection;

@RepositoryRestResource(collectionResourceRel = "cps", path = "cps", 
						excerptProjection = ContractPaymentScheduleProjection.class)
public interface ContractPaymentScheduleDao extends ConPayScheduleRepo, CrudRepository<ContractPaymentSchedule, Long> {

	@Query("from ContractPaymentSchedule c where "
			+ "c.contract.company.id = ?1 "
			+ "and c.contract.branch.id = ?2 "
			+ "and c.paymentDate <= ?3 "
			+ "and c.contract.contractStatus.id <> 4 "
			+ "and not exists ("
			+ "		select d.id from ContractPaymentSchedule d "
			+ "		where d.contract.id = c.contract.id "
			+ "		and d.isFirstpayment = true "
			+ "		and d.summ > d.paid "
			+ "		and d.id <> c.id) "
			+ "and c.summ > c.paid")
	List<ContractPaymentSchedule> getAllReceivables(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("dte") Date dte
		);
	
	@Query("from ContractPaymentSchedule c where "
			+ "c.contract.id = ?1 "
			+ "and c.paymentDate <= ?2 "
			+ "and c.summ > c.paid")
	List<ContractPaymentSchedule> getContractReceivables(
			@Param("conId") Integer conId,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("dte") Date dte
		);
	
	@Query("from ContractPaymentSchedule c where "
			+ "c.contract.company.id = ?1 "
			+ "and c.contract.branch.id = ?2 "
			+ "and c.paymentDate <= ?3 "
			+ "and c.contract.contractStatus.id <> 4 "
			+ "and c.isFirstpayment = true "
			+ "and c.summ > c.paid")
	List<ContractPaymentSchedule> getReceivableFirstPayments(
			@Param("cid") Integer cid,
			@Param("bid") Integer bid,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("dte") Date dte
		);
	
	@Query("from ContractPaymentSchedule c where "
			+ "c.contract.id = ?1 "
			+ "and not exists ("
			+ "		select d.id from ContractPaymentSchedule d "
			+ "		where d.contract.id = c.contract.id "
			+ "		and d.isFirstpayment = true "
			+ "		and d.summ > d.paid "
			+ "		and d.id <> c.id) ")
	List<ContractPaymentSchedule> getConPaySchById(
			@Param("cid") Long cid);
	
	@Query(value = "from ContractPaymentSchedule c "
			+ "left join Contract e on e.id = c.contract_id "
			+ "where c.payment_date is not null "
			+ "and c.payment_date between ?2 and ?3 "
			+ "and e.collector is not null "
			+ "and e.collector = ?1",
			nativeQuery = true)
	List<ContractPaymentSchedule> getConPaySheByCollector(
			@Param("collectorId") Long collectorId,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("dateStart") Date dateStart,
			@DateTimeFormat(pattern = "yyyy-MM-dd") @Param("dateEnd") Date endDate);
}
