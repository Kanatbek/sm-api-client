package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.InvoiceItem;
import com.cordialsr.domain.projection.InvoiceItemProjection;

@RepositoryRestResource(collectionResourceRel = "invoiceItem", path = "invoiceItem",
						excerptProjection = InvoiceItemProjection.class)
public interface InvoiceItemDao extends JpaRepository<InvoiceItem, Long> {
	
	@Query(value = "Select * from invoice_item c where c.id = ?1 LIMIT 1",
			nativeQuery = true)
			InvoiceItem findById(@Param("id") Long id);

}
