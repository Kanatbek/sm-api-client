package com.cordialsr.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.FinGlAccount;

@RepositoryRestResource(collectionResourceRel = "glacc", path = "glacc")
public interface FinGlAccountDao extends CrudRepository<FinGlAccount, String>{
	
}
