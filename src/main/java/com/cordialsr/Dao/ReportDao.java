package com.cordialsr.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


import com.cordialsr.domain.Report;
import com.cordialsr.domain.projection.PlanFactProjection;

@RepositoryRestResource(collectionResourceRel = "report", path = "report", 
						excerptProjection=PlanFactProjection.class)
public interface ReportDao extends CrudRepository<Report, Long> {
	
	
	@Query(value = "from Report c "
			+ "where c.year = ?1 "
			+ "and c.month = ?2 ")
	List<Report> getReport(@Param("year") Integer year, @Param("mon") Integer mon);

	@Query(value = "from Report c "
			+ "where c.company.id = ?1 "
			+ "and c.year = ?2 "
			+ "and c.month = ?3 ")
	List<Report> getReportByCompany(@Param("cid") Integer cid, 
									@Param("year") Integer year,
									@Param("mon") Integer mon);
	
}
