package com.cordialsr.Dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.OrderHeader;
import com.cordialsr.domain.projection.OrderProjection;

@RepositoryRestResource(collectionResourceRel = "order", path = "order",
						excerptProjection = OrderProjection.class)
public interface OrderDao extends CrudRepository<OrderHeader, Integer>{
	

	@Query(value = "SELECT * from order_header s "
			+ "where s.company = ?1 "
			+ "and s.branch = ?2",
			nativeQuery = true)
	Iterable<OrderHeader> getAllOrderItems(@Param("cid") Long cid,
			@Param("bid") Long bid);
	
	@Query(value = "SELECT * FROM order_header c " +
			"where c.company = ?1 " + 
			"and c.branch = ?2",
			nativeQuery = true)
	Iterable<OrderHeader> findByInventory(@Param("cid") Integer cid,
			@Param("bid") Integer bid);

	@Query(value = "SELECT * from order_header s "
			+ "where s.company = ?1 "
			+ "and s.branch != ?2",
			nativeQuery = true)
	Iterable<OrderHeader> getAllOrderExcHead(@Param("cid") Long cid,
			@Param("bid") Long bid);
	
	
	
}
