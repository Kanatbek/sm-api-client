package com.cordialsr.Dao.custrepo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.cordialsr.domain.FinEntry;
import com.cordialsr.domain.FinGlAccount;
import com.cordialsr.dto.Balance;
import com.cordialsr.dto.Money;

public class FinEntryDaoImpl implements FinReportRepo {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<Balance> getInitialBalance(Integer cid, String ds) {
		String query = "select " + 
				"gl, " + 
				"a.akt, " + 
				"case when a.AKT = 0 then (db - cr) else (cr - db) end as balance, " + 
				"currency " + 
				"from " + 
					" (SELECT " + 
					" f.gl_account as gl, " + 
					" sum(case when f.dc = 'D' then f.dsumm else 0 end) as db, " + 
					" sum(case when f.dc = 'C' then f.dsumm else 0 end) as cr, " + 
					" f.dcurrency as currency " + 
					" FROM fin_entry f " + 
					" where " + 
					" f.company = " + cid + 
					" and f.ddate < '" + ds + "' " + 
					" group by f.gl_account) as summ " + 
				"left join fin_gl_account a on a.code = summ.gl;";
		
		List<Balance> res = new ArrayList<>();
		Query q = this.em.createNativeQuery(query);
		List<Object[]> resultList = q.getResultList();
		
		for (Object[] o : resultList) {
			Balance balance = new Balance();
			if (o[0] != null)
				balance.setGlCode(String.valueOf(o[0]));
			Boolean akt = null;
			if (o[1] != null)
				akt = (Boolean) o[1];
			
			if (!akt && o[2] != null) {
				Money debet = new Money();
				debet.setSumm(new BigDecimal(String.valueOf(o[2])));
				if (o[3] != null) {
					debet.setCurrency(String.valueOf(o[3]));
				}
				balance.setDebet(debet);
			} else if (akt && o[2] != null) {
				Money credit = new Money();
				credit.setSumm(new BigDecimal(String.valueOf(o[2])));
				if (o[3] != null) {
					credit.setCurrency(String.valueOf(o[3]));
				}
				balance.setCredit(credit);
			}
			res.add(balance);
		}
			
		return res;
	}
	
	@Override
	public List<Balance> getInitialTotals(Integer cid, String ds) {
		String query = "SELECT " + 
				"f.gl_account as gl, " + 
				"sum(case when f.dc = 'D' then f.dsumm else 0 end) as db, " + 
				"sum(case when f.dc = 'C' then f.dsumm else 0 end) as cr, " +
				"f.dcurrency " + 
				"FROM fin_entry f " + 
				"where  " + 
				"f.company = " + cid +
				" and f.ddate < '" + ds + "' " +
				"group by f.gl_account;";
		
		List<Balance> res = new ArrayList<>();
		Query q = this.em.createNativeQuery(query);
		List<Object[]> resultList = q.getResultList();
		
		for (Object[] o : resultList) {
			Balance balance = new Balance();
			if (o[0] != null)
				balance.setGlCode(String.valueOf(o[0]));
			Money debet = new Money();
			if (o[1] != null)
				debet.setSumm(new BigDecimal(String.valueOf(o[1])));
			Money credit = new Money();
			if (o[2] != null)
				credit.setSumm(new BigDecimal(String.valueOf(o[2])));
			if (o[3] != null) {
				debet.setCurrency(String.valueOf(o[3]));
				credit.setCurrency(String.valueOf(o[3]));
			}
			balance.setDebet(debet);
			balance.setCredit(credit);
			res.add(balance);
		}
			
		return res;
	}
	
	@Override
	public List<Balance> getTurnoverBalance(Integer cid, String ds, String de) {
		String query = "SELECT " + 
				"f.gl_account as gl, " + 
				"sum(case when f.dc = 'D' then f.dsumm else 0 end) as db, " + 
				"sum(case when f.dc = 'C' then f.dsumm else 0 end) as cr, " +
				"f.dcurrency " + 
				"FROM fin_entry f " + 
				"where  " + 
				"f.company = " + cid +
				" and f.ddate between '" + ds + "' " +
				"and '" + de + "' " + 
				"group by f.gl_account;";
		
		List<Balance> res = new ArrayList<>();
		Query q = this.em.createNativeQuery(query);
		List<Object[]> resultList = q.getResultList();
		
		for (Object[] o : resultList) {
			Balance balance = new Balance();
			if (o[0] != null)
				balance.setGlCode(String.valueOf(o[0]));
			Money debet = new Money();
			if (o[1] != null)
				debet.setSumm(new BigDecimal(String.valueOf(o[1])));
			Money credit = new Money();
			if (o[2] != null)
				credit.setSumm(new BigDecimal(String.valueOf(o[2])));
			if (o[3] != null) {
				debet.setCurrency(String.valueOf(o[3]));
				credit.setCurrency(String.valueOf(o[3]));
			}
			balance.setDebet(debet);
			balance.setCredit(credit);
			res.add(balance);
		}
			
		return res;
	}

	@Override
	public List<Balance> getInnerCfsInitialBalance(Integer cid, String glCode, String ds) {
		String query = "select " + 
				"gl, " + 
				"cfs, " + 
				"a.akt, " + 
				"case when a.AKT = 0 then (db - cr) else (cr - db) end as balance, " + 
				"currency " + 
				"from " + 
					" (SELECT " + 
					" f.gl_account as gl, " +
					"  f.CASHFLOW_STATEMENT as cfs, " + 
					" sum(case when f.dc = 'D' then f.dsumm else 0 end) as db, " + 
					" sum(case when f.dc = 'C' then f.dsumm else 0 end) as cr, " + 
					" f.dcurrency as currency " + 
					" FROM fin_entry f " + 
					" where " + 
					" f.company = " + cid + 
					" and f.gl_account = '" + glCode + "'" +
					" and f.ddate < '" + ds + "' " + 
					" group by f.gl_account, f.CASHFLOW_STATEMENT) as summ " + 
				"left join fin_gl_account a on a.code = summ.gl;";
		
		List<Balance> res = new ArrayList<>();
		Query q = this.em.createNativeQuery(query);
		List<Object[]> resultList = q.getResultList();
		
		for (Object[] o : resultList) {
			Balance balance = new Balance();
			if (o[0] != null)
				balance.setGlCode(String.valueOf(o[0]));
			if (o[1] != null) {
				balance.setPartyId(Long.valueOf(String.valueOf(o[1])));
			}
			Boolean akt = null;
			if (o[2] != null)
				akt = (Boolean) o[2];
			
			if (!akt && o[3] != null) {
				Money debet = new Money();
				debet.setSumm(new BigDecimal(String.valueOf(o[3])));
				if (o[4] != null) {
					debet.setCurrency(String.valueOf(o[4]));
				}
				balance.setDebet(debet);
			} else if (akt && o[3] != null) {
				Money credit = new Money();
				credit.setSumm(new BigDecimal(String.valueOf(o[3])));
				if (o[4] != null) {
					credit.setCurrency(String.valueOf(o[4]));
				}
				balance.setCredit(credit);
			}
			res.add(balance);
		}
			
		return res;
	}

	@Override
	public List<Balance> getInnerPartyInitialBalance(Integer cid, String glCode, Integer cfs, String ds) {
		String query = "select " + 
				"gl, " + 
				"party, " + 
				"a.akt, " + 
				"case when a.AKT = 0 then (db - cr) else (cr - db) end as balance, " + 
				"currency " + 
				"from " + 
					" (SELECT " + 
					" f.gl_account as gl, " +
					" f.party_id as party, " + 
					" sum(case when f.dc = 'D' then f.dsumm else 0 end) as db, " + 
					" sum(case when f.dc = 'C' then f.dsumm else 0 end) as cr, " + 
					" f.dcurrency as currency " + 
					" FROM fin_entry f " + 
					" where " + 
					" f.company = " + cid + 
					" and f.gl_account = '" + glCode + "'";
		
		String cfsStr = (cfs != null ) ? " = " + cfs : " is null ";
		query += " and f.CASHFLOW_STATEMENT " + cfsStr +
					" and f.ddate < '" + ds + "' " + 
					" group by f.gl_account, f.CASHFLOW_STATEMENT, f.party_id) as summ " + 
				"left join fin_gl_account a on a.code = summ.gl;";
		
		List<Balance> res = new ArrayList<>();
		Query q = this.em.createNativeQuery(query);
		List<Object[]> resultList = q.getResultList();
		
		for (Object[] o : resultList) {
			Balance balance = new Balance();
			if (o[0] != null)
				balance.setGlCode(String.valueOf(o[0]));
			if (o[1] != null) {
				balance.setPartyId(Long.valueOf(String.valueOf(o[1])));
			}
			Boolean akt = null;
			if (o[2] != null)
				akt = (Boolean) o[2];
			
			if (!akt && o[3] != null) {
				Money debet = new Money();
				debet.setSumm(new BigDecimal(String.valueOf(o[3])));
				if (o[4] != null) {
					debet.setCurrency(String.valueOf(o[4]));
				}
				balance.setDebet(debet);
			} else if (akt && o[3] != null) {
				Money credit = new Money();
				credit.setSumm(new BigDecimal(String.valueOf(o[3])));
				if (o[4] != null) {
					credit.setCurrency(String.valueOf(o[4]));
				}
				balance.setCredit(credit);
			}
			res.add(balance);
		}
			
		return res;
	}
	
	@Override
	public List<Balance> getInnerCfsTurnoverBalance(Integer cid, String glCode, String ds, String de) {
		String query = "SELECT " + 
				"f.gl_account as gl, " +
				"f.CASHFLOW_STATEMENT, " + 
				"sum(case when f.dc = 'D' then f.dsumm else 0 end) as db, " + 
				"sum(case when f.dc = 'C' then f.dsumm else 0 end) as cr, " +
				"f.dcurrency " + 
				"FROM fin_entry f " + 
				"where  " + 
				"f.company = " + cid +
				" and f.gl_account = '" + glCode + "'" +
				" and f.ddate between '" + ds + "' and '" + de + "' " + 
				"group by f.gl_account, f.CASHFLOW_STATEMENT;";
		
		List<Balance> res = new ArrayList<>();
		Query q = this.em.createNativeQuery(query);
		List<Object[]> resultList = q.getResultList();
		
		for (Object[] o : resultList) {
			Balance balance = new Balance();
			if (o[0] != null)
				balance.setGlCode(String.valueOf(o[0]));
			if (o[1] != null) {
				balance.setPartyId(Long.valueOf(String.valueOf(o[1])));
			}
			Money debet = new Money();
			if (o[2] != null)
				debet.setSumm(new BigDecimal(String.valueOf(o[2])));
			Money credit = new Money();
			if (o[3] != null)
				credit.setSumm(new BigDecimal(String.valueOf(o[3])));
			if (o[4] != null) {
				debet.setCurrency(String.valueOf(o[4]));
				credit.setCurrency(String.valueOf(o[4]));
			}
			balance.setDebet(debet);
			balance.setCredit(credit);
			res.add(balance);
		}
			
		return res;
	}
	
	@Override
	public List<Balance> getInnerPartyTurnoverBalance(Integer cid, String glCode, Integer cfs, String ds, String de) {
		String query = "SELECT " + 
				"f.gl_account as gl, " +
				"f.party_id, " + 
				"sum(case when f.dc = 'D' then f.dsumm else 0 end) as db, " + 
				"sum(case when f.dc = 'C' then f.dsumm else 0 end) as cr, " +
				"f.dcurrency " + 
				"FROM fin_entry f " + 
				"where  " + 
				"f.company = " + cid +
				" and f.gl_account = '" + glCode + "'";
		
		String cfsStr = (cfs != null ) ? " = " + cfs : " is null ";
		query += " and f.CASHFLOW_STATEMENT " + cfsStr +
				" and f.ddate between '" + ds + "' and '" + de + "' " + 
				"group by f.gl_account, f.CASHFLOW_STATEMENT, f.party_id;";
		
		List<Balance> res = new ArrayList<>();
		Query q = this.em.createNativeQuery(query);
		List<Object[]> resultList = q.getResultList();
		
		for (Object[] o : resultList) {
			Balance balance = new Balance();
			if (o[0] != null)
				balance.setGlCode(String.valueOf(o[0]));
			if (o[1] != null) {
				balance.setPartyId(Long.valueOf(String.valueOf(o[1])));
			}
			Money debet = new Money();
			if (o[2] != null)
				debet.setSumm(new BigDecimal(String.valueOf(o[2])));
			Money credit = new Money();
			if (o[3] != null)
				credit.setSumm(new BigDecimal(String.valueOf(o[3])));
			if (o[4] != null) {
				debet.setCurrency(String.valueOf(o[4]));
				credit.setCurrency(String.valueOf(o[4]));
			}
			balance.setDebet(debet);
			balance.setCredit(credit);
			res.add(balance);
		}
			
		return res;
	}

	@Override
	public List<FinEntry> getPartyTransactions(Integer cid, Long pid, String ds, String de) {
		// TODO Auto-generated method stub
		return null;
	}
	
	// ****************************************************************************************************
	
	@Override
	public List<Map<String, Object>> getCashflowGeneral(Integer cid, String ds, String de) {
		String query = "select " + 
				"code, " + 
				"name, " + 
				"cf, " +
				"cfname, " + 
				"db, " +
				"cr, " +
				"db-cr as total, " + 
				"currency " + 
				"from " + 
				"(select " + 
					"a.code, " + 
					"a.name, " + 
					"s.id as cf, " + 
					"s.name as cfname, " + 
					"sum(case when e.dc = 'D' then e.dsumm else 0 end) as db, " + 
					"sum(case when e.dc = 'C' then e.dsumm else 0 end) as cr, " + 
					"e.dcurrency as currency " + 
					"from fin_entry e " + 
					"left join fin_cashflow_statement s on s.id = e.CASHFLOW_STATEMENT " + 
					"left join fin_activity_type a on a.code = e.ACT_TYPE " + 
					"where  " + 
					"e.company = " + cid + 
					" and e.gl_account in ('" + FinGlAccount.A_ST_CASH_ON_HAND_1010 + 
										"', '" + FinGlAccount.A_ST_CASH_ON_BANK_1030 +"') " + 
					"and e.ddate between '" + ds + "' and '" + de + "' " + 
					// "and s.ACT_TYPE = '" + FinActivityType.TYPE_OPERATIVE + "' " + 
					// "and e.CASHFLOW_STATEMENT <> 3 " + 
					"group by e.CASHFLOW_STATEMENT, e.dcurrency) as dbcr " + 
				"order by code desc, cfname asc;";
		
		List<Map<String, Object>> res = new ArrayList<>();
		Query q = this.em.createNativeQuery(query);
		List<Object[]> resultList = q.getResultList();
		
		for (Object[] o : resultList) {
			Map<String, Object> cf = new HashMap<>();
			if (o[0] != null) cf.put("code", o[0]);
			if (o[1] != null) cf.put("name", o[1]);
			if (o[2] != null) cf.put("cfId", o[2]);
			if (o[3] != null) cf.put("cfName", o[3]);
			if (o[4] != null) cf.put("db", o[4]);
			if (o[5] != null) cf.put("cr", o[5]);
			if (o[6] != null) cf.put("sum", o[6]);
			if (o[7] != null) cf.put("currency", o[7]);
			res.add(cf);
		}
			
		return res;
	}
	
	@Override
	public List<Map<String, Object>> getCashflowBranch(Integer cid, Integer cfId, String ds, String de) {
		String query = "select " + 
				"branch, " + 
				"cf, " +
				"cfname, " + 
				"db, " +
				"cr, " +
				"db-cr as total, " + 
				"currency " + 
				"from " + 
				"(select " + 
					"e.branch, " + 
					"s.id as cf, " + 
					"s.name as cfname, " + 
					"sum(case when e.dc = 'D' then e.dsumm else 0 end) as db, " + 
					"sum(case when e.dc = 'C' then e.dsumm else 0 end) as cr, " + 
					"e.dcurrency as currency " + 
					"from fin_entry e " + 
					"left join fin_cashflow_statement s on s.id = e.CASHFLOW_STATEMENT " + 
					"where  " + 
					"e.company = " + cid + 
					" and e.gl_account in ('" + FinGlAccount.A_ST_CASH_ON_HAND_1010 + 
										"', '" + FinGlAccount.A_ST_CASH_ON_BANK_1030 +"') " + 
					"and e.ddate between '" + ds + "' and '" + de + "' " + 
					// "and s.ACT_TYPE = '" + FinActivityType.TYPE_OPERATIVE + "' " + 
					"and e.CASHFLOW_STATEMENT = " + cfId + 
					" group by e.branch, e.CASHFLOW_STATEMENT, e.dcurrency) as dbcr " + 
				"order by branch, cfname asc;";
		
		List<Map<String, Object>> res = new ArrayList<>();
		Query q = this.em.createNativeQuery(query);
		List<Object[]> resultList = q.getResultList();
		
		for (Object[] o : resultList) {
			Map<String, Object> cf = new HashMap<>();
			if (o[0] != null) cf.put("branch", o[0]);
			if (o[1] != null) cf.put("cfId", o[1]);
			if (o[2] != null) cf.put("cfName", o[2]);
			if (o[3] != null) cf.put("db", o[3]);
			if (o[4] != null) cf.put("cr", o[4]);
			if (o[5] != null) cf.put("sum", o[5]);
			if (o[6] != null) cf.put("currency", o[6]);
			res.add(cf);
		}
			
		return res;
	}
	
	@Override
	public List<Map<String, Object>> getCashflowDetailed(Integer cid, Integer bid, Integer cfId, String ds, String de) {
		String query = "select " + 
				"cf, " +
				"cfname, " + 
				"party_id, " + 
				"fio, " +
				"db, " +
				"cr, " +
				"db-cr as total, " + 
				"currency, " +
				"info, " +
				"finDocId, " +
				"docno " +
				"from " + 
				"(select " + 
					"s.id as cf, " + 
					"s.name as cfname, " +
					"e.party_id, " +
				    "concat(coalesce(p.firstname, ''), ' ',  coalesce(p.lastname, ''),  ' ',  coalesce(p.middlename, ''), ' ', coalesce(p.company_name, '')) as fio, " +
				    "e.info, " + 
					"(case when e.dc = 'D' then e.wsumm else 0 end) as db, " + 
					"(case when e.dc = 'C' then e.wsumm else 0 end) as cr, " + 
					"e.wcurrency as currency, " +
					"e.FIN_DOC_id as finDocId, " +
					"f.docno " +
					"from fin_entry e " + 
					"left join fin_cashflow_statement s on s.id = e.CASHFLOW_STATEMENT " + 
					"left join party p on p.id = e.party_id " +
					"left join fin_doc f on f.id = e.FIN_DOC_id " +
					"where  " + 
					"e.company = " + cid + 
					" and e.branch = " + bid + 
					" and e.gl_account in ('" + FinGlAccount.A_ST_CASH_ON_HAND_1010 + 
										"', '" + FinGlAccount.A_ST_CASH_ON_BANK_1030 +"') " + 
					"and e.ddate between '" + ds + "' and '" + de + "' " +
					// "and e.wcurrency = '" + currency + "' " +
					// "and s.ACT_TYPE = '" + FinActivityType.TYPE_OPERATIVE + "' " + 
					"and e.CASHFLOW_STATEMENT = " + cfId +
					") as dbcr " + 
				"order by fio desc;";
		
		List<Map<String, Object>> res = new ArrayList<>();
		Query q = this.em.createNativeQuery(query);
		List<Object[]> resultList = q.getResultList();
		
		for (Object[] o : resultList) {
			Map<String, Object> cf = new HashMap<>();
			if (o[0] != null) cf.put("cfId", o[0]);
			if (o[1] != null) cf.put("cfName", o[1]);
			if (o[2] != null) cf.put("partyId", o[2]);
			if (o[3] != null) cf.put("fio", o[3]);
			if (o[4] != null) cf.put("db", o[4]);
			if (o[5] != null) cf.put("cr", o[5]);
			if (o[6] != null) cf.put("sum", o[6]);
			if (o[7] != null) cf.put("currency", o[7]);
			if (o[8] != null) cf.put("info", o[8]);
			if (o[9] != null) cf.put("finDocId", o[9]);
			if (o[10] != null) cf.put("docno", o[10]);
			res.add(cf);
		}
			
		return res;
	}

	@Override
	public Money getCashBalance(Integer cid, String ds, String equal) {
		String query = "select " + 
				"(db - cr) as balance, " + 
				"currency " + 
				"from " + 
				"(select " + 
					"sum(case when e.dc = 'D' then e.dsumm else 0 end) as db, " + 
					"sum(case when e.dc = 'C' then e.dsumm else 0 end) as cr, " + 
					"e.dcurrency as currency " + 
					"from fin_entry e " +
					"where " +
					"e.company = " + cid + 
					" and e.ddate <" + equal + " '" + ds + "' " +
					"and e.gl_account in ('" + FinGlAccount.A_ST_CASH_ON_HAND_1010 + "', '" 
											+ FinGlAccount.A_ST_CASH_ON_BANK_1030 + "') " + 
					"group by e.dcurrency " + 
				") as dbcr;";
		
		Query q = this.em.createNativeQuery(query);
		List<Object[]> resultList = q.getResultList();
		
		Money balance = null;
		for (Object[] o : resultList) {
			balance = new Money();
			if (o[0] != null) balance.setSumm(new BigDecimal(String.valueOf(o[0])));
			if (o[1] != null) balance.setCurrency(String.valueOf(o[1]));
		}
			
		return balance;
	}
	
	
	@Override
	public List<Balance> getTurnoverBalanceForSection(Integer cid, String ds, String de, String section) {
		String query = "select " + 
				"code, " + 
				"sum(debet), " + 
				"sum(credit), " + 
				"dcurrency " + 
				"from " + 
				"(SELECT  " + 
				"gl.code,  " + 
				"case when e.dc = 'D' then e.dsumm else 0 end as debet, " + 
				"case when e.dc = 'C' then e.dsumm else 0 end as credit, " +
				"e.dcurrency " +
				"FROM fin_entry e " + 
				"left join fin_gl_account gl on gl.code = e.gl_account " + 
				"where  " + 
				"e.company = " + cid +
				" and e.gl_account like '" + section + "%' " + 
				"and e.ddate between '" + ds + "' and '" + de + "' " + 
				") as asf " + 
				"group by code; " + 
				"";
		
		List<Balance> res = new ArrayList<>();
		Query q = this.em.createNativeQuery(query);
		List<Object[]> resultList = q.getResultList();
		
		for (Object[] o : resultList) {
			Balance balance = new Balance();
			if (o[0] != null)
				balance.setGlCode(String.valueOf(o[0]));
			Money debet = new Money();
			if (o[1] != null)
				debet.setSumm(new BigDecimal(String.valueOf(o[1])));
			Money credit = new Money();
			if (o[2] != null)
				credit.setSumm(new BigDecimal(String.valueOf(o[2])));
			if (o[3] != null) {
				debet.setCurrency(String.valueOf(o[3]));
				credit.setCurrency(String.valueOf(o[3]));
			}
			balance.setDebet(debet);
			balance.setCredit(credit);
			res.add(balance);
		}
			
		return res;
	}
}
