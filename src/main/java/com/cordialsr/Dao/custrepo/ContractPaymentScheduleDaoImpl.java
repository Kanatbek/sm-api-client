package com.cordialsr.Dao.custrepo;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class ContractPaymentScheduleDaoImpl implements ConPayScheduleRepo {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<Map<String, Object>> getAllReceivablesTotal(Integer cid, Integer bid, Date dte) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		String query = "select " + 
				"c.id, " + 
				"sum(p.summ - p.paid) as paymentDue, " + 
				"c.currency " + 
				"from contract_payment_schedule p " + 
				"left join contract c on p.contract_id = c.id " + 
				"where " + 
				"c.company = " + cid +
				" and c.branch = " + bid +
				" and p.payment_date <= '" + df.format(dte) + "' " +   
				// " and c.contract_status_id <> 4 " +
				"and c.storno = 0 " +
				"and p.summ > p.paid " + 
				"group by c.id;";
		
		Query q = this.em.createNativeQuery(query);
		List<Object[]> resultList = q.getResultList();
		
		List<Map<String, Object>> res = new ArrayList<>();
		for (Object[] o : resultList) {
			Map<String, Object> r = new HashMap<>();
			if (o[0] != null) r.put("conId", new Long(String.valueOf(o[0])));
			if (o[1] != null) r.put("summ", new BigDecimal(String.valueOf(o[1])));
			if (o[2] != null) r.put("currency", String.valueOf(o[1]));
			res.add(r);
		}
		
		return res;
	}

}
