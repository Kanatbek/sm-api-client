package com.cordialsr.Dao.custrepo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.cordialsr.domain.FinDocType;
import com.cordialsr.domain.PlanFact;

public class PlanFactDaoImpl implements PlanFactRepo {

	@PersistenceContext
	private EntityManager em;

	public Map<String, Object> resultMap = new HashMap<>();

	@Override
	public List<Map<String, Object>> getAllCollectorsPlanFact(Integer cid, Integer bid, Date dte) {
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
		
		String query = "SELECT " + 
				"c.collector, " + 
				"c.currency " + 
				"FROM " + 
				"contract c " + 
				"where " + 
				"c.company = " + cid + 
				" and c.service_branch = " + bid + 
				" and coalesce(c.storno, 0) = 0 " +
				"group by c.collector, c.currency;";
		
		List<Map<String, Object>> res = new ArrayList<>();
		Query q = this.em.createNativeQuery(query);
		List<Object[]> resultList = q.getResultList();
		Calendar cal = Calendar.getInstance();
		cal.setTime(dte);
		for (Object[] o : resultList) {
			Map<String, Object> result = new HashMap<>();
			PlanFact pf = new PlanFact();
			if (o[0] != null)
				result.put("collector", o[0]);
			if (o[1] != null)
				result.put("currency", o[1]);
			
			// FACT
			Map<String, Object> fact = new HashMap<>();
			if (!String.valueOf(result.get("collector")).equals("null")) {
				fact = getCollectorFactForMonth(cid, bid, 
						Long.valueOf(String.valueOf(result.get("collector"))), 
						String.valueOf(result.get("currency")), 
						(cal.get(Calendar.MONTH) + 1), cal.get(Calendar.YEAR));
			} else {
				fact = getCollectorFactForMonth(cid, bid, null, 
						String.valueOf(result.get("currency")), 
						(cal.get(Calendar.MONTH) + 1), cal.get(Calendar.YEAR));
				
			}
			if (fact.size() > 0) {
				result.put("paid_past", fact.get("paid_past"));
				result.put("paid_current", fact.get("paid_current"));
				result.put("paid_ahead", fact.get("paid_ahead"));
				result.put("paid_total", fact.get("paid_total"));
			}

			
			// REMAIN
			
			Map<String, Object> remain = new HashMap<>();
			if (!String.valueOf(result.get("collector")).equals("null")) {
				remain = getCollectorRemain(cid, bid, 
						Long.valueOf(String.valueOf(result.get("collector"))), 
						String.valueOf(result.get("currency")), cal.getTime());
			} else {
				remain = getCollectorRemain(cid, bid, null, 
						String.valueOf(result.get("currency")), cal.getTime());
				
			}
			if (remain.size() > 0) {
				result.put("remain", remain.get("remain"));
			}
			
			res.add(result);
		}
		return res;
	}
	
	public Map<String, Object> getCollectorRemain(Integer cid, Integer bid, Long pid, String currency, Date dte) {
		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
		
		String query = "SELECT " + 
				"sum(p.summ - coalesce(p.paid, 0)) as remain, " +
				"c.currency " + 
				"FROM " + 
				"contract c " + 
				"left join contract_payment_schedule p on c.id = p.contract_id " + 
				"where " + 
				"c.company = " + cid + 
				" and c.service_branch = " + bid + 
				" and p.payment_date <= '" + f.format(dte) + "' " +
				" and coalesce(p.payment_order, 0) > 0 " +
				" and p.summ > coalesce(p.paid, 0) " +
				" and coalesce(c.storno, 0) = 0 " +
				" and c.currency = '" + currency + "'";

				if (pid != null) query += " and c.collector = " + pid;
				else query += " and c.collector is null ";
		
		Map<String, Object> result = new HashMap<>();
		Query q = this.em.createNativeQuery(query);
		List<Object[]> resultList = q.getResultList();
		Calendar cal = Calendar.getInstance();
		cal.setTime(dte);
		for (Object[] o : resultList) {
			if (o[0] != null)
				result.put("remain", o[0]);
			if (o[1] != null)
				result.put("currency", o[1]);
			
		}	
		return result;
	}

	private Map<String, Object> getCollectorFactForMonth(Integer cid, Integer bid, Long eid, String currency, Integer month, Integer year) {
		
		String query = "select " + 
				"f.employee_id, " + 
				
				"sum(case when ((MONTH(f.payment_due) < " + month 
					 + " and YEAR(f.payment_due) = " + year + ") or (YEAR(f.payment_due) < " + year + ")) " + 
				"		 then f.wsumm else 0 end) as paid_past, " +
						
				"sum(case when (MONTH(f.payment_due) = " + month 
					 + " and YEAR(f.payment_due) = " + year + ") " + 
				"		 then f.wsumm else 0 end) as paid_current, " +
					 
				"sum(case when ((MONTH(f.payment_due) > " + month 
					 + " and YEAR(f.payment_due) = " + year + ") or (YEAR(f.payment_due) > " + year + ")) " + 
				"		 then f.wsumm else 0 end) as paid_ahead, " +
					 
				"sum(f.wsumm) as paid_total, " + 
				"f.wcurrency " + 
				"from fin_doc f " + 
				"where " + 
				" f.company = " + cid;
				if (eid != null) query += " and f.employee_id = " + eid;
				else query += " and f.employee_id is null and f.branch = " + bid;
				
		query += " and MONTH(f.doc_date) = " + month + "" + 
				" and YEAR(f.doc_date) = " + year + "" + 
				" and f.findoc_type = '" + FinDocType.TYPE_CP_CONTRACT_PAYMENT + "'" +
				" and f.payment_order > 0 " + 
				" and coalesce(f.storno, 0) = 0 " + 
				" group by f.wcurrency;";
		
		Map<String, Object> res = new HashMap<>();
		Query q = this.em.createNativeQuery(query);
		List<Object[]> resultList = q.getResultList();
		
		for (Object[] o : resultList) {
			res = new HashMap<>();
			if (o[0] != null)
				res.put("collector", o[0]);
			if (o[1] != null)
				res.put("paid_past", o[1]);
			if (o[2] != null)
				res.put("paid_current", o[2]);
			if (o[3] != null)
				res.put("paid_ahead", o[3]);
			if (o[4] != null)
				res.put("paid_total", o[4]);
			if (o[5] != null)
				res.put("currency", o[5]);
		}
		
		return res;
	}
	
}
