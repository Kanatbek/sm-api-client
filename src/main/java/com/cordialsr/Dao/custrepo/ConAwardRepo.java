package com.cordialsr.Dao.custrepo;

import java.util.List;
import java.util.Map;

public interface ConAwardRepo {
	List<Map<String, Object>> getAllContractOverdueSum(Integer bid, String de);
	List<Map<String, Object>> getSalesAndRentsCount(Integer cid, Integer bid, Integer pos, String ds, String de);
}
