package com.cordialsr.Dao.custrepo;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ConPayScheduleRepo {
	List<Map<String, Object>> getAllReceivablesTotal(Integer cid, Integer bid, Date dte);
}
