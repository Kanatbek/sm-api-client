package com.cordialsr.Dao.custrepo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class KassaBankDaoImpl implements KassaBankRepo {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Map<String, Object> getKassaBalance(Integer kbid, String date) {
		// SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
		
		String query = "select "
				+ "sum(debet)-sum(credit) as balance, currency " 
				+ "from ( " + 
				"	Select" + 
				"	case when e.dc = 'D' then e.wsumm else 0 end as debet," + 
				"	case when e.dc = 'C' then e.wsumm else 0 end as credit," + 
				"	e.wcurrency as currency" + 
				"	from Fin_Entry e" +
				"   left join fin_doc f on f.id = e.FIN_DOC_id" +
				"	where" +			 
				"   coalesce(f.storno, 0) = 0" +
				"	and e.kassa_bank_id = " + kbid + 
				"	and e.ddate <= '" + date + "'" + 
				"   and (e.gl_account = '1010' or e.gl_account = '1030')" + 
				") as debcred";
		
		Query q = this.em.createNativeQuery(query);
		List<Object[]> r = q.getResultList();
		// Object r = q.getSingleResult();
		Map<String, Object> resultMap = new HashMap<>();
		for (Object[] o : r) {
			if (o[0] != null)
				resultMap.put("balance", o[0]);
			if (o[1] != null)
				resultMap.put("currency", o[1]);
			break;
		}
		return resultMap;
	}
}
