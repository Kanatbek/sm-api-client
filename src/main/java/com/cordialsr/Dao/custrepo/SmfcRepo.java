package com.cordialsr.Dao.custrepo;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface SmfcRepo {
	Object actCancCount();
	List<Map<String, Object>> getCaremenCurrentPlanRemain(Integer cid, Date date) throws Exception;
	List<Map<String, Object>> getCaremenCurrentPlanDone(Integer cid, Date date) throws Exception;
	List<Map<String, Object>> getCaremenCurrentPlan(Integer cid, Date date) throws Exception;
}
