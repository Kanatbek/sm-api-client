package com.cordialsr.Dao.custrepo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class PayrollDaoImpl implements PayrollRepo {
	
	@PersistenceContext
	private EntityManager em;

	public Map<String, Object> resultMap = new HashMap<>();

	@Override
	public List<Map<String, Object>>  getAllStaffBalanceByBranch(Integer cid, Integer bid, Date dt) {

		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
		
		String query = "select " + 
				"d.party_id," + 
				"sum(d.credit)-sum(d.debet) as balance, " + 
				"d.currency " + 
				"from ( " + 
				"	Select " + 
				"		e.party_id," + 
				"		case when e.dc = 'D' then e.wsumm else 0 end as debet," + 
				"		case when e.dc = 'C' then e.wsumm else 0 end as credit," + 
				"		e.wcurrency as currency" + 
				"    from Fin_Entry e" + 
				"    left join fin_doc f on f.id = e.FIN_DOC_id" +
				"    where" +			 
				"    	coalesce(f.storno, 0) = 0" +
				"		and e.company = " + cid +
				"		and e.gl_account = '3350'" +
				" 		and e.ddate <= '" + f.format(dt) + "' " + 
				") as d " + 
				"left join party p on p.id = d.party_id " +
				"where " + 
				"exists (select f1.id from fin_doc f1 where " + 
				"	 p.id = f1.party_id " + 
				"	 and f1.company = " + cid + 
				"    and f1.branch = " + bid + 
				"    and f1.findoc_type in ('PP', 'PS', 'PB', 'KI', 'KO'))" +
				"group by d.party_id, d.currency " + 
				"order by p.lastname, p.firstname, p.middlename;";
		
		List<Map<String, Object>> res = new ArrayList<>();
		Query q = this.em.createNativeQuery(query);
		List<Object[]> resultList = q.getResultList();
		
		for (Object[] o : resultList) {
			resultMap = new HashMap<>();
			if (o[0] != null)
				resultMap.put("partyId", o[0]);
			if (o[1] != null)
				resultMap.put("balance", o[1]);
			if (o[2] != null)
				resultMap.put("currency", o[2]);
			res.add(resultMap);
		}
			
		return res;
	}
	
	@Override
	public Map<String, Object>  getSingleStaffBalance(Integer cid, Long partyId, String currency, Date dt) {

		SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
		
		String query = "select " + 
				"d.party_id," + 
				"sum(d.credit)-sum(d.debet) as balance, " + 
				"d.currency " + 
				"from ( " + 
				"	Select " + 
				"		e.party_id," + 
				"		case when e.dc = 'D' then e.wsumm else 0 end as debet," + 
				"		case when e.dc = 'C' then e.wsumm else 0 end as credit," + 
				"		e.wcurrency as currency" + 
				"    from Fin_Entry e" + 
				"    left join fin_doc f on f.id = e.FIN_DOC_id" +
				"    where" +			 
				"    	coalesce(f.storno, 0) = 0" +
				"		and e.company = " + cid +
				"		and e.party_id = " + partyId +
				"		and e.wcurrency = '" + currency + "'" +
				"		and e.gl_account = '3350'" +
				" 		and e.ddate <= '" + f.format(dt) + "' " + 
				") as d " + 
				"group by d.party_id, d.currency;";
		
		Map<String, Object> res = new HashMap<>();
		Query q = this.em.createNativeQuery(query);
		List<Object[]> resultList = q.getResultList();
		
		for (Object[] o : resultList) {
			res = new HashMap<>();
			if (o[0] != null)
				res.put("partyId", o[0]);
			if (o[1] != null)
				res.put("balance", o[1]);
			if (o[2] != null)
				res.put("currency", o[2]);
		}
			
		return res;
	}
}
