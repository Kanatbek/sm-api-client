package com.cordialsr.Dao.custrepo;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface PlanFactRepo {
	List<Map<String, Object>> getAllCollectorsPlanFact(Integer cid, Integer bid, Date dte);
}
