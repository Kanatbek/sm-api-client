package com.cordialsr.Dao.custrepo;

import java.util.List;
import java.util.Map;

import com.cordialsr.domain.FinEntry;
import com.cordialsr.dto.Balance;
import com.cordialsr.dto.Money;

public interface FinReportRepo {
	List<Balance> getInitialBalance(Integer cid, String ds);
	List<Balance> getTurnoverBalance(Integer cid, String ds, String de);
	
	List<Balance> getInnerCfsInitialBalance(Integer cid, String glCode, String ds);
	List<Balance> getInnerCfsTurnoverBalance(Integer cid, String glCode, String ds, String de);
	
	List<Balance> getInnerPartyInitialBalance(Integer cid, String glCode, Integer cfs, String ds);
	List<Balance> getInnerPartyTurnoverBalance(Integer cid, String glCode, Integer cfs, String ds, String de);
	
	List<FinEntry> getPartyTransactions(Integer cid, Long pid, String ds, String de);
	
	List<Balance> getInitialTotals(Integer cid, String ds);
	
	List<Map<String, Object>> getCashflowGeneral(Integer cid, String ds, String de);
	List<Map<String, Object>> getCashflowBranch(Integer cid, Integer cfId, String ds, String de);
	List<Map<String, Object>> getCashflowDetailed(Integer cid, Integer bid, Integer cfId, String ds, String de);
	
	Money getCashBalance(Integer cid, String ds, String equal);
	
	List<Balance> getTurnoverBalanceForSection(Integer cid, String ds, String de, String section);
}