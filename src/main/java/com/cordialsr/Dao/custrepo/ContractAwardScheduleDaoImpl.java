package com.cordialsr.Dao.custrepo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.cordialsr.domain.Position;
import com.cordialsr.general.GeneralUtil;

public class ContractAwardScheduleDaoImpl implements ConAwardRepo {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<Map<String, Object>> getAllContractOverdueSum(Integer bid, String de) {
		String query = "select " + 
				"p.contract_id, " + 
				"sum(p.summ) - sum(p.paid) as remain, " +
				"c.currency " +
				"from contract c " + 
				"left join contract_payment_schedule p on p.contract_id = c.id " + 
				"where " + 
				"c.branch = " + bid + 
				" and p.payment_date <= '" + de +"' " + 
				"and p.contract_id is not null " + 
				"group by p.contract_id;";
		
		Query q = this.em.createNativeQuery(query);
		List<Object[]> resultList = q.getResultList();
		
		List<Map<String, Object>> res = new ArrayList<>();
		for (Object[] o : resultList) {
			Map<String, Object> r = new HashMap<>();
			if (o[0] != null) r.put("conId", new Long(String.valueOf(o[0])));
			if (o[1] != null) r.put("summ", new BigDecimal(String.valueOf(o[1])));
			if (o[2] != null) r.put("currency", String.valueOf(o[2]));
			res.add(r);
		}
		
		return res;
	}

	@Override
	public List<Map<String, Object>> getSalesAndRentsCount(Integer cid, Integer bid, Integer pos, String ds,
			String de) {
		
		String brStr = "";
		if (!GeneralUtil.isEmptyInteger(bid)) brStr += " and c.branch = " + bid; 
		
		String position = "";
		switch(pos) {
			case Position.POS_DEALER: position = "dealer"; break;
			case Position.POS_DEMOSEC: position = "demosec"; break;
			case Position.POS_MANAGER: position = "manager"; break;
			case Position.POS_DIRECTOR: position = "director"; break;
			case Position.POS_COORDINATOR: position = "coordinator"; brStr = ""; break;
			case Position.POS_CHIEF_COORDINATOR: brStr = ""; break;
		}
		
		String query = "select "; 
		query += "sum(case when c.is_rent then 1 else 0 end) as rent, " + 
				"sum(case when c.is_rent then 0 else 1 end) as sale, " + 
				"count(c.id) as total"; 
		if (position.length() > 0) query += ", c." + position; 
		query += " from " + 
				"contract c " + 
				"where " + 
				"c.company = " + cid;
		if (brStr.length() > 0) query += brStr;
		query += " and c.storno = 0 " + 
				" and c.date_signed between '" + ds + "' and '" + de + "' "; 
		if (position.length() > 0) {
			query += "and c." + position + " is not null ";
			query += "group by c." + position + ";";
		}
		
		Query q = this.em.createNativeQuery(query);
		List<Object[]> resultList = q.getResultList();
		
		List<Map<String, Object>> res = new ArrayList<>();
		for (Object[] o : resultList) {
			Map<String, Object> r = new HashMap<>();
			if (o[0] != null) r.put("rent", new Integer(String.valueOf(o[0])));
			if (o[1] != null) r.put("sale", new Integer(String.valueOf(o[1])));
			if (o[2] != null) r.put("total", new Integer(String.valueOf(o[2])));
			if (position.length() > 0 && o[3] != null) r.put("emplId", new Long(String.valueOf(o[3])));
			res.add(r);
		}
		
		return res;
	}
	
}
