package com.cordialsr.Dao.custrepo;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface PayrollRepo {
	List<Map<String, Object>> getAllStaffBalanceByBranch(Integer cid, Integer bid, Date dt);
	Map<String, Object>  getSingleStaffBalance(Integer cid, Long partyId, String currency, Date dt);
}
