package com.cordialsr.Dao.custrepo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.cordialsr.domain.PlanFact;

public class SmFilterChangeDaoImpl implements SmfcRepo {

	@PersistenceContext
	private EntityManager em;
	
	public Map<String, Object> resultMap = new HashMap<>();
	
	@Override
	public Object actCancCount() {
		String query =  "select " + 
						"sum(active) as active, " + 
						"sum(cancelled) as cancelled " + 
						"from (SELECT " + 
							"case when c.enabled = 1 then 1 else 0 end as active, " + 
							"case when c.enabled = 0 then 1 else 0 end as cancelled " + 
							"FROM cordialsr.sm_filter_change c) as calcTable";
		Query q = this.em.createNativeQuery(query);
		List<Object[]> r = q.getResultList();
		// Object r =	q.getSingleResult();
		resultMap = new HashMap<>();
		for (Object[] o: r) {
			if (o[0] != null) resultMap.put("active", o[0]);
			if (o[1] != null) resultMap.put("cancelled", o[1]);
			break;
		}
		return resultMap;
	}

	@Override
	public List<Map<String, Object>>  getCaremenCurrentPlanRemain(Integer cid, Date date) throws Exception {
		try {
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			String dt = df.format(date);
			
			String query = "select " + 
					"c.careman, " + 
					"count(c.id) " + 
					"from " + 
					"contract c " + 
					"left join sm_filter_change f on f.CONTRACT = c.id " + 
					"where  " + 
					"coalesce(c.storno, 0) = 0  " + 
					"and c.company = " + cid + 
					" and ( " + 
					"	f.PR_NEXT between (LAST_DAY('" + dt + "' - INTERVAL 1 MONTH) + INTERVAL 1 DAY) and (LAST_DAY('" + dt + "')) " + 
					"	or f.F1_NEXT between (LAST_DAY('" + dt + "' - INTERVAL 1 MONTH) + INTERVAL 1 DAY) and (LAST_DAY('" + dt + "')) " + 
					"   or f.F2_NEXT between (LAST_DAY('" + dt + "' - INTERVAL 1 MONTH) + INTERVAL 1 DAY) and (LAST_DAY('" + dt + "')) " + 
					"   or f.F3_NEXT between (LAST_DAY('" + dt + "' - INTERVAL 1 MONTH) + INTERVAL 1 DAY) and (LAST_DAY('" + dt + "')) " + 
					"   or f.F4_NEXT between (LAST_DAY('" + dt + "' - INTERVAL 1 MONTH) + INTERVAL 1 DAY) and (LAST_DAY('" + dt + "')) " + 
					"   or f.F5_NEXT between (LAST_DAY('" + dt + "' - INTERVAL 1 MONTH) + INTERVAL 1 DAY) and (LAST_DAY('" + dt + "')) " + 
					") " + 
					"group by c.careman;";
			
			Query q = this.em.createNativeQuery(query);
			List<Object[]> r = q.getResultList();
			
			List<Map<String, Object>> res = new ArrayList<>();
			
			for (Object[] o : r) {
				Map<String, Object> result = new HashMap<>();
			
				if (o[0] != null) result.put("careman", o[0]);
				if (o[1] != null) result.put("remain", o[1]);

				res.add(result);
			}
			return res;
		} catch (Exception e) {
			throw e;
		}
	}
	
	
	@Override
	public List<Map<String, Object>>  getCaremenCurrentPlanDone(Integer cid, Date date) throws Exception {
		try {
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			String dt = df.format(date);
			
			String query = "select " + 
					"s.careman, " + 
					"count(s.id)  " + 
					"from " + 
					"sm_service s " + 
					"where " + 
					"s.company = " + cid +
					" and coalesce(s.overdue, 0) = 0 " + 
					"and s.SDATE between (LAST_DAY('" + dt + "' - INTERVAL 1 MONTH) + INTERVAL 1 DAY) and (LAST_DAY('" + dt + "')) " + 
					"group by s.careman;";
			
			Query q = this.em.createNativeQuery(query);
			List<Object[]> r = q.getResultList();
			
			List<Map<String, Object>> res = new ArrayList<>();
			
			for (Object[] o : r) {
				Map<String, Object> result = new HashMap<>();
			
				if (o[0] != null) result.put("careman", o[0]);
				if (o[1] != null) result.put("done", o[1]);

				res.add(result);
			}
			return res;
		} catch (Exception e) {
			throw e;
		}
	}
	
	
	@Override
	public List<Map<String, Object>>  getCaremenCurrentPlan(Integer cid, Date date) throws Exception {
		try {
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			String dt = df.format(date);
			
			String query = "select  " + 
					"r.careman, " + 
					"(coalesce(r.remain, 0) + coalesce( " + 
					"	( " + 
					"		select " + 
					"		count(s.id) " + 
					"		from  " + 
					"		sm_service s  " + 
					"		where  " + 
					"		s.company = " + cid +
					"		and coalesce(s.overdue, 0) = 0 " + 
					"		and s.careman = r.careman " + 
					"		and s.SDATE between (LAST_DAY('" + dt + "' - INTERVAL 1 MONTH) + INTERVAL 1 DAY) and (LAST_DAY('" + dt + "')) " + 
					"		group by s.careman " + 
					"	) " + 
					", 0)) as plan " + 
					"from " + 
					"(select  " + 
					"	c.careman, " + 
					"	count(c.id) as remain " + 
					"	from contract c  " + 
					"	left join sm_filter_change f on f.CONTRACT = c.id " + 
					"	where  " + 
					"	c.company = " + cid +
					"	and coalesce(c.storno, 0) = 0  " + 
					"	and ( " + 
					"		f.PR_NEXT between (LAST_DAY('" + dt + "' - INTERVAL 1 MONTH) + INTERVAL 1 DAY) and (LAST_DAY('" + dt + "')) " + 
					"		or f.F1_NEXT between (LAST_DAY('" + dt + "' - INTERVAL 1 MONTH) + INTERVAL 1 DAY) and (LAST_DAY('" + dt + "')) " + 
					"		or f.F2_NEXT between (LAST_DAY('" + dt + "' - INTERVAL 1 MONTH) + INTERVAL 1 DAY) and (LAST_DAY('" + dt + "')) " + 
					"		or f.F3_NEXT between (LAST_DAY('" + dt + "' - INTERVAL 1 MONTH) + INTERVAL 1 DAY) and (LAST_DAY('" + dt + "')) " + 
					"		or f.F4_NEXT between (LAST_DAY('" + dt + "' - INTERVAL 1 MONTH) + INTERVAL 1 DAY) and (LAST_DAY('" + dt + "')) " + 
					"		or f.F5_NEXT between (LAST_DAY('" + dt + "' - INTERVAL 1 MONTH) + INTERVAL 1 DAY) and (LAST_DAY('" + dt + "')) " + 
					"	) " + 
					"	group by c.careman " + 
					") as r;";
			
			Query q = this.em.createNativeQuery(query);
			List<Object[]> r = q.getResultList();
			
			List<Map<String, Object>> res = new ArrayList<>();
			
			for (Object[] o : r) {
				Map<String, Object> result = new HashMap<>();
			
				if (o[0] != null) result.put("careman", o[0]);
				if (o[1] != null) result.put("plan", o[1]);

				res.add(result);
			}
			return res;
		} catch (Exception e) {
			throw e;
		}
	}
	
}
