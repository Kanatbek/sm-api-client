package com.cordialsr.Dao.custrepo;

import java.util.Map;

public interface KassaBankRepo {
	Map<String, Object> getKassaBalance(Integer kbid, String date);
}
