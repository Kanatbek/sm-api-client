package com.cordialsr.Dao;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;

import com.cordialsr.domain.SmService;
import com.cordialsr.domain.projection.SmServiceInlineBranchProjection;

@RepositoryRestResource(collectionResourceRel = "smService", path = "smService", excerptProjection = SmServiceInlineBranchProjection.class)
public interface SmServiceDao extends PagingAndSortingRepository<SmService, Integer> {
	
	@Query("from SmService c where c.id = ?1 ORDER BY id DESC")
	SmService findById(@Param("id") Long id);
	
	@Query("from SmService c where "
			+ "c.customer.id = ?1")
	List<SmService> findAllByCustomer(@Param("customer") Long customer);
	

	@Query("from SmService c where "
			+ "c.contract.id = ?1")
	List<SmService> findAllByContract(@Param("contract") Long contract);
	
	@Query("from SmService c where "
			+ "c.company.id = ?1 "
			+ "and c.branch.id = ?2 ORDER BY id DESC")
	Page<SmService> findAllByBranch(@Param("cid") Integer cid,
									@Param("bid") Integer bid,
									 Pageable pageRequest);
	
	@Query("from SmService c where c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "AND c.type = ?3 ORDER BY id DESC")
	Page<SmService> findAllByBranchType(@Param("cid") Integer cid,
										@Param("bid") Integer bid, 
										@Param("tp") String tp,
										 Pageable pageRequest);	
	
	 
	
	@Query("from SmService c where c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "AND c.sdate >= ?3 ORDER BY id DESC")
	Page<SmService> findAllByBranchFrom(@Param("cid") Integer cid,
			                            @Param("bid") Integer bid, 
	   @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("from") Date from,
		                                     Pageable pageRequest);
	
	@Query("from SmService c where c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "AND c.sdate <= ?3 ORDER BY id DESC")
	Page<SmService> findAllByBranchTo(@Param("cid") Integer cid,
			                          @Param("bid") String bid, 
	 @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("to") Date to,
	 										Pageable pageRequest);
	
	
	
	@Query("from SmService c where c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "AND c.sdate >= ?3 AND c.type = ?4 ORDER BY id DESC")
	Page<SmService> findAllByBranchFromType(@Param("cid") Integer cid,
			                                @Param("bid") Integer bid, 
		   @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("from") Date from, 
												  @Param("tp") String tp,
												   Pageable pageRequest);
	
	@Query("from SmService c where c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "AND c.sdate <= ?3  AND c.type = ?4 ORDER BY id DESC")
	Page<SmService> findAllByBranchToType(@Param("cid") Integer cid,
			                              @Param("bid") Integer bid, 
		 @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("to") Date to, 
												@Param("tp") String tp,
												 Pageable pageRequest);
	
	
		
	@Query("from SmService c where c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "AND c.sdate between ?3 and ?4 ORDER BY id DESC")
	Page<SmService> findAllByBranchFromTo(@Param("cid") Integer cid,
			                              @Param("bid") Integer bid, 
   @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("from") Date from,
   @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("to") Date to,
	 									  Pageable pageRequest);
	
	
	@Query("from SmService c where c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "AND c.type = ?5 "
			+ "AND c.sdate between ?3 and ?4 ORDER BY id DESC")
	Page<SmService> findAllByBranchFromToType(@Param("cid") Integer cid,
			                                  @Param("bid") Integer bid, 
	   @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("from") Date from,
	   @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("to") Date to,
			                                  @Param("tp") String tp,
												 Pageable pageRequest);
	
	///////////////////////////////////////////////////////////////////  With Filter ............................................
	
	
	@Query("from SmService c where "
			+ "c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "and "
			+ "(c.caremanFio like %?3% "
			+ "or c.type like %?3% "
			+ "or c.serviceNumber like %?3% "
			+ "or c.contract.contractNumber like %?3% "
			+ "or c.info like %?3% "
			
			+ "or c.customer.firstname like %?3% "
			+ "or c.customer.lastname like %?3% "
			+ "or c.customer.middlename like %?3% "
			+ "or c.customer.iinBin like %?3% "
			+ "or c.customer.companyName like %?3% "
			+ "or c.customer.passportNumber like %?3% "
			+ ")")
	Page<SmService> findAllByBranchFilter(@Param("cid") Integer cid,
									      @Param("bid") Integer bid,
									      @Param("flt") String flt, 
									 Pageable pageRequest);
	
	@Query("from SmService c where c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "AND c.type = ?3 "
			+ "and "
			+ "(c.caremanFio like %?4% "
			+ "or c.type like %?4% "
			+ "or c.serviceNumber like %?4% "
			+ "or c.contract.contractNumber like %?4% "
			+ "or c.info like %?4% "
			
			+ "or c.customer.firstname like %?4% "
			+ "or c.customer.lastname like %?4% "
			+ "or c.customer.middlename like %?4% "
			+ "or c.customer.iinBin like %?4% "
			+ "or c.customer.companyName like %?4% "
			+ "or c.customer.passportNumber like %?4% "
			
			+ ")"
			+ "ORDER BY id DESC")
	Page<SmService> findAllByBranchTypeFilter(@Param("cid") Integer cid,
									          @Param("bid") Integer bid, 
										      @Param("tp") String tp,
										      @Param("flt") String flt,  
										      Pageable pageRequest);	
	
	 
	
	@Query("from SmService c where c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "AND c.sdate >= ?3 "
			+ "and "
			+ "(c.caremanFio like %?4% "
			+ "or c.type like %?4% "
			+ "or c.serviceNumber like %?4% "
			+ "or c.contract.contractNumber like %?4% "
			+ "or c.info like %?4% "
			
			+ "or c.customer.firstname like %?4% "
			+ "or c.customer.lastname like %?4% "
			+ "or c.customer.middlename like %?4% "
			+ "or c.customer.iinBin like %?4% "
			+ "or c.customer.companyName like %?4% "
			+ "or c.customer.passportNumber like %?4% "
			
			+ ")"
			+ "ORDER BY id DESC")
	Page<SmService> findAllByBranchFromFilter(@Param("cid") Integer cid,
			                                  @Param("bid") Integer bid, 
	   @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("from") Date from,
	   										  @Param("flt") String flt,  
		                                      Pageable pageRequest);
	
	@Query("from SmService c where c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "AND c.sdate <= ?3 " 
			+ "and "
			+ "(c.caremanFio like %?4% "
			+ "or c.type like %?4% "
			+ "or c.serviceNumber like %?4% "
			+ "or c.contract.contractNumber like %?4% "
			+ "or c.info like %?4% "
			
			+ "or c.customer.firstname like %?4% "
			+ "or c.customer.lastname like %?4% "
			+ "or c.customer.middlename like %?4% "
			+ "or c.customer.iinBin like %?4% "
			+ "or c.customer.companyName like %?4% "
			+ "or c.customer.passportNumber like %?4% "
			
			+ ")"
			+ "ORDER BY id DESC")
	Page<SmService> findAllByBranchToFilter(@Param("cid") Integer cid,
			                                @Param("bid") String bid, 
	 @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("to") Date to,
	 										@Param("flt") String flt,  
	 										Pageable pageRequest);
	
	
	
	@Query("from SmService c where c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "AND c.sdate >= ?3 AND c.type = ?4 "
			+ "and "
			+ "(c.caremanFio like %?5% "
			+ "or c.type like %?5% "
			+ "or c.serviceNumber like %?5% "
			+ "or c.contract.contractNumber like %?5% "
			+ "or c.info like %?5% "
			
			+ "or c.customer.firstname like %?5% "
			+ "or c.customer.lastname like %?5% "
			+ "or c.customer.middlename like %?5% "
			+ "or c.customer.iinBin like %?5% "
			+ "or c.customer.companyName like %?5% "
			+ "or c.customer.passportNumber like %?5% "
			
			+ ")"
			+ "ORDER BY id DESC")
	Page<SmService> findAllByBranchFromTypeFilter(@Param("cid") Integer cid,
			                                      @Param("bid") Integer bid, 
		   @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("from") Date from, 
												  @Param("tp") String tp,
												  @Param("flt") String flt,  
												   Pageable pageRequest);
	
	@Query("from SmService c where c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "AND c.sdate <= ?3  AND c.type = ?4 "
			+ "and "
			+ "(c.caremanFio like %?5% "
			+ "or c.type like %?5% "
			+ "or c.serviceNumber like %?5% "
			+ "or c.contract.contractNumber like %?5% "
			+ "or c.info like %?5% "
			
			+ "or c.customer.firstname like %?5% "
			+ "or c.customer.lastname like %?5% "
			+ "or c.customer.middlename like %?5% "
			+ "or c.customer.iinBin like %?5% "
			+ "or c.customer.companyName like %?5% "
			+ "or c.customer.passportNumber like %?5% "
			
			+ ")"
			+ "ORDER BY id DESC")
	Page<SmService> findAllByBranchToTypeFilter(@Param("cid") Integer cid,
			                                    @Param("bid") Integer bid, 
		 @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("to") Date to, 
												@Param("tp") String tp,
												@Param("flt") String flt,  
												 Pageable pageRequest);
	
	
		
	@Query("from SmService c where c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "AND c.sdate between ?3 and ?4 "
			+ "and "
			+ "(c.caremanFio like %?5% "
			+ "or c.type like %?5% "
			+ "or c.serviceNumber like %?5% "
			+ "or c.contract.contractNumber like %?5% "
			+ "or c.info like %?5% "
			
			+ "or c.customer.firstname like %?5% "
			+ "or c.customer.lastname like %?5% "
			+ "or c.customer.middlename like %?5% "
			+ "or c.customer.iinBin like %?5% "
			+ "or c.customer.companyName like %?5% "
			+ "or c.customer.passportNumber like %?5% "
			
			+ ")"
			+ "ORDER BY id DESC")
	Page<SmService> findAllByBranchFromToFilter(@Param("cid") Integer cid,
			                                    @Param("bid") Integer bid, 
         @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("from") Date from,
         @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("to") Date to,
   										        @Param("flt") String flt,  
	 									        Pageable pageRequest);
	
	
	@Query("from SmService c where c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "AND c.type = ?5 "
			+ "AND c.sdate between ?3 and ?4 "
			+ "and "
			+ "(c.caremanFio like %?6% "
			+ "or c.type like %?6% "
			+ "or c.serviceNumber like %?6% "
			+ "or c.contract.contractNumber like %?6% "
			+ "or c.info like %?6% "
			
			+ "or c.customer.firstname like %?6% "
			+ "or c.customer.lastname like %?6% "
			+ "or c.customer.middlename like %?6% "
			+ "or c.customer.iinBin like %?6% "
			+ "or c.customer.companyName like %?6% "
			+ "or c.customer.passportNumber like %?6% "
			
			+ ")"
			+ "ORDER BY id DESC")
	Page<SmService> findAllByBranchFromToTypeFilter(@Param("cid") Integer cid,
			                                        @Param("bid") Integer bid, 
	         @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("from") Date from,
	         @DateTimeFormat(pattern = "yyyy-MM-dd")@Param("to") Date to,
			                                        @Param("tp") String tp,
			                                        @Param("flt") String flt,  
						        					Pageable pageRequest);
	
	


	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Query("from SmService c "
			+ "where c.company.id = ?1 " 
			+ "and c.branch.id = ?2 "
			+ "and c.verified = 0"
            + "ORDER BY id DESC")
	Page<SmService> findAllForCall(@Param("cid") Integer cid,
							 	   @Param("bid") Integer bid,  
							 	      Pageable pageRequest);
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Query(value = "SELECT id, TYPE, SDATE, STIME, ITIME, CAREMAN_FIO, STORNO, BRANCH_CODE, info, contract, company, customer, COST, DISCOUNT, CURRENCY, PREMI_CURRENCY, inventory, refkey, user_added, service_number, enquiry_id, premi, verified, overdue, "
			+ "sum(s.mark) as mark, "  
			+ "careman, branch, early, sm_visit, "  
			+ "count(s.id) as summ, " 
			+ "sum(s.verified = 1) as paid "
			+ "from sm_service s "
			+ "where s.company = ?1 "
			+ "and s.sdate between ?2 and ?3 "
			+ "and coalesce(s.storno, 0) = 0 "
			+ "group by s.CAREMAN_FIO",
			nativeQuery = true)
	ArrayList<SmService> findAllForReport(@Param("cid") Integer cid,
 @DateTimeFormat(pattern = "yyyy-MM-dd") @Param("from") Date from,
 @DateTimeFormat(pattern = "yyyy-MM-dd") @Param("to") Date to);
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Query(value = "SELECT id, TYPE, SDATE, STIME, ITIME, CAREMAN_FIO, STORNO, BRANCH_CODE, info, contract, company, customer, COST, DISCOUNT, CURRENCY, PREMI_CURRENCY, inventory, refkey, user_added, service_number, enquiry_id, premi, verified, overdue, "
			+ "sum(s.mark) as mark, "  
			+ "careman, branch, early, sm_visit, "  
			+ "count(s.id) as summ, " 
			+ "sum(s.verified = 1) as paid "
			+ "from sm_service s "
			+ "where s.company = ?1 "
			+ "and s.sdate between ?2 and ?3 "
			+ "and coalesce(s.storno, 0) = 0 "
			+ "group by YEAR(s.sdate), MONTH(s.sdate)",
			nativeQuery = true)
	ArrayList<SmService> findAllForRepOnPeriod(@Param("cid") Integer cid,
       @DateTimeFormat(pattern = "yyyy-MM-dd") @Param("from") Date from,
       @DateTimeFormat(pattern = "yyyy-MM-dd") @Param("to") Date to);
	
}