package com.cordialsr.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.FinOperation;
import com.cordialsr.domain.projection.FinOperProjection;

@RepositoryRestResource(collectionResourceRel = "finoper", path = "finoper", 
						excerptProjection = FinOperProjection.class)
public interface FinOperDao extends CrudRepository<FinOperation, Integer> {

	@Query("from FinOperation c ORDER by c.id ASC")
	List<FinOperation> getAllOrderedById();
	
	@Query("from FinOperation c "
			+ "where c.transaction.id = ?1 "
			+ "and c.enabled = true "
			+ "ORDER by c.id ASC")
	List<FinOperation> getAllByTrId(@Param("trid") Integer trid);
}
