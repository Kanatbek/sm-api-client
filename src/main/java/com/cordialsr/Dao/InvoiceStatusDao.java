package com.cordialsr.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.InvoiceStatus;

@RepositoryRestResource(collectionResourceRel = "invoiceStatus", path = "invoiceStatus")
public interface InvoiceStatusDao extends CrudRepository<InvoiceStatus, String>{

}




