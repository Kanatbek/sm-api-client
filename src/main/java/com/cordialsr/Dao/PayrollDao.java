package com.cordialsr.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.Dao.custrepo.PayrollRepo;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.Payroll;

@RepositoryRestResource(collectionResourceRel = "payroll", path = "payroll")
public interface PayrollDao extends CrudRepository<Payroll, Long>, PayrollRepo {

	@Query(value = "Select * from payroll c where "
			+ "c.company = ?1 "
			+ "and c.branch = ?2 "
			+ "and c.month = ?3 "
			+ "and c.year = ?4",
			nativeQuery = true)
			List<Payroll> getAllByMonth(
					@Param("cid") Integer cid,
					@Param("bid") Integer bid,
					@Param("mn") Integer mn,
					@Param("yr") Integer yr
				);
	
	@Query(value = "Select * from payroll c where "
			+ "c.company = ?1 "
			+ "and c.branch = ?2 "
			+ "and c.kind = ?3 "
			+ "and c.month = ?4 "
			+ "and c.year = ?5",
			nativeQuery = true)
			List<Payroll> getAllByMonthKind(
					@Param("cid") Integer cid,
					@Param("bid") Integer bid,
					@Param("knd") String knd,
					@Param("mn") Integer mn,
					@Param("yr") Integer yr
				);
	
	@Query(value = "Select c.* from payroll c "
			+ "left join employee e on e.id = c.employee "
			+ "where "
			+ "c.company = ?1 "
			+ "and c.branch = ?2 "
			+ "and c.kind = ?3 "
			+ "and e.position_id = ?4 "
			+ "and c.month = ?5 "
			+ "and c.year = ?6",
			nativeQuery = true)
			List<Payroll> getAllByMonthBranchPos(
					@Param("cid") Integer cid,
					@Param("bid") Integer bid,
					@Param("knd") String knd,
					@Param("pos") Integer pos,
					@Param("mn") Integer mn,
					@Param("yr") Integer yr
				);
	
	@Query(value = "Select * from payroll c "
			+ "left join employee e on e.id = c.employee "
			+ "where "
			+ "c.company = ?1 "
			+ "and c.kind = ?2 "
			+ "and e.position_id = ?3 "
			+ "and c.month = ?4 "
			+ "and c.year = ?5 ",
			nativeQuery = true)
			List<Payroll> getAllByMonthPos(
					@Param("cid") Integer cid,
					@Param("knd") String knd,
					@Param("pos") Integer pos,
					@Param("mn") Integer mn,
					@Param("yr") Integer yr
				);
	
	@Query("from Payroll c where "
			+ "c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "and c.kind = '" + Payroll.KIND_SALARY + "' "
			+ "and c.month = ?3 "
			+ "and c.year = ?4 "
			+ "and c.employee.id = ?5 ")
			Payroll getEmplPostedPayrollSalary(
					@Param("cid") Integer cid,
					@Param("bid") Integer bid,
					@Param("mn") Integer mn,
					@Param("yr") Integer yr,
					@Param("emplId") Long emplId
				);
	
	@Query("from Payroll c where "
			+ "c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "and c.kind = ?3 " 
			+ "and c.month = ?4 "
			+ "and c.year = ?5 "
			+ "and c.party.id = ?6 "
			+ "and c.sum > 0")
			Payroll getPartyPostedPayroll(
					@Param("cid") Integer cid,
					@Param("bid") Integer bid,
					@Param("kind") String kind,
					@Param("mn") Integer mn,
					@Param("yr") Integer yr,
					@Param("pid") Long pid
				);
	
	@Query(value = "select * from payroll c where "
			+ "c.company = ?1 "
			+ "and c.branch = ?2 "
			+ "and c.kind = ?3 "
			+ "and c.month = ?4 "
			+ "and c.year = ?5 "
			+ "and c.employee = ?6 "
			+ "and c.contract_id = ?7 "
			+ "and c.sum > 0 "
			+ "limit 1", nativeQuery = true)
			Payroll getSinglePostedPayrollKindByEmpl(
					@Param("cid") Integer cid,
					@Param("bid") Integer bid,
					@Param("knd") String knd,
					@Param("mn") Integer mn,
					@Param("yr") Integer yr,
					@Param("emplId") Long emplId,
					@Param("conId") Long conId
				);
	
	List<Payroll> findAllByContract(Contract contract);
	
	
	@Query(value = "Select * from payroll c where "
			+ "c.contract_id = ?1 "
			+ "and c.employee = ?2 ",
			nativeQuery = true)
			List<Payroll> getAllByContractEmpl(
					@Param("cid") Long cid,
					@Param("empl") Long empl
				);
}
