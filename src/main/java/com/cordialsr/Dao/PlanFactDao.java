package com.cordialsr.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.Dao.custrepo.PlanFactRepo;
import com.cordialsr.domain.PlanFact;
import com.cordialsr.domain.Position;
import com.cordialsr.domain.projection.PlanFactProjection;

@RepositoryRestResource(collectionResourceRel = "planfact", path = "planfact", 
						excerptProjection=PlanFactProjection.class)
public interface PlanFactDao extends JpaRepository<PlanFact, Integer>, PlanFactRepo {
	
	@Query(value = "from PlanFact c where c.id = ?1")
	PlanFact planId(@Param("id") Integer id);
	
	@Query(value = "from PlanFact c "
			+ "where c.company.id = ?1 "
			+ "and c.position.id = ?2 "
			+ "and c.year = ?3 "
			+ "and c.month = ?4 ")
	List<PlanFact> getPlanFactByPosForMonthYear(@Param("cid") Integer cid, 
												@Param("pos") Integer pos, 
												@Param("year") Integer year, 
												@Param("mon") Integer mon);
	
	@Query(value = "from PlanFact c "
			+ "where c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "and c.position.id = ?3 "
			+ "and c.year = ?4 "
			+ "and c.month = ?5 ")
	List<PlanFact> getPlanFactBraByPosForMonthYear(@Param("cid") Integer cid, 
												   @Param("bid") Integer bid,
											       @Param("pos") Integer pos, 
												   @Param("year") Integer year, 
												   @Param("mon") Integer mon);
	
	@Query(value = "SELECT id, year, month, company, department, region, branch, "
			+ "position, party, sum(plan) as plan, fact, cpu_date, currency, user_created, updated_date, updated_user, overdue "
			+ "FROM plan_fact c "  
			+ "where c.company = ?1 "  
			+ "and c.branch = ?2 "  
			+ "and c.position = ?3 "
			+ "and c.year = ?4 "
			+ "and c.month = ?5 "
			+ "group by c.month" ,
			nativeQuery = true)
	
	PlanFact getPlanFactGroupBraByPosForMonthYear(@Param("cid") Integer cid, 
											      @Param("bid") Integer bid,
										          @Param("pos") Integer pos, 
											      @Param("year") Integer year, 												   
											      @Param("mon") Integer mon);
	
	@Query(value = "from PlanFact c "
			+ "where c.party.id = ?1 "
			+ "and c.position.id = " + Position.POS_CAREMAN
			+ " and c.year = ?2 "
			+ "and c.month = ?3 ")
	PlanFact getPlanFactByPidMonthYear(@Param("pid") Long pid, 
											 @Param("year") Integer year, 
											 @Param("mon") Integer mon);
	
	@Query(value = "from PlanFact c "
			+ "where c.party.id = ?1 "
			+ "and c.company.id = ?2 "
			+ "and c.branch.id = ?3 "			
			+ "and c.position.id = " + Position.POS_CAREMAN
			+ " and c.year = ?4 "
			+ "and c.month = ?5 ")
	PlanFact findCaremanPlan(@Param("pid") Long pid,
											@Param("cid") Integer cid,
											@Param("bid") Integer bid,
											 @Param("year") Integer year, 
											 @Param("mon") Integer mon);
	
	@Query(value = "from PlanFact c "
			+ "where c.branch.id = ?1 "
			+ "and c.position.id = " + Position.POS_DIRECTOR
			+ " and c.year = ?2 "
			+ "and c.month = ?3 ")
	PlanFact getPlanByBranchMonthYear(@Param("brId") Integer brId, 
											 @Param("year") Integer year, 
											 @Param("mon") Integer mon);
	
	@Query(value = "from PlanFact c "
			+ "where c.company.id = ?1 "
			+ "and c.year = ?2 "
			+ "and c.month = ?3 ")
	List<PlanFact> getPlan(@Param("cid") Integer cid, 
						   @Param("year") Integer year, 
						   @Param("month") Integer month);

	
	@Query(value = "from PlanFact c "
			+ "where c.company.id = ?1 "
			+ "and c.branch.id = ?2 "
			+ "and c.position.id = ?3 "
			+ "and c.party.id = ?4 "
			+ "and c.year = ?5 "
			+ "and c.month = ?6 ")
	PlanFact getPlanComBraByPartyMonYer(@Param("cid") Integer cid,
									    @Param("bid") Integer bid,
									    @Param("position") Integer pos,
									    @Param("party") Long pid,
									    @Param("year") Integer year, 
									    @Param("month") Integer month);
	
	
}
