
package com.cordialsr.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.projection.BranchProjection;

@RepositoryRestResource(collectionResourceRel = "branch", path = "branch",
								excerptProjection = BranchProjection.class)
public interface BranchDao extends CrudRepository<Branch, Integer>{
	
	@Query("from Branch")
	List<Branch> findAllBranches();
	
	@Query("from Branch b "
			+ "where b.company.id = ?1 ")
	List<Branch> findAllByCompany(
					@Param("companyId") Integer companyId);
	
	@Query("from Branch c where c.company.id = ?1")
	List<Branch> findAllSalesBranchesByCompany(
					@Param("companyId") Integer companyId);
	
	@Query(value = "select * from Branch b "
			+ "right join region_has_branch r on r.branch_id = b.id "
			+ "where "
			+ "b.company_id = ?1 "
			+ "and r.region_id = ?2 "
			+ "order by b.branch_name",
			nativeQuery = true)
	List<Branch> findAllByRegion(@Param("companyId") Integer companyId,
								 @Param("regionId") Integer regionId);

	@Query(value = "select * from Branch b "
			+ "where b.id = ?1",
			nativeQuery = true)
	Branch findById(@Param("bid") Integer bid);
	
// **************************************************************************************
	
	
	//for Check is branch with region exist
	@Query(value = "select * from Branch b "
			+ "right join region_has_branch r on r.branch_id = b.id "
			+ "where "
			+ "b.id = ?1 "
			+ "and r.region_id = ?2 ",
			nativeQuery = true)
	Branch findByIdRegion(@Param("brId") Integer brId,
						  @Param("regionId") Integer regionId);
}
