package com.cordialsr.Dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.Currency;

@RepositoryRestResource(collectionResourceRel = "currency", path = "currency")
public interface CurrencyDao extends CrudRepository<Currency, String>{

}
