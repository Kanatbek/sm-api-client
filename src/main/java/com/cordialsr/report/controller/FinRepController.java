package com.cordialsr.report.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.reducer.PartyReducer;
import com.cordialsr.dto.FinRepCashFlowDto;
import com.cordialsr.dto.ProfitLoss;
import com.cordialsr.dto.TurnoverBalance;
import com.cordialsr.report.service.FinReportService;

@RepositoryRestController
@RequestMapping(value = "finrep")
public class FinRepController {

	@Autowired
	FinReportService frService;
	
	@RequestMapping(value = "tbs", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getTurnoverBalanceSheet(
    		@RequestParam("cid") Integer cid,
    		@RequestParam("ds") String ds,
    		@RequestParam("de") String de) {
		try {
			List<TurnoverBalance> tbL = frService.getTurnoverbalance(cid, ds, de);
			return ResponseEntity.status(HttpStatus.OK).body(tbL);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	@RequestMapping(value = "pl", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getProfitLoss(
    		@RequestParam("cid") Integer cid,
    		@RequestParam("ds") String ds,
    		@RequestParam("de") String de) {
		try {
			ProfitLoss pl = frService.getProfitLoss(cid, ds, de);
			return ResponseEntity.status(HttpStatus.OK).body(pl);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	@RequestMapping(value = "tbsglcfs", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getInnerTurnoverBalanceByCFS(
    		@RequestParam("cid") Integer cid,
    		@RequestParam("gl") String gl,
    		@RequestParam("ds") String ds,
    		@RequestParam("de") String de) {
		try {
			List<TurnoverBalance> tbL = frService.getInnerTurnoverBalanceByCFStatement(cid, gl, ds, de);
			for (TurnoverBalance tb: tbL) {
				tb.setParty(PartyReducer.reduceMax(tb.getParty()));
			}
			return ResponseEntity.status(HttpStatus.OK).body(tbL);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	@RequestMapping(value = "tbsglcfsparty", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getInnerTurnoverBalanceByParty(
    		@RequestParam("cid") Integer cid,
    		@RequestParam("gl") String gl,
    		@RequestParam(value = "cfs", required = false) Integer cfs,
    		@RequestParam("ds") String ds,
    		@RequestParam("de") String de) {
		try {
			List<TurnoverBalance> tbL = frService.getInnerTurnoverBalanceByParty(cid, gl, cfs, ds, de);
			for (TurnoverBalance tb: tbL) {
				tb.setParty(PartyReducer.reduceMax(tb.getParty()));
			}
			return ResponseEntity.status(HttpStatus.OK).body(tbL);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// *******************************************************************************
	
	@RequestMapping(value = "cfgen", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getCashFlowGeneral(
    		@RequestParam("cid") Integer cid,
    		@RequestParam("ds") String ds,
    		@RequestParam("de") String de) {
		try {
			List<FinRepCashFlowDto> frcfL = frService.getCashFlowGeneral(cid, ds, de);
			return ResponseEntity.status(HttpStatus.OK).body(frcfL);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	@RequestMapping(value = "cfbr", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getCashFlowBranches(
    		@RequestParam("cid") Integer cid,
    		@RequestParam("cfId") Integer cfId,
    		@RequestParam("ds") String ds,
    		@RequestParam("de") String de) {
		try {
			List<FinRepCashFlowDto> frcfL = frService.getCashFlowBranch(cid, cfId, ds, de);
			return ResponseEntity.status(HttpStatus.OK).body(frcfL);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	@RequestMapping(value = "cfdet", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getCashFlowDetailed(
    		@RequestParam("cid") Integer cid,
    		@RequestParam("bid") Integer bid,
    		@RequestParam("cfId") Integer cfId,
    		@RequestParam("ds") String ds,
    		@RequestParam("de") String de) {
		try {
			List<FinRepCashFlowDto> frcfL = frService.getCashFlowDetailed(cid, bid, cfId, ds, de);
			return ResponseEntity.status(HttpStatus.OK).body(frcfL);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
}
