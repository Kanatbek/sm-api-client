package com.cordialsr.report.controller;

import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.SmCall;
import com.cordialsr.domain.reducer.SmCallReducer;
import com.cordialsr.report.service.SmCallReportService;

@RepositoryRestController
@RequestMapping(value = "smCallReport")
public class SmCallReportController {
	
	@Autowired
	SmCallReportService smCallService;

	@RequestMapping(value = "/confirmCallReport", method = RequestMethod.GET, 
            produces = MediaTypes.HAL_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Object> saveSmService(@RequestParam("cid") Integer cid,
    	 @DateTimeFormat(pattern = "yyyy-MM-dd")@RequestParam("from") Date fromDate,
    	 @DateTimeFormat(pattern = "yyyy-MM-dd")@RequestParam("to") Date toDate) {
        
		String reason = null;
        ArrayList<SmCall> created = null;
        if (cid != null && fromDate != null && toDate != null ) {
        	ImmutablePair<ArrayList<SmCall>, String> result = newReportCalls(cid, fromDate, toDate);
            reason = result.getRight();            
            created = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Report to be created is null").body(cid);
        }
 
        if (reason == null) {
        	for (SmCall sm: created) {
        		sm = SmCallReducer.reduceMax(sm);
        	}
            return ResponseEntity.status(HttpStatus.CREATED).body(created);
        } else {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Transactional(rollbackFor = Exception.class)
	private ImmutablePair<ArrayList<SmCall>, String> newReportCalls(Integer cid, Date fromDate, Date toDate) {
		String reason = null;
		ArrayList<SmCall> calls = null;
		try { 
			calls = smCallService.reportNewCalls(cid, fromDate, toDate);
		} catch (Exception ex) {
			ex.printStackTrace();
			reason = "Error ";
        	reason += ex.getMessage();
		}
		return new ImmutablePair<ArrayList<SmCall>, String>(calls, reason);
	}
}
