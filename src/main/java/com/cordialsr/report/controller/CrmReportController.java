package com.cordialsr.report.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.Report;
import com.cordialsr.report.service.ReportService;

@RepositoryRestController
@RequestMapping(value = "reports")
public class CrmReportController {
	
	@Autowired
	ReportService repService;
		
	@RequestMapping(value = "/getReports/{cid}/{dt}", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getByDt(
    		@PathVariable("cid") Integer cid,
    		@PathVariable("dt") @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt
    		) {
		try {
			List<Report> reps = repService.getReports(cid,dt);

			if (reps == null) {
				return ResponseEntity.status(HttpStatus.OK).body("Crm reports not found!");
			}
			return ResponseEntity.status(HttpStatus.OK).body(reps);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// ***********************************************************************************************************
}
