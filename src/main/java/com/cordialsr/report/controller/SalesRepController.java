package com.cordialsr.report.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.Position;
import com.cordialsr.dto.ReportSalesDto;
import com.cordialsr.report.service.SalesReportService;

@RepositoryRestController
@RequestMapping(value = "salesrep")
public class SalesRepController {
	
	@Autowired
	SalesReportService salesRepService;
	
	// **********************************************************************************************************************************************************

	@RequestMapping(value = "/getReportSalesByCompany", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getReportSalesByBranch(
    		    @RequestParam(value="cid", required=true) Integer cid,
    		    @RequestParam(value="bid", required=false) Integer bid,
    		    @RequestParam(value="pos", required = false) Integer posId,
    		    @RequestParam(value="dt", required=false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt
    		) {
		try {
			List<ReportSalesDto> rsList = new ArrayList<ReportSalesDto>();
			if (posId == Position.POS_DIRECTOR) {
				rsList = salesRepService.getReportSales(cid, posId, dt);	
			}
			
			
			return ResponseEntity.status(HttpStatus.OK).body(rsList);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// ***********************************************************************************************************************************************************
	
	@RequestMapping(value = "/getReportSalesByManager", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getReportSalesByManager(
    		    @RequestParam(value="cid", required=true) Integer cid,
    		    @RequestParam(value="bid", required=false) Integer bid,
    		    @RequestParam(value="dt", required=false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt
    		) {
		try {
			List<ReportSalesDto> rsList = new ArrayList<ReportSalesDto>();
			rsList = salesRepService.getReportSalesByAllManager(cid, bid, dt);
			
			
			
			return ResponseEntity.status(HttpStatus.OK).body(rsList);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
	// ***********************************************************************************************************************************************************
	
		@RequestMapping(value = "/getReportSalesByDealer", method = RequestMethod.GET,
				produces = MediaTypes.HAL_JSON_VALUE)
		@ResponseBody
	    public ResponseEntity<Object> getReportSalesByDealer(
	    		    @RequestParam(value="cid", required=true) Integer cid,
	    		    @RequestParam(value="bid", required=false) Integer bid,
	    		    @RequestParam(value="man", required = false) Long manId,
	    		    @RequestParam(value="dt", required=false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date dt
	    		) {
			try {
				List<ReportSalesDto> rsList = new ArrayList<ReportSalesDto>();
				if (manId > 0) {
					rsList = salesRepService.getReportSalesByOneDealer(cid, bid, manId, dt);
				} else {
					rsList = salesRepService.getReportSalesByAllDealers(cid, bid, dt);
				}
				
				return ResponseEntity.status(HttpStatus.OK).body(rsList);
			} catch (Exception e) {
				e.printStackTrace();
				return ResponseEntity
	                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
	                    .body(e.getMessage());
			}
	    }

}
