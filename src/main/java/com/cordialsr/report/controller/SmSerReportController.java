package com.cordialsr.report.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.SmService;
import com.cordialsr.domain.reducer.SmServiceReducer;
import com.cordialsr.report.service.ServiceReportService;

@RepositoryRestController
@RequestMapping(value = "serviceReport")
public class SmSerReportController {
	@Autowired
	ServiceReportService servRepService; 
	
//////////////////////////////////////////////////////Careman make happines ........
	
	@RequestMapping(value = "/happycallReport", method = RequestMethod.GET, 
									produces = MediaTypes.HAL_JSON_VALUE)
					@ResponseBody
					public ResponseEntity<Object> newHappyCallReport(@RequestParam("cid") Integer cid,
						 @DateTimeFormat(pattern = "yyyy-MM-dd")@RequestParam("from") Date fromDate,
						 @DateTimeFormat(pattern = "yyyy-MM-dd")@RequestParam("to") Date toDate) {

		String reason = null;
		List<SmService> created = null;
		if (cid != null && fromDate != null && toDate != null ) {
			ImmutablePair<List<SmService>, String> result = newReportService(cid, fromDate, toDate);
			reason = result.getRight();            
			created = result.getLeft();
		} else {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Report to be created is null").body(cid);
		}
		
		if (reason == null) {
			for (SmService sm: created) {
					sm = SmServiceReducer.reduceMax(sm);
			}
			return ResponseEntity.status(HttpStatus.CREATED).body(created);
		} else {
			return ResponseEntity
					.status(HttpStatus.BAD_REQUEST)
					.body(reason);
		}
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@RequestMapping(value = "/happycallOnExcel", method = RequestMethod.GET, 
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Object> happycallReportOnExcel(@RequestParam("cid") Integer cid,
			      @DateTimeFormat(pattern = "yyyy-MM-dd")@RequestParam("from") Date fromDate,
			      @DateTimeFormat(pattern = "yyyy-MM-dd")@RequestParam("to") Date toDate) {

		String reason = null;
		String created = null;
		if (cid != null && fromDate != null && toDate != null ) {
			ImmutablePair<String, String> result = happyCallOnExcel(cid, fromDate, toDate);
			reason = result.getRight();            
			created = result.getLeft();
		} else {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Report to be created is null").body(cid);
		}

		if (reason == null) {
			return ResponseEntity.status(HttpStatus.CREATED).body(created);
		} else {
			return ResponseEntity
					.status(HttpStatus.BAD_REQUEST)
					.body(reason);
		}
	}
	
	/////////////////////////////////////////////Report on Period ........ //////////////////////////////////////////////////////////
	
	@RequestMapping(value = "/happycallReportOnPeriod", method = RequestMethod.GET, 
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Object> happycallReportOnPeriod(@RequestParam("cid") Integer cid,
			       @DateTimeFormat(pattern = "yyyy-MM-dd")@RequestParam("from") Date fromDate,
			       @DateTimeFormat(pattern = "yyyy-MM-dd")@RequestParam("to") Date toDate) {

		String reason = null;
		List<SmService> created = null;
		if (cid != null && fromDate != null && toDate != null ) {
			ImmutablePair<List<SmService>, String> result = happyCallOnPeriod(cid, fromDate, toDate);
			reason = result.getRight();            
			created = result.getLeft();
		} else {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Report to be created is null").body(cid);
		}
		
		if (reason == null) {
			for (SmService sm: created) {
					sm = SmServiceReducer.reduceMax(sm);
			}
			return ResponseEntity.status(HttpStatus.CREATED).body(created);
		} else {
			return ResponseEntity
					.status(HttpStatus.BAD_REQUEST)
					.body(reason);
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Transactional(rollbackFor = Exception.class)
	private ImmutablePair<List<SmService>, String> happyCallOnPeriod(Integer cid, Date fromDate, Date toDate) {
		String reason = null;
		List<SmService> service = null;
		try { 
			service = servRepService.reportNewServiceOnPeriod(cid, fromDate, toDate);
		} catch (Exception ex) {
			ex.printStackTrace();
			reason = "Error: ";
        	reason += ex.getMessage();
		}
		return new ImmutablePair<List<SmService>, String>(service, reason);
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Transactional(rollbackFor = Exception.class)
	private ImmutablePair<String, String> happyCallOnExcel(Integer cid, Date fromDate, Date toDate) {
		String reason = null;
		String base64 = null;
		try { 
			base64 = servRepService.reportOnExcel(cid, fromDate, toDate);
		} catch (Exception ex) {
			ex.printStackTrace();
			reason = "Error: ";
        	reason += ex.getMessage();
		}
		return new ImmutablePair<String, String>(base64, reason);
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	@Transactional(rollbackFor = Exception.class)
	private ImmutablePair<List<SmService>, String> newReportService(Integer cid, Date fromDate, Date toDate) {
		String reason = null;
		List<SmService> service = null;
		try { 
			service = servRepService.reportNewService(cid, fromDate, toDate);
		} catch (Exception ex) {
			ex.printStackTrace();
			reason = "Error: ";
        	reason += ex.getMessage();
		}
		return new ImmutablePair<List<SmService>, String>(service, reason);
	}
}
