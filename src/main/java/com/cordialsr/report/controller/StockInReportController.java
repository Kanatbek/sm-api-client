package com.cordialsr.report.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.StockIn;
import com.cordialsr.report.service.StockReportService;

@RepositoryRestController
@RequestMapping(value = "stockReport")
public class StockInReportController {

	@Autowired
	StockReportService stockService;
		
	@RequestMapping(value = "/getReports", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getByDt(
    		@RequestParam("cid") Integer cid,
    		@RequestParam("bid") Integer bid,
    		@RequestParam("inv") Integer inv) {
		try {
			ArrayList<StockIn> reps = stockService.getStocksReports(cid, bid, inv);

			if (reps == null) {
				return ResponseEntity.status(HttpStatus.OK).body("Stock's reports not found!");
			}
			return ResponseEntity.status(HttpStatus.OK).body(reps);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
}
