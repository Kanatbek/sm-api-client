package com.cordialsr.report.controller;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.Invoice;
import com.cordialsr.dto.InstoreDto;
import com.cordialsr.report.service.InvoiceReportService;
import com.cordialsr.report.service.StockReportService;

@RepositoryRestController
@RequestMapping(value = "invoiceReport")
public class InvoiceReportController {
	
	@Autowired
	StockReportService stockRepService;
	
	@Autowired
	InvoiceReportService invoiceRepService;

	@RequestMapping(value = "/checkSnByInvoice",
			method = {RequestMethod.PATCH, RequestMethod.PUT},
			produces = MediaTypes.HAL_JSON_VALUE)	
	public @ResponseBody
	ResponseEntity<Object> checkInvoice(@RequestBody Invoice inv) {
		String reason = null;
		Invoice newInv = new Invoice();
		if (inv != null) {
			ImmutablePair<Invoice, String> result = checkStocksByInvoice(inv);
			reason = result.getRight();             
			newInv = result.getLeft();
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Empty Invoice");
		}
		if (reason != null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
		}
		return ResponseEntity.status(HttpStatus.OK).body(newInv);
	}
	
	///////

	 //////////////////////////////////////////////////// Report for Instore .....
	
		@RequestMapping(value = "/instoreReport", method = RequestMethod.GET,
				produces = MediaTypes.HAL_JSON_VALUE)
		@ResponseBody
	    public ResponseEntity<Object> getInvoicesForReport(@RequestParam("cid") Integer cid,
	    		    @DateTimeFormat(pattern = "yyyy-MM-dd")@RequestParam("from") Date from,
	    			@DateTimeFormat(pattern = "yyyy-MM-dd")@RequestParam("to") Date to) {
			try {
				
				List<InstoreDto> instores  = invoiceRepService.getInvoiceForReport(cid, from, to);
				
				if (instores != null) {
					return ResponseEntity.status(HttpStatus.OK).body(instores);
				} else {
					return ResponseEntity
		                    .status(HttpStatus.NOT_FOUND)
		                    .body("Invoices for respost not found.");
				}			
			} catch (Exception e) {
				e.printStackTrace();
				return ResponseEntity
	                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
	                    .body(e.getMessage());
			}
	    }

	//////////////////////////////////////////////////// Connect ..............................................
	
	@Transactional(rollbackFor = Exception.class)
	private ImmutablePair<Invoice, String> checkStocksByInvoice(Invoice inv) {
		String reason = null;
        try {
        	inv = stockRepService.checkStocksForSN(inv);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = "Error: " + ex.getMessage();        	
        }
        
        return new ImmutablePair<Invoice, String>(inv, reason);
		
	}
	
}
