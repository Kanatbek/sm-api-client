package com.cordialsr.report.service;

import java.util.ArrayList;
import java.util.Date;

import com.cordialsr.domain.SmCall;

public interface SmCallReportService {

	ArrayList<SmCall> reportNewCalls(Integer cid, Date fromDate, Date toDate) throws Exception;
}
