package com.cordialsr.report.service;

import java.util.Date;
import java.util.List;

import com.cordialsr.domain.SmService;

public interface ServiceReportService {
	
	List<SmService> reportNewService(Integer cid, Date fromDate, Date toDate)throws Exception;
	String reportOnExcel(Integer cid, Date fromDate, Date toDate) throws Exception;
	List<SmService> reportNewServiceOnPeriod(Integer cid, Date fromDate, Date toDate) throws Exception;
	
}
