package com.cordialsr.report.service;

import java.util.Date;
import java.util.List;

import com.cordialsr.domain.StockIn;
import com.cordialsr.dto.InstoreDto;

public interface InvoiceReportService {

	List<InstoreDto> getInvoiceForReport(Integer cid, Date from, Date to) throws Exception;
	StockIn getStockForInvRep(Integer cid, Integer bid, Integer inv) throws Exception;
	
}
