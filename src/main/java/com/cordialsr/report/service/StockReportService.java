package com.cordialsr.report.service;


import java.util.ArrayList;

import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.StockIn;

public interface StockReportService {


	Invoice checkStocksForSN(Invoice inv) throws Exception;
	ArrayList<StockIn> getStocksReports(Integer cid, Integer bid, Integer inv) throws Exception;
	
}
