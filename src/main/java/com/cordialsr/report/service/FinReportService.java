package com.cordialsr.report.service;

import java.util.List;

import com.cordialsr.dto.FinRepCashFlowDto;
import com.cordialsr.dto.ProfitLoss;
import com.cordialsr.dto.TurnoverBalance;

public interface FinReportService {
	List<TurnoverBalance> getTurnoverbalance(Integer cid, String ds, String de)  throws Exception;
	
	List<TurnoverBalance> getInnerTurnoverBalanceByCFStatement(Integer cid, String glCode, String ds, String de)  throws Exception;
	
	List<TurnoverBalance> getInnerTurnoverBalanceByParty(Integer cid, String glCode, Integer cfs, String ds, String de)  throws Exception;
	
	List<TurnoverBalance> getTurnoverbalancePreviews(Integer cid, String ds, String de)  throws Exception;
	
	ProfitLoss getProfitLoss(Integer cid, String ds, String de)  throws Exception;
	
	List<FinRepCashFlowDto> getCashFlowGeneral(Integer cid, String ds, String de)  throws Exception;
	
	List<FinRepCashFlowDto> getCashFlowBranch(Integer cid, Integer cfId, String ds, String de)  throws Exception;
	
	List<FinRepCashFlowDto> getCashFlowDetailed(Integer cid, Integer bid, Integer cfId, String ds, String de)  throws Exception;
}
