package com.cordialsr.report.service;

import java.util.Date;
import java.util.List;

import com.cordialsr.dto.ReportSalesDto;

public interface SalesReportService {
	
	List<ReportSalesDto> getReportSales(Integer cid, Integer posId, Date dt) throws Exception;
	
	List<ReportSalesDto> getReportSalesByAllManager(Integer cid, Integer bid, Date dt) throws Exception;
	
	List<ReportSalesDto> getReportSalesByOneDealer(Integer cid, Integer bid, Long manager, Date dt) throws Exception;
	
	List<ReportSalesDto> getReportSalesByAllDealers(Integer cid, Integer bid, Date dt) throws Exception;
	
}
