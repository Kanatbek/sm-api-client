package com.cordialsr.report.service;

import java.util.Date;
import java.util.List;

import com.cordialsr.domain.Report;

public interface ReportService {
	List<Report> getReports(Integer cid, Date dt) throws Exception;
	Report[] saveAllReports(Report[] pfL) throws Exception;
}
