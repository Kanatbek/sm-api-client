package com.cordialsr.report.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.StockDao;
import com.cordialsr.Dao.StockOutDao;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.StockIn;
import com.cordialsr.domain.StockOut;
import com.cordialsr.domain.validator.InvoiceValidator;
import com.cordialsr.report.service.InvoiceReportService;
import com.cordialsr.report.service.StockReportService;

@Service
@Transactional(readOnly = true)
public class StockReportServiceImpl implements StockReportService {

	@Autowired
	StockDao skInDao;
	
	@Autowired 
	StockOutDao skOutDao;
	
	@Autowired
	InvoiceReportService invRepSer;
	
	@Override
	public Invoice checkStocksForSN(Invoice inv) throws Exception {
		try {
			InvoiceValidator.validateForStockSerNum(inv);
			ArrayList<StockIn> listSI = new ArrayList<StockIn>();
			ArrayList<StockOut> listSO = new ArrayList<StockOut>();
			for (StockIn sk: inv.getInvoiceItems().get(0).getChildStockIns()) {
				
				StockIn stock = skInDao.findByJustSerialNumber(sk.getSerialNumber());
				if (stock == null) {
					listSI.add(sk);
				} else {
					StockIn stkIn = stock.clone();
					stkIn.setId(sk.getId());
					listSI.add(stkIn);
				}
				
				StockOut sout = skOutDao.findByJustSerialNumber(sk.getSerialNumber());
				if (sout == null) {
					sout = new StockOut();
					sout.setId(sk.getId());
					sout.setSerialNumber(sk.getSerialNumber());
					listSO.add(sout);
				} else {
					StockOut stkIn = sout.clone();
					stkIn.setId(sk.getId());
					listSO.add(stkIn);
				}
			}
			
			inv.getInvoiceItems().get(0).setChildStockIns(listSI);
			inv.getInvoiceItems().get(0).setChildStockOuts(listSO);
			return inv;
		} catch (Exception ex) {
			throw ex;
		}
	}
	
	@Override
	public ArrayList<StockIn> getStocksReports(Integer cid, Integer bid, Integer inv) throws Exception {
		try {
			ArrayList<StockIn> list = new ArrayList<StockIn>();
			StockIn sk = skInDao.getStocksForReport(cid, bid, inv);
			list.add(sk);
			StockIn st = invRepSer.getStockForInvRep(cid, bid, inv);
			list.add(st);
			return list;
		} catch (Exception ex) {
			throw ex;
		}
	}
	
}
