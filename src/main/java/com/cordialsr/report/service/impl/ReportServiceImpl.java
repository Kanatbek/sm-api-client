package com.cordialsr.report.service.impl;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.BranchDao;
import com.cordialsr.Dao.ContractDao;
import com.cordialsr.Dao.CrmDemoDao;
import com.cordialsr.Dao.CrmLeadDao;
import com.cordialsr.Dao.EmployeeDao;
import com.cordialsr.Dao.PartyDao;
import com.cordialsr.Dao.PlanFactDao;
import com.cordialsr.Dao.RegionDao;
import com.cordialsr.Dao.ReportDao;
import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.CrmDemo;
import com.cordialsr.domain.Department;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.PlanFact;
import com.cordialsr.domain.Position;
import com.cordialsr.domain.Region;
import com.cordialsr.domain.Report;
import com.cordialsr.domain.projection.ContractForCountProjection;
import com.cordialsr.domain.reducer.BranchReducer;
import com.cordialsr.report.service.ReportService;

@Service
@Transactional
public class ReportServiceImpl implements ReportService {

	@Autowired
	ReportDao reportDao;
	
	@Autowired
	RegionDao regionDao;
	
	@Autowired 
	BranchDao branchDao;
	
	@Autowired
	EmployeeDao employeeDao;
	
	@Autowired
	PlanFactDao pfDao;
	
	@Autowired
	PartyDao partyDao;
	
	@Autowired
	CrmDemoDao demoDao;
	
	@Autowired
	ContractDao conDao;
	
	@Autowired
	CrmLeadDao leadDao;
	
	
	@Override
	public List<Report> getReports(Integer cid, Date dt) throws Exception {
		List<Report> reportNew = assembleReport(cid, dt);
		
//		for (Report rep: reportNew) {
//			if (!reportOld.contains(rep)) {
//				reportOld.add(rep);
//			}
//		}
		return reportNew;
		
	}
	
	
	// ******************************************************************************************************
	
	public List<Report> assembleReport(Integer cid, Date dt) throws Exception {
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(dt);
			Integer year = cal.get(Calendar.YEAR);
			Integer month = cal.get(Calendar.MONTH) + 1;
			Integer day = cal.get(Calendar.DATE);
			
			
			List<Report> report = new ArrayList<>();
			List<Region> regL = new ArrayList<>();
			List<Branch> brL = new ArrayList<>();
			List<PlanFact> pfL = new ArrayList<>();
			if (cid == -1) {
				brL = branchDao.findAllBranches();
			} else {
				regL = regionDao.getAllRegionByComAndDep(cid, Department.DEP_MARKETING_AND_SALES);
				pfL = pfDao.getPlan(cid, year, month);
			}
			int ind = 0;
			for (Region reg: regL) {
				brL = branchDao.findAllByRegion(cid, reg.getId());
				for (Branch br: brL) {
					Report rep = new Report();
					
					// * Finding directors of the branch *
					Employee dir = employeeDao.findDirByBranch(br.getId());
					
					// * Finding dealers *
					int dealerNumber = employeeDao.numberByBrPos(br.getId(), Position.POS_DEALER, dt);
					
					// * Finding demosec *	
					int demosecNumber = employeeDao.numberByBrPos(br.getId(), Position.POS_DEMOSEC, dt);
					
					int groupNumber = employeeDao.numberByBrPos(br.getId(), Position.POS_MANAGER, dt);				

					// * Finding demos *
					int demoToday = demoDao.findByDay(br, day, month, year);
					int demoCurrentMonth = demoDao.findByMonth(br, month , year);
					int demoPreviousMonth = demoDao.findByMonth(br, month - 1, year);
					
					int contractToday = conDao.findByDay(br, day, month, year);
					int contractCurrentMonth = conDao.findByMonth(br, month, year);
					int contractPreviousMonth = conDao.findByMonth(br, month - 1, year);
					
										
//					List<CrmLead> leadNumber = leadDao.findByDay(day);
					
					// * Filling the party * 
					if (dir != null) rep.setParty(dir.getParty());
					
//					// * Filling the fields *
					if (pfL.size() > 0) {
						PlanFact pf = getPf(pfL, br.getId());
						if (pf != null) {
							rep.setPlan(pf.getPlan());
						}
					}
					
					rep.setRegion(reg.getName());
					
					rep.setForMonthDemoPerSale(0);
					rep.setForPreviousMonthDemoPerSale(0);
					rep.setDemoCurrentMonth(0);
					rep.setDemoPreviousMonth(0);
					rep.setForTodayDemoPerDealer(0);
					rep.setForCurrentMonthDemoPerDealer(0);
					rep.setForCurrentMonthSalePerDealer(0);
					
					rep.setDemoPreviousMonth(demoPreviousMonth);
					rep.setDemoCurrentMonth(demoCurrentMonth);	
					rep.setDemoToday(demoToday);
					
					rep.setSalesPreviousMonth(contractPreviousMonth);
					rep.setSalesCurrentMonth(contractCurrentMonth);	
					rep.setSalesToday(contractToday);
					
					rep.setDealerNumber(dealerNumber);
					rep.setDemosecNumber(demosecNumber);
					rep.setGroupNumber(groupNumber);
					
					// * Reducing and filling branch *
					br = BranchReducer.reduceBranchForView(br);
					rep.setBranch(br);
					
					report.add(rep);
					ind++;
				}
			}
			
			return report;	
		} catch(Exception e) {
			throw e;
		}
	}
	
	private PlanFact getPf(List<PlanFact> pfL, Integer bid) {
		for (PlanFact pf:pfL) {
			if (pf.getBranch().getId() == bid) {
				return pf;
			}
		}
		return null;
	}
	
	// ******************************************************************************************************

	@Override
	public Report[] saveAllReports(Report[] pfL) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
