package com.cordialsr.report.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.InvoiceDao;
import com.cordialsr.Dao.StockDao;
import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.StockIn;
import com.cordialsr.domain.factory.InvoiceFactory;
import com.cordialsr.dto.InstoreDto;
import com.cordialsr.dto.ReportDto;
import com.cordialsr.report.service.InvoiceReportService;

@Service
@Transactional(readOnly = true)
public class InvoiceReportServiceImpl implements InvoiceReportService{

	@Autowired
	InvoiceDao invDao;

	@Autowired
	StockDao stockDao;
	
	@Override
	public List<InstoreDto> getInvoiceForReport(Integer cid, Date from, Date to) throws Exception {

		List<InstoreDto> instores = new ArrayList<InstoreDto>();
		ArrayList<Invoice> invoices = new ArrayList<Invoice>();
		
		try {
			invoices = invDao.getInvoicesToPeriodFromTo(cid, from, to);
			
			if (invoices == null) {
				throw new Exception("Invoices is empty");
			} else {
				instores = createReportDto(invoices);
			}
			for (InstoreDto ins: instores) {
				for (ReportDto rep: ins.getReports()) {
					StockIn sk = stockDao.getStocksForReport(cid, ins.getBranch().getId(), rep.getInventory().getId());
					if (sk != null) {
					rep.setBalance(rep.getBalance().add(sk.getQuantity()));
					ins.setBalance(ins.getBalance().add(sk.getQuantity()));
					}
				}
			}
			return instores;
		} catch(Exception ex) {
			throw ex;
		}
	}
	
	
	private List<InstoreDto> createReportDto(ArrayList<Invoice> invoices) throws Exception{
		List<InstoreDto> instore = new ArrayList<InstoreDto>();
		try {
			for (Invoice inv: invoices) {
				
				int c1 = 0;
				int c2 = 0;
				instore = checkInstoreBranch(instore, inv.getBranchImporter());
				for (InstoreDto ins: instore) {
					if (ins.getBranch().getId() == inv.getBranchImporter().getId()) {
						break;
					}
					c1 ++;
				}
				
				if (inv.getBranch().getId() != inv.getBranchImporter().getId()) {
					instore = checkInstoreBranch(instore, inv.getBranch());
					for (InstoreDto ins: instore) {
						if (ins.getBranch().getId() == inv.getBranch().getId()) {
							break;
						}
						c2 ++;
					}
				}
				
				if (inv.getInvoiceType().equals(Invoice.INVOICE_TYPE_IMPORT)) {
					instore.set(c1, InvoiceFactory.constructImportInvoice(instore.get(c1), inv));
				} else if (inv.getInvoiceType().equals(Invoice.INVOICE_TYPE_TRANSFER)) {
					instore.set(c1, InvoiceFactory.constructTransferInvoice(instore.get(c1), inv, 1));
					instore.set(c2, InvoiceFactory.constructTransferInvoice(instore.get(c2), inv, 2));
				}  else if (inv.getInvoiceType().equals(Invoice.INVOICE_TYPE_RENT)) {
					instore.set(c1, InvoiceFactory.constructRentInvoice(instore.get(c1), inv));
				} else if (inv.getInvoiceType().equals(Invoice.INVOICE_TYPE_SELL)) {
					instore.set(c1, InvoiceFactory.constructSellInvoice(instore.get(c1), inv));
				} else if (inv.getInvoiceType().equals(Invoice.INVOICE_TYPE_SERVICE)) {
					instore.set(c1, InvoiceFactory.constructServiceInvoice(instore.get(c1), inv));
				} else if (inv.getInvoiceType().equals(Invoice.INVOICE_TYPE_RETURN)) {
					instore.set(c1, InvoiceFactory.constructReturnInvoice(instore.get(c1), inv));
				} else if (inv.getInvoiceType().equals(Invoice.INVOICE_TYPE_WRITEOFF)) {
					instore.set(c1, InvoiceFactory.constructWriteOffInvoice(instore.get(c1), inv));
				} else if (inv.getInvoiceType().equals(Invoice.INVOICE_TYPE_ACCOUNTABLE)) {
					InstoreDto ins = InvoiceFactory.constructAccountableInvoice(instore.get(c1), inv); 
					instore.set(c1, ins);
				}
			}
			return instore;
		} catch(Exception ex) {
			throw ex;
		}
	}
	
	private List<InstoreDto> checkInstoreBranch(List<InstoreDto> instore, Branch branch) {
		Integer count = 0;
		for (InstoreDto ins: instore) {
			if (branch.getId() == ins.getBranch().getId()) {
				count ++;
				break;
			}
		}
		if (count == 0) {
			InstoreDto ins = new InstoreDto();
			ins.setBranch(new Branch(branch.getId()));
			ins.getBranch().setBranchName(branch.getBranchName());
			ins.setAccountable(new BigDecimal(0));
			ins.setBalance(new BigDecimal(0));
			ins.setGetting(new BigDecimal(0));
			ins.setClient(new BigDecimal(0));
			ins.setPosting(new BigDecimal(0));
			ins.setReturned(new BigDecimal(0));
			ins.setTransferIn(new BigDecimal(0));
			ins.setTransferOut(new BigDecimal(0));
			ins.setWriteOff(new BigDecimal(0));
			ins.setAccountReturn(new BigDecimal(0));
			instore.add(ins);
		}
		return instore;
	}


	@Override
	public StockIn getStockForInvRep(Integer cid, Integer bid, Integer inv) throws Exception {
		try {
			return new StockIn();
		} catch(Exception ex) {
			throw ex;
		}
	}
	
}
