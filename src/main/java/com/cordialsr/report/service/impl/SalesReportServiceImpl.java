package com.cordialsr.report.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.BranchDao;
import com.cordialsr.Dao.CompanyDao;
import com.cordialsr.Dao.ContractDao;
import com.cordialsr.Dao.EmployeeDao;
import com.cordialsr.Dao.PlanFactDao;
import com.cordialsr.domain.Branch;
import com.cordialsr.domain.BranchType;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.PlanFact;
import com.cordialsr.domain.Position;
import com.cordialsr.dto.ReportSalesDto;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.report.service.SalesReportService;

@Service
@Transactional(readOnly = true)
public class SalesReportServiceImpl implements SalesReportService {

	@Autowired
	ContractDao conDao;
	
	@Autowired
	CompanyDao companyDao;
	
	@Autowired
	PlanFactDao pfDao;
	
	@Autowired
	BranchDao branchDao;
	
	@Autowired
	EmployeeDao emplDao;
	
	@Override
	public List<ReportSalesDto> getReportSales(Integer cid, Integer posId, Date dt) throws Exception {
		try {	
			Date dtMonthStart = GeneralUtil.startOfMonth(dt);
			Date dtMonthEnd = GeneralUtil.endOfMonth(dt);
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(dt);
			Integer year = cal.get(Calendar.YEAR);
			Integer month = cal.get(Calendar.MONTH) + 1;
			
			Company company = companyDao.findcid(cid);
			
			List<ReportSalesDto> repDto = new ArrayList<ReportSalesDto>();
			
			List<PlanFact> pfL = pfDao.getPlanFactByPosForMonthYear(cid, posId, year, month);
			
			List<Contract> contracts = conDao.getAllDirectorByContract(cid, dtMonthStart, dtMonthEnd);
			List<Branch> brList = new ArrayList<Branch>();
			for (Contract con: contracts) {
				if (con.getBranch() != null) {
					brList.add(con.getBranch());
				}
			}
			
			
//			List<Branch> brList = branchDao.findAllByCompany(cid);
			if (brList.size() > 0) {
				for (Branch branch: brList) {
					if (branch.getBranchType().getName().equals(BranchType.TYPE_OFFICE)
							|| branch.getBranchType().getName().equals(BranchType.TYPE_STORE)) {
						Employee dir = emplDao.findDirByBranch(branch.getId());
						ReportSalesDto rep = new ReportSalesDto();
						Integer lumpsum = 0;
						Integer sales = 0;
						Integer rental = 0;
						Integer total = 0;
						
						lumpsum = conDao.getDirectorContractQuantity(company.getId(), branch.getId(), dtMonthStart, dtMonthEnd, 0, 1, false);
						sales = conDao.getDirectorContractQuantity(company.getId(), branch.getId(), dtMonthStart, dtMonthEnd, 2, 99, false);
						rental = conDao.getDirectorContractQuantity(company.getId(), branch.getId(),dtMonthStart, dtMonthEnd, 0, 99, true);
						total = lumpsum + sales + rental;
						
						
						rep.setCompany(company);
						rep.setBranch(branch);
						if (dir != null) {
							rep.setDirector(dir);	
						}
						
						rep.setPlan(new BigDecimal(0));
						rep.setSales(new BigDecimal(sales));
						rep.setLumpsum(new BigDecimal(lumpsum));
						rep.setRental(new BigDecimal(rental));
						rep.setFact(new BigDecimal(total));
						
						for (PlanFact pf : pfL) {
							if (pf.getBranch().getId() == branch.getId()) rep.setPlan(pf.getPlan()); 
						}
						
						repDto.add(rep);
					}
				}
			}
			
			return repDto;
		} catch (Exception e) {
			throw e;
		}
	}

	// ********************************************************************************************************************************************
	
	@Override
	public List<ReportSalesDto> getReportSalesByAllManager(Integer cid, Integer bid, Date dt) throws Exception {
		try {
			List<ReportSalesDto> rsList = new ArrayList<ReportSalesDto>();
			
			Date dtMonthStart = GeneralUtil.startOfMonth(dt);
			Date dtMonthEnd = GeneralUtil.endOfMonth(dt);
			
			List<Contract> contracts = conDao.getAllManagerByDate(cid, bid, dtMonthStart, dtMonthEnd);
//			List<Contract> contractWithoutMan = conDao.getContractWithoutManager(cid, bid, dtMonthStart, dtMonthEnd);
//			List<Employee> managers = emplDao.findEmployeeByContract(cid, bid, dtMonthStart, dtMonthEnd);
			List<Employee> managers = new ArrayList<Employee>();
			for (Contract con: contracts) {
				if (con.getManager() != null) {
					managers.add(con.getManager());
				}
			}
			
			for (Employee empl: managers) {
				ReportSalesDto rep = new ReportSalesDto();
				Integer lumpsum = 0;
				Integer sales = 0;
				Integer rental = 0;
				Integer total = 0;
				
				lumpsum = conDao.getManagerContractQuantity(cid, bid, empl.getId(), dtMonthStart, dtMonthEnd, 0, 1, false);	
				sales = conDao.getManagerContractQuantity(cid, bid, empl.getId(), dtMonthStart, dtMonthEnd, 2, 99, false);
				rental = conDao.getManagerContractQuantity(cid, bid, empl.getId(), dtMonthStart, dtMonthEnd, 0, 99, true);
				total = lumpsum + sales + rental;
				
				if (empl.getAccountableTo() != null) {
					rep.setDirector(empl.getAccountableTo());
				}
				
				Company company = companyDao.findcid(cid);
				Branch branch = branchDao.findById(bid);

				rep.setCompany(company);
				rep.setBranch(branch);
				rep.setPlan(new BigDecimal(0));
				rep.setManager(empl);
				rep.setSales(new BigDecimal(sales));
				rep.setLumpsum(new BigDecimal(lumpsum));
				rep.setRental(new BigDecimal(rental));
				rep.setFact(new BigDecimal(total));
				rsList.add(rep);
				
			}
			
			ReportSalesDto rep = setContractNoEmployee(cid, bid, dtMonthStart, dtMonthEnd);
			if (rep != null) {
				rsList.add(rep);	
			}
			
			
			
			return rsList;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// ********************************************************************************************************************************************


	@Override
	public List<ReportSalesDto> getReportSalesByOneDealer(Integer cid, Integer bid, Long manager, Date dt) throws Exception {
		try {
			List<ReportSalesDto> rsList = new ArrayList<ReportSalesDto>();
			
			Date dtMonthStart = GeneralUtil.startOfMonth(dt);
			Date dtMonthEnd = GeneralUtil.endOfMonth(dt);
			
			List<Contract> contracts = conDao.getAllContractByManager(cid, bid, manager, dtMonthStart, dtMonthEnd);
			List<Employee> dealers = new ArrayList<Employee>();
			for (Contract con: contracts) {
				if (con.getDealer() != null) {
					dealers.add(con.getDealer());
				}
			}
			
			for (Employee empl: dealers) {
				
				ReportSalesDto rep = new ReportSalesDto();
				Integer lumpsum = 0;
				Integer sales = 0;
				Integer rental = 0;
				Integer total = 0;
				
				lumpsum = conDao.getDealerContractQuantity(cid, bid, empl.getId(), dtMonthStart, dtMonthEnd, 0, 1, false);	
				sales = conDao.getDealerContractQuantity(cid, bid, empl.getId(), dtMonthStart, dtMonthEnd, 2, 99, false);
				rental = conDao.getDealerContractQuantity(cid, bid, empl.getId(), dtMonthStart, dtMonthEnd, 0, 99, true);
				total = lumpsum + sales + rental;

				Employee man = emplDao.findById(manager);
				rep.setManager(man);
				
				if (man != null && man.getAccountableTo() != null 
								&& man.getAccountableTo().getPosition().getId() == Position.POS_DIRECTOR) {
					rep.setDirector(rep.getManager().getAccountableTo());
				}
				
				Company company = companyDao.findcid(cid);
				Branch branch = branchDao.findById(bid);
				
				
				rep.setCompany(company);
				rep.setBranch(branch);
				rep.setPlan(new BigDecimal(0));
				rep.setDealer(empl);
				rep.setSales(new BigDecimal(sales));
				rep.setLumpsum(new BigDecimal(lumpsum));
				rep.setRental(new BigDecimal(rental));
				rep.setFact(new BigDecimal(total));
				rsList.add(rep);
				
			}
			
			
			return rsList;
		} catch (Exception e) {
			throw e;
		}
		
	}
	


	// ********************************************************************************************************************************************

	@Override
	public List<ReportSalesDto> getReportSalesByAllDealers(Integer cid, Integer bid, Date dt) throws Exception {
		try {
			
			List<ReportSalesDto> rsList = new ArrayList<ReportSalesDto>();
			
			Date dtMonthStart = GeneralUtil.startOfMonth(dt);
			Date dtMonthEnd = GeneralUtil.endOfMonth(dt);
			
			List<Contract> contracts = conDao.getAllDealerByDate(cid, bid, dtMonthStart, dtMonthEnd);
			List<Employee> dealers = new ArrayList<Employee>();
			
			for (Contract con: contracts) {
				if (con.getDealer() != null) {
					dealers.add(con.getDealer());
				}
			}
			
//			List<Employee> dealers = emplDao.findAllDealerByCid(cid, bid, dtMonthStart);
			
			for (Employee empl: dealers) {
				ReportSalesDto rep = new ReportSalesDto();
				Integer lumpsum = 0;
				Integer sales = 0;
				Integer rental = 0;
				Integer total = 0;
					
				lumpsum = conDao.getDealerContractQuantity(cid, bid, empl.getId(), dtMonthStart, dtMonthEnd, 0, 1, false);	
				sales = conDao.getDealerContractQuantity(cid, bid, empl.getId(), dtMonthStart, dtMonthEnd, 2, 99, false);
				rental = conDao.getDealerContractQuantity(cid, bid, empl.getId(), dtMonthStart, dtMonthEnd, 0, 99, true);
				total = lumpsum + sales + rental;
				
				if (empl.getAccountableTo() != null 
						&& empl.getAccountableTo() != null
						&& empl.getAccountableTo().getPosition().getId() == Position.POS_MANAGER) {
					rep.setManager(empl.getAccountableTo());
				}
				
				if (rep.getManager() != null 
						&& rep.getManager().getAccountableTo() != null 
						&& rep.getManager().getAccountableTo().getPosition().getId() == Position.POS_DIRECTOR) {
					rep.setDirector(rep.getManager().getAccountableTo());
				}
				
				rep.setCompany(empl.getCompany());
				rep.setBranch(empl.getBranch());
				rep.setPlan(new BigDecimal(0));
				rep.setDealer(empl);
				rep.setSales(new BigDecimal(sales));
				rep.setLumpsum(new BigDecimal(lumpsum));
				rep.setRental(new BigDecimal(rental));
				rep.setFact(new BigDecimal(total));
				rsList.add(rep);
				
			}
			
			return rsList;
		} catch (Exception e) {
			throw e;
		}
	}
	
	
	// *******************************************************************************************************************************************
	
	
	
ReportSalesDto setContractNoEmployee(Integer cid, Integer bid, Date dtMonthStart, Date dtMonthEnd) {
	ReportSalesDto rep = new ReportSalesDto();
	
	Integer lumpsumNoMan = 0;
	Integer salesNoMan = 0;
	Integer rentalNoMan = 0;
	Integer totalNoMan = 0;
	
	lumpsumNoMan = conDao.getNoManagerContractQuantity(cid, bid, dtMonthStart, dtMonthEnd, 0, 1, false);	
	salesNoMan = conDao.getNoManagerContractQuantity(cid, bid, dtMonthStart, dtMonthEnd, 2, 99, false);
	rentalNoMan = conDao.getNoManagerContractQuantity(cid, bid, dtMonthStart, dtMonthEnd, 0, 99, true);
	totalNoMan = lumpsumNoMan + salesNoMan + rentalNoMan;
	
	Company company = companyDao.findcid(cid);
	Branch branch = branchDao.findById(bid);
	
	if (lumpsumNoMan != 0 || salesNoMan != 0 
		|| rentalNoMan != 0 || totalNoMan != 0) {
		rep.setCompany(company);
		rep.setBranch(branch);
		rep.setPlan(new BigDecimal(0));
		rep.setManager(null);
		rep.setSales(new BigDecimal(salesNoMan));
		rep.setLumpsum(new BigDecimal(lumpsumNoMan));
		rep.setRental(new BigDecimal(rentalNoMan));
		rep.setFact(new BigDecimal(totalNoMan));
	} else {
		rep = null;
	}
		
	
	return rep;
}
}
