package com.cordialsr.report.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cordialsr.Dao.BranchDao;
import com.cordialsr.Dao.CompanyDao;
import com.cordialsr.Dao.FinCashflowStatementDao;
import com.cordialsr.Dao.FinEntryDao;
import com.cordialsr.Dao.FinGlAccountDao;
import com.cordialsr.Dao.PartyDao;
import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.FinActivityType;
import com.cordialsr.domain.FinCashflowStatement;
import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.FinGlAccount;
import com.cordialsr.domain.Party;
import com.cordialsr.dto.Balance;
import com.cordialsr.dto.FinRepCashFlowDto;
import com.cordialsr.dto.Money;
import com.cordialsr.dto.ProfitLoss;
import com.cordialsr.dto.TurnoverBalance;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.report.service.FinReportService;

@Service
public class FinReportServiceImpl implements FinReportService {

	@Autowired
	FinEntryDao feDao;
	
	@Autowired
	FinGlAccountDao glDao;
	
	@Autowired
	CompanyDao comDao;
	
	@Autowired 
	PartyDao partyDao;
	
	@Autowired
	BranchDao brDao;
	
	@Autowired
	FinCashflowStatementDao cfsDao;
	
	
	@Override
	public ProfitLoss getProfitLoss(Integer cid, String ds, String de) throws Exception {
		try {
			Company company = comDao.findOne(cid);
			
			List<TurnoverBalance> incomeTbL = new ArrayList<>();
			BigDecimal incomeTotalSum = BigDecimal.ZERO;
			List<Balance> incomeB = feDao.getTurnoverBalanceForSection(cid, ds, de, "6");
			for (Balance bs: incomeB) {
				TurnoverBalance tb = new TurnoverBalance();
				FinGlAccount gl = glDao.findOne(bs.getGlCode());
				tb.setGlAccount(gl);
				tb.setTurnover(bs);
				
				Balance be = initNewBalance(company.getCurrency().getCurrency(), FinGlAccount.AKT_ACTIVE);
				be.getDebet().setSumm(bs.getCredit().getSumm().subtract(bs.getDebet().getSumm()));
				
				incomeTotalSum = incomeTotalSum.add(be.getDebet().getSumm());
				be.getDebet().setCurrency(bs.getDebet().getCurrency());
				tb.setBalanceEnd(be);
				
				incomeTbL.add(tb);
			}
			Balance incomeTotal = initNewBalance(company.getCurrency().getCurrency(), FinGlAccount.AKT_ACTIVE);
			incomeTotal.getDebet().setSumm(incomeTotalSum);
			
			List<TurnoverBalance> expenceTbL = new ArrayList<>();
			BigDecimal expenceTotalSum = BigDecimal.ZERO;
			List<Balance> expenceB = feDao.getTurnoverBalanceForSection(cid, ds, de, "7");
			for (Balance bs: expenceB) {
				TurnoverBalance tb = new TurnoverBalance();
				FinGlAccount gl = glDao.findOne(bs.getGlCode());
				tb.setGlAccount(gl);
				tb.setTurnover(bs);
				
				Balance be = initNewBalance(company.getCurrency().getCurrency(), FinGlAccount.AKT_ACTIVE);
				be.getDebet().setSumm(bs.getDebet().getSumm().subtract(bs.getCredit().getSumm()));
				
				expenceTotalSum = expenceTotalSum.add(be.getDebet().getSumm());
				be.getDebet().setCurrency(bs.getDebet().getCurrency());
				tb.setBalanceEnd(be);
				expenceTbL.add(tb);
			}
			
			Balance expenceTotal = initNewBalance(company.getCurrency().getCurrency(), FinGlAccount.AKT_ACTIVE);
			expenceTotal.getDebet().setSumm(expenceTotalSum);
			
			Balance total = initNewBalance(company.getCurrency().getCurrency(), FinGlAccount.AKT_ACTIVE);
			total.getDebet().setSumm(incomeTotalSum.subtract(expenceTotalSum));
			
			ProfitLoss pl = new ProfitLoss();
			
			pl.setIncomeTbL(incomeTbL);
			pl.setIncomeTotal(incomeTotal);
			pl.setExpenceTbL(expenceTbL);
			pl.setExpenceTotal(expenceTotal);
			pl.setTotal(total);
			
			return pl;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<TurnoverBalance> getTurnoverbalance(Integer cid, String ds, String de) throws Exception {
		try {
			Company company = comDao.findOne(cid);
			List<TurnoverBalance> tbL = new ArrayList<>();
			
			// Initial Balance
			List<Balance> ib = feDao.getInitialBalance(cid, ds);
			for (Balance bs: ib) {
				TurnoverBalance tb = new TurnoverBalance();
				FinGlAccount gl = glDao.findOne(bs.getGlCode());
				tb.setGlAccount(gl);
				tb.setBalanceStart(bs);
				tbL.add(tb);
			}

			// Turnover for period
			List<Balance> toL = feDao.getTurnoverBalance(cid, ds, de);
			for (Balance to: toL) {
				TurnoverBalance tb = findTbByGl(tbL, to.getGlCode());
				if (tb == null) {
					tb = new TurnoverBalance();
					FinGlAccount gl = glDao.findOne(to.getGlCode());
					tb.setGlAccount(gl);
					tbL.add(tb);
				}
				tb.setTurnover(to);
			}
			
			// Balance at the end of the period
			TurnoverBalance total = new TurnoverBalance();
			Balance tbs = initNewBalance(company.getCurrency().getCurrency(), 2);
			Balance tto = initNewBalance(company.getCurrency().getCurrency(), 2);
			Balance tbe = initNewBalance(company.getCurrency().getCurrency(), 2);
			total.setBalanceStart(tbs);
			total.setTurnover(tto);
			total.setBalanceEnd(tbe);
			FinGlAccount tgl = new FinGlAccount();
			tgl.setName("ИТОГО");
			total.setGlAccount(tgl);
			
			for (TurnoverBalance tb: tbL) {
				Balance eb = new Balance(tb.getGlAccount().getCode());
				if (tb.getBalanceStart() != null) {
					eb = initNewBalance(company.getCurrency().getCurrency(), tb.getGlAccount().getAkt());
					if (tb.getTurnover() != null) {
						if (tb.getBalanceStart().getDebet() != null) {
							eb.getDebet().setSumm(tb.getBalanceStart().getDebet().getSumm());
						}
						if (tb.getBalanceStart().getCredit() != null) {
							eb.getCredit().setSumm(tb.getBalanceStart().getCredit().getSumm());
						}
					} else {
						if (eb.getDebet() != null) {
							eb.getDebet().setSumm(tb.getBalanceStart().getDebet().getSumm());
						}
						if (eb.getCredit() != null) {
							eb.getCredit().setSumm(tb.getBalanceStart().getCredit().getSumm());
						}
					}
				} else {
					Balance bs = initNewBalance(company.getCurrency().getCurrency(), tb.getGlAccount().getAkt());
					tb.setBalanceStart(bs);
					eb = initNewBalance(company.getCurrency().getCurrency(), tb.getGlAccount().getAkt());
				}
				tb.setBalanceEnd(eb);
				
				if (tb.getBalanceStart().getDebet() != null) {
					BigDecimal diff = new BigDecimal(0);
					if (tb.getTurnover() != null) {
						diff = tb.getTurnover().getDebet().getSumm().subtract(tb.getTurnover().getCredit().getSumm());
					}
					eb.getDebet().setSumm(eb.getDebet().getSumm().add(diff));
					
					total.getBalanceEnd().getDebet().setSumm(total.getBalanceEnd().getDebet().getSumm().add(tb.getBalanceEnd().getDebet().getSumm()));
					total.getBalanceStart().getDebet().setSumm(total.getBalanceStart().getDebet().getSumm().add(tb.getBalanceStart().getDebet().getSumm()));
					
				} else if (tb.getBalanceStart().getCredit() != null) {
					BigDecimal diff = new BigDecimal(0);
					if (tb.getTurnover() != null) {
						diff = tb.getTurnover().getCredit().getSumm().subtract(tb.getTurnover().getDebet().getSumm());
					}
					eb.getCredit().setSumm(eb.getCredit().getSumm().add(diff));
					
					total.getBalanceEnd().getCredit().setSumm(total.getBalanceEnd().getCredit().getSumm().add(tb.getBalanceEnd().getCredit().getSumm()));
					total.getBalanceStart().getCredit().setSumm(total.getBalanceStart().getCredit().getSumm().add(tb.getBalanceStart().getCredit().getSumm()));
				}
				
				if (tb.getTurnover() != null) {
					total.getTurnover().getDebet().setSumm(total.getTurnover().getDebet().getSumm().add(tb.getTurnover().getDebet().getSumm()));
					total.getTurnover().getCredit().setSumm(total.getTurnover().getCredit().getSumm().add(tb.getTurnover().getCredit().getSumm()));	
				}
			}
			
			tbL.sort(new Comparator<TurnoverBalance>() {
				@Override
				public int compare(TurnoverBalance o1, TurnoverBalance o2) {
					return Integer.valueOf(o1.getGlAccount().getCode()).compareTo(Integer.valueOf(o2.getGlAccount().getCode()));
				}
			});
			
			tbL.add(total);
			return tbL;
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public List<TurnoverBalance> getTurnoverbalancePreviews(Integer cid, String ds, String de)  throws Exception {
		try {
			Company company = comDao.findOne(cid);
			List<TurnoverBalance> tbL = new ArrayList<>();
			
			// Initial Balance
			List<Balance> ib = feDao.getInitialTotals(cid, ds);
			for (Balance bs: ib) {
				TurnoverBalance tb = new TurnoverBalance();
				FinGlAccount gl = glDao.findOne(bs.getGlCode());
				tb.setGlAccount(gl);
				tb.setBalanceStart(bs);
				tbL.add(tb);
			}

			// Turnover for period
			List<Balance> toL = feDao.getTurnoverBalance(cid, ds, de);
			for (Balance to: toL) {
				TurnoverBalance tb = findTbByGl(tbL, to.getGlCode());
				if (tb == null) {
					tb = new TurnoverBalance();
					FinGlAccount gl = glDao.findOne(to.getGlCode());
					tb.setGlAccount(gl);
					tbL.add(tb);
				}
				tb.setTurnover(to);
			}
			
			// Balance at the end of the period
			TurnoverBalance total = new TurnoverBalance();
			Balance tbs = initNewBalance(company.getCurrency().getCurrency(), 2);
			Balance tto = initNewBalance(company.getCurrency().getCurrency(), 2);
			Balance tbe = initNewBalance(company.getCurrency().getCurrency(), 2);
			total.setBalanceStart(tbs);
			total.setTurnover(tto);
			total.setBalanceEnd(tbe);
			FinGlAccount tgl = new FinGlAccount();
			tgl.setName("ИТОГО");
			total.setGlAccount(tgl);
			
			for (TurnoverBalance tb: tbL) {
				Balance eb = new Balance(tb.getGlAccount().getCode());
				if (tb.getBalanceStart() != null) {
					eb = new Balance(); 
					eb = tb.getBalanceStart().clone();
					if (tb.getTurnover() != null) {
						BigDecimal db = tb.getTurnover().getDebet().getSumm();
						BigDecimal cr = tb.getTurnover().getCredit().getSumm();
						eb.getDebet().setSumm(tb.getBalanceStart().getDebet().getSumm().add(db));
						eb.getCredit().setSumm(tb.getBalanceStart().getCredit().getSumm().add(cr));
					} else {
						Balance to = initNewBalance(company.getCurrency().getCurrency(), 2);
						tb.setTurnover(to);
					}
				} else {
					Balance bs = initNewBalance(company.getCurrency().getCurrency(), tb.getGlAccount().getAkt());
					tb.setBalanceStart(bs);
					eb = tb.getTurnover().clone();
				}
				tb.setBalanceEnd(eb);
				
				total.getBalanceStart().getDebet().setSumm(total.getBalanceStart().getDebet().getSumm().add(tb.getBalanceStart().getDebet().getSumm()));
				total.getBalanceStart().getCredit().setSumm(total.getBalanceStart().getCredit().getSumm().add(tb.getBalanceStart().getCredit().getSumm()));
				
				total.getTurnover().getDebet().setSumm(total.getTurnover().getDebet().getSumm().add(tb.getTurnover().getDebet().getSumm()));
				total.getTurnover().getCredit().setSumm(total.getTurnover().getCredit().getSumm().add(tb.getTurnover().getCredit().getSumm()));
				
				total.getBalanceEnd().getDebet().setSumm(total.getBalanceEnd().getDebet().getSumm().add(tb.getBalanceEnd().getDebet().getSumm()));
				total.getBalanceEnd().getCredit().setSumm(total.getBalanceEnd().getCredit().getSumm().add(tb.getBalanceEnd().getCredit().getSumm()));
			}
			tbL.add(total);
			return tbL;
		} catch (Exception e) {
			throw e;
		}
	}
	
	private Balance initNewBalance(String currency, Integer akt) {
		Balance newBalance = new Balance();
		if (akt == FinGlAccount.AKT_ACTIVE || akt == 2) {
			Money db = new Money(new BigDecimal(0), currency);
			newBalance.setDebet(db);
		}
		if (akt == FinGlAccount.AKT_PASSIVE  || akt == 2) {
			Money cr = new Money(new BigDecimal(0), currency);
			newBalance.setCredit(cr);
		}
		return newBalance;
	}
	
	private TurnoverBalance findTbByGl(List<TurnoverBalance> tbL, String glCode) {
		for (TurnoverBalance tb : tbL) {
			if (tb.getGlAccount().getCode().equals(glCode)) {
				return tb;
			}
		}
		return null; 
	}
	
	private TurnoverBalance findTbByPartyId(List<TurnoverBalance> tbL, Long partyId) {
		for (TurnoverBalance tb : tbL) {
			if (tb.getParty() == null && partyId == null 
					|| tb.getParty() != null && tb.getParty().getId().equals(partyId)) {
				return tb;
			}
		}
		return null; 
	}

	@Override
	public List<TurnoverBalance> getInnerTurnoverBalanceByCFStatement(Integer cid, String glCode, String ds, String de)
			throws Exception {
		try {
			Company company = comDao.findOne(cid);
			List<TurnoverBalance> tbL = new ArrayList<>();
			
			// Initial Balance
			List<Balance> ib = feDao.getInnerCfsInitialBalance(cid, glCode, ds);
			for (Balance bs: ib) {
				TurnoverBalance tb = new TurnoverBalance();
				if (!GeneralUtil.isEmptyLong(bs.getPartyId())) {
					FinCashflowStatement cfs = cfsDao.findOne(Integer.valueOf(bs.getPartyId().toString()));
					tb.setCfs(cfs);
				}
				FinGlAccount gl = glDao.findOne(bs.getGlCode());
				tb.setGlAccount(gl);
				tb.setBalanceStart(bs);
				tbL.add(tb);
			}

			// Turnover for period
			List<Balance> toL = feDao.getInnerCfsTurnoverBalance(cid, glCode, ds, de);
			for (Balance to: toL) {
				TurnoverBalance tb = findTbByPartyId(tbL, to.getPartyId());
				if (tb == null) {
					tb = new TurnoverBalance();
					if (!GeneralUtil.isEmptyLong(to.getPartyId())) {
						FinCashflowStatement cfs = cfsDao.findOne(Integer.valueOf(to.getPartyId().toString()));
						tb.setCfs(cfs);
					}
					FinGlAccount gl = glDao.findOne(to.getGlCode());
					tb.setGlAccount(gl);
					tbL.add(tb);
				}
				tb.setTurnover(to);
			}
			
			// Balance at the end of the period
			TurnoverBalance total = new TurnoverBalance();
			Balance tbs = initNewBalance(company.getCurrency().getCurrency(), 2);
			Balance tto = initNewBalance(company.getCurrency().getCurrency(), 2);
			Balance tbe = initNewBalance(company.getCurrency().getCurrency(), 2);
			total.setBalanceStart(tbs);
			total.setTurnover(tto);
			total.setBalanceEnd(tbe);
			Party tp = new Party();
			tp.setCompanyName("ИТОГО");
			total.setParty(tp);
			FinCashflowStatement cfs = new FinCashflowStatement();
			total.setCfs(cfs);
			
			for (TurnoverBalance tb: tbL) {
				Balance eb = new Balance(tb.getGlAccount().getCode());
				if (tb.getBalanceStart() != null) {
					eb = initNewBalance(company.getCurrency().getCurrency(), tb.getGlAccount().getAkt());
					if (tb.getTurnover() != null) {
						BigDecimal db = tb.getTurnover().getDebet().getSumm();
						BigDecimal cr = tb.getTurnover().getCredit().getSumm();
						if (tb.getBalanceStart().getDebet() != null) {
							eb.getDebet().setSumm(tb.getBalanceStart().getDebet().getSumm().add(db));
						}
						if (tb.getBalanceStart().getCredit() != null) {
							eb.getCredit().setSumm(tb.getBalanceStart().getCredit().getSumm().add(cr));
						}
					} else {
						if (eb.getDebet() != null) {
							eb.getDebet().setSumm(tb.getBalanceStart().getDebet().getSumm());;
						}
						if (eb.getCredit() != null) {
							eb.getCredit().setSumm(tb.getBalanceStart().getCredit().getSumm());;
						}
					}
				} else {
					Balance bs = initNewBalance(company.getCurrency().getCurrency(), tb.getGlAccount().getAkt());
					tb.setBalanceStart(bs);
					eb = initNewBalance(company.getCurrency().getCurrency(), tb.getGlAccount().getAkt());
				}
				tb.setBalanceEnd(eb);
				
				if (tb.getBalanceStart().getDebet() != null) {
					BigDecimal diff = new BigDecimal(0);
					if (tb.getTurnover() != null) {
						diff = tb.getTurnover().getDebet().getSumm().subtract(tb.getTurnover().getCredit().getSumm());
					}
					eb.getDebet().setSumm(eb.getDebet().getSumm().add(diff));
					
					total.getBalanceEnd().getDebet().setSumm(total.getBalanceEnd().getDebet().getSumm().add(tb.getBalanceEnd().getDebet().getSumm()));
					total.getBalanceStart().getDebet().setSumm(total.getBalanceStart().getDebet().getSumm().add(tb.getBalanceStart().getDebet().getSumm()));
					
				} else if (tb.getBalanceStart().getCredit() != null) {
					BigDecimal diff = new BigDecimal(0);
					if (tb.getTurnover() != null) {
						diff = tb.getTurnover().getCredit().getSumm().subtract(tb.getTurnover().getDebet().getSumm());
					}
					eb.getCredit().setSumm(eb.getCredit().getSumm().add(diff));
					
					total.getBalanceEnd().getCredit().setSumm(total.getBalanceEnd().getCredit().getSumm().add(tb.getBalanceEnd().getCredit().getSumm()));
					total.getBalanceStart().getCredit().setSumm(total.getBalanceStart().getCredit().getSumm().add(tb.getBalanceStart().getCredit().getSumm()));
				}
				
				if (tb.getTurnover() != null) {
					total.getTurnover().getDebet().setSumm(total.getTurnover().getDebet().getSumm().add(tb.getTurnover().getDebet().getSumm()));
					total.getTurnover().getCredit().setSumm(total.getTurnover().getCredit().getSumm().add(tb.getTurnover().getCredit().getSumm()));	
				}
			}
			
			tbL.sort(new Comparator<TurnoverBalance>() {
				@Override
				public int compare(TurnoverBalance o1, TurnoverBalance o2) {
					return Integer.valueOf(o1.getGlAccount().getCode()).compareTo(Integer.valueOf(o2.getGlAccount().getCode()));
				}
			});
			
			tbL.add(total);
			return tbL;
		} catch (Exception e) {
			throw e;
		}
	}

	

	@Override
	public List<TurnoverBalance> getInnerTurnoverBalanceByParty(Integer cid, String glCode, Integer cfs, String ds, String de)
			throws Exception {
		try {
			Company company = comDao.findOne(cid);
			List<TurnoverBalance> tbL = new ArrayList<>();
			
			// Initial Balance
			List<Balance> ib = feDao.getInnerPartyInitialBalance(cid, glCode, cfs, ds);
			for (Balance bs: ib) {
				TurnoverBalance tb = new TurnoverBalance();
				if (!GeneralUtil.isEmptyLong(bs.getPartyId())) {
					Party party = partyDao.findOne(bs.getPartyId());
					tb.setParty(party);	
				}
				FinGlAccount gl = glDao.findOne(bs.getGlCode());
				tb.setGlAccount(gl);
				tb.setBalanceStart(bs);
				tbL.add(tb);
			}

			// Turnover for period
			List<Balance> toL = feDao.getInnerPartyTurnoverBalance(cid, glCode, cfs, ds, de);
			for (Balance to: toL) {
				TurnoverBalance tb = findTbByPartyId(tbL, to.getPartyId());
				if (tb == null) {
					tb = new TurnoverBalance();
					if (!GeneralUtil.isEmptyLong(to.getPartyId())) {
						Party party = partyDao.findOne(to.getPartyId());
						tb.setParty(party);	
					}
					FinGlAccount gl = glDao.findOne(to.getGlCode());
					tb.setGlAccount(gl);
					tbL.add(tb);
				}
				tb.setTurnover(to);
			}
			
			// Balance at the end of the period
			TurnoverBalance total = new TurnoverBalance();
			Balance tbs = initNewBalance(company.getCurrency().getCurrency(), 2);
			Balance tto = initNewBalance(company.getCurrency().getCurrency(), 2);
			Balance tbe = initNewBalance(company.getCurrency().getCurrency(), 2);
			total.setBalanceStart(tbs);
			total.setTurnover(tto);
			total.setBalanceEnd(tbe);
			Party tp = new Party();
			tp.setCompanyName("ИТОГО");
			total.setParty(tp);
			
			for (TurnoverBalance tb: tbL) {
				Balance eb = new Balance(tb.getGlAccount().getCode());
				if (tb.getBalanceStart() != null) {
					eb = initNewBalance(company.getCurrency().getCurrency(), tb.getGlAccount().getAkt());
					if (tb.getTurnover() != null) {
						BigDecimal db = tb.getTurnover().getDebet().getSumm();
						BigDecimal cr = tb.getTurnover().getCredit().getSumm();
						if (tb.getBalanceStart().getDebet() != null) {
							eb.getDebet().setSumm(tb.getBalanceStart().getDebet().getSumm().add(db));
						}
						if (tb.getBalanceStart().getCredit() != null) {
							eb.getCredit().setSumm(tb.getBalanceStart().getCredit().getSumm().add(cr));
						}
					} else {
						if (eb.getDebet() != null) {
							eb.getDebet().setSumm(tb.getBalanceStart().getDebet().getSumm());;
						}
						if (eb.getCredit() != null) {
							eb.getCredit().setSumm(tb.getBalanceStart().getCredit().getSumm());;
						}
					}
				} else {
					Balance bs = initNewBalance(company.getCurrency().getCurrency(), tb.getGlAccount().getAkt());
					tb.setBalanceStart(bs);
					eb = initNewBalance(company.getCurrency().getCurrency(), tb.getGlAccount().getAkt());
				}
				tb.setBalanceEnd(eb);
				
				if (tb.getBalanceStart().getDebet() != null) {
					BigDecimal diff = new BigDecimal(0);
					if (tb.getTurnover() != null) {
						diff = tb.getTurnover().getDebet().getSumm().subtract(tb.getTurnover().getCredit().getSumm());
					}
					eb.getDebet().setSumm(eb.getDebet().getSumm().add(diff));
					
					total.getBalanceEnd().getDebet().setSumm(total.getBalanceEnd().getDebet().getSumm().add(tb.getBalanceEnd().getDebet().getSumm()));
					total.getBalanceStart().getDebet().setSumm(total.getBalanceStart().getDebet().getSumm().add(tb.getBalanceStart().getDebet().getSumm()));
					
				} else if (tb.getBalanceStart().getCredit() != null) {
					BigDecimal diff = new BigDecimal(0);
					if (tb.getTurnover() != null) {
						diff = tb.getTurnover().getCredit().getSumm().subtract(tb.getTurnover().getDebet().getSumm());
					}
					eb.getCredit().setSumm(eb.getCredit().getSumm().add(diff));
					
					total.getBalanceEnd().getCredit().setSumm(total.getBalanceEnd().getCredit().getSumm().add(tb.getBalanceEnd().getCredit().getSumm()));
					total.getBalanceStart().getCredit().setSumm(total.getBalanceStart().getCredit().getSumm().add(tb.getBalanceStart().getCredit().getSumm()));
				}
				
				if (tb.getTurnover() != null) {
					total.getTurnover().getDebet().setSumm(total.getTurnover().getDebet().getSumm().add(tb.getTurnover().getDebet().getSumm()));
					total.getTurnover().getCredit().setSumm(total.getTurnover().getCredit().getSumm().add(tb.getTurnover().getCredit().getSumm()));	
				}
			}
			
			tbL.sort(new Comparator<TurnoverBalance>() {
				@Override
				public int compare(TurnoverBalance o1, TurnoverBalance o2) {
					return Integer.valueOf(o1.getGlAccount().getCode()).compareTo(Integer.valueOf(o2.getGlAccount().getCode()));
				}
			});
			
			tbL.add(total);
			return tbL;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// ********************************************************************************************************************
	
	@Override
	public List<FinRepCashFlowDto> getCashFlowGeneral(Integer cid, String ds, String de) throws Exception {
		try {
			Company company = comDao.findOne(cid);
			if (company != null) {
				List<FinRepCashFlowDto> frcfL = new ArrayList<>();
				List<Map<String, Object>> res = feDao.getCashflowGeneral(cid, ds, de);
				Money startBalance = feDao.getCashBalance(cid, ds, "");
				FinCashflowStatement cfStart = new FinCashflowStatement();
				cfStart.setName("Денежные средства на начало периода");
				if (startBalance == null) {
					startBalance = new Money();
					startBalance.setSumm(new BigDecimal(0));
					startBalance.setCurrency(company.getCurrency().getCurrency());
				}
				FinRepCashFlowDto frcfStart = new FinRepCashFlowDto(null, cfStart, null, null, startBalance, null, null);
				frcfL.add(frcfStart);
				
				for (Map<String, Object> r: res) {
					FinActivityType fat = new FinActivityType(String.valueOf(r.get("code")));
					fat.setName(String.valueOf(r.get("name")));
					
					FinCashflowStatement cf = new FinCashflowStatement(Integer.valueOf(String.valueOf(r.get("cfId"))));
					cf.setName(String.valueOf(r.get("cfName")));
					cf.setFinActType(fat);
					
					Balance balance = new Balance();
					Money db = new Money();
					db.setSumm(new BigDecimal(String.valueOf(r.get("db"))));
					db.setCurrency(String.valueOf(r.get("currency")));
					Money cr = new Money();
					cr.setSumm(new BigDecimal(String.valueOf(r.get("cr"))));
					cr.setCurrency(String.valueOf(r.get("currency")));
					if (!GeneralUtil.isEmptyBigDecimal(db.getSumm())) balance.setDebet(db);
					if (!GeneralUtil.isEmptyBigDecimal(cr.getSumm())) balance.setCredit(cr);
					
					
					Money total = new Money();
					total.setSumm(new BigDecimal(String.valueOf(r.get("sum"))));
					total.setCurrency(String.valueOf(r.get("currency")));
					
					FinRepCashFlowDto frcf = new FinRepCashFlowDto(null, cf, null, balance, total, null, null);
					frcfL.add(frcf);
				}
				Money endBalance = feDao.getCashBalance(cid, de, "=");
				FinCashflowStatement cfEnd = new FinCashflowStatement();
				cfEnd.setName("Денежные средства на конец периода");
				FinRepCashFlowDto frcfEnd = new FinRepCashFlowDto(null, cfEnd, null, null, endBalance, null, null);
				frcfL.add(frcfEnd);
				
				return frcfL;
			} else {
				throw new Exception("Company with id [" + cid + "] not found.");
			}
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<FinRepCashFlowDto> getCashFlowBranch(Integer cid, Integer cfId, String ds, String de) throws Exception {
		List<FinRepCashFlowDto> frcfL = new ArrayList<>();
		List<Map<String, Object>> res = feDao.getCashflowBranch(cid, cfId, ds, de);
		
		for (Map<String, Object> r: res) {
			Branch branch = brDao.findOne(Integer.valueOf(String.valueOf(r.get("branch"))));
			
			FinCashflowStatement cf = new FinCashflowStatement(Integer.valueOf(String.valueOf(r.get("cfId"))));
			cf.setName(String.valueOf(r.get("cfName")));
			
			Balance balance = new Balance();
			Money db = new Money();
			db.setSumm(new BigDecimal(String.valueOf(r.get("db"))));
			db.setCurrency(String.valueOf(r.get("currency")));
			Money cr = new Money();
			cr.setSumm(new BigDecimal(String.valueOf(r.get("cr"))));
			cr.setCurrency(String.valueOf(r.get("currency")));
			if (!GeneralUtil.isEmptyBigDecimal(db.getSumm())) balance.setDebet(db);
			if (!GeneralUtil.isEmptyBigDecimal(cr.getSumm())) balance.setCredit(cr);
			
			
			Money total = new Money();
			total.setSumm(new BigDecimal(String.valueOf(r.get("sum"))));
			total.setCurrency(String.valueOf(r.get("currency")));
			
			FinRepCashFlowDto frcf = new FinRepCashFlowDto(branch, cf, null, balance, total, null, null);
			frcfL.add(frcf);
		}
		
		return frcfL;
	}
	
	@Override
	public List<FinRepCashFlowDto> getCashFlowDetailed(Integer cid, Integer bid, Integer cfId, String ds, String de)
			throws Exception {
		List<FinRepCashFlowDto> frcfL = new ArrayList<>();
		List<Map<String, Object>> res = feDao.getCashflowDetailed(cid, bid, cfId, ds, de);
		
		for (Map<String, Object> r: res) {
			FinCashflowStatement cf = new FinCashflowStatement(Integer.valueOf(String.valueOf(r.get("cfId"))));
			cf.setName(String.valueOf(r.get("cfName")));
			
			Party party = new Party(Long.valueOf(String.valueOf(r.get("partyId"))));
			party.setFirstname(String.valueOf(r.get("fio")));
			
			Balance balance = new Balance();
			Money db = new Money();
			db.setSumm(new BigDecimal(String.valueOf(r.get("db"))));
			db.setCurrency(String.valueOf(r.get("currency")));
			Money cr = new Money();
			cr.setSumm(new BigDecimal(String.valueOf(r.get("cr"))));
			cr.setCurrency(String.valueOf(r.get("currency")));
			if (!GeneralUtil.isEmptyBigDecimal(db.getSumm())) balance.setDebet(db);
			if (!GeneralUtil.isEmptyBigDecimal(cr.getSumm())) balance.setCredit(cr);
			
			Money total = new Money();
			total.setSumm(new BigDecimal(String.valueOf(r.get("sum"))));
			total.setCurrency(String.valueOf(r.get("currency")));
			
			String info = String.valueOf(r.get("info"));
			
			FinDoc finDoc = new FinDoc(Long.valueOf(String.valueOf(r.get("finDocId"))));
			finDoc.setDocno(String.valueOf(r.get("docno")));
			
			FinRepCashFlowDto frcf = new FinRepCashFlowDto(null, cf, party, balance, total, info, finDoc);
			frcfL.add(frcf);
		}
		
		return frcfL;
	}
}
