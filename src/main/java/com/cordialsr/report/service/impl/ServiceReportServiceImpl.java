package com.cordialsr.report.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.SmFilterChangeDao;
import com.cordialsr.Dao.SmServiceDao;
import com.cordialsr.domain.SmCall;
import com.cordialsr.domain.SmService;
import com.cordialsr.domain.factory.SmServiceReportFactory;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.report.service.ServiceReportService;
import com.cordialsr.report.service.SmCallReportService;
import com.cordialsr.service.PlanFactService;

@Service
@Transactional(readOnly = true)
public class ServiceReportServiceImpl implements ServiceReportService {

	@Autowired
	SmServiceDao serviceDao;
	
	@Autowired
	SmFilterChangeDao smFcDao;
	
	@Autowired
	SmCallReportService smCallRepSer;
	
	@Autowired
	PlanFactService pfService;
	
	@Override
	public List<SmService> reportNewService(Integer cid, Date fromDate, Date toDate) throws Exception {
		try {
			fromDate = GeneralUtil.startOfDay(fromDate);
			toDate = GeneralUtil.endOfDay(toDate);
			List<SmService> service = serviceDao.findAllForReport(cid, fromDate, toDate);
			
			List<Map<String, Object>> res = smFcDao.getCaremenCurrentPlan(cid, fromDate);
			
			for (Map<String, Object> m : res) {
				if (m.get("careman") != null) {
					Long emplId = Long.valueOf(String.valueOf(m.get("careman")));
					Integer plan = Integer.valueOf(String.valueOf(m.get("plan")));
					SmService s = findByCaremanId(service, emplId);
					if (s != null) s.setDiscount(new BigDecimal(plan));
				}
			}
			
			return service;
		}catch(Exception ex) {
			throw ex;
		}
		
	}
	
	private SmService findByCaremanId(List<SmService> smServL, Long caremanId) {
		for (SmService s: smServL) {
			if (s.getCareman().getId().equals(caremanId)) return s;
		}
		return null;
	}
	
	@Override
	public String reportOnExcel(Integer cid, Date fromDate, Date toDate) throws Exception {
		try {
			List<SmService> services = reportNewService(cid, fromDate, toDate);
			ArrayList<SmCall> calls = smCallRepSer.reportNewCalls(cid, fromDate, toDate);
			String ekzel = SmServiceReportFactory.createExcelFile(services, calls, fromDate);
			return ekzel;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<SmService> reportNewServiceOnPeriod(Integer cid, Date fromDate, Date toDate) throws Exception {
		try {
			
			fromDate = GeneralUtil.startOfDay(fromDate);
			toDate = GeneralUtil.endOfDay(toDate);
			List<SmService> service = serviceDao.findAllForRepOnPeriod(cid, fromDate, toDate);
			List<SmService> smSer = new ArrayList<SmService>();
			for (SmService ser: service) {	
				SmService serv = ser.clone();
				if (!ser.getSdate().equals(null)) {
					BigDecimal pf = pfService.getPlanFactCaremansByService(ser);
					serv.setDiscount(pf);
				}
				smSer.add(serv);
			}
			return smSer;
		} 	catch (Exception ex) {
			throw ex;
		}
	}
	
	
}
