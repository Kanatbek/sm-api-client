package com.cordialsr.report.service.impl;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.SmCallDao;
import com.cordialsr.domain.SmCall;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.report.service.SmCallReportService;

@Service
@Transactional(readOnly = true)
public class SmCallReportServiceImpl implements SmCallReportService{
	
	@Autowired 
	SmCallDao smCallDao;
	
	@Override
	public ArrayList<SmCall> reportNewCalls(Integer cid, Date fromDate, Date toDate) throws Exception {
		try {
			fromDate = GeneralUtil.startOfDay(fromDate);
			toDate = GeneralUtil.endOfDay(toDate);
			ArrayList<SmCall> call= smCallDao.findAllForReport(cid, fromDate, toDate);
			return call;
		}catch(Exception ex) {
			throw ex;
		}
	}
	
}
