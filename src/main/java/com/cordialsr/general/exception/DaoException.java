package com.cordialsr.general.exception;

public class DaoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public DaoException() {		
	}

	public DaoException(String msg) {
		super(msg);
	}
	
	public DaoException(Exception e) {
		super(e);
	}
	
	public static String justifyMessage(String msg) {
		if (msg != null && msg.length() > 0) {
			int p = msg.indexOf(':');
			msg = msg.substring(p+1, msg.length());
		}
		return msg;
	}
	
	public static String justifyMessageFull(String msg) {
		while (msg.contains("Exception")) {
			int p = msg.indexOf(':');
			msg = msg.substring(p+2, msg.length());
		}
		return msg;
	}
}
