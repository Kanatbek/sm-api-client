package com.cordialsr.general;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class GeneralUtil {

	public static Date addMonth(Date d, int month) {
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.add(Calendar.MONTH, month);  // number of days to add
		return c.getTime();
	}
	
	public static int calcAgeInMonth(Date d1, Date d2) {
		Calendar cd1 = Calendar.getInstance();
		cd1.setTime(d1);
		Calendar cd2 = Calendar.getInstance();
		cd2.setTime(d2);		
		int year = cd2.get(Calendar.YEAR) - cd1.get(Calendar.YEAR);
		int month = cd2.get(Calendar.MONTH) - cd1.get(Calendar.MONTH);
		int age = (year*12) + month;
		return age;
	}
	
	public static long calcAgeInDays(Date d1, Date d2) {
	    long diff = d2.getTime() - d1.getTime();
	    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}
	
	
	public static Date startOfMonth(Date d) {
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.set(Calendar.HOUR_OF_DAY, c.getActualMinimum(Calendar.HOUR_OF_DAY));
		c.set(Calendar.MINUTE, c.getActualMinimum(Calendar.MINUTE));
		c.set(Calendar.SECOND, c.getActualMinimum(Calendar.SECOND));
		c.set(Calendar.MILLISECOND, c.getActualMinimum(Calendar.MILLISECOND));
		c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
		return c.getTime();
	}
	
	public static Date endOfMonth(Date d) {
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		c.set(Calendar.HOUR_OF_DAY, c.getActualMaximum(Calendar.HOUR_OF_DAY));
		c.set(Calendar.MINUTE, c.getActualMaximum(Calendar.MINUTE));
		c.set(Calendar.SECOND, c.getActualMaximum(Calendar.SECOND));
		c.set(Calendar.MILLISECOND, c.getActualMaximum(Calendar.MILLISECOND));
		return c.getTime();
	}
	
	public static Date startOfDay(Date d) {
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.set(Calendar.HOUR_OF_DAY, c.getActualMinimum(Calendar.HOUR_OF_DAY));
		c.set(Calendar.MINUTE, c.getActualMinimum(Calendar.MINUTE));
		c.set(Calendar.SECOND, c.getActualMinimum(Calendar.SECOND));
		c.set(Calendar.MILLISECOND, c.getActualMinimum(Calendar.MILLISECOND));
		return c.getTime();
	}
	
	public static Date endOfDay(Date d) {
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.set(Calendar.HOUR_OF_DAY, c.getActualMaximum(Calendar.HOUR_OF_DAY));
		c.set(Calendar.MINUTE, c.getActualMaximum(Calendar.MINUTE));
		c.set(Calendar.SECOND, c.getActualMaximum(Calendar.SECOND));
		c.set(Calendar.MILLISECOND, c.getActualMaximum(Calendar.MILLISECOND));
		return c.getTime();
	}
	
	public static boolean isEmptyInteger(Integer number) {
		return ((number == null) || (number <= 0));
	}
	
	public static boolean isEmptyLong(Long number) {
		return ((number == null) || (number <= 0));
	}
	
	public static boolean isEmptyOrNegativeSumm(BigDecimal number) {
		return ((number == null) || (number.compareTo(new BigDecimal(0)) < 0));
	}
	
	public static boolean isEmptyBigDecimal(BigDecimal number) {
		return ((number == null) || (number.compareTo(new BigDecimal(0)) <= 0));
	}

	public static boolean isEmptyString(String text) {
		if (text != null) {
			String copyTxt = text.substring(0, text.length());
			copyTxt = copyTxt.trim();
			return (copyTxt.length() == 0);
		}
		return true;
	}

	public static Calendar getCalendar(Integer month, Integer year) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MONTH, (month-1));
		cal.set(Calendar.YEAR, year);
		cal.setTime(startOfMonth(cal.getTime()));
		return cal;
	}
	
	public static String getDateStr(Date dt) {
		DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
		return f.format(dt);
	}
	
	// Translit varieties
	
	private static final String TREN = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static final String TRRU = "АВС-Е--Н--К-М-ОР-Я-Т---ХУ-";
	
	public static void getAllTranslitVariaties(String a, int k, List<String> psL) {
		int l = a.length();
		if (k+1 < l) getAllTranslitVariaties(a, k+1, psL);
		
		char t = a.charAt(k);
		int i = TRRU.indexOf(t);
		if (i > -1 && TREN.charAt(i) != '-') {
			t = TREN.charAt(i);
			a = a.substring(0, k) + t + a.substring(k+1, l);
			psL.add(a);
			if (k+1 < l) getAllTranslitVariaties(a, k+1, psL);
		}
		else {
			i = TREN.indexOf(t);
			if (i > -1 && TRRU.charAt(i) != '-') {
				t = TRRU.charAt(i);
				a = a.substring(0, k) + t + a.substring(k+1, l);
				psL.add(a);
				if (k+1 < l) getAllTranslitVariaties(a, k+1, psL);
			}
		}
	}
}
