package com.cordialsr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/*
 * Cordial ERP System
 * Author: Stalbek Raimberdiyev
 * stalbekr@gmail.com * 
 * Copyright 2017
 */

// @Configuration
// @ComponentScan
// @EnableJpaRepositories
// @Import(RepositoryRestMvcConfiguration.class)
// @EnableAutoConfiguration
// @EnableWebMvc
@SpringBootApplication
public class SmApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	       return application.sources(SmApplication.class);
	}
	
	public static void main(String[] args) {
		SpringApplication.run(SmApplication.class, args);
//		ApplicationContext ctx = SpringApplication.run(SmApplication.class, args);
//		
//		DispatcherServlet dispatcherServlet = (DispatcherServlet) ctx.getBean("dispatcherServlet");
//        dispatcherServlet.setThrowExceptionIfNoHandlerFound(true);
	}
}
