package com.cordialsr.service;

import java.util.Date;
import java.util.List;

import com.cordialsr.domain.PriceList;

public interface PriceListService {
	PriceList saveNewPriceList(PriceList newPriceList) throws Exception;

	PriceList updatePriceList(PriceList pList) throws Exception;
	
	List<PriceList> getAllByExample(PriceList pl) throws Exception;
	
	
	List<PriceList> getPriceByScopePriority(Integer cid, 
			Integer bid, Integer ctype, Boolean isRent, Boolean isYur, Date dt) throws Exception;

}
