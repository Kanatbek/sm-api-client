package com.cordialsr.service;

import com.cordialsr.domain.PartyEducation;

public interface PartyEducationService {
		PartyEducation saveNewEducation(PartyEducation newEducation) throws Exception;
}
