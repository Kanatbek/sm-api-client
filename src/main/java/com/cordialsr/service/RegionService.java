package com.cordialsr.service;

import com.cordialsr.domain.Region;

public interface RegionService {
	Region saveRegionChanges(Region region) throws Exception;
	Region saveNewRegion(Region region) throws Exception;
}
