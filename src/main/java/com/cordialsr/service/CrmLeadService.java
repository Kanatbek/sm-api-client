package com.cordialsr.service;

import com.cordialsr.domain.CrmLead;

public interface CrmLeadService {
		CrmLead saveLead (CrmLead ch) throws Exception;
		CrmLead[] saveLeads (CrmLead[] leads) throws Exception;
		CrmLead updateLead(CrmLead lead) throws Exception;
}
