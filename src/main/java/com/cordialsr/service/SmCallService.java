package com.cordialsr.service;

import com.cordialsr.domain.SmCall;
import com.cordialsr.domain.SmEnquiry;

public interface SmCallService {
	SmCall newSmCall(SmCall newCall, Integer verify, Integer level) throws Exception;
	void newOutgoingCall(SmEnquiry newEnquiry) throws Exception;
}
