package com.cordialsr.service;

import java.util.List;

import com.cordialsr.domain.FinCashflowStatement;
import com.cordialsr.domain.FinGlAccount;
import com.cordialsr.domain.FinOperation;

public interface FinRefService {
	FinOperation saveFinOper(FinOperation newFo) throws Exception;
	FinCashflowStatement saveCf(FinCashflowStatement newCf) throws Exception;
	List<FinGlAccount> getAllGlAccounts();
}
