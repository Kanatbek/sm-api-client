package com.cordialsr.service;

import java.util.ArrayList;

import com.cordialsr.domain.InvItemSn;
import com.cordialsr.domain.Invoice;

public interface InvItemSnService {

	ArrayList<Invoice> checkInvItemSns(InvItemSn invItemSn) throws Exception;

}
