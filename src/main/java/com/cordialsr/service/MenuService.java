package com.cordialsr.service;

import com.cordialsr.domain.security.Menu;

public interface MenuService {
	Iterable<Menu> getMenuTree() throws Exception;
	Iterable<Menu> getMenuTreeByUserId(Long userId) throws Exception;
	Iterable<Menu> getMenuTreeByUsername(String username) throws Exception;
}
