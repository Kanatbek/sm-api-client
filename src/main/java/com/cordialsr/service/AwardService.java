package com.cordialsr.service;

import java.util.Date;
import java.util.List;

import com.cordialsr.domain.Award;

public interface AwardService {
	Award saveNewAward(Award newAward) throws Exception;
	Award getPremi(Integer cid, Integer did, Integer pos, Integer brid, Integer subcat, Integer invid, Date dt, Integer month, Boolean isRent) throws Exception;
	List<Award> getAllByExample(Award aw) throws Exception;
	Award updateAward(Award aw) throws Exception;

}
