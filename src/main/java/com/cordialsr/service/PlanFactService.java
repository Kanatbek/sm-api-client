package com.cordialsr.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.cordialsr.domain.Party;
import com.cordialsr.domain.PlanFact;
import com.cordialsr.domain.SmService;

public interface PlanFactService {
		List<PlanFact> getCollectorPlan(Integer cid, Integer bid, Date dt) throws Exception;
		List<PlanFact> getCollectorPlanFact(Integer cid, Integer bid, Date dt) throws Exception;
		List<PlanFact> getPlanFact(Integer cid, Integer bid, Integer pos, Date dt) throws Exception;
		List<PlanFact> getPlan(Integer cid, Integer bid, Integer pos, Date dt) throws Exception;
		List<PlanFact> getCaremanPlanFact(Integer cid, Integer bid, Long caremanId, Date dt) throws Exception;
		PlanFact getCollectorPlanFact(Long collectorId, Date startDate, Date endDate) throws Exception;
		
		void getPlanCaremanOnMonth(Integer cid,Integer bid, Date sdate, Party party) throws Exception;
		
		PlanFact savePlanFact(PlanFact pf) throws Exception;
		List<PlanFact> saveAllPlanFacts(List<PlanFact> pfL) throws Exception;
		BigDecimal getPlanFactCaremansByService(SmService ser) throws Exception;
		
}
