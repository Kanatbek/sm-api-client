package com.cordialsr.service;

import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.OrderHeader;

public interface OrderService {
	
	Boolean saveNewOrders(OrderHeader ordHead) throws Exception;

	void deleateFromOrder(Invoice invoice) throws Exception; 
	
}
