package com.cordialsr.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.cordialsr.domain.Contract;
import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.FinEntry;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.KassaBank;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.Payroll;
import com.cordialsr.domain.StockIn;
import com.cordialsr.domain.StockOut;
import com.cordialsr.dto.ContractPaymentDto;

public interface FinDocService {
	
	FinDoc saveFinDoc(FinDoc newFinDoc) throws Exception;
	FinDoc saveNewFinDoc(FinDoc newFinDoc) throws Exception;
	FinDoc saveNewKassaRashodFindoc(FinDoc newFinDoc) throws Exception;
	FinDoc saveNewExchangeCurrencyFindoc(FinDoc newFinDoc) throws Exception;
	FinDoc saveNewCashIncomeFindoc(FinDoc newFinDoc) throws Exception;
	FinDoc saveNewTransferMoneyFindoc(FinDoc newFinDoc) throws Exception;
	FinDoc getOneFinDoc(Long id);
	FinEntry getOneFinEntry(Long id);
	FinDoc updateFinDoc(FinDoc changedFd, FinDoc newFd) throws Exception;
	String saveNewStockIncomeFinDoc(FinDoc newFinDoc) throws Exception;
	String saveNewContractFinDoc(Contract contract, Invoice invoice) throws Exception;
	ContractPaymentDto doPostContractPayment(ContractPaymentDto cpd) throws Exception;
	ContractPaymentDto[] doPostContractPayments(ContractPaymentDto[] cpDto) throws Exception;
//	ContractPaymentDto[] doPostContractPaymentsOld(ContractPaymentDto[] cpDto) throws Exception;
	String doPostPayroll(Payroll p, String refkeyMain) throws Exception;
	String doPostPayrollToTransit(Payroll p, String refkeyMain) throws Exception;
	String doPostPayrollFromTransit(Payroll p) throws Exception;
	FinDoc writeOffContractGoodsFromInvoice(Invoice invoice, List<StockOut> newStockOutList, String trCode, Long userId) throws Exception;
	FinDoc returnContractGoodsFromInvoice(Invoice invoice, List<StockIn> newStockInList, String trCode, Long userId) throws Exception;
	String returnServiceGoodsFromInvoice(Invoice invoice, List<StockOut> newStockOutList, String trCode, Long userId) throws Exception;
	FinDoc storno(FinDoc fd, String info) throws Exception;
	boolean cancelContractFinDoc(Contract contract, String info, Date stornoDate, String trCode, Long userId, boolean stornoAll) throws Exception;
	String reissueContractFinDoc(Contract contract) throws Exception;
	void payPremiToStaff(Contract con, KassaBank kb, BigDecimal amount, Party party) throws Exception;
	
}
