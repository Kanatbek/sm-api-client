package com.cordialsr.service;

import java.math.BigDecimal;
import java.util.Date;

import com.cordialsr.domain.SmCall;
import com.cordialsr.domain.SmService;
import com.cordialsr.domain.SmVisit;

public interface SmServService {

	SmService saveNewService(SmService service) throws Exception;
	String convertToService(String contractNumber, Date sdate, Date sTime, Date iTime, Integer fno1
								,Integer fno2, Integer fno3, Integer fno4
								,Integer fno5, String type, String info, String username
								,BigDecimal longitude, BigDecimal latitude) throws Exception;
	SmService updateNewService(Long smId, SmService service) throws Exception;
	SmService cancelNewService(SmService service)throws Exception;
	void changeVerify(SmCall newCall, Integer level) throws Exception;
	void assignServiceNumbers() throws Exception;
}
