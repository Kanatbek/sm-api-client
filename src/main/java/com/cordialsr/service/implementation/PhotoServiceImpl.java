package com.cordialsr.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.PhotoDao;
import com.cordialsr.domain.Photo;
import com.cordialsr.domain.validator.PhotoValidator;
import com.cordialsr.service.PhotoService;

@Service
@Transactional
public class PhotoServiceImpl implements PhotoService {

	@Autowired
	PhotoDao conDao;
	
	@Override
	public Photo savePhoto(Photo newPhoto) throws Exception {
		try {
			PhotoValidator.validateBasic(newPhoto);

			conDao.save(newPhoto);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return newPhoto;
	}

	

	
	
	
}
