package com.cordialsr.service.implementation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.BranchDao;
import com.cordialsr.Dao.CompanyDao;
import com.cordialsr.Dao.ContractAwardScheduleDao;
import com.cordialsr.Dao.ContractDao;
import com.cordialsr.Dao.ContractHistoryDao;
import com.cordialsr.Dao.ContractPaymentScheduleDao;
import com.cordialsr.Dao.EmployeeDao;
import com.cordialsr.Dao.FinDocDao;
import com.cordialsr.Dao.FinEntryDao;
import com.cordialsr.Dao.InvoiceDao;
import com.cordialsr.Dao.KassaBankDao;
import com.cordialsr.Dao.PartyDao;
import com.cordialsr.Dao.PayrollDao;
import com.cordialsr.Dao.PlanFactDao;
import com.cordialsr.domain.Address;
import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractAwardSchedule;
import com.cordialsr.domain.ContractHistory;
import com.cordialsr.domain.ContractItem;
import com.cordialsr.domain.ContractPaymentSchedule;
import com.cordialsr.domain.ContractStatus;
import com.cordialsr.domain.ContractStornoRequest;
import com.cordialsr.domain.CrmLead;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.FinEntry;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.KassaBank;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.PaymentStatus;
import com.cordialsr.domain.Payroll;
import com.cordialsr.domain.PhoneNumber;
import com.cordialsr.domain.Position;
import com.cordialsr.domain.ServiceCategory;
import com.cordialsr.domain.nogenerator.ContractNoGenerator;
import com.cordialsr.domain.validator.ContractEditValidator;
import com.cordialsr.domain.validator.ContractValidator;
import com.cordialsr.dto.ContractPaymentDto;
import com.cordialsr.dto.ContractPaymentListDto;
import com.cordialsr.dto.Money;
import com.cordialsr.dto.mini.ContractMiniDto;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.ContractPaymentService;
import com.cordialsr.service.ContractService;
import com.cordialsr.service.FinDocService;
import com.cordialsr.service.InvoiceService;
import com.cordialsr.service.PayrollService;
import com.cordialsr.service.SmFilterChangeService;
import com.cordialsr.service.StockService;

@Service
@Transactional(rollbackFor = Exception.class)
public class ContractServiceImpl implements ContractService {

	@Autowired
	ContractDao conDao;
	
	@Autowired
	ContractPaymentScheduleDao cpsDao;
	
	@Autowired
	FinDocDao fdDao;

	@Autowired
	FinEntryDao feDao;

	@Autowired
	KassaBankDao kbDao;

	@Autowired
	CompanyDao companyDao;
	
	@Autowired
	PlanFactDao pfDao;
	
	@Autowired
	BranchDao branchDao;
	
	@Autowired
	EmployeeDao emplDao;
	
	@Autowired
	PartyDao partyDao;
	
	@Autowired
	InvoiceService invoiceService;

	@Autowired
	StockService stockService;
	
	@Autowired
	FinDocService findocService;
	
	@Autowired
	InvoiceDao invoiceDao;
	
	@Autowired
	SmFilterChangeService smFilterChange;
	
	@Autowired
	PayrollService payrollService;
	
	@Autowired
	ContractPaymentService cpService;
	
	@Autowired
	ContractHistoryDao conHistoryDao;
	
	@Autowired
	ContractAwardScheduleDao cawScheduleDao;
	
	@Autowired
	PayrollDao payrollDao;
	
	@Override
	public Contract saveNewContract(Contract newContract) throws Exception {
		try {
			
			ContractValidator.validateBasic(newContract);
			String conNo = ContractNoGenerator.generateContractNo(newContract);
			newContract.setContractNumber(conNo);
			newContract.setRegisteredDate(Calendar.getInstance());
			conDao.save(newContract);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return newContract;
	}
	
	
	//*****************************************************************************************
	
	@Override
	public Contract getOneContract(Long id) {
		return conDao.findOne(id);
	}
	
	
	//*****************************************************************************************
	
	
	@Override
	public Contract saveContract(Contract newContract) throws Exception {	
		try {
			ContractValidator.validateBasic(newContract);
			ContractValidator.validatePriceAndPaymentSchedule(newContract);
			
			for (ContractItem at: newContract.getContractItems()) {
				at.setContract(newContract);
			}
			Invoice invoice = invoiceService.saveNewInvoiceFromContract(newContract);
			
			String refkey = findocService.saveNewContractFinDoc(newContract, invoice);

			for (ContractPaymentSchedule cps: newContract.getPaymentSchedules()) {
				cps.setContract(newContract);
			}
			
			newContract.setContractStatus(new ContractStatus(ContractStatus.STATUS_NEW));
			newContract.setPaymentStatus(new PaymentStatus(PaymentStatus.PSTAT_REGULAR));
			newContract.setServiceCategory(new ServiceCategory(ServiceCategory.CAT_GREEN));
			conDao.save(newContract);
			String conNo = ContractNoGenerator.generateContractNo(newContract);
			newContract.setContractNumber(conNo);
			newContract.setRegisteredDate(Calendar.getInstance());
			
			// First Payment to Branch cashbox	
			ImmutablePair<KassaBank, BigDecimal> res = cpService.postFirstPaymentFromNewContract(newContract);
			
			invoice.setDocId(newContract.getId());
			invoice.setDocno(newContract.getContractNumber());
			invoiceDao.save(invoice);
			
			newContract.setRefkey(refkey);
			newContract = conDao.save(newContract);
			
			payrollService.payrollPremisFromNewContract(newContract);
			
			if (newContract.getIsRent() && res != null 
					&& res.getLeft() != null && !GeneralUtil.isEmptyBigDecimal(res.getRight())
					&& newContract.getDealer() != null) {
				// PayrollFirstPremiForDealer
				payrollService.payrollFirstPremiForDealer(newContract);
				
				// Pay First Premi to Dealer from CashBox
				Employee dealer = emplDao.findOne(newContract.getDealer().getId());
				findocService.payPremiToStaff(newContract, res.getLeft(), res.getRight(), dealer.getParty());
			}
			
			smFilterChange.newGraph(newContract);
			
			return newContract;
		} catch (Exception e) {
			throw e;
		}
	}
	

	//*****************************************************************************************
	
	@Override
	public List<ContractPaymentDto> getAllReceivables(Integer cid, Integer bid, Date dte,
			Long conId, Long cusId, Long collectorId, Long dealerId) throws Exception {
		try {
			List<ContractPaymentDto> cpL = new ArrayList<>();
			dte = GeneralUtil.endOfMonth(dte);
//			List<Map<String, Object>> resL = cpsDao.getAllReceivablesTotal(cid, bid, dte);
//			
//			for (Map<String, Object> res : resL) {
//				ContractPaymentDto cp = new ContractPaymentDto();
//				Contract con = conDao.findOne(new Long(String.valueOf(res.get("conId"))));
//				cp.setContract(con);
//				cp.setBranch(con.getBranch());
//				cp.setPaymentDue(new BigDecimal(String.valueOf(res.get("summ"))));
//				cpL.add(cp);
//			}
			List<Contract> conL = new ArrayList<>(); 
			if (!GeneralUtil.isEmptyLong(conId)) {
				Contract con = getOneContract(conId);
				conL.add(con);
			} else if (!GeneralUtil.isEmptyLong(cusId)) {
				conL = conDao.getReceivablesForDateByCustomer(cid, bid, cusId, dte);
			} else if (!GeneralUtil.isEmptyLong(collectorId)) {
				conL = conDao.getReceivablesForDateByCollector(cid, bid, collectorId, dte);
			} else if (!GeneralUtil.isEmptyLong(dealerId)) {
				conL = conDao.getReceivablesForDateByDealer(cid, bid, dealerId, dte);
			} else {
				conL = conDao.getReceivablesForDateByServiceBranch(cid, bid, dte);
			}
			
			for (Contract con : conL) {
				BigDecimal paymentDue = new BigDecimal(0);
				for (ContractPaymentSchedule ps : con.getPaymentSchedules()) {
					BigDecimal remain = ps.getSumm().subtract(ps.getPaid());
					if (ps.getPaymentDate().getTime() <= dte.getTime()
							&& remain.compareTo(BigDecimal.ZERO) > 0) {
						paymentDue = paymentDue.add(remain);
					}
				}
				
				if (paymentDue.compareTo(BigDecimal.ZERO) > 0) {
					ContractPaymentDto cp = new ContractPaymentDto();
					cp.setContract(con);
					cp.setBranch(con.getBranch());
					cp.setPaymentDue(paymentDue);
					cpL.add(cp);
				}
			}
			
			return cpL;
		} catch(Exception e) {
			throw e;
		}
	}

	//*****************************************************************************************
	
	@Override
	public ContractPaymentSchedule paymentPosted(ContractPaymentSchedule cps, BigDecimal payment) throws Exception {
		try {
			cps = cpsDao.findOne(cps.getId());
			cps.setPaid(cps.getPaid().add(payment));
			Contract con = conDao.findOne(cps.getContract().getId());
			updateContractPaymentStatus(con, payment);
			return cps;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// ********************************************************************************************************************************************

	@Override
	public Contract cancelPayment(Contract con, BigDecimal payment, Integer order) throws Exception {
		try {
			BigDecimal amount = payment.multiply(new BigDecimal(-1));
			BigDecimal due = payment;
			
			
			// update ContractPayment Schedule
			for (ContractPaymentSchedule ps: con.getPaymentSchedules()) {
				
			}
			
			updateContractPaymentStatus(con, amount);
			return con;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// ********************************************************************************************************************************************

	private void updateContractPaymentStatus(Contract con, BigDecimal payment) throws Exception {
		try {
			con.setPaid(con.getPaid().add(payment));
			if (con.getPaid().compareTo(con.getSumm()) > 0)
				throw new Exception("Payment sum exceeds the Cost sum of contract.");
			else if (con.getPaid().compareTo(con.getSumm()) == 0) {
				con.setPaymentStatus(new PaymentStatus(PaymentStatus.PSTAT_PAID));
				con.setContractStatus(new ContractStatus(ContractStatus.STATUS_COMPLETE));
			} else {
				// con.setPaymentStatus(new PaymentStatus(PaymentStatus.PSTAT_REGULAR));
				con.setContractStatus(new ContractStatus(ContractStatus.STATUS_ACTIVE));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//*****************************************************************************************
	
	
	@Override
	public List<ContractPaymentListDto> getCollectorPaymentGraph(Integer cid, Integer bid, Long sid, Date dte, Boolean rent, Boolean sale)
			throws Exception {
		try {
			ModelMapper modelMapper = new ModelMapper();
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			
			Date ds = GeneralUtil.startOfMonth(dte);
			Date de = GeneralUtil.endOfMonth(dte);
			List<Contract> conL = new ArrayList<>();
			if (GeneralUtil.isEmptyLong(sid)) conL = conDao.getReceivablesForDateByServiceBranch(cid, bid, de);
			else conL = conDao.getReceivablesForDateByCollector(cid, bid, sid, de);
			
			List<ContractPaymentListDto> cplDtoL = new ArrayList<>();
			for (Contract con: conL) {
				if ((rent != null && rent && con.getIsRent())
						|| (sale != null && sale && !con.getIsRent())
						|| ((rent == null && sale == null))
					) {
					ContractPaymentListDto cpDto = new ContractPaymentListDto();
					cpDto.setContract(new ContractMiniDto());
					modelMapper.map(con, cpDto.getContract());
					cpDto.setCost(con.getCost());
					cpDto.setPaid(con.getPaid());
					cpDto.setRemain(con.getSumm().subtract(con.getPaid()));
					cpDto.getContract().setDateIssue(con.getContractItems().get(0).getWriteoffDate());
					
					BigDecimal overdue = new BigDecimal(0);
					BigDecimal paymentDue = new BigDecimal(0);
					ContractPaymentSchedule prev = null;
					
					for (ContractPaymentSchedule cps: con.getPaymentSchedules()) {
						if (cpDto.getCurrentDate() != null 
								&& !GeneralUtil.isEmptyBigDecimal(cpDto.getCurrentSumm())) {
							cpDto.setNextDate(cps.getPaymentDate());
							cpDto.setNextSumm(cps.getSumm().subtract(cps.getPaid()));
							cpDto.setNextOrder(cps.getPaymentOrder());
							break;
						}
						if (ds.compareTo(cps.getPaymentDate()) > 0) {
							overdue = overdue.add(cps.getSumm().subtract(cps.getPaid()));
						}
						if ((ds.compareTo(cps.getPaymentDate()) <= 0) && (de.compareTo(cps.getPaymentDate()) >= 0)) {
							paymentDue = paymentDue.add(cps.getSumm().subtract(cps.getPaid()));
							cpDto.setCurrentDate(cps.getPaymentDate());
							cpDto.setCurrentSumm(paymentDue);
							cpDto.setCurrentOrder(cps.getPaymentOrder());
							if (prev != null) {
								cpDto.setPreviewsDate(prev.getPaymentDate());
								cpDto.setPreviewsSumm(prev.getSumm().subtract(prev.getPaid()));
								cpDto.setPreviewsOrder(cps.getPaymentOrder());
							}
						}
						prev = cps.clone();
					}
					paymentDue = paymentDue.add(overdue);
					cpDto.setOverdue(overdue);
					cpDto.setPaymentDue(paymentDue);
					cplDtoL.add(cpDto);
				}
			}
			
			return cplDtoL;
		} catch (Exception e) {
			throw e;
		}
	}
	
	//*****************************************************************************************
	
	@Override
	public List<ContractPaymentListDto> getCollectorsList(Integer cid, Integer bid, Long sid, Date dte, Boolean rent, Boolean sale)
			throws Exception {
		try {
			ModelMapper modelMapper = new ModelMapper();
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			
			Date ds = GeneralUtil.startOfMonth(dte);
			Date de = GeneralUtil.endOfMonth(dte);
			List<Contract> conL = new ArrayList<>();
			if (GeneralUtil.isEmptyLong(sid)) conL = conDao.getReceivablesForDateByServiceBranch(cid, bid, de);
			else conL = conDao.getReceivablesForDateByCollector(cid, bid, sid, de);
			
			List<ContractPaymentListDto> cplDtoL = new ArrayList<>();
			for (Contract con: conL) {
				if ((rent != null && rent && con.getIsRent())
						|| (sale != null && sale && !con.getIsRent())
						|| ((rent == null && sale == null))
					) {
					ContractPaymentListDto cpDto = new ContractPaymentListDto();
//					modelMapper.map(con, cpDto.getContract().getId());	
					if (con.getCustomer() != null) {
						cpDto.setCustomerFIO(con.getCustomer().getFullFIO());
					}
					if (con.getCurrency() != null) {
						cpDto.setCurrency(con.getCurrency().getCurrency());
					}
					cpDto.setContractId(con.getId());
					cpDto.setCost(con.getCost());
					cpDto.setPaid(con.getPaid());
					cpDto.setRemain(con.getCost().subtract(con.getPaid()));
//					cpDto.getContract().setDateIssue(con.getContractItems().get(0).getWriteoffDate());
					
					BigDecimal overdue = new BigDecimal(0);
					BigDecimal paymentDue = new BigDecimal(0);
					ContractPaymentSchedule prev = null;
					
					for (ContractPaymentSchedule cps: con.getPaymentSchedules()) {
						if (cpDto.getCurrentDate() != null 
								&& !GeneralUtil.isEmptyBigDecimal(cpDto.getCurrentSumm())) {
							cpDto.setNextDate(cps.getPaymentDate());
							cpDto.setNextSumm(cps.getSumm().subtract(cps.getPaid()));
							cpDto.setNextOrder(cps.getPaymentOrder());
							break;
						}
						if (ds.compareTo(cps.getPaymentDate()) > 0) {
							overdue = overdue.add(cps.getSumm().subtract(cps.getPaid()));
						}
						if ((ds.compareTo(cps.getPaymentDate()) <= 0) && (de.compareTo(cps.getPaymentDate()) >= 0)) {
							paymentDue = paymentDue.add(cps.getSumm().subtract(cps.getPaid()));
							cpDto.setCurrentDate(cps.getPaymentDate());
							cpDto.setCurrentSumm(paymentDue);
							cpDto.setCurrentOrder(cps.getPaymentOrder());
							if (prev != null) {
								cpDto.setPreviewsDate(prev.getPaymentDate());
								cpDto.setPreviewsSumm(prev.getSumm().subtract(prev.getPaid()));
								cpDto.setPreviewsOrder(cps.getPaymentOrder());
							}
						}
						prev = cps.clone();
					}
					paymentDue = paymentDue.add(overdue);
					cpDto.setOverdue(overdue);
					cpDto.setPaymentDue(paymentDue);
					cplDtoL.add(cpDto);
				}
			}
			
			return cplDtoL;
		} catch (Exception e) {
			throw e;
		}
	}
	
	
	//*****************************************************************************************
	
	@Override
	public Contract updateContract(Contract contract) throws Exception {
		try {
			ContractEditValidator.validateBasic(contract);
//			ContractValidator.validatePriceAndPaymentSchedule(contract);
			
			if (contract.getId() != null) {
				Contract oldContract = conDao.findOne(contract.getId());

				if (contract.getContractHistory() != null) {
					for (int i = oldContract.getContractHistory().size(); i < contract.getContractHistory().size(); i++) {
						String d = contract.getContractHistory().get(i).getTitle();
						if (d.equals("SN")) {
							if (contract.getContractItems() != null && contract.getContractItems().get(0) != null) {
					 			if (contract.getContractItems().get(0).getSerialNumber() != null) {
					 				if (stockService.changeStocksInContract(contract.getContractHistory().get(i).getOldValue())) {
					 						invoiceService.stornoInvoiceByContract(contract);
					 						invoiceService.saveNewInvoiceFromContract(contract);
					 				}
					 			}
					 		}
						}
					}
				}	
			}

			for (ContractPaymentSchedule cps : contract.getPaymentSchedules()) {
				cps.setContract(contract);
			}
			contract.setUpdatedDate(Calendar.getInstance());
			return conDao.save(contract);
		} catch (CloneNotSupportedException e) {
			throw new Exception(e.getMessage());
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	//*****************************************************************************************
	
	@Override
	public Contract reissueContract(Contract contract) throws Exception {
		try {
//			ContractValidator.validatePriceAndPaymentSchedule(contract);
			
			Contract con = updateContract(contract);
			
			// call reissue method for FinDoc
			String refkey = findocService.reissueContractFinDoc(con);
			
			con.setRefkey(refkey);
			conDao.save(con);
			
			return con;
		} catch (CloneNotSupportedException e) {
			throw new Exception(e.getMessage());
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	//*****************************************************************************************

	@Override
	public Money getCustomerPaymentDueByIIN(String iinBin, Calendar dateEnd, String curr, Boolean rental, Boolean sales) throws Exception {
		try {
			List<Contract> conL = conDao.getAllContractsByIinBin(iinBin);
			Money paymentDue = getPaymentDueFomConL(conL, dateEnd, curr, rental, sales);
			return paymentDue;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// ********************************************************************************************************************************************

	@Override
	public Money getCustomerPaymentDueByID(Long customerId, Calendar dateEnd, String curr, Boolean rental, Boolean sales) throws Exception {
		try {
			List<Contract> conL = conDao.getAllConByCustomerId(customerId);
			Money paymentDue = getPaymentDueFomConL(conL, dateEnd, curr, rental, sales);
			return paymentDue;
		} catch (Exception e) {
			throw e;
		}
	}

	// ********************************************************************************************************************************************

	@Override
	public Money getCustomerContractSummByID(Long customerId, Calendar dateEnd, String curr, Boolean rental, Boolean sales) throws Exception {
		try {
			List<Contract> conL = conDao.getAllConByCustomerId(customerId);
			Money contractSumm = getContractSummFromConL(conL, dateEnd, curr, rental, sales);
			return contractSumm;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// ********************************************************************************************************************************************

	private Money getPaymentDueFomConL(List<Contract> conL, Calendar dateEnd, String curr, Boolean rental, Boolean sales) throws Exception {
		try {
			if (conL == null || conL.size() == 0) throw new Exception("Договора не найдены!");
			
			BigDecimal summ = new BigDecimal(0);
			Calendar pd = Calendar.getInstance();
			
			if (GeneralUtil.isEmptyString(curr)) throw new Exception("Currency is empty.");
			
			for (Contract con: conL) {
				if (con.getStorno() == null || con.getStorno() == false) {
					if (con.getIsRent() && rental || !con.getIsRent() && sales) {
						if (con.getCurrency().getCurrency().equalsIgnoreCase(curr)) {
							for (ContractPaymentSchedule ps: con.getPaymentSchedules() ) {
								pd.setTime(ps.getPaymentDate());
								if ((pd.getTimeInMillis() <= dateEnd.getTimeInMillis()) || 
										(pd.get(Calendar.MONTH) == dateEnd.get(Calendar.MONTH) 
											&& (pd.get(Calendar.YEAR) == dateEnd.get(Calendar.YEAR)))) {
									if (ps.getSumm().compareTo(ps.getPaid()) > 0) {
										summ = summ.add(ps.getSumm().subtract(ps.getPaid()));
									}
								}
							}
						}
					}		
				}
			}
			Money paymentDue = new Money(summ, curr);
			return paymentDue;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// ********************************************************************************************************************************************

		private Money getContractSummFromConL(List<Contract> conL, Calendar dateEnd, String curr, Boolean rental, Boolean sales) throws Exception {
			try {
				if (conL == null || conL.size() == 0) throw new Exception("Договора не найдены!");
				
				BigDecimal summ = new BigDecimal(0);
				Calendar pd = Calendar.getInstance();
				
				if (GeneralUtil.isEmptyString(curr)) throw new Exception("Currency is empty.");
				
				for (Contract con: conL) {
					if (con.getStorno() == null || con.getStorno() == false) {
						if (con.getIsRent() && rental || !con.getIsRent() && sales) {
							if (con.getCurrency().getCurrency().equalsIgnoreCase(curr)) {
//								for (ContractPaymentSchedule ps: con.getPaymentSchedules() ) {
//									pd.setTime(ps.getPaymentDate());
//									if ((pd.getTimeInMillis() <= dateEnd.getTimeInMillis()) || 
//											(pd.get(Calendar.MONTH) == dateEnd.get(Calendar.MONTH) 
//												&& (pd.get(Calendar.YEAR) == dateEnd.get(Calendar.YEAR)))) {
//										if (ps.getSumm().compareTo(ps.getPaid()) > 0) {
//											summ = summ.add(ps.getSumm().subtract(ps.getPaid()));
//										}
//									}
//								}
								summ = summ.add(con.getSumm());
							}
						}		
					}
				}
				Money paymentDue = new Money(summ, curr);
				return paymentDue;
			} catch (Exception e) {
				throw e;
			}
		}
	
	// ******************************************************************************************
	
	// Check for Contract overdue sum existence (1st option)
	public boolean conOdSumExist(List<Map<String, Object>> conOdSumL, Long conIdIn) {
		for (Map<String, Object> cos : conOdSumL) {
			Long conId = new Long(String.valueOf(cos.get("conId")));
			if (conId.equals(conIdIn)) {
				BigDecimal summ = new BigDecimal(String.valueOf(cos.get("summ")));
				if (!GeneralUtil.isEmptyBigDecimal(summ)) return true;
				else return false;
			}
		}
		return true;
	}

	// ******************************************************************************************

	@Override
	public Page<Contract> getContractList(Integer cid, Integer bid, Boolean storno, Boolean servbr, Boolean isRent, Long empl, Date dts,
			Date dte, Boolean caremanSales, String filter, Pageable pageable) throws Exception {
		try {
			Contract conEx = new Contract();
			if (!GeneralUtil.isEmptyInteger(cid)) {
				conEx.setCompany(new Company(cid));
				if (!GeneralUtil.isEmptyInteger(bid)) {
					
					if (servbr != null && servbr) {
						conEx.setServiceBranch(new Branch(bid));
					} else conEx.setBranch(new Branch(bid));
					
					if (storno != null) {
						conEx.setStorno(storno);
					}

					if (isRent != null) {
						conEx.setIsRent(isRent);
					}
					
					if (caremanSales != null) {
						conEx.setCaremanSales(caremanSales);
					}
					
					if (conEx.getManager() != null) {
						Employee manager = conEx.getManager();
						conEx.setManager(manager);
					}
					
					Example<Contract> example = Example.of(conEx); 
					List<Contract> conL = conDao.findAll(example);
					Collections.reverse(conL); 
					
					
					if (empl != null && empl > 0) {
						Employee employee = emplDao.findOne(empl);
						
						if (employee != null) {
							if (employee.getPosition().getId() == Position.POS_DEALER) {
								List<Contract> res = new ArrayList<>();
								for (Contract con: conL) {
									if (con.getDealer() != null) {
										if (con.getDealer().getId() == employee.getId()) {
											res.add(con);
										}
									}
								}
								conL = new ArrayList<>(res);
							}
							else if (employee.getPosition().getId() == Position.POS_CAREMAN) {
								List<Contract> res = new ArrayList<>();
								for (Contract con: conL) {
									if (con.getCareman() != null) {
										if (con.getCareman().getId() == employee.getId()) {
											res.add(con);
										}
									}
								}
								conL = new ArrayList<>(res);
							}
							else if (employee.getPosition().getId() == Position.POS_COORDINATOR) {
								List<Contract> res = new ArrayList<>();
								for (Contract con: conL) {
									if (con.getCoordinator() != null) {
										if (con.getCoordinator().getId() == employee.getId()) {
											res.add(con);
										}
									}
								}
								conL = new ArrayList<>(res);
							}
							else if (employee.getPosition().getId() == Position.POS_DEMOSEC) {
								List<Contract> res = new ArrayList<>();
								for (Contract con: conL) {
									if (con.getDemosec() != null) {
										if (con.getDemosec().getId() == employee.getId()) {
											res.add(con);
										}
									}
								}
								conL = new ArrayList<>(res);
							}
							else if (employee.getPosition().getId() == Position.POS_DIRECTOR) {
								List<Contract> res = new ArrayList<>();
								for (Contract con: conL) {
									if (con.getDirector() != null) {
										if (con.getDirector().getId() == employee.getId()) {
											res.add(con);
										}
									}
								}
								conL = new ArrayList<>(res);
							}
							else if (employee.getPosition().getId() == Position.POS_FITTER) {
								List<Contract> res = new ArrayList<>();
								for (Contract con: conL) {
									if (con.getFitter() != null) {
										if (con.getFitter().getId() == employee.getId()) {
											res.add(con);
										}
									}
								}
								conL = new ArrayList<>(res);
							}
							else if (employee.getPosition().getId() == Position.POS_MANAGER) {
								List<Contract> res = new ArrayList<>();
								for (Contract con: conL) {
									if (con.getManager() != null) {
										if (con.getManager().getId() == employee.getId()) {
											res.add(con);
										}
									}
								}
								conL = new ArrayList<>(res);
							}
							else if (employee.getPosition().getId() == Position.POS_COLLECTOR) {
								List<Contract> res = new ArrayList<>();
								for (Contract con: conL) {
									if (con.getCollector() != null) {
										if (con.getCollector().getId() ==  employee.getId()) {
											res.add(con);
										}
									}
								}
								conL = new ArrayList<>(res);
							}

						}
					}
					
					if (dts != null) dts = GeneralUtil.startOfDay(dts);
					if (dte != null) dte = GeneralUtil.endOfDay(dte);
					
					if (dts != null) {
						List<Contract> res = new ArrayList<>();
						for (Contract con : conL) {
							if (con.getDateSigned().getTime() >= dts.getTime()) {
								res.add(con);
							}
						}
						conL = new ArrayList<>(res);
					}
					if (dte != null) {
						List<Contract> res = new ArrayList<>();
						for (Contract con : conL) {
							if (con.getDateSigned().getTime() <= dte.getTime()) {
								res.add(con);
							}
						}
						conL = new ArrayList<>(res);
					}
					
//					if (!GeneralUtil.isEmptyString(filter)) {
//						conL = filterContractList(conL, filter);
//					}
					Integer totalElem = conL.size();
					conL = getOffsetPage(conL, pageable.getPageNumber(), pageable.getPageSize());
					Page<Contract> pages = new PageImpl<Contract>(conL, pageable, totalElem);
					
					return pages;
				} else throw new Exception("Укажите филиал!");
			} else throw new Exception("Укажите компанию!");
		} catch (Exception e) {
			throw e;
		}
	}
	
	private List<Contract> getOffsetPage(List<Contract> conL, Integer page, Integer size) throws Exception {
		try {
			List<Contract> res = new ArrayList<>();
			Integer si = page * size;
			Integer ei = si + size;
			if (ei > conL.size()) ei = conL.size();
			res = conL.subList(si, ei);
			return res;
		} catch (Exception e) {
			throw e;
		}
	}


	@Override
	public Contract findContractByNumber(Integer cid, Integer bid, String cn) throws Exception {
		try {
			Contract contract = conDao.getContractByCnCidBid(cid, bid, cn);
			return contract;
		} catch (Exception e) {
			throw e;
		}
	}

	// ****************************************************_CANCEL_CONTRACT_*********************************************************

	@Autowired
	SmFilterChangeService smfcService;
	
	@Override
	public Contract cancelContract(ContractStornoRequest stornoReq) throws Exception {
		try {
			Contract contract = conDao.findOne(stornoReq.getContract().getId());
			String trCode = "MSCANREQL";
			if (contract != null) stornoReq.setContract(contract);
			else throw new Exception("Договор не найден!");
			
			if (contract.getStorno()) throw new Exception("Договор уже отменен.");
			
			// List<FinDoc> fdL = contract.getFinDocs();
			if (findocService.cancelContractFinDoc(contract, stornoReq.getInfo(), stornoReq.getCloseDate(), trCode, stornoReq.getReqAuthor().getUserid(), false)
					&& cancelContractAws(stornoReq, contract, trCode)) {
				
				smfcService.updateFcByContract(contract.getId(), false);
				
				invoiceService.restoreFromReserved(contract);
				
				contract.setStorno(true);
				contract.setStornoDate(stornoReq.getCloseDate());
				contract.setUpdatedDate(Calendar.getInstance());
				contract.setUpdatedUser(stornoReq.getGrantAuthor());
				conDao.save(contract);
			}
			return contract;
		} catch (Exception e) {
			throw e;
		}
	}
	
	public boolean cancelContractAws(ContractStornoRequest stornoReq, Contract contract, String trCode) throws Exception {
		try {
			Integer monthAge = GeneralUtil.calcAgeInMonth(contract.getDateSigned(), stornoReq.getReqDate().getTime());
			return payrollService.rollbackContractAwards(stornoReq, monthAge, trCode);
		} catch (Exception e) {
			throw e;
		}
	}


	
	
	
	// **************************************************************************************************************************************************************
	
	@Override
	public void updateWriteOffDate(String contractNumber, String sn, Integer inventoryId, Date writeOffDate) throws Exception {
		try {
			Contract contract = conDao.findByContractNumber(contractNumber);
			if (contract == null) {
				throw new Exception("Договор не найден!");
			}
			if (contract.getContractItems().size() > 0) {
				for (ContractItem conItems: contract.getContractItems()) {
					if (inventoryId != null && conItems.getInventory().getId() == inventoryId) {
						conItems.setWriteoffDate(writeOffDate);
					} 
					if (sn != null && conItems.getSerialNumber().equals(sn)) {
						conItems.setWriteoffDate(writeOffDate);
					}
				}
			}
			conDao.save(contract);

		} catch (Exception e) {
			throw e;
		}
		
	}
	
	// **************************************************************************************************************************************************************
	
	@Override
	public Contract[] saveContractsChange(Contract[] contracts, Long careman, Long collector) throws Exception {
		try {
			for (Contract contract: contracts) {	
				Contract mContract = conDao.findcid(contract.getId());
				if (careman != null && careman > 0) {
					Employee mCareman = emplDao.findById(careman);
					mContract.setCareman(mCareman);
				}
				if (collector != null && collector > 0) {
					Employee mCollector = emplDao.findById(collector);
					mContract.setCollector(mCollector);
				}
				conDao.save(mContract);
			}
			return contracts;
		} catch (Exception e) {
			throw e;
		}
	}


	@Override
	public ContractHistory saveContractHistory(ContractHistory contractHistory) throws Exception {
		try {
			if (contractHistory != null) {
				conHistoryDao.save(contractHistory);
			}
			return contractHistory;
		} catch (Exception e) {
			throw e;
		}
	}

	
	//  *********************************************************************************************************************

	@Override
	public Contract changeContractEmployee(ContractHistory contractHistory, Long contractId, Long newPartyId,
			Long newEmployeeId, Long oldEmplId, Integer position) throws Exception {
			
		Contract contract = conDao.findcid(contractId);
		Party newParty = partyDao.findAllByParty(newPartyId);
		Employee newEmployee = emplDao.findById(newEmployeeId);
		Employee oldEmployee = emplDao.findById(oldEmplId);
		
		
		changeContractAwardSchedule(contract, newParty, position);
		changePayroll(contract, oldEmployee, newParty, newEmployee);
		contract = changeStaffFromContract(contract, position, newEmployee);
		if (contractHistory != null) {
			conHistoryDao.save(contractHistory);
		}
		return contract;
	}
	
	private Contract changeStaffFromContract(Contract contract, Integer position, Employee newEmployee) {
		if (position == Position.POS_CAREMAN) {
			contract.setCareman(newEmployee);
		} else if (position == Position.POS_COLLECTOR) {
			contract.setCollector(newEmployee);
		} else if (position == Position.POS_COORDINATOR) {
			contract.setCoordinator(newEmployee);
		} else if (position == Position.POS_DEALER) {
			contract.setDealer(newEmployee);
		} else if (position == Position.POS_DEMOSEC) {
			contract.setDemosec(newEmployee);
		} else if (position == Position.POS_DIRECTOR) {
			contract.setDirector(newEmployee);
		} else if (position == Position.POS_FITTER) {
			contract.setFitter(newEmployee);
		} else if (position == Position.POS_MANAGER) {
			contract.setManager(newEmployee);
		}
		return contract;
	}
	
	private void changeContractAwardSchedule(Contract contract, Party newParty, Integer position) {
		List<ContractAwardSchedule> cawSchedule = cawScheduleDao.getCawByContract(contract.getId(), position);
		if (cawSchedule != null && cawSchedule.size() > 0) {
			for (int i=0;i<cawSchedule.size();i++) {
				ContractAwardSchedule caw = cawSchedule.get(i);
				if (caw != null && caw.getParty() != null) {
					caw.setParty(newParty);
				}
			}	
		}
	}

	private void changePayroll(Contract contract, Employee oldEmpl, Party newParty, Employee newEmployee) {
		if (oldEmpl != null && oldEmpl.getId() > 0) {
			List<Payroll> payroll = payrollDao.getAllByContractEmpl(contract.getId(), oldEmpl.getId());
			if (payroll != null && payroll.size() > 0) {
				for (int i=0;i<payroll.size();i++) {
					Payroll pr = payroll.get(i);
					pr.setParty(newParty);
					pr.setEmployee(newEmployee);
				}
				changeFinDocFinEntry(payroll, newParty);
			}
		}
	}

	private void changeFinDocFinEntry(List<Payroll> payrolls, Party newParty) {
		for (int i=0;i<payrolls.size();i++) {
			Payroll pr = payrolls.get(i);
			if (pr.getRefkey() != null) {
				FinDoc fd = fdDao.findByRefkey(pr.getRefkey());
				if (fd != null) {
					fd.setParty(newParty);
					changeFinEntry(fd, newParty);
				}
			}
		}
	}
	
	private void changeFinEntry(FinDoc fd, Party newParty) {
		List<FinEntry> finEntry = feDao.allByFinDoc(fd.getId());
		if (finEntry != null && finEntry.size() > 0) {
			for (int i=0;i<finEntry.size();i++) {
				FinEntry fe = finEntry.get(i);
				fe.setParty(newParty);
			}
		}
	}


	@Override
	public List<Contract> getContractByCustomerId(Long customerId) throws Exception {
		List<Contract> conList = new ArrayList<>();
		
		
		return conList;
	}
	
	
	
	
	

}