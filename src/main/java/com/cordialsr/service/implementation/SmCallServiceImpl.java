package com.cordialsr.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.SmCallDao;
import com.cordialsr.domain.SmCall;
import com.cordialsr.domain.SmEnquiry;
import com.cordialsr.domain.nogenerator.SmCallNoGenerator;
import com.cordialsr.domain.validator.SmCallValidator;
import com.cordialsr.service.SmCallService;
import com.cordialsr.service.SmServService;

@Service
@Transactional
public class SmCallServiceImpl implements SmCallService {

	@Autowired 
	SmCallDao smCallDao;
	
	@Autowired
	SmServService smSerService;
	
	@Override
	public SmCall newSmCall(SmCall newCall, Integer verify, Integer level) throws Exception {
		
		try {
			
			SmCallValidator.validateNewSmCall(newCall);
			
			if (verify == 1) {
				smSerService.changeVerify(newCall, level);
			}
			
			saveCalls(newCall);
			
		} catch(Exception ex) {
			throw ex;
		}
			
		return newCall;
	}

	@Override
	public void newOutgoingCall(SmEnquiry enq) throws Exception {
		try {
			SmCallValidator.validateNewOutgoingCall(enq);
			
			SmCall newCall = new SmCall();
			newCall.setCompany(enq.getCompany());
			newCall.setBranch(enq.getBranch());
			newCall.setCallDate(enq.getEdate());
			newCall.setCallTime(enq.getEtime());
			newCall.setUser(enq.getOperator());
			newCall.setContract(enq.getContract());
			newCall.setInfo(enq.getInfo());
			newCall.setStatus(SmCall.STATUS_ANSWER);
			newCall.setIsOutgoing(false);
			
			saveCalls(newCall);
			
		} catch(Exception ex) {
			throw ex;
		}
	}
	
	private void saveCalls(SmCall call) throws Exception {
		try {
			smCallDao.save(call);
			if (call.getCallNumber() == null || call.getCallNumber().length() == 0) {
				String enqNo = SmCallNoGenerator.generateSmCallNo(call);
				call.setCallNumber(enqNo);
				smCallDao.save(call);
			}
			
		} catch(Exception ex) {
			throw ex;
		}
	}
}
