package com.cordialsr.service.implementation;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.EmployeeDao;
import com.cordialsr.Dao.FinEntryDao;
import com.cordialsr.Dao.PartyDao;
import com.cordialsr.Dao.PayrollDao;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.FinEntry;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.reducer.PartyReducer;
import com.cordialsr.dto.Money;
import com.cordialsr.dto.StaffBalanceDto;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.PartyService;
import com.cordialsr.service.StaffAccountService;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
@Transactional(readOnly = true)
public class StaffAccountServiceImpl implements StaffAccountService {

	@Autowired
	PartyDao partyDao;
	
	@Autowired
	PayrollDao prlDao;
	
	@Autowired
	EmployeeDao emplDao;
	
	@Autowired
	PartyService partyService;
	
	@Override
	public List<StaffBalanceDto> getStaffBalanceByBranch(Integer cid, Integer bid, Date dt) throws Exception {
		try {
			List<Map<String, Object>> res = prlDao.getAllStaffBalanceByBranch(cid, bid, dt);
			List<StaffBalanceDto> sbL = new ArrayList<>();
			
			for (Map<String, Object> m: res) {
				Long pid = Long.valueOf((m.get("partyId").toString()));
				Party staff = partyDao.findOne(pid);
				
				partyService.appendPartyPositions(staff, bid, false);
				
				staff = PartyReducer.reduceMax(staff);
				Money money = new Money();
				money.setSumm(new BigDecimal((m.get("balance")).toString()));
				money.setCurrency((String) m.get("currency"));
				
				StaffBalanceDto sb = findByStaff(sbL, staff);
				if (sb == null) {
					sb = new StaffBalanceDto();
					sb.setStaff(staff);
					List<Money> balance = new ArrayList<>();
					balance.add(money);
					sb.setBalance(balance);
					sbL.add(sb);
				} else {
					sb.getBalance().add(money);
				}
			}
			
			return sbL;
		} catch (Exception e) {
			throw e;
		}
	}
	
	private StaffBalanceDto findByStaff(List<StaffBalanceDto> sbL, Party staff) {
		for (StaffBalanceDto s : sbL) {
			if (s.getStaff().getId() == staff.getId())
				return s;
		}
		return null;
	}
	
	// ***********************************************************************************************************
	
	@Autowired
	FinEntryDao feDao;
	
	private List<FinEntry> getSbFes(Integer cid, Long partyId, String currency, String ds, String de) throws Exception {
		try {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			List<FinEntry> feL = new ArrayList<>();
			if (!GeneralUtil.isEmptyString(ds) && ds.length() > 7) {
				Date dsDate = df.parse(ds);
				if (!GeneralUtil.isEmptyString(de) && de.length() > 7) {
					Date deDate = df.parse(de);
					feL = feDao.allBySbFromTill(cid, partyId, currency, dsDate, deDate);
				} else {
					feL = feDao.allBySbFrom(cid, partyId, currency, dsDate);
				}
			} else if (!GeneralUtil.isEmptyString(de) && de.length() > 7) {
				Date deDate = df.parse(de);
				feL = feDao.allBySbTill(cid, partyId, currency, deDate);
			} else {
				feL = feDao.allBySb(cid, partyId, currency);
			}
			return feL;
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public List<FinEntry> getSbJournal(Integer cid, Integer bid, Long partyId, String currency, String ds, String de) throws Exception {
		try {
			List<FinEntry> feL = getSbFes(cid, partyId, currency, ds, de);
			List<FinEntry> out = new ArrayList<>(); 
					
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

			Date dsDate = df.parse(ds);
			Calendar cal = Calendar.getInstance();
			cal.setTime(dsDate);
			cal.add(Calendar.DATE, -1);
			dsDate = cal.getTime();
			
			Date deDate = df.parse(de);
			
			FinEntry feStart = new FinEntry();
			Map<String, Object> resMap = prlDao.getSingleStaffBalance(cid, partyId, currency, dsDate);
			if (resMap != null && resMap.size() > 0) {
				feStart.setWsumm(new BigDecimal(Double.valueOf(String.valueOf(resMap.get("balance")))));
			} else {
				feStart.setWsumm(new BigDecimal(0));
			}
			feStart.setDsumm(feStart.getWsumm());
			feStart.setDdate(dsDate);
			feStart.setWcurrency(new Currency(currency));
			feStart.setInfo("Сальдо на начало периода");
			BigDecimal balance = feStart.getDsumm();
			
			out.add(feStart);
			for (FinEntry fe: feL) {
				FinEntry o = fe.clone();
				if (o.getDc().equals(FinEntry.DC_DEBET)) {
					o.setWsumm(o.getWsumm().multiply(new BigDecimal(-1)));
				}
				balance = balance.add(o.getWsumm());
				o.setDsumm(balance);
				out.add(o);
			}
			
			FinEntry feEnd = new FinEntry();
			feEnd.setDdate(deDate);
			feEnd.setWcurrency(new Currency(currency));
			feEnd.setDsumm(balance);
			feEnd.setWsumm(new BigDecimal(0));
			feEnd.setInfo("Сальдо на конец периода");
			out.add(feEnd);
			
			return out;
		} catch (Exception e) {
			throw e;
		}
	}

	private static final String FILE_TYPE_PDF = "PDF";
	private static final String FILE_TYPE_EXCEL = "XLS";
	
	@Override
	public String getSbJournalFile(Integer cid, Integer bid, Long partyId, String currency, Date ds, Date de,
			String format) throws Exception {
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		List<FinEntry> feL = getSbJournal(cid, bid, partyId, currency, df.format(ds), df.format(de));
		
		JasperReport jr = JasperCompileManager.compileReport("src/main/resources/reports/finance/payroll/staffCashJournal.jrxml");
		
		Map<String, Object> params = new HashMap<>();
		Party staff = partyDao.findOne(partyId);
		params.put("staff", staff.getFullFIO());
		params.put("dateStart", ds);
		params.put("dateEnd", de);
		params.put("currency", currency);
		
		JasperPrint jp = JasperFillManager.fillReport(jr, params, new JRBeanCollectionDataSource(feL));
		
//		JasperViewer jv = new JasperViewer(jp);
//		jv.setVisible(true);
		
//		JasperExportManager.exportReportToPdfFile(jp, "src/main/resources/reports/finance/payroll/sbjournal.pdf");
		
//		RandomStringUtils gen = new RandomStringUtils();
//		String name = gen.randomAlphanumeric(16);
//		System.out.println("FILE NAME: " + name);
//		String path = "src/main/resources/static/tmp/" + name + ".pdf";
//		JasperExportManager.exportReportToPdfFile(jp, path);
//		
//		File pdf = new File(path);
//		FileInputStream pdfFile = new FileInputStream(pdf);
//
//		pdfFile.close();
//		if (!pdf.delete()) throw new Exception("Couldn't delete the temporary file");
		
		// Blob blob = new javax.sql.rowset.serial.SerialBlob(pdf);
		
		String str = "";
		
		if (format.equals(FILE_TYPE_EXCEL)) {
			str = JasperExportManager.exportReportToXml(jp);
		} else if (format.equals(FILE_TYPE_PDF)) {
			byte[] file = JasperExportManager.exportReportToPdf(jp);
			str = DatatypeConverter.printBase64Binary(file);
		}
		
		return str;
	}
	
}
