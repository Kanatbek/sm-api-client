package com.cordialsr.service.implementation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.AwardDao;
import com.cordialsr.Dao.BranchDao;
import com.cordialsr.Dao.CompanyDao;
import com.cordialsr.Dao.ContractAwardScheduleDao;
import com.cordialsr.Dao.ContractDao;
import com.cordialsr.Dao.CurrencyDao;
import com.cordialsr.Dao.EmployeeDao;
import com.cordialsr.Dao.FinCurrateDao;
import com.cordialsr.Dao.PartyDao;
import com.cordialsr.Dao.PayrollDao;
import com.cordialsr.Dao.PositionDao;
import com.cordialsr.domain.Award;
import com.cordialsr.domain.AwardType;
import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractAwardSchedule;
import com.cordialsr.domain.ContractStornoRequest;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.FinCurrate;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.Payroll;
import com.cordialsr.domain.Position;
import com.cordialsr.domain.factory.PayrollFactory;
import com.cordialsr.domain.validator.PayrollValidator;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.ContractAwardService;
import com.cordialsr.service.FinDocService;
import com.cordialsr.service.PayrollService;

@Service
@Transactional(rollbackFor = Exception.class)
public class PayrollServiceImpl implements PayrollService {

	@Autowired
	PayrollDao prlDao;
	
	@Autowired
	EmployeeDao emplDao;
	
	@Autowired
	PartyDao partyDao;
	
	@Autowired
	FinDocService fdService;
	
	@Autowired
	FinCurrateDao rateDao;
	
	@Autowired 
	CompanyDao companyDao;
	
	@Autowired
	AwardDao awDao;

	@Autowired
	BranchDao brDao;

	@Autowired
	ContractDao conDao;
	
	@Autowired
	ContractAwardService cawService;
	
	@Autowired
	ContractAwardScheduleDao cawDao;
	
	@Autowired
	CurrencyDao curDao;
	
	@Autowired 
	PositionDao posDao;
	
	@Override
	public List<Payroll> getPayrollsForMonth(Integer cid, Integer bid, String kind, Integer pos, Boolean dbt, Integer month, Integer year) throws Exception {
		try {
			
			List<Payroll> prlL = new ArrayList<>();
			if (!GeneralUtil.isEmptyInteger(pos))
				if (pos == Position.POS_COORDINATOR)
					prlL = prlDao.getAllByMonthPos(cid, kind, pos, month, year);
				else 
					prlL = prlDao.getAllByMonthBranchPos(cid, bid, kind, pos, month, year);
			else {
				prlL = prlDao.getAllByMonthKind(cid, bid, kind, month, year);
			}
			if (kind.equals(Payroll.KIND_SALARY)) {
				prlL = assembleSalaryPayrolls(prlL, cid, bid, month, year);
			} else if (kind.equals(Payroll.KIND_PREMI) || kind.equals(Payroll.KIND_BONUS)) {
				prlL = assemblePremiBonusPayrolls(prlL, cid, bid, kind, pos, dbt, month, year);
			}
			
			return prlL;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// ***********************************************************************************************************
	
	private List<Payroll> assembleSalaryPayrolls(List<Payroll> oldPrlL, Integer cid, Integer bid, Integer month, Integer year) throws Exception {
		try {
			List<Employee> emplL = emplDao.findAllAcutalByComBr(cid, bid);
			Calendar cal = Calendar.getInstance();
			DateFormat f = new SimpleDateFormat("dd.MM.yyyy");
			List<Payroll> prlL = new ArrayList<>();
			for (Employee e: emplL) {
				cal.setTime(e.getDateHired());
				boolean isNewEmployee = ((cal.get(Calendar.YEAR) == year) && (cal.get(Calendar.MONTH)+1) == month);
				
				Payroll payroll = PayrollFactory.payrollContains(oldPrlL, e.getId());
				
				if (payroll != null) {
					if (payroll.getSum().compareTo(e.getSalary()) != 0 
							&& !isNewEmployee) {
						payroll.setSum(e.getSalary());
						payroll.setCurrency(e.getCurrency());
					}
				} else {
					BigDecimal accrueSum  = e.getSalary();
					String info = "";
					if (isNewEmployee) {
						Integer maxDate = cal.getActualMaximum(Calendar.DATE);
						Integer days = maxDate - cal.get(Calendar.DATE);
						double accrueRate = ((double) days)/((double) maxDate);
						accrueSum = new BigDecimal(e.getSalary().doubleValue() * accrueRate);
						info = "Начал " + f.format(e.getDateHired()) + " | Начислено за: " + days + " дней.";
					}
					
					if (accrueSum.compareTo(BigDecimal.ZERO) > 0) {
						Date payrollDate = GeneralUtil.endOfMonth(GeneralUtil.getCalendar(month, year).getTime());
						Payroll newPrl = PayrollFactory.formPayrollFromEmployee(e);
						newPrl.setSum(accrueSum);
						newPrl.setKind(Payroll.KIND_SALARY);
						newPrl.setMonth(month);
						newPrl.setYear(year);
						newPrl.setPayrollDate(payrollDate);
						newPrl.setInfo(info);
						prlL.add(newPrl);
					}
				}
			}
			prlL.addAll(oldPrlL);
			return prlL;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// ********************************************************************************************************************
	
	private List<Payroll> assemblePremiBonusPayrolls(List<Payroll> oldPrlL, Integer cid, Integer bid, 
			String kind, Integer pos, Boolean dbt, Integer month, Integer year) throws Exception {
		try {
			Calendar cal = GeneralUtil.getCalendar(month, year);
			Date ds = GeneralUtil.startOfMonth(cal.getTime());
			Date de = GeneralUtil.endOfMonth(ds);
			
			String awType = null;
			switch (kind) {
				case Payroll.KIND_PREMI : awType = AwardType.TYPE_PREMI;break;
				case Payroll.KIND_BONUS : awType = AwardType.TYPE_BONUS;break;
			}
			if (awType != null) {
				List<Payroll> prlL = new ArrayList<>();
				List<ContractAwardSchedule> cawL = null;
				if (!GeneralUtil.isEmptyInteger(pos)) {
					if (pos == Position.POS_COORDINATOR) cawL = cawDao.getAccruableAwardListByPos(cid, awType, pos, de);
					else cawL = cawDao.getAccruableAwardListByPosBr(cid, bid, awType, pos, de);
				} else { 
					cawL = cawDao.getAccruableAwardList(cid, bid, awType, de);
				}
				
				for (ContractAwardSchedule caw: cawL) {
					Payroll payroll = PayrollFactory.findPostedPremiPayroll(oldPrlL, caw);
					BigDecimal accrueSum  = caw.getSumm().subtract(caw.getAccrued());
					if (payroll != null) {
						accrueSum = accrueSum.subtract(payroll.getSum());
					}
					if (!GeneralUtil.isEmptyBigDecimal(accrueSum)) {
						Contract con = caw.getContract();
						Employee e = PayrollFactory.getEmployeeFromContract(con, caw.getPosition());
						if (e != null) {
							Payroll newPrl = PayrollFactory.constructPayrollFromContract(con, caw, e);
							newPrl.setSum(accrueSum);
							newPrl.setKind(kind);
							
							Integer payrollMonth = caw.getDateSchedule().getMonth() + 1;
							Integer payrollYear = caw.getDateSchedule().getYear() + 1900;
							newPrl.setMonth(payrollMonth);
							newPrl.setYear(payrollYear);
							
							if (PayrollFactory.conOdSumExist(con, de) && dbt != null && dbt) {
								newPrl.setRestricted(true);
							}
							prlL.add(newPrl);
						} else {
							throw new Exception(caw.getPosition().getName() +  " not bound to contract [" + con.getContractNumber() + "]");
						}
					}
				}

				for (Payroll op : oldPrlL) {
					if (!op.getContract().getStorno()) {
						prlL.add(op);		
					}
				}
				
				return prlL;
			} else {
				throw new Exception("AwardType is null.");
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	// **********************************************************************************************************************************
	
	@Override
	public List<Payroll> generateBonusSchedule(Integer cid, Integer bid, Integer pos, Integer month, Integer year) throws Exception {
		try {
			Calendar cal = GeneralUtil.getCalendar(month, year);
			Date ds = GeneralUtil.startOfMonth(cal.getTime());
			Date de = GeneralUtil.endOfMonth(cal.getTime());
			
			List<Payroll> prlL = assembleBonusSchedule(cid, bid, pos, ds, de);
			
			return prlL;
		} catch (Exception e) {
			throw e;
		}
	}
	
	private List<Payroll> assembleBonusSchedule(Integer cid, Integer bid, Integer pos, Date ds, Date de) throws Exception {
		try {
			List<Payroll> prlL = new ArrayList<>();
			
			List<Map<String, Object>> srL = cawDao.getSalesAndRentsCount(cid, bid, pos, 
					GeneralUtil.getDateStr(ds), GeneralUtil.getDateStr(de));
			
			Branch branch = brDao.findOne(bid);
			if (branch != null) {
				for (Map<String, Object> sr:srL) {
					Long emplId = null;
					if (sr.get("emplId") != null) emplId = new Long(String.valueOf(sr.get("emplId")));
					Integer rent =  new Integer(String.valueOf(sr.get("rent")));
					Integer sale =  new Integer(String.valueOf(sr.get("sale")));
					Integer total =  new Integer(String.valueOf(sr.get("total")));
					Employee employee = emplDao.findOne(emplId);
					
					if (employee != null && employee.getParty() != null) {
						Integer cnt = branch.getCountry().getId();
						if (!GeneralUtil.isEmptyInteger(cnt)) {
							BigDecimal accrueSum = new BigDecimal(0);
							Currency currency = new Currency();
							// Rent bonus
							Award bonusRent = awDao.getBonusRuleForRent(cid, cnt, AwardType.TYPE_BONUS, pos, rent, de);
							if (bonusRent != null) {
								List<Contract> conL = null;
								switch(pos) {
									case Position.POS_DEALER: conL = conDao.getDealerContractsFor(cid, bid, emplId, true, ds, de); break;
									case Position.POS_DEMOSEC: conL = conDao.getDemosecContractsFor(cid, bid, emplId, true, ds, de); break;
									case Position.POS_MANAGER: conL = conDao.getManagerContractsFor(cid, bid, emplId, true, ds, de); break;
									case Position.POS_DIRECTOR: conL = conDao.getDirectorContractsFor(cid, bid, emplId, true, ds, de); break;
									case Position.POS_COORDINATOR: conL = conDao.getCoordinatorContractsFor(cid, emplId, true, ds, de); break;
								}
								cawService.updateContractAwards(conL, bonusRent, employee);
								accrueSum = accrueSum.add(bonusRent.getSumm().multiply(new BigDecimal(rent)));
								currency = bonusRent.getCurrency();
							}
							// Sales bonus
							Award bonusSale = awDao.getBonusRuleForSale(cid, cnt, AwardType.TYPE_BONUS, pos, sale, de);
							if (bonusSale != null) {
								List<Contract> conL = null;
								switch(pos) {
									case Position.POS_DEALER: conL = conDao.getDealerContractsFor(cid, bid, emplId, false, ds, de); break;
									case Position.POS_DEMOSEC: conL = conDao.getDemosecContractsFor(cid, bid, emplId, false, ds, de); break;
									case Position.POS_MANAGER: conL = conDao.getManagerContractsFor(cid, bid, emplId, false, ds, de); break;
									case Position.POS_DIRECTOR: conL = conDao.getDirectorContractsFor(cid, bid, emplId, false, ds, de); break;
									case Position.POS_COORDINATOR: conL = conDao.getCoordinatorContractsFor(cid, emplId, false, ds, de); break;
								}
								cawService.updateContractAwards(conL, bonusSale, employee);
								accrueSum = accrueSum.add(bonusSale.getSumm().multiply(new BigDecimal(sale)));
								currency = bonusSale.getCurrency();
							}
							
							// Construct new Payroll
							if (!GeneralUtil.isEmptyBigDecimal(accrueSum) || 1 == 1) {
								String info = "";
								info += employee.getPosition().getName() + " | ";
								info += "Аренда: " + rent + " | ";
								info += "Продажа: " + sale + " | ";
								info += "Итого: " + total + " | ";


								Payroll newPrl = PayrollFactory.formPayrollFromEmployee(employee);
								newPrl.setSum(accrueSum);
								newPrl.setCurrency(currency);
								newPrl.setRent(rent);
								newPrl.setSale(sale);
								newPrl.setTotal(total);
								newPrl.setKind(Payroll.KIND_BONUS);
								Calendar cal = Calendar.getInstance();
								cal.setTime(ds);
								newPrl.setMonth(cal.get(Calendar.MONTH) + 1);
								newPrl.setYear(cal.get(Calendar.YEAR));
								newPrl.setInfo(info);
								newPrl.setPosted(true);
								prlL.add(newPrl);
							}
							
						} else {
							throw new Exception("Country not found.");
						}
					}
				}
				return prlL;
			} else {
				throw new Exception("Branch not found with id[" + bid + "].");
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	// **********************************************************************************************************************************
	
	@Override
	public List<Payroll> doPostPayrolls(Integer cid, List<Payroll> prlL) throws Exception {
		try {
			List<Payroll> out = new ArrayList<>();
			Company company = companyDao.findOne(cid);
			for (Payroll p: prlL) {
				p.setPostedDate(new Date());
				PayrollValidator.validateBasic(p);
				if (p.getApproved() && !p.getPosted()) {
					FinCurrate extRate = rateDao.extRateByDate(p.getCompany().getId(), 
							 company.getCurrency().getCurrency(), 
							 p.getCurrency().getCurrency(), p.getPostedDate());
					if (extRate != null) {
						p.setCompany(company);
						
						if (p.getEmployee() != null) {
							Employee empl = emplDao.findOne(p.getEmployee().getId());
							p.setEmployee(empl);
						}
						Integer porder = 0;
						if (p.getConAward() != null && p.getConAward().getId() != null) {
							ContractAwardSchedule conAward = cawDao.findOne(p.getConAward().getId());
							porder = conAward.getPorder();	
						}
						p.setInfo(PayrollFactory.generateInfo(p, porder));
						String refkeyMain = null;
						if (p.getContract() != null) refkeyMain = p.getContract().getRefkey();
						
						// Posted payroll or Payroll is for penalty
						Payroll postedPrl = getPostedPayrol(p);
						if (postedPrl == null || !postedPrl.getPosted() || p.getSum().compareTo(BigDecimal.ZERO) < 0) {
							p.setRate(extRate.getRate());
							p.setAccrued(p.getSum());
							String refkey = null;
							
							if ((p.getKind().equals(Payroll.KIND_PREMI) || p.getKind().equals(Payroll.KIND_BONUS))
									&& p.getConAward() != null) {
								ContractAwardSchedule caw = cawService.updateConAwardSchedule(p);
								p.setParty(caw.getParty());
								p.setConAward(caw);
							}
							
							if (p.getKind().equals(Payroll.KIND_PREMI) 
									&& postedPrl != null && !postedPrl.getPosted()) {
								postedPrl.setPostedDate(new Date());
								postedPrl.setPosted(true);
								refkey = fdService.doPostPayrollFromTransit(postedPrl);
							} else {
								refkey = fdService.doPostPayroll(p, refkeyMain);
							}
							p.setRefkey(refkey);
							if (p.getConAward() != null) p.getConAward().setRefkey(p.getRefkey());
							p.setPosted(true);
							
							// Post payroll
							p = prlDao.save(p);
						} else {
							p.setInfo("Повторное начисление запрещена!");
							p.setRestricted(true);
						}
						out.add(p);
					} else {
						throw new Exception("Курс валют " + company.getCurrency().getCurrency() + " в " + p.getCurrency().getCurrency() + " не найден.");
					}
				}
			}
			return out;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// ******************************************************************************************************************************************************
	// ******************************************************************************************************************************************************
	
	@Override
	public List<Payroll> doPostPayrollsToTransitAccount(Integer cid, List<Payroll> prlL) throws Exception {
		try {
			List<Payroll> out = new ArrayList<>();
			Company company = companyDao.findOne(cid);
			for (Payroll p: prlL) {
				PayrollValidator.validateBasic(p);
				if (!p.getApproved() && !p.getPosted()) {
					FinCurrate extRate = rateDao.extRateByDate(p.getCompany().getId(), 
							 company.getCurrency().getCurrency(), 
							 p.getCurrency().getCurrency(), p.getPayrollDate());
					if (extRate != null) {
						p.setPosted(false);
						p.setCompany(company);
						
						if (p.getEmployee() != null) {
							Employee empl = emplDao.findOne(p.getEmployee().getId());
							p.setEmployee(empl);
						}
						
						p.setInfo(PayrollFactory.generateInfo(p, null));
						String refkeyMain = null;
						if (p.getContract() != null) refkeyMain = p.getContract().getRefkey();
						// Posted payroll
						Payroll postedPrl = getPostedPayrol(p);
						if (postedPrl == null) {
							p.setRate(extRate.getRate());
							String refkey = fdService.doPostPayrollToTransit(p, refkeyMain);
							p.setRefkey(refkey);
							
							p = prlDao.save(p);
							
							p.getConAward().setRefkey(p.getRefkey());
							cawDao.save(p.getConAward());
						} else {
							p.setInfo("Повторное начисление запрещена!");
							p.setRestricted(true);
						}
						out.add(p);
					} else {
						throw new Exception("Курс валют " + company.getCurrency().getCurrency() + " в " + p.getCurrency().getCurrency() + " не найден.");
					}
				}
			}
			return out;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<Payroll> doPostPayrollsFromTransit(Integer cid, List<Payroll> prlL) throws Exception {
		try {
			List<Payroll> out = new ArrayList<>();
			Company company = companyDao.findOne(cid);
			for (Payroll p: prlL) {
				p.setPostedDate(new Date());
				PayrollValidator.validateBasic(p);
				if (p.getApproved() && !p.getPosted()) {
					FinCurrate extRate = rateDao.extRateByDate(p.getCompany().getId(), 
							 company.getCurrency().getCurrency(), 
							 p.getCurrency().getCurrency(), p.getPostedDate());
					if (extRate != null) {
						p.setPosted(true);						
						p.setRate(extRate.getRate());
						fdService.doPostPayrollFromTransit(p);
						p.setAccrued(p.getSum());
						if ((p.getKind().equals(Payroll.KIND_PREMI) || p.getKind().equals(Payroll.KIND_BONUS))
								&& p.getConAward() != null) {
							cawService.updateConAwardSchedule(p);
						}
						// Post payroll
						p = prlDao.save(p);
						out.add(p);
					} else {
						throw new Exception("Курс валют " + company.getCurrency().getCurrency() + " в " + p.getCurrency().getCurrency() + " не найден.");
					}
				}
			}
			return out;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// ******************************************************************************************************************************************************
	// ******************************************************************************************************************************************************
	
	private Payroll getPostedPayrol(Payroll p) throws Exception {
		try {
			Payroll prl = null;
			if (p.getEmployee() == null) {
				prl = prlDao.getPartyPostedPayroll(p.getCompany().getId(), p.getBranch().getId(), p.getKind(), 
						p.getMonth(), p.getYear(), p.getParty().getId());
			} else if (p.getContract() == null) {
				prl = prlDao.getEmplPostedPayrollSalary(p.getCompany().getId(), p.getBranch().getId(), 
						p.getMonth(), p.getYear(), p.getEmployee().getId());
			} else {
				prl = prlDao.getSinglePostedPayrollKindByEmpl(p.getCompany().getId(), p.getBranch().getId(), p.getKind(), 
						p.getMonth(), p.getYear(), p.getEmployee().getId(), p.getContract().getId());
			}
			return prl;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// *********************************************************************************************************************
	
	// *********************************************************************************************************************
	
	private static final String[] inputExcelPayrolHeaderOrder = {"COMPANY", "BRANCH", "IIN", "FIO", "TYPE", "MONTH", "YEAR", "SUMMA", "VALUTA", "INFO"};
	
	@Override
	public List<Payroll> getPayrollsFromXls(Integer cid, Integer bid, Integer month, Integer year, String xls64) throws Exception {
		try {
			RandomStringUtils gen = new RandomStringUtils();
			String name = gen.randomAlphanumeric(16);
			System.out.println("FILE NAME: " + name);
			String path = "src/main/resources/static/tmp/" + name;
			FileOutputStream fos = new FileOutputStream(path);
			fos.write(Base64.getDecoder().decode(xls64));
			fos.close();
			
			File xls = new File(path);
			FileInputStream excelFile = new FileInputStream(xls);
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = datatypeSheet.iterator();
            
            Row headerRow = iterator.next();
            Iterator<Cell> hcIterator = headerRow.iterator();
            int i = 0;
            while (hcIterator.hasNext()) {
            	Cell hCell = hcIterator.next();
                if (!hCell.getStringCellValue().equals(inputExcelPayrolHeaderOrder[i])) 
                	throw new Exception("Порядок заголовок не правильна: '" + hCell.getStringCellValue() + "' вместо '" + inputExcelPayrolHeaderOrder[i] + "'");
                i++;
            }
            
            Company company = companyDao.findOne(cid);
            Branch branch = brDao.findOne(bid);
            List<Payroll> prlL = new ArrayList<>();
            int rowNumber = 0;
            if (company != null && branch != null) {
            	while (iterator.hasNext()) {
            		rowNumber++;
            		Row currentRow = iterator.next();
                    Iterator<Cell> cellIterator = currentRow.iterator();
                      
                    Cell cell = cellIterator.next();
                    Long comId = Math.round(cell.getNumericCellValue());
                    if (comId.intValue() != cid) throw new Exception("ID Организации [" + comId + "] не соотсветствует к ID исходной организации [" + cid + "]" + " Строка: " + rowNumber);

                    cell = cellIterator.next();
                    Long brId = Math.round((cell.getNumericCellValue()));
                    if (brId.intValue() != bid) throw new Exception("ID филиала [" + brId + "] не соотсветствует к ID исходного филиала [" + bid + "]" + " Строка: " + rowNumber);

                    cell = cellIterator.next();
                    String iin = cell.getStringCellValue();
                    Party party = partyDao.findByIinBin(cid, iin);
                    if (party == null) throw new Exception("Контрагент с ИИН [" + iin + "] не найден." + " Строка: " + rowNumber);
                    
                    cell = cellIterator.next();
                    String fio = cell.getStringCellValue();
                    
                    cell = cellIterator.next();
                    String kind = cell.getStringCellValue();
                    
                    cell = cellIterator.next();
                    Long mon = Math.round(cell.getNumericCellValue());
                    if (mon.intValue() < 1 || mon.intValue() > 12 || mon.intValue() != month) throw new Exception("Месяц [" + mon +"] не соответствует к исходному месяцу [" + month + "] " + " Строка: " + rowNumber);
                    
                    cell = cellIterator.next();
                    Long yr = Math.round(cell.getNumericCellValue());
                    if (yr.intValue() != year) throw new Exception("Год " + yr + " не соответствует исходному году " + year + ". " + " Строка: " + rowNumber);
                    
                    cell = cellIterator.next();
                    BigDecimal summ = new BigDecimal(cell.getNumericCellValue());
                    if (summ == null || summ.compareTo(BigDecimal.ZERO) == 0) throw new Exception("Сумма неправильна [" + summ.setScale(2, RoundingMode.HALF_EVEN) + "]" + " Строка: " + rowNumber);
                    
                    cell = cellIterator.next();
                    String cur = cell.getStringCellValue();
                    Currency currency = curDao.findOne(cur);
                    if (currency == null) throw new Exception("Такая валюта [" + cur + "] отсутствует в базе!" + " Строка: " + rowNumber);
                    
                    String info = "";
                    if (cellIterator.hasNext()) {
                    	cell = cellIterator.next();
                        info = cell.getStringCellValue();
                        if (info.length() > 127) info = info.substring(0, 127);
                    }
                    
                    Payroll newPrl = new Payroll();
                    newPrl.setCompany(company);
            		newPrl.setBranch(branch);
            		newPrl.setParty(party);
            		newPrl.setPayrollDate(new Date());
            		newPrl.setPostedDate(new Date());
            		newPrl.setApproved(false);
            		newPrl.setPosted(false);
            		newPrl.setStorno(true);
            		newPrl.setKind(kind);
					newPrl.setSum(summ);
					newPrl.setCurrency(currency);
            		newPrl.setMonth(month);
					newPrl.setYear(year);
					newPrl.setInfo(info);
					
					Payroll oldPrl = prlDao.getPartyPostedPayroll(newPrl.getCompany().getId(), newPrl.getBranch().getId(), newPrl.getKind(), 
							newPrl.getMonth(), newPrl.getYear(), newPrl.getParty().getId());
					
					if (oldPrl != null) {
						prlL.add(oldPrl);
						if (oldPrl.getSum().compareTo(newPrl.getSum()) != 0) {
							newPrl.setRestricted(true);
							prlL.add(newPrl);
						}
					} else {
						prlL.add(newPrl);
					}
                }
                
            } else {
            	throw new Exception("Организация или филиал не указаны!");
            }
            excelFile.close();
            if (!xls.delete()) throw new Exception("Couldn't delete the temporary file");
            return prlL;
        } catch (FileNotFoundException e) {
            throw e;
        } catch (IOException e) {
        	throw e;
        } catch( Exception e) {
        	throw e;
		}
	}

	// ***********************************************************************************************************
	
	@Override
	public List<Payroll> payrollPremisFromNewContract(Contract con) throws Exception {
		try {
			List<Payroll> prlL = constructPayrollsFromContract(con);
			prlL = doPostPayrollsToTransitAccount(con.getCompany().getId(), prlL);
			return prlL;
		} catch (Exception e) {
			throw e;
		}
	}
	
	private List<Payroll> constructPayrollsFromContract(Contract con) throws Exception {
		try {
			List<Payroll> prlL = new ArrayList<Payroll>();
			
			for (ContractAwardSchedule caw: con.getCawSchedules()) {
				BigDecimal accrueSum  = caw.getSumm();
				
				if (!GeneralUtil.isEmptyBigDecimal(accrueSum)) {
					Position pos = posDao.findOne(caw.getPosition().getId());
					caw.setPosition(pos);
					
					Employee empl = PayrollFactory.getEmployeeFromContract(con, caw.getPosition());
					empl = emplDao.findOne(empl.getId());
					if (empl != null) {
						Payroll newPrl = PayrollFactory.constructPayrollFromContract(con, caw, empl);
						prlL.add(newPrl);
					} else {
						throw new Exception(caw.getPosition().getName() +  " not bound to contract [" + con.getContractNumber() + "]");
					}
				}
			}
			return prlL;
		} catch(Exception e) {
			throw e;
		}
	}
	
	// **************************************************************************************************************
	
	@Override
	public boolean rollbackContractAwards(ContractStornoRequest stReq, Integer monthAge, String trCode) throws Exception {
		try {
			double returnRatio = Payroll.AGE_UNTIL_6_MONTH_STORNO_RATIO;
			if (monthAge >= 6 && monthAge < 12) returnRatio = Payroll.AGE_UNTIL_12_MONTH_STORNO_RATIO;
			else if (monthAge >= 12) returnRatio = Payroll.AGE_ABOVE_12_MONTH_STORNO_RATIO;
			
			if (returnRatio > 0) {
				List<Payroll> prlL = assembleStornoPayrollsForContract(stReq, returnRatio);
				
				for (Payroll p : prlL) {
					p.setApproved(true);
					p.setInfo(p.getInfo() + " | " + stReq.getInfo());
				}
				
				doPostPayrolls(stReq.getCompany().getId(), prlL);
			}
			return true;
		} catch(Exception e) {
			throw e;
		}
	}
	
	private List<Payroll> assembleStornoPayrollsForContract(ContractStornoRequest stReq, Double returnRatio) throws Exception {
		try {
			Contract contract = stReq.getContract();
			List<Payroll> prlL = new ArrayList<>();
			
			for (ContractAwardSchedule caw: contract.getCawSchedules()) {
				if (caw.getRevertOnCancel()) {
					BigDecimal accrueSum = caw.getAccrued().multiply(new BigDecimal(returnRatio));
					
					if (caw.getRevertSumm() != null && accrueSum.compareTo(caw.getRevertSumm()) > 0 ) 
						accrueSum = caw.getRevertSumm();
					accrueSum = accrueSum.negate();
					System.out.println(accrueSum);
					
					
					if (accrueSum.compareTo(BigDecimal.ZERO) < 0) {
						Contract con = caw.getContract();
						Employee empl = PayrollFactory.getEmployeeFromContract(con, caw.getPosition());
						if (empl != null) {
							Payroll newPrl = PayrollFactory.constructPayrollFromContract(con, caw, empl);
							newPrl.setPayrollDate(stReq.getCloseDate());
							newPrl.setSum(accrueSum);
							String kind;
							if (caw.getAwardType().getName().equals(AwardType.TYPE_BONUS)) kind = Payroll.KIND_BONUS;
							else if (caw.getAwardType().getName().equals(AwardType.TYPE_PREMI)) kind = Payroll.KIND_PREMI;
							else throw new Exception("Неопределенный тип начисления!");
							newPrl.setKind(kind);
							
							newPrl.setPayrollDate(stReq.getCloseDate());
							Calendar cal = Calendar.getInstance(); cal.setTime(stReq.getCloseDate());
							Integer month = cal.get(Calendar.MONTH) + 1;
							Integer year = cal.get(Calendar.YEAR);
							newPrl.setMonth(month);
							newPrl.setYear(year);
							
							newPrl.setStorno(true);
							prlL.add(newPrl);
						} else {
							throw new Exception(caw.getPosition().getName() +  " not bound to contract [" + con.getContractNumber() + "]");
						}
					}
				}
			}
			return prlL;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void payrollFirstPremiForDealer(Contract con) throws Exception {
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(con.getDateSigned());
			Payroll prl = prlDao.getSinglePostedPayrollKindByEmpl(
					con.getCompany().getId(), 
					con.getBranch().getId(), 
					Payroll.KIND_PREMI, 
					(cal.get(Calendar.MONTH) + 1), 
					cal.get(Calendar.YEAR), 
					con.getDealer().getId(), 
					con.getId());
			if (prl != null) {
				List<Payroll> prlL = new ArrayList<>();
				prl.setApproved(true);
				prlL.add(prl);
				doPostPayrolls(con.getCompany().getId(), prlL);	
			}
		} catch (Exception e) {
			throw e;
		}
	}

}