package com.cordialsr.service.implementation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.cordialsr.Dao.AwardDao;
import com.cordialsr.Dao.ContractDao;
import com.cordialsr.Dao.EmployeeDao;
import com.cordialsr.Dao.FinDocDao;
import com.cordialsr.Dao.InvoiceDao;
import com.cordialsr.Dao.PayrollDao;
import com.cordialsr.Dao.SmFilterChangeDao;
import com.cordialsr.api.uz.payme.common.exception.UnableCompleteException;
import com.cordialsr.domain.Award;
import com.cordialsr.domain.AwardType;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractAwardSchedule;
import com.cordialsr.domain.ContractHistory;
import com.cordialsr.domain.ContractPaymentSchedule;
import com.cordialsr.domain.Department;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.FinDocType;
import com.cordialsr.domain.FinEntry;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.Payroll;
import com.cordialsr.domain.Position;
import com.cordialsr.domain.SmFilterChange;
import com.cordialsr.domain.factory.ConAwSchFactory;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.ContractPostRefactorService;

@Service
@Transactional(rollbackFor = Exception.class)
public class ContractPostRefactorServiceImpl implements ContractPostRefactorService {

	@Autowired
	ContractDao conDao;
	
	@Autowired
	PayrollDao prlDao;
	
	@Autowired
	FinDocDao fdDao;
	
	@Autowired
	InvoiceDao invoiceDao;
	
	@Override
	public void shiftContractDate(String conNum, String dateStr) throws Exception {
		try {
			Contract con = conDao.findByContractNumber(conNum);
			if (con != null) {
				DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
				Date date = f.parse(dateStr);
				
				Long monthBtw = ChronoUnit.MONTHS.between(
				        LocalDate.parse(dateStr).withDayOfMonth(1),
		        		LocalDate.parse(f.format(con.getDateSigned())).withDayOfMonth(1));
				// Long monthBtw = 1L;
				// if (monthBtw >= 3L) throw new Exception("Change allowed period expired. [3 months] for contract " + conNum);
				int monthStep = -monthBtw.intValue();
				System.out.println(monthStep);
				
				con.setDateSigned(date);
				
				Calendar dc = Calendar.getInstance();
				dc.setTime(date);
				int dd = dc.get(Calendar.DATE);
				System.out.println(dd);
				
				for (ContractPaymentSchedule ps : con.getPaymentSchedules()) {
					ps.setPaymentDate(shiftDate(ps.getPaymentDate(), monthStep, dd));
				}
				
				for (ContractAwardSchedule caw : con.getCawSchedules()) {
					if (caw.getAwardType().getName().equals(AwardType.TYPE_PREMI)) {
						caw.setDateSchedule(shiftDate(caw.getDateSchedule(), monthStep, dd));
					}
				}
				
				// PAYROLL
				refactorPayrollDate(con, monthStep, dd);
				
				// FINDOC
				List<FinDoc> fdL = fdDao.findAllByContract(con);
				for (FinDoc fd : fdL) {
					if (fd.getFinDocType().getCode().equals(FinDocType.TYPE_IR_INVOICE_RECIEVABLE)
							|| fd.getFinDocType().getCode().equals(FinDocType.TYPE_GS_GENERAL_SELL_CONTRACT)
							|| fd.getFinDocType().getCode().equals(FinDocType.TYPE_GR_GENERAL_RENT_CONTRACT)
							|| fd.getFinDocType().getCode().equals(FinDocType.TYPE_DC_DISCOUNT_COMPANY)
							|| fd.getFinDocType().getCode().equals(FinDocType.TYPE_SO_SETOFF)) {
						refactorFinDocDate(fd, monthStep, dd);
					} else {
						System.out.println(fd.getFinDocType().getCode() + " - Пропущена.");
					}
				}
				
				// SMFC
				refactorFilterChangeGraph(con);
				
				// INVOICE
				refactorInvoiceDate(con);
				
			} else throw new Exception("Contract [" + conNum + "] not found!");
		} catch (Exception e) {
			throw e;
		}
	}
	
	private void refactorPayrollDate(Contract con, int monthStep, int dd) throws Exception {
		try {
			List<Payroll> prlL = prlDao.findAllByContract(con);
			for (Payroll prl : prlL) {
				if (prl.getKind().equals(Payroll.KIND_PREMI)) {
					Calendar pd = Calendar.getInstance();
					pd.setTime(shiftDate(prl.getPayrollDate(), monthStep, dd));
					int month = pd.get(Calendar.MONTH) + 1;
					int year = pd.get(Calendar.YEAR);
					prl.setMonth(month);
					prl.setYear(year);
					prl.setPayrollDate(pd.getTime());
					
					FinDoc fd = fdDao.findByRefkey(prl.getRefkey());
					if (fd != null) refactorFinDocDate(fd, monthStep, dd);
				}
			}
			prlDao.save(prlL);
		} catch(Exception e) {
			throw e;
		}
	}
	
	private void refactorFinDocDate(FinDoc fd, int monthStep, int dd) throws Exception {
		try {
			if (fd != null) {
				Calendar newDate = Calendar.getInstance();
				newDate.setTime(shiftDate(fd.getDocDate(), monthStep, dd));
				int month = newDate.get(Calendar.MONTH) + 1;
				int year = newDate.get(Calendar.YEAR);
				
				fd.setDocDate(newDate.getTime());
				fd.setMonth(month);
				fd.setYear(String.valueOf(year));
				
				for (FinEntry fe : fd.getFinEntries()) {
					fe.setMonth(month);
					fe.setYear(String.valueOf(year));
					fe.setDdate(newDate.getTime());
				}
				fdDao.save(fd);
			} else throw new Exception("FinDoc not found!");
		} catch(Exception e) {
			throw e;
		}
	}
	
	private void refactorInvoiceDate(Contract con) throws Exception {
		try {
			int invType = Invoice.INVOICE_TYPE_SELL;
			if (con.getIsRent()) invType = Invoice.INVOICE_TYPE_RENT;
			Invoice invoice = invoiceDao.findByDocId(con.getId(), invType);
			if (invoice == null) throw new Exception("Invoice not found.");
			invoice.setData(con.getDateSigned());
			invoiceDao.save(invoice);
		} catch(Exception e) {
			throw e;
		}
	}
	
	@Autowired
	SmFilterChangeDao smfcDao;

	private void refactorFilterChangeGraph(Contract con)throws Exception {
		try {
			SmFilterChange smFc = smfcDao.findByContractId(con.getId());
			if (smFc == null)
				throw new Exception("Filter change graph not found.");
			
			smFc.setContract(con);
			smFc.setEnabled(!con.getStorno());
			smFc.setContractNumber(con.getContractNumber());
			
			smFc.setF1Last(con.getDateSigned());
			smFc.setF2Last(con.getDateSigned());
			smFc.setF3Last(con.getDateSigned());
			smFc.setF4Last(con.getDateSigned());
			smFc.setF5Last(con.getDateSigned());
			smFc.setPrLast(con.getDateSigned());
			
			smFc.setF1Next(GeneralUtil.addMonth(con.getDateSigned(), smFc.getF1Mt()));
			smFc.setF2Next(GeneralUtil.addMonth(con.getDateSigned(), smFc.getF2Mt()));
			smFc.setF3Next(GeneralUtil.addMonth(con.getDateSigned(), smFc.getF3Mt()));
			smFc.setF4Next(GeneralUtil.addMonth(con.getDateSigned(), smFc.getF4Mt()));
			smFc.setF5Next(GeneralUtil.addMonth(con.getDateSigned(), smFc.getF5Mt()));
			smFc.setPrNext(GeneralUtil.addMonth(con.getDateSigned(), smFc.getPrMt()));
			
			smfcDao.save(smFc);
		} catch (Exception ex) {
			throw ex;
		}
		
	}
	
	private Date shiftDate(Date date, int monthStep, int dd) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		//	System.out.println(f.format(cal.getTime()));
		cal.add(Calendar.MONTH, monthStep);
		if (dd > cal.getActualMaximum(Calendar.DATE)) dd = cal.getActualMaximum(Calendar.DATE);
		cal.set(Calendar.DATE, dd);
		//	System.out.println(f.format(cal.getTime()));
		return cal.getTime();
	}
	
	
	// ************************************************************************************************
	
	
	@Override
	public void stornoContractPayment(String conNum, Double sum) throws Exception {
		try {
			BigDecimal summ = new BigDecimal(sum);
			
			Contract con = conDao.findByContractNumber(conNum);
			if (con == null) throw new Exception("Contract [" + conNum + "] not found!");
			
			// Get all receivables
			List<ContractPaymentSchedule> cpsL = new ArrayList<>();
			cpsL.addAll(con.getPaymentSchedules());
			
			// Sort receivables by payment date in descending order
			cpsL.sort(new Comparator<ContractPaymentSchedule>() {
				@Override
				public int compare(ContractPaymentSchedule o1, ContractPaymentSchedule o2) {
					return o2.getPaymentDate().compareTo(o1.getPaymentDate());
				}
			});
			
			// Post payments for overdue & current & future receivables
			for (ContractPaymentSchedule cps : cpsL) {
				if (summ.compareTo(BigDecimal.ZERO) > 0) {
					if (cps.getPaid().compareTo(BigDecimal.ZERO) > 0) {
						BigDecimal payStrn = stornoCP(cps.getContract(), cps, summ);
						summ = summ.subtract(payStrn);
					}
				} else break;
			}
			
			if (summ.compareTo(BigDecimal.ZERO) > 0) 
				throw new UnableCompleteException("Storno amount exceeds total paid amount.");
			
			summ = new BigDecimal(sum);
			con.setPaid(con.getPaid().subtract(summ));
			
			System.out.println(con.getContractNumber() + " - " + con.getCustomer().getFullFIO() + " [-" + sum + "] success!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private BigDecimal stornoCP(Contract con, ContractPaymentSchedule ps, BigDecimal summ) throws Exception {
		try {
			BigDecimal payStrn = ps.getPaid();
			if (payStrn.compareTo(summ) > 0) payStrn = summ;
			
			ps.setPaid(ps.getPaid().subtract(payStrn));
			
			if (!GeneralUtil.isEmptyString(ps.getRefkey())) {
				FinDoc fdIR = fdDao.findByRefkey(ps.getRefkey());
				if (fdIR == null) throw new Exception("FinDoc IR with Refkey [" + ps.getRefkey() +"] not found");
				
				fdIR.setWsummPaid(fdIR.getWsummPaid().subtract(payStrn));
				
				List<FinDoc> fdCPL = fdDao.getFdByTypeAndRefkeyMain(fdIR.getRefkey(), FinDocType.TYPE_CP_CONTRACT_PAYMENT);
				BigDecimal fdStAmount = payStrn;
				
				for (FinDoc fdCP:fdCPL) {
					if (fdStAmount.compareTo(BigDecimal.ZERO) > 0) {
						if (!fdCP.getStorno()) {
							BigDecimal sumSt = fdCP.getWsumm();
							if (sumSt.compareTo(fdStAmount) > 0) sumSt = fdStAmount;
							
							if (fdCP.getWsumm().compareTo(sumSt) == 0) {
								fdCP.setStorno(true);
								fdDao.delete(fdCP);
							}
							else {
								fdCP.setWsumm(fdCP.getWsumm().subtract(sumSt));
								BigDecimal ds = fdCP.getWsumm().divide(fdCP.getRate(), 12, RoundingMode.HALF_EVEN);
								fdCP.setDsumm(ds);
								
								fdCP.setWsummPaid(fdCP.getWsumm());
								fdCP.setDsummPaid(fdCP.getDsumm());
								
								for (FinEntry fe:fdCP.getFinEntries()) {
									fe.setWsumm(fdCP.getWsumm());
									fe.setDsumm(fdCP.getDsumm());									
								}		
							}
							
							fdStAmount = fdStAmount.subtract(sumSt);
						}
					} else break;
				}
			}
			
			return payStrn;
		} catch(Exception e) {
			throw e;
		}
	}

	
	// *********************************************************************************************************************
	
	@Override
	public void numeratePaymentOrderForCAWS(Integer brId) throws Exception {
		try {
			List<Contract> conL = conDao.getContractByCidBid(1, brId);
			
			for (Contract con : conL) {
				List<ContractAwardSchedule> cawL = con.getCawSchedules();
				
				cawL.sort(Comparator.comparing(ContractAwardSchedule::getAwardType, (a1, a2) -> {
					return a1.getName().compareTo(a2.getName());
				}).thenComparing(ContractAwardSchedule::getPosition, (p1, p2) -> {
					return p1.getName().compareTo(p2.getName());
				}).thenComparing(ContractAwardSchedule::getDateSchedule));
				
				int pos = 0;
				int porder = 1;
				String awType = AwardType.TYPE_PREMI;
				for (ContractAwardSchedule caw : cawL) {
					if (caw.getPosition().getId() != pos || !caw.getAwardType().getName().equals(awType)) {
						pos = caw.getPosition().getId();
						awType = caw.getAwardType().getName();
						porder = 1;
					}
					caw.setPorder(porder);
					porder++;
				}
			}
			
		} catch (Exception e) {
			throw e;
		}
	}
	
	// *********************************************************************************************************************
		
	@Autowired
	AwardDao awardDao;
	
	@Autowired
	EmployeeDao emplDao;

	@Override
	public void genrateCawsForCoordinator(Integer brId, Integer month, Integer year) throws Exception {
		try {
			List<Contract> conL = conDao.getContractByCidBidMonth(1, brId, month, year);
			
			int pos = Position.POS_COORDINATOR;
			String awType = AwardType.TYPE_PREMI;
			
			for (Contract con : conL) {
				int porder = 1;
				
				if (!existCaw(con, pos)) {
					Award award = null;
					if (con.getIsRent()) {
						award = awardDao.findRentPremiByScopePriority(
								con.getCompany().getId(),
								Department.DEP_MARKETING_AND_SALES, 
								pos, 
								con.getBranch().getId(), 
								null, // City 
								con.getBranch().getCountry().getId(), 
								null, // Region
								con.getInventory().getInvSubCategory().getId(), 
								null, // Inventory
								con.getDateSigned(), 
								con.getMonth());
					} else {
						award = awardDao.findSalesPremiByScopePriority(con.getCompany().getId(), 
								Department.DEP_MARKETING_AND_SALES, 
								pos, con.getBranch().getId(), 
								null, // City
								con.getBranch().getCountry().getId(), 
								null, // Region 
								con.getInventory().getInvSubCategory().getId(), 
								null, // Inventory 
								con.getDateSigned(), con.getMonth());
					}
					
					if (award != null) {
						if (con.getCoordinator() == null) {
							Employee coord = emplDao.getCoordinatorByBranchDep(con.getBranch().getId(), Department.DEP_MARKETING_AND_SALES, con.getDateSigned());
							if (coord != null) con.setCoordinator(coord);
						}
						
						if (con.getCoordinator() != null) {
							List<ContractAwardSchedule> cawL = ConAwSchFactory.constructCawLFor(con, con.getCoordinator(), award, BigDecimal.ZERO);
							for (ContractAwardSchedule caw : cawL) {
								if (caw.getPosition().getId() != pos || !caw.getAwardType().getName().equals(awType)) {
									pos = caw.getPosition().getId();
									awType = caw.getAwardType().getName();
									porder = 1;
								}
								caw.setPorder(porder);
								con.getCawSchedules().add(caw);
								porder++;
							}
						}
						
						conDao.save(con);
					}
					
				}
			}
			
		} catch (Exception e) {
			throw e;
		}
	}

	private boolean existCaw(Contract con, Integer position) {
		for (ContractAwardSchedule caw : con.getCawSchedules()) {
			if (caw.getPosition().getId() == position) return true;
		}
		return false;
	}

	@Override
	public boolean stornoContractPaymentFromFront(ContractHistory conHistory) throws Exception {
		try {
			Contract con = conDao.findOne(conHistory.getContract().getId());
			if (con != null) {
				BigDecimal discount = new BigDecimal(conHistory.getNewValue());
				stornoContractPayment(con.getContractNumber(), discount.doubleValue());
			} else {
				throw new Exception("Contract not found!");
			}
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	public boolean stornoCustomerPayment(ContractHistory conHistory) throws Exception {
		try {
			
				BigDecimal summ = new BigDecimal(conHistory.getNewValue());
			
				/* Get all contract by customerId*/
				Long customerId = Long.parseLong(conHistory.getField());
				List<Contract> conList = conDao.getAllContractsByCustomerId(customerId);
				
				/* Get all paymentDue */
				List<ContractPaymentSchedule> paymentSchedules = new ArrayList();
				if (conList.size() > 0) {
					for (int i=0; i<conList.size(); i++) {
						for (int j=0; j<conList.get(i).getPaymentSchedules().size(); j++) {
							ContractPaymentSchedule conPay = conList.get(i).getPaymentSchedules().get(j);
							if (conPay.getPaid().compareTo(new BigDecimal(0)) > 0) {
								paymentSchedules.add(conPay);
							}
						}
					}
					
				// Sort by reverse paymentDate 	
				paymentSchedules.sort(Comparator.comparing((ContractPaymentSchedule u) -> u.getPaymentDate()).reversed());
					
				
				/* Subtract paymentSum */
				for (int i=0; i<paymentSchedules.size(); i++) {
					ContractPaymentSchedule payment = paymentSchedules.get(i);
					FinDoc finDoc = fdDao.findByRefkey(payment.getRefkey());
					if (summ.compareTo(payment.getPaid())> 0) {
						summ = summ.subtract(payment.getPaid());
						payment.setPaid(new BigDecimal(0));
						if (finDoc != null) {
							finDoc.setWsummPaid(new BigDecimal(0));
							finDoc.setClosed(false);
						}
					} else {
						payment.setPaid(payment.getPaid().subtract(summ));
						summ = new BigDecimal(0);
						if (finDoc != null) {
							finDoc.setWsummPaid(payment.getPaid().subtract(summ));
							finDoc.setClosed(false);
							
						}
						fdDao.save(finDoc);
						break;
					}
					fdDao.save(finDoc);
				}
				
				
				// Set new paid value
				for (int i=0; i<conList.size(); i++) {
					BigDecimal paidSumm = new BigDecimal(0);
					for (int j=0;j<conList.get(i).getPaymentSchedules().size(); j++) {
						ContractPaymentSchedule conPay = conList.get(i).getPaymentSchedules().get(j);
						paidSumm = paidSumm.add(conPay.getPaid());
					}
					conList.get(i).setPaid(paidSumm);
				}
				
				
				
				} else {
					throw new Exception("Contracts not found!");
				}
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
