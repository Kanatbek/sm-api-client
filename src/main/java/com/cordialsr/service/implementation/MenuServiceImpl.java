package com.cordialsr.service.implementation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.MenuDao;
import com.cordialsr.domain.security.Menu;
import com.cordialsr.domain.security.Role;
import com.cordialsr.domain.security.RoleTransactionPermission;
import com.cordialsr.domain.security.User;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.security.repository.UserRepository;
import com.cordialsr.service.MenuService;

@Service
@Transactional
public class MenuServiceImpl implements MenuService {

	@Autowired
	MenuDao menuDao;
	
	@Autowired 
	UserRepository userDao;
	
	@Override
	public Iterable<Menu> getMenuTree() throws Exception {
		try {
			Iterable<Menu> mt = menuDao.getAllTree();			
			return mt;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Iterable<Menu> getMenuTreeByUserId(Long userId) throws Exception {
		try {
			List<Menu> mt = new ArrayList<>();
			if (!GeneralUtil.isEmptyLong(userId)) {
				User user = userDao.findOne(userId);
				if (user != null) {
					List<Menu> level1 = menuDao.getAllByLevel(1);
					for (Menu m: level1) {
						if (m.getChildMenus().size() > 0) {
							List<Menu> mc = getMenuChildTreeByUser(m, user);
							if (mc.size() > 0) m.setChildMenus(mc);
							else m.setChildMenus(new ArrayList<>());
						}
						
						if (m.getEnabled()) {
							if (m.getTransaction() != null && m.getTransaction().getId() > 0) {
								if (userHasAccess(m.getTransaction().getId(), user))
									mt.add(m);
							} else if (m.getChildMenus().size() > 0) {
								mt.add(m);
							}
						}
					}
				}
			}
			return mt;
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public Iterable<Menu> getMenuTreeByUsername(String username) throws Exception {
		try {
			List<Menu> mt = new ArrayList<>();
			if (!GeneralUtil.isEmptyString(username)) {
				User user = userDao.findByUsername(username);
				if (user != null) {
					List<Menu> level1 = menuDao.getAllByLevel(1);
					for (Menu m: level1) {
						if (m.getChildMenus().size() > 0) {
							List<Menu> mc = getMenuChildTreeByUser(m, user);
							if (mc.size() > 0) m.setChildMenus(mc);
							else m.setChildMenus(new ArrayList<>());
						}
						
						if (m.getEnabled()) {
							if (m.getTransaction() != null && m.getTransaction().getId() > 0) {
								if (userHasAccess(m.getTransaction().getId(), user))
									mt.add(m);
							} else if (m.getChildMenus().size() > 0) {
								mt.add(m);
							}
						}
					}
				}
			}
			return mt;
		} catch (Exception e) {
			throw e;
		}
	}
	
	private List<Menu> getMenuChildTreeByUser(Menu menu, User user) throws Exception {
		try {
			List<Menu> childs = menu.getChildMenus();
			List<Menu> mt = new ArrayList<>();
			for (Menu m:childs) {
				if (m.getChildMenus().size() > 0) {
					List<Menu> mc = getMenuChildTreeByUser(m, user);
					if (mc.size() > 0) m.setChildMenus(mc);
					else m.setChildMenus(new ArrayList<>());
				}
				
				if (m.getEnabled()) {
					if (m.getTransaction() != null && m.getTransaction().getId() > 0) {
						if (userHasAccess(m.getTransaction().getId(), user))
							mt.add(m);
					} else if (m.getChildMenus().size() > 0) {
						mt.add(m);
					}
				}
			}			
			return mt;
		} catch (Exception e) {
			throw e;
		}
	}
	
	private boolean userHasAccess(Integer trId, User user) throws Exception {
		try {
			boolean hasAccess = false;
			for (Role r: user.getUserRoles()) {
				for (RoleTransactionPermission rtp: r.getRoleTransactionPermissions()) {
					if (rtp.getTransaction().getId() == trId 
							&& rtp.getPermission() > 0) {
						hasAccess = true;
						break;
					}
				}
				if (hasAccess) break;
			}
			return hasAccess; 
		} catch (Exception e) {
			throw e;
		}
	}

}
