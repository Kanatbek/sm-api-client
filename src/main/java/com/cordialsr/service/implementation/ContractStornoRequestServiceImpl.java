package com.cordialsr.service.implementation;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.ContractDao;
import com.cordialsr.Dao.ContractStornoRequestDao;
import com.cordialsr.Dao.FinDocDao;
import com.cordialsr.Dao.FinEntryDao;
import com.cordialsr.Dao.PayrollDao;
import com.cordialsr.Dao.SmFilterChangeDao;
import com.cordialsr.Dao.StockDao;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractAwardSchedule;
import com.cordialsr.domain.ContractStornoRequest;
import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.FinEntry;
import com.cordialsr.domain.Payroll;
import com.cordialsr.domain.SmFilterChange;
import com.cordialsr.domain.StockIn;
import com.cordialsr.domain.StockStatus;
import com.cordialsr.domain.nogenerator.ContractStornoRequestGenerator;
import com.cordialsr.domain.validator.ContractStornoRequestValidator;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.ContractService;
import com.cordialsr.service.ContractStornoRequestService;
import com.cordialsr.service.InvoiceService;

@Service
@Transactional(rollbackFor = Exception.class)
public class ContractStornoRequestServiceImpl implements ContractStornoRequestService {

	@Autowired
	ContractStornoRequestDao reqDao;
	
	@Autowired
	ContractService contractService;
		
	@Autowired
	SmFilterChangeDao filterChangeDao;
	
	@Autowired
	StockDao stockDao;
	
	@Autowired
	FinDocDao finDocDao;
	
	@Autowired
	FinEntryDao finEntryDao;
	
	@Autowired
	PayrollDao payrollDao;
	
	@Autowired
	ContractDao contractDao;
	
	
	@Override
	public ContractStornoRequest saveNewContractStornoRequest(ContractStornoRequest newContractStornoRequest) throws Exception {
		try {
			Contract contract = contractService.getOneContract(newContractStornoRequest.getContract().getId());
			newContractStornoRequest.setContract(contract);
			newContractStornoRequest.setReqDate(Calendar.getInstance());
			
			ContractStornoRequestValidator.validateBasic(newContractStornoRequest);
			reqDao.save(newContractStornoRequest);
			String conNo = ContractStornoRequestGenerator.generateRequestNo(newContractStornoRequest);
			newContractStornoRequest.setReqNumber(conNo);
			reqDao.save(newContractStornoRequest);
			return newContractStornoRequest;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// ***************************************************************************************************************************
	
	@Autowired
	InvoiceService invoiceService;
	
	@Override
	public ContractStornoRequest updateContractStornoRequest(ContractStornoRequest conRequest) throws Exception {
		try {
			Contract contract = contractService.getOneContract(conRequest.getContract().getId());
			conRequest.setContract(contract);
			conRequest.setReqDate(Calendar.getInstance());
			
			ContractStornoRequestValidator.postValidate(conRequest);
			
			if (conRequest.getStatus() == ContractStornoRequest.STATUS_APPROVED) {
				if (invoiceService.checkCustomerHasStock(conRequest.getContract().getContractNumber()) == false) {
					contractService.cancelContract(conRequest);
				} else {
					throw new Exception("Выполните сперва возврат товаров!");
				}
			}
			conRequest.setGrantDate(Calendar.getInstance());
			return reqDao.save(conRequest);
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	@Override
	public Contract cancelContractStorno(Long conId) throws Exception {
		Contract contract = null;
		try {
			contract = contractService.getOneContract(conId);
			
			restoreFilterChangeStatus(conId);
			changeContractStornoRequest(conId);
			returnContractItem(contract.getInventorySn());
			restoreFinDoc(conId, contract.getRefkey());
			deleteStornoPayroll(contract);
			restoreContractStornoStatus(contract);
			return contract;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	public void restoreContractStornoStatus(Contract contract) throws Exception{
		try {
			contract.setStorno(false);
			contractDao.save(contract);
//			contractService.saveContract(contract);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public void deleteStornoPayroll(Contract contract) throws Exception {
		try {
			List<Payroll> payrolls = payrollDao.findAllByContract(contract);
			for (int i=0; i<payrolls.size(); i++) {
				if (payrolls.get(i).getSum().compareTo(BigDecimal.ZERO) < 0) {
					if (!GeneralUtil.isEmptyString(payrolls.get(i).getRefkey())) {
						deleteStornoPayrolFinDocFinEntry(payrolls.get(i).getRefkey());
					}
					restoreCawRevertSum(payrolls.get(i));
					payrollDao.delete(payrolls.get(i));
				}
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	private void restoreCawRevertSum(Payroll payroll) throws Exception {
		try {
			ContractAwardSchedule caw = payroll.getConAward();
			if (caw != null) {
				caw.setDeduction(caw.getDeduction().subtract(payroll.getSum()));
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	public void deleteStornoPayrolFinDocFinEntry(String refkey) throws Exception {
		try {
			FinDoc finDoc = finDocDao.findByRefkey(refkey);
			if (finDoc != null) {
				finDocDao.delete(finDoc);	
			} else {
				System.out.println("Hmmmm ... no FinDoc for Payroll refkey " + refkey);
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	public void restoreFilterChangeStatus(Long conId) throws Exception {
		try {
			SmFilterChange filterChange = filterChangeDao.findByContractId(conId);
			if (filterChange != null) {
				filterChange.setEnabled(true);
				filterChangeDao.save(filterChange);
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	public void changeContractStornoRequest(Long conId) throws Exception {
		try {
			ContractStornoRequest conStornoRequest = reqDao.findByContractId(conId);
			if (conStornoRequest != null) {
				conStornoRequest.setStatus(ContractStornoRequest.STATUS_CANCEL_NEW);
				reqDao.save(conStornoRequest);
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	public void returnContractItem(String inventorySn) throws Exception {
		try {
			StockIn stockIn = stockDao.findByJustSerialNumber(inventorySn);
			if (stockIn != null) {
				StockStatus stStatus = new StockStatus(StockStatus.STATUS_INT_RESERVED);
				stockIn.setIntStatus(stStatus);
			} else {
				throw new Exception("inventory is not in stockIn");
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	public void restoreFinDoc(Long contractId, String refkey) throws Exception {
		try {
			List<FinDoc> finDocs = finDocDao.getStornoContractFinDocs(contractId, refkey);
			for(int i=0; i<finDocs.size(); i++) {
				finDocs.get(i).setStorno(false);
				finDocs.get(i).setStDocno(null);
				finDocs.get(i).setStYear(null);
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
}
