package com.cordialsr.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.PartyExperienceDao;
import com.cordialsr.domain.PartyExperience;
import com.cordialsr.service.PartyExperienceService;


@Service
@Transactional
public class PartyExperienceServiceImpl implements PartyExperienceService{

	@Autowired
	PartyExperienceDao expDao;
	
	@Override
	public PartyExperience saveNewExperience(PartyExperience newExperience) throws Exception {
		try {
//			Validator
			expDao.save(newExperience);
			return newExperience;
			
		} catch (Exception e) {
			throw e;
		}
	}
	
}
