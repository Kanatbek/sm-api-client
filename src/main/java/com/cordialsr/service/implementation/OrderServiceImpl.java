package com.cordialsr.service.implementation;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.OrderDao;
import com.cordialsr.Dao.OrderItemDao;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.InvoiceItem;
import com.cordialsr.domain.InvoiceStatus;
import com.cordialsr.domain.OrderHeader;
import com.cordialsr.domain.OrderItem;
import com.cordialsr.domain.nogenerator.OrderNoGenerator;
import com.cordialsr.domain.validator.OrderValidator;
import com.cordialsr.service.OrderService;

@Service
@Transactional(rollbackFor = Exception.class)
public class OrderServiceImpl implements OrderService{

	@Autowired
	OrderDao orderDao;
	
	@Autowired
	OrderItemDao orderItemDao;
	
	@Override
	public Boolean saveNewOrders(OrderHeader ordHead) throws Exception {
		try {
			
			OrderValidator.validateBasic(ordHead);
			
			if (ordHead.getId() == null) {
				ordHead.setDateCreated(Calendar.getInstance().getTime());
			} else {
				ordHead.setDateUpdated(new Date());
			}
			
			orderDao.save(ordHead);
		
			if (ordHead.getOrderNumber() == null || ordHead.getOrderNumber().length() == 0) {
				String invNo = OrderNoGenerator.generateOrderNo(ordHead);
				ordHead.setOrderNumber(invNo);
				ordHead.setDateCreated(Calendar.getInstance().getTime());
				orderDao.save(ordHead);
			}
			
			return true;
		} catch (Exception ex) {
			throw ex;
		}
	}
	
	@Override
	public void deleateFromOrder(Invoice invoice) throws Exception{
		try {
			for (InvoiceItem it: invoice.getInvoiceItems()) {
				
				BigDecimal count = it.getQuantity();
				
				OrderValidator.validateForDeleteOrder(invoice);
				
				Iterable<OrderHeader> orderHead = orderDao.findByInventory(invoice.getCompany().getId(),invoice.getBranchImporter().getId());
				
				for (OrderHeader oh: orderHead) {
					
					for (OrderItem oi: oh.getOrderItems()) {
						
						if (oi.getInventory().getId().equals(it.getInventory().getId())) {
							
							if (oi.getQuantity().compareTo(count) == -1 || oi.getQuantity().compareTo(count) == 0) {
								count = count.subtract(oi.getQuantity());
								oh.getOrderItems().remove(oi);
								oh.setOrderStatus(new InvoiceStatus(InvoiceStatus.STATUS_PROCESS));
								orderDao.save(oh);
								if (oh.getOrderItems().size() == 0) {
									orderDao.delete(oh);
								}
							} else {
								
								oi.setQuantity(oi.getQuantity().subtract(count));
								count = new BigDecimal(0);
								oh.setOrderStatus(new InvoiceStatus(InvoiceStatus.STATUS_PROCESS));
								orderDao.save(oh);
							}
							break;
						}
						if (count.equals(new BigDecimal(0))) {
							break;
						}
					}
			
					if (count.equals(new BigDecimal(0))) {
						break;
					}
				}
				
			}
			
		} catch (Exception e) {
			throw e;
		}
		
	}

}
