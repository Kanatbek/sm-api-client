package com.cordialsr.service.implementation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.BranchDao;
import com.cordialsr.Dao.CompanyDao;
import com.cordialsr.Dao.CurrencyDao;
import com.cordialsr.Dao.IncotermDao;
import com.cordialsr.Dao.InventoryDao;
import com.cordialsr.Dao.InvoiceDao;
import com.cordialsr.Dao.InvoiceItemDao;
import com.cordialsr.Dao.PartyDao;
import com.cordialsr.Dao.StockDao;
import com.cordialsr.Dao.StockOutDao;
import com.cordialsr.Dao.UnitDao;
import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractItem;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.Incoterm;
import com.cordialsr.domain.InvItemSn;
import com.cordialsr.domain.InvMainCategory;
import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.InvoiceItem;
import com.cordialsr.domain.InvoiceStatus;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.ScopeType;
import com.cordialsr.domain.SmService;
import com.cordialsr.domain.SmServiceItem;
import com.cordialsr.domain.StockIn;
import com.cordialsr.domain.StockOut;
import com.cordialsr.domain.StockStatus;
import com.cordialsr.domain.Unit;
import com.cordialsr.domain.factory.InvoiceFactory;
import com.cordialsr.domain.nogenerator.InvoiceNoGenerator;
import com.cordialsr.domain.validator.InvoiceValidator;
import com.cordialsr.domain.validator.StockValidator;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.ContractService;
import com.cordialsr.service.FinDocService;
import com.cordialsr.service.InvoiceService;
import com.cordialsr.service.OrderService;
import com.cordialsr.service.StockService;

@Service
@Transactional(rollbackFor = Exception.class)
public class InvoiceServiceImpl implements InvoiceService {

	@Autowired
	InvoiceDao invDao;

	@Autowired
	CompanyDao comDao;

	@Autowired
	PartyDao ptDao;

	@Autowired
	InventoryDao iDao;

	@Autowired
	BranchDao braDao;

	@Autowired
	CurrencyDao curDao;

	@Autowired
	IncotermDao incDao;

	@Autowired
	UnitDao uniDao;

	@Autowired
	InvoiceItemDao invoiceItemDao;

	@Autowired
	StockDao stockDao;

	@Autowired
	StockOutDao soutDao;

	@Autowired
	FinDocService finDocService;
	
	@Autowired
	OrderService orderService;
	
	@Autowired
	StockService stockService;
	
	@Autowired
	ContractService contractService;

	//////////////// Before save get by Type /////////////////////////////////

	@Override
	public Invoice invoiceToType(Invoice invoice, String trCode, Long userId) throws Exception {
		try {
			if (invoice.getInvoiceType().equals(Invoice.INVOICE_TYPE_ACCOUNTABLE)) {
				invoice = constructInvoiceAccountable(invoice);
			} else if (invoice.getInvoiceType().equals(Invoice.INVOICE_TYPE_RENT)
					|| invoice.getInvoiceType().equals(Invoice.INVOICE_TYPE_SELL)) {
				invoice = constructInvoiceSellRent(invoice, trCode, userId);
			} else if (invoice.getInvoiceType().equals(Invoice.INVOICE_TYPE_WRITEOFF)) {
				invoice = constructInvoiceWriteOff(invoice);
			} else if (invoice.getInvoiceType().equals(Invoice.INVOICE_TYPE_IMPORT)) {
				invoice = newInvoiceImport(invoice);
			} else if (invoice.getInvoiceType().equals(Invoice.INVOICE_TYPE_TRANSFER)) {
				invoice = newInvoiceTransfer(invoice);
			} else if (invoice.getInvoiceType().equals(Invoice.INVOICE_TYPE_RETURN)) {
				invoice = constructInvoiceReturn(invoice, trCode, userId);
			} else if (invoice.getInvoiceType().equals(Invoice.INVOICE_TYPE_SERVICE)) {
				invoice = constructInvoiceService(invoice);
			}
			return invoice;
		} catch (Exception ex) {
			throw ex;
		}
	}
	
	/////////////////////////// Save Full Invoice//////////////////////////////////
	@Override
	public Invoice saveInvoice(Invoice invoice) throws Exception {
		try {
			InvoiceValidator.validateBasic(invoice);
			invoice.setStorno(false);
			if (invoice.getId() == null) {
				invoice.setDateCreated(Calendar.getInstance());
			} else {
				invoice.setDateUpdated(Calendar.getInstance());
			}

			invoice = invDao.save(invoice);

			if (invoice.getInvoiceNumber() == null || invoice.getInvoiceNumber().length() == 0) {
				String invNo = InvoiceNoGenerator.generateInvoiceNo(invoice);
				invoice.setInvoiceNumber(invNo);
				invoice.setDateCreated(Calendar.getInstance());
				invDao.save(invoice);
			}

			return invoice;
		} catch (Exception ex) {
			throw ex;
		}
	}
	
	//////////////////////////////////////// Save Just Invoice //////////////////////////////////////////////////////////
	
	private Invoice saveJustInvoice(Invoice invoice) throws Exception {
		try {
			
			Invoice newInv = invoice.clone();
			newInv.setInvoiceItems(new ArrayList<>());
			for (InvoiceItem ii: invoice.getInvoiceItems()) {
				InvoiceItem ic = ii.clone();
				ic.setInvoice(newInv);
				ic.setChildStockIns(new ArrayList<StockIn>());
				ic.setChildInvSns(new ArrayList<InvItemSn>());
				ic.setQuantity(new BigDecimal(0));
				newInv.getInvoiceItems().add(ic);
			}	
			newInv = saveInvoice(newInv);
			return newInv;
		} catch(Exception ex) {
			throw ex;
		}
	}
	
////////////////////////////////////////////////////////////////// Return invoices ..............................	
	
	@Override
	public Invoice returnInvoice(Invoice invoice) throws Exception {
		try {
			if (invoice.getInvoiceType() == Invoice.INVOICE_TYPE_SERVICE) {
				invoice = invDao.findOne(invoice.getId());
				Invoice newInv = InvoiceFactory.constructReturnInvoiceService(invoice);
				saveInvoice(newInv);
				invoice = stockService.returnInvoiceService(invoice, newInv);
				saveInvoice(invoice);
			}
			return invoice;
		} catch(Exception ex) {
			throw ex;
		}
	}
	//////////////////////////////////////////////////////
  private Invoice constructInvoiceService(Invoice invoice) throws Exception {
		try {
			Invoice newInv = invDao.findinv(invoice.getId());
			invoice.setParty(newInv.getParty());
			
			invoice = writeOffInvoiceStock(invoice);
			
			newInv.getInvoiceItems().clear();
			newInv.getInvoiceItems().addAll(invoice.getInvoiceItems());
			newInv.setInvoiceStatus(new InvoiceStatus(InvoiceStatus.STATUS_CLOSED));
			newInv.setUserUpdated(invoice.getUserUpdated());
			
			saveInvoice(newInv);
			return newInv;
			
		}catch(Exception ex) {
			throw ex;
		}
	}
  
  /////////////////////////////////////////// Write off stocks ..................
  
  private Invoice writeOffInvoiceStock(Invoice invoice) throws Exception{
	  try {
		  for (InvoiceItem ii: invoice.getInvoiceItems()) {

	  		ii.setChildStockOuts(new ArrayList<StockOut>());
	  		ii.setChildInvSns(new ArrayList<InvItemSn>());
			for (StockIn sk: ii.getChildStockIns()) {
				
				if (!GeneralUtil.isEmptyString(sk.getSerialNumber())) {
					InvItemSn invItemSn = new InvItemSn(ii, sk.getSerialNumber());
					ii.getChildInvSns().add(invItemSn);
				}
				
				if (sk.getId() == null) {
					continue;
				}
				StockIn stock = stockDao.findOne(sk.getId());
				if (sk.getDsumm().compareTo(stock.getQuantity()) == 1) {
					throw new Exception("Into instore less items of stocks:" + sk.getId());
				}
				if (sk.getQuantity().equals(sk.getDsumm())) {
					stockDao.delete(stock);
					StockOut so = new StockOut();
					BeanUtils.copyProperties(stock, so);
					so.setId(null);
					so.setDateOut(invoice.getDateUpdated().getTime());
					so.setInvoiceItem(ii);
					so.setGenStatus(new StockStatus(StockStatus.STATUS_GEN_SOLD));
					ii.getChildStockOuts().add(so);
				}
				else {
					stock.setQuantity(stock.getQuantity().subtract(sk.getDsumm()));
					stockDao.save(stock);
					StockOut so = new StockOut();
					BeanUtils.copyProperties(stock, so);
					so.setId(null);
					so.setQuantity(sk.getDsumm());
					so.setDateOut(invoice.getDateUpdated().getTime());
					so.setInvoiceItem(ii);
					so.setGenStatus(new StockStatus(StockStatus.STATUS_GEN_SOLD));
					ii.getChildStockOuts().add(so);
				}
			}
			ii.setChildStockIns(new ArrayList<StockIn>());
		  }
		  return invoice;
	  }catch (Exception ex) {
		  throw ex;
	  }
  }
////////////////////////// Invoice Return ******************
	
	private Invoice constructInvoiceReturn(Invoice invoice, String trCode, Long userId) throws Exception {
		try {
			
			if (StockValidator.validateStockForReturnType(invoice)) {
				invoice = newInvoiceReturnAccountable(invoice);
			} else {
				invoice = newInvoiceReturnContract(invoice, trCode, userId);
			}
			
			invoice = saveInvoice(invoice);
			return invoice;
			
		} catch (Exception ex) {
			throw ex;
		}
	}

	
	//////////////// new Invoice Return Accountable
	private Invoice newInvoiceReturnAccountable(Invoice invoice)throws Exception {
		try {
			Invoice newInv = invoice.clone();
			newInv.setInvoiceItems(new ArrayList<>());
			for (InvoiceItem ii: invoice.getInvoiceItems()) {
				InvoiceItem ic = ii.clone();
				ic.setInvoice(newInv);
				ic.setChildStockIns(new ArrayList<StockIn>());
				ic.setChildInvSns(new ArrayList<InvItemSn>());
				ic.setQuantity(new BigDecimal(0));
				newInv.getInvoiceItems().add(ic);
			}	
			newInv = saveInvoice(newInv);
			
			// Bidirectional consistency problem occured
			
			for (InvoiceItem invItem: newInv.getInvoiceItems()) {
				for (InvoiceItem ii: invoice.getInvoiceItems()) {
					if (ii.getInventory().getId() == invItem.getInventory().getId()) {
						for (StockIn stock: ii.getChildStockIns()) {
							
							if (stock.getSerialNumber().length() > 0 && !stock.getSerialNumber().equals("null")) {
								
								StockIn sk = stockDao.findByJustSerialNumber(stock.getSerialNumber());	
								if (sk == null) {
									throw new Exception("Stock is empty");
								}
								
								sk.setIntStatus(new StockStatus(StockStatus.STATUS_INT_IN));
								sk.setInvoiceItem(invItem);
								invItem.getChildStockIns().add(sk);
								invItem.setQuantity(invItem.getQuantity().add(sk.getQuantity()));
								
								InvItemSn invItemSn = new InvItemSn(invItem, sk.getSerialNumber());
								invItem.getChildInvSns().add(invItemSn);
							} else {
							
								StockIn sk = stockDao.findOne(stock.getId());
								if (sk == null) {
									throw new Exception("Stock is empty");
								}
								StockValidator.validateStockAccountable(sk);
								StockValidator.validateStockForQuantityEquals(stock, sk);
								
								if (sk.getQuantity().compareTo(stock.getDsumm()) == 0) {
									sk.setIntStatus(new StockStatus(StockStatus.STATUS_INT_IN));
									sk.setInvoiceItem(invItem);
									invItem.getChildStockIns().add(sk);
									invItem.setQuantity(invItem.getQuantity().add(sk.getQuantity()));
								} else {
									
									sk.setQuantity(sk.getQuantity().subtract(stock.getDsumm()));
									stockDao.save(sk);
									
									StockIn stc = sk.clone();
									stc.setId(null);
									stc.setQuantity(stock.getDsumm());
									stc.setIntStatus(new StockStatus(StockStatus.STATUS_INT_IN));
									stc.setInvoiceItem(invItem);
									invItem.getChildStockIns().add(stc);
									invItem.setQuantity(invItem.getQuantity().add(stock.getDsumm()));
								}
							}
							
						}
					}
				}					
			}
			
			return newInv;
			
		} catch(Exception ex) {
			throw ex;
		}
	}
	
	////////////////////// new Invoice Return Contract
	
	private Invoice newInvoiceReturnContract(Invoice invoice, String trCode, Long userId)throws Exception {
		try {
			ImmutablePair<Invoice, List<StockIn>> res = createInvoiceReturn(invoice);
			Invoice newInv = res.getLeft();
			List<StockIn> siL = res.getRight();
			
			if (invoice.getInvoiceType() == Invoice.INVOICE_TYPE_SELL) {
				FinDoc fd = finDocService.returnContractGoodsFromInvoice(newInv, siL, trCode, userId);
				if (fd != null) {
					newInv.setRefkey(fd.getRefkey());
					newInv.setDateUpdated(Calendar.getInstance());
					newInv = invDao.save(newInv);
				}
			}
			
			Invoice oldInv = invDao.findOne(invoice.getId());
			if (oldInv != null) {
				oldInv.setStorno(true);
			}
			newInv = saveInvoice(newInv);
			return newInv;
		} catch(Exception ex) {
			throw ex;
		}
	}
	
	private ImmutablePair<Invoice, List<StockIn>> createInvoiceReturn(Invoice invoice) throws Exception {
		try {
			Invoice newInv = new Invoice();
			newInv.setCompany(invoice.getCompany());
			newInv.setBranch(invoice.getBranch());
			newInv.setBranchImporter(invoice.getBranchImporter());
			newInv.setData(invoice.getData());
			newInv.setDateCreated(Calendar.getInstance());
			newInv.setInvoiceType(invoice.getInvoiceType());
			newInv.setParty(invoice.getParty());
			newInv.setScopeType(invoice.getScopeType());
			newInv.setUserAdded(invoice.getUserAdded());
			newInv.setDocno(invoice.getDocno());
			newInv.setDocId(invoice.getDocId());
			
			newInv.setInvoiceStatus(new InvoiceStatus(InvoiceStatus.STATUS_CLOSED));
			newInv.setInvoiceItems(new ArrayList<InvoiceItem>());
			List<StockIn> siL = new ArrayList<>();
			
			for (InvoiceItem ii: invoice.getInvoiceItems()) {
				InvoiceItem invItem = new InvoiceItem();
				invItem = ii.clone();
				invItem.setInvoice(newInv);
				invItem.setId(null);
				invItem.setChildStockIns(new ArrayList<StockIn>());
				invItem.setChildStockOuts(null);
				invItem.setChildInvSns(new ArrayList<InvItemSn>());
				
				for (StockOut so: ii.getChildStockOuts()) {
					StockOut origStock = soutDao.findOne(so.getId());
					StockValidator.validateStockOutForEquals(origStock, so);
					StockIn si = new StockIn();
					BeanUtils.copyProperties(origStock, si);
					si.setId(null);
					si.setIntStatus(new StockStatus(StockStatus.STATUS_INT_IN));
					si.setGenStatus(new StockStatus(StockStatus.STATUS_GEN_RETURNED));
					si.setDateIn(Calendar.getInstance().getTime());
					si.setInvoiceItem(invItem);
					
					if (origStock.getQuantity().compareTo(so.getDsumm()) == 0) {
						soutDao.delete(origStock);
						invItem.getChildStockIns().add(si);
					} else {
						origStock.setQuantity(origStock.getQuantity().subtract(so.getDsumm()));
						soutDao.save(origStock);
						si.setQuantity(so.getDsumm());
						invItem.getChildStockIns().add(si);
					}
					
					if (!GeneralUtil.isEmptyString(si.getSerialNumber())) {
						InvItemSn iSn = new InvItemSn(invItem, si.getSerialNumber());
						invItem.getChildInvSns().add(iSn);
					}
					
					siL.add(si);
				}
				newInv.getInvoiceItems().add(invItem);
			}
			return new ImmutablePair<>(newInv, siL);
		} catch (Exception ex) {
			throw ex;
		}
	}
	
	private Invoice newInvoiceTransfer(Invoice invoice) throws Exception {
		try {
			Company company = comDao.findOne(invoice.getCompany().getId());
			invoice.setParty(company.getParty());
			for (InvoiceItem ii: invoice.getInvoiceItems()) {
				ii.setChildStockIns(null);
			}
			invoice = saveInvoice(invoice);
			
			return invoice;
		} catch (Exception e) {
			throw e;
		}
	}


	private Invoice newInvoiceImport(Invoice invoice) throws Exception {
		try {
			invoice.setPaid(new BigDecimal(0));
			
			orderService.deleateFromOrder(invoice);
			
			invoice = saveInvoice(invoice);
			return invoice;
		} catch (Exception e) {
			throw e;
		}
	}

	//////////////////////Create Invoice Write-Off
	
	private Invoice constructInvoiceWriteOff(Invoice invoice) throws Exception {
		try {
			Invoice newInv = saveJustInvoice(invoice);
				 
			for (InvoiceItem invItem: newInv.getInvoiceItems()) {
				for (InvoiceItem ii: invoice.getInvoiceItems()) {
					if (ii.getInventory().getId() == invItem.getInventory().getId()) {
						for (StockIn stock: ii.getChildStockIns()) {
							
							if (stock.getSerialNumber().length() > 0 && !stock.getSerialNumber().equals("null")) {
								StockIn sk = stockDao.findByJustSerialNumber(stock.getSerialNumber());	
								if (sk == null) {
									throw new Exception("Stock is empty");
								}
								stockDao.delete(sk);
								StockOut so = new StockOut();
								BeanUtils.copyProperties(sk, so);
								so.setId(null);
								so.setDateOut(newInv.getDateCreated().getTime());
								so.setInvoiceItem(invItem);
								so.setGenStatus(new StockStatus(StockStatus.STATUS_GEN_WASTED));
								invItem.getChildStockOuts().add(so);
								invItem.setQuantity(invItem.getQuantity().add(so.getQuantity()));
								
								InvItemSn invItemSn = new InvItemSn(invItem, so.getSerialNumber());
								invItem.getChildInvSns().add(invItemSn);
							} else {
							
								StockIn sk = stockDao.findOne(stock.getId());
								if (sk == null) {
									throw new Exception("Stock is empty");
								}
								if (StockValidator.validateStockIn(sk) && StockValidator.validateStockForQuantityEquals(stock, sk)) {;
									
									if (sk.getQuantity().compareTo(stock.getDsumm()) == 0) {
										
										stockDao.delete(sk);
										StockOut so = new StockOut();
										BeanUtils.copyProperties(sk, so);
										so.setId(null);
										so.setDateOut(newInv.getDateCreated().getTime());
										so.setInvoiceItem(invItem);
										so.setGenStatus(new StockStatus(StockStatus.STATUS_GEN_WASTED));
										invItem.getChildStockOuts().add(so);
										invItem.setQuantity(invItem.getQuantity().add(so.getQuantity()));
										
									} else {
										
										sk.setQuantity(sk.getQuantity().subtract(stock.getDsumm()));
										
										StockOut so = new StockOut();
										BeanUtils.copyProperties(sk, so);
										so.setId(null);
										so.setDateOut(newInv.getDateCreated().getTime());
										so.setInvoiceItem(invItem);
										so.setQuantity(stock.getDsumm());
										so.setGenStatus(new StockStatus(StockStatus.STATUS_GEN_WASTED));
										invItem.getChildStockOuts().add(so);
										invItem.setQuantity(invItem.getQuantity().add(so.getQuantity()));
									}
								}
							}
							
						}
					}
				}					
			}
			saveInvoice(newInv);
			return newInv;
		} catch (Exception ex) {
			throw ex;
		}
	}

	////////////////////// Updating Invoice Contract ///////////////////

	private Invoice constructInvoiceSellRent(Invoice invoice, String trCode, Long userId) throws Exception {
		try {
			Invoice newInv = new Invoice();
			if (invoice.getInvoiceStatus().getName().equals(InvoiceStatus.STATUS_PROCESS)) {
				newInv = updateInvoiceSellRent(invoice);
				
			} else if (invoice.getInvoiceStatus().getName().equals(InvoiceStatus.STATUS_CLOSED)) {
	
				ImmutablePair<Invoice, ArrayList<StockOut>> result = doWriteOffSellRent(invoice);
				
				newInv = result.getLeft();
				ArrayList<StockOut> stockOutList = result.getRight();
				
				for (StockOut so: stockOutList) {
					contractService.updateWriteOffDate(newInv.getDocno(), so.getSerialNumber(), so.getInventory().getId(), so.getDateOut());
				}
	
				FinDoc fd = finDocService.writeOffContractGoodsFromInvoice(newInv, stockOutList, trCode, userId);
				if (fd != null) {
					newInv.setRefkey(fd.getRefkey());
					newInv.setDateUpdated(Calendar.getInstance());
					newInv = invDao.save(newInv);
				}
			}
			
			return newInv;
		} catch (Exception ex) {
			throw ex;
		}
	}

	private ImmutablePair<Invoice, ArrayList<StockOut>> doWriteOffSellRent(Invoice invoice) throws Exception {
		try {
			ArrayList<StockOut> sout = new ArrayList<>();

			Invoice newInv = invDao.findOne(invoice.getId());
			newInv.setInvoiceType(invoice.getInvoiceType());
			newInv.setDateUpdated(Calendar.getInstance());
			newInv.setUserUpdated(invoice.getUserUpdated());
			newInv.setInvoiceStatus(new InvoiceStatus(InvoiceStatus.STATUS_CLOSED));

			for (InvoiceItem ii : newInv.getInvoiceItems()) {

				Integer count = 0;
				for (StockIn stock : ii.getChildStockIns()) {
					count = count + stock.getQuantity().intValue();
					StockOut sk = new StockOut();
					BeanUtils.copyProperties(stock, sk);
					sk.setDateOut(invoice.getDateUpdated().getTime());
					sk.setInvoiceItem(ii);
					sk.setBroken(false);
					if (invoice.getInvoiceType().equals(Invoice.INVOICE_TYPE_RENT)) {
						sk.setGenStatus(new StockStatus(StockStatus.STATUS_GEN_RENT));
					} else if (invoice.getInvoiceType().equals(Invoice.INVOICE_TYPE_SELL)) {
						sk.setGenStatus(new StockStatus(StockStatus.STATUS_GEN_SOLD));
					}
					stockDao.delete(stock.getId());
					soutDao.save(sk);
					sout.add(sk);

				}

				for (StockOut stock : ii.getChildStockOuts()) {
					count = count + stock.getQuantity().intValue();
				}
				ii.getChildStockIns().clear();
							
				if (count != ii.getQuantity().intValue()) {
					newInv.setInvoiceStatus(new InvoiceStatus(InvoiceStatus.STATUS_PROCESS));
				}
			}
			newInv = invDao.save(newInv);
			return new ImmutablePair<Invoice, ArrayList<StockOut>>(newInv, sout);
		} catch (Exception e) {
			throw e;
		}
	}

	private Invoice updateInvoiceSellRent(Invoice invoice) throws Exception {
		try {
			Invoice newInv = invDao.findOne(invoice.getId());
			Integer count = 0;

			for (InvoiceItem ii : invoice.getInvoiceItems()) {

				count = 0;
				for (InvoiceItem it : newInv.getInvoiceItems()) {
					if (it.getInventory().getId().compareTo(ii.getInventory().getId()) == 0) {
						break;
					}
					count++;
				}

				newInv.getInvoiceItems().get(count).getChildInvSns().clear();
				
				for (StockIn stock : ii.getChildStockIns()) {

					if (!stock.getIntStatus().getId().equals(StockStatus.STATUS_INT_RESERVED)) {

						StockIn sk = stockDao.findOne(stock.getId());

						//////////////// Stock's Quantity equal 1 or useful quantity equal stock's
						//////////////// quantity
						if (sk.getQuantity().compareTo(new BigDecimal(1)) == 0
								|| sk.getQuantity().equals(stock.getDsumm())) {

							sk.setInvoiceItem(newInv.getInvoiceItems().get(count));
							sk.setIntStatus(new StockStatus(StockStatus.STATUS_INT_RESERVED));
							newInv.getInvoiceItems().get(count).getChildStockIns().add(sk);

						} else {
							/////////////// Stock's Quantity more than We need
							sk.setQuantity(sk.getQuantity().subtract(stock.getDsumm()));
							stockDao.save(sk);

							stock.setQuantity(stock.getDsumm());
							stock.setDsumm(sk.getDsumm());
							stock.setIntStatus(new StockStatus(StockStatus.STATUS_INT_RESERVED));
							stock.setInvoiceItem(newInv.getInvoiceItems().get(count));
							newInv.getInvoiceItems().get(count).getChildStockIns().add(stock);

						}
					}
				}

			}
			
			newInv = saveInvoice(newInv);
			return newInv;
		} catch (Exception e) {
			throw e;
		}
	}



	/////////////////////////// Create Accountable Invoice
	/////////////////////////// ////////////////////////////////////
	private Invoice constructInvoiceAccountable(Invoice invoice) throws Exception {
		try {
			
			Invoice newInv = invoice.clone();
			newInv.setInvoiceItems(new ArrayList<>());
			for (InvoiceItem ii: invoice.getInvoiceItems()) {
				InvoiceItem ic = ii.clone();
				ic.setInvoice(newInv);
				ic.setChildStockIns(new ArrayList<StockIn>());
				ic.setChildInvSns(new ArrayList<InvItemSn>());
				ic.setQuantity(new BigDecimal(0));
				newInv.getInvoiceItems().add(ic);
			}	
			newInv = saveInvoice(newInv);
			
			for (InvoiceItem invItem: newInv.getInvoiceItems()) {
				for (InvoiceItem ii: invoice.getInvoiceItems()) {
					if (ii.getInventory().getId() == invItem.getInventory().getId()) {
						for (StockIn stock: ii.getChildStockIns()) {
							
							if (stock.getSerialNumber().length() > 0 && !stock.getSerialNumber().equals("null")) {
								StockIn sk = stockDao.findByJustSerialNumber(stock.getSerialNumber());	
								if (sk == null) {
									throw new Exception("Stock is empty");
								}
								
								sk.setIntStatus(new StockStatus(StockStatus.STATUS_INT_ACCOUNTABLE));
								sk.setInvoiceItem(invItem);
								invItem.getChildStockIns().add(sk);
								invItem.setQuantity(invItem.getQuantity().add(sk.getQuantity()));
								
								InvItemSn invItemSn = new InvItemSn(invItem, sk.getSerialNumber());
								invItem.getChildInvSns().add(invItemSn);
							} else {
							
								StockIn sk = stockDao.findOne(stock.getId());
								if (sk == null) {
									throw new Exception("Stock is empty");
								}
								if (StockValidator.validateStockIn(sk) && StockValidator.validateStockForQuantityEquals(stock, sk)) {;
									
									if (sk.getQuantity().compareTo(stock.getDsumm()) == 0) {
										sk.setIntStatus(new StockStatus(StockStatus.STATUS_INT_ACCOUNTABLE));
										sk.setInvoiceItem(invItem);
										invItem.getChildStockIns().add(sk);
										invItem.setQuantity(invItem.getQuantity().add(sk.getQuantity()));
									} else {
										
										sk.setQuantity(sk.getQuantity().subtract(stock.getDsumm()));
										
										StockIn stc = sk.clone();
										stc.setId(null);
										stc.setQuantity(stock.getDsumm());
										stc.setIntStatus(new StockStatus(StockStatus.STATUS_INT_ACCOUNTABLE));
										stc.setInvoiceItem(invItem);
										invItem.getChildStockIns().add(stc);
										invItem.setQuantity(invItem.getQuantity().add(stock.getDsumm()));
									}
								}
							}
							
						}
					}
				}					
			}
			saveInvoice(newInv);
			return newInv;
		} catch (Exception ex) {
			throw ex;
		}
	}
	
	

	////////////////////////////////// update Invoice
	////////////////////////////////// ///////////////////////////////////
	@Override
	public Invoice updateInvoice(Invoice newInv) throws Exception {
		try {
		
			Invoice oldInv = invDao.findinv(newInv.getId());

			if (newInv.getParty() != null) {
				Party ptNew = ptDao.findOne(newInv.getParty().getId());
				oldInv.setParty(ptNew);
			}

			if (newInv.getData() != null) {
				oldInv.setData(newInv.getData());
			}
			
			if (newInv.getCost() != null) {
				oldInv.setCost(newInv.getCost());
			}
			
			if (newInv.getBankPartner() != null) {
				Party ptNew = ptDao.findOne(newInv.getBankPartner().getId());
				oldInv.setBankPartner(ptNew);
			} else {
				oldInv.setBankPartner(null);
			}

			if (newInv.getCompany() != null) {
				Company com = comDao.findOne(newInv.getCompany().getId());
				oldInv.setCompany(com);
			}

			if (newInv.getBranch() != null) {
				Branch com = braDao.findOne(newInv.getBranch().getId());
				oldInv.setBranch(com);
			}
			if (newInv.getBranchImporter() != null) {
				Branch com = braDao.findOne(newInv.getBranchImporter().getId());
				oldInv.setBranchImporter(com);
			}

			if (newInv.getCurrency() != null) {
				Currency com = curDao.findOne(newInv.getCurrency().getCurrency());
				oldInv.setCurrency(com);
			}

			if (newInv.getIncoterm() != null) {
				Incoterm com = incDao.findOne(newInv.getIncoterm().getCode());
				oldInv.setIncoterm(com);
			}

			if (newInv.getInvoiceItems() != null) {
				List<InvoiceItem> iiL = new ArrayList<>();
				for (InvoiceItem ii : newInv.getInvoiceItems()) {

					if (ii.getId() != null && ii.getId() > 0) {
						InvoiceItem ii2 = invoiceItemDao.findOne(ii.getId());

						ii2 = ii.clone();

						if (ii2.getInventory() != null) {
							Inventory inv = iDao.findOne(ii.getInventory().getId());
							ii2.setInventory(inv);
						}

						if (ii2.getUnit() != null) {
							Unit inv = uniDao.findOne(ii.getUnit().getName());
							ii2.setUnit(inv);
						}

						if (ii2.getCurrency() != null) {
							Currency inv = curDao.findOne(ii.getCurrency().getCurrency());
							ii2.setCurrency(inv);
						}

						if (ii2.getInvoice() != null) {
							Invoice inv = invDao.findOne(ii.getInvoice().getId());
							ii2.setInvoice(inv);
						}

						iiL.add(ii2);
					} else {
						iiL.add(ii);
					}
				}
				oldInv.getInvoiceItems().clear();
				oldInv.getInvoiceItems().addAll(iiL);
			}
			oldInv.setDateCreated(Calendar.getInstance());
			invDao.save(oldInv);
			return oldInv;
		} catch (Exception ex) {
			throw ex;
		}
	}

	////////////////////////// Take Invoice by ID
	////////////////////////// //////////////////////////////////////////////////////
	@Override
	public Invoice getInvoiceById(Long id) throws Exception {
		try {
			Invoice res = invDao.findinv(id);
			return res;
		} catch (Exception ex) {
			throw ex;
		}
	}

	//////////// Invoice For Contract //////////////////////////////////////////////

	@Override
	public Invoice saveNewInvoiceFromContract(Contract newContract) throws Exception {
		Invoice invoice = new Invoice();
		try {
			InvoiceValidator.validateForContract(newContract);
			invoice.setCompany(newContract.getCompany());
			invoice.setBranch(newContract.getServiceBranch());
			invoice.setBranchImporter(newContract.getServiceBranch());
			invoice.setParty(newContract.getCustomer());
			invoice.setUserAdded(newContract.getRegisteredUser());
			invoice.setData(newContract.getDateSigned());
			invoice.setInvoiceStatus(new InvoiceStatus(InvoiceStatus.STATUS_NEW));
			invoice.setScopeType(new ScopeType(ScopeType.SCOPE_INT));
			invoice.setDocId(newContract.getId());
			invoice.setDocno(newContract.getContractNumber());
			
			if (newContract.getIsRent().equals(true)) {
				invoice.setInvoiceType(Invoice.INVOICE_TYPE_RENT);
			} else {
				invoice.setInvoiceType(Invoice.INVOICE_TYPE_SELL);
			}

			constructInvoiceItems(invoice, newContract);
			
			invoice = saveInvoice(invoice);
			return invoice;
		} catch (Exception ex) {
			throw ex;
		}
	}

	private void constructInvoiceItems(Invoice invoice, Contract contract) throws Exception {
		try {
			invoice.setInvoiceItems(new ArrayList<>());
			
			for (ContractItem ci : contract.getContractItems()) {
				
				InvoiceItem it = new InvoiceItem();
				it.setInventory(ci.getInventory());
				it.setUnit(ci.getUnit());
				it.setInvoice(invoice);
				it.setQuantity(new BigDecimal(0));

				Boolean bool = true;
				Integer sum = 0;

				for (InvoiceItem ii : invoice.getInvoiceItems()) {
					if (ii.getInventory().getId() == it.getInventory().getId()
							&& ii.getChildStockIns().get(1).getGlCode() == ci.getGlCode()) {
						bool = false;
						break;
					}
					sum++;
				}

				/// Inventories with SN

				if (ci.getInventory().getInvMainCategory().getId().equals(InvMainCategory.CAT_TOVAR)) {

					///// Item From DATABATH with SN
					if (ci.getSerialNumber().length() > 0) {

						StockIn stock = stockDao.findBySerialNumber(invoice.getCompany().getId(),
								invoice.getBranch().getId(), ci.getInventory().getId(), ci.getSerialNumber());
						if (stock != null) {
							
							InvItemSn iiSn = new InvItemSn(it, stock.getSerialNumber());
							it.getChildInvSns().add(iiSn);
							
							StockValidator.validateStockForContract(stock);
							stock.setIntStatus(new StockStatus(StockStatus.STATUS_INT_RESERVED));
							///////// to old InvoiceItem
							if (bool == false) {
								stock.setInvoiceItem(invoice.getInvoiceItems().get(sum));
								invoice.getInvoiceItems().get(sum).getChildStockIns().add(stock);
								invoice.getInvoiceItems().get(sum).setQuantity(
										invoice.getInvoiceItems().get(sum).getQuantity().add(stock.getQuantity()));
								////// to new InvoiceItem
							} else {

								stock.setInvoiceItem(it);
								it.setChildStockIns(new ArrayList<>());
								it.getChildStockIns().add(stock);
								it.setQuantity(it.getQuantity().add(stock.getQuantity()));
							}
						} else throw new Exception("Товар SN " + ci.getSerialNumber() + " не найден.");
						
						/// Item not from DATABASE without SN
					} else {
						if (bool == false) {
							invoice.getInvoiceItems().get(sum).setQuantity(
									invoice.getInvoiceItems().get(sum).getQuantity().add(ci.getQuantity()));
						} else {
							it.setChildStockIns(new ArrayList<>());
							it.setQuantity(it.getQuantity().add(ci.getQuantity()));
						}
					}

					/// Inventories without SN
				} else {

					Iterable<StockIn> stocks = stockDao.getStockInByGlAndInv(contract.getCompany().getId(),
							contract.getBranch().getId(), ci.getGlCode(), ci.getInventory().getId(),
							ci.getQuantity().intValue());
					Integer count = ci.getQuantity().intValue();

					//// To old InvoiceItem
					if (bool == false) {
						invoice.getInvoiceItems().get(sum)
								.setQuantity(invoice.getInvoiceItems().get(sum).getQuantity().add(ci.getQuantity()));

						for (StockIn stock : stocks) {
							StockValidator.validateStockForContract(stock);
							//////////// if stock's quantity <= for our need quantity
							if (stock.getQuantity().compareTo(new BigDecimal(count)) == 1
									|| stock.getQuantity().compareTo(new BigDecimal(count)) == 0) {
								stock.setIntStatus(new StockStatus(StockStatus.STATUS_INT_RESERVED));
								stock.setInvoiceItem(invoice.getInvoiceItems().get(sum));
								invoice.getInvoiceItems().get(sum).getChildStockIns().add(stock);
								count = count - stock.getQuantity().intValue();

							} else {
								////////// if stock's quantity > for our need quantity
								stock.setQuantity(stock.getQuantity().subtract(new BigDecimal(count)));
								stockDao.save(stock);
								StockIn sk = stock.clone();
								sk.setIntStatus(new StockStatus(StockStatus.STATUS_INT_RESERVED));
								sk.setId(null);
								sk.setQuantity(new BigDecimal(count));
								sk.setInvoiceItem(invoice.getInvoiceItems().get(sum));
								invoice.getInvoiceItems().get(sum).getChildStockIns().add(sk);
								count = 0;
							}
							if (count == 0) {
								break;
							}
						}
						//// to new InvoiceItem
					} else {
						it.setChildStockIns(new ArrayList<>());
						it.setQuantity(ci.getQuantity());
						
						for (StockIn stock : stocks) {

							StockValidator.validateStockForContract(stock);
							//// if stock's quantity <= our quantity
							if (stock.getQuantity().compareTo(new BigDecimal(count)) == 1
									|| stock.getQuantity().compareTo(new BigDecimal(count)) == 0) {

								stock.setIntStatus(new StockStatus(StockStatus.STATUS_INT_RESERVED));
								stock.setInvoiceItem(it);
								it.getChildStockIns().add(stock);

								count = count - stock.getQuantity().intValue();

								/// if stock's quantity > our quantity
							} else {
								stock.setQuantity(stock.getQuantity().subtract(new BigDecimal(count)));
								stockDao.save(stock);

								StockIn sk = stock.clone();
								sk.setId(null);
								sk.setIntStatus(new StockStatus(StockStatus.STATUS_INT_RESERVED));
								sk.setQuantity(new BigDecimal(count));
								sk.setInvoiceItem(it);
								it.getChildStockIns().add(sk);

								count = 0;
							}
							if (count == 0) {
								break;
							}
						}
					}

				}

				if (bool == true) {
					invoice.getInvoiceItems().add(it);
				}
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public Boolean updateInvoiceFromContract(Contract contract) throws Exception {
		try {
			Invoice invoice = restoreFromReserved(contract);
			
			invDao.save(invoice);
			
			constructInvoiceItems(invoice, contract);
			
			return true;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Invoice newInvoiceService(SmService service) throws Exception {
			Invoice newInv = new Invoice();
			Integer count = 0;
		try {
			InvoiceValidator.validateService(service);
			
			newInv.setCompany(service.getContract().getCompany());
			newInv.setBranch(service.getBranch());
			newInv.setBranchImporter(service.getBranch());
			newInv.setData(service.getSdate());
			newInv.setScopeType(new ScopeType(ScopeType.SCOPE_INT));
			newInv.setParty(service.getCareman().getParty());
			newInv.setInvoiceStatus(new InvoiceStatus(InvoiceStatus.STATUS_NEW));
			newInv.setUserAdded(service.getUserAdded());
			newInv.setInvoiceType(Invoice.INVOICE_TYPE_SERVICE);
			newInv.setDocId(service.getId());
			newInv.setDocno(service.getServiceNumber());
			newInv.setInvoiceItems(new ArrayList<>());
			
			for (SmServiceItem si: service.getSmServiceItems()) {
				if (si.getIsTovar() == false) {
					continue;
				}
				count = 0;
				for (InvoiceItem ii: newInv.getInvoiceItems()) {
					if (si.getInventory().getId().equals(ii.getInventory().getId())) {
						ii.setQuantity(ii.getQuantity().add(new BigDecimal(1)));
						count ++;
					}
				}
				if (count != 0) {
					continue;
				}
				InvoiceItem ii = new InvoiceItem();
				ii.setInvoice(newInv);
				ii.setQuantity(si.getQuantity());
				ii.setInventory(si.getInventory());
				ii.setUnit(si.getUnit());
				
				newInv.getInvoiceItems().add(ii);
			}
			
			newInv = saveInvoice(newInv);
			
			return newInv;
		} catch (Exception ex) {
			throw ex;
		}
		
	}
	
	@Override
	public Invoice getInvoiceForServices(Integer cid, Integer bid, Date from, Date to) throws Exception {
		Invoice invoice = new Invoice();
		ArrayList<Invoice> invoices = new ArrayList<Invoice>();
		try {
			
			if (bid == 0) {
				invoices = invDao.getInvoicesForServices(cid, from, to, Invoice.INVOICE_TYPE_SERVICE);
			} else {
				invoices = invDao.getInvoicesForServicesByBranch(cid, bid, from, to, Invoice.INVOICE_TYPE_SERVICE);
			}
			
			invoice = createServiceReport(invoices);
			
			return invoice;
		}catch(Exception ex) {
			throw ex;
		}
	}
	
	private Invoice createServiceReport(ArrayList<Invoice> invoices) {
		
		Invoice invoice = new Invoice();
		
		for (Invoice inv: invoices) {
			for (InvoiceItem ii: inv.getInvoiceItems()) {
				
				Integer count = 0;
				
				for (InvoiceItem invIte: invoice.getInvoiceItems()) {
					if (ii.getInventory().getId() == invIte.getInventory().getId()) {
						count ++;
						invIte.setQuantity(invIte.getQuantity().add(ii.getQuantity()));
						break;
					}
				}
				
				if (count == 0) {
					InvoiceItem invItem = new InvoiceItem();
					invItem.setInventory(ii.getInventory());
					invItem.setQuantity(ii.getQuantity());
					invoice.getInvoiceItems().add(invItem);
				}
				
			}
		}
		
		return invoice;
	}
	
	@Override
	public Boolean checkCustomerHasStock(String contractNumber) throws Exception {
		try {
			Invoice invoice = invDao.findByDocno(contractNumber, Invoice.INVOICE_TYPE_SELL);
			if (invoice == null) {
				invoice = invDao.findByDocno(contractNumber, Invoice.INVOICE_TYPE_RENT);
			}
			if (invoice == null)
				throw new Exception("Накладная отсутствует");
			
			for (InvoiceItem ii: invoice.getInvoiceItems()) {
				if (ii.getChildStockOuts().size() > 0) {
					return true;
				}
			}
			
			return false;
		}catch(Exception ex) {
			throw ex;
		}
	}

	// *************************************************************************************************
		
	@Override
	public Invoice restoreFromReserved(Contract contract) throws Exception {
		try {
			if (checkCustomerHasStock(contract.getContractNumber())) {
				throw new Exception("Выполните возврат товаров от клиента.");
			}
			
			Invoice invoice = invDao.findByDocId(contract.getId(), Invoice.INVOICE_TYPE_SELL);
			if (invoice == null) {
				invoice = invDao.findByDocId(contract.getId(), Invoice.INVOICE_TYPE_RENT);
			}
			if (invoice == null)
				throw new Exception("Накладная отсутствует");
						
			for (InvoiceItem ii: invoice.getInvoiceItems()) {
				for (StockIn si : ii.getChildStockIns()) {
					if (si.getIntStatus().getId() != StockStatus.STATUS_INT_IN) {
						si.setIntStatus(new StockStatus(StockStatus.STATUS_INT_IN));
						si.setInvoiceItem(null);
					}
				}
			}
			
			return invoice;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Boolean stornoInvoiceByContract(Contract contract) throws Exception {
		try {
			
			// Проверить списан ли первый инвойс
			
			List<Invoice> invoices = invDao.findAllInvoiceDocno(contract.getContractNumber());
			if (invoices.size() == 0) {
				return true;
			}
			
			for (Invoice inv: invoices) {
				if ((inv.getInvoiceType() == Invoice.INVOICE_TYPE_RENT || inv.getInvoiceType() == Invoice.INVOICE_TYPE_SELL) 
						&& !inv.getInvoiceStatus().getName().equals(InvoiceStatus.STATUS_NEW) && inv.getStorno() == false) {
					throw new Exception ("Проведите возврат товара");
				}
			}
			// Или был ли возврат по новому инвойсу

			// Если нет то перести статус старого инвойс на STORNO = 1
			
			for (Invoice inv: invoices) {
				if ((inv.getInvoiceType() == Invoice.INVOICE_TYPE_RENT || inv.getInvoiceType() == Invoice.INVOICE_TYPE_SELL) 
						&& inv.getInvoiceStatus().getName().equals(InvoiceStatus.STATUS_NEW) && inv.getStorno() == false) {
					inv.setStorno(true);
				}
			}
			
			
			return null;
		} catch (Exception e) {
			throw e;
		}
	}
	
}
