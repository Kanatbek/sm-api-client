package com.cordialsr.service.implementation;

import java.io.File;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.domain.security.User;
import com.cordialsr.service.SendEmailService;

@Service
@Transactional(rollbackFor = Exception.class)
public class SendEmailServiceImpl implements SendEmailService{

	@Autowired
	JavaMailSender sender;

	@Override
	public void sendNotification(User user, String subject, String body) throws Exception {
		try {
		
			MimeMessage message = sender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setTo("150103002@stu.sdu.edu.kz");
			helper.setText(body, true);
			helper.setSubject(subject);
			helper.addInline("cordial_logo",
	                new File("src/main/resources/static/images/cordial_logo.png"));
			
			sender.send(message);
		
		} catch (Exception e) {
			throw e;
		}
		
	}

	
	
}
