package com.cordialsr.service.implementation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cordialsr.Dao.ContractDao;
import com.cordialsr.Dao.SmFcTermDao;
import com.cordialsr.Dao.SmFilterChangeDao;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.SmFcTerm;
import com.cordialsr.domain.SmFilterChange;
import com.cordialsr.domain.SmService;
import com.cordialsr.domain.SmServiceItem;
import com.cordialsr.domain.validator.SmFilterChangeValidator;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.SmFilterChangeService;

@Service
@Transactional(rollbackFor = Exception.class)
public class SmFilterChangeServiceImpl implements SmFilterChangeService {
	
	@Autowired
	SmFilterChangeDao smfcDao;
	
	@Autowired
	ContractDao contractDao;
	
	@Autowired
	SmFcTermDao termDao;
	
	public Iterable<SmFilterChange> findAll() {
		return smfcDao.findAll();
	}	
	
	public SmFilterChange findById(Integer id) {
		return smfcDao.findOne(id);
	}
	
	public void saveFC(SmFilterChange fc) {
		smfcDao.save(fc);
	}
	
	public boolean isFcExist(SmFilterChange fc) {
		SmFilterChange fcRetrieved = smfcDao.findOne(fc.getId());				
		return fcRetrieved != null;
	}
	
	public void updateFC(SmFilterChange fc) {
		smfcDao.save(fc);		
	}
	
	////////////////////// New Sevice
	@Override
	public Boolean newService(SmService service) throws Exception {
		
		SmFilterChange newChange = null;
		
		try {
			if (service == null)
				throw new Exception("Service is empty");
			if (service.getContract() == null || GeneralUtil.isEmptyLong(service.getContract().getId()))
				throw new Exception("Service's Contract is Empty");
			
			newChange = smfcDao.findByContractId(service.getContract().getId());
			if (newChange == null)
				throw new Exception("Filter change graph not found.");
			
			Boolean overdue = isOverdueFC(newChange, service.getSdate());
						
			// Обновление даты замены фильтров
			if (service.getType().equals(SmService.SERVICE_TYPE_ZAMF)) {
				for (SmServiceItem si: service.getSmServiceItems()) {
					newChange = changeNewData(si, newChange, service.getSdate());
				}
			}			
			// Для профилактики контрольный
			newChange = changeNewData(null, newChange, service.getSdate());
			smfcDao.save(newChange);
			
			return overdue;
			
		} catch (Exception ex) {
			throw ex;
		}
	}
	
	@Override
	public Boolean isOverdueFC(SmFilterChange fc, Date sdate) throws Exception {
		boolean overdue = false;
		boolean current = false;
		
		if (fc.getPrNext() == null 
				|| fc.getF1Next() == null
				|| fc.getF1Next() == null
				|| fc.getF1Next() == null
				|| fc.getF1Next() == null
				|| fc.getF1Next() == null) 
			throw new Exception("Дата следующего обслуживания не указана. Договор: " + fc.getContractNumber());
		
		if (// fc.getPrNext().before(sdate)
				(GeneralUtil.calcAgeInMonth(fc.getF1Next(), sdate) > 0)
				|| (GeneralUtil.calcAgeInMonth(fc.getF2Next(), sdate) > 0)
				|| (GeneralUtil.calcAgeInMonth(fc.getF3Next(), sdate) > 0)
				|| (GeneralUtil.calcAgeInMonth(fc.getF4Next(), sdate) > 0)
				|| (GeneralUtil.calcAgeInMonth(fc.getF5Next(), sdate) > 0)) 
			overdue = true; 
		
		if (// fc.getPrNext().before(sdate)
				(GeneralUtil.calcAgeInMonth(fc.getF1Next(), sdate) == 0)
				|| (GeneralUtil.calcAgeInMonth(fc.getF2Next(), sdate) == 0)
				|| (GeneralUtil.calcAgeInMonth(fc.getF3Next(), sdate) == 0)
				|| (GeneralUtil.calcAgeInMonth(fc.getF4Next(), sdate) == 0)
				|| (GeneralUtil.calcAgeInMonth(fc.getF5Next(), sdate) == 0)) 
			current = true;
		
		return (overdue && !current);
	}
	
	
	////////////////////////////////////////////////////////// Is it get early .........................................................................

	@Override
	public Boolean earlyService(SmService service) throws Exception {
		SmFilterChange fc = null;
		try {
			fc = smfcDao.findByContractId(service.getContract().getId());
			for (SmServiceItem item: service.getSmServiceItems()) {
				if (item.getFno() == null) {
					return false;
				}else if (item.getFno().equals(1) && GeneralUtil.calcAgeInMonth(fc.getF1Prev(), fc.getF1Last()) < fc.getF1Mt() - 1) {
					return true;
				} else if (item.getFno().equals(2) && GeneralUtil.calcAgeInMonth(fc.getF2Prev(), fc.getF2Last()) < fc.getF2Mt() - 1) {
					return true;
				} else if (item.getFno().equals(3) && GeneralUtil.calcAgeInMonth(fc.getF3Prev(), fc.getF3Last()) < fc.getF3Mt() - 1) {
					return true;
				} else if (item.getFno().equals(4) && GeneralUtil.calcAgeInMonth(fc.getF4Prev(), fc.getF4Last()) < fc.getF4Mt() - 1) {
					return true;
				} else if (item.getFno().equals(5) && GeneralUtil.calcAgeInMonth(fc.getF5Prev(), fc.getF5Last()) < fc.getF5Mt() - 1) {
					return true;
				}
			}	
			return false;
		} catch(Exception ex) {
			throw ex;
		}
	}
	
	/////////////////////////////////////////////// Update Service ...
	@Override
	public void updateService(SmService service) throws Exception {
		SmFilterChange newChange = null;
		
		try {
			if (service == null)
				throw new Exception("Service is empty");
			if (service.getContract() == null || GeneralUtil.isEmptyLong(service.getContract().getId()))
				throw new Exception("Service's Contract is Empty");
			
			newChange = smfcDao.findByContractId(service.getContract().getId());
			if (newChange == null)
				throw new Exception("Filter change graph not found.");
			
			// Обновление даты замены фильтров
			if (service.getType().equals(SmService.SERVICE_TYPE_ZAMF)) {
				for (SmServiceItem si: service.getSmServiceItems()) {
					newChange = changePrevData(si, newChange, service.getSdate());
				}
			}			
			// Для профилактики контрольный
			newChange = changePrevData(null, newChange, service.getSdate());
			smfcDao.save(newChange);
			
			return;
			
		} catch (Exception ex) {
			throw ex;
		}
	}
	
	
//////////////////////////////////////////Service previous change Date .........................
	private SmFilterChange changePrevData(SmServiceItem si, SmFilterChange newChange, Date sdate)throws Exception {
		try {
			if (si == null) {
				newChange.setPrLast(newChange.getPrPrev());
				if (newChange.getPrLast() != null && newChange.getPrMt() != null) {
					newChange.setPrNext(GeneralUtil.addMonth(newChange.getPrLast(), newChange.getPrMt()));	
				}
				newChange.setPrPrev(null);
			} else if (si.getFno().equals(1)) {
				newChange.setF1Last(newChange.getF1Prev());
				if (newChange.getF1Last() != null && newChange.getF1Mt() != null) {
					newChange.setF1Next(GeneralUtil.addMonth(newChange.getF1Last(), newChange.getF1Mt()));
				}
				newChange.setF1Prev(null);
			} else if (si.getFno().equals(2)) {
				newChange.setF2Last(newChange.getF2Prev());
				if (newChange.getF2Last() != null && newChange.getF2Mt() != null) {
					newChange.setF2Next(GeneralUtil.addMonth(newChange.getF2Last(), newChange.getF2Mt()));
				}
				newChange.setF2Prev(null);
			} else if (si.getFno().equals(3)) {
				newChange.setF3Last(newChange.getF3Prev());
				if (newChange.getF3Last() != null && newChange.getF3Mt() != null) {
					newChange.setF3Next(GeneralUtil.addMonth(newChange.getF3Last(), newChange.getF3Mt()));	
				}
				newChange.setF3Next(GeneralUtil.addMonth(newChange.getF3Last(), newChange.getF3Mt()));
				newChange.setF3Prev(null);
			} else if (si.getFno().equals(4)) {
				newChange.setF4Last(newChange.getF4Prev());
				if (newChange.getF4Last() != null && newChange.getF4Mt() != null) {
					newChange.setF4Next(GeneralUtil.addMonth(newChange.getF4Last(), newChange.getF4Mt()));	
				}
				newChange.setF4Prev(null);
			} else if (si.getFno().equals(5)) {
				newChange.setF5Last(newChange.getF5Prev());
				if (newChange.getF5Last() != null && newChange.getF5Mt() != null) {
					newChange.setF5Next(GeneralUtil.addMonth(newChange.getF5Last(), newChange.getF5Mt()));
				}
				newChange.setF5Prev(null);
			}
			return newChange;
		}catch(Exception ex) {
			throw ex;
		}
	}
	
    ////////////////////////////////////////// Service new change Date .........................
	private SmFilterChange changeNewData(SmServiceItem si, SmFilterChange newChange, Date sdate)throws Exception {
		try {
			if (si == null) {
				if (SmFilterChangeValidator.validateDates(sdate, newChange.getPrLast())) {
					newChange.setPrPrev(newChange.getPrLast());
					newChange.setPrLast(sdate);
					newChange.setPrNext(GeneralUtil.addMonth(sdate, newChange.getPrMt()));
				}
			} else if (si.getFno().equals(1)) {
				if (SmFilterChangeValidator.validateDates(sdate, newChange.getF1Last())) {
					newChange.setF1Prev(newChange.getF1Last());
					newChange.setF1Last(sdate);
					newChange.setF1Next(GeneralUtil.addMonth(sdate, newChange.getF1Mt()));
				}
			} else if (si.getFno().equals(2)) {
				if (SmFilterChangeValidator.validateDates(sdate, newChange.getF2Last())) {
					newChange.setF2Prev(newChange.getF2Last());
					newChange.setF2Last(sdate);
					newChange.setF2Next(GeneralUtil.addMonth(sdate, newChange.getF2Mt()));
				}
			} else if (si.getFno().equals(3)) {
				if (SmFilterChangeValidator.validateDates(sdate, newChange.getF3Last())) {
					newChange.setF3Prev(newChange.getF3Last());
					newChange.setF3Last(sdate);
					newChange.setF3Next(GeneralUtil.addMonth(sdate, newChange.getF3Mt()));
				}
			} else if (si.getFno().equals(4)) {
				if (SmFilterChangeValidator.validateDates(sdate, newChange.getF4Last())) {
					newChange.setF4Prev(newChange.getF4Last());
					newChange.setF4Last(sdate);
					newChange.setF4Next(GeneralUtil.addMonth(sdate, newChange.getF4Mt()));					
				}
			} else if (si.getFno().equals(5)) {
				if (SmFilterChangeValidator.validateDates(sdate, newChange.getF5Last())) {
					newChange.setF5Prev(newChange.getF5Last());
					newChange.setF5Last(sdate);
					newChange.setF5Next(GeneralUtil.addMonth(sdate, newChange.getF5Mt()));
				}
			}
			
			return newChange;
		}catch(Exception ex) {
			throw ex;
		}
	}
     /////////////////////////////////////////// New Graph of service
	@Override
	public SmFilterChange newGraph(Contract newContract) throws Exception {
		
		SmFilterChange smGraph = null;
		SmFcTerm smTerm = null;
		try {
			
			SmFilterChangeValidator.validateNewContract(newContract);
			Iterable<SmFcTerm> t = termDao.findOneByInventory(newContract.getContractItems().get(0).getInventory().getId());
			
			for (SmFcTerm term: t) {
				if (smTerm == null || term.getSdate().after(smTerm.getSdate())) {
					smTerm = term;
				}
			}
			
			smGraph = createGraphOfService(smTerm, newContract);
			smGraph.setCpuDate(Calendar.getInstance());
			
			smfcDao.save(smGraph);
			
			return smGraph;
			
		} catch(Exception ex) {
			throw ex;
		}
	}

	/////////////////////////////////// Creating Graph of Service
	
	private SmFilterChange createGraphOfService(SmFcTerm term, Contract contract)throws Exception {
		SmFilterChange smGraph = new SmFilterChange();
		try {
			
			smGraph.setContract(contract);
			smGraph.setEnabled(!contract.getStorno());
			smGraph.setContractNumber(contract.getContractNumber());
			
			smGraph.setF1Mt(term.getF1());
			smGraph.setF2Mt(term.getF2());
			smGraph.setF3Mt(term.getF3());
			smGraph.setF4Mt(term.getF4());
			smGraph.setF5Mt(term.getF5());
			smGraph.setPrMt(term.getProf());
			
			smGraph.setF1Last(contract.getDateSigned());
			smGraph.setF2Last(contract.getDateSigned());
			smGraph.setF3Last(contract.getDateSigned());
			smGraph.setF4Last(contract.getDateSigned());
			smGraph.setF5Last(contract.getDateSigned());
			smGraph.setPrLast(contract.getDateSigned());
			
			smGraph.setF1Next(GeneralUtil.addMonth(contract.getDateSigned(), term.getF1()));
			smGraph.setF2Next(GeneralUtil.addMonth(contract.getDateSigned(), term.getF2()));
			smGraph.setF3Next(GeneralUtil.addMonth(contract.getDateSigned(), term.getF3()));
			smGraph.setF4Next(GeneralUtil.addMonth(contract.getDateSigned(), term.getF4()));
			smGraph.setF5Next(GeneralUtil.addMonth(contract.getDateSigned(), term.getF5()));
			smGraph.setPrNext(GeneralUtil.addMonth(contract.getDateSigned(), term.getProf()));
			
			return smGraph;
		} catch (Exception ex) {
			throw ex;
		}
		
	}

	@Override
	public SmFilterChange updateFilterChange(SmFilterChange fc) throws Exception {
		try {
			SmFilterChangeValidator.validateUpdate(fc);
			
			fc = smFilterFactory(fc);
			
			smfcDao.save(fc);
			
			return fc;
			
		} catch(Exception ex) {
			throw ex;
		}
	}
	
	

	private SmFilterChange smFilterFactory(SmFilterChange fc) throws Exception {
		try {
			SmFilterChange filtChang = smfcDao.findOne(fc.getId());
			if (filtChang != null) {
				filtChang = fc.clone();
				return filtChang;
			} else {
				throw new Exception("FC card not found!");
			}
		} catch(Exception ex) {
			throw ex;
		}
	}
	///////////////////////////////////////////////////////////////

	@Override
	public List<SmFilterChange> loadFilterChanges(Integer cid, Integer bid, Boolean bool, Date from, Date to, Long eid) throws Exception {
		try {
			List<SmFilterChange> fcList = null;
			if (cid == 0 && bid == 0) {
				fcList = smfcDao.findByDatesCaremanBetween(from, to, eid);
			}
			else if (eid == 0) {
				if (bid == 0) {
					if (bool == false) {
						fcList = smfcDao.findByDatesBetween(from, to, cid);
					} else {
						fcList = smfcDao.findByDatesOverdue(from, to, cid);
					}
				} else {
					if (bool == false) {
						fcList = smfcDao.findByDatesBetweenBranch(from, to, bid, cid);
					} else {
						fcList = smfcDao.findByDatesOverdueBranch(from, to, bid, cid);
					}
				}
			} else {
				if (bid == 0) {
					if (bool == false) {
						fcList = smfcDao.findByDatesBetweenCareman(from, to, cid, eid);
					} else {

						fcList = smfcDao.findByDatesOverdueCareman(from, to, cid, eid);
					}
				} else {
					if (bool == false) {
						fcList = smfcDao.findByDatesBetweenBranchCareman(from, to, bid, cid, eid);
					} else {

						fcList = smfcDao.findByDatesOverdueBranchCareman(from, to, bid, cid, eid);
					}
				}
			}
			for (SmFilterChange fc: fcList) {
				fc.getContract();
				if (fc.getContract() != null && fc.getContract().getCustomer() != null) {
					fc.setCustomerId(fc.getContract().getCustomer().getId());
				}
			}
			return fcList;
		} catch(Exception ex) {
			throw ex;
		}
	
	}

	@Override
	public void updateFcByContract(Long conId, Boolean enabled) throws Exception {
		try {
			if (!GeneralUtil.isEmptyLong(conId)) {
				SmFilterChange fc = smfcDao.findByContractId(conId);
				fc.setEnabled(enabled);
				smfcDao.save(fc);
			}
		} catch(Exception e) {
			throw e;
		}
	}

	@Override
	public Integer loadCountFilterChanges(Integer cid, Integer bid, boolean b, Integer year, Integer month, Long eid) throws Exception {
		try {
			List<SmFilterChange> fcList = new ArrayList<SmFilterChange>();
			if (b == false) { 
				fcList = smfcDao.findByMonthBranchCareman(year, month, bid, cid, eid);
			} else {
				fcList = smfcDao.findOverdueByMonthBranchCareman(year, month, bid, cid, eid);
			}
			System.out.println(fcList.size());
			return fcList.size();
		} catch (Exception ex) {
			throw ex;
		}
	}

	@Override
	public SmFilterChange findBySerialNumber(String ser_num) throws Exception {
		try {
			SmFilterChange newfc = smfcDao.findBySN(ser_num);
			
			Contract contract = contractDao.findByContractNumber(newfc.getContractNumber());
			Party customer = contract.getCustomer();
			if (customer != null && customer.getId() > 0) {
				newfc.setCustomerId(customer.getId());
			}
			
			if (contract != null && contract.getId() > 0) {
				newfc.setContractId(contract.getId());
			}
			
			if (newfc == null) {
				throw new Exception("По серинному номеру, график замен фильтров не найден");
			}
			return newfc; 
		} catch (Exception ex) {
			throw ex;
		}
		
	}
}
