package com.cordialsr.service.implementation;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.ContractDao;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.BranchService;

@Service
@Transactional(rollbackFor = Exception.class)
public class BranchServiceImpl implements BranchService{

	@Autowired
	ContractDao contractDao;
	
	@Override
	public Integer getBranchContractNumber(Integer cid, Integer bid, Date dateStart) throws Exception {
		try {
			Date dtMonthStart = GeneralUtil.startOfMonth(dateStart);
			Date dtMonthEnd = GeneralUtil.endOfMonth(dateStart);
			
			Integer contractNumber = contractDao.findContractNumberByBranch(cid, bid, dtMonthStart, dtMonthEnd);
			return contractNumber;
		} catch (Exception e) {
			throw e;
		}
		
	}

}
