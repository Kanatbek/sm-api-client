package com.cordialsr.service.implementation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.BranchDao;
import com.cordialsr.Dao.CompanyDao;
import com.cordialsr.Dao.ContractDao;
import com.cordialsr.Dao.EmployeeDao;
import com.cordialsr.Dao.InventoryDao;
import com.cordialsr.Dao.InvoiceDao;
import com.cordialsr.Dao.PartyDao;
import com.cordialsr.Dao.SmServiceDao;
import com.cordialsr.Dao.SmServiceItemDao;
import com.cordialsr.Dao.SmVisitDao;
import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.InvSubCategory;
import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.InvoiceStatus;
import com.cordialsr.domain.SmCall;
import com.cordialsr.domain.SmService;
import com.cordialsr.domain.SmServiceItem;
import com.cordialsr.domain.SmVisit;
import com.cordialsr.domain.Unit;
import com.cordialsr.domain.nogenerator.SmServiceNoGenerator;
import com.cordialsr.domain.validator.InvoiceValidator;
import com.cordialsr.domain.validator.ServiceValidator;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.security.service.UserService;
import com.cordialsr.service.InvoiceService;
import com.cordialsr.service.PlanFactService;
import com.cordialsr.service.SmFilterChangeService;
import com.cordialsr.service.SmServService;

@Service
@Transactional(rollbackFor = Exception.class)
public class SmServServiceImpl implements SmServService {

	@Autowired
	SmServiceDao serviceDao;
	
	@Autowired
	SmServiceItemDao smSerIteDao;
	
	@Autowired
	InvoiceDao invoiceDao;

	@Autowired
	InvoiceService invoiceService;

	@Autowired
	SmFilterChangeService smFiltChangeService;

	@Autowired
	InventoryDao inventoryDao;
	
	@Autowired
	EmployeeDao caremanDao;
	
	@Autowired
	PartyDao partyDao;
	
	@Autowired
	ContractDao contractDao;
	
	@Autowired
	BranchDao branchDao;
	
	@Autowired
	CompanyDao companyDao;
	
	@Autowired
	SmVisitDao smVisitDao;
	
	@Autowired
	PlanFactService plaFacSer;
	
	@Autowired
	UserService userService;
	
	@Autowired
	SmServService service;

	@Override
	public SmService saveNewService(SmService service) throws Exception {
		try {
			
			Employee care = caremanDao.findById(service.getCareman().getId());
			if (care == null) {
				throw new Exception("Careman не найден.");
			}
			service.setCareman(care);
			ServiceValidator.validateNewService(service);

			//plaFacSer.getPlanCaremanOnMonth(service.getCompany().getId(), service.getBranch().getId(), service.getSdate(), service.getCareman().getParty());
			
			/////////////// Call for service .............

			Inventory inventory = inventoryDao.findOne(service.getInventory().getId());
			if ((inventory.getInvSubCategory().getId() == InvSubCategory.SUBCAT_WATER_PURIFYING_SYSTEM)
					|| (inventory.getInvSubCategory().getId() == InvSubCategory.SUBCAT_AIR_PURIFYING_SYSTEM)) {
				Boolean overdue = smFiltChangeService.newService(service);
				service.setOverdue(overdue);
				Boolean isEarly = smFiltChangeService.earlyService(service);
				service.setIsEarly(isEarly);
			}
			
			service = saveService(service);

			if (!service.getType().equals(SmService.SERVICE_TYPE_PROF) && InvoiceValidator.validateForService(service)) {
				invoiceService.newInvoiceService(service);
			}

			return service;
		} catch (Exception ex) {
			throw ex;
		}
	}

	@Override
	public void changeVerify(SmCall newCall, Integer level) throws Exception {
		try {

			SmService service = serviceDao.findById(newCall.getService().getId());
			if (service != null) {
				service.setVerified(true);
				service.setMark(level);
				serviceDao.save(service);
			} else {
				throw new Exception("Service is empty");
			}

		} catch (Exception ex) {
			throw ex;
		}

	}

	@Override
	public void assignServiceNumbers() {
		try {
			Iterable<SmService> smL = serviceDao.findAll();

			for (SmService sm : smL) {
				// if (GeneralUtil.isEmptyString(sm.getServiceNumber())) {
					sm.setServiceNumber(SmServiceNoGenerator.generateServiceNo(sm));
					serviceDao.save(sm);
				// }
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public SmService updateNewService(Long smId, SmService service) throws Exception {
		try {
			SmService newService = serviceDao.findById(smId);
			if (newService == null)
				throw new Exception("Service with id: " + smId + "is empty");
			
			
			Inventory inventory = inventoryDao.findOne(newService.getInventory().getId());
			if ((inventory.getInvSubCategory().getId() == InvSubCategory.SUBCAT_WATER_PURIFYING_SYSTEM)
					|| (inventory.getInvSubCategory().getId() == InvSubCategory.SUBCAT_AIR_PURIFYING_SYSTEM)) {
				smFiltChangeService.updateService(newService);
			}

			if (!newService.getType().equals(SmService.SERVICE_TYPE_PROF) && InvoiceValidator.validateForService(newService)) {
			
				Invoice inv = invoiceDao.findByDocId(newService.getId(), Invoice.INVOICE_TYPE_SERVICE);
				if (inv != null) {
					if (!inv.getInvoiceStatus().getName().equals(InvoiceStatus.STATUS_NEW)) {
						throw new Exception("Накладная по сервису уже используется");
					}
					invoiceDao.delete(inv);
				}
				
			}
	
			if (service.getCareman() != null) {
				Employee careman = caremanDao.findOne(service.getCareman().getId());
				if (careman == null)
					throw new Exception ("Careman is empty");
				newService.setCareman(careman);
			}
			
			if (service.getCaremanFio() != null) {
				newService.setCaremanFio(service.getCaremanFio());
			}
			
			if (service.getInfo() != null) {
				newService.setInfo(service.getInfo());
			}
			
			for (SmServiceItem ii: newService.getSmServiceItems()) {
				smSerIteDao.delete(ii);
			}
			
			newService.getSmServiceItems().clear();
			newService.getSmServiceItems().addAll(service.getSmServiceItems());

			newService = saveNewService(newService); 
			
			return newService;
			
		} catch(Exception ex) {
			throw ex;
		}
	}

	private SmService saveService(SmService service) throws Exception{
		try {
			serviceDao.save(service);
			if (service.getServiceNumber() == null || service.getServiceNumber().length() == 0) {
				String serNo = SmServiceNoGenerator.generateServiceNo(service);
				service.setServiceNumber(serNo);
				serviceDao.save(service);
			}
			return service;
		} catch(Exception ex) {
			throw ex;
		}
	}

	@Override
	public SmService cancelNewService(SmService service) throws Exception {
		try {
			SmService newService = serviceDao.findById(service.getId());
			if (newService == null)
				throw new Exception("Service with id: " + service.getId() + "is empty");
			
			
			Inventory inventory = inventoryDao.findOne(newService.getInventory().getId());
			if ((inventory.getInvSubCategory().getId() == InvSubCategory.SUBCAT_WATER_PURIFYING_SYSTEM)
					|| (inventory.getInvSubCategory().getId() == InvSubCategory.SUBCAT_AIR_PURIFYING_SYSTEM)) {
				smFiltChangeService.updateService(newService);
			}

			if (!newService.getType().equals(SmService.SERVICE_TYPE_PROF) && InvoiceValidator.validateForService(newService)) {
			
				Invoice inv = invoiceDao.findByDocId(newService.getId(), Invoice.INVOICE_TYPE_SERVICE);
				if (inv != null) {
					if (!inv.getInvoiceStatus().getName().equals(InvoiceStatus.STATUS_NEW)) {
						throw new Exception("Накладная по сервису уже используется");
					}
					inv.setStorno(true);
					invoiceDao.save(inv);
				}
				
			}
			newService.setStorno(true);
			newService.setInfo(service.getInfo());
			serviceDao.save(newService);
			
			return newService;
			
		} catch (Exception ex) {
			throw ex;
		}
	}
	
	@Override
	public String convertToService(String contractNumber, Date sdate, Date sTime, Date iTime, Integer fno1, Integer fno2, Integer fno3,
			Integer fno4, Integer fno5, String type, String info, String username, BigDecimal longitude, BigDecimal latitude) throws Exception {
		try {
			if (type.equals("PROF")) {
				makePROF(contractNumber, sdate, sTime, iTime, type, info, username, longitude ,latitude);
			} else if (type.equals("ZAMF")) {
				makeZAMF(contractNumber, sdate, sTime, iTime, fno1, fno2, fno3, fno4, fno5, type, info, username, longitude, latitude);
			} else if (type.equals("ZAMFPROF") || type.equals("PROFZAMF")){
				makePROF(contractNumber, sdate, sTime, iTime, type, info, username, longitude, latitude);
				makeZAMF(contractNumber, sdate, sTime, iTime, fno1, fno2, fno3, fno4, fno5, type, info, username, longitude, latitude);
			} else {
				return "Тип замены неправильно";
			}
			return "Успешно";
		} catch(Exception ex) {
			throw ex;
		}
	}
	
	public void makeZAMF(String contractNumber, Date sdate, Date sTime, Date iTime, Integer fno1, Integer fno2, Integer fno3,
			Integer fno4, Integer fno5, String type, String info, String username, BigDecimal longitude, BigDecimal latitude) {
		SmService smService = new SmService();
		SmVisit smVisit = new SmVisit();
		SmServiceItem smServiceItem = new SmServiceItem();
		Contract contract = contractDao.findByContractNumber(contractNumber);
		if (contract != null) {
			smService.setContract(contract);
		}
		if (username != null) {
			if (userService.checkUsernameExists(username)) {
				smService.setUserAdded(userService.findByUsername(username));
			}
		}
		if (contract != null && contract.getCareman() != null) {
			smVisit.setEmployee(contract.getCareman());
			smVisit.setParty(contract.getCareman().getParty());
		}
		if (longitude != null) {
			smVisit.setLongitude(longitude);
		}
		if (latitude != null) {
			smVisit.setLatitude(latitude);
		}
		if (sdate != null) {
			smService.setSdate(sdate);
			smVisit.setInDate(sdate);
		}
		if (sTime != null) {
			smService.setStime(sTime);
			smVisit.setInTime(sTime);
		}
		if (iTime != null) {
			smService.setItime(iTime);
			smVisit.setOutTime(iTime);
		}
		if (info != null) {
			smService.setInfo(info);
		}
		
		smService.setStorno(false);
		smService.setType("ZAMF");
		smService.setVerified(false);
		if (smService.getContract().getBranch() != null) {
			smService.setBranch(branchDao.findById(smService.getContract().getBranch().getId()));	
		}
		
		if (smService.getContract().getCareman() != null) {
			Employee careman = caremanDao.findById(smService.getContract().getCareman().getId());
			if (careman != null) {
				smService.setCareman(careman);
				smService.setCaremanFio(careman.getParty().getFullFIO());
			}
		}
		
		if (smService.getContract().getCompany() != null) {
			smService.setCompany(companyDao.findcid(smService.getContract().getCompany().getId()));
		}
		
		if (smService.getContract().getExploiter() != null) {
			smService.setCustomer(partyDao.findAllByParty(smService.getContract().getExploiter().getId()));
		}
		
		if (smService.getContract().getInventory() != null) {
			smService.setInventory(inventoryDao.findOne(smService.getContract().getInventory().getId()));
		}
		Unit unit = new Unit();
		
		unit.setFullName("Штук");
		unit.setInfo("Количество");
		unit.setName("PCS");
		
		if (fno1 != null && fno1 == 1) {
			smServiceItem = new SmServiceItem();
			Inventory inventory = inventoryDao.findOne(30);
			smServiceItem.setFno(1);
			smServiceItem.setIsTovar(true);
			smServiceItem.setQuantity(new BigDecimal(1));
			smServiceItem.setUnit(unit);
			smServiceItem.setSmService(smService);
			if (inventory != null) {
				smServiceItem.setInventory(inventory);
				smServiceItem.setItemName(inventory.getName());
			}
			if (smServiceItem != null) {
				smService.getSmServiceItems().add(smServiceItem);
			}
		}
		if (fno2 != null && fno2 == 1) {
			smServiceItem = new SmServiceItem();
			Inventory inventory = inventoryDao.findOne(34);
			smServiceItem.setFno(2);
			smServiceItem.setIsTovar(true);
			smServiceItem.setQuantity(new BigDecimal(1));
			smServiceItem.setUnit(unit);
			smServiceItem.setSmService(smService);
			if (inventory != null) {
				smServiceItem.setInventory(inventory);
				smServiceItem.setItemName(inventory.getName());
			}
			if (smServiceItem != null) {
				smService.getSmServiceItems().add(smServiceItem);
			}
		}
		if (fno3 != null && fno3 == 1) {
			smServiceItem = new SmServiceItem();
			Inventory inventory = inventoryDao.findOne(42);
			smServiceItem.setFno(3);
			smServiceItem.setIsTovar(true);
			smServiceItem.setQuantity(new BigDecimal(1));
			smServiceItem.setUnit(unit);
			smServiceItem.setSmService(smService);
			if (inventory != null) {
				smServiceItem.setInventory(inventory);
				smServiceItem.setItemName(inventory.getName());
			}
			if (smServiceItem != null) {
				smService.getSmServiceItems().add(smServiceItem);
			}
		}
		if (fno4 != null && fno4 == 1) {
			smServiceItem = new SmServiceItem();
			Inventory inventory = inventoryDao.findOne(43);
			smServiceItem.setFno(4);
			smServiceItem.setIsTovar(true);
			smServiceItem.setQuantity(new BigDecimal(1));
			smServiceItem.setUnit(unit);
			smServiceItem.setSmService(smService);
			if (inventory != null) {
				smServiceItem.setInventory(inventory);
				smServiceItem.setItemName(inventory.getName());
			}
			if (smServiceItem != null) {
				smService.getSmServiceItems().add(smServiceItem);
			}
		}
		if (fno5 != null && fno5 == 1) {
			smServiceItem = new SmServiceItem();
			Inventory inventory = inventoryDao.findOne(44);
			smServiceItem.setFno(5);
			smServiceItem.setIsTovar(true);
			smServiceItem.setQuantity(new BigDecimal(1));
			smServiceItem.setUnit(unit);
			smServiceItem.setSmService(smService);
			if (inventory != null) {
				smServiceItem.setInventory(inventory);
				smServiceItem.setItemName(inventory.getName());
			}
			if (smServiceItem != null) {
				smService.getSmServiceItems().add(smServiceItem);
			}
		}
		try {
			smVisitDao.save(smVisit);
		} catch (Exception e) {
			e.printStackTrace();
		}
		smService.setSmVisit(smVisit);
		try {
			service.saveNewService(smService);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void makePROF(String contractNumber, Date sdate, Date sTime, Date iTime
						,String type, String info, String username, BigDecimal longitude, BigDecimal latitude) {
		SmService smService = new SmService();
		SmVisit smVisit = new SmVisit();
		
		
		Contract contract = contractDao.findByContractNumber(contractNumber);
		if (contract != null) {
			smService.setContract(contract);
		}
		if (username != null) {
			if (userService.checkUsernameExists(username)) {
				smService.setUserAdded(userService.findByUsername(username));
			}
		}
		if (contract != null && contract.getCareman() != null) {
			smVisit.setEmployee(contract.getCareman());
			smVisit.setParty(contract.getCareman().getParty());
		}
		if (longitude != null) {
			smVisit.setLongitude(longitude);
		}
		if (latitude != null) {
			smVisit.setLatitude(latitude);
		}
		if (sdate != null) {
			smVisit.setInDate(sdate);
			smService.setSdate(sdate);
		}
		if (sTime != null) {
			smVisit.setInTime(sTime);
			smService.setStime(sTime);
		}
		if (iTime != null) {
			smVisit.setOutTime(iTime);
			smService.setItime(iTime);
		}
		if (info != null) {
			smService.setInfo(info);
		}
		smService.setStorno(false);
		smService.setType("PROF");
		smService.setVerified(false);
		if (smService.getContract().getBranch() != null) {
			smService.setBranch(branchDao.findById(smService.getContract().getBranch().getId()));	
		}
		if (smService.getContract().getCareman() != null) {
			Employee careman = caremanDao.findById(smService.getContract().getCareman().getId());
			if (careman != null) {
				smService.setCareman(careman);
				smService.setCaremanFio(careman.getParty().getFullFIO());
			}
		}
		if (smService.getContract().getCompany() != null) {
			smService.setCompany(companyDao.findcid(smService.getContract().getCompany().getId()));
		}
		
		if (smService.getContract().getExploiter() != null) {
			smService.setCustomer(partyDao.findAllByParty(smService.getContract().getExploiter().getId()));
		}
		
		if (smService.getContract().getInventory() != null) {
			smService.setInventory(inventoryDao.findOne(smService.getContract().getInventory().getId()));
		}
		try {
			smVisitDao.save(smVisit);
		} catch (Exception e) {
			e.printStackTrace();
		}
		smService.setSmVisit(smVisit);
		try {
			service.saveNewService(smService);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
