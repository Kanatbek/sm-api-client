package com.cordialsr.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.RegionDao;
import com.cordialsr.domain.Region;
import com.cordialsr.service.RegionService;

@Service
@Transactional
public class RegionServiceImpl implements RegionService {

	@Autowired
	RegionDao regionDao;
	
	@Override
	public Region saveRegionChanges(Region region) throws Exception {
		try {
			Region oldReg = regionDao.findOne(region.getId());
			oldReg = region.clone();
			oldReg.setBranches(region.getBranches());
			regionDao.save(oldReg);
			return oldReg;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Region saveNewRegion(Region region) throws Exception {
		try {
			regionDao.save(region);
			return region;
		} catch(Exception e) {
			throw e;
		}
	}
}
