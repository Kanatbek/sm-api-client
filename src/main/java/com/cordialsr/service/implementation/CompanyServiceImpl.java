package com.cordialsr.service.implementation;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.ContractDao;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.CompanyService;

@Service
@Transactional(rollbackFor = Exception.class)
public class CompanyServiceImpl implements CompanyService{

	@Autowired
	ContractDao contractDao;
	
	public Integer getCompanyContractNumber(Integer cid, Date dateStart) throws Exception {
		try {
			Date dtMonthStart = GeneralUtil.startOfMonth(dateStart);
			Date dtMonthEnd = GeneralUtil.endOfMonth(dateStart);
			
			Integer contractNumber = contractDao.findContractNumberByCompany(cid, dtMonthStart, dtMonthEnd);
			return contractNumber;
		} catch (Exception e) {
			throw e;
		}
	}

}
