package com.cordialsr.service.implementation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.AwardDao;
import com.cordialsr.Dao.CompanyDao;
import com.cordialsr.Dao.ContractDao;
import com.cordialsr.Dao.ContractPaymentScheduleDao;
import com.cordialsr.Dao.ContractTypeDao;
import com.cordialsr.Dao.EmployeeDao;
import com.cordialsr.Dao.InvoiceDao;
import com.cordialsr.Dao.PartyDao;
import com.cordialsr.Dao.PayrollDao;
import com.cordialsr.Dao.SmConPaidDao;
import com.cordialsr.Dao.SmConPaymentDao;
import com.cordialsr.Dao.SmContractDao;
import com.cordialsr.domain.Address;
import com.cordialsr.domain.AddressType;
import com.cordialsr.domain.Award;
import com.cordialsr.domain.AwardTemplate;
import com.cordialsr.domain.AwardType;
import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.ConItemStatus;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractAwardSchedule;
import com.cordialsr.domain.ContractItem;
import com.cordialsr.domain.ContractPaymentSchedule;
import com.cordialsr.domain.ContractStatus;
import com.cordialsr.domain.ContractType;
import com.cordialsr.domain.Country;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Department;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.FinEntry;
import com.cordialsr.domain.FinGlAccount;
import com.cordialsr.domain.InvItemSn;
import com.cordialsr.domain.InvMainCategory;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.InvoiceItem;
import com.cordialsr.domain.InvoiceStatus;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.PartyType;
import com.cordialsr.domain.PaymentStatus;
import com.cordialsr.domain.Payroll;
import com.cordialsr.domain.PhoneNumber;
import com.cordialsr.domain.Position;
import com.cordialsr.domain.Region;
import com.cordialsr.domain.ScopeType;
import com.cordialsr.domain.ServiceCategory;
import com.cordialsr.domain.SmConPaid;
import com.cordialsr.domain.SmConPayment;
import com.cordialsr.domain.SmConSalesPs;
import com.cordialsr.domain.SmContract;
import com.cordialsr.domain.StockOut;
import com.cordialsr.domain.StockStatus;
import com.cordialsr.domain.Unit;
import com.cordialsr.domain.factory.FinDocFactory;
import com.cordialsr.domain.factory.PayrollFactory;
import com.cordialsr.domain.nogenerator.InvoiceNoGenerator;
import com.cordialsr.domain.security.User;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.FinDocService;
import com.cordialsr.service.InvoiceService;
import com.cordialsr.service.MigrationService;
import com.cordialsr.service.SmFilterChangeService;

@Service
@Transactional(rollbackFor = Exception.class)
public class MigrationServiceImpl implements MigrationService {

	private static final Logger log = LoggerFactory.getLogger(MigrationServiceImpl.class);
	
	@Autowired 
	ContractDao conDao;
	
	@Autowired 
	ContractTypeDao ctDao;

	@Autowired 
	PartyDao partyDao;
	
	@Autowired 
	SmContractDao smConDao;
	
	@Autowired
	EmployeeDao emplDao;
	
	@Autowired
	SmFilterChangeService smfcService;
	
	@Autowired
	ContractPaymentScheduleDao cpsDao;
	
	@Autowired
	AwardDao awardDao;
	
	@Autowired
	private EntityManager entityManager;

	private static final BigDecimal UZS_RATE_USD = new BigDecimal(8000);
	private static final BigDecimal KGS_RATE_USD = new BigDecimal(68);
	private static final BigDecimal KZT_RATE_USD = new BigDecimal(340);
	
	@Override
	public void migrateContracts(Integer offset, Integer limit) {
		try {
			List<SmContract> smConL = smConDao.findFirstN(offset, limit);
//			List<SmContract> smConL = new ArrayList<>();
//			smConL.add(smConDao.findByContractNumber("003-0000573"));
//			SmContract sm1 = smConDao.findOne(70849);
//			List<SmContract> smConL = new ArrayList<>();
//			smConL.add(sm1);
			Company company = new Company(1); // 1 - Cordial Service currently
			for (SmContract sm : smConL) {
				Branch branch = sm.getSmBranch().getRealBranch();
				Contract con = conDao.getContractByCn(company.getId(), sm.getContractNumber());
				if (con == null) {
					con = new Contract();
					con.setContractNumber(sm.getContractNumber());
					con.setDateSigned(sm.getCdate());
					con.setBranch(branch);
					con.setServiceBranch(branch);
					con.setCompany(company);
					con.setForbuh(false);
					con.setInfo(sm.getInfo());
					
					if (sm.getUpdatedDate() != null) {
						Calendar updatedDate = Calendar.getInstance();
						updatedDate.setTime(sm.getUpdatedDate());
						con.setUpdatedDate(updatedDate);
						con.setUpdatedUser(sm.getUpdatedUser());
					}
					
					con.setOldId(sm.getId());
					con.setRegisteredDate(Calendar.getInstance());
					con.setRegisteredUser(new User(1L));
					con.setMonth(sm.getMonthTerm());
					con.setDiscount(new BigDecimal(0));
					
					BigDecimal rate = null;
					if (branch.getCountry().getId() == Country.COUNTRY_UZ) rate = UZS_RATE_USD;
					else if (branch.getCountry().getId() == Country.COUNTRY_KG) rate = KGS_RATE_USD;
					else if (branch.getCountry().getId() == Country.COUNTRY_KZ) rate = KZT_RATE_USD;
					con.setRate(rate);
					
					con.setFromDealerSumm(new BigDecimal(0));
					con.setInventory(sm.getInventory());
					con.setInventorySn(sm.getSn());
					con.setVerified(true);
					
					initContractType(con, sm);
					initContractStatus(con, sm);
					initContractItem(con, sm);
					
					Party customer = migrateCustomer(sm, company);
					con.setCustomer(customer);
					con.setExploiter(customer);
					setContractContacts(con);
					
					Employee coordinator = migrateStaff(con, sm.getCoordinator(), Position.POS_COORDINATOR);
					con.setCoordinator(coordinator);
					
					Employee director = migrateStaff(con, sm.getDirector(), Position.POS_DIRECTOR);
					con.setDirector(director);
					
					Employee manager = migrateStaff(con, sm.getManager(), Position.POS_MANAGER);
					con.setManager(manager);
					
					Employee dealer = migrateStaff(con, sm.getDealer(), Position.POS_DEALER);
					con.setDealer(dealer);
					
					Employee demosec = migrateStaff(con, sm.getDemosec(), Position.POS_DEMOSEC);
					con.setDemosec(demosec);
					
					Employee careman = migrateStaff(con, sm.getCareman(), Position.POS_CAREMAN);
					con.setCareman(careman);
					
					Employee fitter = migrateStaff(con, sm.getFitter(), Position.POS_FITTER);
					con.setFitter(fitter);
				
					if (!GeneralUtil.isEmptyBigDecimal(sm.getPrice()) 
							&& !GeneralUtil.isEmptyBigDecimal(sm.getFirstPayment())
							&& !sm.getStorno()) {
						if (con.getIsRent()) setPriceAndPaymentScheduleForRentalContract(con, sm);
						else if (!con.getIsRent()) setPriceAndPaymentScheduleForSalesContract(con, sm);
					}
					
					conDao.save(con);
					
					smfcService.newGraph(con);
					
//					counter++;
//					if (counter == 50) {
//						counter = 0;
//						entityManager.flush(); // Commit on every 50 records...						
//					}
				} else {
//					con.setBranch(branch);
//					con.setServiceBranch(branch);
					
					con.setInventory(sm.getInventory());
					con.setInventorySn(sm.getSn());
					initContractType(con, sm);
					if (GeneralUtil.isEmptyString(con.getRefkey())) {
						if (sm.getStorno() != null) con.setStorno(sm.getStorno());
						smfcService.updateFcByContract(con.getId(), !con.getStorno());
					}
					
					Party customer = migrateCustomer(sm, company);
					con.setCustomer(customer);
					con.setExploiter(customer);
					setContractContacts(con);
					
					if (con.getCoordinator() == null) {
						Employee coordinator = migrateStaff(con, sm.getCoordinator(), Position.POS_COORDINATOR);
						con.setCoordinator(coordinator);
					}
					
					if (con.getDirector() == null) {
						Employee director = migrateStaff(con, sm.getDirector(), Position.POS_DIRECTOR);
						con.setDirector(director);							
					}
					
					if (con.getManager() == null) {
						Employee manager = migrateStaff(con, sm.getManager(), Position.POS_MANAGER);
						con.setManager(manager);
					}
					
					if (con.getDealer() == null) {
						Employee dealer = migrateStaff(con, sm.getDealer(), Position.POS_DEALER);
						con.setDealer(dealer);
					}
					
					if (con.getDemosec() == null) {
						Employee demosec = migrateStaff(con, sm.getDemosec(), Position.POS_DEMOSEC);
						con.setDemosec(demosec);
					}
					
					if (con.getFitter() == null) {
						Employee fitter = migrateStaff(con, sm.getFitter(), Position.POS_FITTER);
						con.setFitter(fitter);
					}
					
					if (con.getCareman() == null) {
						Employee careman = migrateStaff(con, sm.getCareman(), Position.POS_CAREMAN);
						con.setCareman(careman);
					}
					
					if (!GeneralUtil.isEmptyString(sm.getSn())) {
						con.setInventorySn(sm.getSn());
						if (sm.getInventory() != null) {
							con.setInventory(sm.getInventory());							
						}
						if (con.getContractItems().size() > 0) {
							ContractItem ci = con.getContractItems().get(0);
							ci.setSerialNumber(con.getInventorySn());
							ci.setInventory(con.getInventory());
						} else {
							throw new Exception("ContractItem is empty!");
						}						
					}
					
					if (GeneralUtil.isEmptyBigDecimal(con.getRate())) {
						BigDecimal rate = null;
						if (branch.getCountry().getId() == Country.COUNTRY_UZ) rate = UZS_RATE_USD;
						else if (branch.getCountry().getId() == Country.COUNTRY_KG) rate = KGS_RATE_USD;
						else if (branch.getCountry().getId() == Country.COUNTRY_KZ) rate = KZT_RATE_USD;
						con.setRate(rate);
					}
					
					if (con.getRegisteredUser() == null) {
						con.setRegisteredDate(Calendar.getInstance());
						con.setRegisteredUser(new User(1L));
					}
					
					if (GeneralUtil.isEmptyInteger(con.getMonth()) &&  !GeneralUtil.isEmptyInteger(sm.getMonthTerm())) {
						con.setMonth(sm.getMonthTerm());
					}
					
					if (con.getPaymentSchedules().size() == 0) {
						if (!GeneralUtil.isEmptyBigDecimal(sm.getPrice()) 
								&& !GeneralUtil.isEmptyBigDecimal(sm.getFirstPayment())
								&& !sm.getStorno()) {
							if (con.getIsRent()) con = setPriceAndPaymentScheduleForRentalContract(con, sm);
							else if (!con.getIsRent()) con = setPriceAndPaymentScheduleForSalesContract(con, sm);
							
						}
					}
					
					con.setOldId(sm.getId());
					conDao.save(con);
				}
			}
//			entityManager.flush(); // Commit at the end
		} catch (Exception e) {
			// entityManager.clear();
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	// *****************************************************************************************************
	
	private Contract setPriceAndPaymentScheduleForSalesContract(Contract con, SmContract sm) throws Exception {
		try {
			con.setCurrency(new Currency(sm.getCurrency()));
			
			BigDecimal cost = sm.getPrice();
			BigDecimal paid = sm.getFirstPayment().add(sm.getPaidVznos());
			con.setCost(cost);
			con.setDiscount(BigDecimal.ZERO);
			con.setFromDealerSumm(BigDecimal.ZERO);
			con.setSumm(cost);
			con.setPaid(paid);
			con.setMonth(sm.getMonthTerm());
			
			con.setPaymentStatus(new PaymentStatus(PaymentStatus.PSTAT_REGULAR));
			if (con.getPaid().compareTo(con.getCost()) >= 0) {
				con.setPaymentStatus(new PaymentStatus(PaymentStatus.PSTAT_PAID));
			}
			
			List<ContractPaymentSchedule> psL = new ArrayList<>();  
			ContractPaymentSchedule fp = new ContractPaymentSchedule();
			fp.setIsFirstpayment(true);
			fp.setPaymentDate(sm.getCdate());
			fp.setPaymentOrder(0);
			fp.setSumm(sm.getFirstPayment());
			fp.setPaid(sm.getFirstPayment());
			fp.setContract(con);
			psL.add(fp);

			Calendar cal = Calendar.getInstance();
			cal.setTime(sm.getCdate());
			
			Calendar thisMonth = Calendar.getInstance();
			thisMonth.set(2018, 6, 1);
			
			BigDecimal paidRemain = paid.subtract(sm.getFirstPayment());
			BigDecimal remain = sm.getPrice().subtract(sm.getFirstPayment());
			Integer overdue = 0;
			for (int i = 1; i <= sm.getMonthTerm(); i++ ) {
				cal.add(Calendar.MONTH, 1);
				ContractPaymentSchedule ps = new ContractPaymentSchedule();
				
				ps.setIsFirstpayment(false);
				ps.setPaymentOrder(i);
				ps.setPaymentDate(cal.getTime());
				
				BigDecimal vznos = sm.getVznos();
				if (i == 1 && !GeneralUtil.isEmptyBigDecimal(sm.getFirstMonthVznos())) {
					vznos = sm.getFirstMonthVznos();
				} else if (i == sm.getMonthTerm()) {
					vznos = remain;
				}
				ps.setSumm(vznos);
				remain = remain.subtract(vznos);
				
				BigDecimal vpaid = new BigDecimal(0);
				if (paidRemain.doubleValue() > 0) {
					vpaid = (paidRemain.compareTo(vznos) > 0 && i < sm.getMonthTerm()) ? vznos : paidRemain;
					paidRemain = paidRemain.subtract(vpaid);
				}
				ps.setPaid(vpaid);
				
				ps.setContract(con);
//				cpsDao.save(ps);
				psL.add(ps);
				
				if (cal.before(thisMonth) 
						&& (ps.getSumm().compareTo(ps.getPaid()) > 0)) 
					overdue++;
			}
			if (overdue > 0) {
				con.setPaymentStatus(new PaymentStatus(PaymentStatus.PSTAT_OVERDUE));
				if (overdue >= 3) {
					con.setPaymentStatus(new PaymentStatus(PaymentStatus.PSTAT_PROBLEM));
				}
			}
			con.getPaymentSchedules().clear();
			con.getPaymentSchedules().addAll(psL);
			return con;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// *****************************************************************************************************
	
	private Contract setPriceAndPaymentScheduleForRentalContract(Contract con, SmContract sm) throws Exception {
		try {
			con.setCurrency(new Currency(sm.getCurrency()));
			
			BigDecimal cost = sm.getFirstPayment().add(sm.getVznos().multiply(new BigDecimal(sm.getMonthTerm())));
			BigDecimal paid = sm.getFirstPayment().add(sm.getPaidVznos());
			con.setCost(cost);
			con.setDiscount(BigDecimal.ZERO);
			con.setFromDealerSumm(BigDecimal.ZERO);
			con.setSumm(cost);
			con.setPaid(paid);
			
			con.setPaymentStatus(new PaymentStatus(PaymentStatus.PSTAT_REGULAR));
			if (con.getPaid().compareTo(con.getCost()) >= 0) {
				con.setPaymentStatus(new PaymentStatus(PaymentStatus.PSTAT_PAID));
			}
			
			List<ContractPaymentSchedule> psL = new ArrayList<>();
			ContractPaymentSchedule fp = new ContractPaymentSchedule();
			fp.setIsFirstpayment(true);
			fp.setPaymentDate(sm.getCdate());
			fp.setPaymentOrder(0);
			fp.setSumm(sm.getFirstPayment());
			fp.setPaid(sm.getFirstPayment());
			fp.setContract(con);
			psL.add(fp);
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(sm.getCdate());
			
			Calendar thisMonth = Calendar.getInstance();
			thisMonth.set(2018, 3, 1);
			
			BigDecimal paidRemain = paid.subtract(sm.getFirstPayment());
			Integer overdue = 0;
			for (int i = 1; i <= sm.getMonthTerm(); i++ ) {
				cal.add(Calendar.MONTH, 1);
				ContractPaymentSchedule ps = new ContractPaymentSchedule();
				
				ps.setIsFirstpayment(false);
				ps.setPaymentOrder(i);
				ps.setPaymentDate(cal.getTime());
				ps.setSumm(sm.getVznos());
				
				BigDecimal vpaid = new BigDecimal(0);
				if (paidRemain.doubleValue() > 0) {
					vpaid = (paidRemain.compareTo(sm.getVznos()) > 0) ? sm.getVznos() : paidRemain;
					paidRemain = paidRemain.subtract(vpaid);
				}
				ps.setPaid(vpaid);
				
				ps.setContract(con);
				psL.add(ps);
				
				if (cal.before(thisMonth) 
						&& (ps.getSumm().compareTo(ps.getPaid()) > 0)) 
					overdue++;
			}
			if (overdue > 0) {
				con.setPaymentStatus(new PaymentStatus(PaymentStatus.PSTAT_OVERDUE));
				if (overdue >= 3) {
					con.setPaymentStatus(new PaymentStatus(PaymentStatus.PSTAT_PROBLEM));
				}
			}
			con.getPaymentSchedules().clear();
			con.getPaymentSchedules().addAll(psL);
			return con;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// *****************************************************************************************************
	
	private void initContractType(Contract con, SmContract sm) throws Exception {
		try {
			if (sm.getInventory() != null 
					&& !GeneralUtil.isEmptyInteger(sm.getInventory().getId())) {
				ContractType ct = ctDao.findByComBrInv(con.getCompany().getId(), sm.getInventory().getId());
				if (ct != null) {
					con.setContractType(ct);
				}
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	private void setContractContacts(Contract con) throws Exception {
		try {
			setContractAddresses(con);
			setContractPhones(con);
		} catch (Exception e) {
			throw e;
		}
	}

	private void setContractPhones(Contract con) {
//		Iterator<PhoneNumber> iterator = con.getCustomer().getPhoneNumbers().iterator();
		
		List<PhoneNumber> phL = (List)Arrays.asList(con.getCustomer().getPhoneNumbers().toArray());
		
		Comparator<PhoneNumber> phComparator = new Comparator<PhoneNumber>() {
			@Override
			public int compare(PhoneNumber o1, PhoneNumber o2) {
				return o1.getId().compareTo(o2.getId());
			}
		};
		
		phL.sort(phComparator);
		
		if (phL.size() > 0 && phL.get(0) != null) {
			PhoneNumber phone = phL.get(0);
			con.setPhonePay(phone);
		}
		if (phL.size() > 1 && phL.get(1) != null) {
			PhoneNumber mobile = phL.get(1);
			con.setMobilePay(mobile);
		}
		
		phL = new ArrayList<>();
		phL = (List)Arrays.asList(con.getExploiter().getPhoneNumbers().toArray());
		phL.sort(phComparator);
		
		if (phL.size() > 0 && phL.get(0) != null) {
			PhoneNumber phone = phL.get(0);
			con.setPhoneFact(phone);
		}
		if (phL.size() > 1 && phL.get(1) != null) {
			PhoneNumber mobile = phL.get(1);
			con.setMobileFact(mobile);
		}
	}

	private void setContractAddresses(Contract con) {
		
//		@SuppressWarnings("unchecked")
//		List<Address> addrL = (List)Arrays.asList(con.getCustomer().getAddresses().toArray());
		
//		Set<Address> addrL = con.getCustomer().getAddresses();
		
//		addrL.sort(new Comparator<Address>() {
//			@Override
//			public int compare(Address o1, Address o2) {
//				return o2.getId().compareTo(o1.getId());
//			}
//		});
		
//		Collections.sort(addrL, new Comparator<Address>() {
//			@Override
//			public int compare(Address o1, Address o2) {
//				return o2.getId().compareTo(o1.getId());
//			}
//		});
//		Collections.reverse(addrL);

		Address addrPay = null;
		long max = 0L;
		for (Address addr : con.getCustomer().getAddresses()) {
//			if (addr.getAddressType().equals(AddressType.TYPE_RESIDENCE)) {
//				con.setAddrPay(addr);
//			}
			if (max < addr.getId()) {
				max = addr.getId();
				addrPay = addr;
			}
		}
		con.setAddrPay(addrPay);
		
		Address addrFact = null;
		max = 0L;
		for (Address addr : con.getExploiter().getAddresses()) {
//			if (addr.getAddressType().equals(AddressType.TYPE_RESIDENCE)) {
//				con.setAddrFact(addr);
//			}
			if (max < addr.getId()) {
				max = addr.getId();
				addrFact = addr;
			}
		}
		con.setAddrFact(addrFact);
	}
	
	private Employee migrateStaff(Contract con, String fio, Integer posId) throws Exception {
		try {
			if (!GeneralUtil.isEmptyString(fio)) {
				String fn[] = fio.split(" ");
				String f = null, i = null, o = null;
				if (fn.length > 0 && fn[0] != null) f = fn[0];
				if (fn.length > 1 && fn[1] != null) i = fn[1];
				if (fn.length > 2 && fn[2] != null) o = fn[2];
				
				Employee empl = null;
				
				if (f != null && i != null && o != null) {
					List<Employee> eL = emplDao.findByFIO(
							con.getCompany().getId(), 
							con.getBranch().getId(),
							posId, f, i, o);
					if (eL != null && eL.size() > 0) empl = eL.get(0);
				} else if (f != null && i != null) {
					List<Employee> eL = emplDao.findByFI(
							con.getCompany().getId(), 
							con.getBranch().getId(),
							posId, f, i);
					if (eL != null && eL.size() > 0) empl = eL.get(0);
				} else if (i != null) {
					List<Employee> eL = emplDao.findByF(
							con.getCompany().getId(), 
							con.getBranch().getId(),
							posId, i);
					if (eL != null && eL.size() > 0) empl = eL.get(0);
				} else if (f != null) {
					List<Employee> eL = emplDao.findByL(
							con.getCompany().getId(), 
							con.getBranch().getId(),
							posId, f);
					if (eL != null && eL.size() > 0) empl = eL.get(0);
				}
				
				if (empl == null) {
					List<Party> staffL = null;
					if (f != null && i != null && o != null) {
						staffL = partyDao.findByTypeFIO(con.getCompany().getId(), PartyType.TYPE_STAFF, f, i, o);
					} else if (f != null && i != null) {
						staffL = partyDao.findByTypeFI(con.getCompany().getId(), PartyType.TYPE_STAFF, f, i);
					} else if (i != null) {
						staffL = partyDao.findByTypeF(con.getCompany().getId(), PartyType.TYPE_STAFF, i);
					} else if (f != null) {
						staffL = partyDao.findByTypeL(con.getCompany().getId(), PartyType.TYPE_STAFF, f);
					}
					Party staff = null;
					if (staffL.size() > 0) staff = staffL.get(0);
					
					if (staff == null) {
						staff = new Party();
						staff.setCompany(con.getCompany());
						if (f != null && f.length() > 0) staff.setLastname(f);
						if (i != null && i.length() > 0) staff.setFirstname(i);
						if (o != null && o.length() > 0) staff.setMiddlename(o);
						staff.setIsYur(false);
						staff.setCpuDate(new Date());
						staff.setPartyType(new PartyType(PartyType.TYPE_STAFF));
						staff = partyDao.save(staff);
					}
					
					empl = new Employee();
					empl.setCompany(con.getCompany());
					empl.setBranch(con.getBranch());
					empl.setDateHired(con.getDateSigned());
					empl.setDepartment(new Department(Department.DEP_MARKETING_AND_SALES));
					empl.setParty(staff);
					empl.setPosition(new Position(posId));
					empl.setRegion(getRegionByBranchAndDep(empl.getBranch(), empl.getDepartment().getId()));
					empl.setCpuDate(Calendar.getInstance());
					emplDao.save(empl);
				}
				return empl;
			}
			return null;
		} catch (Exception e) {
			throw e;
		}
	}
	
	private Region getRegionByBranchAndDep(Branch branch, Integer depId) {
		for (Region reg:branch.getRegions()) {
			if (reg.getDepartment().getId() == depId) {
				return reg;
			}
		}
		return null;
	}
	
	private void initContractStatus(Contract con, SmContract sm) {
		ContractStatus cs = new ContractStatus(ContractStatus.STATUS_ACTIVE);
		if (sm.getStorno() != null && sm.getStorno()) {
			cs.setId(ContractStatus.STATUS_CANCEL);
		}
		con.setContractStatus(cs);
//		con.setPaymentStatus(PaymentStatus.PSTAT_REGULAR);
		con.setResigned(sm.getResigned());
		con.setStorno(sm.getStorno());
		con.setStornoDate(sm.getStornoDate());
		con.setIsRent(sm.getIsRent());
		con.setResignedDate(sm.getResignedDate());
		con.setServiceCategory(new ServiceCategory(ServiceCategory.CAT_GREEN));
	}
	
	private void initContractItem(Contract con, SmContract sm) throws Exception {
		try {
			ContractItem ci = new ContractItem();
			ci.setConItemStatus(new ConItemStatus(ConItemStatus.STATUS_INSTALLED));
			ci.setInventory(sm.getInventory());
			ci.setSerialNumber(sm.getSn());
			ci.setGlCode(FinGlAccount.A_LT_FIXED_ASSETS_2410);
			ci.setQuantity(new BigDecimal(1));
			ci.setUnit(new Unit(Unit.UNIT_PCS));
			ci.setWriteoffDate(sm.getCdate());
			ci.setContract(con);
//			ci.setCost(cost);
//			ci.setSumm(summ);
//			ci.setPrice(price);
//			ci.setDiscount(discount);
//			ci.setPriceList(priceList);
			con.getContractItems().add(ci);
		} catch (Exception e) {
			throw e;
		}
	}
	
	private Party migrateCustomer(SmContract sm, Company company) throws Exception {
		try {
			Party customer = null;
			if (!GeneralUtil.isEmptyString(sm.getIinbin())) {
				customer = partyDao.findByIinBin(company.getId(), sm.getIinbin());
			}
			
			if (customer == null) {
				String f = null, i = null, o = null;
				if (sm.getIsYur()) {
					List<Party> cusL = partyDao.findByCompanyName(company.getId(), sm.getCustomerFio());
					if (cusL.size() > 0) customer = cusL.get(0);
				} else {
					String fn[] = sm.getCustomerFio().split(" ");
					if (fn.length > 0 && fn[0] != null) f = fn[0];
					if (fn.length > 1 && fn[1] != null) i = fn[1];
					if (fn.length > 2 && fn[2] != null) o = fn[2];

					if (f != null && i != null && o != null) {
						List<Party> cusL = partyDao.findByExactFIO(company.getId(), f, i, o);
						if (cusL.size() > 0) customer = cusL.get(0);
					} else if (f != null && i != null) {
						List<Party> cusL = partyDao.findByExactFI(company.getId(), f, i);
						if (cusL.size() > 0) customer = cusL.get(0);
					}
				}
				
				if (customer == null 
						|| ((customer != null) 
								&& ((!sm.getIsYur() && 
										(!(!GeneralUtil.isEmptyString(customer.getPassportNumber()) && customer.getPassportNumber().equals(sm.getPassportNo()))
										|| (!GeneralUtil.isEmptyString(customer.getFirstname()) && !customer.getFirstname().equals(i)) 
										|| (!GeneralUtil.isEmptyString(customer.getLastname()) && !customer.getLastname().equals(f))
										|| (!GeneralUtil.isEmptyString(customer.getMiddlename()) && !customer.getMiddlename().equals(o))))
									) || (sm.getIsYur()))) {
					customer = new Party();
					if (sm.getIinbin() != null && sm.getIinbin().length() > 0)
						customer.setIinBin(sm.getIinbin());
					customer.setCompany(company);
					customer.setIsYur(sm.getIsYur());
					if (sm.getIsYur()) {
						customer.setCompanyName(sm.getCustomerFio());
					} else {
						if (f != null && f.length() > 0) customer.setLastname(f);
						if (i != null && i.length() > 0) customer.setFirstname(i);
						if (o != null && o.length() > 0) customer.setMiddlename(o);
						customer.setBirthday(sm.getBirthday());
						customer.setPassportNumber(sm.getPassportNo());
						customer.setIssuedInstance(sm.getPassIssuedBy());
						customer.setDateIssue(sm.getPassDateIssue());
						customer.setDateExpire(sm.getPassDateExpire());
					}
					customer.setCpuDate(new Date());
					customer.setPartyType(new PartyType(PartyType.TYPE_CUSTOMER));
					customer.setOldId(sm.getCustomerId());
				} else {
					customer = updatePartyFields(sm, customer);
				}
			} else {
				customer = updatePartyFields(sm, customer);
			}
			initAddr(customer, sm);
			initPhone(customer, sm);
			customer = partyDao.save(customer);
			return customer;
		} catch (Exception e) {
			throw e;
		}
	}

	private Party updatePartyFields(SmContract sm, Party customer) {
		customer.setIsYur(sm.getIsYur());
		customer.setBirthday(sm.getBirthday());
		customer.setPassportNumber(sm.getPassportNo());
		customer.setIssuedInstance(sm.getPassIssuedBy());
		customer.setDateIssue(sm.getPassDateIssue());
		customer.setDateExpire(sm.getPassDateExpire());
		customer.setCpuDate(new Date());
		return customer;
	}
	
	private void initAddr(Party party, SmContract sm) throws Exception {
		try {
			if (!GeneralUtil.isEmptyString(sm.getAddrFact()) && sm.getAddrFact().length() > 1) {
				if (!addrExists(party.getAddresses(), sm.getAddrFact())) {
					Address addr = new Address();
					addr.setAddressType(new AddressType(AddressType.TYPE_RESIDENCE));
					addr.setStreet(sm.getAddrFact());
					addr.setInfo(sm.getAddrFact());
					addr.setParty(party);
					addr.setCpuDate(Calendar.getInstance());
					party.getAddresses().add(addr);
				}
			}
			if (!GeneralUtil.isEmptyString(sm.getAddrReg()) && sm.getAddrReg().length() > 1) {
				if (!addrExists(party.getAddresses(), sm.getAddrReg())) {
					Address addr = new Address();
					addr.setAddressType(new AddressType(AddressType.TYPE_REGISTERED));
					addr.setStreet(sm.getAddrReg());
					addr.setInfo(sm.getAddrReg());
					addr.setParty(party);
					addr.setCpuDate(Calendar.getInstance());
					party.getAddresses().add(addr);
				}
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	private void initPhone(Party party, SmContract sm) throws Exception {
		try {
			String pn[] = sm.getPhone().split("(, |,)");
			for (String p: pn) {
				if (!GeneralUtil.isEmptyString(p) && p.length() > 1) {
					if (!phoneExists(party.getPhoneNumbers(), p)) {
						PhoneNumber ph = new PhoneNumber(p);
						ph.setIsmain(true);
						ph.setIsmobile(true);
						ph.setParty(party);
						ph.setCpuDate(Calendar.getInstance());
						party.getPhoneNumbers().add(ph);
					}
				}
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	private boolean phoneExists(Set<PhoneNumber> phL, String number) {
		for (PhoneNumber ph : phL){
			if (ph.getPhNumber() !=null && ph.getPhNumber().equalsIgnoreCase(number)) return true;
		}
		return false;
	}
	
	private boolean addrExists(Set<Address> addrL, String addr) {
		for (Address a : addrL) {
			if (a.getInfo() != null && a.getInfo().equalsIgnoreCase(addr)) return true;
		}
		return false;
	}

	// ***********************************************_GENERATE_PAYMENT_SCHEDULE_****************************************************
	
	@Override
	public void generatePaymentSchedForSales(Integer offset, Integer limit) {
		try {
			List<SmContract> smConL = smConDao.findFirstN(offset, limit);
//			List<SmContract> smConL = new ArrayList<>();
//			smConL.add(smConDao.findByContractNumber("П001-000180"));
//			smConL.add(smConDao.findByContractNumber("П001-000184"));
//			smConL.add(smConDao.findByContractNumber("П001-000185"));
			
			for (SmContract sm : smConL) {
				Contract con = conDao.getContractByOldId(sm.getId());
				if (con == null) {
					System.out.println(sm.getId());
				}
				
				if (con == null) {
					throw new RuntimeException("Contract [" + sm.getContractNumber() + "] with oldId [" + sm.getId() + "] not found!");
				}
				
				BigDecimal paid = getSmConTotalPaid(sm);
				log.info("PAID = " + paid);
				sm.setPaidVznos(paid);
				
				List<ContractPaymentSchedule> psL = new ArrayList<>();
				if (sm.getIsRent()) {
					psL = getRentalPsL(con, sm, paid);
				} else {
					psL = getSalesPsL(con, sm, paid);
				}
				
				con.getPaymentSchedules().clear();
				con.getPaymentSchedules().addAll(psL);
				
				con.setPaid(sm.getFirstPayment().add(sm.getPaidVznos()));
				BigDecimal price = getTotalPrice(psL);
				con.setCost(price);
				con.setSumm(con.getCost());
				con.setMonth(con.getPaymentSchedules().size() - 1); // Except First Payment
				con.setCurrency(new Currency(Currency.CUR_KZT));
				con = conDao.save(con);
				log.info("PS size: " + con.getPaymentSchedules().size());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	private BigDecimal getTotalPrice(List<ContractPaymentSchedule> psL) {
		BigDecimal price = BigDecimal.ZERO;
		for(ContractPaymentSchedule ps: psL) {
			price = price.add(ps.getSumm());
		}
		return price;
	}
	
	@Autowired
	SmConPaidDao smConPaidDao;
	
	@Autowired
	SmConPaymentDao smPaymentDao;
	
	private BigDecimal getSmConTotalPaid(SmContract sm) {
		
		BigDecimal paid = BigDecimal.ZERO;
		SmConPaid smConPaid = smConPaidDao.finByConNo(sm.getContractNumber());
		if (smConPaid != null && smConPaid.getAmount() != null) paid = smConPaid.getAmount();
		
		if (!GeneralUtil.isEmptyString(sm.getResignConSn())) {
			SmConPaid paidResign = smConPaidDao.finByConNo(sm.getResignConSn());
			if (paidResign != null && paidResign.getAmount() != null) paid = paid.add(paidResign.getAmount());
		}
		
		if (!GeneralUtil.isEmptyString(sm.getTransfConSn())) {
			SmConPaid paidTransf = smConPaidDao.finByConNo(sm.getTransfConSn());
			if (paidTransf != null && paidTransf.getAmount() != null) paid = paid.add(paidTransf.getAmount());
		}
		
		return paid;
	}
	
	
	
	public List<ContractPaymentSchedule> getSalesPsL(Contract con, SmContract sm, BigDecimal paid) {
		try {
			
			List<ContractPaymentSchedule> psL = new ArrayList<>();
			
			ContractPaymentSchedule fp = constructFirstPayment(con, sm);
			psL.add(fp);
			
			int m = 1;
			for (SmConSalesPs sps:sm.getSalesPs()) {
				BigDecimal p = sps.getAmount();
				if (paid.compareTo(p) < 0) p = paid;
				
				Calendar cal = Calendar.getInstance();
				cal.setTime(sps.getPeriod());
				ContractPaymentSchedule ps = constructPaymentSchedule(con, sps.getAmount(), m, cal, p);
				psL.add(ps);
				
				paid = paid.subtract(p);
				m++;
			}
			
			return psL;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	
	public List<ContractPaymentSchedule> getRentalPsL(Contract con, SmContract sm, BigDecimal paid) {
		try {
			
			List<ContractPaymentSchedule> psL = new ArrayList<>();
			
			ContractPaymentSchedule fp = constructFirstPayment(con, sm);
			psL.add(fp);
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(con.getDateSigned());
			
			for (int m = 1; m <= sm.getMonthTerm(); m++) {
				cal.add(Calendar.MONTH, 1);
				
				BigDecimal p = sm.getVznos();
				if (paid.compareTo(p) < 0) p = paid;
				
				ContractPaymentSchedule ps = constructPaymentSchedule(con, sm.getVznos(), m, cal, p);
				psL.add(ps);
				
				paid = paid.subtract(p);
			}
			
			return psL;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	private ContractPaymentSchedule constructPaymentSchedule(Contract con, BigDecimal vznos, int m, Calendar cal, BigDecimal paid) {
		ContractPaymentSchedule ps = new ContractPaymentSchedule();
		ps.setContract(con);
		ps.setIsFirstpayment(false);
		ps.setPaymentOrder(m);
		ps.setSumm(vznos);
		ps.setPaymentDate(cal.getTime());
		ps.setPaid(paid);
		return ps;
	}
	
	private ContractPaymentSchedule constructFirstPayment(Contract con, SmContract sm) {
		ContractPaymentSchedule fp = new ContractPaymentSchedule();
		fp.setIsFirstpayment(true);
		fp.setSumm(sm.getFirstPayment());
		fp.setPaymentDate(con.getDateSigned());
		fp.setContract(con);
		fp.setPaid(fp.getSumm());
		fp.setPaymentOrder(0);
		fp.setRefkey(con.getRefkey());
		return fp;
	}
	
	// ************************************************_GENERATE_FIN_DOCS_********************************************************
	
	@Autowired
	FinDocService findocService;
	
	@Autowired
	InvoiceService invoiceService;
	
	@Override
	public void addFinDocsForNewContracts(Integer limit) {
		try {
			List<Contract> conL = conDao.getNewContractsWithRefkeyNullFirstN(limit);
			System.out.printf("Contracts found %d \n", conL.size());
			
			for (Contract con : conL) {
				if (GeneralUtil.isEmptyString(con.getRefkey())) {
					if (!GeneralUtil.isEmptyOrNegativeSumm(con.getSumm())  
							&& !GeneralUtil.isEmptyOrNegativeSumm(con.getRate())
							&& !GeneralUtil.isEmptyString(con.getInventorySn())
							&& con.getInventory() != null) {
						Invoice invoice = saveNewInvoiceFromContract(con);
						
						String refkey = saveNewContractFinDoc(con, invoice);
						con.setRefkey(refkey);
						con = conDao.save(con);
					}
				}				
				// payrollService.payrollPremisFromNewContract(con);
			}
//			entityManager.flush(); // Commit at the end
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	// ******************************************************************************************************
	
	@Autowired
	CompanyDao companyDao;
	
	public String saveNewContractFinDoc(Contract contract, Invoice invoice) throws Exception {
		Company company = companyDao.findOne(contract.getCompany().getId());
		contract.setCompany(company);
		
		// MainFinDoc
		FinDoc newFd = null;
		int paymentOrder = 0;
		
		for (ContractPaymentSchedule ps: contract.getPaymentSchedules()) {
			BigDecimal ws = ps.getSumm().subtract(ps.getPaid());
			if (ps.getIsFirstpayment()) {
				newFd = FinDocFactory.constructFdFromContractGeneral(contract, contract.getRate(), ps, paymentOrder);
				newFd.setInvoice(invoice);
				BigDecimal ds = ws.divide(contract.getRate(), 12, RoundingMode.HALF_EVEN);
				newFd.setWsumm(ws);
				newFd.setDsumm(ds);
				newFd.setWsummPaid(new BigDecimal(0));
				for (FinEntry fe : newFd.getFinEntries()) {
					fe.setWsumm(ws);
					fe.setDsumm(ds);
				}
				newFd = findocService.saveNewFinDoc(newFd);
				ps.setRefkey(newFd.getRefkey());
			} else if (ws.doubleValue() > 0) {
				// Installment invoices
				
				FinDoc newIR = FinDocFactory.constructFdFromContractGeneral(contract, contract.getRate(), ps, paymentOrder);
				newIR.setParentFindoc(newFd);
				newIR.setRefkeyMain(newFd.getRefkey());
				BigDecimal ds = ws.divide(contract.getRate(), 12, RoundingMode.HALF_EVEN);
				newIR.setWsumm(ws);
				newIR.setDsumm(ds);
				newIR.setWsummPaid(new BigDecimal(0));
				for (FinEntry fe : newIR.getFinEntries()) {
					fe.setWsumm(ws);
					fe.setDsumm(ds);
				}
				newIR = findocService.saveNewFinDoc(newIR);
				ps.setRefkey(newIR.getRefkey());
			}
			paymentOrder++;
		}
		
		return newFd.getRefkey();
	}
	
	// ******************************************************************************************************
	
//	@Autowired
//	StockOutDao stockOutDao;
	
	private Invoice saveNewInvoiceFromContract(Contract newContract) throws Exception {
		Invoice invoice = new Invoice();
		try {
			// InvoiceValidator.validateForContract(newContract);
			invoice.setCompany(newContract.getCompany());
			invoice.setBranch(newContract.getBranch());
			invoice.setBranchImporter(newContract.getBranch());
			invoice.setParty(newContract.getCustomer());
			invoice.setUserAdded(newContract.getRegisteredUser());
			invoice.setData(newContract.getDateSigned());
			invoice.setInvoiceStatus(new InvoiceStatus(InvoiceStatus.STATUS_CLOSED));
			invoice.setScopeType(new ScopeType(ScopeType.SCOPE_INT));
			invoice.setDocId(newContract.getId());
			invoice.setDocno(newContract.getContractNumber());
			
			if (newContract.getIsRent().equals(true)) {
				invoice.setInvoiceType(Invoice.INVOICE_TYPE_RENT);
			} else {
				invoice.setInvoiceType(Invoice.INVOICE_TYPE_SELL);
			}
			invoice.setInvoiceItems(new ArrayList<>());
			List<StockOut> soL = new ArrayList<>();
			for (ContractItem ci : newContract.getContractItems()) {
				InvoiceItem it = new InvoiceItem();
				it.setInventory(ci.getInventory());
				it.setUnit(ci.getUnit());
				it.setInvoice(invoice);
				it.setQuantity(new BigDecimal(0));

				/// Inventories with SN
				if (ci.getInventory().getInvMainCategory().getId().equals(InvMainCategory.CAT_TOVAR)) {
					///// Item From DATABATH with SN
					if (ci.getSerialNumber().length() > 0) {
						StockOut so = addStockOutFromContractItem(newContract, ci);
						so.setInvoiceItem(it);
						it.setChildStockOuts(new ArrayList<>());
						it.getChildStockOuts().add(so);
						it.setQuantity(it.getQuantity().add(so.getQuantity()));
						soL.add(so);
						
						InvItemSn iiSn = new InvItemSn(ci.getSerialNumber());
						iiSn.setInvoiceItem(it);
						it.getChildInvSns().add(iiSn);
					}
				}
				invoice.getInvoiceItems().add(it);
			}
			invoice.setDocId(newContract.getId());
			invoice.setDocno(newContract.getContractNumber());
			invoice = invoiceService.saveInvoice(invoice);
			
			addInvoiceNumber(invoice);
			
//			if (!newContract.getIsRent()) {
//				String refkey = findocService.writeOffContractGoodsFromInvoice(invoice, soL, "MIGRATED", 1L);
//				for (StockOut so : soL) {
//					so.setRefkey(refkey);
//					stockOutDao.save(so);
//				}
//			}
			return invoice;
		} catch (Exception ex) {
			throw ex;
		}
	}
	
	private static final BigDecimal UZ_PROD_COST_USD = new BigDecimal(165); 
	private static final BigDecimal USD_RATE_USD = new BigDecimal(1);
	
	private StockOut addStockOutFromContractItem(Contract con, ContractItem ci) { 
		try {
			StockOut so = new StockOut();
			
			so.setCompany(con.getCompany());
			so.setBranch(con.getBranch());
			so.setBroken(false);
			so.setDateOut(ci.getWriteoffDate());
			so.setDcurrency(con.getCompany().getCurrency());
			so.setDsumm(UZ_PROD_COST_USD);
			so.setRate(USD_RATE_USD);
			so.setWcurrency(so.getDcurrency());
			so.setWsumm(so.getDsumm());
			so.setGlCode(FinGlAccount.A_LT_FIXED_ASSETS_2410);
			so.setInventory(ci.getInventory());
			// so.setInvoiceItem();
			so.setQuantity(ci.getQuantity());
			// so.setRefkey(refkey);
			so.setSerialNumber(ci.getSerialNumber());
			so.setUnit(ci.getUnit());
			if (con.getIsRent()) so.setGenStatus(new StockStatus(StockStatus.STATUS_GEN_RENT));
			else so.setGenStatus(new StockStatus(StockStatus.STATUS_GEN_SOLD));
			
			// so = stockOutDao.save(so);
			return so;
		} catch(Exception e) {
			throw e;
		}
	}

	// *********************************************************************************************
	
	@Autowired
	InvoiceDao invDao;
	
	@Override
	public void addInvoiceNumbers() {
		try {
			List<Invoice> invoiceL = invDao.findAllInvNUmNull();
			for (Invoice invoice : invoiceL) {
				addInvoiceNumber(invoice);
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	private void addInvoiceNumber(Invoice invoice) throws Exception {
		try {
			if (invoice.getInvoiceNumber() == null || invoice.getInvoiceNumber().length() == 0) {
				String invNo = InvoiceNoGenerator.generateInvoiceNo(invoice);
				invoice.setInvoiceNumber(invNo);
				invoice.setDateCreated(Calendar.getInstance());
				invDao.save(invoice);
			}
		} catch (Exception e) {
			throw e;
		}
	}

	// *********************************************************************************************
	
	@Override
	public void replaceDuplicatedStaff() {
		try {
			
			// Get Staff Party list with field 'original' not null. - DONE
			// Loop through the staff list - DONE 

			// Get Employee List of Party & Replace party of employee with original - DONE
			
			// Get Invoice List & Replace party with original - 
			
			// Get FinDoc List & Replace party with original - 
			
			// Get Contract List & Replace party with original - 
			
			// Get ContractAwardList List & Replace party with original - 
			
			List<Party> stL = partyDao.getDuplicatedStaffList(Company.COMPANY_CORDIAL_SERVICE);
			
			for (Party staff : stL) {
				Iterable<Employee> emplL = emplDao.findAllByParty(staff.getId());
				for (Employee e : emplL) {
					e.setParty(staff.getOriginal());
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void appendDealerPremis(Integer cid, Integer bid, Integer month, Integer year) {
		try {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
			
			// de - Date of award to be applied
			Calendar de = GeneralUtil.getCalendar(month, year);
			
			// accrueDate - set all awards accrued until including this month
			Calendar accrueDate = GeneralUtil.getCalendar(9, 2019);
			
			int cawCount = 0;
			List<Contract> conL = conDao.getAllMigratedContractsBtw(cid, bid, 
					GeneralUtil.getCalendar(1, 2014).getTime(), 
					GeneralUtil.getCalendar(10, 2019).getTime());
			
			System.out.println("Contracts found: " + conL.size());
			
			for (Contract con : conL) {
				if (!con.getStorno()
						&& !GeneralUtil.isEmptyInteger(con.getMonth())
						&& con.getInventory().getId() == 1
//						&& con.getMonth() == 60
						) {
					
					Award awDealer = null; 
					if (con.getIsRent()) {
						awDealer = awardDao.findRentPremiByScopePriority(cid, 
								Department.DEP_MARKETING_AND_SALES, 
								Position.POS_DEALER, 
								null, null, Country.COUNTRY_KZ, null,
								con.getInventory().getInvSubCategory().getId(),
								null,
//								de.getTime(), 
								con.getDateSigned(),
								con.getMonth());
					} else {
						awDealer = awardDao.findSalesPremiByScopePriority(cid, 
								Department.DEP_MARKETING_AND_SALES, 
								Position.POS_DEALER, 
								null, null, Country.COUNTRY_KZ, null,
								con.getInventory().getInvSubCategory().getId(),
								null,
//								de.getTime(), 
								con.getDateSigned(),
								con.getMonth());
					}
					if (awDealer != null) {
						List<ContractAwardSchedule> cawL = new ArrayList<>();
//						for (ContractAwardSchedule caw : con.getCawSchedules()) {
//							if (!(caw.getAwardType().getName().equals(AwardType.TYPE_PREMI)
//									&& caw.getPosition().getId() == Position.POS_DEALER)) {
//								cawL.add(caw);
//							}
//						}
						con.getCawSchedules().clear();
						// con.getCawSchedules().addAll(cawL);
						
						for (AwardTemplate awt : awDealer.getAwardTemplates()) {
							for (int i = 0; i < awt.getMonth(); i++) {
								ContractAwardSchedule caw = new ContractAwardSchedule();
								caw.setContract(con);
								caw.setAwardType(new AwardType(AwardType.TYPE_PREMI));
								caw.setCurrency(awDealer.getCurrency());
								caw.setParty(con.getDealer().getParty());
								caw.setPosition(new Position(Position.POS_DEALER));
								caw.setRevertOnCancel(true);
								caw.setSumm(awt.getSumm());
								caw.setRevertSumm(awt.getRevertSumm());
								
								Calendar aps = Calendar.getInstance();
								aps.setTime(con.getDateSigned());
								aps.add(Calendar.MONTH, (awt.getPayrollMonth() + i));
								caw.setDateSchedule(aps.getTime());
								caw.setAccrued(BigDecimal.ZERO);
								
								if (GeneralUtil.calcAgeInMonth(aps.getTime(), accrueDate.getTime()) >= 0) {
	//							if (aps.getTimeInMillis() < de.getTimeInMillis()) {
									System.out.println(df.format(aps.getTime()) + " is before than " + df.format(accrueDate.getTime()));
									caw.setAccrued(caw.getSumm());
									caw.setDateAccrued(aps.getTime());
									caw.setClosed(true);
								}
								con.getCawSchedules().add(caw);
								cawCount++;
							}
						}
					} else {
						throw new Exception("Premi not found! Contract Number: " + con.getContractNumber());
					}
				}
				
//				if (++i % 100 == 0) entityManager.flush();					
			}
			System.out.println("Total new CAWS inserted: " + cawCount);
		} catch (Exception e) {
			e.printStackTrace();
//			throw new RuntimeException(e);
		}
	}

	
	@Autowired
	PayrollDao prlDao;
	
	
	@Override
	public void generateAwardPayrolls(Integer cid, Integer bid, Integer month, Integer year) {
		try {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
			Calendar de = GeneralUtil.getCalendar(month, year);
			System.out.println("Date boundary: " + de.getTime());
			List<Contract> conL = conDao.getAllMigratedContracts(cid, bid);
			
			System.out.println("Contracts found: " + conL.size());
			
			for (Contract con : conL) {
				if (!con.getStorno()) {
					for (ContractAwardSchedule caw : con.getCawSchedules()) {
						if (caw.getAccrued().compareTo(BigDecimal.ZERO) > 0
								&& caw.getDateAccrued().before(de.getTime())
								&& GeneralUtil.isEmptyString(caw.getRefkey())) {
							Employee empl = new Employee();
							if (caw.getPosition().getId() == Position.POS_DEALER) empl = con.getDealer();
							else if (caw.getPosition().getId() == Position.POS_DEMOSEC) empl = con.getDemosec();
							else if (caw.getPosition().getId() == Position.POS_CAREMAN) empl = con.getCareman();
							else if (caw.getPosition().getId() == Position.POS_COORDINATOR) empl = con.getCoordinator();
							else if (caw.getPosition().getId() == Position.POS_DIRECTOR) empl = con.getDirector();
							else if (caw.getPosition().getId() == Position.POS_FITTER) empl = con.getFitter();
							else if (caw.getPosition().getId() == Position.POS_MANAGER) empl = con.getManager();
							
							Payroll prl = PayrollFactory.constructPayrollFromContract(con, caw, empl);
							prl.setApproved(true);
							prl.setPosted(true);
							prl.setRestricted(true);
							prl.setUser(new User(1L));
							prlDao.save(prl);
						}
					}
				}
			}		
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void updateContractAddrAndPhones(Integer brId) {
		try {
			List<Contract> conL = conDao.getAllMigratedContracts(Company.COMPANY_CORDIAL_SERVICE, brId);
			for (Contract con : conL) {
				setContractPhones(con);
				conDao.save(con);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
}
