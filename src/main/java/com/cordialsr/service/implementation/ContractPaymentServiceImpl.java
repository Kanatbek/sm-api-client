package com.cordialsr.service.implementation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.CompanyDao;
import com.cordialsr.Dao.ContractDao;
import com.cordialsr.Dao.FinCurrateDao;
import com.cordialsr.Dao.KassaBankDao;
import com.cordialsr.api.kg.asisnur.AsisnurTransaction;
import com.cordialsr.api.kg.mobilnik.MobilnikTransaction;
import com.cordialsr.api.kg.qiwi.QiwiKgTransaction;
import com.cordialsr.api.uz.payme.common.exception.UnableCompleteException;
import com.cordialsr.api.uz.payme.entity.PaymeTransaction;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractPaymentSchedule;
import com.cordialsr.domain.FinCurrate;
import com.cordialsr.domain.KassaBank;
import com.cordialsr.domain.security.User;
import com.cordialsr.dto.ContractPaymentDto;
import com.cordialsr.service.ContractPaymentService;
import com.cordialsr.service.FinDocService;

@Service
@Transactional(rollbackFor = {Exception.class, UnableCompleteException.class})
public class ContractPaymentServiceImpl implements ContractPaymentService {

	@Autowired
	ContractDao conDao;
	
	@Autowired
	CompanyDao companyDao;
	
	// **************************************PAYME*UZ****************************************************
	
	@Override
	public boolean postPaymeCustomerPayment(PaymeTransaction paymeTr) throws Exception {
		try {
			List<Contract> conL = conDao.getAllContractsByCustomerId(paymeTr.getParty().getId());
			
			if (conL == null || conL.size() == 0) throw new Exception("Договора не найдены!");
			BigDecimal summ = paymeTr.getAmount();
			
			// Get all receivables
			List<ContractPaymentSchedule> cpsL = new ArrayList<>();
			for (Contract con: conL) {
				cpsL.addAll(con.getPaymentSchedules());
			}
			
			// Sort receivables by payment date in ascending order
			cpsL.sort(new Comparator<ContractPaymentSchedule>() {
				@Override
				public int compare(ContractPaymentSchedule o1, ContractPaymentSchedule o2) {
					return o1.getPaymentDate().compareTo(o2.getPaymentDate());
				}
			});
			
			// Post payments for overdue & current & future receivables
			for (ContractPaymentSchedule cps : cpsL) {
				if (summ.compareTo(BigDecimal.ZERO) > 0) {
					if (cps.getSumm().compareTo(cps.getPaid()) > 0) {
						BigDecimal payment = postPaymePayment(cps.getContract(), cps, summ, paymeTr);
						summ = summ.subtract(payment);
					}
				} else break;
			}
			
			if (summ.compareTo(BigDecimal.ZERO) > 0) 
				throw new UnableCompleteException("Payment amount exceeds total receivables.");
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			throw new UnableCompleteException(e.getMessage());
		}
	}
	
	@Autowired
	KassaBankDao kbDao;
	
	@Autowired
	FinCurrateDao rateDao;
	
	@Autowired
	FinDocService fdService;

	private static final String BANK_ACCOUNT_PAYMEUZ = "UZSPAYME";
	private static final Long PAYME_USERID = 56L;
	
	private BigDecimal postPaymePayment(Contract con, ContractPaymentSchedule ps, BigDecimal summ, PaymeTransaction paymeTr) throws Exception {
		try {
			BigDecimal payment = ps.getSumm().subtract(ps.getPaid());
			if (payment.compareTo(summ) > 0) payment = summ;
			
			// DONE - Get KassaBank for Payme - by BANK_ACCOUNT_NUMBER
			// DONE - Get actual ExtRate - Company, Today, External Rate
			// DONE - Initialize ConPaymentDto
			// DONE - Post Payment
			// DONE - Test
			// DONE - Check
			
			KassaBank kb = kbDao.findByAccountNumber(BANK_ACCOUNT_PAYMEUZ);
			
			if (kb != null) {
				FinCurrate rate = rateDao.extRateByDate(
						con.getCompany().getId(), 
						con.getCompany().getCurrency().getCurrency(), 
						con.getCurrency().getCurrency(), 
						new Date());
				
				ContractPaymentDto cpd = new ContractPaymentDto(
						con.getBranch(), new Date(), ps, con, 
						payment, kb, payment, con.getCurrency(), 
						null, BigDecimal.ZERO, con.getCompany().getCurrency(), 
						rate.getRate(), payment, true, "", new User(PAYME_USERID), paymeTr.getPaymeId()); 
				
				fdService.doPostContractPayment(cpd);	
			} else throw new UnableCompleteException("Банковский счет не найден.");
			
			return payment;
		} catch (Exception e) {
			throw e;
		}
	}

	// ***************************************MOBILNIK*KG************************************************

	@Override
	public boolean postMobilnikCustomerPayment(MobilnikTransaction mobilTr)
			throws Exception {
		try {
			List<Contract> conL = conDao.getAllContractsByCustomerId(mobilTr.getParty().getId());
			
			if (conL == null || conL.size() == 0) throw new Exception("Договора не найдены!");
			BigDecimal summ = new BigDecimal(mobilTr.getSumm().doubleValue());
			
			// Get all receivables
			List<ContractPaymentSchedule> cpsL = new ArrayList<>();
			for (Contract con: conL) {
				cpsL.addAll(con.getPaymentSchedules());
			}
			
			// Sort receivables by payment date in ascending order
			cpsL.sort(new Comparator<ContractPaymentSchedule>() {
				@Override
				public int compare(ContractPaymentSchedule o1, ContractPaymentSchedule o2) {
					return o1.getPaymentDate().compareTo(o2.getPaymentDate());
				}
			});
			
			// Post payments for overdue & current & future receivables
			for (ContractPaymentSchedule cps : cpsL) {
				if (summ.compareTo(BigDecimal.ZERO) > 0) {
					if (cps.getSumm().compareTo(cps.getPaid()) > 0) {
						BigDecimal payment = postMobilnikPayment(cps.getContract(), cps, summ, mobilTr);
						summ = summ.subtract(payment);
					}
				} else break;
			}
			
			if (summ.compareTo(BigDecimal.ZERO) > 0) throw new UnableCompleteException("Payment amount exceeds total receivables.");
			
			return true;
		} catch (Exception e) {
			throw e;
		}
	}
	
	private static final String BANK_ACC_KICB_CORDIAL_KG = "KICBCORDIALKG";
	private static final String BANK_ACC_KICB_CORDIAL_BSK = "KICBCORDIALBSK";
	
	private static final Long MOBILNIK_USERID = 60L;
	
	private BigDecimal postMobilnikPayment(Contract con, ContractPaymentSchedule ps, BigDecimal summ, MobilnikTransaction mobilTr) throws Exception {
		try {
			BigDecimal payment = ps.getSumm().subtract(ps.getPaid());
			if (payment.compareTo(summ) > 0) payment = summ;
			
			String bank_account = BANK_ACC_KICB_CORDIAL_KG;
			if (mobilTr.getPayType() == MobilnikTransaction.PayType_CORDIAL_BISHKEK) bank_account = BANK_ACC_KICB_CORDIAL_BSK;
			
			KassaBank kb = kbDao.findByAccountNumber(bank_account);
			
			if (kb != null) {
				FinCurrate rate = rateDao.extRateByDate(
						con.getCompany().getId(), 
						con.getCompany().getCurrency().getCurrency(), 
						con.getCurrency().getCurrency(), 
						new Date());
				
				ContractPaymentDto cpd = new ContractPaymentDto(
						con.getBranch(), new Date(), ps, con, 
						payment, kb, payment, con.getCurrency(), 
						null, BigDecimal.ZERO, con.getCompany().getCurrency(), 
						rate.getRate(), payment, true, "", new User(MOBILNIK_USERID), mobilTr.getTxn_id()); 
				
				fdService.doPostContractPayment(cpd);	
			} else throw new UnableCompleteException("Банковский счет не найден.");
			
			return payment;
		} catch (Exception e) {
			throw e;
		}
	}
	
	
	// ***************************************ASISNUR*KG************************************************

	@Override
	public boolean postAsisnurCustomerPayment(AsisnurTransaction asisTr)
			throws Exception {
		try {
			List<Contract> conL = conDao.getAllContractsByCustomerId(asisTr.getParty().getId());
			
			if (conL == null || conL.size() == 0) throw new Exception("Договора не найдены!");
			BigDecimal summ = new BigDecimal(asisTr.getSumm().doubleValue());
			
			// Get all receivables
			List<ContractPaymentSchedule> cpsL = new ArrayList<>();
			for (Contract con: conL) {
				cpsL.addAll(con.getPaymentSchedules());
			}
			
			// Sort receivables by payment date in ascending order
			cpsL.sort(new Comparator<ContractPaymentSchedule>() {
				@Override
				public int compare(ContractPaymentSchedule o1, ContractPaymentSchedule o2) {
					return o1.getPaymentDate().compareTo(o2.getPaymentDate());
				}
			});
			
			// Post payments for overdue & current & future receivables
			for (ContractPaymentSchedule cps : cpsL) {
				if (summ.compareTo(BigDecimal.ZERO) > 0) {
					if (cps.getSumm().compareTo(cps.getPaid()) > 0) {
						BigDecimal payment = postAsisnurPayment(cps.getContract(), cps, summ, asisTr);
						summ = summ.subtract(payment);
					}
				} else break;
			}
			
			if (summ.compareTo(BigDecimal.ZERO) > 0) throw new UnableCompleteException("Payment amount exceeds total receivables.");
			
			return true;
		} catch (Exception e) {
			throw e;
		}
	}
	
	private static final Long ASISNUR_USERID = 67L;
	
	private BigDecimal postAsisnurPayment(Contract con, ContractPaymentSchedule ps, BigDecimal summ, AsisnurTransaction asisTr) throws Exception {
		try {
			BigDecimal payment = ps.getSumm().subtract(ps.getPaid());
			if (payment.compareTo(summ) > 0) payment = summ;
			
			String bank_account = BANK_ACC_KICB_CORDIAL_KG;
			if (asisTr.getPayType() == AsisnurTransaction.PayType_CORDIAL_BISHKEK) bank_account = BANK_ACC_KICB_CORDIAL_BSK;
			
			KassaBank kb = kbDao.findByAccountNumber(bank_account);
			
			if (kb != null) {
				FinCurrate rate = rateDao.extRateByDate(
						con.getCompany().getId(), 
						con.getCompany().getCurrency().getCurrency(), 
						con.getCurrency().getCurrency(), 
						new Date());
				
				ContractPaymentDto cpd = new ContractPaymentDto(
						con.getBranch(), new Date(), ps, con, 
						payment, kb, payment, con.getCurrency(), 
						null, BigDecimal.ZERO, con.getCompany().getCurrency(), 
						rate.getRate(), payment, true, "", new User(ASISNUR_USERID), asisTr.getTxn_id()); 
				
				fdService.doPostContractPayment(cpd);	
			} else throw new UnableCompleteException("Банковский счет не найден.");
			
			return payment;
		} catch (Exception e) {
			throw e;
		}
	}
		
	// ******************************************FIRST*PAYMENT*******************************************
		
	@Override
	public ImmutablePair<KassaBank, BigDecimal> postFirstPaymentFromNewContract(Contract con) throws Exception {
		try {
			KassaBank kassa = kbDao.getKassaByBrAndCur(con.getServiceBranch().getId(), con.getCurrency().getCurrency());
			if (kassa == null) throw new Exception("Касса " + con.getCurrency().getCurrency() + " в филиале [" + con.getBranch().getBranchName() + "] не найдена!");
			
			FinCurrate rate = rateDao.extRateByDate(
					con.getCompany().getId(), 
					con.getCompany().getCurrency().getCurrency(), 
					con.getCurrency().getCurrency(), 
					con.getDateSigned());
			
			ContractPaymentSchedule firstPayment = con.getPaymentSchedules().get(0);
			if (firstPayment == null) throw new Exception("График первоначальной оплаты не найдена.");
			
			ContractPaymentDto cpd = new ContractPaymentDto(
					con.getBranch(), con.getDateSigned(), firstPayment, con, 
					firstPayment.getSumm(), kassa, firstPayment.getSumm(), con.getCurrency(), 
					null, BigDecimal.ZERO, con.getCompany().getCurrency(), 
					rate.getRate(), firstPayment.getSumm(), true, "Первоначальная оплата Нового Договора", con.getRegisteredUser(), "MSNWCON");
			
			fdService.doPostContractPayment(cpd);
			return new ImmutablePair<KassaBank, BigDecimal>(kassa, firstPayment.getSumm());
		} catch (Exception e) {
			throw e;
		}
	}
	
	// ***************************************QIWI*KG************************************************

		@Override
		public boolean postQiwiKgCustomerPayment(QiwiKgTransaction qiwiTr)
				throws Exception {
			try {
				List<Contract> conL = conDao.getAllContractsByCustomerId(qiwiTr.getParty().getId());
				
				if (conL == null || conL.size() == 0) throw new Exception("Договора не найдены!");
				BigDecimal summ = new BigDecimal(qiwiTr.getSumm().doubleValue());
				
				// Get all receivables
				List<ContractPaymentSchedule> cpsL = new ArrayList<>();
				for (Contract con: conL) {
					cpsL.addAll(con.getPaymentSchedules());
				}
				
				// Sort receivables by payment date in ascending order
				cpsL.sort(new Comparator<ContractPaymentSchedule>() {
					@Override
					public int compare(ContractPaymentSchedule o1, ContractPaymentSchedule o2) {
						return o1.getPaymentDate().compareTo(o2.getPaymentDate());
					}
				});
				
				// Post payments for overdue & current & future receivables
				for (ContractPaymentSchedule cps : cpsL) {
					if (summ.compareTo(BigDecimal.ZERO) > 0) {
						if (cps.getSumm().compareTo(cps.getPaid()) > 0) {
							BigDecimal payment = postQiwiKgPayment(cps.getContract(), cps, summ, qiwiTr);
							summ = summ.subtract(payment);
						}
					} else break;
				}
				
				if (summ.compareTo(BigDecimal.ZERO) > 0) throw new UnableCompleteException("Payment amount exceeds total receivables.");
				
				return true;
			} catch (Exception e) {
				throw e;
			}
		}
		
		private static final Long QIWIKG_USERID = 64L;
		
		private BigDecimal postQiwiKgPayment(Contract con, ContractPaymentSchedule ps, BigDecimal summ, QiwiKgTransaction qiwiKgTr) throws Exception {
			try {
				BigDecimal payment = ps.getSumm().subtract(ps.getPaid());
				if (payment.compareTo(summ) > 0) payment = summ;
				
				KassaBank kb = kbDao.findByAccountNumber(BANK_ACC_KICB_CORDIAL_KG);
				
				if (kb != null) {
					FinCurrate rate = rateDao.extRateByDate(
							con.getCompany().getId(), 
							con.getCompany().getCurrency().getCurrency(), 
							con.getCurrency().getCurrency(), 
							new Date());
					
					ContractPaymentDto cpd = new ContractPaymentDto(
							con.getBranch(), new Date(), ps, con, 
							payment, kb, payment, con.getCurrency(), 
							null, BigDecimal.ZERO, con.getCompany().getCurrency(), 
							rate.getRate(), payment, true, "", new User(QIWIKG_USERID), qiwiKgTr.getTxn_id()); 
					
					fdService.doPostContractPayment(cpd);	
				} else throw new UnableCompleteException("Банковский счет не найден.");
				
				return payment;
			} catch (Exception e) {
				throw e;
			}
		}
	
}
