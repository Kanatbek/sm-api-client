package com.cordialsr.service.implementation;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.CrmDemoDao;
import com.cordialsr.domain.Address;
import com.cordialsr.domain.CrmDemo;
import com.cordialsr.service.CrmDemoService;

@Service
@Transactional
public class CrmDemoServiceImpl implements CrmDemoService {
	@Autowired 
	CrmDemoDao demoDao;
	
	@Override
	public CrmDemo saveDemo(CrmDemo newDemo) throws Exception {
		try {
//			ChargeValidator.validateBasic(newCharge);
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(newDemo.getDemoDate());
			
			
			demoDao.save(newDemo);
			return newDemo;
		} catch (Exception e) {
			throw e;
		}
		
	}
	
	// ***************************************************************************************
	
	@Override
	public CrmDemo updateDemo(CrmDemo demo) throws Exception {
		try {
//			DemoValidator.validateBasic(demo);
			CrmDemo demoOrigin = demoDao.findOne(demo.getId());
			demoOrigin = demo.clone();
			
			
			demoOrigin = demoDao.save(demoOrigin);
			return demoOrigin;
		} catch (Exception e) {
			throw e;
		}
	}
}
