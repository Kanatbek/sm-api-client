package com.cordialsr.service.implementation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.EmployeeDao;
import com.cordialsr.Dao.PartyDao;
import com.cordialsr.Dao.ContractDao;
import com.cordialsr.Dao.CurrencyDao;
import com.cordialsr.Dao.SmVisitCollectorDao;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.SmVisitCollector;
import com.cordialsr.domain.projection.ContractProjection;
import com.cordialsr.dto.SmVisitCollectorDto;
import com.cordialsr.service.SmVisitCollectorService;

@Service
@Transactional(rollbackFor = Exception.class)
public class SmVisitCollectorImpl implements SmVisitCollectorService{

	@Autowired 
	EmployeeDao employeeDao;
	
	@Autowired
	PartyDao partyDao;
	
	@Autowired
	ContractDao contractDao;
	
	@Autowired
	SmVisitCollectorDao smVisitCollectorDao;
	
	@Autowired
	CurrencyDao currencyDao;
	
	@Override
	public SmVisitCollector newCollectorVisit(Date inDate, Date inTime, Date outTime, Long emplId, Long partyId,
			BigDecimal summ, Long contractId, BigDecimal longitude, BigDecimal latitude, String info) throws Exception {
		SmVisitCollector smVisit = null;
		try {
			Employee employee = null;
			Party party = null;
			Contract contract = null;
			Currency mCurrency = null;
			
			if (emplId != null) {
				employee = employeeDao.findById(emplId);
			}
			if (partyId != null) {
				party = partyDao.findAllByParty(partyId);
			}
			if (contractId != null) {
				contract  = contractDao.findOne(contractId);
				if (contract != null && contract.getCurrency() != null) {
					mCurrency = currencyDao.findOne(contract.getCurrency().getCurrency());
				}
			}
			
			smVisit = new SmVisitCollector(null, inTime, outTime, inDate, longitude, latitude,
					summ, null, employee, party, contract, mCurrency, info);			
			smVisit = smVisitCollectorDao.save(smVisit);
			
		} catch (Exception e) {
			throw e;
		}
		return smVisit;
	}

	@Override
	public List<SmVisitCollectorDto> getVisits(Date dt1, Date dt2, Long emplId) throws Exception {
		List<SmVisitCollector> visits = new ArrayList<>();
		List<SmVisitCollectorDto> visitDto = new ArrayList<>();
		visits = smVisitCollectorDao.getVisits(dt1, dt2, emplId);
		for (int i=0;i<visits.size();i++) {
			SmVisitCollectorDto dto = new SmVisitCollectorDto();
				if (visits.get(i) != null 
						&& visits.get(i).getContract() != null
						&& visits.get(i).getContract().getCustomer() != null) {
					if (visits.get(i).getContract().getCustomer().getFullFIO() != null) {
						dto.setCustomerFio(visits.get(i).getContract().getCustomer().getFullFIO());
					} else {
						dto.setCustomerFio(visits.get(i).getContract().getCustomer().getFirstname() + ' ' 
											+ visits.get(i).getContract().getCustomer().getLastname());
					}
				}
				if (visits.get(i) != null
						&& visits.get(i).getParty() != null) {
					if (visits.get(i).getParty().getFullFIO() != null) {
						dto.setCollectorFio(visits.get(i).getParty().getFullFIO());
					} else {
						dto.setCollectorFio(visits.get(i).getParty().getFirstname() + " " 
										  + visits.get(i).getParty().getLastname());
					}
				}
				if(visits.get(i).getInTime() != null && visits.get(i).getOutTime() != null) {
					Calendar inCalendar = Calendar.getInstance();
					Calendar outCalendar = Calendar.getInstance();
					
					inCalendar.setTime(visits.get(i).getInTime());
					outCalendar.setTime(visits.get(i).getOutTime());
					
					int inMinute = inCalendar.getTime().getMinutes();
					int inHour = inCalendar.getTime().getHours();
					
					int outMinute = outCalendar.getTime().getMinutes();
					int outHour = outCalendar.getTime().getHours();
					
					dto.setVisitTime( (outMinute + (outHour * 60)) - ((inHour * 60) + inMinute));
				}
				if (visits.get(i).getInDate() != null) {
					dto.setInDate(visits.get(i).getInDate());
				}
				if (visits.get(i).getSumm() != null) {
					dto.setSumm(visits.get(i).getSumm());
				}
				if (visits.get(i).getToCashBox() != null) {
					dto.setToCashbox(visits.get(i).getToCashBox());
				}
				if (visits.get(i).getCurrency() != null) {
					dto.setCurrency(visits.get(i).getCurrency().getCurrency());
				}
				if (visits.get(i).getInfo() != null) {
					dto.setInfo(visits.get(i).getInfo());
				}
				visitDto.add(dto);
		}
		return visitDto;
	}
	
	
	
	
	// --================================================================================================================================================
	
	@Override
	public List<SmVisitCollectorDto> getCustomersVisit(Date dt1, Date dt2, Long emplId, Long customerId) throws Exception {
		List<SmVisitCollector> visits = new ArrayList<>();
		List<SmVisitCollectorDto> visitDto = new ArrayList<>();
		visits = smVisitCollectorDao.getCustomersVisit(dt1, dt2, emplId, customerId);
		for (int i=0;i<visits.size();i++) {
			SmVisitCollectorDto dto = new SmVisitCollectorDto();
				if (visits.get(i) != null 
						&& visits.get(i).getContract() != null
						&& visits.get(i).getContract().getCustomer() != null) {
					if (visits.get(i).getContract().getCustomer().getFullFIO() != null) {
						dto.setCustomerFio(visits.get(i).getContract().getCustomer().getFullFIO());
					} else {
						dto.setCustomerFio(visits.get(i).getContract().getCustomer().getFirstname() + ' ' 
											+ visits.get(i).getContract().getCustomer().getLastname());
					}
				}
				
				if (visits.get(i) != null
						&& visits.get(i).getParty() != null) {
					if (visits.get(i).getParty().getFullFIO() != null) {
						dto.setCollectorFio(visits.get(i).getParty().getFullFIO());
					} else {
						dto.setCollectorFio(visits.get(i).getParty().getFirstname() + " " 
										  + visits.get(i).getParty().getLastname());
					}
				}
				if(visits.get(i).getInTime() != null && visits.get(i).getOutTime() != null) {
					Calendar inCalendar = Calendar.getInstance();
					Calendar outCalendar = Calendar.getInstance();
					
					inCalendar.setTime(visits.get(i).getInTime());
					outCalendar.setTime(visits.get(i).getOutTime());
					
					int inMinute = inCalendar.getTime().getMinutes();
					int inHour = inCalendar.getTime().getHours();
					
					int outMinute = outCalendar.getTime().getMinutes();
					int outHour = outCalendar.getTime().getHours();
					
					dto.setVisitTime( (outMinute + (outHour * 60)) - ((inHour * 60) + inMinute));
				}
				if (visits.get(i).getInDate() != null) {
					dto.setInDate(visits.get(i).getInDate());
				}
				if (visits.get(i).getSumm() != null) {
					dto.setSumm(visits.get(i).getSumm());
				}
				if (visits.get(i).getToCashBox() != null) {
					dto.setToCashbox(visits.get(i).getToCashBox());
				}
				if (visits.get(i).getCurrency() != null) {
					dto.setCurrency(visits.get(i).getCurrency().getCurrency());
				}
				if (visits.get(i).getInfo() != null) {
					dto.setInfo(visits.get(i).getInfo());
				}
				visitDto.add(dto);
		}
		return visitDto;
	}
	
}
