package com.cordialsr.service.implementation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.AwardDao;
import com.cordialsr.Dao.BranchDao;
import com.cordialsr.Dao.CompanyDao;
import com.cordialsr.Dao.FinCurrateDao;
import com.cordialsr.Dao.RegionDao;
import com.cordialsr.domain.Award;
import com.cordialsr.domain.AwardTemplate;
import com.cordialsr.domain.Branch;
import com.cordialsr.domain.City;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Country;
import com.cordialsr.domain.FinCurrate;
import com.cordialsr.domain.Region;
import com.cordialsr.domain.validator.AwardValidator;
import com.cordialsr.service.AwardService;

@Service
@Transactional
public class AwardServiceImpl implements AwardService {

	@Autowired
	AwardDao awardDao;
	
	@Override
	public Award saveNewAward(Award newAward) throws Exception {
		try {
			AwardValidator.validateBasic(newAward);
				newAward.getAwardTemplates().forEach(at -> {
					at.setAward(newAward);
				if (at.getRevertSumm() == null) {
					at.setRevertSumm(new BigDecimal(0));
				}
			});
			
			newAward.setDateCreated(Calendar.getInstance().getTime());
			newAward.setEnabled(true);
			awardDao.save(newAward);
			return newAward;
		} catch (Exception e) {
			throw e;
		}
		
	}

	// ***********************************************************************************************************

	@Autowired
	RegionDao regionDao;
	
	@Autowired
	BranchDao brDao;
	
	@Autowired
	FinCurrateDao rateDao;
	
	@Autowired 
	CompanyDao comDao;
	
	@Transactional(readOnly = true)
	@Override
	public Award getPremi(Integer cid, Integer did, Integer pos, Integer brid, Integer subcat, Integer invid, Date dt, Integer month, Boolean isRent)
			throws Exception {
		try {
			Company company = comDao.findOne(cid);
			Branch branch = brDao.findOne(brid);
			City city = branch.getCity();
			Country country = branch.getCountry();
			Region region = regionDao.getRegionByBrAndDep(brid, did);
			
			Award aw = null;
			if (isRent != null) {
				if (isRent) {
					aw = awardDao.findRentPremiByScopePriority(cid, did, pos, brid, city.getId(), country.getId(), region.getId(), subcat, invid, dt, month);
					if (aw == null) aw = awardDao.findRentPremiByScopePriority(cid, did, pos, brid, city.getId(), country.getId(), region.getId(), subcat, null, dt, month);
				} else {
					aw = awardDao.findSalesPremiByScopePriority(cid, did, pos, brid, city.getId(), country.getId(), region.getId(), subcat, invid, dt, month);
					if (aw == null) aw = awardDao.findSalesPremiByScopePriority(cid, did, pos, brid, city.getId(), country.getId(), region.getId(), subcat, null, dt, month);
				}
			}
			
			if (aw != null && !aw.getCurrency().getCurrency().equals(country.getLocalCurrency().getCurrency())) {
//				throw new Exception("Award currency[" + aw.getCurrency().getCurrency() 
//						+ "] doesn't satisfy to Local currency[" + country.getLocalCurrency().getCurrency() + "].");
				
				String mainCur = company.getCurrency().getCurrency();
				String awCur = aw.getCurrency().getCurrency();
				String locCur = country.getLocalCurrency().getCurrency();
				
				FinCurrate intRate1 = rateDao.intRate(cid, mainCur, awCur);
				if (intRate1 == null) throw new Exception("Couldn't find Internal Rate for '" + mainCur + "' vs '" + awCur + "'.");
				
				FinCurrate intRate2 = rateDao.intRate(cid, mainCur, locCur);
				if (intRate2 == null) throw new Exception("Couldn't find Internal Rate for '" + mainCur + "' vs '" + locCur + "'.");
				
				aw.setCurrency(country.getLocalCurrency());
				BigDecimal ds = aw.getSumm().divide(intRate1.getRate(), 12, RoundingMode.HALF_EVEN);
				BigDecimal ws = ds.multiply(intRate2.getRate());
				aw.setSumm(ws);
				aw.setCurrency(country.getLocalCurrency());
				for (AwardTemplate at : aw.getAwardTemplates()) {
					ds = at.getSumm().divide(intRate1.getRate(), 12, RoundingMode.HALF_EVEN);
					ws = ds.multiply(intRate2.getRate());
					at.setSumm(ws);
				}
			}
			
			return aw;
				
		} catch(Exception e) {
			throw e;
		}
	}
	
	// ***********************************************************************************************************
	// ***********************************************************************************************************

	@Override
	public List<Award> getAllByExample(Award aw) throws Exception {
		try {
			Example<Award> plEx = Example.of(aw);
			List<Award> res = awardDao.findAll(plEx);
			List<Award> sorted = new ArrayList<Award>();
			if (aw.getScope() != null) {
				if (aw.getScope().getName().equals("BRANCH") 
						&& aw.getBranch() != null 
						&& aw.getBranch().getId() > 0) {
						Integer branchId = aw.getBranch().getId();
						for (int i=0;i<res.size();i++) {
							if (res.get(i).getBranch() != null && res.get(i).getBranch().getId() == branchId) {
								sorted.add(res.get(i));
							}
						}
				} else if(aw.getScope().getName().equals("CITY") 
						&& aw.getCity() != null 
						&& aw.getCity().getId() > 0) {
							Integer cityId = aw.getCity().getId();
							for (int i=0;i<res.size();i++) {
								if (res.get(i).getCity() != null && res.get(i).getCity().getId() == cityId) {
									sorted.add(res.get(i));
								}
							}
				} else if(aw.getScope().getName().equals("COMPANY") 
						&& aw.getCompany() != null 
						&& aw.getCompany().getId() > 0) {
							Integer companyId = aw.getCompany().getId();
							for (int i=0;i<res.size();i++) {
								if (res.get(i).getCompany() != null && res.get(i).getCompany().getId() == companyId) {
									sorted.add(res.get(i));
								}
							}
				} else if(aw.getScope().getName().equals("COUNTRY") 
						&& aw.getCountry() != null 
						&& aw.getCountry().getId() > 0) {
							Integer countryId = aw.getCountry().getId();
							for (int i=0;i<res.size();i++) {
								if (res.get(i).getCountry() != null && res.get(i).getCountry().getId() == countryId) {
									sorted.add(res.get(i));
								}
							}
				} else if(aw.getScope().getName().equals("REGION") 
						&& aw.getRegion() != null 
						&& aw.getRegion().getId() > 0) {
							Integer regionId = aw.getRegion().getId();
							for (int i=0;i<res.size();i++) {
								if (res.get(i).getRegion() != null && res.get(i).getRegion().getId() == regionId) {
									sorted.add(res.get(i));
								}
							}
				} else {
						for (int i=0;i<res.size();i++) {
							sorted.add(res.get(i));
						}
					}
			} else {
				for (int i=0;i<res.size();i++) {
					sorted.add(res.get(i));
				}
			}
			return sorted;
		} catch (Exception e) {
			throw e;
		}
	}
	
	
	// ***********************************************************************************************************

	
	@Override
	public Award updateAward(Award aw) throws Exception {
		try {
			AwardValidator.validateBasic(aw);
//			for (AwardTemplate awT : aw.getAwardTemplates()) {
//				awT.setAward(aw);
//			}
			
			return awardDao.save(aw);
		} catch (CloneNotSupportedException e) {
			throw new Exception(e.getMessage());
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

}
