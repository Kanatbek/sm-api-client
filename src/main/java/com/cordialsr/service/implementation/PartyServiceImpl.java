package com.cordialsr.service.implementation;
import java.util.ArrayList;
import java.util.List;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.EmployeeDao;
import com.cordialsr.Dao.PartyDao;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.validator.PartyValidator;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.PartyService;

@Service
@Transactional(rollbackFor = Exception.class)
public class PartyServiceImpl implements PartyService {

	@Autowired 
	PartyDao partyDao;
	
	@Autowired
	EmployeeDao emplDao;
	
	@Override
	public Party saveParty(Party party) throws Exception {
		try {
			PartyValidator.validateBasic(party);
			party.setCpuDate(Calendar.getInstance().getTime());
			Party res = partyDao.save(party);
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	//  ***************************************************************************************

	@Override
	public Party updateParty(Party party) throws Exception {
		try {
			PartyValidator.validateBasic(party);
			Party partyOrigin = partyDao.findOne(party.getId());
			partyOrigin = party.clone();
			partyOrigin = partyDao.save(partyOrigin);
			return partyOrigin;
		} catch (Exception e) {
			throw e;
		}
	}	
	
	//  ****************************************************************************************
	
	@Override
	public Page<Party> getEmployeeList(Integer cid, Integer bid, Integer posId, Boolean fired, String sortBy, String filter, Pageable pageable) throws Exception {
		try {
			

			if (!GeneralUtil.isEmptyInteger(cid)) {

				Page<Party> pList = null;

				if (filter != null) {
					if (bid == null && posId == null) {
						if (!fired) {
							pList = partyDao.filterPartyByCid(cid, filter, pageable);
						} else {
							pList = partyDao.filterPartyByCidFired(cid, filter, pageable);
						}
					}
					
					if (bid != null && posId == null) {
						if (!fired) {
							pList = partyDao.filterPartyByCidBid(cid, bid, filter, pageable);
						} else {
							pList = partyDao.filterPartyByCidBidFired(cid, bid, filter, pageable);
						}
					}
					
					if (bid == null && posId != null) {
						if (!fired) {
							pList = partyDao.filterPartyByCidPos(cid, posId, filter, pageable);
						} else {
							pList = partyDao.filterPartyByCidPosFired(cid, posId, filter, pageable);
						}
					}
					
					if (bid != null && posId != null) {
						if (!fired) {
							pList = partyDao.filterPartyByCidBidPos(cid, bid, posId, filter, pageable);
						} else {
							pList = partyDao.filterPartyByCidBidPosFired(cid, bid, posId, filter, pageable);
						}
					}
				} else {
					if (sortBy != null) {
						if (sortBy.equals("Firstname")) {		
							if (bid == null && posId == null) {
								if (!fired) {
									pList = partyDao.sortPartyByFirstnameCid(cid, pageable);
								} else {
									pList = partyDao.sortPartyByFirstnameCidFired(cid, pageable);
								}
							}
							
							if (bid != null && posId == null) {
								if (!fired) {
									pList = partyDao.sortPartyByFirstnameCidBid(cid, bid, pageable);
								} else {
									pList = partyDao.sortPartyByFirstnameCidBidFired(cid, bid, pageable);
								}
							}
							
							if (bid == null && posId != null) {
								if (!fired) {
									pList = partyDao.sortPartyByFirstnameCidPos(cid, posId, pageable);
								} else {
									pList = partyDao.sortPartyByFirstnameCidPosFired(cid, posId, pageable);
								}
							}
							
							if (bid != null && posId != null) {
								if (!fired) {
									pList = partyDao.sortPartyByFirstnameCidBidPos(cid, bid, posId, pageable);
								} else {
									pList = partyDao.sortPartyByFirstnameCidBidPosFired(cid, bid, posId, pageable);
								}
							}
						}			
						if (sortBy.equals("Lastname")) {		
							if (bid == null && posId == null) {
								if (!fired) {
									pList = partyDao.sortPartyByLastnameCid(cid, pageable);
								} else {
									pList = partyDao.sortPartyByLastnameCidFired(cid, pageable);
								}
							}
							
							if (bid != null && posId == null) {
								if (!fired) {
									pList = partyDao.sortPartyByLastnameCidBid(cid, bid, pageable);
								} else {
									pList = partyDao.sortPartyByLastnameCidBidFired(cid, bid, pageable);
								}
							}
							
							if (bid == null && posId != null) {
								if (!fired) {
									pList = partyDao.sortPartyByLastnameCidPos(cid, posId, pageable);
								} else {
									pList = partyDao.sortPartyByLastnameCidPosFired(cid, posId, pageable);
								}
							}
							
							if (bid != null && posId != null) {
								if (!fired) {
									pList = partyDao.sortPartyByLastnameCidBidPos(cid, bid, posId, pageable);
								} else {
									pList = partyDao.sortPartyByLastnameCidBidPosFired(cid, bid, posId, pageable);
								}
							}
						}
					} else {
						if (bid == null && posId == null) {
							if (!fired) {
								pList = partyDao.getPartyByCid(cid, pageable);
							} else {
								pList = partyDao.getPartyByCidFired(cid, pageable);
							}
						}
						
						if (bid != null && posId == null) {
							if (!fired) {
								pList = partyDao.getPartyByCidBid(cid, bid, pageable);
							} else {
								pList = partyDao.getPartyByCidBidFired(cid, bid, pageable);
							}
						}
						
						if (bid == null && posId != null) {
							if (!fired) {
								pList = partyDao.getPartyByCidPos(cid, posId, pageable);
							} else {
								pList = partyDao.getPartyByCidPosFired(cid, posId, pageable);
							}
						}
						
						if (bid != null && posId != null) {
							if (!fired) {
								pList = partyDao.getPartyByCidBidPos(cid, bid, posId, pageable);
							} else {
								pList = partyDao.getPartyByCidBidPosFired(cid, bid, posId, pageable);
							}
						}
					}
				}			
				
				
				for (Party p : pList) {
					appendPartyPositions(p, bid, fired);
				}
					
				return pList;	
			} else throw new Exception("Укажите компанию!");
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public void appendPartyPositions(Party p, Integer bid, Boolean fired) throws Exception {
		try {
			List<Employee> emplL = emplDao.findAllByPartyAndBranch(p.getId(), bid);
			String pos = "";
			boolean actual = false;
			for (Employee e : emplL) {
				if (fired) {
					if (e.getDateFired() != null && (e.getBranch().getId() == bid)) {
						pos += (pos.length() > 0) ? ", " : "";
						pos += e.getPosition().getName();
						actual = false;
					}
				}
				if (e.getDateFired() == null && (e.getBranch().getId() == bid)) {
					pos += (pos.length() > 0) ? ", " : "";
					pos += e.getPosition().getName();
					actual = true;
				}
			}
			p.setPositions(pos);
			p.setActual(actual);
		} catch (Exception e) {
			throw e;
		}
	}
	
	private List<Party> getOffsetPage(List<Party> conL, Integer page, Integer size) throws Exception {
		try {
			List<Party> res = new ArrayList<>();
			Integer si = page * size;
			Integer ei = si + size;
			if (ei > conL.size()) ei = conL.size();
			res = conL.subList(si, ei);
			return res;
		} catch (Exception e) {
			throw e;
		}
	}
	
	
}
