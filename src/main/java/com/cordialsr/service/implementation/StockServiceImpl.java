package com.cordialsr.service.implementation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.InvoiceDao;
import com.cordialsr.Dao.OrderDao;
import com.cordialsr.Dao.OrderItemDao;
import com.cordialsr.Dao.StockDao;
import com.cordialsr.Dao.StockOutDao;
import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.FinDocType;
import com.cordialsr.domain.FinEntry;
import com.cordialsr.domain.InvItemSn;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.InvoiceItem;
import com.cordialsr.domain.InvoiceStatus;
import com.cordialsr.domain.StockIn;
import com.cordialsr.domain.StockOut;
import com.cordialsr.domain.StockStatus;
import com.cordialsr.domain.factory.FinDocFactory;
import com.cordialsr.domain.factory.StockFactory;
import com.cordialsr.domain.validator.StockValidator;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.FinDocService;
import com.cordialsr.service.InvoiceService;
import com.cordialsr.service.OrderService;
import com.cordialsr.service.StockService;

@Service
@Transactional(rollbackFor = Exception.class)
public class StockServiceImpl implements StockService{

	@Autowired
	InvoiceService invoiceService;
	
	@Autowired
	FinDocService findocService;
	
	@Autowired
	StockDao stockDao;
	
	@Autowired
	StockOutDao skoutDao;
	
	@Autowired
	InvoiceDao invoiceDao;
	
	@Autowired
	OrderDao orderDao;
	
	@Autowired
	OrderItemDao orderItemDao;
	
	@Autowired
	OrderService orderService;

	// Оприходование Импорт и трансфер
	@Override
	public Boolean saveNewStockIns(FinDoc[] finDocs) throws Exception{
		try {
			// Find Main WI Findoc
			FinDoc fdMain = findMainFindoc(finDocs, FinDocType.TYPE_WI_STOCK_WRITE_IN);
			Invoice invoice  = invoiceService.getInvoiceById(fdMain.getInvoice().getId());
			
			if (invoice.getData().after(fdMain.getDocDate())) {
				throw new Exception("Дата оприходования не может быть позже даты создания накладной");
			}
			
			if (invoice != null) {
				if (invoice.getInvoiceType() == Invoice.INVOICE_TYPE_IMPORT)
					saveNewStockInImport(finDocs, invoice);
				else if (invoice.getInvoiceType() == Invoice.INVOICE_TYPE_TRANSFER)
					saveNewStockInTransfer(fdMain, invoice);
			} else {
				throw new Exception("Invoice not found!");
			}
			
			return true;
		} catch (Exception e) {
			throw e;
		}
	}
	
	private FinDoc findMainFindoc(FinDoc[] finDocs, String fdType) {
		FinDoc fdMain = finDocs[0];
		if (!fdMain.getFinDocType().getCode().equals(fdType)) {
			for (FinDoc fd: finDocs) {
				if (fd.getFinDocType().getCode().equals(fdType)) {
					fdMain = fd;
					break;
				} else {
					continue;
				}
			}
		}
		return fdMain;
	}
	
	// ************************************************************************************************************************

	// Поиск
	@Override
	public List<StockIn> getStockInByGlAndInv(Integer cid, Integer bid, String gl, Integer inv, Integer cnt, Integer sta, Integer prt)
			throws Exception {
		try {
			Iterable<StockIn> siL = new ArrayList<StockIn>(); 
			if (sta == 0) {
				siL = stockDao.getStockInByGlAndInv(cid, bid, gl, inv, cnt);
			} else {
				if (prt == 0) {
					siL = stockDao.getStockInByGlAndInvSta(cid, bid, gl, inv, cnt, sta);
				} else {
					siL = stockDao.getStockInByGlAndInvStaPrt(cid, bid, gl, inv, cnt, sta, prt);
				}
			}
			Integer count = 0;
			List<StockIn> siOutL = new ArrayList<>();
			Iterator<StockIn> i = siL.iterator();
			while (i.hasNext() && count < cnt) {
				StockIn si = i.next();
				siOutL.add(si);
				count += si.getQuantity().intValue();				
			}			
			return siOutL;	
		} catch(Exception e) {
			throw e;
		}
	}
	
	// ************************************************************************************************************************

	// Отправка товаров - Трансфер
	@Override
	public Boolean saveNewStocksOutOperation(Invoice invoice) throws Exception {
		try {
			
			Invoice newInv = invoiceDao.findOne(invoice.getId());
			orderService.deleateFromOrder(newInv);
			
			invoice = saveNewStocksByInvoice(invoice, new StockStatus(StockStatus.STATUS_INT_TRANSFER));
					
			newInv.getInvoiceItems().clear();
			newInv.getInvoiceItems().addAll(invoice.getInvoiceItems());
			newInv.setInvoiceStatus(new InvoiceStatus("PROCESS"));
			
			invoiceDao.save(newInv);
			
			return true;
		} catch(Exception e) {
			throw e;
		}
	}
	// ************************************************************************************************************************
	
	// Отпарвка Трансфер, Выдача Подотчет
	@Override
	public Invoice saveNewStocksByInvoice(Invoice invoice, StockStatus status) throws Exception {
		try {
			List<InvoiceItem> invItems = new ArrayList<InvoiceItem>();
			
			for (InvoiceItem it: invoice.getInvoiceItems()) {
			
				if (it.getChildStockIns().size() == 0) {
					throw new Exception("Invoice Item with inventory: " + it.getInventory().getId() + "stocks is empty!");
				}
				
				InvoiceItem invIt = new InvoiceItem();
				invIt = it.clone();
				
				invIt.setChildStockIns(new ArrayList<StockIn>());
				invIt.setChildInvSns(new ArrayList<InvItemSn>());
				
				for (StockIn stock: it.getChildStockIns()) {
					if (stock.getId() == null) {
						// Case with SN's read from file...
						StockIn si = stockDao.findBySerialNumber(invoice.getCompany().getId(), 
								invoice.getBranch().getId(), it.getInventory().getId(), stock.getSerialNumber().trim());
						
						if (si == null || GeneralUtil.isEmptyLong(si.getId())) {
							throw new Exception("Inventory with SN: " + stock.getSerialNumber().trim() + " not found!");
						}
						
						StockValidator.validateStockForContract(si);
						si.setIntStatus(status);
						si.setInvoiceItem(it);
						invIt.getChildStockIns().add(si);
						
						////////////////InvoiceItem SN ...
						InvItemSn sn = new InvItemSn();
						sn.setSerialNumber(stock.getSerialNumber());
						sn.setInvoiceItem(it);
						invIt.getChildInvSns().add(sn);
					}
					else {
						StockIn stockOrig = stockDao.findOne(stock.getId());
						if (stockOrig == null)
							throw new Exception("Stock with id: " + stock.getId() + "is empty!");
						// stock.dsumm is the fact outgoing quantity
						if (stock.getQuantity().equals(stock.getDsumm())) {
							// Update StockIn Status
							StockValidator.validateStockForContract(stockOrig);
							stockOrig.setIntStatus(status);
							stockOrig.setInvoiceItem(it);
							invIt.getChildStockIns().add(stockOrig);
						} else {
							// Update StockIn Quantity & Status
							stockOrig.setQuantity(stockOrig.getQuantity().subtract(stock.getDsumm()));
							stockDao.save(stockOrig);
							
							// New StockIn
							StockIn stk = stockOrig.clone();
							stk.setId(null);
							stk.setQuantity(stock.getDsumm());
							stk.setIntStatus(status);
							stk.setInvoiceItem(it);							
							invIt.getChildStockIns().add(stk);
						}
					}
				}
				invItems.add(invIt);
			}
			
			invoice.setInvoiceItems(invItems);
			
			return invoice;
		} catch(Exception ex) {
			throw ex;
		}
		
	}
	
	// ************************************************************************************************************************

	public InvoiceItem findInvoiceItemById(Invoice invoice, Long iiId) {
		for (InvoiceItem ii: invoice.getInvoiceItems()) {
			if (ii.getId().compareTo(iiId) == 0) return ii;
		}
		return null;
	}
	
	
	// Оприходование ИМПОРТ
	@Override
	public Boolean saveNewStockInImport(FinDoc[] finDocs, Invoice invoice) throws Exception {
		try {
			if (finDocs.length > 0) {
				
				// Find & Save new WI main findoc
				FinDoc fdMain = findMainFindoc(finDocs, FinDocType.TYPE_WI_STOCK_WRITE_IN);
				fdMain.setIsMain(true);
				
				for (FinEntry fe:fdMain.getFinEntries()) {
					InvoiceItem ii = fe.getInvoiceItem();
					InvoiceItem iiOrigin = findInvoiceItemById(invoice, ii.getId());
					if (iiOrigin != null) {
						iiOrigin.setChildStockIns(ii.getChildStockIns());
					}
				}
				
				fdMain.setInvoice(invoice);
				String refkey = findocService.saveNewStockIncomeFinDoc(fdMain);
				
				for (InvoiceItem ii : invoice.getInvoiceItems()) {
					BigDecimal q = new BigDecimal(0);
					for (StockIn si : ii.getChildStockIns()) {
						si.setCompany(invoice.getCompany());
						si.setBranch(invoice.getBranchImporter());
						si.setInventory(ii.getInventory());
						si.setInvoiceItem(ii);
						// si.setQuantity(new BigDecimal(1));
						si.setRefkey(refkey);
						if (!GeneralUtil.isEmptyString(si.getSerialNumber())) {
							si.setSerialNumber(si.getSerialNumber().trim());
						}
						si.setGenStatus(new StockStatus(StockStatus.STATUS_GEN_NEW));
						si.setIntStatus(new StockStatus(StockStatus.STATUS_INT_IN));
						// si.setUnit(fe.getUnit());
						si.setDateIn(fdMain.getDocDate());
						// si.setWcurrency(fe.getWcurrency());
						// si.setDcurrency(fe.getCompany().getCurrency());
						// si.setRate(newFd.getRate());
						BigDecimal wss = si.getDsumm().multiply(si.getRate());
						si.setWsumm(wss);
						
						StockValidator.validateStock(si);
						if (si.getQuantity() != null) {
							q = q.add(si.getQuantity());
						}
					}
					if (ii.getQuantity().intValue() != q.intValue()) {
						throw new Exception("Stock's count is different from InvoiceItem's quantity.");
					}
				}	
					
				invoice.setRefkey(refkey);
				invoice.setUserUpdated(fdMain.getUser());
				invoice.setDateUpdated(Calendar.getInstance());
				invoice.setInvoiceStatus(new InvoiceStatus("CLOSED"));
				invoiceService.saveInvoice(invoice);
				
				// Save SN's into IvoiceItemSn table 
				insertIiSnsFromStockIn(invoice.getId());
				
				// Save WG && WE findocs
				for (FinDoc fd: finDocs) {
					if (GeneralUtil.isEmptyLong(fd.getId())) {
						if (fd.getFinDocType().getCode().equals(FinDocType.TYPE_WE_IMPORT_EXPENCES) ||
								fd.getFinDocType().getCode().equals(FinDocType.TYPE_WG_IMPORT_GTD)) {
							fd = FinDocFactory.constructFdForWEandWG(fd, invoice, fdMain);
							fd = findocService.saveNewFinDoc(fd);
						}
					} else {
						continue;
					}
				}
				
				return true;
			}
			return false;
		} catch (Exception e) {
			throw e;
		}
	}
	
	private boolean insertIiSnsFromStockIn(Long invId) throws Exception {
		try {
			Invoice invoice = invoiceDao.findinv(invId);
			for (InvoiceItem ii: invoice.getInvoiceItems()) {
				List<InvItemSn> iiSnL = new ArrayList<>();
				for (StockIn si: ii.getChildStockIns()) {
					if (!GeneralUtil.isEmptyString(si.getSerialNumber())) {
						InvItemSn iiSn = new InvItemSn();
						iiSn.setSerialNumber(si.getSerialNumber());
						iiSn.setInvoiceItem(ii);
						iiSnL.add(iiSn);
					}
				}
				ii.getChildInvSns().clear();
				ii.getChildInvSns().addAll(iiSnL);
			}
			invoiceDao.save(invoice);
			return true;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// ************************************************************************************************************************
	
	// Оприходование ТРАНСФЕР
	@Override
	public Boolean saveNewStockInTransfer(FinDoc finDoc, Invoice invoice) throws Exception {
		try {
			if (StockValidator.checkStockInSNPersist(invoice)) {
				finDoc.setInvoice(invoice);
				String refkey = findocService.saveNewStockIncomeFinDoc(finDoc);
				invoice.setRefkey(refkey);
				invoice.setUserUpdated(finDoc.getUser());
				invoice.setDateUpdated(Calendar.getInstance());
				invoice.setInvoiceStatus(new InvoiceStatus("CLOSED"));
				
				for (InvoiceItem ii: invoice.getInvoiceItems()) {
					for (StockIn s: ii.getChildStockIns()) {
						s.setBranch(invoice.getBranchImporter());
						s.setIntStatus(new StockStatus(StockStatus.STATUS_INT_IN));
						s.setDateIn(finDoc.getDocDate());
					}
				}
				
				invoiceService.saveInvoice(invoice);
				return true;
			}			  
			return false;
		} catch (Exception e) {
			throw e;
		}
	} 

	@Override
	public StockIn changeStockInByStatus(StockIn stock) throws Exception {
		
		StockIn newStock = stockDao.findOne(stock.getId());
		newStock.setIntStatus(new StockStatus(StockStatus.STATUS_INT_IN));
		stockDao.save(newStock);
		
		return newStock;
	}

	@Override
	public Invoice returnInvoiceService(Invoice invoice, Invoice newInv) throws Exception {
		try {
			for (InvoiceItem it: invoice.getInvoiceItems()) {
				for (InvoiceItem ii: newInv.getInvoiceItems()) {
				
					if (it.getInventory().getId() == ii.getInventory().getId()) {
						
					
						for (StockOut so: it.getChildStockOuts()) {
							StockOut stock = skoutDao.findOne(so.getId());
							if (stock == null) {
								throw new Exception("Stock is empty ...");
							}
							StockIn sk = new StockIn();
							sk = StockFactory.fromStockOutToIn(stock);
							sk.setId(null);
							sk.setDateIn(invoice.getData());
							sk.setInvoiceItem(ii);
							sk.setGenStatus(new StockStatus(StockStatus.STATUS_GEN_RETURNED));
							sk.setIntStatus(new StockStatus(StockStatus.STATUS_INT_IN));
							stockDao.save(sk);
							skoutDao.delete(stock);
						}
						it.getChildStockOuts().clear();
					}
				}
			}
			invoice.setInvoiceStatus(new InvoiceStatus(InvoiceStatus.STATUS_CANCELLED));
			invoiceDao.save(invoice);
			
			return invoice;
		} catch(Exception ex) {
			throw ex;
		}
	}
	
          ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public Boolean changeStocksInContract(String oldSN) throws Exception {
		try {
			StockOut so = skoutDao.findByJustSerialNumber(oldSN);
			
			if (so != null) {
				throw new Exception("Выполните сперва возврат старого товара SN: " + oldSN);
			}
			
			StockIn si = stockDao.findByJustSerialNumber(oldSN);
			if (si != null) {
				si.setGenStatus(new StockStatus(StockStatus.STATUS_GEN_NEW));
				si.setIntStatus(new StockStatus(StockStatus.STATUS_INT_IN));
				si.setInvoiceItem(null);
			}
			
			return true;
		} catch (Exception ex) {
			throw ex;
		}
	}

}
