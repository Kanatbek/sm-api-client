package com.cordialsr.service.implementation;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.FinEntryDao;
import com.cordialsr.Dao.KassaBankDao;
import com.cordialsr.domain.FinEntry;
import com.cordialsr.domain.KassaBank;
import com.cordialsr.domain.validator.KassaBankValidator;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.general.exception.UniqueConstraintException;
import com.cordialsr.service.KassaBankService;

@Service
@Transactional
public class KassaBankServiceImpl implements KassaBankService {

	@Autowired
	KassaBankDao kbDao;
	
	@Autowired
	FinEntryDao feDao;
	
	@Override
	public KassaBank saveNewKb(KassaBank newKb) throws Exception {
		try {
			KassaBankValidator.validateBasic(newKb);
			if (newKb != null) {
				if (newKb.getIsBank()) {
					if (kbDao.findByAccountNumber(newKb.getAccountNumber()) != null) 
						throw new UniqueConstraintException("Account number [" + newKb.getAccountNumber() + "] already exists!");
				} else if (!newKb.getIsBank()) {
					if (kbDao.getKassaByBrAndCur(newKb.getBranch().getId(), newKb.getCurrency().getCurrency()) != null) {
						throw new UniqueConstraintException("Касса " + newKb.getCurrency().getCurrency() + " в этом филиале [" + newKb.getBranch().getBranchName() + "] уже существует!");
					}
				}
				kbDao.save(newKb);
			}
			return newKb;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Object getKassaBalance(Integer kbid) throws Exception {
		try {
			return kbDao.getKassaBalance(kbid, "2099-12-31");
		} catch(Exception e) {
			throw e;
		}
	}

	@Override
	public Object getKassaBalanceByDate(Integer kbid, String date) throws Exception {
		try {
			return kbDao.getKassaBalance(kbid, date);
		} catch(Exception e) {
			throw e;
		}
	}
	
	@Override
	public List<FinEntry> getKbFes(Integer kbid, String ds, String de) throws Exception {
		List<FinEntry> feL = new ArrayList<>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		if (!GeneralUtil.isEmptyString(ds) && ds.length() > 7) {
			Date dsDate = df.parse(ds);
			if (!GeneralUtil.isEmptyString(de) && de.length() > 7) {
				Date deDate = df.parse(de);
				feL = feDao.allByKbFromTill(kbid, dsDate, deDate);
			} else {
				feL = feDao.allByKbFrom(kbid, dsDate);
			}
		} else if (!GeneralUtil.isEmptyString(de) && de.length() > 7) {
			Date deDate = df.parse(de);
			feL = feDao.allByKbTill(kbid, deDate);
		} else {
			feL = feDao.allByKb(kbid);
		}		return feL; 
	}
	
}
