package com.cordialsr.service.implementation;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cordialsr.Dao.PartyEducationDao;
import com.cordialsr.domain.PartyEducation;
import com.cordialsr.service.PartyEducationService;
@Service
@Transactional
public class PartyEducationServiceImpl implements PartyEducationService{

	@Autowired
	PartyEducationDao educationDao;
	
	@Override
	public PartyEducation saveNewEducation(PartyEducation newEducation) throws Exception {
		try {
			educationDao.save(newEducation);
			return newEducation;
		} catch (Exception e) {
			throw e;
		}
	}

}
