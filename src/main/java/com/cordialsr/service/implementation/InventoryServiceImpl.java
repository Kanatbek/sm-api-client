package com.cordialsr.service.implementation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.CompanyDao;
import com.cordialsr.Dao.InvMainCategoryDao;
import com.cordialsr.Dao.InvSubCategoryDao;
import com.cordialsr.Dao.InventoryDao;
import com.cordialsr.Dao.ManufacturerDao;
import com.cordialsr.Dao.UnitDao;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.InvFno;
import com.cordialsr.domain.InvMainCategory;
import com.cordialsr.domain.InvSubCategory;
import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.Manufacturer;
import com.cordialsr.domain.Unit;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.InventoryService;

@Service
@Transactional(rollbackFor = Exception.class)
public class InventoryServiceImpl implements InventoryService {
	
	@Autowired
	private InventoryDao inventoryDao;
	
	@Autowired
	private CompanyDao companyDao;
	
	@Autowired
	private InvMainCategoryDao invMainCategoryDao;
	
	@Autowired
	private InvSubCategoryDao invSubCategoryDao;
	
	@Autowired
	private ManufacturerDao manufacturerDao;
	
	@Autowired
	private UnitDao unitDao;
	
	@Override
	public Inventory saveNewInventory(Inventory newInv) throws Exception {
		try {
			if (inventoryDao.existsByCode(newInv.getCode())) {
				throw new Exception("Inventroy with Code '" + newInv.getCode() + "' already exisits!");
			}
			
			// Validation
			
			Inventory savedInv = inventoryDao.save(newInv);
			return savedInv;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Inventory updateInventory(Inventory newInventory) throws Exception {
		try {
			Inventory changedInventory = null;
			if (!GeneralUtil.isEmptyInteger(newInventory.getId())) {
				changedInventory = inventoryDao.findOne(newInventory.getId());
			} else {
				throw new Exception("Id is null.");
			}
			
			if (changedInventory == null) {
				throw new Exception("Inentory with id [" + newInventory.getId() + "] not found!");
			}
			
			List<InvFno> invFno = new ArrayList<>();
			
			Integer count = 0;
			for (InvFno inf: newInventory.getInvFno()) {
				count = 0;
				for (InvFno invf: changedInventory.getInvFno()) {
					
					if (inf.getFno().equals(invf.getFno())) {
							invFno.add(invf);
							count ++;
							break;
					}
					
				}
				if (count == 0) {
					invFno.add(inf);
				}
			}

			changedInventory = newInventory.clone();
			changedInventory.setInvFno(invFno);
			inventoryDao.save(changedInventory);
			
			return changedInventory;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<Inventory> getInventoryByCompany(Integer cid) throws Exception {
		try {
			List<Inventory> inventories = (List<Inventory>) inventoryDao.findAllByCompany(cid);
			return inventories;
		} catch(Exception e) {
			throw e;
		}
	}	
}
