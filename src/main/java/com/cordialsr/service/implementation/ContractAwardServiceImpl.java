package com.cordialsr.service.implementation;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.ContractAwardScheduleDao;
import com.cordialsr.Dao.ContractDao;
import com.cordialsr.Dao.EmployeeDao;
import com.cordialsr.domain.Award;
import com.cordialsr.domain.AwardType;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractAwardSchedule;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.Payroll;
import com.cordialsr.domain.Position;
import com.cordialsr.domain.factory.ConAwSchFactory;
import com.cordialsr.service.ContractAwardService;

@Service
@Transactional
public class ContractAwardServiceImpl implements ContractAwardService {

	@Autowired
	ContractAwardScheduleDao cawDao;
	
	@Autowired
	ContractDao conDao;
	
	@Autowired
	EmployeeDao emplDao;
	
	@Override
	public void updateContractAwards(List<Contract> conL, Award award, Employee empl) throws Exception {
		try {
			for (Contract con:conL) {
				List<ContractAwardSchedule> oldCawL = ConAwSchFactory.getContractAwL(con, empl.getPosition().getId(), award.getAwardType().getName(), empl.getParty().getId());
				BigDecimal summ = new BigDecimal(0);
				BigDecimal accruedSumm = new BigDecimal(0);
				for (ContractAwardSchedule caw : oldCawL) {
					summ = summ.add(caw.getSumm());
					accruedSumm = accruedSumm.add(caw.getAccrued());
				}
				if (summ.compareTo(award.getSumm()) != 0 || oldCawL.size() != award.getPremiDiv()) {
					List<ContractAwardSchedule> cawL = ConAwSchFactory.constructCawLFor(con, empl, award, accruedSumm);

					// Remove old Bonus awardSchedules from Contract
					for (ContractAwardSchedule caw : oldCawL) {
						con.getCawSchedules().remove(caw);
					}
					
					// Add new constructed Bonus awardSchedules && save Contract
					con.getCawSchedules().addAll(cawL);
					conDao.save(con);
				}
			}
		} catch(Exception e) {
			throw e;
		}
	}
	
	@Override
	public ContractAwardSchedule updateConAwardSchedule(Payroll p) throws Exception {
		try {
			ContractAwardSchedule caw = cawDao.findOne(p.getConAward().getId());
			if (caw == null) {
				System.out.println("CAW doesn't exist on: " + p.getContractNumber());
				caw = new ContractAwardSchedule();
				if (p.getContract() != null) {
					Contract con = conDao.findOne(p.getContract().getId());
					p.setContract(con);
					caw.setContract(p.getContract());
				}
				caw.setSumm(p.getSum());
				caw.setPosition(p.getEmployee().getPosition());
				
				Employee empl = getEmplFromContractByPos(p.getContract(), p.getEmployee().getPosition().getId());
				
				if (empl != null) {
					caw.setDateSchedule(p.getPayrollDate());
					caw.setRevertOnCancel(true);
					caw.setParty(empl.getParty());
					caw.setCurrency(p.getCurrency());
					AwardType awt = new AwardType(AwardType.TYPE_PREMI);
					if (p.getKind().equals("B")) awt.setName(AwardType.TYPE_BONUS);
					caw.setAwardType(awt);
					
					p.setEmployee(empl);
					p.setParty(empl.getParty());
				} else {
					throw new Exception(p.getEmployee().getPosition().getName() + " Employee not assigned: " + p.getContractNumber());	
				}
			}
				
			p.setContract(caw.getContract());
			BigDecimal accrued = caw.getAccrued();
			if (accrued == null) accrued = new BigDecimal(0);
			
			BigDecimal deduction = caw.getDeduction();
			if (deduction == null) deduction = new BigDecimal(0);
			
			if (p.getAccrued().compareTo(BigDecimal.ZERO) > 0)
				accrued = accrued.add(p.getAccrued());
			else if (p.getAccrued().compareTo(BigDecimal.ZERO) < 0)
				deduction = deduction.add(p.getAccrued());
			
			caw.setAccrued(accrued);
			caw.setDeduction(deduction);
			caw.setRefkey(p.getRefkey());
			caw.setDateAccrued(p.getPostedDate());
			cawDao.save(caw);
			
			return caw; 
		} catch(Exception e) {
			throw e;
		}
	}
	
	public Employee getEmplFromContractByPos(Contract con, Integer pos) throws Exception {
		try {
			switch (pos) {
				case Position.POS_CAREMAN: return emplDao.findOne(con.getCareman().getId());
				case Position.POS_COLLECTOR: return emplDao.findOne(con.getCollector().getId());
				case Position.POS_COORDINATOR: return emplDao.findOne(con.getCoordinator().getId());
				case Position.POS_DEALER: return emplDao.findOne(con.getDealer().getId());
				case Position.POS_DEMOSEC: return emplDao.findOne(con.getDemosec().getId());
				case Position.POS_DIRECTOR: return emplDao.findOne(con.getDirector().getId());
				case Position.POS_FITTER: return emplDao.findOne(con.getFitter().getId());
				case Position.POS_MANAGER: return emplDao.findOne(con.getManager().getId());
			}
			return null;
		} catch (Exception e) {
			throw e;
		}
	}
	
}
