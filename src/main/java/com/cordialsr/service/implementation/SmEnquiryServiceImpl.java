package com.cordialsr.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.BranchDao;
import com.cordialsr.Dao.SmEnquiryDao;
import com.cordialsr.domain.Branch;
import com.cordialsr.domain.SmEnquiry;
import com.cordialsr.domain.factory.SmEnquiryFactory;
import com.cordialsr.domain.nogenerator.SmEnquiryNoGenerator;
import com.cordialsr.domain.security.User;
import com.cordialsr.domain.validator.SmEnquiryValidator;
import com.cordialsr.security.repository.UserRepository;
import com.cordialsr.service.SendEmailService;
import com.cordialsr.service.SmCallService;
import com.cordialsr.service.SmEnquiryService;

@Service
@Transactional
public class SmEnquiryServiceImpl implements SmEnquiryService{

	@Autowired
	SmEnquiryDao smEnqDao;
	
	@Autowired
	SmCallService smCallService;
	
	@Autowired
	SendEmailService mailService;
	
	@Autowired
	UserRepository userDao;
	
	@Autowired
	BranchDao branchDao;
	
	@Override
	public SmEnquiry saveNewEnquiry(SmEnquiry newEnquiry) throws Exception {
		try {
			
//			mailService.sendNotification();
			
			SmEnquiryValidator.validateNewEnquiry(newEnquiry);
			
			smEnqDao.save(newEnquiry);
			
			if (newEnquiry.getEnquiryNumber() == null || newEnquiry.getEnquiryNumber().length() == 0) {
				String enqNo = SmEnquiryNoGenerator.generateSmEnquiryNo(newEnquiry);
				newEnquiry.setEnquiryNumber(enqNo);
				smEnqDao.save(newEnquiry);
			}
			
			smCallService.newOutgoingCall(newEnquiry);
			
			notificateCaremans(newEnquiry);
			
			return newEnquiry;
		
		} catch(Exception ex) {
			throw ex;
		}
	}

	private void notificateCaremans(SmEnquiry enquiry) throws Exception{
		try {
			User user = new User();
			user = userDao.findByUsername("dev");
			if (user == null) {
				return;
			}
			Branch branch = branchDao.findById(enquiry.getBranch().getId());
			enquiry.setBranch(branch);
			String subject = "Поступила новая заявка";
			String body = SmEnquiryFactory.createHTML(enquiry, user);
		
			mailService.sendNotification(user, subject, body);
		} catch(Exception e) {
			throw e;
		}
	}

	@Override
	public List<SmEnquiry> getAllByExample(SmEnquiry se) throws Exception {
		try {
			Example<SmEnquiry> seEx = Example.of(se);
			List<SmEnquiry> res = smEnqDao.findAll(seEx);
			return res;
		}catch(Exception ex) {
			throw ex;
		}
	}

	@Override
	public SmEnquiry getInvoiceById(Long enqId) throws Exception {
		try {
			SmEnquiry newEnq = smEnqDao.findOne(enqId);
			return newEnq;
		}catch(Exception ex) {
			throw ex;
		}
	}

	@Override
	public SmEnquiry updateEnuiry(SmEnquiry newEnq) throws Exception {
		try {
			SmEnquiry mainEnq = smEnqDao.findOne(newEnq.getId());
			if (newEnq.getSmService() != null) {
				mainEnq.setStatus(SmEnquiry.STATUS_DONE);
			} else {
				mainEnq.setStatus(SmEnquiry.STATUS_CANCEL);
			}
			mainEnq.setContract(newEnq.getContract());
			mainEnq.setSmService(newEnq.getSmService());
			
			return mainEnq;
		}catch(Exception ex) {
			throw ex;
		}
	}

}
