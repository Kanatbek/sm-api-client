package com.cordialsr.service.implementation;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.CompanyDao;
import com.cordialsr.Dao.ContractItemDao;
import com.cordialsr.Dao.ContractPaymentScheduleDao;
import com.cordialsr.Dao.EmployeeDao;
import com.cordialsr.Dao.FinCurrateDao;
import com.cordialsr.Dao.FinDocDao;
import com.cordialsr.Dao.FinEntryDao;
import com.cordialsr.Dao.KassaBankDao;
import com.cordialsr.Dao.PartyDao;
import com.cordialsr.api.uz.payme.common.exception.WrongAmountException;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractPaymentSchedule;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.FinCurrate;
import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.FinDocType;
import com.cordialsr.domain.FinEntry;
import com.cordialsr.domain.FinGlAccount;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.KassaBank;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.Payroll;
import com.cordialsr.domain.StockIn;
import com.cordialsr.domain.StockOut;
import com.cordialsr.domain.factory.FinDocFactory;
import com.cordialsr.domain.nogenerator.FinDocNoGenerator;
import com.cordialsr.domain.security.User;
import com.cordialsr.domain.validator.ContractPaymentValidator;
import com.cordialsr.domain.validator.FinDocValidator;
import com.cordialsr.dto.ContractPaymentDto;
import com.cordialsr.dto.Money;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.security.service.UserService;
import com.cordialsr.service.ContractService;
import com.cordialsr.service.FinDocService;
import com.cordialsr.service.InvoiceService;

@Service
@Transactional(rollbackFor = Exception.class)
public class FinDocServiceImpl implements FinDocService {

	@Autowired
	FinDocDao fdDao;

	@Autowired
	FinEntryDao feDao;

	@Autowired
	KassaBankDao kbDao;

	@Autowired
	CompanyDao companyDao;
	
	@Autowired
	InvoiceService invoiceService;
	
	@Autowired
	EmployeeDao emplDao;
	
	@Autowired
	ContractService conService;
	
	@Autowired
	ContractPaymentScheduleDao cpsDao;
	
	@Autowired
	PartyDao partyDao;
	
	@Autowired
	UserService userService;
	
	// ***********************************************************************************************************
	
	@Override
	public FinDoc saveNewFinDoc(FinDoc newFinDoc) throws Exception {
		try {
			FinDocValidator.validateBasic(newFinDoc);
			recalcFdAndFeSums(newFinDoc);
			FinDocValidator.validateSummAndFinEntries(newFinDoc);
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(newFinDoc.getDocDate());
			newFinDoc.setMonth(cal.get(Calendar.MONTH) + 1);
			newFinDoc.setYear(String.valueOf(cal.get(Calendar.YEAR)));
			newFinDoc.setCpuDate(Calendar.getInstance().getTime());
			newFinDoc.setCpuTime(Calendar.getInstance().getTime());
			
			newFinDoc.setDsumm(newFinDoc.getDsumm().setScale(2, RoundingMode.HALF_UP));
			
			int n = newFinDoc.getFinEntries().size();
			
			for (int i=0;i<n;i++) {
				
				newFinDoc.getFinEntries().get(i).setDsumm(
						newFinDoc.getFinEntries().get(i).getDsumm().setScale(2, RoundingMode.HALF_UP));
			}
			
			newFinDoc = fdDao.save(newFinDoc);
			
			String docNo = FinDocNoGenerator.generateFinDocNo(newFinDoc);
			newFinDoc.setDocno(docNo);
			String refkey = FinDocNoGenerator.generateRefkey(newFinDoc);
			newFinDoc.setRefkey(refkey);
			
			for (FinEntry fe:newFinDoc.getFinEntries()) {
				fe.setDocno(docNo);
				fe.setRefkey(refkey);
				fe.setMonth(cal.get(Calendar.MONTH) + 1);
				fe.setYear(String.valueOf(cal.get(Calendar.YEAR)));
				
			}
			
			fdDao.save(newFinDoc);
		} catch (Exception e) {
			throw e;
		}
		return newFinDoc;
	}
	
	// ***********************************************************************************************************
	
	private void recalcFdAndFeSums(FinDoc finDoc) throws Exception {
		BigDecimal ds = finDoc.getWsumm().divide(finDoc.getRate(), 12, RoundingMode.HALF_EVEN);
		finDoc.setDsumm(ds);
		finDoc.setDsumm(finDoc.getDsumm());
		for (FinEntry fe : finDoc.getFinEntries()) {
			if (fe.getRate().doubleValue() > 0) {
				if (finDoc.getFinDocType().getCode().equalsIgnoreCase(FinDocType.TYPE_EX_EXCHANGE_CURRENCY)) {
//					fe.setDsumm(ds);
//					fe.setRate(finDoc.getRate());
//					if (fe.getDc().equalsIgnoreCase(FinEntry.DC_DEBET)) {
//						BigDecimal rate = fe.getWsumm().divide(fe.getDsumm(), 12, RoundingMode.HALF_EVEN);
//						fe.setRate(rate);
//					}
				} else {
					BigDecimal dsFe = fe.getWsumm().divide(fe.getRate(), 12, RoundingMode.HALF_EVEN);
					fe.setDsumm(dsFe);
					fe.setDcurrency(finDoc.getDcurrency());			
					fe.setWcurrency(finDoc.getWcurrency());
				}
			} else {
				throw new Exception("FinEntry Rate is empty.");
			}
		}
	}
	
	// ***********************************************************************************************************

	@Override
	public FinDoc saveNewKassaRashodFindoc(FinDoc newFinDoc) throws Exception {
		try {
			if (checkIssuingKassa(newFinDoc)) {	
				if (newFinDoc.getParentFindoc() != null
						&& !GeneralUtil.isEmptyLong(newFinDoc.getParentFindoc().getId())) {
					FinDoc parentFd = fdDao.findOne(newFinDoc.getParentFindoc().getId());
					parentFd.setWsummPaid(parentFd.getWsummPaid().add(newFinDoc.getWsumm()));
					BigDecimal dsPaid = parentFd.getWsummPaid().divide(parentFd.getRate(), 12, RoundingMode.HALF_EVEN);
					parentFd.setDsummPaid(dsPaid);
				}
				newFinDoc = this.saveNewFinDoc(newFinDoc);
			}
		} catch (Exception e) {
			throw e;
		}
		return newFinDoc;
	}
	
	@Override
	public void payPremiToStaff(Contract con, KassaBank kb, BigDecimal amount, Party party) throws Exception {
		try {
			FinCurrate rate = fcrDao.extRate(
					kb.getCompany().getId(), kb.getCompany().getCurrency().getCurrency(), kb.getCurrency().getCurrency());
			
			FinDoc fd = FinDocFactory.assembleCashPremiPaymentFd(
					con, kb, amount, party, con.getDateSigned(), rate.getRate(), con.getRegisteredUser(), "MSNWCON");
			
			saveNewKassaRashodFindoc(fd);
		} catch (Exception e) {
			throw e;
		}
	}
	
	// ***********************************************************************************************************
	
	@Override
	public FinDoc saveNewCashIncomeFindoc(FinDoc newFinDoc) throws Exception {
		try {
			KassaBank kb = newFinDoc.getFinEntries().iterator().next().getKassaBank();
			if (kb != null && !GeneralUtil.isEmptyInteger(kb.getId())) {
				kb = kbDao.findOne(kb.getId());
				if (kb.getCurrency().getCurrency().equals(newFinDoc.getWcurrency().getCurrency())) {
						newFinDoc = this.saveNewFinDoc(newFinDoc);
				} else {
					throw new Exception("Selected currency " + newFinDoc.getWcurrency() + " is wrong for "
							+ kb.getName() + " (" + kb.getCurrency() + ")");
				}
			} else {
				throw new Exception("KassaBank is null!");
			}
		} catch (Exception e) {
			throw e;
		}
		return newFinDoc;
	}

	// ***********************************************************************************************************
	
	@Override
	public FinDoc saveFinDoc(FinDoc newFinDoc) throws Exception {
		try {
			if (newFinDoc.getId() != null) {
				throw new Exception("You are trying to persist an already existing FinDoc with id: " + newFinDoc.getId());
			}
			
			if (newFinDoc.getFinDocType().getCode().equals(FinDocType.TYPE_KO_KASSA_RASHOD_ORDER) 
					|| newFinDoc.getFinDocType().getCode().equals(FinDocType.TYPE_BO_BANK_RASHOD_ORDER)) {
				saveNewKassaRashodFindoc(newFinDoc);
			} else if (newFinDoc.getFinDocType().getCode().equals(FinDocType.TYPE_KI_KASSA_INCOME_ORDER) 
					|| newFinDoc.getFinDocType().getCode().equals(FinDocType.TYPE_BI_BANK_INCOME_ORDER)) {
				saveNewCashIncomeFindoc(newFinDoc);
			} else if (newFinDoc.getFinDocType().getCode().equals(FinDocType.TYPE_TR_TRANSFER_MONEY)) {
				saveNewTransferMoneyFindoc(newFinDoc);
			} else if (newFinDoc.getFinDocType().getCode().equals(FinDocType.TYPE_EX_EXCHANGE_CURRENCY)) {
				saveNewExchangeCurrencyFindoc(newFinDoc);
			}
		} catch(Exception ex) {
			throw ex;
		}
		
		return newFinDoc;
	}
	
	// ***********************************************************************************************************
	
	@Override
	public FinDoc getOneFinDoc(Long id) {
		return fdDao.findfd(id);
	}

	@Override
	public FinEntry getOneFinEntry(Long id) {
		return feDao.findOne(id);
	}

	// ***********************************************************************************************************
	
	@Override
	public FinDoc updateFinDoc(FinDoc changedFd, FinDoc newFd) throws Exception {
		try {
			changedFd = newFd.clone();
			
			if (newFd.getFinEntries() != null) {
				List<FinEntry> feL = new ArrayList<>();
				for (FinEntry fe : newFd.getFinEntries()) {
					if (fe.getId() != null && fe.getId() > 0) {
						FinEntry fe2 = feDao.findOne(fe.getId());
						feL.add(fe2);
					} else {
						feL.add(fe);
					}
				}
				changedFd.setFinEntries(feL);
			}

			if (newFd.getCompany() != null) {
				Company com = companyDao.findOne(newFd.getCompany().getId());
				changedFd.setCompany(com);
			}
			changedFd = fdDao.save(changedFd);
		} catch (CloneNotSupportedException e) {
			throw new Exception(e.getMessage());
		} catch (Exception e) {
			throw new Exception(e);
		}
		return changedFd;
	}

	// ***********************************************************************************************************
	
	@Override
	public String saveNewStockIncomeFinDoc(FinDoc newFd) throws Exception {
		try {
			if (newFd.getInvoice() != null && !GeneralUtil.isEmptyLong(newFd.getInvoice().getId())) {
				Invoice invoice = newFd.getInvoice();
				if (invoice != null && !GeneralUtil.isEmptyLong(invoice.getId())) {
					if (invoice.getInvoiceType() == Invoice.INVOICE_TYPE_IMPORT) {
						newFd = FinDocFactory.constructFdForStockInImport(newFd, invoice);
					} else if (invoice.getInvoiceType() == Invoice.INVOICE_TYPE_TRANSFER) {
						newFd = FinDocFactory.constructFdForStockInTransfer(newFd, invoice);
					}
					newFd = saveNewFinDoc(newFd);
					return newFd.getRefkey();
				} else {
					throw new Exception("Invoice [" + newFd.getInvoice().getId() + "] not found.");
				}
			} else {
				throw new Exception("Incoming Invoice is empty.");
			}
		} catch(Exception e) {
			throw e;
		}
	}
	
	// ***********************************************************************************************************

	@Override
	public FinDoc saveNewTransferMoneyFindoc(FinDoc newFinDoc) throws Exception {
		try {
			assignCompanyPartyToFinDoc(newFinDoc);
			if (checkIssuingKassa(newFinDoc)) {
				newFinDoc = this.saveNewFinDoc(newFinDoc);
			}
		} catch (Exception e) {
			throw e;
		}
		return newFinDoc;
	}
	
	private void assignCompanyPartyToFinDoc(FinDoc newFinDoc) throws Exception {
		try {
			Company company = companyDao.findOne(newFinDoc.getCompany().getId());
			if (company != null) {
				Party party = company.getParty();
				newFinDoc.setParty(party);
				for (FinEntry fe:newFinDoc.getFinEntries()) {
					fe.setParty(party);
				}
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	// ***********************************************************************************************************
	
	@Override
	public FinDoc saveNewExchangeCurrencyFindoc(FinDoc newFinDoc) throws Exception {
		try {
			assignCompanyPartyToFinDoc(newFinDoc);
			if (checkIssuingKassa(newFinDoc)) {
				Integer entryCount = newFinDoc.getFinEntries().size();
				for (FinEntry fe: newFinDoc.getFinEntries()) {
					fe.setEntrycount(entryCount);
				}
				newFinDoc = this.saveNewFinDoc(newFinDoc);
			}
		} catch (Exception e) {
			throw e;
		}
		return newFinDoc;
	}
	
	// ***********************************************************************************************************
	
	private boolean checkIssuingKassa(FinDoc newFinDoc) throws Exception {
		try {
			KassaBank kb = null;
			for (FinEntry fe:newFinDoc.getFinEntries()) {
				if (fe.getDc().equalsIgnoreCase(FinEntry.DC_CREDIT)) {
					kb = kbDao.findOne(fe.getKassaBank().getId());
					break;
				}
			}
			if (kb != null && !GeneralUtil.isEmptyInteger(kb.getId())) {
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Map<String, Object> res = kbDao.getKassaBalance(kb.getId(), dateFormat.format(newFinDoc.getDocDate()));
				if (res != null && res.get("balance") != null) {
					Double balance = Double.valueOf(res.get("balance").toString());
					// System.out.println("KASSA BALANCE: " + balance);
					if (kb.getCurrency().getCurrency().equals(newFinDoc.getWcurrency().getCurrency())) {
						if (balance >= newFinDoc.getWsumm().doubleValue()) {						
							return true;
						} else {
							throw new Exception("Insufficient amount of balance in " + kb.getName() 
							+ " = " + balance + " " + kb.getCurrency().getCurrency()
							+ " on " + dateFormat.format(newFinDoc.getDocDate()));
						}
					} else {
						throw new Exception("Selected currency " + newFinDoc.getWcurrency() + " is different for "
								+ kb.getName() + " (" + kb.getCurrency() + ")");
					}
				} else {
					throw new Exception("Insufficient amount of balance in " + kb.getName());
				}				
			} else {
				throw new Exception("KassaBank is null!");
			}
		} catch( Exception e) {
			throw e;
		}
	}
	
	// ***********************************************************************************************************
	
	@Autowired 
	FinCurrateDao fcrDao;
	
	@Override
	public String saveNewContractFinDoc(Contract contract, Invoice invoice) throws Exception {
		Company company = companyDao.findOne(contract.getCompany().getId());
		contract.setCompany(company);
		
		FinCurrate currate = fcrDao.extRate(company.getId(), company.getCurrency().getCurrency(), contract.getCurrency().getCurrency());
		
		// MainFinDoc
		FinDoc newFd = null;
		int paymentOrder = 0;
		for (ContractPaymentSchedule ps: contract.getPaymentSchedules()) {
			if (ps.getIsFirstpayment()) {
				newFd = FinDocFactory.constructFdFromContractGeneral(contract, currate.getRate(), ps, paymentOrder);
				newFd.setInvoice(invoice);
				newFd = saveNewFinDoc(newFd);
				ps.setRefkey(newFd.getRefkey());
			} else {
				// Installment bills
				FinDoc newIR = FinDocFactory.constructFdFromContractGeneral(contract, currate.getRate(), ps, paymentOrder);
				newIR.setParentFindoc(newFd);
				newIR.setRefkeyMain(newFd.getRefkey());
				newIR = saveNewFinDoc(newIR);
				ps.setRefkey(newIR.getRefkey());
			}
			
			paymentOrder++;
		}
		
		if (!GeneralUtil.isEmptyBigDecimal(contract.getDiscount())) {
			FinDoc discountFd = FinDocFactory.constructDiscountFdFromContractSales(contract);
			discountFd.setParentFindoc(newFd);
			discountFd.setRefkeyMain(newFd.getRefkey());
			discountFd = saveNewFinDoc(discountFd);
		}
		
		if (!GeneralUtil.isEmptyBigDecimal(contract.getFromDealerSumm())) {
			Employee dealer =  emplDao.findOne(contract.getDealer().getId());
			contract.setDealer(dealer);
			FinDoc discountFd = FinDocFactory.constructDealerDiscountFdFromContractSales(contract);
			discountFd.setParentFindoc(newFd);
			discountFd.setRefkeyMain(newFd.getRefkey());
			discountFd = saveNewFinDoc(discountFd);
		}
		
		return newFd.getRefkey();
	}

	// ***********************************************************************************************************
	
	@Override
	public ContractPaymentDto doPostContractPayment(ContractPaymentDto cp) throws Exception {
		try {
			ContractPaymentValidator.validateCp(cp);
			
			Contract con = conService.getOneContract(cp.getContract().getId());
			cp.setContract(con);
			
			Iterator<ContractPaymentSchedule> cpsI = con.getPaymentSchedules().iterator();
			while ((cpsI.hasNext()) && (cp.getTotal().compareTo(BigDecimal.ZERO) > 0)) {
				ContractPaymentSchedule ps = cpsI.next();
				if (ps.getSumm().compareTo(ps.getPaid()) > 0) {
					
					BigDecimal payment = ps.getSumm().subtract(ps.getPaid());
					if (payment.compareTo(cp.getTotal()) > 0) payment = cp.getTotal();

					ContractPaymentDto cp2 = cp.clone();
					cp2.setContractPaymentSchedule(ps);
					cp2.setWsumm(payment);
					cp2.setTotal(payment);
					cp2.setPaymentDue(ps.getSumm().subtract(ps.getPaid()));
					ContractPaymentValidator.validateCpSums(cp2);
					
					// BindParentFinDoc
					FinDoc fdIR = fdDao.findByRefkey(ps.getRefkey());
					if (fdIR == null || GeneralUtil.isEmptyLong(fdIR.getId())) {
						throw new Exception("Счет на оплату на " + ps.getPaymentOrder() + " месяц  договора №" + con.getContractNumber() + " не найден!");
					}
					
					// Post Local Currency Payment
					if (cp2.getWsumm().doubleValue() > 0) {
						KassaBank kb = cp.getWcash();
//						if (kb == null || GeneralUtil.isEmptyInteger(kb.getId())) 
//							kb = kbDao.getKassaByBrAndCur(cp.getBranch().getId(), cp.getWcurrency().getCurrency());
						if (kb != null && !GeneralUtil.isEmptyInteger(kb.getId())) {
							FinDoc fd = FinDocFactory.assembleConCashPaymentFdFromCp(cp2, kb, fdIR);
							Long userId = 0l;
							User user = new User();
							if (fd.getUser() != null && fd.getUser().getUserid() > 0) {
								userId = fd.getUser().getUserid();
								user = userService.findById(userId);
								fd.setUser(user);
							}
							saveNewFinDoc(fd);
							
							fdIR.setWsummPaid(fdIR.getWsummPaid().add(payment));
							if (fdIR.getWsumm().setScale(2, RoundingMode.HALF_EVEN).compareTo(fdIR.getWsummPaid()) == 0) {
								fdIR.setClosed(true);
							}
						} else {
							throw new Exception(cp.getWcurrency().getCurrency() + " Cashbox for branch [" + cp.getBranch().getBranchName() + "] not found!");
						}
					}
					
					conService.paymentPosted(ps, payment);
					cp.setTotal(cp.getTotal().subtract(payment));
				}
			}
			return cp;
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public ContractPaymentDto[] doPostContractPayments(ContractPaymentDto[] cpDto) throws Exception {
		try {
			for (ContractPaymentDto cp: cpDto) {
				// Contract con = conService.getOneContract(cp.getContract().getId());
				BigDecimal remain = cp.getContract().getSumm().subtract(cp.getContract().getPaid());
				if (cp.getWsumm().compareTo(remain) > 0) 
					throw new Exception("Сумма превышает общий остаток задолженности по договору!");
				cp = doPostContractPayment(cp);
			}
			return cpDto;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// ***********************************************************************************************************
	
//		@Override
//		public ContractPaymentDto[] doPostContractPaymentsOld(ContractPaymentDto[] cpDto) throws Exception {
//			try {
//				// This method is used when Customer pays in double currency simultaneously
//				for (ContractPaymentDto cp: cpDto) {
//					ContractPaymentValidator.validateCp(cp);
//					System.out.println(cp);
//					
//					Contract con = conService.getOneContract(cp.getContract().getId());
//					cp.setContract(con);
//					
//					ContractPaymentSchedule cpsOrigin = cpsDao.findOne(cp.getContractPaymentSchedule().getId());
//					cp.setContractPaymentSchedule(cpsOrigin);
//					ContractPaymentValidator.validateCpSums(cp);
//					
//					// BindParentFinDoc
//					FinDoc gsFd = fdDao.getGsByContract(con.getId());
//					
//					if (gsFd == null || GeneralUtil.isEmptyLong(gsFd.getId())) {
//						throw new Exception("Первичный финансовый документ договора [ID: " + con.getId() + "] не найден!");
//					}
//					
//					if (cp.getWsumm().doubleValue() > 0) {
//						KassaBank kb = kbDao.getKassaByBrAndCur(cp.getBranch().getId(), cp.getWcurrency().getCurrency());
//						if (kb != null && !GeneralUtil.isEmptyInteger(kb.getId())) {
//							FinDoc fd = FinDocFactory.assembleConCashPaymentFdFromCp(cp, kb, gsFd);
//							System.out.println("FINDOC: " + cp.getWcurrency().getCurrency() + " | " + fd);
//							saveNewFinDoc(fd);
//						} else {
//							throw new Exception(cp.getWcurrency().getCurrency() + "CashBank for branch [" + cp.getBranch().getBranchName() + "]");
//						}
//					}
//					
//					if (cp.getDsumm().doubleValue() > 0) {
//						KassaBank kb = kbDao.getKassaByBrAndCur(cp.getBranch().getId(), cp.getDcurrency().getCurrency());
//						if (kb != null && !GeneralUtil.isEmptyInteger(kb.getId())) {
//							FinDoc fd = FinDocFactory.assembleConCashPaymentFdFromCp(cp, kb, gsFd);
//							System.out.println("FINDOC: " + cp.getDcurrency().getCurrency() + " | " + fd);
//							saveNewFinDoc(fd);
//						} else {
//							throw new Exception(cp.getDcurrency().getCurrency() + "CashBank for branch [" + cp.getBranch().getBranchName() + "]");
//						}
//					}
//					
//					gsFd.setWsummPaid(gsFd.getWsummPaid().add(cp.getTotal()));
//					BigDecimal bd = gsFd.getWsummPaid().divide(gsFd.getRate(), 12, RoundingMode.HALF_EVEN);
//					gsFd.setDsummPaid(bd);
//					
//					if (gsFd.getWsumm().setScale(2, RoundingMode.HALF_EVEN).compareTo(gsFd.getWsummPaid()) == 0) {
//						gsFd.setClosed(true);
//					}
//					
//					ContractPaymentSchedule cps = conService.paymentPosted(cp.getContractPaymentSchedule(), cp.getTotal());
//					cp.setContractPaymentSchedule(cps);
//				}
//				return cpDto;
//			} catch (Exception e) {
//				throw e;
//			}
//		}

	// *********************************************************************************************************************
	// **************************SALARY*PAYROLL*****************************************************************************
	
	@Override
	public String doPostPayroll(Payroll p, String refkeyMain) throws Exception {
		try {
			FinDoc fd = FinDocFactory.assemblePayrollFd(p);
			fd.setRefkeyMain(refkeyMain);
			if (p.getKind().equals(Payroll.KIND_PREMI) || p.getKind().equals(Payroll.KIND_BONUS)) {
				FinDoc mainFd = fdDao.findByRefkey(refkeyMain);
				if (mainFd != null) fd.setParentFindoc(mainFd);
				else throw new Exception("Первичный документ не найден!");
			} 
			fd = saveNewFinDoc(fd);
			
			return fd.getRefkey();
		} catch (Exception e) {
			throw e;
		}
	}
	
	// *********************************************************************************************************************
	// *************************************PAYROLL*TO*TRANSIT*3351*********************************************************
	
	@Override
	public String doPostPayrollToTransit(Payroll p, String refkeyMain) throws Exception {
		try {
			FinDoc mainFd = fdDao.findByRefkey(refkeyMain);
			if (mainFd != null) {
				FinDoc fd = FinDocFactory.assemblePayrollFd(p);
				fd.setRefkeyMain(refkeyMain);
				fd.setParentFindoc(mainFd);
				fd.setIsMain(false);
				for (FinEntry fe: fd.getFinEntries()) {
					if (fe.getDc().equals(FinEntry.DC_CREDIT)) {
						fe.setGlAccount(new FinGlAccount(FinGlAccount.P_ST_PAYROLL_DEBT_TRANSIT_3351));
					}
				}
				fd = saveNewFinDoc(fd);
				return fd.getRefkey();
			} else throw new Exception("Первичный документ не найден!");			
		} catch (Exception e) {
			throw e;
		}
	}

	// *************************************UPDATE*TRANSIT*TO*3350*********************************************************
	
	@Override
	public String doPostPayrollFromTransit(Payroll p) throws Exception {
		try {
			FinDoc fd = fdDao.findByRefkey(p.getRefkey());
			if (fd == null) throw new Exception("Документ начисления не найден! Refkey: " + p.getRefkey());
			fd.setRate(p.getRate());
			BigDecimal ds = fd.getWsumm().divide(fd.getRate(), 12, RoundingMode.HALF_EVEN);
			fd.setDsumm(ds);
			for (FinEntry fe : fd.getFinEntries()) {
				fe.setDsumm(fd.getDsumm());
				fe.setRate(fd.getRate());
				if (fe.getDc().equals(FinEntry.DC_CREDIT)) {
					fe.setGlAccount(new FinGlAccount(FinGlAccount.P_ST_PAYROLL_DEBT_3350));
				}
			}
			fdDao.save(fd);
			return fd.getRefkey();
		} catch (Exception e) {
			throw e;
		}
	}

	// *********************************************************************************************************************
	// *********************************************************************************************************************
	
	@Autowired 
	ContractItemDao ciDao;
	
	@Override
	public FinDoc writeOffContractGoodsFromInvoice(Invoice invoice, List<StockOut> newStockOutList, String trCode, Long userId) throws Exception {
		try {
			Contract contract = conService.findContractByNumber(invoice.getCompany().getId(), invoice.getBranch().getId(), invoice.getDocno());
//			FinDoc mainFd = fdDao.findByInvoiceId(invoice.getId());
			FinDoc mainFd = fdDao.findByRefkey(contract.getRefkey());
			FinDoc fd = null;
			if (mainFd != null) {
				if (!mainFd.getContract().getIsRent()) {
					FinDoc newFd = FinDocFactory.constructFdForStockOutToCustomer(mainFd, invoice, newStockOutList, trCode, userId);
					if (newFd != null && newFd.getFinEntries().size() > 0) {
						fd = saveNewFinDoc(newFd);
					}
				}
			} else {
				throw new Exception("Первичный документ не найден!");
			}
			return fd;
		} catch (Exception e) {
			throw e;
		}
	}
	
	public String writeOffServiceGoodsFromInvoice(Invoice invoice, List<StockOut> newStockOutList, String trCode, Long userId) throws Exception {
		try {
			FinDoc mainFd = fdDao.findByInvoiceId(invoice.getId());
			String refkey = "";
			if (!mainFd.getContract().getIsRent()) {
				FinDoc fd = FinDocFactory.constructFdForStockOutToCustomer(mainFd, invoice, newStockOutList, trCode, userId);
				fd = saveNewFinDoc(fd);
				refkey = fd.getRefkey();	
			}
			return refkey;
		} catch (Exception e) {
			throw e;
		}
	}

	// ********************************************************RETURN**GOODS**FROM**INVOICE****************************************************
	
	@Override
	public FinDoc returnContractGoodsFromInvoice(Invoice invoice, List<StockIn> newStockInList, String trCode, Long userId) throws Exception {
		try {
			Contract contract = conService.findContractByNumber(invoice.getCompany().getId(), invoice.getBranch().getId(), invoice.getDocno());
//			FinDoc mainFd = fdDao.findByInvoiceId(invoice.getId());
			FinDoc mainFd = fdDao.findByRefkey(contract.getRefkey());
			FinDoc fd = null;
			if (mainFd != null) {
				if (!mainFd.getContract().getIsRent()) {
					FinDoc newFd = FinDocFactory.constructFdForStockReturnFromCustomer(mainFd, invoice, newStockInList, trCode, userId);
					if (newFd != null && newFd.getFinEntries().size() > 0) {
						fd = saveNewFinDoc(newFd);
					}
				}
			} else {
				throw new Exception("Первичный документ не найден!");
			}
			return fd;
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public String returnServiceGoodsFromInvoice(Invoice invoice, List<StockOut> newStockOutList, String trCode, Long userId) throws Exception {
		try {
			FinDoc mainFd = fdDao.findByInvoiceId(invoice.getId());
			String refkey = "";
			if (!mainFd.getContract().getIsRent()) {
				FinDoc fd = FinDocFactory.constructFdForStockOutToCustomer(mainFd, invoice, newStockOutList, trCode, userId);
				fd = saveNewFinDoc(fd);
				refkey = fd.getRefkey();	
			}
			return refkey;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// *****************************************************************************************************************************************
	
	// ******************___STORNO___******************КРАСНОЕ*СТОРНО***************************************************************************	
	@Override
	public FinDoc storno(FinDoc fd, String info) throws Exception {
		try {
			FinDoc storno = new FinDoc();
			storno = fd.clone();
			
			storno.setFinDocType(new FinDocType(FinDocType.TYPE_ST_STORNO));
			storno.setParentFindoc(fd);
			storno.setDsumm(fd.getDsumm().multiply(new BigDecimal(-1)));
			storno.setWsumm(fd.getWsumm().multiply(new BigDecimal(-1)));
			storno.setInfo(info);
			
			// ParentFinDoc reverse operation
			FinDoc parentFd = fd.getParentFindoc();
			if (parentFd != null && !GeneralUtil.isEmptyLong(parentFd.getId())) {
				if (fd.getFinDocType().getCode().equals(FinDocType.TYPE_CP_CONTRACT_PAYMENT)) {
					contractPaymentStorno(parentFd, fd.getWsumm(), fd.getDsumm());
				}
			}
			
			return saveNewFinDoc(storno);
		} catch (Exception e) {
			throw e;
		}
	}
	
	private void contractPaymentStorno(FinDoc fd, BigDecimal stornoWsumm, BigDecimal stornoDsumm) throws Exception {
		try {
			fd.setWsummPaid(fd.getWsummPaid().subtract(stornoWsumm));
			fd.setDsummPaid(fd.getDsummPaid().subtract(stornoDsumm));
			
			if (fd.getWsumm().setScale(2, RoundingMode.HALF_EVEN).compareTo(fd.getWsummPaid()) == 0) {
				fd.setClosed(true);
			} else fd.setClosed(false);
			
			// Cancel contract payment, update contract
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public boolean cancelContractFinDoc(Contract contract, String info, Date stornoDate, String trCode, Long userId, boolean stornoAll) throws Exception {
		try {
			FinDoc fdMain = fdDao.findByRefkey(contract.getRefkey());
			if (fdMain != null) {
				
				Date startOfMonth = GeneralUtil.startOfMonth(stornoDate);
				
				if (fdMain.getDocDate().after(startOfMonth) || stornoAll) {
					fdMain.setStorno(true);
					fdMain.setUpdatedDate(stornoDate);
					fdDao.save(fdMain);
				}
				
				List<FinDoc> fdIrL = fdDao.getFdByTypeAndRefkeyMain(fdMain.getRefkey(), FinDocType.TYPE_IR_INVOICE_RECIEVABLE);
				
				for (FinDoc ir : fdIrL) {
					if (ir.getDocDate().after(startOfMonth) || stornoAll) {
						ir.setStorno(true);
						ir.setUpdatedDate(stornoDate);
						fdDao.save(ir);
					}
				}	
				
				stornoFinDocByRefkeyMainAndType(fdMain.getRefkey(), FinDocType.TYPE_DC_DISCOUNT_COMPANY, stornoDate);
				
				return true;
			} else throw new Exception("Первичный документ не найден!");
		} catch (Exception e) {
			throw e;
		}
	}

	private void stornoFinDocByRefkeyMainAndType(String refkey, String fdType, Date stornoDate) throws Exception {
		try {
			List<FinDoc> fdL = fdDao.getFdByTypeAndRefkeyMain(refkey, fdType);
			for (FinDoc fd : fdL) {
				fd.setStorno(true);
				fd.setUpdatedDate(stornoDate);
				fdDao.save(fd);
			}
		} catch(Exception e) {
			throw e;
		}
	}
	
	// ***************************************************************************************************************************
	
	@Override
	public String reissueContractFinDoc(Contract contract) throws Exception {
		try {
			if (cancelContractFinDoc(contract, "Переоформление", contract.getResignedDate(), "MSCONRES", contract.getUpdatedUser().getUserid(), true)) {
				
				Company company = companyDao.findOne(contract.getCompany().getId());
				contract.setCompany(company);
				
				FinCurrate currate = fcrDao.extRate(company.getId(), company.getCurrency().getCurrency(), contract.getCurrency().getCurrency());
				
				// MainFinDoc
				FinDoc newFd = null;
				int paymentOrder = 0;
				for (ContractPaymentSchedule ps: contract.getPaymentSchedules()) {
					if (ps.getIsFirstpayment()) {
						newFd = FinDocFactory.constructFdFromContractGeneral(contract, currate.getRate(), ps, paymentOrder);
						// newFd.setInvoice(invoice);
						newFd = saveNewFinDoc(newFd);
						ps.setRefkey(newFd.getRefkey());
					} else {
						// Installment invoices
						if (ps.getSumm().compareTo(ps.getPaid()) > 0) {
							FinDoc newIR = FinDocFactory.constructFdFromContractGeneral(contract, currate.getRate(), ps, paymentOrder);
							newIR.setParentFindoc(newFd);
							newIR.setRefkeyMain(newFd.getRefkey());
							newIR = saveNewFinDoc(newIR);
							ps.setRefkey(newIR.getRefkey());
						}
					}
					
					paymentOrder++;
				}
				
				if (!GeneralUtil.isEmptyBigDecimal(contract.getDiscount())) {
					FinDoc discountFd = FinDocFactory.constructDiscountFdFromContractSales(contract);
					discountFd.setParentFindoc(newFd);
					discountFd.setRefkeyMain(newFd.getRefkey());
					discountFd = saveNewFinDoc(discountFd);
				}
				
				return newFd.getRefkey();
			} else throw new Exception("Не удалось сторнировать старые документы.");
		} catch (Exception e) {
			throw e;
		}
	}
	
}
