package com.cordialsr.service.implementation;


import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.FinCashflowStatementDao;
import com.cordialsr.Dao.FinGlAccountDao;
import com.cordialsr.Dao.FinOperDao;
import com.cordialsr.domain.FinCashflowStatement;
import com.cordialsr.domain.FinGlAccount;
import com.cordialsr.domain.FinOperation;
import com.cordialsr.domain.validator.CfStatementValidator;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.FinRefService;

@Service
@Transactional
public class FinRefServiceImpl implements FinRefService {

	@Autowired
	FinOperDao foDao;
	
	@Autowired
	FinCashflowStatementDao cfDao;
	
	@Autowired 
	FinGlAccountDao glDao;
	
	@Override
	public FinOperation saveFinOper(FinOperation newFo) throws Exception {
		try {
			if (newFo != null) {
				FinOperation fo = new FinOperation();
				if (!GeneralUtil.isEmptyInteger(newFo.getId())) {
					fo = foDao.findOne(newFo.getId());
				}
				fo = newFo.clone();
				foDao.save(fo);
				return fo;
			}
			return null;
		} catch(Exception e) {
			throw e;
		}
	}

	// ************************************************************************************************
	
	@Override
	public FinCashflowStatement saveCf(FinCashflowStatement newCf) throws Exception {
		try {
			CfStatementValidator.validate(newCf);
			FinCashflowStatement cf = new FinCashflowStatement();
			if (!GeneralUtil.isEmptyInteger(newCf.getId())) {
				cf = cfDao.findOne(newCf.getId());
			}
			// cf = newCf.clone();
			BeanUtils.copyProperties(newCf, cf);
			cfDao.save(cf);
			return cf;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<FinGlAccount> getAllGlAccounts() {
		return (List<FinGlAccount>) glDao.findAll();
	}
}
