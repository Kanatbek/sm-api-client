package com.cordialsr.service.implementation;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.InvItemSnDao;
import com.cordialsr.Dao.StockDao;
import com.cordialsr.Dao.StockOutDao;
import com.cordialsr.domain.InvItemSn;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.InvoiceItem;
import com.cordialsr.domain.StockIn;
import com.cordialsr.domain.StockOut;
import com.cordialsr.service.InvItemSnService;

@Service
@Transactional(rollbackFor = Exception.class)
public class InvItemSnServiceImpl implements InvItemSnService{

	@Autowired
	InvItemSnDao invItemSnDao;

	@Autowired
	StockDao stockDao;
	
	@Autowired
	StockOutDao sout;
	
	@Override
	public ArrayList<Invoice> checkInvItemSns(InvItemSn invItemSn) throws Exception {
		try {
			ArrayList<InvItemSn> newInvItems = new ArrayList<InvItemSn>();
			ArrayList<Invoice> invoices = new ArrayList<Invoice>();
			if (invItemSn == null) {
				throw new Exception("InvItemSn is null");
			} 
			if (invItemSn.getSerialNumber() == null || invItemSn.getSerialNumber().equals("null")) {
				throw new Exception("Serial number is null");
			}
			newInvItems = invItemSnDao.findBySerNum(invItemSn.getSerialNumber());
			if (newInvItems.size() == 0) {
				throw new Exception("Нет историй этого серийного номера");
			}
			
			Invoice inv = new Invoice();
			inv.setInvoiceItems(new ArrayList<InvoiceItem>());
			InvoiceItem invItem = new InvoiceItem();
			
			invItem.setChildStockIns(new ArrayList<StockIn>());
			invItem.setChildStockOuts(new ArrayList<StockOut>());
			
			StockIn si = stockDao.findByJustSerialNumber(invItemSn.getSerialNumber());
			if (si == null) {
				StockOut so = sout.findByJustSerialNumber(invItemSn.getSerialNumber());
				if (so == null) {
					throw new Exception("Товар был удален в ручную");
				}
				invItem.getChildStockOuts().add(so);
			} else {
				invItem.getChildStockIns().add(si);
			}
			inv.getInvoiceItems().add(invItem);
			invoices.add(inv);
			
			for (InvItemSn invSn: newInvItems) {
				invoices.add(invSn.getInvoiceItem().getInvoice());
			}
			
			return invoices;
		} catch(Exception ex) {
			throw ex;
		}
	}

}
