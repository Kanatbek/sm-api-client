package com.cordialsr.service.implementation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.BranchDao;
import com.cordialsr.Dao.CompanyDao;
import com.cordialsr.Dao.ContractPaymentScheduleDao;
import com.cordialsr.Dao.EmployeeDao;
import com.cordialsr.Dao.PartyDao;
import com.cordialsr.Dao.PlanFactDao;
import com.cordialsr.Dao.PositionDao;
import com.cordialsr.Dao.RegionDao;
import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.ContractPaymentSchedule;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Department;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.PlanFact;
import com.cordialsr.domain.Position;
import com.cordialsr.domain.Region;
import com.cordialsr.domain.SmFilterChange;
import com.cordialsr.domain.SmService;
import com.cordialsr.domain.reducer.BranchReducer;
import com.cordialsr.domain.reducer.PartyReducer;
import com.cordialsr.domain.validator.PlanFactValidator;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.PlanFactService;
import com.cordialsr.service.SmFilterChangeService;

@Service
@Transactional
public class PlanFactServiceImpl implements PlanFactService {
	
	@Autowired 
	PlanFactDao pfDao;
	
	@Autowired 
	CompanyDao comDao;
	
	@Autowired 
	BranchDao brDao;
	
	@Autowired 
	PartyDao partyDao;
	
	@Autowired
	ContractPaymentScheduleDao conPaySchDao;
	
	@Autowired 
	EmployeeDao emplDao;

	@Autowired
	RegionDao regDao;
	
	@Autowired
	PositionDao posDao;

	@Autowired 
	SmFilterChangeService smfcService;
	
	@Override
	public List<PlanFact> getPlanFact(Integer cid, Integer bid, Integer pos, Date dt) throws Exception {
		List<PlanFact> res = new ArrayList<>();
		if (pos == Position.POS_COLLECTOR) {
			res = getCollectorPlanFact(cid, bid, dt);
		} else if (pos == Position.POS_DIRECTOR) {
//			res = getDirectorsPlanFact(cid, dt);
		}
		return res;
	}
	
	
	@Override
	public List<PlanFact> getPlan(Integer cid, Integer bid, Integer pos, Date dt) throws Exception {
		List<PlanFact> res = new ArrayList<>();
		if (pos == Position.POS_COLLECTOR) {
			res = getCollectorPlan(cid, bid, dt);
		} else if (pos == Position.POS_DIRECTOR) {
			res = getDirectorsPlan(cid, dt);
		} else if (pos == Position.POS_CAREMAN) {
			res = getCaremanPlan(cid, bid, dt);
		}
		return res;
	}
	
	public List<PlanFact> getCaremanPlan(Integer cid, Integer bid, Date dt) throws Exception {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		Integer year = cal.get(Calendar.YEAR);
		Integer month = cal.get(Calendar.MONTH) + 1;
		
		List<PlanFact> pfOld = pfDao.getPlanFactBraByPosForMonthYear(cid, bid, Position.POS_CAREMAN, year, month);
		List<PlanFact> pfNew = assemblePlanFactForCar(cid, bid, year, month);
		
		List<PlanFact> pfPlan = new ArrayList<PlanFact>();
		int count;
		pfPlan.addAll(pfOld);
		for (PlanFact pfN: pfNew) {
			count = 0;
			for (PlanFact pfO: pfOld) {
				if (pfN.getParty().getId().equals(pfO.getParty().getId())) {
					count++;
				}
			}
			if (count == 0) {
				pfN.setIsOverdue(false);
				pfPlan.add(pfN);
			}
		}
		List<PlanFact> plF = new ArrayList<PlanFact>();
		for (PlanFact pf: pfPlan) {
			int coun = 0;
			if (pf.getIsOverdue().equals(false)) {
				plF.add(pf);
				for (PlanFact plfa: pfPlan) {
					if (plfa.getIsOverdue().equals(true) && plfa.getParty().getId().equals(pf.getParty().getId())) {
						plF.add(plfa);
						coun ++;
						break;
					}
				}
				if (coun == 0) {
					PlanFact planf = pf.clone();
					planf.setIsOverdue(true);
					planf.setFact(new BigDecimal(0));
					planf.setPlan(new BigDecimal(0));
					plF.add(planf);
				}
			}
		}
		
		pfPlan = plF;
		for (PlanFact pf: pfPlan) {
			List<Employee> caremans = emplDao.findAllByParty(pf.getParty().getId());
			Employee careman = new Employee();
			for (Employee care: caremans) {
				if (care.getPosition().getId().equals(Position.POS_CAREMAN)) {
					careman = care;
				}
			}
			
			if (pf.getIsOverdue() == false && pf.getFact().equals(new BigDecimal(0))) {
				Integer list = smfcService.loadCountFilterChanges(cid, bid, false, year, month, careman.getId());
				pf.setFact(new BigDecimal(list));
			} else if (pf.getFact().equals(new BigDecimal(0))){
				Integer list = smfcService.loadCountFilterChanges(cid, bid, true, year, month, careman.getId());
				pf.setFact(new BigDecimal(list));
			}
		}
		return pfPlan;
	}
	
	public List<PlanFact> assemblePlanFactForCar(Integer cid, Integer bid, Integer year, Integer month) throws Exception {
		try {
			List<PlanFact> pfL = new ArrayList<>();
			List<Employee> caremans = emplDao.findAllByComBrDep(cid, bid, Position.POS_CAREMAN, Department.DEP_SERVICE);
			Department dep = new Department(Department.DEP_SERVICE);
			
			for (Employee em: caremans) {
				
				PlanFact pf = new PlanFact();
				pf.setCompany(new Company(cid));
				pf.setParty(em.getParty());
				
				pf.setMonth(month);
				pf.setYear(year);
				pf.setPlan(new BigDecimal(0));
				pf.setFact(new BigDecimal(0));
				pf.setDepartment(dep);
				Position pos = posDao.findOne(Position.POS_COLLECTOR);
				pf.setPosition(pos);

				Region reg = regDao.getRegionByBrAndDep(bid, dep.getId());
				if (reg != null) pf.setRegion(reg);
				pf.setBranch(new Branch(bid));
				
				pfL.add(pf);
			}
			
			return pfL;	
		} catch(Exception e) {
			throw e;
		}
	}	
	
	public List<PlanFact> getDirectorsPlan(Integer cid, Date dt) throws Exception {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		Integer year = cal.get(Calendar.YEAR);
		Integer month = cal.get(Calendar.MONTH) + 1;
		
		List<PlanFact> pfOld = pfDao.getPlanFactByPosForMonthYear(cid, Position.POS_DIRECTOR, year, month);
		List<PlanFact> pfNew = assemblePlanFactForDir(cid, year, month);
		
		for (int i=0;i<pfNew.size();i++) {	
			pfOld.add(pfNew.get(i));
		}
		return pfOld;
	}
	
	public List<PlanFact> assemblePlanFactForDir(Integer cid, Integer year, Integer month) throws Exception {
		try {
			List<PlanFact> pfL = new ArrayList<>();
			List<Branch> brL = brDao.findAllSalesBranchesByCompany(cid);
			Department dep = new Department(Department.DEP_MARKETING_AND_SALES);
			
			for (Branch br: brL) {
				PlanFact pf = new PlanFact();
				pf.setCompany(br.getCompany());
				
				Employee dir = emplDao.findDirByBranch(br.getId());
				if (dir != null) pf.setParty(dir.getParty());
				
				pf.setMonth(month);
				pf.setYear(year);
				pf.setPlan(new BigDecimal(0));
				pf.setFact(new BigDecimal(0));
				pf.setDepartment(dep);
				Position pos = posDao.findOne(Position.POS_COLLECTOR);
				pf.setPosition(pos);

				Region reg = regDao.getRegionByBrAndDep(br.getId(), dep.getId());
				if (reg != null) pf.setRegion(reg);
				
				br = BranchReducer.reduceBranchForView(br);
				pf.setBranch(br);
				
				pfL.add(pf);
			}
			
			return pfL;	
		} catch(Exception e) {
			throw e;
		}
	}	

	@Override
	public List<PlanFact> getCollectorPlan(Integer cid, Integer bid, Date dt) throws Exception {
		List<PlanFact> pfNew = assembleCollectorPlan(cid, bid, dt);
		
		return pfNew;
	}
	
	public List<PlanFact> assembleCollectorPlan(Integer cid, Integer bid, Date dt) throws Exception {
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(dt);
			Integer year = cal.get(Calendar.YEAR);
			Integer month = cal.get(Calendar.MONTH) + 1;

			List<Employee> caremans = emplDao.findAllStaffByComBrPos(cid, bid, Position.POS_COLLECTOR, dt);

			List<PlanFact> pfL = new ArrayList<>();
			Company company = comDao.findOne(cid);
			Branch branch = brDao.findOne(bid);
			Department dep = new Department(Department.DEP_MARKETING_AND_SALES);
			Region reg = regDao.getRegionByBrAndDep(branch.getId(), dep.getId());

			branch = BranchReducer.reduceBranchForView(branch);

			for (Employee empl: caremans) {
				Party caremanParty = empl.getParty();
				caremanParty = PartyReducer.reduceMax(caremanParty);
				PlanFact pf = new PlanFact();
				PlanFact oldPf = pfDao.getPlanFactByPidMonthYear(caremanParty.getId(), year, month);
				 pf.setCompany(company);
				 pf.setBranch(branch);
				
				 if (caremanParty != null) {
					 pf.setParty(caremanParty);
				 }
				 Position pos = posDao.findOne(Position.POS_COLLECTOR);
				 pf.setPosition(pos);
				 pf.setCurrency(new Currency((String) company.getCurrency().getCurrency()));
				 pf.setDepartment(dep);
				 pf.setRegion(reg);
				 pf.setMonth(month);
				 pf.setYear(year);

				 if (oldPf != null && oldPf.getPlan() != null) {
					 pf.setPlan(oldPf.getPlan());
				 } else {
					 pf.setPlan(new BigDecimal(0));
				 }
				 pfL.add(pf);
			}
			
			return pfL;	
		} catch(Exception e) {
			throw e;
		}
	}
	
	@Override
	public List<PlanFact> getCollectorPlanFact(Integer cid, Integer bid, Date dt) throws Exception {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		Integer year = cal.get(Calendar.YEAR);
		Integer month = cal.get(Calendar.MONTH) + 1;
		
		List<PlanFact> pfOld = pfDao.getPlanFactByPosForMonthYear(cid, Position.POS_COLLECTOR, year, month);
		List<PlanFact> pfNew = assembleCollectorPlanFact(cid, bid, dt);
		
		for (PlanFact pfN: pfNew) {
			if (!pfOld.contains(pfN)) {
				pfOld.add(pfN);
			}
		}
		
		return pfOld;
	}
	
	public List<PlanFact> assembleCollectorPlanFact(Integer cid, Integer bid, Date dt) throws Exception {
		try {
			
			List<Map<String, Object>> cpL = pfDao.getAllCollectorsPlanFact(cid, bid, 
					GeneralUtil.endOfMonth(dt));
//			List<Employee> caremans = emplDao.findAllStaffByComBrPos(cid, bid, Position.POS_CAREMAN);
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(dt);
			
			List<PlanFact> pfL = new ArrayList<>();
			Company company = comDao.findOne(cid);
			Branch branch = brDao.findOne(bid);
			Department dep = new Department(Department.DEP_MARKETING_AND_SALES);
			Region reg = regDao.getRegionByBrAndDep(branch.getId(), dep.getId());
			
			branch = BranchReducer.reduceBranchForView(branch);
			
			for (Map<String, Object> cp: cpL) {
				PlanFact pf = new PlanFact();
				pf.setCompany(company);
				pf.setBranch(branch);
				
				if (!String.valueOf(cp.get("collector")).equals("null")) {
					Employee collector = emplDao.findOne( Long.valueOf(String.valueOf(cp.get("collector"))));
					Party collectorParty = collector.getParty();
					collectorParty = PartyReducer.reduceMax(collectorParty);
					if (collectorParty != null) {
						pf.setParty(collectorParty);
					}
				} else {
					Party collectorParty = new Party();
					collectorParty.setFirstname("");
					collectorParty.setLastname("");
					collectorParty.setMiddlename("");
					pf.setParty(collectorParty);
				}
				
				Position pos = posDao.findOne(Position.POS_COLLECTOR);
				pf.setPosition(pos);
				pf.setCurrency(new Currency((String)cp.get("currency")));
				pf.setDepartment(dep);
				pf.setRegion(reg);
				pf.setMonth(cal.get(Calendar.MONTH) + 1);
				pf.setYear(cal.get(Calendar.YEAR));
				
				boolean addToList = false;
				BigDecimal remain = new BigDecimal(0);
				if (cp.get("remain") != null) {
					remain = new BigDecimal(String.valueOf(cp.get("remain")));
					addToList = true;
				}
				
				BigDecimal paidPast = new BigDecimal(0);
				if (cp.get("paid_past") != null) {
					paidPast = new BigDecimal(String.valueOf(cp.get("paid_past")));
					addToList = true;
				}
				
				BigDecimal paidCurrent = new BigDecimal(0);
				if (cp.get("paid_current") != null) {
					paidCurrent = new BigDecimal(String.valueOf(cp.get("paid_current")));
					addToList = true;
				}
				
				BigDecimal paidAhead = new BigDecimal(0);
				if (cp.get("paid_ahead") != null) {
					paidAhead = new BigDecimal(String.valueOf(cp.get("paid_ahead")));
					addToList = true;
				}
				
				BigDecimal paidTotal = new BigDecimal(0);
				if (cp.get("paid_total") != null) {
					paidTotal = new BigDecimal(String.valueOf(cp.get("paid_total")));
					addToList = true;
				}
				
				pf.setFact(paidTotal);
					
				BigDecimal plan = remain.add(paidCurrent.add(paidPast));
				pf.setPlan(plan);
				
				pf.setPaidAhead(paidAhead);
				pf.setPaidCurrent(paidCurrent);
				pf.setPaidPast(paidPast);
				pf.setRemain(remain);
				
				if (addToList) pfL.add(pf);
			}
			
			return pfL;	
		} catch(Exception e) {
			throw e;
		}
	}
	
	@Override
	public PlanFact savePlanFact(PlanFact newPlanFact) throws Exception {	
		try {
			PlanFactValidator.validateBasic(newPlanFact);
			pfDao.save(newPlanFact);
			return newPlanFact;
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public List<PlanFact> saveAllPlanFacts(List<PlanFact> pfL) throws Exception {	
		try {
			for (PlanFact pf: pfL) {
				if (pf.getPlan().intValue() > 0) {
					pf.setCpuDate(Calendar.getInstance().getTime());
					PlanFactValidator.validateBasic(pf);
					PlanFact pfNew = new PlanFact();
					
					if (pf.getPosition() != null && pf.getPosition().getId().equals(Position.POS_DIRECTOR)) {
						pfNew = pfDao.getPlanByBranchMonthYear(pf.getBranch().getId(), pf.getYear(), pf.getMonth());
						if (pfNew != null) {
							Integer id = pfNew.getId();
							pfNew = pf.clone();
							pfNew.setId(id);
							pfNew.setUpdatedDate(Calendar.getInstance().getTime());
						}
					} else {
						if (!GeneralUtil.isEmptyLong(pf.getParty().getId())) {
							pfNew = pfDao.getPlanFactByPidMonthYear(pf.getParty().getId(), pf.getYear(), pf.getMonth());
							if (pfNew != null) {
								Integer id = pfNew.getId();
								pfNew = pf.clone();
								pfNew.setId(id);	
								
								pfNew.setUpdatedDate(Calendar.getInstance().getTime());
								}
							}	
						}
					if (pfNew != null) {
						pfDao.save(pfNew);	
					} else {
						pf.setUpdatedUser(null);
						pfDao.save(pf);
					}		
				}
			}
			return pfL;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void getPlanCaremanOnMonth(Integer cid, Integer bid, Date sdate, Party party) throws Exception {
		try {
			Integer yea = sdate.getYear() + 1900;
			Integer mon = sdate.getMonth() + 1;
			PlanFact plan = pfDao.getPlanComBraByPartyMonYer(cid, bid, Position.POS_CAREMAN ,party.getId(), yea, mon);
			if (plan == null) {
				throw new Exception("Добавьте план Caremana за указанный месяц");
			}
		} catch (Exception ex) {
			throw ex;
		}
	}

	@Override
	public BigDecimal getPlanFactCaremansByService(SmService service) throws Exception {
		try {
			Integer yea = service.getSdate().getYear() + 1900;
			Integer mon = service.getSdate().getMonth() + 1;

			PlanFact plan = pfDao.getPlanFactGroupBraByPosForMonthYear(service.getCompany().getId(), service.getBranch().getId(), Position.POS_CAREMAN, yea, mon);
			BigDecimal pf = new BigDecimal(0);
			if (plan != null) {
				pf = plan.getPlan(); 
			}
			return pf;
		} catch (Exception ex) {
			throw ex;
		}
	}

	public List<PlanFact> getCaremanPlanFact(Integer cid, Integer bid, Long caremanId, Date dt) throws Exception {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		Integer year = cal.get(Calendar.YEAR);
		Integer month = cal.get(Calendar.MONTH) + 1;
		
		List<PlanFact> pfPlan = new ArrayList<PlanFact>();
		
		PlanFact pfPlanList = pfDao.findCaremanPlan(caremanId, cid, bid, year, month);
		pfPlan.add(pfPlanList);
		for (PlanFact pf: pfPlan) {
				List<Employee> caremans = emplDao.findAllByParty(pf.getParty().getId());
				Employee careman = new Employee();
				for (Employee care: caremans) {
					if (care.getPosition().getId().equals(Position.POS_CAREMAN)) {
						careman = care;
					}
				}
				Integer list1 = smfcService.loadCountFilterChanges(cid, bid, false, year, month, careman.getId());
//				Integer list2 = smfcService.loadCountFilterChanges(cid, bid, true, year, month, careman.getId());
				pf.setFact(new BigDecimal(list1));
//				pf.setPlan(new BigDecimal(list2));
		}
		return pfPlan;
	}


	@Override
	public PlanFact getCollectorPlanFact(Long collectorId, Date startDate, Date endDate) throws Exception {
		List<ContractPaymentSchedule> paymentList = new ArrayList<>();
		paymentList = conPaySchDao.getConPaySheByCollector(collectorId, startDate, endDate);
		PlanFact planFact = new PlanFact();
		BigDecimal plan = new BigDecimal(0);
		BigDecimal fact = new BigDecimal(0);
		Currency currency = new Currency();
		if (paymentList != null && paymentList.size() > 0) {
			for (int i=0;i<paymentList.size();i++) {
				fact.add(paymentList.get(i).getPaid());
				plan.add(paymentList.get(i).getSumm().subtract(paymentList.get(i).getPaid()));
				if (currency == null) {
					if (paymentList.get(i).getContract() != null
							&& paymentList.get(i).getContract().getCurrency() != null) {
						currency = paymentList.get(i).getContract().getCurrency();
					}
						
				}
			}
		}
		Party party = partyDao.findPartyByEmployee(collectorId);
		planFact.setCurrency(currency);
		planFact.setPlan(plan);
		planFact.setFact(fact);
		planFact.setParty(party);
		
		return planFact;
	}
}
