package com.cordialsr.service.implementation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.BranchDao;
import com.cordialsr.Dao.CompanyDao;
import com.cordialsr.Dao.ContractTypeDao;
import com.cordialsr.Dao.FinCurrateDao;
import com.cordialsr.Dao.PriceListDao;
import com.cordialsr.Dao.RegionDao;
import com.cordialsr.domain.Branch;
import com.cordialsr.domain.City;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Country;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Department;
import com.cordialsr.domain.FinCurrate;
import com.cordialsr.domain.PaymentTemplate;
import com.cordialsr.domain.PriceList;
import com.cordialsr.domain.Region;
import com.cordialsr.service.PriceListService;
import com.cordialsr.domain.validator.PriceListValidator;

@Service
@Transactional
public class PriceListServiceImpl implements PriceListService {

	@Autowired
	PriceListDao priceListDao;

	@Override
	public PriceList saveNewPriceList(PriceList newPriceList) throws Exception {
		try {
			PriceListValidator.validateBasic(newPriceList);
			for (PaymentTemplate pt : newPriceList.getPaymentTemplates()) {
				pt.setPriceList(newPriceList);
			}
			priceListDao.save(newPriceList);
			return newPriceList;
		} catch (Exception e) {
			throw e;
		}

	}

	@Override
	public List<PriceList> getAllByExample(PriceList pl) throws Exception {
		try {
			if (!pl.getIsRent())
				pl.setIsRent(null);
			if (!pl.getIsSell())
				pl.setIsSell(null);
			if (!pl.getForFiz())
				pl.setForFiz(null);
			if (!pl.getForYur())
				pl.setForYur(null);
			Example<PriceList> plEx = Example.of(pl);
			List<PriceList> res = priceListDao.findAll(plEx);
			return res;
		} catch (Exception e) {
			throw e;
		}
	}

	// *****************************************************************************************************************

	@Autowired
	RegionDao regionDao;

	@Autowired
	BranchDao brDao;

	@Autowired
	FinCurrateDao rateDao;

	@Autowired
	CompanyDao comDao;

	@Autowired
	ContractTypeDao ctDao;

	@Transactional(readOnly = true)
	@Override
	public List<PriceList> getPriceByScopePriority(Integer cid, Integer bid, Integer inv, Boolean isRent,
			Boolean isYur, Date dt) throws Exception {
		try {
			Company company = comDao.findOne(cid);
			Branch branch = brDao.findOne(bid);
			City city = branch.getCity();
			Country country = branch.getCountry();
			Region region = regionDao.getRegionByBrAndDep(bid, Department.DEP_MARKETING_AND_SALES);
			Currency mc = company.getCurrency();
			Currency lc = country.getLocalCurrency();

			if (region == null) throw new Exception("Не удалось найти Регион."); 
			
			List<PriceList> res = null;
			if (isRent) {
				if (isYur) {
					res = priceListDao.getRentPlForYurByScPriority(cid, inv, bid,
							city.getId(), country.getId(), region.getId(), dt);
				} else {
					res = priceListDao.getRentPlForFizByScPriority(cid, inv, bid,
							city.getId(), country.getId(), region.getId(), dt);
				}
			} else {
				if (isYur) {
					res = priceListDao.getSalesPlForYurByScPriority(cid, inv, bid,
							city.getId(), country.getId(), region.getId(), dt);
				} else {
					res = priceListDao.getSalesPlForFizByScPriority(cid, inv, bid,
							city.getId(), country.getId(), region.getId(), dt);
				}
			}

			List<PriceList> plL = new ArrayList<>();
			FinCurrate lcRate = rateDao.intRate(cid, mc.getCurrency(), lc.getCurrency());
			
			if (lcRate == null) throw new Exception("Couldn't find Local currency Internal Rate [" + lc.getCurrency() + "]");
			
			for (PriceList plOrigin : res) {
				PriceList pl = plOrigin.clone();
				if (!pl.getCurrency().getCurrency().equals(lc.getCurrency())) {
					pl = convertPlToLocalCurrency(cid, mc, lc, lcRate, pl);
				}
				plL.add(pl);
			}
			
			plL.sort(new Comparator<PriceList>() {
				@Override
				public int compare(PriceList o1, PriceList o2) {
					return o1.getMonth().compareTo(o2.getMonth());
				}
			});

			return plL;
		} catch (Exception e) {
			throw e;
		}
	}

	// ******************************************************************************************************************************************
	
	private PriceList convertPlToLocalCurrency(Integer cid, Currency mc, Currency lc, FinCurrate lcRate, PriceList pl) throws Exception {
		try {
			System.out.println(mc.getCurrency());
			System.out.println(pl.getCurrency().getCurrency());
			
			FinCurrate rate2 = rateDao.intRate(cid, mc.getCurrency(), pl.getCurrency().getCurrency());
			BigDecimal ds = pl.getPrice().divide(rate2.getRate(), 12, RoundingMode.HALF_EVEN);
			BigDecimal ws = ds.multiply(lcRate.getRate());
			pl.setPrice(ws);
			pl.setCurrency(lc);
			ds = pl.getFirstPayment().divide(rate2.getRate(), 12, RoundingMode.HALF_EVEN);
			ws = ds.multiply(lcRate.getRate());
			pl.setFirstPayment(ws);	
			List<PaymentTemplate> ptL = new ArrayList<>();
			for (PaymentTemplate pt2 : pl.getPaymentTemplates()) {
				PaymentTemplate pt = pt2.clone();
				ds = pt.getSumm().divide(rate2.getRate(), 12, RoundingMode.HALF_EVEN);
				ws = ds.multiply(lcRate.getRate());
				pt.setSumm(ws);
				ptL.add(pt);
			}
			pl.setPaymentTemplates(ptL);
			return pl;
		} catch (Exception e) {
			throw e;
		}
	}

	public PriceList updatePriceList(PriceList pList) throws Exception {
		try {
			PriceListValidator.validateBasic(pList);
			pList.setPaymentTemplates(null);
			
			return priceListDao.save(pList);	
		} catch (CloneNotSupportedException e) {
			throw new Exception(e.getMessage());
		} catch (Exception e) {
			throw new Exception(e);
		}
		
	}
	
}
