package com.cordialsr.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.PartyRelativesDao;
import com.cordialsr.domain.PartyRelatives;
import com.cordialsr.service.PartyRelativesService;


@Service
@Transactional
public class PartyRelativesServiceImpl implements PartyRelativesService{
	
	@Autowired
	PartyRelativesDao relativeDao;
	
	@Override
	public PartyRelatives saveNewRelative(PartyRelatives newRelative) throws Exception {
		try {
//			Validator
			relativeDao.save(newRelative);
			return newRelative;
			
		} catch (Exception e) {
			throw e;
		}
	}
}
