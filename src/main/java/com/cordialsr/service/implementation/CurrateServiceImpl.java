package com.cordialsr.service.implementation;

import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.cordialsr.Dao.BranchDao;
import com.cordialsr.Dao.CountryDao;
import com.cordialsr.Dao.FinCurrateDao;
import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Country;
import com.cordialsr.domain.FinCurrate;
import com.cordialsr.dto.Money;
import com.cordialsr.service.CurrateService;

@Service
public class CurrateServiceImpl implements CurrateService {

	@Autowired
	FinCurrateDao crDao;
	
	@Autowired
	BranchDao brDao;
	
	@Autowired
	CountryDao countryDao;
	
	@Override
	public FinCurrate saveNewCurrate(FinCurrate newCr) throws Exception {
		try {
			newCr.setCpuDate(Calendar.getInstance().getTime());
			crDao.save(newCr);
			return newCr;
		}catch(Exception e) {
			throw e;
		}
	}

	@Override
	public List<Money> getLastNbRates(Integer branchId) throws Exception {
		try {
			List<Money> rates = null;
			Branch br = brDao.findOne(branchId); 
	        switch (br.getCountry().getId()) {
	        	case Country.COUNTRY_KZ: {
	        		// http://www.nationalbank.kz/rss/rates_all.xml?switch=kazakh
	        		rates = getLastNbKZRates();
	        		break;
	        	}
	        	case Country.COUNTRY_KG: {
	        		// http://www.nbkr.kg/XML/daily.xml

	        		break;
	        	}
	        	case Country.COUNTRY_AZ: {
	        		
	        		break;
	        	}
	        	case Country.COUNTRY_UZ: {
	        		// http://www.cbu.uz/ru/arkhiv-kursov-valyut/xml/
	        		
	        		break;
	        	}
	        	case Country.COUNTRY_RU: {
	        		
	        		break;
	        	}
	        }
	        return rates;
		} catch (Exception e) {
			throw e;
		}
        
	}
	
	private List<Money> getLastNbKZRates() throws Exception {
		try {
			String url = "http://www.nationalbank.kz/rss/rates_all.xml?switch=kazakh";
			URL rssUrl = new URL (url);
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(rssUrl.openStream());
			NodeList nl = doc.getChildNodes();
			
			List<Money> rates = new ArrayList<>();  
			
			parseNodes(nl, rates);
	        
	        return rates;
		} catch (Exception e) {
			throw e;
		}
        
	}
	
	private void parseNodes(NodeList nl, List<Money> rates) {
		for (int i = 0; i < nl.getLength(); i++) {
			Node node = nl.item(i);
			if (node.getParentNode().getNodeName() == "title" 
					&& node.getNodeValue().length() == 3) {
				Money newRate = new Money(node.getNodeValue());
				rates.add(newRate);
			}
			
			if (node.getParentNode().getNodeName() == "description" && rates.size() > 0) {
				Money rate = rates.get(rates.size() - 1);
				rate.setSumm(new BigDecimal(node.getNodeValue()));
			}
			
			if (node.hasChildNodes()) {
				parseNodes(node.getChildNodes(), rates);
			}
		}
	}

	@Override
	public FinCurrate getRateExt(Integer cid, String mc, String sc, Date dt) throws Exception {
		try {
			FinCurrate fcr = crDao.extRateByDate(cid, mc, sc, dt);
			return fcr;
		} catch(Exception e) {
			throw e;
		}
	}

	@Override
	public FinCurrate getRateInt(Integer cid, String mc, String sc, Date dt) throws Exception {
		try {
			FinCurrate fcr = crDao.intRateByDate(cid, mc, sc, dt);
			return fcr;
		} catch(Exception e) {
			throw e;
		}
	}
	

	@Override
	public List<FinCurrate> getCurrentRates(Integer bid) throws Exception {
		try {
			Branch branch = brDao.findById(bid);
			
			Integer countryId = branch.getCountry().getId();
					
			Country country = countryDao.findOne(countryId);
			
			String localCurrency = country.getLocalCurrency().getCurrency();
			List<FinCurrate> currateList = crDao.getCurrentDaysRate(localCurrency, "USD");
			return currateList;
		} catch(Exception e) {
			throw e; 
		}
	}
	
	
}
