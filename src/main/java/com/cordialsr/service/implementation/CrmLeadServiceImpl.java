package com.cordialsr.service.implementation;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.CrmLeadDao;
import com.cordialsr.domain.Address;
import com.cordialsr.domain.CrmLead;
import com.cordialsr.domain.PhoneNumber;
import com.cordialsr.service.CrmLeadService;

@Service
@Transactional
public class CrmLeadServiceImpl implements CrmLeadService {

	@Autowired 
	CrmLeadDao leadDao;
	
	@Override
	public CrmLead saveLead(CrmLead newLead) throws Exception {
		try {
//			LeadValidator.validateBasic(newLead);

			List<PhoneNumber> leadPh = newLead.getPhoneNumbers();
			Address addr = newLead.getAddress();
			newLead.setCpuDate(Calendar.getInstance().getTime());

			leadDao.save(newLead);
			for (PhoneNumber ph: leadPh) {
				ph.setLead(newLead);
			}
			addr.setLead(newLead);
			
			

	
			newLead.setAddress(addr);
			newLead.setPhoneNumbers(leadPh);
			leadDao.save(newLead);
			return newLead;
		} catch (Exception e) {
			throw e;
		}
	}

	// *********************************************************************************************************
	
	@Override
	public CrmLead[] saveLeads(CrmLead[] newLeads) throws Exception {
		try {
//			LeadValidator.validateBasic(newLead);

			for (CrmLead lead: newLeads) {
				List<PhoneNumber> leadPh = lead.getPhoneNumbers();
				lead.setCpuDate(Calendar.getInstance().getTime());
				
				Address addr = lead.getAddress();
				Address adviserAddr = lead.getAdviser().getAddress();
				
				if (adviserAddr == null || adviserAddr.getAddressType() == null) {
					lead.getAdviser().setAddress(null);
				}
				if (addr == null || addr.getAddressType() == null) {
					lead.setAddress(null);
				}
					
				leadDao.save(lead);
				for (PhoneNumber ph: leadPh) {
					ph.setLead(lead);
				}
				if (lead.getPhone() != null) {
					lead.getPhone().setLead(lead);
				}
				if (adviserAddr == null || adviserAddr.getAddressType() == null) {
					lead.setAddress(null);
				} else {
					adviserAddr.setLead(lead);
					lead.getAdviser().setAddress(adviserAddr);
				}
				
				if (addr == null || addr.getAddressType() == null) {
					lead.setAddress(null);
				} else {
					addr.setLead(lead);	
					lead.setAddress(addr);
				}
				lead.setPhoneNumbers(leadPh);
				leadDao.save(lead);
			}
			
			return newLeads;
		} catch (Exception e) {
			throw e;
		}
	}
	
	// *********************************************************************************************************
	
	@Override
	public CrmLead updateLead(CrmLead lead) throws Exception {
		try {
//			LeadValidator.validateBasic(lead);
			CrmLead leadOrigin = leadDao.findOne(lead.getId());
			lead.setAdviser(leadOrigin.getAdviser());
			
			leadOrigin = lead.clone();
			for (PhoneNumber ph: leadOrigin.getPhoneNumbers()) {
				ph.setLead(leadOrigin);
			}
			leadOrigin.getPhone().setLead(leadOrigin);
			if (leadOrigin.getAddress() != null) {
				leadOrigin.getAddress().setLead(leadOrigin);
			}
			leadOrigin.setCpuDate(Calendar.getInstance().getTime());
			
			leadOrigin = leadDao.save(leadOrigin);
			return leadOrigin;
		} catch (Exception e) {
			throw e;
		}
	}	

}
