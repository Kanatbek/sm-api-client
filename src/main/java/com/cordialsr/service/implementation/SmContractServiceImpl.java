package com.cordialsr.service.implementation;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cordialsr.Dao.SmContractDao;
import com.cordialsr.Dao.SmFilterChangeDao;
import com.cordialsr.domain.SmContract;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.SmContractService;

@Service
@Transactional
public class SmContractServiceImpl implements SmContractService {

	@Autowired 
	SmContractDao smcDao;
	
	@Autowired
	SmFilterChangeDao smfcDao;
	
	@Override
	public SmContract updateSmContract(SmContract smCon) throws Exception {
		SmContract oldCon = getSmContractById(smCon.getId());
		
		try {
			oldCon = smCon.clone();
		} catch (CloneNotSupportedException e) {
			throw new Exception(e);
		}
		
		if (smCon.getSmBranch() != null && !GeneralUtil.isEmptyString(smCon.getSmBranch().getCode())) {
			oldCon.setSmBranch(smCon.getSmBranch());
		}
		
		smcDao.save(oldCon);		
		return oldCon;
	}

	@Override
	public SmContract getSmContractById(Integer id) throws Exception {
		try {
			SmContract smc = smcDao.findOne(id);
			return smc;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Object getActiveCancelledCount() throws Exception {
		try {
			return smfcDao.actCancCount();
		} catch(Exception e) {
			throw e;
		}
	}
}
