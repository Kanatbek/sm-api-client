package com.cordialsr.service.implementation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.ContractDao;
import com.cordialsr.Dao.EmployeeDao;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.Position;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.EmployeeService;

@Service
@Transactional(rollbackFor = Exception.class)
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDao emplDao;
	
	@Autowired
	private ContractDao contractDao;
	
	@Override
	public Page<Employee> getEmployeeList(Integer cid, Integer bid, Integer posId, String sortBy, Boolean fired, String filter, Pageable pageable) throws Exception {
		try {
			if (!GeneralUtil.isEmptyInteger(cid)) {

				List<Employee> employeeList = null;
				
				if (filter != null) {
					if (bid == null && posId == null) {
						if (fired == false) {
						employeeList = emplDao.filterEmplByCidFired(cid, filter);
						} else {
							employeeList = emplDao.filterEmplByCid(cid, filter);
						}
					}
					
					if (bid != null && posId == null) {
						if (fired) { 
							employeeList = emplDao.filterEmplByCidBid(cid, bid, filter);
						} else {
						employeeList = emplDao.filterEmplByCidBidFired(cid, bid, filter);
						}
					}
					
					if (bid == null && posId != null) {
						if (fired) {
							employeeList = emplDao.filterEmplByCidPos(cid, posId, filter);
						} else {
						employeeList = emplDao.filterEmplByCidPosFired(cid, posId, filter);
						}
					}
					
					if (bid != null && posId != null) {
						if (fired) {
							employeeList = emplDao.filterEmplByCidBidPos(cid, bid, posId, filter);
						} else {
						employeeList = emplDao.filterEmplByCidBidPosFired(cid, bid, posId, filter);
						}
					}
				} else {
					if (sortBy != null) {
						if (sortBy.equals("Firstname")) {		
							if (bid == null && posId == null) {
								if (fired) {
									employeeList = emplDao.sortEmplByFirstnameCid(cid);
								} else {
								employeeList = emplDao.sortEmplByFirstnameCidFired(cid); 
								}
							}
							
							if (bid != null && posId == null) {
								if (fired) {
									employeeList = emplDao.sortEmplByFirstnameCidBid(cid, bid);
								} else { 
								employeeList = emplDao.sortEmplByFirstnameCidBidFired(cid, bid);
								}
							}
							
							if (bid == null && posId != null) {
								if (fired) {
									employeeList = emplDao.sortEmplByFirstnameCidPos(cid, posId);
								} else {
								employeeList = emplDao.sortEmplByFirstnameCidPosFired(cid, posId);
								}
							}
							
							if (bid != null && posId != null) {
								if (fired) {
									employeeList = emplDao.sortEmplByFirstnameCidBidPos(cid, bid, posId);
								} else {
								employeeList = emplDao.sortEmplByFirstnameCidBidPosFired(cid, bid, posId);
								}
							}
						}			
						if (sortBy.equals("Lastname")) {		
							if (bid == null && posId == null) {
								if (fired) {
									employeeList = emplDao.sortEmplByLastnameCid(cid);
								} else {
								employeeList = emplDao.sortEmplByLastnameCidFired(cid);
								}
							}
							
							if (bid != null && posId == null) {
								if (fired) {
									employeeList = emplDao.sortEmplByLastnameCidBid(cid, bid);
								} else {
								employeeList = emplDao.sortEmplByLastnameCidBidFired(cid, bid); 
								}
							}
							
							if (bid == null && posId != null) {
								if (fired) {
									employeeList = emplDao.sortEmplByLastnameCidPos(cid, posId);
								} else {
								employeeList = emplDao.sortEmplByLastnameCidPosFired(cid, posId);
								}
							}
							
							if (bid != null && posId != null) {
								if (fired) {
									employeeList = emplDao.sortEmplByLastnameCidBidPos(cid, bid, posId);
								} else {
								employeeList = emplDao.sortEmplByLastnameCidBidPosFired(cid, bid, posId);
								}
							}
						}
					} else {
						if (bid == null && posId == null) {
							if (fired) {
								employeeList = emplDao.getEmplByCid(cid);
							} else {
							employeeList = emplDao.getEmplByCidFired(cid);
							}
						}
						
						if (bid != null && posId == null) {
							if (fired) {
								employeeList = emplDao.getEmplByCidBid(cid, bid);
							} else {
							employeeList = emplDao.getEmplByCidBidFired(cid, bid);
							}
						}
						
						if (bid == null && posId != null) {
							if (fired) {
								employeeList = emplDao.getEmplByCidPos(cid, posId);
							} else {
							employeeList = emplDao.getEmplByCidPosFired(cid, posId);
							}
						}
						
						if (bid != null && posId != null) {
							if (fired) {
								employeeList = emplDao.getEmplByCidBidPos(cid, bid, posId);
							} else {
							employeeList = emplDao.getEmplByCidBidPosFired(cid, bid, posId);
							}
						}
					}
				}
					
					Integer totalElem = employeeList.size();
					employeeList = getOffsetPage(employeeList, pageable.getPageNumber(), pageable.getPageSize());
					Page<Employee> pages = new PageImpl<Employee>(employeeList, pageable, totalElem);
					
					return pages;
			} else throw new Exception("Укажите компанию!");
		} catch (Exception e) {
			throw e;
		}
	}
	
	private List<Employee> getOffsetPage(List<Employee> conL, Integer page, Integer size) throws Exception {
		try {
			List<Employee> res = new ArrayList<>();
			Integer si = page * size;
			Integer ei = si + size;
			if (ei > conL.size()) ei = conL.size();
			res = conL.subList(si, ei);
			return res;
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public Page<Employee> getAllManager(Integer cid, Integer bid, Date dateStart,
			Date dateEnd, String filter, Pageable pageable) throws Exception {
		try {	
				Page<Employee> managers = emplDao.findAllManager(cid, bid, Position.POS_MANAGER, dateStart, dateEnd, pageable);
				
//				Integer branchContract = contractDao.findContractNumberByBranch(cid, bid, dateStart, dateEnd);
//				Integer companyContract = contractDao.findContractNumberByCompany(cid, dateStart, dateEnd);
				
				List<Employee> emplL = new ArrayList<>();
				for (Employee empl: managers) {
					Integer contractNumber = contractDao.getCountManagerContract(empl.getId(), dateStart, dateEnd);
					empl.setContractNumber(contractNumber);
					emplL.add(empl);
				}	
				Page<Employee> pages = new PageImpl<Employee>(emplL, pageable, managers.getTotalElements());
				return pages;
		} catch (Exception e) {
			throw e;
		}
	}
	// *****************************************************************************************************************************

	@Override
	public List<Employee> getDealerList(Long emplId, Date dateStart, Date dateEnd) throws Exception {
		try {
			List<Employee> dealerList = emplDao.findAllDealerByManager(emplId, dateStart, dateEnd);
			for (Employee empl: dealerList) {
				Integer contractNumber = contractDao.getCountDealerContract(empl.getId(), emplId, dateStart, dateEnd);
				empl.setContractNumber(contractNumber);
			}
			return dealerList;
		} catch (Exception e) {
			throw e;
		}
	}
	

}
