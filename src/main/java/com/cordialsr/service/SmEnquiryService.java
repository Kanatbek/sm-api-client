package com.cordialsr.service;

import java.util.List;

import com.cordialsr.domain.SmEnquiry;

public interface SmEnquiryService {

	SmEnquiry saveNewEnquiry(SmEnquiry newEnquiry) throws Exception;
	List<SmEnquiry> getAllByExample(SmEnquiry se) throws Exception;
	SmEnquiry getInvoiceById(Long enqId) throws Exception;
	SmEnquiry updateEnuiry(SmEnquiry newEnq) throws Exception;

}
