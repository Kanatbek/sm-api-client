package com.cordialsr.service;

import com.cordialsr.domain.PartyRelatives;

public interface PartyRelativesService {
	PartyRelatives saveNewRelative(PartyRelatives newRelative) throws Exception;
}
