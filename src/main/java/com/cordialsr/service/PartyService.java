package com.cordialsr.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.cordialsr.domain.Party;

public interface PartyService {
	Party saveParty(Party party) throws Exception;
	Party updateParty(Party party) throws Exception;
	Page<Party> getEmployeeList(Integer cid, Integer bid, Integer pos, Boolean fired, String sortBy, String filter, Pageable pageRequest) throws Exception;
	void appendPartyPositions(Party p, Integer bid, Boolean fired) throws Exception;
}
