package com.cordialsr.service;

import java.util.Date;

public interface BranchService {
		Integer getBranchContractNumber(Integer cid, Integer bid, Date dateStart) throws Exception;
}
