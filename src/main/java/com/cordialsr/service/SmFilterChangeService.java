package com.cordialsr.service;

import java.util.Date;
import java.util.List;

import com.cordialsr.domain.Contract;
import com.cordialsr.domain.SmFilterChange;
import com.cordialsr.domain.SmService;

public interface SmFilterChangeService {
	Iterable<SmFilterChange> findAll();
	SmFilterChange findById(Integer id);
	void saveFC(SmFilterChange fc);
	boolean isFcExist(SmFilterChange fc);
	SmFilterChange updateFilterChange(SmFilterChange fc) throws Exception;
	
	Boolean newService(SmService service) throws Exception;
	Boolean earlyService(SmService service) throws Exception;
	SmFilterChange newGraph(Contract newContract) throws Exception;
	List<SmFilterChange> loadFilterChanges(Integer cid, Integer bid, Boolean bool, Date from, Date to, Long eid) throws Exception;
	void updateService(SmService service) throws Exception;
	void updateFcByContract(Long conId, Boolean enabled) throws Exception;
	Boolean isOverdueFC(SmFilterChange fc, Date sdate) throws Exception;
	Integer loadCountFilterChanges(Integer cid, Integer bid, boolean b, Integer year, Integer month, Long id) throws Exception;
	SmFilterChange findBySerialNumber(String ser_num) throws Exception;
}
