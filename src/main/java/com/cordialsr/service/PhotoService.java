package com.cordialsr.service;

import com.cordialsr.domain.Photo;

public interface PhotoService {
	Photo savePhoto(Photo newPhoto) throws Exception;
	
}
