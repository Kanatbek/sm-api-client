package com.cordialsr.service;

import com.cordialsr.domain.ContractHistory;

public interface ContractPostRefactorService {

	void shiftContractDate(String conNum, String dateStr) throws Exception;
	
	void stornoContractPayment(String conNum, Double sum) throws Exception;
	
	void numeratePaymentOrderForCAWS(Integer brId) throws Exception;
	
	void genrateCawsForCoordinator(Integer brId, Integer month, Integer year) throws Exception;
	
	boolean stornoContractPaymentFromFront(ContractHistory conHistory) throws Exception;
	
	boolean stornoCustomerPayment(ContractHistory conHistory) throws Exception;
	
}
