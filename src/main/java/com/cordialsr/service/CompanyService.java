package com.cordialsr.service;

import java.util.Date;

public interface CompanyService {
		Integer getCompanyContractNumber(Integer cid, Date dateStart) throws Exception;
}
