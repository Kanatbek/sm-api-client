package com.cordialsr.service;

import java.math.BigDecimal;

import org.apache.commons.lang3.tuple.ImmutablePair;

import com.cordialsr.api.kg.asisnur.AsisnurTransaction;
import com.cordialsr.api.kg.mobilnik.MobilnikTransaction;
import com.cordialsr.api.kg.qiwi.QiwiKgTransaction;
import com.cordialsr.api.uz.payme.entity.PaymeTransaction;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.KassaBank;

public interface ContractPaymentService {

	boolean postPaymeCustomerPayment(PaymeTransaction paymeTr) throws Exception;	
	
	ImmutablePair<KassaBank, BigDecimal> postFirstPaymentFromNewContract(Contract con) throws Exception;
	
	boolean postMobilnikCustomerPayment(MobilnikTransaction mobilTr) throws Exception;
	
	boolean postAsisnurCustomerPayment(AsisnurTransaction asisTr) throws Exception;
	
	boolean postQiwiKgCustomerPayment(QiwiKgTransaction qiwiKgTr) throws Exception;
	
}
