package com.cordialsr.service;

import com.cordialsr.domain.CrmDemo;

public interface CrmDemoService {
	CrmDemo saveDemo (CrmDemo ch) throws Exception;
	CrmDemo updateDemo (CrmDemo demo) throws Exception;
}
