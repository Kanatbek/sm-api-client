package com.cordialsr.service;

import java.util.Date;
import java.util.List;

import com.cordialsr.domain.FinEntry;

public interface StaffAccountService {
	
	Object getStaffBalanceByBranch(Integer cid, Integer bid, Date dt) throws Exception;
	
	List<FinEntry> getSbJournal(Integer cid, Integer bid, Long partyId, String currency, String ds, String de) throws Exception;
	
	String getSbJournalFile(Integer cid, Integer bid, Long partyId, String currency, Date ds, Date de, String format) throws Exception;
	
}
