package com.cordialsr.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.cordialsr.domain.Currency;
import com.cordialsr.domain.SmVisitCollector;
import com.cordialsr.dto.SmVisitCollectorDto;

public interface SmVisitCollectorService {
		SmVisitCollector newCollectorVisit(Date inDate, Date inTime, Date outTime, Long emplId, Long partyId,
											BigDecimal summ, Long contractId, BigDecimal longitude, 
											BigDecimal latitude, String info) throws Exception;
		
		List<SmVisitCollectorDto> getVisits(Date dt1, Date dt2, Long emplId) throws Exception;
		List<SmVisitCollectorDto> getCustomersVisit(Date dt1, Date dt2, Long emplId, Long customerId) throws Exception;
		
}
