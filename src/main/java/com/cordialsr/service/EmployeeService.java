package com.cordialsr.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.cordialsr.domain.Employee;

public interface EmployeeService {
	Page<Employee> getEmployeeList(Integer cid, Integer bid, Integer posId, String sortBy, Boolean fired, String filter, Pageable Pagerequest) throws Exception;
	Page<Employee> getAllManager(Integer cid, 
			Integer bid, 
			Date dateStart,
			Date dateEnd,
			String filter, Pageable pageRequest) throws Exception;
	List<Employee> getDealerList(Long emplId, Date dateStart, Date dateEnd) throws Exception;

}
