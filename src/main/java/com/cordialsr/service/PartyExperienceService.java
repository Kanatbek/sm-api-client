package com.cordialsr.service;

import com.cordialsr.domain.PartyExperience;

public interface PartyExperienceService {
	PartyExperience saveNewExperience(PartyExperience newExperience) throws Exception;
}
