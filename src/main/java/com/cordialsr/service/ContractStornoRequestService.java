package com.cordialsr.service;

import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractStornoRequest;
import com.cordialsr.domain.SmFilterChange;

public interface ContractStornoRequestService {
	ContractStornoRequest saveNewContractStornoRequest(ContractStornoRequest newContractStornoRequest) throws Exception;
	ContractStornoRequest updateContractStornoRequest(ContractStornoRequest conRequest) throws Exception;
	Contract cancelContractStorno(Long conId) throws Exception;
}
