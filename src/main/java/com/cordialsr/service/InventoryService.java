package com.cordialsr.service;

import java.util.List;

import com.cordialsr.domain.Inventory;

public interface InventoryService  {
	Inventory saveNewInventory(Inventory newInv) throws Exception;
	Inventory updateInventory(Inventory newInv) throws Exception;
	List<Inventory> getInventoryByCompany(Integer cid) throws Exception;
}
