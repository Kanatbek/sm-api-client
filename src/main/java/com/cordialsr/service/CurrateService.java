package com.cordialsr.service;

import java.util.Date;
import java.util.List;

import com.cordialsr.domain.FinCurrate;
import com.cordialsr.dto.Money;

public interface CurrateService {
	FinCurrate saveNewCurrate(FinCurrate newCr) throws Exception;
	List<Money> getLastNbRates(Integer branchId) throws Exception;
	FinCurrate getRateExt(Integer cid, String mc, String sc, Date dt) throws Exception;
	FinCurrate getRateInt(Integer cid, String mc, String sc, Date dt) throws Exception;
	List<FinCurrate> getCurrentRates(Integer bid) throws Exception;
}
