package com.cordialsr.service;

import java.util.Date;

import com.cordialsr.domain.Contract;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.SmService;

public interface InvoiceService {
	Invoice saveInvoice(Invoice newInvoice) throws Exception;
	Invoice updateInvoice(Invoice newInvoice) throws Exception;	
	Invoice getInvoiceById(Long id) throws Exception;
	Invoice saveNewInvoiceFromContract(Contract newContract) throws Exception;
	Boolean updateInvoiceFromContract(Contract newContract) throws Exception;
	Invoice invoiceToType(Invoice changedFd, String trCode, Long userId)throws Exception;
	Invoice newInvoiceService(SmService service) throws Exception;
	Invoice getInvoiceForServices(Integer cid, Integer bid, Date from, Date to) throws Exception;
	Boolean checkCustomerHasStock(String contractNumber) throws Exception;
	Invoice returnInvoice(Invoice newInv) throws Exception;
	Invoice restoreFromReserved(Contract contract) throws Exception;
	
	Boolean stornoInvoiceByContract(Contract contract) throws Exception;
}
