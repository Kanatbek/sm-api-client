package com.cordialsr.service;

import java.util.List;

import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.StockIn;
import com.cordialsr.domain.StockStatus;

public interface StockService {

	Boolean saveNewStockIns(FinDoc[] finDoc) throws Exception;
	Boolean saveNewStockInImport(FinDoc[] finDoc, Invoice invoice) throws Exception;
	Boolean saveNewStockInTransfer(FinDoc finDoc, Invoice invoice) throws Exception;
	Boolean saveNewStocksOutOperation(Invoice invoice) throws Exception;
	List<StockIn> getStockInByGlAndInv(Integer cid, Integer bid, String gl, Integer inv, Integer cnt, Integer sta, Integer prt) throws Exception;
	StockIn changeStockInByStatus(StockIn stock) throws Exception;
	Invoice saveNewStocksByInvoice(Invoice invoice, StockStatus status) throws Exception;
	Invoice returnInvoiceService(Invoice invoice, Invoice newInv) throws Exception;
	Boolean changeStocksInContract(String oldSN) throws Exception;
}
