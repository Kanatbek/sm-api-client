package com.cordialsr.service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractHistory;
import com.cordialsr.domain.ContractPaymentSchedule;
import com.cordialsr.domain.ContractStornoRequest;
import com.cordialsr.domain.Employee;
import com.cordialsr.dto.ContractPaymentDto;
import com.cordialsr.dto.ContractPaymentListDto;
import com.cordialsr.dto.Money;
import com.cordialsr.dto.mini.ContractMiniDto;

public interface ContractService {
	Contract saveContract(Contract newContract) throws Exception;
	ContractHistory saveContractHistory(ContractHistory contractHistory) throws Exception;
	
	Contract changeContractEmployee(ContractHistory contractHistory, Long contractId, Long newPartyId,
										Long newEmployeeId, Long oldEmplId, Integer position) throws Exception;	
	Contract saveNewContract(Contract newContract) throws Exception;
	Contract updateContract(Contract contract) throws Exception;
	Contract reissueContract(Contract contract) throws Exception;
	Contract cancelContract(ContractStornoRequest stornoReq) throws Exception;
	Contract getOneContract(Long id) throws Exception;
	Contract findContractByNumber(Integer cid, Integer bid, String cn) throws Exception;
	
	List<ContractPaymentDto> getAllReceivables(Integer cid, Integer bid, Date dte, 
			Long conId, Long cusId, Long collectorId, Long dealerId) throws Exception;
	
	List<ContractPaymentListDto> getCollectorsList(Integer cid, Integer bid, 
			Long sid, Date dte, Boolean rent, Boolean sale) throws Exception;
	
	List<ContractPaymentListDto> getCollectorPaymentGraph(Integer cid, Integer bid, 
			Long sid, Date dte, Boolean rent, Boolean sale) throws Exception;
	
	ContractPaymentSchedule paymentPosted(ContractPaymentSchedule cps, BigDecimal payment) throws Exception;
	Contract cancelPayment(Contract con, BigDecimal payment, Integer order) throws Exception;
	Money getCustomerPaymentDueByIIN(String iinBin, Calendar dateEnd, String curr, Boolean rental, Boolean sales) throws Exception;
	Money getCustomerPaymentDueByID(Long customerId, Calendar dateEnd, String currency, Boolean rental, Boolean sales) throws Exception;
	Money getCustomerContractSummByID(Long customerId, Calendar dateEnd, String currency, Boolean rental, Boolean sales) throws Exception;
	
	Page<Contract> getContractList(Integer cid, 
									Integer bid, 
									Boolean storno, 
									Boolean servbr, 
									Boolean isRent, 
									Long empl, 
									Date dts,
									Date dte, 
									Boolean caremanSales,
									String filter, Pageable pageRequest) throws Exception;
	
	void updateWriteOffDate (String contractNumber, String sn, Integer inventoryId, Date writeOffDate ) throws Exception;
	Contract[] saveContractsChange(Contract[] newContracts, Long careman, Long collector) throws Exception;
	
	List<Contract> getContractByCustomerId(Long customerId) throws Exception;
	
}
