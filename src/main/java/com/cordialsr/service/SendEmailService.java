package com.cordialsr.service;

import com.cordialsr.domain.security.User;

public interface SendEmailService {

	void sendNotification(User user, String subject, String body) throws Exception;
}
