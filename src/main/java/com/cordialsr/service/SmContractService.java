package com.cordialsr.service;

import com.cordialsr.domain.SmContract;

public interface SmContractService {
	SmContract updateSmContract(SmContract smCon) throws Exception;
	SmContract getSmContractById(Integer id) throws Exception;
	Object getActiveCancelledCount() throws Exception;
}