package com.cordialsr.service;

import java.util.List;

import com.cordialsr.domain.Award;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractAwardSchedule;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.Payroll;

public interface ContractAwardService {
	
	void updateContractAwards(List<Contract> conL, Award award, Employee empl) throws Exception;
	ContractAwardSchedule updateConAwardSchedule(Payroll p) throws Exception;
	
}
