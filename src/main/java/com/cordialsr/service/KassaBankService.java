package com.cordialsr.service;

import java.util.List;

import com.cordialsr.domain.FinEntry;
import com.cordialsr.domain.KassaBank;

public interface KassaBankService {
	KassaBank saveNewKb(KassaBank newKb) throws Exception;
	Object getKassaBalance(Integer kbid) throws Exception;
	Object getKassaBalanceByDate(Integer kbid, String date) throws Exception;
	List<FinEntry> getKbFes(Integer kbid, String ds, String de) throws Exception;
}
