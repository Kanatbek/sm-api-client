package com.cordialsr.service;

public interface MigrationService {
	void migrateContracts(Integer offset, Integer limit);
	void generatePaymentSchedForSales(Integer offset, Integer limit);
	void addFinDocsForNewContracts(Integer limit);
	void addInvoiceNumbers();
	void replaceDuplicatedStaff();
	void appendDealerPremis(Integer cid, Integer bid, Integer month, Integer year);
	void generateAwardPayrolls(Integer cid, Integer bid, Integer month, Integer year);
	void updateContractAddrAndPhones(Integer brId);
}
