package com.cordialsr.service;

import java.util.List;

import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractStornoRequest;
import com.cordialsr.domain.Payroll;

public interface PayrollService {

	List<Payroll> getPayrollsForMonth(Integer cid, Integer bid, String kind, Integer pos, Boolean dbt, Integer month, Integer year) throws Exception;
	List<Payroll> payrollPremisFromNewContract(Contract con) throws Exception;
	List<Payroll> doPostPayrolls(Integer cid, List<Payroll> prlL) throws Exception;
	List<Payroll> doPostPayrollsToTransitAccount(Integer cid, List<Payroll> prlL) throws Exception;
	List<Payroll> doPostPayrollsFromTransit(Integer cid, List<Payroll> prlL) throws Exception;
	List<Payroll> generateBonusSchedule(Integer cid, Integer bid, Integer pos, Integer month, Integer year) throws Exception;
	List<Payroll> getPayrollsFromXls(Integer cid, Integer bid, Integer month, Integer year, String xls64) throws Exception;
	boolean rollbackContractAwards(ContractStornoRequest stReq, Integer monthAge, String trCode) throws Exception;
	
	void payrollFirstPremiForDealer(Contract newContract) throws Exception;
}
