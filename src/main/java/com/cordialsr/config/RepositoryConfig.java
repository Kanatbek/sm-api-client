package com.cordialsr.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

import com.cordialsr.domain.Address;
import com.cordialsr.domain.AddressType;
import com.cordialsr.domain.Employee;
import com.cordialsr.domain.Award;
import com.cordialsr.domain.AwardCase;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractAwardSchedule;
import com.cordialsr.domain.ContractHistory;
import com.cordialsr.domain.ContractPaymentSchedule;
import com.cordialsr.domain.AwardTemplate;
import com.cordialsr.domain.AwardType;
import com.cordialsr.domain.Branch;
import com.cordialsr.domain.BranchType;
import com.cordialsr.domain.City;
import com.cordialsr.domain.CityArea;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Country;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Department;
import com.cordialsr.domain.FinCashflowStatement;
import com.cordialsr.domain.FinCurrate;
import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.FinDocType;
import com.cordialsr.domain.FinEntry;
import com.cordialsr.domain.FinGlAccount;
import com.cordialsr.domain.FinActivityType;
import com.cordialsr.domain.FinOperation;
import com.cordialsr.domain.Incoterm;
import com.cordialsr.domain.InvFno;
import com.cordialsr.domain.InvMainCategory;
import com.cordialsr.domain.InvSubCategory;
import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.Invoice;
import com.cordialsr.domain.InvoiceItem;
import com.cordialsr.domain.InvoiceStatus;
import com.cordialsr.domain.KassaBank;
import com.cordialsr.domain.Manufacturer;
import com.cordialsr.domain.Oblast;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.PartyExperience;
import com.cordialsr.domain.PartyType;
import com.cordialsr.domain.PaymentTemplate;
import com.cordialsr.domain.PhoneNumber;
import com.cordialsr.domain.PlanFact;
import com.cordialsr.domain.Position;
import com.cordialsr.domain.PriceList;
import com.cordialsr.domain.Region;
import com.cordialsr.domain.Report;
import com.cordialsr.domain.Scope;
import com.cordialsr.domain.ScopeType;
import com.cordialsr.domain.SmBranch;
import com.cordialsr.domain.SmContract;
import com.cordialsr.domain.SmFilterChange;
import com.cordialsr.domain.SmService;
import com.cordialsr.domain.StockIn;
import com.cordialsr.domain.StockStatus;
import com.cordialsr.domain.Unit;
import com.cordialsr.domain.security.Menu;
import com.cordialsr.domain.security.Role;
import com.cordialsr.domain.security.RoleDomainPermission;
import com.cordialsr.domain.security.RoleTransactionPermission;
import com.cordialsr.domain.security.Transaction;
import com.cordialsr.domain.security.User;

@Configuration
public class RepositoryConfig extends RepositoryRestConfigurerAdapter {
	
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(SmFilterChange.class, SmContract.class, SmBranch.class, SmService.class, Party.class, PartyType.class, Address.class, PriceList.class, Company.class, 
        		Country.class, Oblast.class, City.class, CityArea.class, AddressType.class, User.class, Department.class, Position.class, Currency.class, Branch.class,
        		Manufacturer.class, InvMainCategory.class, InvSubCategory.class, Unit.class, Inventory.class, Incoterm.class, FinCashflowStatement.class,
        		KassaBank.class, FinGlAccount.class, BranchType.class, FinActivityType.class, FinDoc.class, FinEntry.class, FinDocType.class, Invoice.class, InvoiceItem.class, 
        		PhoneNumber.class, InvoiceStatus.class, Menu.class, Transaction.class, Role.class, RoleTransactionPermission.class, RoleDomainPermission.class, Scope.class,
        		Region.class, StockIn.class, StockStatus.class, Award.class, AwardCase.class, AwardType.class, AwardTemplate.class, PaymentTemplate.class, Employee.class, 
        		Contract.class, FinOperation.class, ContractAwardSchedule.class, FinCurrate.class, ScopeType.class, InvFno.class, PlanFact.class, ContractPaymentSchedule.class,
        		ContractHistory.class, Report.class, PartyExperience.class);
    }

}