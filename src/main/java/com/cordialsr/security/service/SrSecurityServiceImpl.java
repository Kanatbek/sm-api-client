package com.cordialsr.security.service;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.domain.security.Role;
import com.cordialsr.domain.security.RoleDomainPermission;
import com.cordialsr.domain.security.RoleTransactionPermission;
import com.cordialsr.domain.security.User;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.security.repository.RoleRepository;

@Service
@Transactional
public class SrSecurityServiceImpl implements SrSecurityService {

	@Autowired 
	RoleRepository roleRep;
		
	@Override
	public Role updateRole(Role newRole) throws Exception {
		try {
			Role oldRole = roleRep.findOne(newRole.getRoleId());
			Set<User> ul = oldRole.getUsers();
			Set<RoleDomainPermission> rdpL = oldRole.getRoleDomainPermissions();
			oldRole = newRole.clone();
			oldRole.setUsers(ul);
			oldRole.setRoleDomainPermissions(rdpL);
			
			Set<RoleTransactionPermission> rtL = new HashSet<>();
			for (RoleTransactionPermission rt : newRole.getRoleTransactionPermissions()) {
				rt.setRole(oldRole);
				rtL.add(rt);
			}
			oldRole.setRoleTransactionPermissions(rtL);
			oldRole.setUpdatedDate(Calendar.getInstance().getTime());
			roleRep.save(oldRole);
			return oldRole;
		} catch( Exception e) {
			throw e;
		}
	}

	@Override
	public Role saveNewRole(Role newRole) throws Exception {
		try {
			Role oldRole = roleRep.findByName(newRole.getName());
			if (oldRole != null && !GeneralUtil.isEmptyInteger(oldRole.getRoleId())) {
				throw new Exception("Role '" + newRole.getName() + "' already exists!");
			}
			Set<RoleTransactionPermission> rtL = new HashSet<>();
			for (RoleTransactionPermission rt : newRole.getRoleTransactionPermissions()) {
				rt.setRole(newRole);
				rtL.add(rt);
			}
			newRole.setRoleTransactionPermissions(rtL);
			newRole.setCreatedDate(Calendar.getInstance().getTime());
			roleRep.save(newRole);
			return newRole;
		} catch( Exception e) {
			throw e;
		}
	}	

}
