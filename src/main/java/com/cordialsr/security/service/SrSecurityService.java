package com.cordialsr.security.service;

import com.cordialsr.domain.security.Role;

public interface SrSecurityService {
	Role updateRole(Role newRole) throws Exception;
	Role saveNewRole(Role newRole) throws Exception;
}
