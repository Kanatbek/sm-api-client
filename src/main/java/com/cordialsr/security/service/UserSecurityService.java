package com.cordialsr.security.service;

import java.util.Calendar;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.cordialsr.domain.security.User;
import com.cordialsr.domain.security.UserLog;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.security.repository.UserLogDao;
import com.cordialsr.security.repository.UserRepository;

@Service
public class UserSecurityService implements UserDetailsService {
	// The application Logger
	private static final Logger LOG = LoggerFactory.getLogger(UserSecurityService.class);
	
	@Autowired 
	private UserRepository userDao;
	
	@Autowired
    private UserLogDao ulDao;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userDao.findByUsername(username);
		if (null == user) {
			LOG.warn("Username {} not found", username);
			throw new UsernameNotFoundException("Username" + username + " not found");
		}		
		LOG.info("User found: " + user);
		// System.out.println("EncodedPW: " + user.getPassword());		
		return user;
	}
	
	@Transactional
	public void logUserLogin(String username) {
		try {
			User user = userDao.findByUsername(username);
			if (user != null && !GeneralUtil.isEmptyLong(user.getUserid())) {
				UserLog ul = new UserLog();
				ul.setDate(Calendar.getInstance().getTime());
				ul.setTime(Calendar.getInstance().getTime());
				ul.setInfo("Пользователь " + username + " выполнил ВХОД в программу.");
				ul.setLogType(UserLog.TYPE_LOGIN);
				ul.setTransaction(null);
				ul.setUser(user);
				ulDao.save(ul);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Transactional
	public void logUserLogout(String username) {
		try {
			User user = userDao.findByUsername(username);
			if (user != null && !GeneralUtil.isEmptyLong(user.getUserid())) {
				UserLog ul = new UserLog();
				ul.setDate(Calendar.getInstance().getTime());
				ul.setTime(Calendar.getInstance().getTime());
				ul.setInfo("Пользователь " + username + " выполнил ВЫХОД из программы.");
				ul.setLogType(UserLog.TYPE_LOGOUT);
				ul.setTransaction(null);
				ul.setUser(user);
				ulDao.save(ul);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
}
