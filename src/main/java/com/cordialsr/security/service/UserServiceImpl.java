package com.cordialsr.security.service;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.cordialsr.domain.security.Role;
import com.cordialsr.domain.security.RoleTransactionPermission;
import com.cordialsr.domain.security.User;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.security.repository.UserRepository;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	
	private static final Logger LOG = LoggerFactory.getLogger(UserSecurityService.class);
	
	@Autowired
	private UserRepository userDao;
	
	@Autowired 
	private BCryptPasswordEncoder passwordEncoder;
	
	@Override
	public void save(User user) {
		userDao.save(user);
	}
	
	@Override
	public User findByUsername(String username) {
		return userDao.findByUsername(username);
	}
	
	@Override
	public User findById(Long userId) {
		return userDao.findById(userId);
	}
	
	@Override
	public User findByEmail(String email) {
		return userDao.findByEmail(email);
	}
	
	@Override
	public boolean checkUsernameExists(String username) {
		if (null != findByUsername(username)) {
			return true;			
		} 
		return false;
	}
	
	@Override
	public boolean checkEmailExists(String email) {
		if (null != findByEmail(email)) {
			return true;			
		} 
		return false;
	}


	public boolean checkUserExists(String username, String email) {
		return checkUsernameExists(username) || checkEmailExists(email);
	}
	
	
	public User createUser(User user, Set<Role> userRoles) throws Exception {
        try {
        	User localUser = userDao.findByUsername(user.getUsername());

            if (localUser != null) {
                LOG.info("User with username {} already exist. Nothing will be done. ", user.getUsername());
                throw new Exception("User with username " + user.getUsername() + " already exist. Nothing will be done. ");
            } else {
                String encryptedPassword = passwordEncoder.encode(user.getPassword());
                user.setPassword(encryptedPassword);

                Date today = new Date();
                user.setDateCreated(today);
                user.setEnabled(true);
                user.setUserRoles(userRoles);
                localUser = userDao.save(user);
                
            }
            return localUser;
        } catch (Exception e) {
        	throw e;
        }
    }
	
	@Override
	public User updateUser(User newUser) throws Exception {
		try {
			User oldUser = userDao.findOne(newUser.getUserid());

			String pw = oldUser.getPassword();
			oldUser = newUser.clone();
			oldUser.setPassword(pw);
			
			Set<Role> rL = new HashSet<>();
			for (Role r : newUser.getUserRoles()) {
				Set<User> roleUsers = r.getUsers();
				roleUsers.add(oldUser);
				r.setUsers(roleUsers);
				rL.add(r);
			}
			
			if (!GeneralUtil.isEmptyString(newUser.getPassword())) {
				String encryptedPassword = passwordEncoder.encode(newUser.getPassword());
	            oldUser.setPassword(encryptedPassword);
			}			
			if (newUser.getCompany() != null && !GeneralUtil.isEmptyInteger(newUser.getCompany().getId())) {
				oldUser.setCompany(newUser.getCompany());				
			}
			if (newUser.getRegion() != null && !GeneralUtil.isEmptyInteger(newUser.getRegion().getId())) {
				oldUser.setRegion(newUser.getRegion());				
			}
			if (newUser.getBranch() != null && !GeneralUtil.isEmptyInteger(newUser.getBranch().getId())) {
				oldUser.setBranch(newUser.getBranch());				
			}			
			oldUser.setUserRoles(rL);
			oldUser.setDateUpdated(Calendar.getInstance().getTime());
			userDao.save(oldUser);
			return oldUser;
		} catch( Exception e) {
			throw e;
		}
	}

	@Override
	public List<RoleTransactionPermission> getUserRoleTrPermissions(User user) throws Exception {
		try {
			List<RoleTransactionPermission> rtpList = new ArrayList<>();
			for (Role r: user.getUserRoles()) {
				rtpList.addAll(r.getRoleTransactionPermissions());
			}
			return rtpList;
		} catch (Exception e) {
			throw e;
		}
	}
}
