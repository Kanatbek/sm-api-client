package com.cordialsr.security.service;

import java.util.List;
import java.util.Set;

import com.cordialsr.domain.security.Role;
import com.cordialsr.domain.security.RoleTransactionPermission;
import com.cordialsr.domain.security.User;

public interface UserService {

	void save(User user);

	User findByUsername(String username);

	User findById(Long userId);
	
	User createUser(User user, Set<Role> userRoles) throws Exception;
	
	User updateUser(User newUser) throws Exception;

	boolean checkUserExists(String username, String email);

	User findByEmail(String email);

	boolean checkUsernameExists(String username);

	boolean checkEmailExists(String email);
	
	List<RoleTransactionPermission> getUserRoleTrPermissions(User user) throws Exception;

}
