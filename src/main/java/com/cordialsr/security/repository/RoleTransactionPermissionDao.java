package com.cordialsr.security.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.projection.RoleTransactionPermissionProjection;
import com.cordialsr.domain.security.RoleTransactionPermission;

@RepositoryRestResource(collectionResourceRel = "roletrpermission", path = "roletrpermission",
						excerptProjection = RoleTransactionPermissionProjection.class)
public interface RoleTransactionPermissionDao extends CrudRepository<RoleTransactionPermission, Integer> {
	
}
