package com.cordialsr.security.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.projection.UserLogProjection;
import com.cordialsr.domain.security.UserLog;

@RepositoryRestResource(collectionResourceRel = "usrlg", path = "usrlg",
						excerptProjection = UserLogProjection.class)
public interface UserLogDao extends JpaRepository<UserLog, Long>{

	@Query("from UserLog c "
			+ " where c.user.id = ?1 "
			+ " ORDER BY c.id DESC")
	Page<UserLog> getAllByUser(@Param("uid") Long uid, Pageable pageRequest);
	
	@Query("from UserLog c "
			+ "ORDER BY c.id DESC")
	Page<UserLog> getAll(Pageable pageRequest);
	
	@Query("from UserLog c where "			
			+ " c.user.username like %?1%"
			+ " or c.user.firstName like %?1%"
			+ " or c.user.lastName like %?1%"
			+ " or c.date like %?1%"
			+ " or c.time like %?1%"
			+ " or c.logType like %?1%"
			+ " order by c.id desc")
	Page<UserLog> filter(
					@Param("filter") String inFilter, Pageable pageRequest);
}
