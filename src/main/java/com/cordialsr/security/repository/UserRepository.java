package com.cordialsr.security.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.projection.UserProjection;
import com.cordialsr.domain.security.User;

/**
 * Created by stalbek on 20.06.17.
 */
@RepositoryRestResource(collectionResourceRel = "users", path = "users",
						excerptProjection = UserProjection.class, exported = true)
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(@Param("username") String username);
	User findByEmail(@Param("email") String email);
	Iterable<User> findByEnabled(@Param("enabled") Boolean enabled);

	
	@Query(value = "from User c "
			+ " where c.id = ?1")
	User findById(@Param("userId") Long userId);
	
	@Query(value = "from User c "
			+ " where c.branch.id = ?1")
	ArrayList<User> getCaremanByBranch(@Param("bra") Integer bid);
}