package com.cordialsr.security.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.security.Transaction;

@RepositoryRestResource(collectionResourceRel = "transaction", path = "transaction")
public interface TransactionDao extends CrudRepository<Transaction, Integer> {

	@Query("from Transaction t order by t.id asc")
	Iterable<Transaction> getAllOrdered();
	
}
