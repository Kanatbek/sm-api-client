package com.cordialsr.security.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cordialsr.domain.projection.RoleProjection;
import com.cordialsr.domain.security.Role;

@RepositoryRestResource(collectionResourceRel = "role", path = "role", excerptProjection = RoleProjection.class)
public interface RoleRepository extends CrudRepository<Role, Integer> {
	Role findByName(String name);
}
