package com.cordialsr.security;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import com.cordialsr.domain.security.Role;
import com.cordialsr.domain.security.User;
import com.cordialsr.general.GeneralUtil;

public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Value("${jwt.header}")
    private String tokenHeader;

    private static final String paymeIp1 = "195.158.31.134";
    private static final String paymeIp2 = "195.158.31.10";
    
//    private static final String mobilnikKG_TEST = "77.235.7.130";
    private static final String mobilnikKG_PROD01 = "213.145.147.131";
    private static final String mobilnikKG_PROD02 = "92.38.41.100";
    
    private static final String asisnurKG_PROD = "95.47.232.100";
//    private static final String self = "185.217.180.34";
//    private static final String self = "0:0:0:0:0:0:0:1";
    
    private static final String qiwiKG_PROD[] = {
    			"0:0:0:0:0:0:0:1",
    			"91.135.193.250",
    			"185.217.180.34",
    			"212.42.104.209",
    			"212.42.102.25"
    		};
    
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        String originIp = request.getRemoteAddr();
        logger.info("Request origin address: " + originIp);
        
        String authToken = request.getHeader(this.tokenHeader);
        
        if(authToken != null && authToken.startsWith("Bearer ")) {
            authToken = authToken.substring(7);

            String username = jwtTokenUtil.getUsernameFromToken(authToken);

            logger.info("checking authentication for user " + username);

            if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {

                // It is not compelling necessary to load the use details from the database. You could also store the information
                // in the token and read it from it. It's up to you ;)
                UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);

                // For simple validation it is completely sufficient to just check the token integrity. You don't have to call
                // the database compellingly. Again it's up to you ;)
                if (jwtTokenUtil.validateToken(authToken, userDetails)) {
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    logger.info("authenticated user " + username + ", setting security context");
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
            
        // Basic token Authentication
        } else if(authToken != null && authToken.startsWith("Basic ") && checkIpForPaymeUZ(originIp)) {
            authToken = authToken.substring(6);
            if (authToken != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                	String login = jwtTokenUtil.validateBasicToken(authToken);
                    if (!GeneralUtil.isEmptyString(login)) {
                    	User user = new User(login);
                    	Set<Role> roles = new HashSet<>();
                    	Role role = new Role("ROLE_PAYME", true, new Date(), null, null, null, null);
                    	roles.add(role);
                    	user.setUserRoles(roles);
                    	UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
                    	authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                        logger.info("authenticated user " + "payme" + ", setting security context");
                        SecurityContextHolder.getContext().setAuthentication(authentication);
                    } else {
                    	String resBody = "{\"error\" : {\"code\" : -32504, \"message\" : \"Wrong access credentials.\", \"data\" : \"account\"} }";
                    	response.getWriter().append(resBody).flush();
                    }
            }
        } else if (originIp.equals(mobilnikKG_PROD01)
        		|| originIp.equals(mobilnikKG_PROD02)
//        		|| originIp.equals(self)
        		) {
        	User user = new User("mobilnik");
        	Set<Role> roles = new HashSet<>();
        	Role role = new Role("ROLE_PAYME", true, new Date(), null, null, null, null);
        	roles.add(role);
        	user.setUserRoles(roles);
        	UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
        	authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            logger.info("authenticated user " + "mobilnik" + ", setting security context");
            SecurityContextHolder.getContext().setAuthentication(authentication);
            
        } else if (originIp.equals(asisnurKG_PROD)
//        		|| originIp.equals(self)
        		) {
        	User user = new User("asisnur");
        	Set<Role> roles = new HashSet<>();
        	Role role = new Role("ROLE_PAYME", true, new Date(), null, null, null, null);
        	roles.add(role);
        	user.setUserRoles(roles);
        	UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
        	authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            logger.info("authenticated user " + "asisnur" + ", setting security context");
            SecurityContextHolder.getContext().setAuthentication(authentication);
            
        } else if (checkIpForQiwiKg(originIp)) {
        	User user = new User("qiwikg");
        	Set<Role> roles = new HashSet<>();
        	Role role = new Role("ROLE_PAYME", true, new Date(), null, null, null, null);
        	roles.add(role);
        	user.setUserRoles(roles);
        	UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
        	authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            logger.info("authenticated user " + "qiwikg" + ", setting security context");
            SecurityContextHolder.getContext().setAuthentication(authentication);
        } else {
        	if (originIp.equals(paymeIp1) || originIp.equals(paymeIp2)) {
        		String resBody = "{\"error\" : {\"code\" : -32504, \"message\" : \"Wrong access credentials.\", \"data\" : \"account\"} }";
            	response.getWriter().append(resBody).flush();
        	}
        }

        chain.doFilter(request, response);
    }
    
    private boolean checkIpForQiwiKg(String originIp) {
    	if (!GeneralUtil.isEmptyString(originIp)) {
    		for (String ip : qiwiKG_PROD) {
    			if (originIp.equals(ip)) return true;
    		}
    	}
    	return false;
    }
    
    private boolean checkIpForPaymeUZ(String originIp) {
    	if (!GeneralUtil.isEmptyString(originIp)) {    		
    		if (originIp.equals(paymeIp1) || originIp.equals(paymeIp2)) return true;
    	}
    	return false;
    }
}