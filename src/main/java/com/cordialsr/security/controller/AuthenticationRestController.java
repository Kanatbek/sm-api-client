package com.cordialsr.security.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cordialsr.domain.security.User;
import com.cordialsr.domain.security.UserLog;
import com.cordialsr.security.JwtAuthenticationRequest;
import com.cordialsr.security.JwtTokenUtil;
import com.cordialsr.security.JwtUser;
import com.cordialsr.security.repository.UserLogDao;
import com.cordialsr.security.service.JwtAuthenticationResponse;
import com.cordialsr.security.service.UserSecurityService;

@RestController
public class AuthenticationRestController {

    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;
    
    @Autowired
    private UserSecurityService usService;
    
    @Autowired
    @Qualifier("sessionRegistry")
    private SessionRegistry sessionRegistry;

    @RequestMapping(value = "${jwt.route.authentication.path}", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest, Device device) throws AuthenticationException {

        // Perform the security
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authenticationRequest.getUsername(),
                        authenticationRequest.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // Reload password post-security so we can generate token
        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        final String token = jwtTokenUtil.generateToken(userDetails, device);

        usService.logUserLogin(authenticationRequest.getUsername());
        
        // Return the token
        return ResponseEntity.ok(new JwtAuthenticationResponse(token));
    }

    @RequestMapping(value = "${jwt.route.authentication.refresh}", method = RequestMethod.GET)
    public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
        String token = request.getHeader(tokenHeader);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        JwtUser user = (JwtUser) userDetailsService.loadUserByUsername(username);

        if (jwtTokenUtil.canTokenBeRefreshed(token, user.getLastPasswordResetDate())) {
            String refreshedToken = jwtTokenUtil.refreshToken(token);
            return ResponseEntity.ok(new JwtAuthenticationResponse(refreshedToken));
        } else {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @RequestMapping(value = "loginusrls", method = RequestMethod.GET)
    public ResponseEntity<?> getAuthenticatedUsersList(HttpServletRequest request) {
    	List<Object> principals = sessionRegistry.getAllPrincipals();
    	List<String> usersNamesList = new ArrayList<String>();
        for (Object principal: principals) {
        	if (principal instanceof User) {
        		List<SessionInformation> siL = sessionRegistry.getAllSessions(principal, false);
        		System.out.println("SESSIONINFO: " + siL.get(0).getSessionId());
        		usersNamesList.add(((User) principal).getUsername());
            }
        }
        return ResponseEntity.ok(usersNamesList);
    }
    
    @RequestMapping(value = "${jwt.route.session.registry}", method = RequestMethod.GET)
    public ResponseEntity<?> getLoggeInUsersList(HttpServletRequest request) {
    	List<Object> principals = sessionRegistry.getAllPrincipals();
    	List<User> usersNamesList = new ArrayList<User>();
        for (Object principal: principals) {
        	if (principal instanceof User) {
        		User un = new User();
        		un.setUsername(((User) principal).getUsername());
        		if (!usersNamesList.contains(un))        			
        			usersNamesList.add(un);
            }
        }
        return ResponseEntity.ok(usersNamesList);        
    }

    @RequestMapping(value = "${jwt.route.session.kill}", method = RequestMethod.GET)
    public ResponseEntity<?> killSession(@RequestParam("un") String un) {
    	System.out.println("USERNAME LOGOUT: " + un);
    	UserDetails principal = userDetailsService.loadUserByUsername(un);
    	
    	if (principal != null) {
    		List<SessionInformation> siL = sessionRegistry.getAllSessions(principal, false);
    		System.out.println("ACTIVE SESSIONS: " + siL.size());
    		
    		for (SessionInformation si:siL) {
    			si.expireNow();
    		}    		
    		usService.logUserLogout(principal.getUsername());
        	System.out.println("Principal LOGGING OUT: " + principal.getUsername());
    		return ResponseEntity.ok("User successfully logged out.");
    	}    	
    	return ResponseEntity.ok("User logout failed!");
    }
}