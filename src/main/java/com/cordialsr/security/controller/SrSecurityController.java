package com.cordialsr.security.controller;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.domain.security.Role;
import com.cordialsr.domain.security.User;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.general.exception.DaoException;
import com.cordialsr.security.service.SrSecurityService;
import com.cordialsr.security.service.UserService;

@RepositoryRestController
@RequestMapping(value = "srsec")
public class SrSecurityController {

	@Autowired
	SrSecurityService secService;
	
	@Autowired 
	UserService userService;
	
	// **********************************************************************************************************

	@RequestMapping(value = "/newrole", method = RequestMethod.POST, 
            produces = MediaTypes.HAL_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Object> saveNewRole(@RequestBody Role newRole) {
        Role createdRole;
        String reason = null;
        
        if (newRole != null) {
        	ImmutablePair<Role, String> result = saveTheNewRole(newRole);
            reason = DaoException.justifyMessage(result.getRight());            
            createdRole = result.getLeft();
        } else {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "Role to be created is null").body(newRole);
        }
        
        if (GeneralUtil.isEmptyString(reason) && createdRole != null && createdRole.getRoleId() != null) {
            return ResponseEntity.status(HttpStatus.CREATED).body(createdRole);
        } else {
        	return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(reason);
        }
    }
	
	@Transactional
    private ImmutablePair<Role, String> saveTheNewRole(Role newRole) {
        String reason = null;
        try {
        	secService.saveNewRole(newRole);
        } catch(Exception ex) {
        	reason = ex.getMessage();        	
        }        
        return new ImmutablePair<Role, String>(newRole, reason);
    }

	// **********************************************************************************************************

	@RequestMapping(value = "/updateRole/{id}",
            method = {RequestMethod.PATCH, RequestMethod.PUT},
            produces = MediaTypes.HAL_JSON_VALUE)	
	public @ResponseBody
    ResponseEntity<Object> updateRole(@PathVariable("id") Integer roleId, @RequestBody Role role) {
        String reason = null;
        if (roleId != null) {
            if (role != null) { 
                ImmutablePair<Role, String> result = updateTheRole(role);
                reason = result.getRight();
                role = result.getLeft();
            } else {
                reason = "not enough data to update the Role [" + roleId + "]";
            }
        }
        if (StringUtils.isNotBlank(reason)) {
        	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
        }
        return ResponseEntity.status(HttpStatus.OK).body(role);
    }
	
	@Transactional
    private ImmutablePair<Role, String> updateTheRole(Role newRole) {
        String reason = null;
        try {
        	newRole = secService.updateRole(newRole);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();
        }        
        return new ImmutablePair<Role, String>(newRole, reason);
    }
	
	
	// **********************************************************************************************************

		@RequestMapping(value = "/newuser", method = RequestMethod.POST, 
	            produces = MediaTypes.HAL_JSON_VALUE)
	    @ResponseBody
	    public ResponseEntity<Object> saveNewUser(@RequestBody User newUser) {
	        User createdUser;
	        String reason = null;
	        
	        if (newUser != null) {
	        	ImmutablePair<User, String> result = saveTheNewUser(newUser);
	            reason = DaoException.justifyMessage(result.getRight());            
	            createdUser = result.getLeft();
	        } else {
	            return ResponseEntity.status(HttpStatus.NO_CONTENT).header("reason", "User to be created is null").body(newUser);
	        }
	 
	        if (GeneralUtil.isEmptyString(reason) && createdUser != null && createdUser.getUserid() != null) {
	            return ResponseEntity.status(HttpStatus.CREATED).body(createdUser);
	        } else {
	            return ResponseEntity
	                    .status(HttpStatus.BAD_REQUEST)
	                    .body(reason);
	        }
	    }
		
		@Transactional
	    private ImmutablePair<User, String> saveTheNewUser(User newUser) {
	        String reason = null;

	        try {
	        	userService.createUser(newUser, newUser.getUserRoles());
	        } catch(Exception ex) {
	        	reason = ex.getMessage();        	
	        }
	        
	        return new ImmutablePair<User, String>(newUser, reason);
	    }

	// **********************************************************************************************************
	
	@RequestMapping(value = "/updateUser/{id}",
            method = {RequestMethod.PATCH, RequestMethod.PUT},
            produces = MediaTypes.HAL_JSON_VALUE)	
	public @ResponseBody
    ResponseEntity<Object> updateUser(@PathVariable("id") Integer userId, @RequestBody User user) {
        String reason = null;
        if (userId != null) {
            if (user != null) { 
                ImmutablePair<User, String> result = updateTheUser(user);
                reason = result.getRight();
                user = result.getLeft();
            } else {
                reason = "not enough data to update the User [" + userId + "]";
            }
        }
        if (StringUtils.isNotBlank(reason)) {
        	return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(reason);
        }
        return ResponseEntity.status(HttpStatus.OK).body(user);
    }
	
	@Transactional
    private ImmutablePair<User, String> updateTheUser(User newUser) {
        String reason = null;
        try {
        	newUser = userService.updateUser(newUser);
        } catch(Exception ex) {
        	ex.printStackTrace();
        	reason = ex.getMessage();
        }        
        return new ImmutablePair<User, String>(newUser, reason);
    }
	
	// **********************************************************************************************************
	
	@RequestMapping(value = "/user/{username}", method = RequestMethod.GET,
			produces = MediaTypes.HAL_JSON_VALUE)
	@ResponseBody
    public ResponseEntity<Object> getUser(@PathVariable("username") String username) {
		try {
			User user = userService.findByUsername(username);
			user.setPassword("");
			return ResponseEntity.status(HttpStatus.OK).body(user);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
}