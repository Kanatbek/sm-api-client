package com.cordialsr.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class SmVisitCollectorDto implements Serializable, Cloneable {
	
	private String customerFio;
	private Integer visitTime;
	private BigDecimal summ;
	private BigDecimal toCashbox;
	private String currency;
	private String info;
	private Date inDate;
	private String collectorFio;
	
	public SmVisitCollectorDto() {
		
	}

	
	public SmVisitCollectorDto(String customerFio, Integer visitTime, BigDecimal summ,
							BigDecimal toCashbox, String currency, String info, Date inDate, String collectorFio) {
		this.customerFio = customerFio;
		this.visitTime = visitTime;
		this.summ = summ;
		this.toCashbox = toCashbox;
		this.currency = currency;
		this.info = info;
		this.inDate = inDate;
		this.collectorFio = collectorFio;
	}
	
	public String getCustomerFio() {
		return customerFio;
	}
	
	public void setCustomerFio(String customerFio) {
		this.customerFio = customerFio;
	}

	public Integer getVisitTime() {
		return visitTime;
	}

	public void setVisitTime(Integer visitTime) {
		this.visitTime = visitTime;
	}

	public BigDecimal getSumm() {
		return summ;
	}

	public void setSumm(BigDecimal summ) {
		this.summ = summ;
	}

	public BigDecimal getToCashbox() {
		return toCashbox;
	}

	public void setToCashbox(BigDecimal toCashbox) {
		this.toCashbox = toCashbox;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
	public Date getInDate() {
		return inDate;
	}

	public void setInDate(Date inDate) {
		this.inDate = inDate;
	}
	
	public String getCollectorFio() {
		return collectorFio;
	}
	
	public void setCollectorFio(String collectorFio) {
		this.collectorFio = collectorFio;
	}

	@Override
	public SmVisitCollectorDto clone() throws CloneNotSupportedException {
		return (SmVisitCollectorDto) super.clone();
	}

}
