package com.cordialsr.dto;

import java.io.Serializable;
import java.math.BigDecimal;

//@Relation(value = "findoc", collectionRelation = "findocs")
public class Money implements Serializable, Cloneable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal summ;
	private String currency;
	
	public Money() {
	}

	public Money(String currency) {
		this.currency = currency;
	}
	
	public Money(BigDecimal summ, String currency) {
		this.summ = summ;
		this.currency = currency;
	}
	
	public BigDecimal getSumm() {
		return summ;
	}
	public void setSumm(BigDecimal summ) {
		this.summ = summ;
	}
	
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Override
	public Money clone() throws CloneNotSupportedException {
		return (Money) super.clone();
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currency == null) ? 0 : currency.hashCode());
		result = prime * result + ((summ == null) ? 0 : summ.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Money other = (Money) obj;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (summ == null) {
			if (other.summ != null)
				return false;
		} else if (!summ.equals(other.summ))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Money [summ=" + summ + ", currency=" + currency + "]";
	}

	
}
