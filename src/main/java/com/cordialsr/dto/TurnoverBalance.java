package com.cordialsr.dto;

import java.io.Serializable;

import com.cordialsr.domain.FinCashflowStatement;
import com.cordialsr.domain.FinGlAccount;
import com.cordialsr.domain.Party;

public class TurnoverBalance implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private FinGlAccount glAccount;
	private Party party;
	private FinCashflowStatement cfs;
	private Balance balanceStart;
	private Balance turnover;
	private Balance balanceEnd; 
	
	public TurnoverBalance() {
	}
	
	public TurnoverBalance(FinGlAccount glAccount, Party party, FinCashflowStatement cfs, Balance balanceStart, Balance turnover, Balance balanceEnd) {
		this.glAccount = glAccount;
		this.party = party;
		this.cfs = cfs;
		this.balanceStart = balanceStart;
		this.turnover = turnover;
		this.balanceEnd = balanceEnd;
	}
	
	public FinGlAccount getGlAccount() {
		return glAccount;
	}
	public void setGlAccount(FinGlAccount glAccount) {
		this.glAccount = glAccount;
	}
	public Balance getBalanceStart() {
		return balanceStart;
	}
	public void setBalanceStart(Balance balanceStart) {
		this.balanceStart = balanceStart;
	}
	public Balance getTurnover() {
		return turnover;
	}
	public void setTurnover(Balance turnover) {
		this.turnover = turnover;
	}
	public Balance getBalanceEnd() {
		return balanceEnd;
	}
	public void setBalanceEnd(Balance balanceEnd) {
		this.balanceEnd = balanceEnd;
	}
	
	public Party getParty() {
		return party;
	}

	public void setParty(Party party) {
		this.party = party;
	}
	
	public FinCashflowStatement getCfs() {
		return cfs;
	}

	public void setCfs(FinCashflowStatement cfs) {
		this.cfs = cfs;
	}

	@Override
	public TurnoverBalance clone() throws CloneNotSupportedException {
		return (TurnoverBalance) super.clone();
	}
	
}
