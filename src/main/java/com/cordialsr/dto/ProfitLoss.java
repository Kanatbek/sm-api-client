package com.cordialsr.dto;

import java.io.Serializable;
import java.util.List;

public class ProfitLoss implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<TurnoverBalance> incomeTbL;
	private Balance incomeTotal;
	private List<TurnoverBalance> expenceTbL;
	private Balance expenceTotal;
	private Balance total;
	
	public ProfitLoss() {
	
	}

	public List<TurnoverBalance> getIncomeTbL() {
		return incomeTbL;
	}

	public void setIncomeTbL(List<TurnoverBalance> incomeTbL) {
		this.incomeTbL = incomeTbL;
	}

	public Balance getIncomeTotal() {
		return incomeTotal;
	}

	public void setIncomeTotal(Balance incomeTotal) {
		this.incomeTotal = incomeTotal;
	}

	public List<TurnoverBalance> getExpenceTbL() {
		return expenceTbL;
	}

	public void setExpenceTbL(List<TurnoverBalance> expenceTbL) {
		this.expenceTbL = expenceTbL;
	}

	public Balance getExpenceTotal() {
		return expenceTotal;
	}

	public void setExpenceTotal(Balance expenceTotal) {
		this.expenceTotal = expenceTotal;
	}

	public Balance getTotal() {
		return total;
	}

	public void setTotal(Balance total) {
		this.total = total;
	}
	
}
