package com.cordialsr.dto.mini;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Transient;

import com.cordialsr.domain.Company;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Department;
import com.cordialsr.domain.Position;

public class PlanFactMiniDto {
	private Integer id;
	private Integer year;
	private Integer month;
	private Company company;
	private Department department;
	private RegionMiniDto region;
	private BranchMiniDto branch;
	private Position position;
	private PartyMiniDto party;
	private BigDecimal plan;
	private BigDecimal fact;
	private Boolean isOverdue;
	private Date cpuDate;
	private Currency currency;
	private BigDecimal remain;
	private BigDecimal paidPast;
	private BigDecimal paidCurrent;
	private BigDecimal paidAhead;
	

	public PlanFactMiniDto() {
	}
	
	public PlanFactMiniDto(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public RegionMiniDto getRegion() {
		return region;
	}

	public void setRegion(RegionMiniDto region) {
		this.region = region;
	}

	public BranchMiniDto getBranch() {
		return branch;
	}

	public void setBranch(BranchMiniDto branch) {
		this.branch = branch;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public PartyMiniDto getParty() {
		return party;
	}

	public void setParty(PartyMiniDto party) {
		this.party = party;
	}

	public BigDecimal getPlan() {
		return plan;
	}

	public void setPlan(BigDecimal plan) {
		this.plan = plan;
	}

	public BigDecimal getFact() {
		return fact;
	}

	public void setFact(BigDecimal fact) {
		this.fact = fact;
	}

	public Date getCpuDate() {
		return cpuDate;
	}

	public void setCpuDate(Date cpuDate) {
		this.cpuDate = cpuDate;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Boolean getIsOverdue() {
		return isOverdue;
	}

	public void setIsOverdue(Boolean isOverdue) {
		this.isOverdue = isOverdue;
	}
	
	@Transient
	public BigDecimal getRemain() {
		return remain;
	}

	@Transient
	public void setRemain(BigDecimal remain) {
		this.remain = remain;
	}

	@Transient
	public BigDecimal getPaidPast() {
		return paidPast;
	}

	@Transient
	public void setPaidPast(BigDecimal paidPast) {
		this.paidPast = paidPast;
	}

	@Transient
	public BigDecimal getPaidCurrent() {
		return paidCurrent;
	}

	@Transient
	public void setPaidCurrent(BigDecimal paidCurrent) {
		this.paidCurrent = paidCurrent;
	}

	@Transient
	public BigDecimal getPaidAhead() {
		return paidAhead;
	}

	@Transient
	public void setPaidAhead(BigDecimal paidAhead) {
		this.paidAhead = paidAhead;
	}
		
}
