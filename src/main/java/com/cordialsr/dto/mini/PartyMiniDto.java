package com.cordialsr.dto.mini;

import java.util.Date;

import com.cordialsr.domain.PartyType;

public class PartyMiniDto {
	private Long id;
	private PartyType partyType;
	private String iinBin;
	private String firstname;
	private String lastname;
	private String middlename;
	private String email;
	private Date birthday;
	private Boolean isYur;
	private String passportNumber;
	private Date dateIssue;
	private Date dateExpire;
	private String issuedInstance;
	private String iik;
	private String bik;
	private String companyName;
	private String fullFIO;
	
	PartyMiniDto() {
		
	}

	PartyMiniDto(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PartyType getPartyType() {
		return partyType;
	}

	public void setPartyType(PartyType partyType) {
		this.partyType = partyType;
	}

	public String getIinBin() {
		return iinBin;
	}

	public void setIinBin(String iinBin) {
		this.iinBin = iinBin;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Boolean getIsYur() {
		return isYur;
	}

	public void setIsYur(Boolean isYur) {
		this.isYur = isYur;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public Date getDateIssue() {
		return dateIssue;
	}

	public void setDateIssue(Date dateIssue) {
		this.dateIssue = dateIssue;
	}

	public Date getDateExpire() {
		return dateExpire;
	}

	public void setDateExpire(Date dateExpire) {
		this.dateExpire = dateExpire;
	}

	public String getIssuedInstance() {
		return issuedInstance;
	}

	public void setIssuedInstance(String issuedInstance) {
		this.issuedInstance = issuedInstance;
	}

	public String getIik() {
		return iik;
	}

	public void setIik(String iik) {
		this.iik = iik;
	}

	public String getBik() {
		return bik;
	}

	public void setBik(String bik) {
		this.bik = bik;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getFullFIO() {
		return fullFIO;
	}

	public void setFullFIO(String fullFIO) {
		this.fullFIO = fullFIO;
	}
	
}
