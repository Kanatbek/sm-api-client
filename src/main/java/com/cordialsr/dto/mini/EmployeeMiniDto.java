package com.cordialsr.dto.mini;

import com.cordialsr.domain.Position;

public class EmployeeMiniDto {

	private Integer id;
	private PartyMiniDto party;
	private Position position;
	
	public EmployeeMiniDto() {
	
	}
	
	public EmployeeMiniDto(Integer id) {
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public PartyMiniDto getParty() {
		return party;
	}
	public void setParty(PartyMiniDto party) {
		this.party = party;
	}
	public Position getPosition() {
		return position;
	}
	public void setPosition(Position position) {
		this.position = position;
	}
	
}
