package com.cordialsr.dto.mini;

public class RegionMiniDto {
	private Integer id;
	private String name;
	
	public RegionMiniDto() {
	}
	
	public RegionMiniDto(Integer id) {
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
