package com.cordialsr.dto.mini;

import java.math.BigDecimal;
import java.util.Date;

import com.cordialsr.domain.Address;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.PaymentStatus;
import com.cordialsr.domain.PhoneNumber;
import com.cordialsr.domain.ServiceCategory;

public class ContractMiniDto {

	private Long id;
//	private Company company;
	private BranchMiniDto branch;
//	private Branch serviceBranch;
//	private ContractType contractType;
//	private ContractStatus contractStatus;
//	private Employee coordinator;
//	private Employee demosec;
//	private Employee director;
//	private Employee manager;
//	private Employee careman;
	private EmployeeMiniDto dealer;
//	private Employee fitter;
//	
	private PartyMiniDto customer;
	private Address addrPay;
	private PhoneNumber phonePay;
	private PhoneNumber mobilePay;
	
//	private Party exploiter;
//	private Address addrFact;
//	private PhoneNumber phoneFact;
//	private PhoneNumber mobileFact;
	
	private PaymentStatus paymentStatus;
	private ServiceCategory serviceCategory;
	private Date dateSigned;
	private Date dateIssue;
	private String contractNumber;
	private String refkey;
//	private Date registeredDate;
//	private User registeredUser;
//	private Date dateUpdated;
	private BigDecimal cost;
	private Integer month;
	private BigDecimal discount;
	private BigDecimal summ;
	private Currency currency;
	private BigDecimal paid;
	private BigDecimal rate;
	private BigDecimal fromDealerSumm;
	private Boolean forbuh;
	private Boolean isRent;
//	private String info;
	
//	private List<ContractPaymentSchedule> paymentSchedules = new ArrayList<>();
//	private List<FinDoc> finDocs = new ArrayList<>();
//	private List<ContractItem> contractItems = new ArrayList<>();
//	private List<ContractPromos> contractPromos = new ArrayList<>();
//	private List<ContractAwardSchedule> cawSchedules = new ArrayList<>();
	
	public ContractMiniDto() {
		
	}
	
	public ContractMiniDto(Long id) {
		this.id = id;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public PartyMiniDto getCustomer() {
		return customer;
	}
	public void setCustomer(PartyMiniDto customer) {
		this.customer = customer;
	}
	
	public PaymentStatus getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(PaymentStatus paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	
	public ServiceCategory getServiceCategory() {
		return serviceCategory;
	}
	public void setServiceCategory(ServiceCategory serviceCategory) {
		this.serviceCategory = serviceCategory;
	}
	
	public Date getDateSigned() {
		return dateSigned;
	}
	public void setDateSigned(Date dateSigned) {
		this.dateSigned = dateSigned;
	}
	
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	
	public String getRefkey() {
		return refkey;
	}
	public void setRefkey(String refkey) {
		this.refkey = refkey;
	}
	
	public BigDecimal getCost() {
		return cost;
	}
	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}
	
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	
	public BigDecimal getSumm() {
		return summ;
	}
	public void setSumm(BigDecimal summ) {
		this.summ = summ;
	}
	
	public Currency getCurrency() {
		return currency;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	
	public BigDecimal getPaid() {
		return paid;
	}
	public void setPaid(BigDecimal paid) {
		this.paid = paid;
	}
	
	public BigDecimal getRate() {
		return rate;
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	
	public BigDecimal getFromDealerSumm() {
		return fromDealerSumm;
	}
	public void setFromDealerSumm(BigDecimal fromDealerSumm) {
		this.fromDealerSumm = fromDealerSumm;
	}
	
	public Boolean getForbuh() {
		return forbuh;
	}
	public void setForbuh(Boolean forbuh) {
		this.forbuh = forbuh;
	}
	
	public Boolean getIsRent() {
		return isRent;
	}
	public void setIsRent(Boolean isRent) {
		this.isRent = isRent;
	}
	
	public EmployeeMiniDto getDealer() {
		return dealer;
	}
	public void setDealer(EmployeeMiniDto dealer) {
		this.dealer = dealer;
	}
	
	public Date getDateIssue() {
		return dateIssue;
	}
	public void setDateIssue(Date dateIssue) {
		this.dateIssue = dateIssue;
	}
	public BranchMiniDto getBranch() {
		return branch;
	}
	public void setBranch(BranchMiniDto branch) {
		this.branch = branch;
	}
	public Address getAddrPay() {
		return addrPay;
	}
	public void setAddrPay(Address addrPay) {
		this.addrPay = addrPay;
	}
	public PhoneNumber getPhonePay() {
		return phonePay;
	}
	public void setPhonePay(PhoneNumber phonePay) {
		this.phonePay = phonePay;
	}
	public PhoneNumber getMobilePay() {
		return mobilePay;
	}
	public void setMobilePay(PhoneNumber mobilePay) {
		this.mobilePay = mobilePay;
	}
	
}
