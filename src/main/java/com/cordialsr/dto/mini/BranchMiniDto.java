package com.cordialsr.dto.mini;

import com.cordialsr.domain.BranchType;

public class BranchMiniDto {

	private Integer id;
	private String branchName;
	private BranchType branchType;

	public BranchMiniDto() {
	
	}
	
	public BranchMiniDto(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public BranchType getBranchType() {
		return branchType;
	}

	public void setBranchType(BranchType branchType) {
		this.branchType = branchType;
	}
	
}
