package com.cordialsr.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.ContractPaymentSchedule;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Party;


public class ContractDto {
	
	private Long id;
	
	private Party customer;
	private Party exploiter;

	private Company company;
	private Branch branch;
	
	private Date dateSigned;
	private String contractNumber;
	private BigDecimal cost;
	private Integer month;
	private BigDecimal discount;
	private BigDecimal summ;
	private Currency currency;
	private BigDecimal paid;
	private BigDecimal rate;
	private BigDecimal fromDealerSumm;
	private Boolean isRent;
	private String info;
	private Boolean storno;
	private Date stornoDate;
	private List<ContractPaymentSchedule> paymentSchedules = new ArrayList<>();
	
	
	public ContractDto() {
		
	}
	
	public ContractDto(Long id) {
		this.id = id;
	}
	
	public ContractDto(Long id, Party customer, Party exploiter, Company company, Branch branch, Date dateSigned, String contractNumber,
			BigDecimal cost, Integer month, BigDecimal discount, BigDecimal summ, Currency currency, 
			BigDecimal paid, BigDecimal rate, BigDecimal fromDealerSumm, Boolean isRent, 
			String info, Boolean storno, Date stornoDate, List<ContractPaymentSchedule> paymentSchedules) {
		this.id = id;
		this.customer = customer;
		this.exploiter = exploiter;
		this.company = company;
		this.branch = branch;
		this.dateSigned = dateSigned;
		this.contractNumber = contractNumber;
		this.cost = cost;
		this.month = month;
		this.discount = discount;
		this.summ = summ;
		this.currency = currency;
		this.paid = paid;
		this.rate = rate;
		this.fromDealerSumm = fromDealerSumm;
		this.isRent = isRent;
		this.info = info;
		this.storno = storno;
		this.stornoDate = stornoDate;
		this.paymentSchedules = paymentSchedules;
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Party getCustomer() {
		return customer;
	}
	public void setCustomer(Party customer) {
		this.customer = customer;
	}
	
	public Party getExploiter() {
		return exploiter;
	}
	public void setExploiter(Party exploiter) {
		this.exploiter = exploiter;
	}
	
	public Date getDateSigned() {
		return dateSigned;
	}
	public void setDateSigned(Date dateSigned) {
		this.dateSigned = dateSigned;
	}
	
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	
	public BigDecimal getCost() {
		return cost;
	}
	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}
	
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	
	public BigDecimal getSumm() {
		return summ;
	}
	public void setSumm(BigDecimal summ) {
		this.summ = summ;
	}
	
	public Currency getCurrency() {
		return currency;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	
	public BigDecimal getPaid() {
		return paid;
	}
	public void setPaid(BigDecimal paid) {
		this.paid = paid;
	}
	
	public BigDecimal getRate() {
		return rate;
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	
	public BigDecimal getFromDealerSumm() {
		return fromDealerSumm;
	}
	public void setFromDealerSumm(BigDecimal fromDealerSumm) {
		this.fromDealerSumm = fromDealerSumm;
	}
	
	public Boolean getIsRent() {
		return isRent;
	}
	public void setIsRent(Boolean isRent) {
		this.isRent = isRent;
	}
	
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	
	public Boolean getStorno() {
		return storno;
	}
	public void setStorno(Boolean storno) {
		this.storno = storno;
	}
	
	public Date getStornoDate() {
		return stornoDate;
	}
	public void setStornoDate(Date stornoDate) {
		this.stornoDate = stornoDate;
	}
	
	public List<ContractPaymentSchedule> getPaymentSchedules() {
		return paymentSchedules;
	}
	public void setPaymentSchedules(List<ContractPaymentSchedule> paymentSchedules) {
		this.paymentSchedules = paymentSchedules;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}
}
