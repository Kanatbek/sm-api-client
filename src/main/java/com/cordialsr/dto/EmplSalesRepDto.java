package com.cordialsr.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class EmplSalesRepDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long emplId;
	private String fio;
	private String position;
	private String kind;
	private Integer rent;
	private BigDecimal summRent;
	private Integer sale;
	private BigDecimal summSale;
	private Integer total;
	private BigDecimal summTotal;
	private String currency;
	private String info;

	public EmplSalesRepDto() {
		
	}
	
	public EmplSalesRepDto(Long emplId, String fio, String position, Integer rent, String kind, BigDecimal summRent, Integer sale,
			BigDecimal summSale, Integer total, BigDecimal summTotal, String currency, String info) {
		this.emplId = emplId;
		this.fio = fio;
		this.position = position;
		this.rent = rent;
		this.kind = kind;
		this.summRent = summRent;
		this.sale = sale;
		this.summSale = summSale;
		this.total = total;
		this.summTotal = summTotal;
		this.currency = currency;
		this.info = info;
	}

	public Long getEmplId() {
		return emplId;
	}

	public void setEmplId(Long emplId) {
		this.emplId = emplId;
	}

	public String getFio() {
		return fio;
	}

	public void setFio(String fio) {
		this.fio = fio;
	}
	
	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Integer getRent() {
		return rent;
	}

	public void setRent(Integer rent) {
		this.rent = rent;
	}

	public BigDecimal getSummRent() {
		return summRent;
	}

	public void setSummRent(BigDecimal summRent) {
		this.summRent = summRent;
	}

	public Integer getSale() {
		return sale;
	}

	public void setSale(Integer sale) {
		this.sale = sale;
	}

	public BigDecimal getSummSale() {
		return summSale;
	}

	public void setSummSale(BigDecimal summSale) {
		this.summSale = summSale;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public BigDecimal getSummTotal() {
		return summTotal;
	}

	public void setSummTotal(BigDecimal summTotal) {
		this.summTotal = summTotal;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
}

