package com.cordialsr.dto;

import java.io.Serializable;

public class Balance implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String glCode;
	private Long partyId;
	private Money debet;
	private Money credit;

	public Balance() {
		
	}
	
	public Balance(String glCode) {
		this.glCode = glCode;
	}
	
	public Balance(String glCode, Long partyId, Money debet, Money credit) {
		this.glCode = glCode;
		this.partyId = partyId;
		this.debet = debet;
		this.credit = credit;
	}

	public String getGlCode() {
		return glCode;
	}

	public void setGlCode(String glCode) {
		this.glCode = glCode;
	}

	public Money getDebet() {
		return debet;
	}

	public void setDebet(Money debet) {
		this.debet = debet;
	}

	public Money getCredit() {
		return credit;
	}

	public void setCredit(Money credit) {
		this.credit = credit;
	}
	
	public Long getPartyId() {
		return partyId;
	}

	public void setPartyId(Long partyId) {
		this.partyId = partyId;
	}

	@Override
	public Balance clone() throws CloneNotSupportedException {
		return (Balance) super.clone();
	}
	
}
