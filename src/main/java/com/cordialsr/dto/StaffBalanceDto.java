package com.cordialsr.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.cordialsr.domain.Party;

public class StaffBalanceDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Party staff;
	private List<Money> balance = new ArrayList<>();
	
	public StaffBalanceDto() {
	}

	public StaffBalanceDto(Party staff, List<Money> balance) {
		this.staff = staff;
		this.balance = balance;
	}
	
	public Party getStaff() {
		return staff;
	}
	public void setStaff(Party staff) {
		this.staff = staff;
	}
	public List<Money> getBalance() {
		return balance;
	}
	public void setBalance(List<Money> balance) {
		this.balance = balance;
	}
}
