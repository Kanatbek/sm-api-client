package com.cordialsr.dto;

import java.io.Serializable;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.FinCashflowStatement;
import com.cordialsr.domain.FinDoc;
import com.cordialsr.domain.Party;

public class FinRepCashFlowDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Branch branch;
	private FinCashflowStatement cf;
	private Balance balance;
	private Money total;
	private Party party;
	private String info;
	private FinDoc finDoc;
	
	public FinRepCashFlowDto() {
		
	}
	
	public FinRepCashFlowDto(Branch branch, FinCashflowStatement cf, Party party, Balance balance, Money total,
			String info, FinDoc finDoc) {
		this.branch = branch;
		this.cf = cf;
		this.balance = balance;
		this.total = total;
		this.party = party;
		this.info = info;
		this.finDoc = finDoc;
	}
	
	public FinDoc getFinDoc() {
		return finDoc;
	}

	public void setFinDoc(FinDoc finDoc) {
		this.finDoc = finDoc;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public FinCashflowStatement getCf() {
		return cf;
	}
	public void setCf(FinCashflowStatement cf) {
		this.cf = cf;
	}
	public Balance getBalance() {
		return balance;
	}
	public void setBalance(Balance balance) {
		this.balance = balance;
	}
	public Money getTotal() {
		return total;
	}
	public void setTotal(Money total) {
		this.total = total;
	}
	public Party getParty() {
		return party;
	}
	public void setParty(Party party) {
		this.party = party;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	
}
