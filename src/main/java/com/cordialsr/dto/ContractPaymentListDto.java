package com.cordialsr.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.cordialsr.dto.mini.ContractMiniDto;

public class ContractPaymentListDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ContractMiniDto contract;
	private Long contractId;
	private String customerFIO;
	private String currency;
	private BigDecimal cost;
	private BigDecimal paid;
	private BigDecimal overdue;
	private BigDecimal remain;
	
	private Date previewsDate;
	private BigDecimal previewsSumm;
	private Integer previewsOrder;
	
	private Date currentDate;
	private BigDecimal currentSumm;
	private Integer currentOrder;
	
	private Date nextDate;
	private BigDecimal nextSumm;
	private Integer nextOrder;
	
	private BigDecimal paymentDue;
	
	public ContractPaymentListDto() {
		
	}

	public ContractMiniDto getContract() {
		return contract;
	}

	public void setContract(ContractMiniDto contract) {
		this.contract = contract;
	}

	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	public BigDecimal getPaid() {
		return paid;
	}

	public void setPaid(BigDecimal paid) {
		this.paid = paid;
	}

	public BigDecimal getOverdue() {
		return overdue;
	}

	public void setOverdue(BigDecimal overdue) {
		this.overdue = overdue;
	}

	public BigDecimal getRemain() {
		return remain;
	}

	public void setRemain(BigDecimal remain) {
		this.remain = remain;
	}

	public Date getPreviewsDate() {
		return previewsDate;
	}

	public void setPreviewsDate(Date previewsDate) {
		this.previewsDate = previewsDate;
	}

	public BigDecimal getPreviewsSumm() {
		return previewsSumm;
	}

	public void setPreviewsSumm(BigDecimal previewsSumm) {
		this.previewsSumm = previewsSumm;
	}

	public Date getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}

	public BigDecimal getCurrentSumm() {
		return currentSumm;
	}

	public void setCurrentSumm(BigDecimal currentSumm) {
		this.currentSumm = currentSumm;
	}

	public Date getNextDate() {
		return nextDate;
	}

	public void setNextDate(Date nextDate) {
		this.nextDate = nextDate;
	}

	public BigDecimal getNextSumm() {
		return nextSumm;
	}

	public void setNextSumm(BigDecimal nextSumm) {
		this.nextSumm = nextSumm;
	}

	public BigDecimal getPaymentDue() {
		return paymentDue;
	}

	public void setPaymentDue(BigDecimal paymentDue) {
		this.paymentDue = paymentDue;
	}

	public Integer getPreviewsOrder() {
		return previewsOrder;
	}

	public void setPreviewsOrder(Integer previewsOrder) {
		this.previewsOrder = previewsOrder;
	}

	public Integer getCurrentOrder() {
		return currentOrder;
	}

	public void setCurrentOrder(Integer currentOrder) {
		this.currentOrder = currentOrder;
	}

	public Integer getNextOrder() {
		return nextOrder;
	}

	public void setNextOrder(Integer nextOrder) {
		this.nextOrder = nextOrder;
	}
	
	public Long getContractId() {
		return contractId;
	}
	
	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}
	
	public String getCustomerFIO() {
		return customerFIO;
	}
	
	public void setCustomerFIO(String customerFIO) {
		this.customerFIO = customerFIO;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
