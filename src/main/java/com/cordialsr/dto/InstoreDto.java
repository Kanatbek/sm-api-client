package com.cordialsr.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

import com.cordialsr.domain.Branch;

public class InstoreDto implements Serializable, Cloneable {

	private Branch branch;

	private BigDecimal transferIn;
	private BigDecimal transferOut;
	private BigDecimal balance;
	private BigDecimal returned;
	private BigDecimal posting;
	private BigDecimal client;
	private BigDecimal getting;
	private BigDecimal writeOff;
	private BigDecimal accountable;
	private BigDecimal accountReturn;
	
	private ArrayList<ReportDto> reports = new ArrayList<ReportDto>();
	
	public InstoreDto(){
		
	}
	
	public InstoreDto(Branch branch, ArrayList<ReportDto> reports){
		this.branch = branch;
		this.reports = reports;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public ArrayList<ReportDto> getReports() {
		return reports;
	}

	public void setReports(ArrayList<ReportDto> reports) {
		this.reports = reports;
	}
	
	
	
	
	public BigDecimal getTransferIn() {
		return transferIn;
	}

	public void setTransferIn(BigDecimal transferIn) {
		this.transferIn = transferIn;
	}

	public BigDecimal getTransferOut() {
		return transferOut;
	}

	public void setTransferOut(BigDecimal transferOut) {
		this.transferOut = transferOut;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public BigDecimal getReturned() {
		return returned;
	}

	public void setReturned(BigDecimal returned) {
		this.returned = returned;
	}

	public BigDecimal getPosting() {
		return posting;
	}

	public void setPosting(BigDecimal posting) {
		this.posting = posting;
	}

	public BigDecimal getGetting() {
		return getting;
	}

	public void setGetting(BigDecimal getting) {
		this.getting = getting;
	}

	public BigDecimal getAccountable() {
		return accountable;
	}

	public void setAccountable(BigDecimal accountable) {
		this.accountable = accountable;
	}

	
	
	public BigDecimal getClient() {
		return client;
	}

	public void setClient(BigDecimal client) {
		this.client = client;
	}

	
	public BigDecimal getWriteOff() {
		return writeOff;
	}

	public void setWriteOff(BigDecimal writeOff) {
		this.writeOff = writeOff;
	}
	
	

	public BigDecimal getAccountReturn() {
		return accountReturn;
	}

	public void setAccountReturn(BigDecimal accountReturn) {
		this.accountReturn = accountReturn;
	}

	@Override
	public InstoreDto clone() throws CloneNotSupportedException {
		return (InstoreDto) super.clone();
	}
	
}
