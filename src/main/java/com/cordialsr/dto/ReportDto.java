package com.cordialsr.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.cordialsr.domain.Inventory;

public class ReportDto implements Serializable, Cloneable {
	
	private Inventory inventory;
	private BigDecimal transferIn;
	private BigDecimal transferOut;
	private BigDecimal balance;
	private BigDecimal returned;
	private BigDecimal client;
	private BigDecimal posting;
	private BigDecimal getting;
	private BigDecimal writeOff;
	private BigDecimal accountable;
	private BigDecimal accountReturn;
	
	public ReportDto() {		
	}
	
	public ReportDto(Inventory inventory, BigDecimal transferIn, BigDecimal transferOut, 
			BigDecimal balance, BigDecimal returned, BigDecimal posting, BigDecimal getting, BigDecimal accountable) {
		this.inventory = inventory;
		this.transferIn = transferIn;
		this.transferOut = transferOut;
		this.balance = balance;
		this.returned = returned;
		this.posting = posting;
		this.getting = getting;
		this.accountable = accountable;
	}


	public Inventory getInventory() {
		return inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	public BigDecimal getTransferIn() {
		return transferIn;
	}

	public void setTransferIn(BigDecimal transferIn) {
		this.transferIn = transferIn;
	}

	public BigDecimal getTransferOut() {
		return transferOut;
	}

	public void setTransferOut(BigDecimal transferOut) {
		this.transferOut = transferOut;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public BigDecimal getReturned() {
		return returned;
	}

	public void setReturned(BigDecimal returned) {
		this.returned = returned;
	}

	public BigDecimal getPosting() {
		return posting;
	}

	public void setPosting(BigDecimal posting) {
		this.posting = posting;
	}

	public BigDecimal getGetting() {
		return getting;
	}

	public void setGetting(BigDecimal getting) {
		this.getting = getting;
	}
	
	public BigDecimal getAccountable() {
		return accountable;
	}

	public void setAccountable(BigDecimal accountable) {
		this.accountable = accountable;
	}

	
	
	public BigDecimal getClient() {
		return client;
	}

	public void setClient(BigDecimal client) {
		this.client = client;
	}

	
	
	public BigDecimal getWriteOff() {
		return writeOff;
	}

	public void setWriteOff(BigDecimal writeOff) {
		this.writeOff = writeOff;
	}
	
	

	public BigDecimal getAccountReturn() {
		return accountReturn;
	}

	public void setAccountReturn(BigDecimal accountReturn) {
		this.accountReturn = accountReturn;
	}

	@Override
	public ReportDto clone() throws CloneNotSupportedException {
		return (ReportDto) super.clone();
	}

}
