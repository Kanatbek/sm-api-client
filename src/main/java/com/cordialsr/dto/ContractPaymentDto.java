package com.cordialsr.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Contract;
import com.cordialsr.domain.ContractPaymentSchedule;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.KassaBank;
import com.cordialsr.domain.security.User;

public class ContractPaymentDto implements Serializable, Cloneable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Branch branch;
	private Date pdate;
	private ContractPaymentSchedule contractPaymentSchedule;
	private Contract contract;
	private BigDecimal paymentDue;
	private KassaBank wcash;
	private BigDecimal wsumm;
	private Currency wcurrency;
	private KassaBank dcash;
	private BigDecimal dsumm;
	private Currency dcurrency;
	private BigDecimal rate;
	private BigDecimal total;
	private Boolean ok;
	private String info;
	private User user;
	private String trCode;
	
	public ContractPaymentDto() {
	}
	
	public ContractPaymentDto(Branch branch, Date pdate, ContractPaymentSchedule cps, Contract con, BigDecimal paymentDue, 
			KassaBank wcash, BigDecimal wsumm, Currency wcurrency, 
			KassaBank dcash, BigDecimal dsumm, Currency dcurrency, 
			BigDecimal rate, BigDecimal total, Boolean ok, String info, User user, String trCode) {
		this.branch = branch;
		this.pdate = pdate;
		this.contractPaymentSchedule = cps;
		this.contract = con;
		this.paymentDue = paymentDue;
		this.wcash = wcash;
		this.wsumm = wsumm;
		this.wcurrency = wcurrency;
		this.dcash = dcash;
		this.dsumm = dsumm;
		this.dcurrency = dcurrency;
		this.rate = rate;
		this.total = total;
		this.ok = ok;
		this.info = info;
		this.user = user;
		this.trCode = trCode;
	}
	
	public Date getPdate() {
		return pdate;
	}
	public void setPdate(Date pdate) {
		this.pdate = pdate;
	}
	public ContractPaymentSchedule getContractPaymentSchedule() {
		return contractPaymentSchedule;
	}
	public void setContractPaymentSchedule(ContractPaymentSchedule cps) {
		this.contractPaymentSchedule = cps;
	}
	
	public Contract getContract() {
		return contract;
	}

	public void setContract(Contract contract) {
		this.contract = contract;
	}

	public BigDecimal getPaymentDue() {
		return paymentDue;
	}
	public void setPaymentDue(BigDecimal paymentDue) {
		this.paymentDue = paymentDue;
	}
	public BigDecimal getWsumm() {
		return wsumm;
	}
	public void setWsumm(BigDecimal wsumm) {
		this.wsumm = wsumm;
	}
	public BigDecimal getDsumm() {
		return dsumm;
	}
	public void setDsumm(BigDecimal dsumm) {
		this.dsumm = dsumm;
	}
	public BigDecimal getRate() {
		return rate;
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public Boolean getOk() {
		return ok;
	}
	public void setOk(Boolean ok) {
		this.ok = ok;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getTrCode() {
		return trCode;
	}

	public void setTrCode(String trCode) {
		this.trCode = trCode;
	}

	public Currency getWcurrency() {
		return wcurrency;
	}

	public void setWcurrency(Currency wcurrency) {
		this.wcurrency = wcurrency;
	}

	public Currency getDcurrency() {
		return dcurrency;
	}

	public void setDcurrency(Currency dcurrency) {
		this.dcurrency = dcurrency;
	}
	
	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public KassaBank getWcash() {
		return wcash;
	}

	public void setWcash(KassaBank wcash) {
		this.wcash = wcash;
	}

	public KassaBank getDcash() {
		return dcash;
	}

	public void setDcash(KassaBank dcash) {
		this.dcash = dcash;
	}

	@Override
	public String toString() {
		return "ContractPaymentDto [branch = " + ((branch != null) ? branch.getBranchName() : "null") + ", pdate=" + pdate 
				+ ", contractPaymentSchedule=" + ((contractPaymentSchedule != null) ? contractPaymentSchedule.getId() : "null")
				+ ", contract=" + ((contract != null) ? contract.getId() : "null") + ", paymentDue=" + paymentDue 
				+ "wcash=" + ((wcash != null) ? wcash.getName() : "null") + ", wsumm=" + wsumm + " " + ((wcurrency != null) ? wcurrency.getCurrency() : "null") 
				+ "dcash=" + ((dcash != null) ? dcash.getName() : "null") + ", dsumm=" + dsumm + " " + ((dcurrency != null) ? dcurrency.getCurrency() : "null")
				+ ", rate=" + rate + ", total=" + total + ", ok=" + ok + ", info=" + info 
				+ ", user=" + ((user != null) ? user.getUsername() : "null") + ", trCode=" + trCode + "]";
	}

	@Override
	public ContractPaymentDto clone() throws CloneNotSupportedException {
		return (ContractPaymentDto) super.clone();
	}
}
