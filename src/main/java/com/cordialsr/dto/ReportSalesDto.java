package com.cordialsr.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.cordialsr.domain.Branch;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Employee;

public class ReportSalesDto implements Serializable, Cloneable {

	private Company company;
	private Branch branch;
	private Employee director;
	private Employee manager;
	private Employee dealer;
	private BigDecimal plan;
	private BigDecimal rental;
	private BigDecimal sales;
	private BigDecimal lumpsum;
	private BigDecimal fact;
	
	public ReportSalesDto() {
		
	}
	
	public ReportSalesDto(Company company, Branch branch, Employee director, Employee manager,
						  Employee dealer, BigDecimal plan, BigDecimal fact) {
				this.company = company;
				this.branch = branch;
				this.director = director;
				this.manager = manager;
				this.dealer = dealer;
				this.plan = plan;
				this.fact = fact;
	}
	
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Branch getBranch() {
		return branch;
	}

	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	public Employee getDirector() {
		return director;
	}

	public void setDirector(Employee director) {
		this.director = director;
	}

	public Employee getManager() {
		return manager;
	}

	public void setManager(Employee manager) {
		this.manager = manager;
	}

	public Employee getDealer() {
		return dealer;
	}

	public void setDealer(Employee dealer) {
		this.dealer = dealer;
	}

	public BigDecimal getPlan() {
		return plan;
	}

	public void setPlan(BigDecimal plan) {
		this.plan = plan;
	}

	public BigDecimal getFact() {
		return fact;
	}

	public void setFact(BigDecimal fact) {
		this.fact = fact;
	}
	
	public BigDecimal getRental() {
		return rental;
	}

	public void setRental(BigDecimal rental) {
		this.rental = rental;
	}

	public BigDecimal getSales() {
		return sales;
	}

	public void setSales(BigDecimal sales) {
		this.sales = sales;
	}

	public BigDecimal getLumpsum() {
		return lumpsum;
	}

	public void setLumpsum(BigDecimal lumpsum) {
		this.lumpsum = lumpsum;
	}

	@Override
	public ReportSalesDto clone() throws CloneNotSupportedException {
		return (ReportSalesDto) super.clone();
	}
	
}
