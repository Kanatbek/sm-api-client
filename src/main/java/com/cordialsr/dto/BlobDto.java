package com.cordialsr.dto;

import java.io.Serializable;

// @JsonDeserialize(using = BlobDtoDeserializer.class)
public class BlobDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String bin64;
	
	public BlobDto() {
	}
	
	public BlobDto(String bin64) {
		this.bin64 = bin64;
	}

	public String getBin64() {
		return bin64;
	}

	public void setBin64(String bin64) {
		this.bin64 = bin64;
	}
}