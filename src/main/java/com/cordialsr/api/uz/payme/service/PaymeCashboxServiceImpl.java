package com.cordialsr.api.uz.payme.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cordialsr.api.uz.payme.entity.PaymeCashbox;
import com.cordialsr.api.uz.payme.repository.PaymeCashboxRepo;

@Service
public class PaymeCashboxServiceImpl implements PaymeCashboxService {

	@Autowired
	PaymeCashboxRepo cbRepo;
	
	@Override
	public List<PaymeCashbox> getAllActiveCashboxList() {
		return cbRepo.findByIsActive(true);
	}
	
	@Override
	public PaymeCashbox getFirstActiveCashbox() {
		return cbRepo.getFirstActive();
	}

	@Override
	public PaymeCashbox save(PaymeCashbox cb) {
		if (cb != null) {
			cb = cbRepo.save(cb);	
		}
		return cb;
	}

	@Override
	public Boolean updateCashboxKey(String password) {
		PaymeCashbox pcb = getFirstActiveCashbox();
		if (pcb != null) {
			// pcb.setKey(password);
			pcb.setTestKey(password);
			save(pcb);
			return true;
		}
		return false;
	}
	
}
