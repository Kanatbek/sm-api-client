package com.cordialsr.api.uz.payme.entity.json.result;

import com.cordialsr.api.uz.payme.entity.json.PaymeAccount;

public class GetStatementResult {

	private String id;
	private Long time;
	private Integer amount;
	private PaymeAccount account;
	private Long create_time;
	private Long perform_time;
	private Long cancel_time;
	private Long transaction;
	private Integer state;
	private Integer reason;
	
	public GetStatementResult() {
	}
	
	public GetStatementResult(String id, Long time, Integer amount, PaymeAccount account, Long create_time,
			Long perform_time, Long cancel_time, Long transaction, Integer state, Integer reason) {
		this.id = id;
		this.time = time;
		this.amount = amount;
		this.account = account;
		this.create_time = create_time;
		this.perform_time = perform_time;
		this.cancel_time = cancel_time;
		this.transaction = transaction;
		this.state = state;
		this.reason = reason;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getTime() {
		return time;
	}
	public void setTime(Long time) {
		this.time = time;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public PaymeAccount getAccount() {
		return account;
	}
	public void setAccount(PaymeAccount account) {
		this.account = account;
	}
	public Long getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Long create_time) {
		this.create_time = create_time;
	}
	public Long getPerform_time() {
		return perform_time;
	}
	public void setPerform_time(Long perform_time) {
		this.perform_time = perform_time;
	}
	public Long getCancel_time() {
		return cancel_time;
	}
	public void setCancel_time(Long cancel_time) {
		this.cancel_time = cancel_time;
	}
	public Long getTransaction() {
		return transaction;
	}
	public void setTransaction(Long transaction) {
		this.transaction = transaction;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Integer getReason() {
		return reason;
	}
	public void setReason(Integer reason) {
		this.reason = reason;
	}
	
}
