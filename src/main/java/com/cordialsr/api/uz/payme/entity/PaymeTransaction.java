package com.cordialsr.api.uz.payme.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cordialsr.domain.Party;

@Entity
@Table(name = "api_uz_payme_transaction")
public class PaymeTransaction {

	public static final boolean ENABLE_SALES = true;
	public static final boolean ENABLE_RENTAL = true;
	
	private Long id;
	private String paymeId;
	private String passSerie;
	private String passNo;
	private Party party;
	private BigDecimal amount;
	private Integer state;
	private Calendar paymeTime;
	private Calendar createTime;
	private Calendar performTime;
	private Calendar cancelTime;
	private Integer reason;
	
	public PaymeTransaction() {

	}
	
	public PaymeTransaction(String paymeId, String passSerie, String passNo, BigDecimal amount) {
		this.paymeId = paymeId;
		this.passSerie = passSerie;
		this.passNo = passNo;
		this.amount = amount;
	}

	public PaymeTransaction(String paymeId, String passSerie, String passNo, Party party, BigDecimal amount,
			Integer state, Calendar paymeTime, Calendar createTime) {
		this.paymeId = paymeId;
		this.passSerie = passSerie;
		this.passNo = passNo;
		this.party = party;
		this.amount = amount;
		this.state = state;
		this.paymeTime = paymeTime;
		this.createTime = createTime;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "payme_id", length = 24)
	public String getPaymeId() {
		return paymeId;
	}
	public void setPaymeId(String paymeId) {
		this.paymeId = paymeId;
	}
	
	@Column(name = "pass_serie", length = 2)
	public String getPassSerie() {
		return passSerie;
	}
	public void setPassSerie(String passSerie) {
		this.passSerie = passSerie;
	}
	
	@Column(name = "pass_no", length = 15)
	public String getPassNo() {
		return passNo;
	}
	public void setPassNo(String passNo) {
		this.passNo = passNo;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party_id")
	public Party getParty() {
		return party;
	}
	public void setParty(Party party) {
		this.party = party;
	}
	
	@Column(name = "amount", precision = 16, scale = 4)
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	@Column(name = "state")
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "payme_time")
	public Calendar getPaymeTime() {
		return paymeTime;
	}
	public void setPaymeTime(Calendar paymeTime) {
		this.paymeTime = paymeTime;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time")
	public Calendar getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Calendar createTime) {
		this.createTime = createTime;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "perform_time")
	public Calendar getPerformTime() {
		return performTime;
	}
	public void setPerformTime(Calendar performTime) {
		this.performTime = performTime;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cancel_time")
	public Calendar getCancelTime() {
		return cancelTime;
	}
	public void setCancelTime(Calendar cancelTime) {
		this.cancelTime = cancelTime;
	}
	
	@Column(name = "reason")
	public Integer getReason() {
		return reason;
	}
	public void setReason(Integer reason) {
		this.reason = reason;
	}
	
}
