package com.cordialsr.api.uz.payme.service;

import java.util.List;

import com.cordialsr.api.uz.payme.entity.PaymeCashbox;

public interface PaymeCashboxService {
	
	List<PaymeCashbox> getAllActiveCashboxList();
	PaymeCashbox getFirstActiveCashbox();
	PaymeCashbox save(PaymeCashbox cb);
	Boolean updateCashboxKey(String password);
}
