package com.cordialsr.api.uz.payme.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.cordialsr.api.uz.payme.entity.PaymeCashbox;

public interface PaymeCashboxRepo extends CrudRepository<PaymeCashbox, String> {

	List<PaymeCashbox> findByIsActive(Boolean active);
	
	@Query(value = "Select * from api_uz_payme_cashbox c "
			+ "where c.active = 1 "
			+ "limit 1", 
			nativeQuery = true)
	PaymeCashbox getFirstActive();
	
}
