package com.cordialsr.api.uz.payme.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.PartyDao;
import com.cordialsr.api.uz.payme.common.exception.AccountNotFoundException;
import com.cordialsr.api.uz.payme.common.exception.TransactionNotFoundException;
import com.cordialsr.api.uz.payme.common.exception.UnableCancelTransactionException;
import com.cordialsr.api.uz.payme.common.exception.UnableCompleteException;
import com.cordialsr.api.uz.payme.common.exception.WrongAmountException;
import com.cordialsr.api.uz.payme.entity.OrderCancelReason;
import com.cordialsr.api.uz.payme.entity.PaymeTransaction;
import com.cordialsr.api.uz.payme.entity.TransactionState;
import com.cordialsr.api.uz.payme.entity.json.PaymeAccount;
import com.cordialsr.api.uz.payme.entity.json.PaymeTransactions;
import com.cordialsr.api.uz.payme.entity.json.result.CancelTransactionResult;
import com.cordialsr.api.uz.payme.entity.json.result.ChangePasswordResult;
import com.cordialsr.api.uz.payme.entity.json.result.CheckPerformTransactionResult;
import com.cordialsr.api.uz.payme.entity.json.result.CheckTransactionResult;
import com.cordialsr.api.uz.payme.entity.json.result.CreateTransactionResult;
import com.cordialsr.api.uz.payme.entity.json.result.GetStatementResult;
import com.cordialsr.api.uz.payme.entity.json.result.PerformTransactionResult;
import com.cordialsr.api.uz.payme.repository.PaymeTransactionRepository;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Party;
import com.cordialsr.dto.Money;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.ContractPaymentService;
import com.cordialsr.service.ContractService;
import com.googlecode.jsonrpc4j.spring.AutoJsonRpcServiceImpl;

@Service
@AutoJsonRpcServiceImpl
@Transactional(rollbackFor = Exception.class)
public class PaymeServiceImpl implements PaymeService {

	private static final Long TIME_EXPIRED = 43200000L;
	
	@Autowired 
	ContractService conService;
	
	@Autowired 
	ContractPaymentService cpService;
	
	@Autowired 
	PartyDao partyDao;
	
	@Autowired
	PaymeTransactionRepository paymeTrRepo;
	
	@Autowired
	PaymeCashboxService paymeCbService;

	private Party customer;
	
	@Override
	public CheckPerformTransactionResult CheckPerformTransaction(Integer amount, PaymeAccount account) throws Exception {
		try {
			customer = getCustomer(account.getPass_serie(), account.getPass_no());
			if (customer == null) {
				throw new AccountNotFoundException();
			}
			
			Calendar dateEnd = Calendar.getInstance();
			dateEnd.set(Calendar.YEAR, 3000);
			Money money = conService.getCustomerPaymentDueByID(customer.getId(), dateEnd, Currency.CUR_UZS, PaymeTransaction.ENABLE_RENTAL, PaymeTransaction.ENABLE_SALES);
			amount = amount / 100;
			if (amount < 0 || Integer.parseInt(money.getSumm().toBigInteger().toString()) < amount) {
				throw new WrongAmountException();
			}
			
			return new CheckPerformTransactionResult(true);
		} catch (Exception e) {
			throw e;
		}
	}

	private Party getCustomer(String ps, String pn) throws Exception {
		try {
			if (ps == null || ps.length() != 2) {
				throw new AccountNotFoundException();
			}

			List<String> psL = new ArrayList<>();
			psL.add(ps);
			GeneralUtil.getAllTranslitVariaties(ps, 0, psL);
			
			for (String s : psL) {
				String passNo = s + " " + pn;
				Party cus = partyDao.findByPassportNoUZ(Company.COMPANY_CORDIAL_SERVICE, passNo);
				if (cus != null && !GeneralUtil.isEmptyLong(cus.getId())) 
					return cus;
			}
			return null;
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public CreateTransactionResult CreateTransaction(String id, Calendar time, Integer amount, PaymeAccount account) throws Exception {
		try {
			PaymeTransaction paymeTr = paymeTrRepo.findByPaymeId(id);
			Boolean expired = false;
			if (paymeTr == null) {
				if (CheckPerformTransaction(amount, account).getAllow()) {
					Calendar now = Calendar.getInstance();
					// time.add(Calendar.MILLISECOND, 21600000); // add timezone difference UTC+6
					amount = amount / 100;
					paymeTr = new PaymeTransaction(id, 
								account.getPass_serie(), 
								account.getPass_no(), 
								customer,
								new BigDecimal(amount),
								TransactionState.STATE_IN_PROGRESS.getCode(),
								time,
								Calendar.getInstance()
							);
					paymeTrRepo.save(paymeTr);
				}
			} else {
				if (paymeTr.getState() == TransactionState.STATE_IN_PROGRESS.getCode()) {
					if (System.currentTimeMillis() - paymeTr.getPaymeTime().getTimeInMillis() > TIME_EXPIRED) {
						expired = true;
					}
				}
			}
			if (!expired) {
				return new CreateTransactionResult(
						paymeTr.getCreateTime().getTimeInMillis(),
						paymeTr.getId().toString(),
						paymeTr.getState()
					);
			}
			throw new UnableCompleteException();
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public PerformTransactionResult PerformTransaction(String id) throws Exception {
		try {
			PaymeTransaction paymeTr = paymeTrRepo.findByPaymeId(id);
			if (paymeTr != null) {
				if (paymeTr.getState() == TransactionState.STATE_IN_PROGRESS.getCode()) {
					if (System.currentTimeMillis() - paymeTr.getPaymeTime().getTimeInMillis() > TIME_EXPIRED) {
						paymeTr.setState(TransactionState.STATE_CANCELED.getCode());
						paymeTr.setReason(OrderCancelReason.TRANSACTION_TIMEOUT.getCode());
						paymeTrRepo.save(paymeTr);
					} else {
						
						// PostPayment						
						boolean res = cpService.postPaymeCustomerPayment(paymeTr);
						
						paymeTr.setState(TransactionState.STATE_DONE.getCode());
						paymeTr.setPerformTime(Calendar.getInstance());
						paymeTrRepo.save(paymeTr);
					}
				}
				if (paymeTr.getState() == TransactionState.STATE_DONE.getCode()) {
					return new PerformTransactionResult(
							paymeTr.getId().toString(), 
							paymeTr.getPerformTime().getTimeInMillis(), 
							paymeTr.getState()
						);
				} 
				throw new UnableCompleteException();
			} 
			throw new TransactionNotFoundException();
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public CancelTransactionResult CancelTransaction(String id, OrderCancelReason reason) throws Exception {
		try {
			PaymeTransaction paymeTr = paymeTrRepo.findByPaymeId(id);
			if (paymeTr != null) {
				if (paymeTr.getState() == TransactionState.STATE_IN_PROGRESS.getCode()) {
					paymeTr.setState(TransactionState.STATE_CANCELED.getCode());
					paymeTr.setCancelTime(Calendar.getInstance());
					paymeTr.setReason(reason.getCode());
					paymeTrRepo.save(paymeTr);
				} else if (paymeTr.getState() == TransactionState.STATE_DONE.getCode()) {
					Boolean ableToCancel = false;
					
					// Check, can we cancel a transaction? (example business logic)
					
					if (ableToCancel) {
						
						// Cancel transaction and payment
						
						paymeTr.setState(TransactionState.STATE_POST_CANCELED.getCode());
						paymeTr.setCancelTime(Calendar.getInstance());
						paymeTr.setReason(reason.getCode());
						paymeTrRepo.save(paymeTr);
					} else {
						throw new UnableCancelTransactionException();
					}
				} 
				
				return new CancelTransactionResult(
						paymeTr.getId().toString(), 
						paymeTr.getCancelTime().getTimeInMillis(), 
						paymeTr.getState()
					);
			}
			throw new TransactionNotFoundException();
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public CheckTransactionResult CheckTransaction(String id) throws Exception {
		try {
			PaymeTransaction paymeTr = paymeTrRepo.findByPaymeId(id);
			if (paymeTr != null) {
				return new CheckTransactionResult(
						(paymeTr.getCreateTime() != null) ? paymeTr.getCreateTime().getTimeInMillis() : null, 
						(paymeTr.getPerformTime() != null) ? paymeTr.getPerformTime().getTimeInMillis() : null, 
						(paymeTr.getCancelTime() != null) ? paymeTr.getCancelTime().getTimeInMillis() : null, 
						paymeTr.getId(), 
						paymeTr.getState(), 
						paymeTr.getReason()
					);
			}
			throw new TransactionNotFoundException();
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public PaymeTransactions GetStatement(Calendar from, Calendar to) throws Exception {
		try {
			List<GetStatementResult> result = new ArrayList<>();
			List<PaymeTransaction> trL = paymeTrRepo.findByPaycomTimeAndState(from, to, TransactionState.STATE_DONE.getCode());
			
			for (PaymeTransaction tr : trL) {
				result.add(new GetStatementResult(tr.getPaymeId(),
							(tr.getPaymeTime() != null) ? tr.getPaymeTime().getTimeInMillis() : null, 
							Integer.parseInt(tr.getAmount().toBigInteger().toString()),
							new PaymeAccount(tr.getPassSerie(), tr.getPassNo()),
							(tr.getCreateTime() != null) ? tr.getCreateTime().getTimeInMillis() : null,
							(tr.getPerformTime() != null) ? tr.getPerformTime().getTimeInMillis() : null,
							(tr.getCancelTime() != null) ? tr.getCancelTime().getTimeInMillis() : null,
							tr.getId(),
							tr.getState(),
							tr.getReason()
						)
					);
			}
			return new PaymeTransactions(result);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public ChangePasswordResult ChangePassword(String password) throws Exception {
		try {
			// Here you need to change auth password for further usage by Paycom
			Boolean res = paymeCbService.updateCashboxKey(password);
			return new ChangePasswordResult(res);
		} catch (Exception e) {
			throw e;
		}
	}
	
}
