package com.cordialsr.api.uz.payme.repository;

import java.util.Calendar;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.cordialsr.api.uz.payme.entity.PaymeTransaction;

public interface PaymeTransactionRepository extends CrudRepository<PaymeTransaction, Long> {
	
	PaymeTransaction findByPaymeId(String id);
	
	@Query("select o from PaymeTransaction o " +
		    "where o.paymeTime between ?1 and ?2 " +
		    "and o.state = ?3 " +
		    "ORDER BY paymeTime ASC")
	List<PaymeTransaction> findByPaycomTimeAndState(
			Calendar from, 
			Calendar to, 
			Integer state);
}
