package com.cordialsr.api.uz.payme.entity.json;

import java.util.List;

import com.cordialsr.api.uz.payme.entity.json.result.GetStatementResult;


public class PaymeTransactions {

	private List<GetStatementResult> transactions;
	
	public PaymeTransactions(List<GetStatementResult> transactions) {
		this.transactions = transactions;
	}

	public PaymeTransactions() {
		
	}

	public List<GetStatementResult> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<GetStatementResult> transactions) {
		this.transactions = transactions;
	}
	
}
