package com.cordialsr.api.uz.payme.common.exception;

public class UnableCompleteException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnableCompleteException() {
		
	}
	
	public UnableCompleteException(String message) {
		super(message);
	}	
}
