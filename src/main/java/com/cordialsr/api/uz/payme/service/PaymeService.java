package com.cordialsr.api.uz.payme.service;

import java.util.Calendar;

import com.cordialsr.api.uz.payme.common.exception.AccountNotFoundException;
import com.cordialsr.api.uz.payme.common.exception.TransactionNotFoundException;
import com.cordialsr.api.uz.payme.common.exception.UnableCancelTransactionException;
import com.cordialsr.api.uz.payme.common.exception.UnableCompleteException;
import com.cordialsr.api.uz.payme.common.exception.WrongAmountException;
import com.cordialsr.api.uz.payme.entity.OrderCancelReason;
import com.cordialsr.api.uz.payme.entity.json.PaymeAccount;
import com.cordialsr.api.uz.payme.entity.json.PaymeTransactions;
import com.cordialsr.api.uz.payme.entity.json.result.CancelTransactionResult;
import com.cordialsr.api.uz.payme.entity.json.result.ChangePasswordResult;
import com.cordialsr.api.uz.payme.entity.json.result.CheckPerformTransactionResult;
import com.cordialsr.api.uz.payme.entity.json.result.CheckTransactionResult;
import com.cordialsr.api.uz.payme.entity.json.result.CreateTransactionResult;
import com.cordialsr.api.uz.payme.entity.json.result.PerformTransactionResult;
import com.googlecode.jsonrpc4j.JsonRpcError;
import com.googlecode.jsonrpc4j.JsonRpcErrors;
import com.googlecode.jsonrpc4j.JsonRpcParam;
import com.googlecode.jsonrpc4j.JsonRpcService;

@JsonRpcService("/payme")
public interface PaymeService {
	
	@JsonRpcErrors({
		@JsonRpcError(exception = WrongAmountException.class, 
				code = -31001, 
				message = "Wrong amount", 
				data = "amount"),
		@JsonRpcError(exception = AccountNotFoundException.class, 
				code = -31050, 
				message = "Account not found", 
				data = "account")
		})
	CheckPerformTransactionResult CheckPerformTransaction(
			@JsonRpcParam(value = "amount") Integer amount, 
			@JsonRpcParam(value = "account") PaymeAccount account
		) throws Exception;

	@JsonRpcErrors({
		@JsonRpcError(exception = WrongAmountException.class, 
				code = -31001, 
				message = "Wrong amount", 
				data = "amount"),
		@JsonRpcError(exception = AccountNotFoundException.class, 
				code = -31050, 
				message = "Account not found", 
				data = "account"),
		@JsonRpcError(exception = UnableCompleteException.class, 
				code = -31008, 
				message = "Unable to complete operation", 
				data = "transaction")
		})
	CreateTransactionResult CreateTransaction(
			@JsonRpcParam(value = "id") String id,
			@JsonRpcParam(value = "time") Calendar time,
			@JsonRpcParam(value = "amount") Integer amount,
			@JsonRpcParam(value = "account") PaymeAccount account
		) throws Exception;

	@JsonRpcErrors({
		@JsonRpcError(exception = UnableCompleteException.class, 
				code = -31008, 
				message = "Unable to complete operation", 
				data = "transaction"),
		@JsonRpcError(exception = TransactionNotFoundException.class, 
			code = -31003, 
			message = "Account transaction not found", 
			data = "transaction")
		})
	PerformTransactionResult PerformTransaction(
			@JsonRpcParam(value = "id") String id
		) throws Exception;

	@JsonRpcErrors({
		@JsonRpcError(exception = UnableCancelTransactionException.class, 
				code = -31007, 
				message = "Unable to to cancel transaction", 
				data = "transaction"),
		@JsonRpcError(exception = TransactionNotFoundException.class, 
			code = -31003, 
			message = "Account transaction not found", 
			data = "transaction")
		})
	CancelTransactionResult CancelTransaction(
			@JsonRpcParam(value = "id") String id,
			@JsonRpcParam(value = "reason") OrderCancelReason reason
		) throws Exception;

	@JsonRpcErrors({
		@JsonRpcError(exception = TransactionNotFoundException.class, 
			code = -31003, 
			message = "Account transaction not found", 
			data = "transaction")
		})
	CheckTransactionResult CheckTransaction(
			@JsonRpcParam(value = "id") String id
		) throws Exception;

	PaymeTransactions GetStatement(
			@JsonRpcParam(value = "from") Calendar from, 
			@JsonRpcParam(value = "to") Calendar to
		) throws Exception;

	ChangePasswordResult ChangePassword(
			@JsonRpcParam(value = "password") String password
		) throws Exception;
}
