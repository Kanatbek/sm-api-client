package com.cordialsr.api.uz.payme.entity.json.result;

import java.io.Serializable;

public class CheckPerformTransactionResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Boolean allow;

	public CheckPerformTransactionResult(Boolean allow) {
		this.allow = allow;
	}

	public Boolean getAllow() {
		return allow;
	}

	public void setAllow(Boolean allow) {
		this.allow = allow;
	}
	
}
