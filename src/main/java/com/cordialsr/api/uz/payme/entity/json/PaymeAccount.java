package com.cordialsr.api.uz.payme.entity.json;

public class PaymeAccount {
	
	private String pass_serie;
	private String pass_no;
	
	public PaymeAccount() {
		
	}
	
	public PaymeAccount(String pass_serie, String pass_no) {
		this.pass_serie = pass_serie;
		this.pass_no = pass_no;
	}
	
	public String getPass_serie() {
		return pass_serie;
	}
	public void setPass_serie(String pass_serie) {
		this.pass_serie = pass_serie;
	}
	public String getPass_no() {
		return pass_no;
	}
	public void setPass_no(String pass_no) {
		this.pass_no = pass_no;
	}
	
}
