package com.cordialsr.api.uz.payme.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "api_uz_payme_cashbox")
public class PaymeCashbox {

	private String id;
	private String name;
	private String login;
	private String key;
	private String testKey;
	private Boolean isActive;
	
	public PaymeCashbox() {
	
	}

	public PaymeCashbox(String id, String name, String login, String key, String testKey) {
		this.id = id;
		this.name = name;
		this.login = login;
		this.key = key;
		this.testKey = testKey;
	}

	@Id
	@Column(name = "id", unique = true, nullable = false, length = 45)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "name", length = 45)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "login", length = 45)
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Column(name = "api_key", length = 45)
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	@Column(name = "test_key", length = 45)
	public String getTestKey() {
		return testKey;
	}

	public void setTestKey(String testKey) {
		this.testKey = testKey;
	}

	@Column(name = "active")
	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
}
