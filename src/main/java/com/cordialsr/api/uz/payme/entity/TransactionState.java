package com.cordialsr.api.uz.payme.entity;

public enum TransactionState {
	STATE_NEW(0),
	STATE_IN_PROGRESS(1),
	STATE_DONE(2),
	STATE_CANCELED(-1),
	STATE_POST_CANCELED(-2);
	
	private TransactionState(Integer code) {
		this.code = code;
	}

	private Integer code;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
}
