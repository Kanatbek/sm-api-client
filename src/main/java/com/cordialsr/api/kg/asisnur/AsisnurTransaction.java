package com.cordialsr.api.kg.asisnur;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.cordialsr.domain.Party;

@Entity
@Table(name = "api_kg_asisnur_transaction")
public class AsisnurTransaction {

//	0 ОК 
//	1 Временная ошибка. Повторите запрос позже 
//	4 Неверный формат идентификатора абонента + 
//	5 Идентификатор абонента не найден (Ошиблись номером) + 
//	7 Прием платежа запрещен провайдером + 
//	8 Прием платежа запрещен по техническим причинам + 
//	79 Счет абонента не активен + 
//	90 Проведение платежа не окончено 
//	241 Сумма слишком мала + 
//	242 Сумма слишком велика + 
//	243 Невозможно проверить состояние счета + 
//	300 Другая ошибка провайдера +
	
//	+ - Фатальность ошибки
	
	public static final int PayType_CORDIAL_KG = 1;
	public static final int PayType_CORDIAL_BISHKEK = 2;
	
	// yyyy-MM-dd
	public static final String CORDIAL_KG_CONDITION = "2017-07-01";
//	public static final String CORDIAL_KG_CONDITION = "2000-09-01";
	
	public static final boolean ENABLE_SALES = false;
	public static final boolean ENABLE_RENTAL = true;
		
	private String txn_id;
	private String trm_id;
	private String account;
	private BigDecimal summ;
	private Integer payType;
	private Calendar txn_date;
	private Party party;
	private Integer result;
	private String comment; 
	
	public AsisnurTransaction() {
		
	}
		
	public AsisnurTransaction(String txn_id, String trm_id, String account, BigDecimal summ, Calendar txn_date,
			Party party, Integer result, String comment) {
		this.txn_id = txn_id;
		this.trm_id = trm_id;
		this.account = account;
		this.summ = summ;
		this.txn_date = txn_date;
		this.party = party;
		this.result = result;
		this.comment = comment;
	}
	
	@Id
	// @GeneratedValue(strategy = IDENTITY)
	@Column(name = "txn_id", unique = true, nullable = false, length = 28)
	public String getTxn_id() {
		return txn_id;
	}
	public void setTxn_id(String txn_id) {
		this.txn_id = txn_id;
	}
	
	@Column(name = "trm_id", length = 20)
	public String getTrm_id() {
		return trm_id;
	}
	public void setTrm_id(String trm_id) {
		this.trm_id = trm_id;
	}
	
	@Column(name = "account", nullable = false, length = 20)
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	
	@Column(name = "summ", nullable = false, precision = 14, scale = 2)
	public BigDecimal getSumm() {
		return summ;
	}
	public void setSumm(BigDecimal summ) {
		this.summ = summ;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "txn_date", nullable = false)
	public Calendar getTxn_date() {
		return txn_date;
	}
	public void setTxn_date(Calendar txn_date) {
		this.txn_date = txn_date;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "party")
	public Party getParty() {
		return party;
	}
	public void setParty(Party party) {
		this.party = party;
	}

	@Column(name = "result")
	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	@Column(name = "comment", length = 145)
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@Column(name = "pay_type")
	public Integer getPayType() {
		return payType;
	}

	public void setPayType(Integer payType) {
		this.payType = payType;
	}
	
}
