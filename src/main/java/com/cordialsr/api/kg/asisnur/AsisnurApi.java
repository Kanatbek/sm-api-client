package com.cordialsr.api.kg.asisnur;

import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cordialsr.api.kg.asisnur.service.AsisnurApiService;
import com.cordialsr.api.kg.mobilnik.exception.WrongAccountFormatException;
import com.cordialsr.api.kg.mobilnik.exception.WrongAmountBigException;
import com.cordialsr.api.kg.mobilnik.exception.WrongAmountSmallException;
import com.cordialsr.api.kg.mobilnik.service.MobilnikApiService;
import com.cordialsr.api.uz.payme.common.exception.AccountNotFoundException;
import com.cordialsr.api.uz.payme.common.exception.UnableCompleteException;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.naming.NoNameCoder;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;

@Controller
@RequestMapping(value = "asisnur")
public class AsisnurApi {

	private static final String  datePtrn = "yyyyMMddHHmmss"; 
	
	@Autowired
	AsisnurApiService asisService;
	
	@Transactional(rollbackFor = {Exception.class, 
			UnableCompleteException.class, 
			WrongAccountFormatException.class, 
			AccountNotFoundException.class,
			WrongAmountSmallException.class,
			WrongAmountBigException.class})
	@RequestMapping(method = RequestMethod.GET,
//            consumes = {"application/xml","application/json"},
            produces = "application/xml")
	@ResponseBody
    public ResponseEntity<Object> getFdById(
    		@RequestParam(value = "command") String command,
    		@RequestParam(value = "txn_id") String txnId,
    		@RequestParam(value = "txn_date", required = false) @DateTimeFormat(pattern = datePtrn) Date txnDate,
    		@RequestParam(value = "account") String account,
    		@RequestParam(value = "sum") Double sum,
    		@RequestParam(value = "pay_type") Integer payType,
    		@RequestParam(value = "trm_id", required = false) String trmId
    		) {
		try {
			AsisnurResponse res = asisService.postCommand(command, txnId, txnDate, account, sum, payType, trmId);
			
			XStream xstream = new XStream(new DomDriver("UTF-8", new XmlFriendlyNameCoder("_-", "_")));
			Writer writer = new StringWriter();
			writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
			xstream.processAnnotations(AsisnurResponse.class);
			xstream.toXML(res, writer);
			String xml = writer.toString();
			// System.out.println(xml);
			
			return ResponseEntity.status(HttpStatus.OK).body(xml);
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
		}
    }
	
}
