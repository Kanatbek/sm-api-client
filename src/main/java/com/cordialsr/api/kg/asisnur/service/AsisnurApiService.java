package com.cordialsr.api.kg.asisnur.service;

import java.util.Date;

import com.cordialsr.api.kg.asisnur.AsisnurResponse;

public interface AsisnurApiService {
	AsisnurResponse postCommand(String command, String txnId, Date txnDate, String account, Double sum, Integer payType, String trmId) throws Exception;
}
