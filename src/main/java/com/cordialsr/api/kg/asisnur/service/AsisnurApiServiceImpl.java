package com.cordialsr.api.kg.asisnur.service;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.ContractDao;
import com.cordialsr.Dao.PartyDao;
import com.cordialsr.api.kg.asisnur.AsisnurResponse;
import com.cordialsr.api.kg.asisnur.AsisnurTransaction;
import com.cordialsr.api.kg.asisnur.repository.AsisnurTransactionRepo;
import com.cordialsr.api.kg.mobilnik.exception.WrongAccountFormatException;
import com.cordialsr.api.kg.mobilnik.exception.WrongAmountBigException;
import com.cordialsr.api.kg.mobilnik.exception.WrongAmountSmallException;
import com.cordialsr.api.uz.payme.common.exception.AccountNotFoundException;
import com.cordialsr.api.uz.payme.common.exception.UnableCompleteException;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Party;
import com.cordialsr.dto.Money;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.ContractPaymentService;
import com.cordialsr.service.ContractService;

@Service
@Transactional(rollbackFor = {Exception.class, 
		UnableCompleteException.class, 
		WrongAccountFormatException.class, 
		AccountNotFoundException.class,
		WrongAmountSmallException.class,
		WrongAmountBigException.class})
public class AsisnurApiServiceImpl implements AsisnurApiService {

	@Autowired
	PartyDao partyDao;
	
	@Autowired
	ContractService conService;
	
	@Override
	public AsisnurResponse postCommand(String command, String txnId, Date txnDate, String iinBin, Double sum, Integer payType, String trmId) {
		AsisnurResponse response = new AsisnurResponse();
		try {
			response.setOsmpTxnId(txnId);

			System.out.println("Command: " + command + 
								"    TXNID: " + txnId +
								"    IIN: " + iinBin + 
								"    SUM: " + sum + 
								"    PayType: " + payType);
			
			// Temporarily Disable
			Calendar dline = GeneralUtil.getCalendar(2, 2019);
			System.out.println(dline);

			if (new Date().before(dline.getTime()) && payType == AsisnurTransaction.PayType_CORDIAL_KG) {
				response.setComment("Операция не доступна.");
				response.setResult(AsisnurResponse.RES_RESTRICTED_7F);
			} else {
				if (payType == null 
						|| (payType != AsisnurTransaction.PayType_CORDIAL_KG 
						&& payType != AsisnurTransaction.PayType_CORDIAL_BISHKEK)) {
					response.setComment("Операция не доступна.");
					response.setResult(AsisnurResponse.RES_RESTRICTED_7F);
				} else {
					switch (command) {
						case "check" : {
							ImmutablePair<Party, Money> checkRes = check(iinBin, sum, payType);
							Party customer = checkRes.getLeft();
							Money paymentDue = checkRes.getRight();
							String comment = customer.getFullFIO() + " \n К оплате: " + paymentDue.getSumm().toString() + " " + paymentDue.getCurrency();
							response.setComment(comment);
							response.setResult(AsisnurResponse.RES_OK_0);
							break;
						}
						case "pay" : {
							Integer res = pay(txnId, txnDate, iinBin, sum, payType, trmId);
							response.setResult(res);
							break;
						}
						default: throw new Exception("Unknown method");
					}
				}
			}
			
			return response;
		} catch (UnableCompleteException e) {
			response.setResult(AsisnurResponse.RES_TRANSACTION_NOT_FINISHED_90);
			return response;
		} catch (WrongAccountFormatException e) {
			response.setComment("Ваш поставщик услуг " + e.getMessage());
			response.setResult(AsisnurResponse.RES_WRONG_ACCOUNT_FORMAT_4F);
			return response;
		} catch (AccountNotFoundException e) {
			response.setResult(AsisnurResponse.RES_ACCOUNT_NOT_FOUND_5F);
			return response;
		} catch (WrongAmountSmallException e) {
			response.setResult(AsisnurResponse.RES_SUM_SMALL_241F);
			return response;
		} catch (WrongAmountBigException e) {
			response.setResult(AsisnurResponse.RES_SUM_BIG_242F);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			response.setComment(e.getMessage());
			response.setResult(AsisnurResponse.RES_OTHER_PROVIDER_ERROR_300F);
			return response;
		}
	}
	
	@Autowired
	ContractDao conDao;
	
	private ImmutablePair<Party, Money> check(String iinBin, Double sum, Integer payType) throws Exception {
		try {
			Party customer = getCustomer(iinBin);
			if (customer == null) {
				throw new AccountNotFoundException();
			}
			
			// Clarify the vendor by payType
//			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Date conDate = df.parse(AsisnurTransaction.CORDIAL_KG_CONDITION);
			conDate = GeneralUtil.startOfDay(conDate);
			
			if (payType == AsisnurTransaction.PayType_CORDIAL_KG) {
				Integer count = conDao.getCustomerContractsCountUntil(Company.COMPANY_CORDIAL_SERVICE, customer.getId(), Currency.CUR_KGS, conDate);
				if (GeneralUtil.isEmptyInteger(count)) throw new WrongAccountFormatException("CORDIAL BISHKEK");
			} else if (payType == AsisnurTransaction.PayType_CORDIAL_BISHKEK) {
				Integer count = conDao.getCustomerContractsCountSince(Company.COMPANY_CORDIAL_SERVICE, customer.getId(), Currency.CUR_KGS, conDate);
				if (GeneralUtil.isEmptyInteger(count)) throw new WrongAccountFormatException("CORDIAL KG");
			}
			
			Calendar dateEnd = Calendar.getInstance();
			Money paymentDue = conService.getCustomerPaymentDueByID(customer.getId(), dateEnd, Currency.CUR_KGS, AsisnurTransaction.ENABLE_RENTAL, AsisnurTransaction.ENABLE_SALES);
			dateEnd.set(Calendar.YEAR, 3000);
			Money totalRemain = conService.getCustomerPaymentDueByID(customer.getId(), dateEnd, Currency.CUR_KGS, AsisnurTransaction.ENABLE_RENTAL, AsisnurTransaction.ENABLE_SALES);
			
//			sum = sum / 100;
			if (sum < 0)
				throw new WrongAmountSmallException();
			
			if (Integer.parseInt(totalRemain.getSumm().toBigInteger().toString()) < sum) 
				throw new WrongAmountBigException();
			
			return new ImmutablePair<Party, Money>(customer, paymentDue);
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Autowired 
	AsisnurTransactionRepo asisTrDao;
	
	@Autowired 
	ContractPaymentService cpService;
	
	private Integer pay( String txnId, Date txnDate, String iinBin, Double sum, Integer payType, String trmId) throws Exception {
		try {
			ImmutablePair<Party, Money> checkRes = check(iinBin, sum, payType);
			Party customer = checkRes.getLeft();
			
			AsisnurTransaction asisTr = asisTrDao.findOne(txnId);
			
			if (txnDate == null) throw new Exception("Дата не указана.");
			Calendar cal = Calendar.getInstance();
			cal.setTime(txnDate);
			
			BigDecimal summ = new BigDecimal(sum);
			
			if (asisTr == null) {
				asisTr = new AsisnurTransaction(txnId, trmId, customer.getIinBin(), 
						summ, cal, customer, AsisnurResponse.RES_TRANSACTION_NOT_FINISHED_90, "");
				asisTr.setPayType(payType);
				asisTr = asisTrDao.save(asisTr);
				boolean res = cpService.postAsisnurCustomerPayment(asisTr);
				if (res) asisTr.setResult(0);
			}
			
			return asisTr.getResult();
		} catch (Exception e) {
			throw e;
		}
	}
	
	private Party getCustomer(String iinBin) throws Exception {
		try {
			if (iinBin == null || iinBin.length() < 12) {
				throw new WrongAccountFormatException("Неправильный формат ИИН");
			}
			Party cus = partyDao.findByIinBin(Company.COMPANY_CORDIAL_SERVICE, iinBin);
			if (cus != null && !GeneralUtil.isEmptyLong(cus.getId())) 
				return cus;
			return null;
		} catch (Exception e) {
			throw e;
		}
	}
	
}
