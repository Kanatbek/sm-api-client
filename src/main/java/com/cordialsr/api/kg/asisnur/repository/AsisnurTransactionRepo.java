package com.cordialsr.api.kg.asisnur.repository;

import org.springframework.data.repository.CrudRepository;

import com.cordialsr.api.kg.asisnur.AsisnurTransaction;

public interface AsisnurTransactionRepo extends CrudRepository<AsisnurTransaction, String> {

}
