package com.cordialsr.api.kg.mobilnik.repository;

import org.springframework.data.repository.CrudRepository;

import com.cordialsr.api.kg.mobilnik.MobilnikTransaction;

public interface MobilnikTransactionRepo extends CrudRepository<MobilnikTransaction, String> {

	
	
}
