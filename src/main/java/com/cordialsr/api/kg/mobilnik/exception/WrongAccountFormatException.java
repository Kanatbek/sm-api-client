package com.cordialsr.api.kg.mobilnik.exception;

public class WrongAccountFormatException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public WrongAccountFormatException(String message) {
		super(message);
	}
}
