package com.cordialsr.api.kg.mobilnik.service;

import java.util.Date;

import com.cordialsr.api.kg.mobilnik.MobilnikResponse;

public interface MobilnikApiService {
	MobilnikResponse postCommand(String command, String txnId, Date txnDate, String account, Double sum, Integer payType, String trmId) throws Exception;
}
