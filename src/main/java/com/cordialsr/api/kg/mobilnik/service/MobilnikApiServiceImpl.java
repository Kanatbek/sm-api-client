package com.cordialsr.api.kg.mobilnik.service;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.ContractDao;
import com.cordialsr.Dao.PartyDao;
import com.cordialsr.api.kg.mobilnik.MobilnikResponse;
import com.cordialsr.api.kg.mobilnik.MobilnikTransaction;
import com.cordialsr.api.kg.mobilnik.exception.WrongAccountFormatException;
import com.cordialsr.api.kg.mobilnik.exception.WrongAmountBigException;
import com.cordialsr.api.kg.mobilnik.exception.WrongAmountSmallException;
import com.cordialsr.api.kg.mobilnik.repository.MobilnikTransactionRepo;
import com.cordialsr.api.uz.payme.common.exception.AccountNotFoundException;
import com.cordialsr.api.uz.payme.common.exception.UnableCompleteException;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Party;
import com.cordialsr.dto.Money;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.ContractPaymentService;
import com.cordialsr.service.ContractService;

@Service
@Transactional(rollbackFor = {Exception.class, 
		UnableCompleteException.class, 
		WrongAccountFormatException.class, 
		AccountNotFoundException.class,
		WrongAmountSmallException.class,
		WrongAmountBigException.class})
public class MobilnikApiServiceImpl implements MobilnikApiService {

	@Autowired
	PartyDao partyDao;
	
	@Autowired
	ContractService conService;
	
	@Override
	public MobilnikResponse postCommand(String command, String txnId, Date txnDate, String iinBin, Double sum, Integer payType, String trmId) {
		MobilnikResponse response = new MobilnikResponse();
		try {
			response.setOsmpTxnId(txnId);
			
			// Temporarily Disable
			Calendar dline = GeneralUtil.getCalendar(2, 2017);
			System.out.println(dline);
			
			if (new Date().before(dline.getTime()) && payType == MobilnikTransaction.PayType_CORDIAL_KG) {
				response.setComment("Операция не доступна.");
				response.setResult(MobilnikResponse.RES_RESTRICTED_7F);
			} else {
				if (payType == null  
						|| (payType != MobilnikTransaction.PayType_CORDIAL_KG 
						&& payType != MobilnikTransaction.PayType_CORDIAL_BISHKEK)) {
					response.setComment("Операция не доступна.");
					response.setResult(MobilnikResponse.RES_RESTRICTED_7F);
				} else {
					switch (command) {
						case "check" : {
							ImmutablePair<Party, Money> checkRes = check(iinBin, sum, payType);
							Party customer = checkRes.getLeft();
							Money paymentDue = checkRes.getRight();
							String comment = customer.getFullFIO() + " \n К оплате: " + paymentDue.getSumm().toString() + " " + paymentDue.getCurrency();
							response.setComment(comment);
							response.setResult(MobilnikResponse.RES_OK_0);
							break;
						}
						case "pay" : {
							Integer res = pay(txnId, txnDate, iinBin, sum, payType, trmId);
							response.setResult(res);
							break;
						}
						default: throw new Exception("Unknown method");
					}
				}
			}
			
			return response;
		} catch (UnableCompleteException e) {
			response.setResult(MobilnikResponse.RES_TRANSACTION_NOT_FINISHED_90);
			return response;
		} catch (WrongAccountFormatException e) {
			response.setComment("Ваш поставщик услуг " + e.getMessage());
			response.setResult(MobilnikResponse.RES_WRONG_ACCOUNT_FORMAT_4F);
			return response;
		} catch (AccountNotFoundException e) {
			response.setResult(MobilnikResponse.RES_ACCOUNT_NOT_FOUND_5F);
			return response;
		} catch (WrongAmountSmallException e) {
			response.setResult(MobilnikResponse.RES_SUM_SMALL_241F);
			return response;
		} catch (WrongAmountBigException e) {
			response.setResult(MobilnikResponse.RES_SUM_BIG_242F);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			response.setComment(e.getMessage());
			response.setResult(MobilnikResponse.RES_OTHER_PROVIDER_ERROR_300F);
			return response;
		}
	}
	
	@Autowired
	ContractDao conDao;
	
	private ImmutablePair<Party, Money> check(String iinBin, Double sum, Integer payType) throws Exception {
		try {
			Party customer = getCustomer(iinBin);
			if (customer == null) {
				throw new AccountNotFoundException();
			}
			
			// Clarify the vendor by payType
//			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Date conDate = df.parse(MobilnikTransaction.CORDIAL_KG_CONDITION);
			conDate = GeneralUtil.startOfDay(conDate);
			
			if (payType == MobilnikTransaction.PayType_CORDIAL_KG) {
				Integer count = conDao.getCustomerContractsCountUntil(Company.COMPANY_CORDIAL_SERVICE, customer.getId(), Currency.CUR_KGS, conDate);
				if (GeneralUtil.isEmptyInteger(count)) throw new WrongAccountFormatException("CORDIAL BISHKEK");
			} else if (payType == MobilnikTransaction.PayType_CORDIAL_BISHKEK) {
				Integer count = conDao.getCustomerContractsCountSince(Company.COMPANY_CORDIAL_SERVICE, customer.getId(), Currency.CUR_KGS, conDate);
				if (GeneralUtil.isEmptyInteger(count)) throw new WrongAccountFormatException("CORDIAL KG");
			} 
			
			Calendar dateEnd = Calendar.getInstance();
			Money paymentDue = conService.getCustomerPaymentDueByID(customer.getId(), dateEnd, Currency.CUR_KGS, MobilnikTransaction.ENABLE_RENTAL, MobilnikTransaction.ENABLE_SALES);
			dateEnd.set(Calendar.YEAR, 3000);
			Money totalRemain = conService.getCustomerPaymentDueByID(customer.getId(), dateEnd, Currency.CUR_KGS, MobilnikTransaction.ENABLE_RENTAL, MobilnikTransaction.ENABLE_SALES);
			
//			sum = sum / 100;
			if (sum < 0) 
				throw new WrongAmountSmallException();
			
			if (Integer.parseInt(totalRemain.getSumm().toBigInteger().toString()) < sum) 
				throw new WrongAmountBigException();
			
			return new ImmutablePair<Party, Money>(customer, paymentDue);
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Autowired 
	MobilnikTransactionRepo mobTrDao;
	
	@Autowired 
	ContractPaymentService cpService;
	
	private Integer pay( String txnId, Date txnDate, String iinBin, Double sum, Integer payType, String trmId) throws Exception {
		try {
			ImmutablePair<Party, Money> checkRes = check(iinBin, sum, payType);
			Party customer = checkRes.getLeft();
			
			MobilnikTransaction mobilTr = mobTrDao.findOne(txnId);
			
			if (txnDate == null) throw new Exception("Дата не указана.");
			Calendar cal = Calendar.getInstance();
			cal.setTime(txnDate);
			
			BigDecimal summ = new BigDecimal(sum);
			
			if (mobilTr == null) {
				mobilTr = new MobilnikTransaction(txnId, trmId, customer.getIinBin(), 
						summ, cal, customer, MobilnikResponse.RES_TRANSACTION_NOT_FINISHED_90, "");
				mobilTr.setPayType(payType);
				mobilTr = mobTrDao.save(mobilTr);
				boolean res = cpService.postMobilnikCustomerPayment(mobilTr);
				if (res) mobilTr.setResult(0);
			}
			
			return mobilTr.getResult();
		} catch (Exception e) {
			throw e;
		}
	}
	
	private Party getCustomer(String iinBin) throws Exception {
		try {
			if (iinBin == null || iinBin.length() < 12) {
				throw new WrongAccountFormatException("Неправильный формат ИИН");
			}
			Party cus = partyDao.findByIinBin(Company.COMPANY_CORDIAL_SERVICE, iinBin);
			if (cus != null && !GeneralUtil.isEmptyLong(cus.getId())) 
				return cus;
			return null;
		} catch (Exception e) {
			throw e;
		}
	}
	
}
