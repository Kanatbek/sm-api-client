package com.cordialsr.api.kg.qiwi.repository;

import org.springframework.data.repository.CrudRepository;

import com.cordialsr.api.kg.qiwi.QiwiKgTransaction;

public interface QiwiKgTransactionRepo extends CrudRepository<QiwiKgTransaction, String> {

	
	
}
