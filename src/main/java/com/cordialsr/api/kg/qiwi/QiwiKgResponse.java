package com.cordialsr.api.kg.qiwi;

import java.math.BigDecimal;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("response")
public class QiwiKgResponse {

	//	+ - Фатальность ошибки
	
	//	Код		Комментарий														Фатальность
	//	0		ОК
	//	1		Временная ошибка. Повторите запрос позже
	//	4		Неверный формат идентификатора абонента								+
	//	5		Идентификатор абонента не найден									+
	//	7		Прием платежа запрещен провайдером/Счет абонента не активен		
	//			(При отмене платежа – отказ провайдера в отмене платежа)			+
	//	8		Прием платежа запрещен по техническим причинам						+
	//	79		Счет абонента не активен											+
	//	90		Проведение платежа не окончено 
	//			(При отмене платежа – отмена еще не подтверждена. 
	//			Система отправит повторный запрос через некоторое время.)
	//	241		Сумма слишком мала													+
	//	242		Сумма слишком велика												+
	//	243		Невозможно проверить состояние счета								+
	//	300		Другая ошибка провайдера											+
		
	//	Знак «+» в столбце Фатальность показывает то, как Система будет интерпретировать данную ошибку.
	
	public static final int RES_OK_0 = 0;
	public static final int RES_TEMP_ERR_1 = 1;
	public static final int RES_WRONG_ACCOUNT_FORMAT_4F = 4;
	public static final int RES_ACCOUNT_NOT_FOUND_5F = 5;
	public static final int RES_RESTRICTED_7F = 7;
	public static final int RES_TECH_ISSUE_8F = 8;
	public static final int RES_ACCOUNT_DISABLED_79F = 79;
	public static final int RES_TRANSACTION_NOT_FINISHED_90 = 90;
	public static final int RES_SUM_SMALL_241F = 241;
	public static final int RES_SUM_BIG_242F = 242;
	public static final int RES_UNABLE_TO_CHECK_ACCOUNT_STATUS_243F = 243;
	public static final int RES_OTHER_PROVIDER_ERROR_300F = 300;
	
	@XStreamAlias("osmp_txn_id")
	private String osmpTxnId;
	
	@XStreamAlias("prv_txn")
	private String prvTxn;
	
	private BigDecimal sum;
	
	@XStreamAlias("pay_type")
	private String payType;
	
	private Integer result;
	private String comment;
	
	public QiwiKgResponse() {
		
	}
	
	public QiwiKgResponse(String osmpTxnId, String prvTxn, BigDecimal sum, Integer result, String comment) {
		this.osmpTxnId = osmpTxnId;
		this.prvTxn = prvTxn;
		this.sum = sum;
		this.result = result;
		this.comment = comment;
	}

	public String getOsmpTxnId() {
		return osmpTxnId;
	}

	public void setOsmpTxnId(String osmpTxnId) {
		this.osmpTxnId = osmpTxnId;
	}

	public String getPrvTxn() {
		return prvTxn;
	}

	public void setPrvTxn(String prvTxn) {
		this.prvTxn = prvTxn;
	}

	public BigDecimal getSum() {
		return sum;
	}

	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}

	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}
	
}
