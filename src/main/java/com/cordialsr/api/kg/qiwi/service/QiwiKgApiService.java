package com.cordialsr.api.kg.qiwi.service;

import java.util.Date;

import com.cordialsr.api.kg.qiwi.QiwiKgResponse;

public interface QiwiKgApiService {
	QiwiKgResponse postCommand(String command, String txnId, Date txnDate, String account, Double sum, String trmId) throws Exception;
}
