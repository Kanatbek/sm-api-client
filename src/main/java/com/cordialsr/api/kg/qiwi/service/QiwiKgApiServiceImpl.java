package com.cordialsr.api.kg.qiwi.service;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.Dao.ContractDao;
import com.cordialsr.Dao.PartyDao;
import com.cordialsr.api.kg.mobilnik.exception.WrongAccountFormatException;
import com.cordialsr.api.kg.mobilnik.exception.WrongAmountBigException;
import com.cordialsr.api.kg.mobilnik.exception.WrongAmountSmallException;
import com.cordialsr.api.kg.qiwi.QiwiKgResponse;
import com.cordialsr.api.kg.qiwi.QiwiKgTransaction;
import com.cordialsr.api.kg.qiwi.repository.QiwiKgTransactionRepo;
import com.cordialsr.api.uz.payme.common.exception.AccountNotFoundException;
import com.cordialsr.api.uz.payme.common.exception.UnableCompleteException;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Currency;
import com.cordialsr.domain.Party;
import com.cordialsr.dto.Money;
import com.cordialsr.general.GeneralUtil;
import com.cordialsr.service.ContractPaymentService;
import com.cordialsr.service.ContractService;

@Service
@Transactional(rollbackFor = {Exception.class, 
		UnableCompleteException.class, 
		WrongAccountFormatException.class, 
		AccountNotFoundException.class,
		WrongAmountSmallException.class,
		WrongAmountBigException.class})
public class QiwiKgApiServiceImpl implements QiwiKgApiService {

	@Autowired
	PartyDao partyDao;
	
	@Autowired
	ContractService conService;
	
	@Override
	public QiwiKgResponse postCommand(String command, String txnId, Date txnDate, String iinBin, Double sum, String trmId) {
		QiwiKgResponse response = new QiwiKgResponse();
		try {
			response.setOsmpTxnId(txnId);
			
			// Temporarily Disable
			Calendar dline = GeneralUtil.getCalendar(2, 2019);
			System.out.println(dline);
			
			if (new Date().before(dline.getTime())) {
				response.setComment("Операция не доступна.");
				response.setResult(QiwiKgResponse.RES_RESTRICTED_7F);
			} else {
				switch (command) {
					case "check" : {
						ImmutablePair<Party, Money> checkRes = check(iinBin, sum);
						Party customer = checkRes.getLeft();
						Money paymentDue = checkRes.getRight();
						String comment = customer.getFullFIO() + " \n К оплате: " + paymentDue.getSumm().toString() + " " + paymentDue.getCurrency();
						response.setComment(comment);
						response.setResult(QiwiKgResponse.RES_OK_0);
						break;
					}
					case "pay" : {
						Integer res = pay(txnId, txnDate, iinBin, sum, trmId);
						response.setPrvTxn(txnId);
						response.setSum(new BigDecimal(sum));
						response.setResult(res);
						if (res == 0) response.setComment("OK");
						break;
					}
					default: throw new Exception("Unknown method");
				}
			}
			
			return response;
		} catch (UnableCompleteException e) {
			response.setResult(QiwiKgResponse.RES_TRANSACTION_NOT_FINISHED_90);
			return response;
		} catch (WrongAccountFormatException e) {
			response.setComment("Ваш поставщик услуг " + e.getMessage());
			response.setResult(QiwiKgResponse.RES_WRONG_ACCOUNT_FORMAT_4F);
			return response;
		} catch (AccountNotFoundException e) {
			response.setResult(QiwiKgResponse.RES_ACCOUNT_NOT_FOUND_5F);
			return response;
		} catch (WrongAmountSmallException e) {
			response.setResult(QiwiKgResponse.RES_SUM_SMALL_241F);
			return response;
		} catch (WrongAmountBigException e) {
			response.setResult(QiwiKgResponse.RES_SUM_BIG_242F);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			response.setComment(e.getMessage());
			response.setResult(QiwiKgResponse.RES_OTHER_PROVIDER_ERROR_300F);
			return response;
		}
	}
	
	@Autowired
	ContractDao conDao;
	
	private ImmutablePair<Party, Money> check(String iinBin, Double sum) throws Exception {
		try {
			Party customer = getCustomer(iinBin);
			if (customer == null) {
				throw new AccountNotFoundException();
			}
			
			Calendar dateEnd = Calendar.getInstance();
			Money paymentDue = conService.getCustomerPaymentDueByID(customer.getId(), dateEnd, Currency.CUR_KGS, QiwiKgTransaction.ENABLE_RENTAL, QiwiKgTransaction.ENABLE_SALES);
			dateEnd.set(Calendar.YEAR, 3000);
			Money totalRemain = conService.getCustomerPaymentDueByID(customer.getId(), dateEnd, Currency.CUR_KGS, QiwiKgTransaction.ENABLE_RENTAL, QiwiKgTransaction.ENABLE_SALES);
			
			if (sum < 0) 
				throw new WrongAmountSmallException();
			
			if (Integer.parseInt(totalRemain.getSumm().toBigInteger().toString()) < sum) 
				throw new WrongAmountBigException();
			
			return new ImmutablePair<Party, Money>(customer, paymentDue);
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Autowired 
	QiwiKgTransactionRepo qiwiKgTrDao;
	
	@Autowired 
	ContractPaymentService cpService;
	
	private Integer pay( String txnId, Date txnDate, String iinBin, Double sum, String trmId) throws Exception {
		try {
			ImmutablePair<Party, Money> checkRes = check(iinBin, sum);
			Party customer = checkRes.getLeft();
			
			QiwiKgTransaction qiwiKgTr = qiwiKgTrDao.findOne(txnId);
			
			if (txnDate == null) throw new Exception("Дата не указана.");
			Calendar cal = Calendar.getInstance();
			cal.setTime(txnDate);
			
			BigDecimal summ = new BigDecimal(sum);
			
			if (qiwiKgTr == null) {
				qiwiKgTr = new QiwiKgTransaction(txnId, trmId, customer.getIinBin(), 
						summ, cal, customer, QiwiKgResponse.RES_TRANSACTION_NOT_FINISHED_90, "");
				qiwiKgTr = qiwiKgTrDao.save(qiwiKgTr);
				boolean res = cpService.postQiwiKgCustomerPayment(qiwiKgTr);
				if (res) qiwiKgTr.setResult(0);
			}
			
			return qiwiKgTr.getResult();
		} catch (Exception e) {
			throw e;
		}
	}
	
	private Party getCustomer(String iinBin) throws Exception {
		try {
			if (iinBin == null || iinBin.length() < 12) {
				throw new WrongAccountFormatException("Неправильный формат ИИН");
			}
			Party cus = partyDao.findByIinBin(Company.COMPANY_CORDIAL_SERVICE, iinBin);
			if (cus != null && !GeneralUtil.isEmptyLong(cus.getId())) 
				return cus;
			return null;
		} catch (Exception e) {
			throw e;
		}
	}
	
}
