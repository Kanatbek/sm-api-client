package com.cordialsr;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.domain.Company;
import com.cordialsr.service.MigrationService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SmMigration {

	@Test
	public void contextLoads() {
		migrateContracts();
//		generatePaymentSchedForSales();
//		addFinDocsForNewContracts();
//		addInvoiceNumbers();
//		addDealerContractAwards();
		
//		updateContractContacts();
	}
	
	@Autowired 
	MigrationService migService;
	
//	@Autowired
//	private SessionFactory sessionFactory;
	
//	@Autowired
//	private EntityManager entityManager;
	
	@Transactional(rollbackFor = Exception.class)
	private void migrateContracts() {
		int step = 100;
		for (int i = 0; i < 250; i++) {
//		for (int i = 0; i < 1; i++) {
			migService.migrateContracts((i * step), step);
//			sessionFactory.getCurrentSession().flush();
//			entityManager.flush();
		}
	}
	

	@Transactional(rollbackFor = Exception.class)
	private void generatePaymentSchedForSales() {
		int step = 100;
		for (int i = 0; i < 250; i++) {
//		for (int i = 0; i < 1; i++) {
					migService.generatePaymentSchedForSales((i * step), step);
		}
	}
	
	@Transactional(rollbackFor = Exception.class)
	private void updateContractContacts() {
		migService.updateContractAddrAndPhones(21);
		migService.updateContractAddrAndPhones(24);
		migService.updateContractAddrAndPhones(28);
	}
	
	@Transactional(rollbackFor = Exception.class)
	private void addFinDocsForNewContracts() {
		int step = 1000;
		for (int i = 0; i < 1; i++) {
			migService.addFinDocsForNewContracts(step);
//			sessionFactory.getCurrentSession().flush();
//			entityManager.flush();
		}
	}
	
	@Transactional(rollbackFor = Exception.class)
	private void addInvoiceNumbers() {
		migService.addInvoiceNumbers();
	}
	
	@Transactional(rollbackFor = Exception.class)
	private void addDealerContractAwards() {
		
//		2	ALMATY-1
//		3	ALMATY-2
//		4	ASTANA-1
//		5	AKTAU-1
//		6	AKTOBE-1
//		7	ATYRAU-1
//		8	KARAGANDY-1
//		9	OSKEMEN-1
//		10	KYZYLORDA-1
//		11	ORAL-1
//		12	TARAZ-1
//		13	KOKSHETAU-1
//		14	SHYMKENT-1
//		15	TALDYKORGAN-1
//		16	ZHANAOZEN-1
//		20	TURKESTAN
//		23	QORDAY

		
//		migService.appendDealerPremis(Company.COMPANY_CORDIAL_SERVICE, 17, 2, 2016);
//		migService.appendDealerPremis(Company.COMPANY_CORDIAL_SERVICE, 27, 2, 2016);
		
//		migService.appendDealerPremis(Company.COMPANY_CORDIAL_SERVICE, 17, 5, 2018);
//		migService.appendDealerPremis(Company.COMPANY_CORDIAL_SERVICE, 27, 5, 2018);
		
		migService.appendDealerPremis(Company.COMPANY_CORDIAL_SERVICE, 2, 5, 2018);
		migService.appendDealerPremis(Company.COMPANY_CORDIAL_SERVICE, 3, 5, 2018);
//		migService.appendDealerPremis(Company.COMPANY_CORDIAL_SERVICE, 4, 5, 2018);
//		migService.appendDealerPremis(Company.COMPANY_CORDIAL_SERVICE, 5, 5, 2018);
//		migService.appendDealerPremis(Company.COMPANY_CORDIAL_SERVICE, 6, 5, 2018);
//		migService.appendDealerPremis(Company.COMPANY_CORDIAL_SERVICE, 7, 5, 2018);
//		migService.appendDealerPremis(Company.COMPANY_CORDIAL_SERVICE, 8, 5, 2018);
//		migService.appendDealerPremis(Company.COMPANY_CORDIAL_SERVICE, 9, 5, 2018);
//		migService.appendDealerPremis(Company.COMPANY_CORDIAL_SERVICE, 10, 5, 2018);
//		migService.appendDealerPremis(Company.COMPANY_CORDIAL_SERVICE, 11, 5, 2018);
//		migService.appendDealerPremis(Company.COMPANY_CORDIAL_SERVICE, 12, 5, 2018);
//		migService.appendDealerPremis(Company.COMPANY_CORDIAL_SERVICE, 13, 5, 2018);
//		migService.appendDealerPremis(Company.COMPANY_CORDIAL_SERVICE, 14, 5, 2018);
//		migService.appendDealerPremis(Company.COMPANY_CORDIAL_SERVICE, 15, 5, 2018);
//		migService.appendDealerPremis(Company.COMPANY_CORDIAL_SERVICE, 16, 5, 2018);
//		migService.appendDealerPremis(Company.COMPANY_CORDIAL_SERVICE, 20, 5, 2018);
//		migService.appendDealerPremis(Company.COMPANY_CORDIAL_SERVICE, 23, 5, 2018);
	}

}
