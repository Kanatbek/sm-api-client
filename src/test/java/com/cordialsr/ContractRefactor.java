package com.cordialsr;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.service.ContractPostRefactorService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ContractRefactor {

	@Test
	public void contextLoads() {
		try {
			
//			shiftContractDate("121-00D5EB", "2018-05-30");
//			shiftContractDate("121-00E87E", "2018-02-25");
			
			
//			stornoContractPayment("121-00E1D6", 400000d);
//			stornoContractPayment("U01-0000596", 200000d);
			
//			stornoContractPayment("121-00D48B", 200000d);
			
//			numerateCaws();
			
//			genrateCawsForCoordinator();
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
	}
	
	
	@Autowired 
	ContractPostRefactorService cprService;
	
	@Transactional(rollbackFor = Exception.class)
	private void shiftContractDate(String conNum, String dateStr) throws Exception {
		try {
			
			cprService.shiftContractDate(conNum, dateStr);
			
		} catch (Exception e) {
			throw e;
		}
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	private void stornoContractPayment(String conNum, Double sum) throws Exception {
		try {
			if (sum > 0) cprService.stornoContractPayment(conNum, sum);
			else throw new Exception("Sum [" + sum + "] is not acceptable.");
		} catch (Exception e) {
			throw e;
		}
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	private void numerateCaws() throws Exception {
		try {
			
			// cprService.numeratePaymentOrderForCAWS(17);
			cprService.numeratePaymentOrderForCAWS(21);
			cprService.numeratePaymentOrderForCAWS(24);
			cprService.numeratePaymentOrderForCAWS(27);
			cprService.numeratePaymentOrderForCAWS(28);
			
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Transactional(rollbackFor = Exception.class)
	private void genrateCawsForCoordinator() throws Exception {
		try {
			
//			cprService.genrateCawsForCoordinator(17, 3, 2019);
//			cprService.genrateCawsForCoordinator(27, 3, 2019);
//			
//			cprService.genrateCawsForCoordinator(21, 3, 2019);
//			cprService.genrateCawsForCoordinator(24, 3, 2019);
//			cprService.genrateCawsForCoordinator(28, 3, 2019);
			
//			cprService.genrateCawsForCoordinator(17, 4, 2019);
//			cprService.genrateCawsForCoordinator(27, 4, 2019);
//			
//			cprService.genrateCawsForCoordinator(21, 4, 2019);
//			cprService.genrateCawsForCoordinator(24, 4, 2019);
//			cprService.genrateCawsForCoordinator(28, 4, 2019);
//			
//			cprService.genrateCawsForCoordinator(17, 5, 2019);
//			cprService.genrateCawsForCoordinator(27, 5, 2019);
//			
//			cprService.genrateCawsForCoordinator(21, 5, 2019);
//			cprService.genrateCawsForCoordinator(24, 5, 2019);
//			cprService.genrateCawsForCoordinator(28, 5, 2019);
			
			

			
		} catch (Exception e) {
			throw e;
		}
	}
	
}
