package com.cordialsr;

import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.cordialsr.dto.Money;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SmCentralBankRates {

	@Test
	public void contextLoads() {
//		 getNationalBankRates();
	}
	
	private void getNationalBankRates() {
		try {
			String url = "http://www.nationalbank.kz/rss/rates_all.xml?switch=kazakh";
			// String res = rest.get(url);
			URL rssUrl = new URL (url);
//			BufferedReader in = new BufferedReader(new InputStreamReader(rssUrl.openStream()));
//			// System.out.println(res);
//			String sourceCode = "";
//            String line;
//			while ((line = in.readLine()) != null) {
//				sourceCode += line;
////			    int titleEndIndex = 0;
////			    int titleStartIndex = 0;
////			    while (titleStartIndex >= 0) {
////			        titleStartIndex = line.indexOf("<title>", titleEndIndex);
////			        if (titleStartIndex >= 0) {
////			            titleEndIndex = line.indexOf("</title>", titleStartIndex);
////			            sourceCode += line.substring(titleStartIndex + "<title>".length(), titleEndIndex) + "\n";
////			        }
////			    }
//			}
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(rssUrl.openStream());
			NodeList nl = doc.getChildNodes();
			
			List<Money> rates = new ArrayList<>();  
			
			printNodes(nl, "   ", rates);
	        
			System.out.println("**************************************************************************************");
			
			System.out.println(rates);
			
	        // assertTrue("Reponse is empty", res == null || res.length() > 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
        
	}
	
	public void printNodes(NodeList nl, String prefix, List<Money> rates) {
		for (int i = 0; i < nl.getLength(); i++) {
			Node node = nl.item(i);
			if (node.getParentNode().getNodeName() == "title" 
					&& node.getNodeValue().length() == 3) {
				Money newRate = new Money(node.getNodeValue());
				rates.add(newRate);
			}
			
			if (node.getParentNode().getNodeName() == "description" && rates.size() > 0) {
				Money rate = rates.get(rates.size() - 1);
				rate.setSumm(new BigDecimal(node.getNodeValue()));
			}
			
			
			System.out.println(prefix + node.getNodeName() + " = " + node.getNodeValue() + " | " + node.getNodeType());
			if (node.hasChildNodes()) {
				printNodes(node.getChildNodes(), prefix + "   ", rates);
			}
		}
	}
	
}
