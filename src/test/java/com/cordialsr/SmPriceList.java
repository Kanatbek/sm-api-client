package com.cordialsr;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit4.SpringRunner;

import com.cordialsr.Dao.PriceListDao;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.PriceList;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SmPriceList {

	@Test
	public void contextLoads() {
//		testPriceList();
	}
	
	@Autowired 
	PriceListDao plDao;
	
	private void testPriceList() {
		
		PriceList pl = new PriceList();
		pl.setCompany(new Company(1));
		pl.setInventory(new Inventory(1));
		
		List<PriceList> plL = plDao.findAll(Example.of(pl));
		
		System.out.println(plL);
	}

}
