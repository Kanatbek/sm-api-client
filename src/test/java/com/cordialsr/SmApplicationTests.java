package com.cordialsr;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.cordialsr.Dao.PartyDao;
import com.cordialsr.Dao.PriceListDao;
import com.cordialsr.Dao.SmFilterChangeDao;
import com.cordialsr.domain.Company;
import com.cordialsr.domain.FinEntry;
import com.cordialsr.domain.Inventory;
import com.cordialsr.domain.Party;
import com.cordialsr.domain.PriceList;
import com.cordialsr.dto.Money;
import com.cordialsr.general.RestClient;
import com.cordialsr.service.MigrationService;
import com.cordialsr.service.PayrollService;
import com.cordialsr.service.StaffAccountService;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.data.JRTableModelDataSource;
import net.sf.jasperreports.view.JasperViewer;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SmApplicationTests {

	private RestClient rest = new RestClient();
	
	@Test
	public void contextLoads() {
		
		// testRestApi();
		// testSmfc();
		// testJasper();
		// migrateContracts();
		// getNationalBankRates();
//		testPriceList();
	}
	
	@Autowired 
	PriceListDao plDao;
	
	private void testPriceList() {
		
		PriceList pl = new PriceList();
		pl.setCompany(new Company(1));
		pl.setInventory(new Inventory(1));
		
		List<PriceList> plL = plDao.findAll(Example.of(pl));
		
		System.out.println(plL);
	}
	
	@Autowired 
	MigrationService migService;
	
//	@Autowired
//	private SessionFactory sessionFactory;
	
//	@Autowired
//	private EntityManager entityManager;
	
	@Transactional(rollbackFor = Exception.class)
	private void migrateContracts() {
		int step = 100;
		for (int i = 0; i < 137; i++) {
			migService.migrateContracts((i * step), step);
//			sessionFactory.getCurrentSession().flush();
//			entityManager.flush();
		}
	}
	
	private void getNationalBankRates() {
		try {
			String url = "http://www.nationalbank.kz/rss/rates_all.xml?switch=kazakh";
			// String res = rest.get(url);
			URL rssUrl = new URL (url);
//			BufferedReader in = new BufferedReader(new InputStreamReader(rssUrl.openStream()));
//			// System.out.println(res);
//			String sourceCode = "";
//            String line;
//			while ((line = in.readLine()) != null) {
//				sourceCode += line;
////			    int titleEndIndex = 0;
////			    int titleStartIndex = 0;
////			    while (titleStartIndex >= 0) {
////			        titleStartIndex = line.indexOf("<title>", titleEndIndex);
////			        if (titleStartIndex >= 0) {
////			            titleEndIndex = line.indexOf("</title>", titleStartIndex);
////			            sourceCode += line.substring(titleStartIndex + "<title>".length(), titleEndIndex) + "\n";
////			        }
////			    }
//			}
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(rssUrl.openStream());
			NodeList nl = doc.getChildNodes();
			
			List<Money> rates = new ArrayList<>();  
			
			printNodes(nl, "   ", rates);
	        
			System.out.println("**************************************************************************************");
			
			System.out.println(rates);
			
	        // assertTrue("Reponse is empty", res == null || res.length() > 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
        
	}
	
	public void printNodes(NodeList nl, String prefix, List<Money> rates) {
		for (int i = 0; i < nl.getLength(); i++) {
			Node node = nl.item(i);
			if (node.getParentNode().getNodeName() == "title" 
					&& node.getNodeValue().length() == 3) {
				Money newRate = new Money(node.getNodeValue());
				rates.add(newRate);
			}
			
			if (node.getParentNode().getNodeName() == "description" && rates.size() > 0) {
				Money rate = rates.get(rates.size() - 1);
				rate.setSumm(new BigDecimal(node.getNodeValue()));
			}
			
			
			System.out.println(prefix + node.getNodeName() + " = " + node.getNodeValue() + " | " + node.getNodeType());
			if (node.hasChildNodes()) {
				printNodes(node.getChildNodes(), prefix + "   ", rates);
			}
		}
	}
	
	private void testRestApi() {
		String res = rest.get("users");
		System.out.println(res);
		assertTrue("Reponse is empty", res == null || res.length() > 0);
	}
	
	@Autowired
	SmFilterChangeDao smfcDao;
	
	private void testSmfc() {
		Object o = smfcDao.actCancCount();
		System.out.println(o);
		assertTrue("Reponse: ", o != null);
	}

	private DefaultTableModel tableModel;
	
	@Autowired
	StaffAccountService stfaccService;
	
	@Autowired
	PartyDao partyDao;
	
	
	private void testJasper() {
		try {
			List<FinEntry> feL = stfaccService.getSbJournal(1, 2, 112L, "KZT", "2018-01-01", "2018-05-01");
			
			JasperReport jr = JasperCompileManager.compileReport("src/main/resources/reports/finance/payroll/staffCashJournal.jrxml");
			
			Map<String, Object> params = new HashMap<>();
			Party staff = partyDao.findOne(112L);
			params.put("staff", staff.getFullFIO());
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Date dsDate = df.parse("2018-01-01");
			params.put("dateStart", dsDate);
			Date deDate = df.parse("2018-05-01");
			params.put("dateEnd", deDate);
			
			JasperPrint jp = JasperFillManager.fillReport(jr, params, new JRBeanCollectionDataSource(feL));
			
//			JasperViewer jv = new JasperViewer(jp);
//			jv.setVisible(true);
			
//			byte[] pdf = JasperExportManager.exportReportToPdf(jp);
			JasperExportManager.exportReportToPdfFile(jp, "src/main/resources/reports/finance/payroll/sbjournal.pdf");
			
			
			
		} catch( Exception e) {
			e.printStackTrace();
		}
	}
	
}
