package com.cordialsr;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cordialsr.Dao.PartyDao;
import com.cordialsr.domain.FinEntry;
import com.cordialsr.domain.Party;
import com.cordialsr.service.StaffAccountService;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SmJasper {

	@Test
	public void contextLoads() {
//		 testJasper();
	}

	@Autowired
	StaffAccountService stfaccService;
	
	@Autowired
	PartyDao partyDao;
	
	
	private void testJasper() {
		try {
			List<FinEntry> feL = stfaccService.getSbJournal(1, 2, 112L, "KZT", "2018-01-01", "2018-05-01");
			
			JasperReport jr = JasperCompileManager.compileReport("src/main/resources/reports/finance/payroll/staffCashJournal.jrxml");
			
			Map<String, Object> params = new HashMap<>();
			Party staff = partyDao.findOne(112L);
			params.put("staff", staff.getFullFIO());
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Date dsDate = df.parse("2018-01-01");
			params.put("dateStart", dsDate);
			Date deDate = df.parse("2018-05-01");
			params.put("dateEnd", deDate);
			
			JasperPrint jp = JasperFillManager.fillReport(jr, params, new JRBeanCollectionDataSource(feL));
			
//			JasperViewer jv = new JasperViewer(jp);
//			jv.setVisible(true);
			
//			byte[] pdf = JasperExportManager.exportReportToPdf(jp);
			JasperExportManager.exportReportToPdfFile(jp, "src/main/resources/reports/finance/payroll/sbjournal.pdf");
			
			
			
		} catch( Exception e) {
			e.printStackTrace();
		}
	}
	
}
