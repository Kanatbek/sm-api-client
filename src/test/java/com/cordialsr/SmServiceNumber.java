package com.cordialsr;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cordialsr.service.SmServService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SmServiceNumber {

	@Test
	public void contextLoads() {
//		generateServiceNumbers();
	}

	@Autowired
	SmServService smService;
	
	@Transactional
	private void generateServiceNumbers() {
		try {
			smService.assignServiceNumbers();
        } catch( Exception e) {
			e.printStackTrace();
		}
	}
	
}
