package com.cordialsr;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cordialsr.Dao.SmFilterChangeDao;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SmFilterChange {

	@Test
	public void contextLoads() {
//		 testSmfc();
	}
	@Autowired
	SmFilterChangeDao smfcDao;
	
	private void testSmfc() {
		Object o = smfcDao.actCancCount();
		System.out.println(o);
		assertTrue("Reponse: ", o != null);
	}

}
