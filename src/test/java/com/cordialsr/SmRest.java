package com.cordialsr;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.cordialsr.general.RestClient;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SmRest {

	private RestClient rest = new RestClient();
	
	@Test
	public void contextLoads() {
//		 testRestApi();
	}
	
	private void testRestApi() {
		String res = rest.get("users");
		System.out.println(res);
		assertTrue("Reponse is empty", res == null || res.length() > 0);
	}
	
}
